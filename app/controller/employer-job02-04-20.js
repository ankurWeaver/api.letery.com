const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const sendsmscontroller = require('../service/TwilioSms');
const simplemailercontroller = require('../service/mailer');
var http = require('http');
const config =  require('../config/env');
const fcm = require('../service/fcm');
let newRoleArrDb=[];
const employerJobBusiness = require("../businessLogic/employer-job-business");






module.exports = {

    /*
        This function is written to fetch all the job related notification
        for a particular employer
    */
    fetchNotificationforEmployer: (req, res, next) => {
        try{

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Employer Id"                
            });

            let pageNo = req.body.pageNo ? (parseInt(req.body.pageNo == 0 ? 0 : parseInt(req.body.pageNo) - 1)) : 0;
            let perPage = req.body.perPage ? parseInt(req.body.perPage) : 10;

            if (!req.body.employerId) { return; }


            let aggrQuery = [
                {
                    $match: {
                        'employerId': employerId
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'notification': 1
                    }
                },
                {
                    $unwind : "$notification"
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
         
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', pageNo * perPage , perPage
                            ]
                        }
                    }
                }
            ];

            models.employer.aggregate(aggrQuery).exec((err, response) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });

                    return;
                }

                if (response.length < 1) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['1009'],
                        statuscode: 1009,
                        details: null
                    });

                    return;
                }

                if (response.length > 0) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: response
                    });

                    return;
                }
            })    

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }     
    },    

    determineShiftnameLogic1: (req, res, next) => {

        let allSlotContainer=[];
        try{
            /*
            morning: 6.00-12.00
            Afternoon:12.00-18.00
            evening:18.00-24.00
            night:24.00-6.00
            */
           let startTime = req.body.startTime ? req.body.startTime : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Start Time"
            });
            let endTime = req.body.endTime ? req.body.endTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - End Time"
                });
            if (!req.body.startTime || !req.body.endTime) { return; }
            let givenStarttime=parseInt(startTime.replace('.',''));
            let givenEndtime=parseInt(endTime.replace('.',''));
            let startTimeExists='';
            let endTimeExists='';
            let lowerLimit=0;
            let upperLimit=0;
           models.timeslot.find({}, async function (err, item) {
               
                allSlotContainer=item;
                await allSlotContainer.map(time=>{
                    let startTime=parseInt(time.starttime.replace('.',''));
                    let endTime=parseInt(time.endTime.replace('.',''));
                    if(givenStarttime>=startTime && givenStarttime<endTime){
                        startTimeExists=time.slotName
                    }
                    if(givenEndtime>=startTime && givenEndtime<endTime){
                        endTimeExists=time.slotName
                    }
                })

                

                if(startTimeExists==''){
                    startTimeExists="Night" 
                }
                if(endTimeExists==''){
                    endTimeExists="Night" 
                }
                switch(startTimeExists){
                    case "Morning":lowerLimit=1;
                                    break;
                    case "Afternoon":lowerLimit=2;
                                        break;
                    case "Evening":lowerLimit=3;
                                    break;
                    case "Night":lowerLimit=4;
                                    break;
                }
                switch(endTimeExists){
                    case "Morning":upperLimit=1;
                                break;
                    case "Afternoon":upperLimit=2;
                                    break;
                    case "Evening":upperLimit=3;
                                break;
                    case "Night":upperLimit=4;
                                break;
                }

                //=====================
                //these code is written by ankur on  02-01-2020
                // if ( ( (givenStarttime >= 6)&&(givenStarttime < 12) ) && ( (givenEndtime >= 12)&&(givenEndtime < 24) )  ) {
                //     res.json({
                //         finalShift:"Full Day"
                //     })
                //     return;
                // }
                  

                //========================
                if(lowerLimit==upperLimit){
                    res.json({
                       finalShift:startTimeExists
                    })
                    return;
                }
                if(lowerLimit<upperLimit){
                    if(lowerLimit+1==upperLimit){
                        res.json({
                            finalShift:startTimeExists
                        })
                        return;
                    }
                    else{
                        res.json({
                            finalShift:"Full Day"
                        })
                        return;
                    }
                }
                else{
                    if(lowerLimit-2==upperLimit){
                        res.json({
                           finalShift:"Full Day"
                        })
                        return;
                    }
                    else if(lowerLimit-1==upperLimit){
                        res.json({
                           finalShift:"Full Day"
                        })
                        return;
                    }
                    else{
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            finalShift: startTimeExists
                          
                        })
                        return;
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    inviteToApply: (req, res, next) => {
        try{
            let inviteToApplyData = req.body.inviteToApplyData ? req.body.inviteToApplyData : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Invite to apply data",
                isError: true
            });
            inviteToApplyData.map(async tt => {
                console.log(tt['jobId'])
                console.log(tt['employerId'])
                console.log(tt['candidateId'])
                console.log(tt['roleId'])
                let insertedValue={
                    "roleId":tt['roleId'],"employerId":tt['employerId'],
                    "jobId":tt['jobId'],"candidateId":tt['candidateId']
                }
                models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, { $push: { "invitationToApplyJobs":insertedValue} },  function (updatederror, updatedresponse) {
                    if (updatederror) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        }) 
                    }
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        }) 
                    }
                })
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    offerWithFavouriteOne: (req, res, next) => {
        try{
            let tier1pageno = req.body.tier1pageno ? (parseInt(req.body.tier1pageno == 0 ? 0 : parseInt(req.body.tier1pageno) - 1)) : 0;
            let tier1perpage = req.body.tier1perpage ? parseInt(req.body.tier1perpage) : 10;
            let tier2pageno = req.body.tier2pageno ? (parseInt(req.body.tier2pageno == 0 ? 0 : parseInt(req.body.tier2pageno) - 1)) : 0;
            let tier2perpage = req.body.tier2perpage ? parseInt(req.body.tier2perpage) : 10;
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';
           
            if (!req.body.jobId ) { return; }
            let jobaggrQuery

            if (roleName != '') {

                jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            'jobDetails.roleWiseAction.roleName': roleName, 
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },
                    {
                        $redact: {
                            $cond: {
                                if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction": "$jobDetails.roleWiseAction",

                        }
                    },
                    
                ]

            } else {
                if (roleId != '') {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                } else {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                }



            }

                    
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    //console.log(searchJob[0].employerId)
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    console.log("newSkillArrDb==>"+newSkillArrDb)
                    console.log("skilldbbackuplen==>"+skilldbbackup.length)
                    //============
                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
                    

                    //=====================
                  
                    

                    if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
                        skilldbbackup = newSkillArrDb;
                       
                    }



                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 
                    console.log("skilldbbackup==>"+skilldbbackup);
                    console.log("skilldbbackupLen==>"+skilldbbackup.length);  
                    
                    let aggrQuery=[
                        //=============
                        {
                            $geoNear: {
                               near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                               distanceField: "dist.calculated",
                               maxDistance: parseInt(distance),
                               includeLocs: "dist.location",
                               spherical: true
                            }
                        },

                        //==================  
                       
                        { "$unwind": "$skills" }, 
                    
                        {
                            "$match": {  $and: [
                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                            ]}
                        },

                        // {
                        //     "$match": {  $and: [
                        //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                        //     ]}
                        // },
                       
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },
                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            profilePic:1,
                            jobOffers:1,
                            appliedJobs:1,
                            invitationToApplyJobs:1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }
    
                         }},
                      
                         { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            profilePic:1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    
                         }},
                        
                         {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },
                              
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                                "profilePic": {
                                    $first: "$profilePic"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                },
                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },

                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "favouriteBy": {
                                    $first: "$favouriteBy"
                                },
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                favouriteBy:1,
                                skillDetails: 1,
                                isFavourite: 1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                }
                               
    
                            }
                         },
                        {
                            $facet:{
                                "TIER1":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        profilePic:1,
                                        mname: 1,
                                        lname:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1
                
                                     }},
                                     { "$unwind": "$favouriteBy" }, 
                                     {
                                        '$match': { 'favouriteBy': searchJob[0].employerId }
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                         "_id":0
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier1pageno * tier1perpage , tier1perpage
                                                ]
                                            }
                                        }
                                    }


                                ],
                                "TIER2":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        profilePic:1,
                                        isSentInvitation:1
                
                                     }},
                                     { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                    
                                     {
                                        '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "isSentInvitation": {
                                                $first: "$isSentInvitation"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                        "_id":0
                                       }
                                    },
                                    { '$match': 
                                        { 
                                            'rating': {"$gte":3},
                                            'isFavourite': false,
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier2pageno * tier2perpage , tier2perpage
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                        
                    ]



                    //==================
                     let aggrQuery1 = [
                        //=============
                        // {
                        //     $geoNear: {
                        //        near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                        //        distanceField: "dist.calculated",
                        //        maxDistance: parseInt(distance),
                        //        includeLocs: "dist.location",
                        //        spherical: true
                        //     }
                        // },

                        //==================  
                       
                        { "$unwind": "$skills" }, 
                        {
                            "$match": {  $and: [
                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                            

                            ]}
                        },
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },
                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            profilePic:1,
                            jobOffers:1,
                            appliedJobs:1,
                            invitationToApplyJobs:1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }
    
                         }},
                         { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            profilePic:1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    
                         }},
                        
                         {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },
                              
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                                "profilePic": {
                                    $first: "$profilePic"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                },
                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },

                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "favouriteBy": {
                                    $first: "$favouriteBy"
                                },
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                favouriteBy:1,
                                skillDetails: 1,
                                isFavourite: 1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                // isOffered:{
                                //     $cond: {
                                //         "if": { "$eq": [ "$appliedJobs.jobId", jobId] }, 
                                //         "then": true,
                                //          "else": false
                                //     }
                                // }
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                }
                               
    
                            }
                         },
                        {
                            $facet:{
                                "TIER1":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        profilePic:1,
                                        mname: 1,
                                        lname:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1
                
                                     }},
                                     { "$unwind": "$favouriteBy" }, 
                                     {
                                        '$match': { 'favouriteBy': searchJob[0].employerId }
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                         "_id":0
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier1pageno * tier1perpage , tier1perpage
                                                ]
                                            }
                                        }
                                    }


                                ],
                                "TIER2":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        profilePic:1,
                                        isSentInvitation:1
                
                                     }},
                                     { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                    
                                     {
                                        '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "isSentInvitation": {
                                                $first: "$isSentInvitation"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                        "_id":0
                                       }
                                    },
                                    { '$match': 
                                        { 
                                            'rating': {"$gte":3},
                                            'isFavourite': false,
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier2pageno * tier2perpage , tier2perpage
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                    //=====================


                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //================

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //================
                    //=======================================================================
                    

                    //================
                       // this function is used to fetch the candidate irrespective of location
                      async function fetchCandidatewithoutLocation() { 
                         
                          return new Promise(function(resolve, reject){
                                

                                models.candidate.aggregate(aggrQuery1, function (err, response) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "response": response,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } //async function fetchCandidatewithoutLocation() { 

                    //==================


                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                                              
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        }
                        else{

                           

                           /*
                            if (data[0]['TIER2'].length > 0) {

                                let k;
                                let keytoDelete = [];
                                
                                for (k = 0; k < data[0]['TIER2'][0].results.length; k++) { // loop1
                                    
                                    let doDelete = 'NO';
                                    //======================
                                    // Here, we are seraching the details of hired jobs with respect to candidate
                                    let fetchHiredDateStatus = await fetchHiredDate(data[0]['TIER2'][0].results[k].candidateId);

                                    if (fetchHiredDateStatus.isError == true) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });

                                        return;
                                    }

                                    let p;
                            
                                    for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {
                                        

                                        let m;
                            
                                        for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
                                            

                                            let jobStartDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].startDate);
                                            let jobEndDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].endDate);
                                            
                                            let jobdate=new Date(jobStartDate).getDate();
                                            let jobmonth=new Date(jobStartDate).getMonth()+1;
                                            let jobyear=new Date(jobStartDate).getFullYear();

                                            let  jobIds = [];

                                            let i;
                                            for (i = 0; i < fetchHiredDateStatus.responseCandidate[0].hiredJobs.length; i++) {
                                                jobIds.push(fetchHiredDateStatus.responseCandidate[0].hiredJobs[i].jobId);
                                            }

                                            // Here, we are fetching set time 
                                            let fetchJobDetailStatus = await fetchJobDetail(jobIds);

                                            if (fetchJobDetailStatus.isError == true) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });

                                                return;
                                            }

                                            let j; 
                                            let startDate;
                                            let endDate;
                                            let startDateReadable

                                            for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                                
                                                startDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].startDate);
                                                endDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].endDate);
                                                //console.log(startDate + "=====" + endDate);

                                                if ((jobStartDate < startDate) && (startDate > jobEndDate)) {
                                                    
                                                    doDelete = 'YES';
                                                    break;
                                                }

                                                if ((jobStartDate < endDate) && (endDate > jobEndDate)) {
                                                   
                                                    doDelete = 'YES';
                                                    break;
                                                }

                                           
                                                let hiredate=new Date(startDate).getDate();
                                                let hiremonth=new Date(startDate).getMonth()+1;
                                                let hireyear=new Date(startDate).getFullYear();

                                                if ( (hiredate == jobdate) && (hiremonth == jobmonth) && (hireyear == jobyear) ) {
                                                    doDelete = 'YES';
                                                    break;
                                                }
                                                
                                                                                
                                            } // END for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                                // if (doDelete == 'YES') {
                                                //     break;   
                                                // } 
                                             
                                        } // for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
                                            // if (doDelete == 'YES') {
                                            //     break;   
                                            // } 

                                    } // END for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {   

                                    if (doDelete == 'YES') {
                                        keytoDelete.push(k);    
                                    } 

                                } // for (k = 0; k < data[0]['TIER2'][0].results.length; k++) { // loop1

                                keytoDelete.sort();
                                keytoDelete.reverse();

                                let o;
                                for (o = 0; o < keytoDelete.length; o++) {
                                    data[0]['TIER2'][0].results.splice(keytoDelete[o], 1)
                                }    
                                                                    
                                
                            } // END if (data[0]['TIER2'].length > 0) { 




                           

                            if (data[0]['TIER1'].length > 0) {

                                
                               
                    
                                let k;
                                let keytoDelete2 = [];
                                
                                for (k = 0; k < data[0]['TIER1'][0].results.length; k++) { // loop1
                                console.log("loop1")    
                                    let doDelete = 'NO';
                                    //======================
                                    // Here, we are seraching the details of hired jobs with respect to candidate
                                    let fetchHiredDateStatus = await fetchHiredDate(data[0]['TIER1'][0].results[k].candidateId);

                                    if (fetchHiredDateStatus.isError == true) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });

                                        return;
                                    }



                                    let p;
                            
                                    for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {
                                    console.log("loop2")    

                                        let m;
                            
                                        for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
                                        console.log("loop3")    

                                            let jobStartDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].startDate);
                                            let jobEndDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].endDate);
                                            
                                            let jobdate=new Date(jobStartDate).getDate();
                                            let jobmonth=new Date(jobStartDate).getMonth()+1;
                                            let jobyear=new Date(jobStartDate).getFullYear();

                                            let  jobIds = [];

                                            let i;
                                            for (i = 0; i < fetchHiredDateStatus.responseCandidate[0].hiredJobs.length; i++) {
                                                jobIds.push(fetchHiredDateStatus.responseCandidate[0].hiredJobs[i].jobId);
                                            }



                                            // Here, we are fetching set time 
                                            let fetchJobDetailStatus = await fetchJobDetail(jobIds);

                                            if (fetchJobDetailStatus.isError == true) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });

                                                return;
                                            }

                                            
                                            let j; 
                                            let startDate;
                                            let endDate;
                                            let startDateReadable

                                            for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                             console.log("loop4")      
                                                startDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].startDate);
                                                endDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].endDate);
                                                //console.log(startDate + "=====" + endDate);

                                                if ((jobStartDate < startDate) && (startDate > jobEndDate)) {
                                                    
                                                    doDelete = 'YES';
                                                    break;
                                                }

                                                if ((jobStartDate < endDate) && (endDate > jobEndDate)) {
                                                   
                                                    doDelete = 'YES';
                                                    break;
                                                }

                                           
                                                let hiredate=new Date(startDate).getDate();
                                                let hiremonth=new Date(startDate).getMonth()+1;
                                                let hireyear=new Date(startDate).getFullYear();

                                                if ( (hiredate == jobdate) && (hiremonth == jobmonth) && (hireyear == jobyear) ) {
                                                    doDelete = 'YES';
                                                    break;
                                                }
                                                console.log('dododelete'+doDelete);
                                                                                
                                            } // END for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                                // if (doDelete == 'YES') {
                                                //     break;   
                                                // } 
                                             
                                        } // for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
                                            // if (doDelete == 'YES') {
                                            //     break;   
                                            // } 

                                    } // END for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {   

                                    // if (doDelete == 'YES') {
                                    //     keytoDelete.push(k);    
                                    // } 

                                } // for (k = 0; k < data[0]['TIER2'][0].results.length; k++) { // loop1

                                keytoDelete2.sort();
                                keytoDelete2.reverse();

                                let o;
                                for (o = 0; o < keytoDelete2.length; o++) {
                                    data[0]['TIER1'][0].results.splice(keytoDelete2[o], 1)
                                }    
                                                                  
                                
                            } // END if (data[0]['TIER1'].length > 0) { 
                             */   
                               
                            let data;
                            
                            if (response[0]['TIER1'].length > 0 || response[0]['TIER2'].length > 0) {
                                data = response;
                            } else {
                                let fetchCandidatewithoutLocationStatus = await fetchCandidatewithoutLocation();
                                
                                data = fetchCandidatewithoutLocationStatus.response;
                            }   
                         
                            if (data[0]['TIER1'].length > 0 || data[0]['TIER2'].length > 0) {
                                    
                                if(data[0]['TIER1'].length) {
                                    // ==============================
                                    let offerJobStatus;
                                    let tier1Data = data[0]['TIER1'][0].results;
                                    let tier1DataTag;
                                    let sendNotificationEmailStatus;
                                    let sendNotificationSmsStatus;

                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                        
                                        if (tier1Data[tier1DataTag].isOffered == false) {                        
                                            offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                            

                                            if ( offerJobStatus.isError == false) {
                                                
                                                
                                                findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                                

                                                if (findMailStatus.isError == false) {
                                                    tier1Data[tier1DataTag].isOffered = true;
                                                    if (findMailStatus.isItemExist == true) {
                                                            
                                                            if(findMailStatus.item.mobileNo == ""){
                                                                sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                
                                                            }
                                                            else{
                                                                sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                
                                                            }

                                                    } // END if (findMailStatus.isItemExist == true) {
                                                } // END if (findMailStatus.isError == false) {
                                            } // END if ( offerJobStatus.isError == false) {

                                            
                                        }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    data[0]['TIER1'][0].results = tier1Data;

                                    // ================================ 
                                }   



                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                               
                                    details:{
                                        "TIER1":data[0]['TIER1'][0],
                                        "TIER2":data[0]['TIER2'][0],
                                       
                                    }
                                })
                            }   


                            if (data[0]['TIER1'].length < 1 && data[0]['TIER2'].length < 1) {
                                res.json({
                                    isError: false,
                                    message: 'No Data Found',
                                    statuscode: 204,
                               
                                    details:null
                                })
                            }                         
                            
                        }// END else{
                    })
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    //===========
    offerWithFavouriteOneAnd: (req, res, next) => {
        try{
            let tier1pageno = req.body.tier1pageno ? (parseInt(req.body.tier1pageno == 0 ? 0 : parseInt(req.body.tier1pageno) - 1)) : 0;
            let tier1perpage = req.body.tier1perpage ? parseInt(req.body.tier1perpage) : 10;
            let tier2pageno = req.body.tier2pageno ? (parseInt(req.body.tier2pageno == 0 ? 0 : parseInt(req.body.tier2pageno) - 1)) : 0;
            let tier2perpage = req.body.tier2perpage ? parseInt(req.body.tier2perpage) : 10;
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let skillArrb = [];

            skillArrb = req.body.skillArrb ?  req.body.skillArrb : [];
           
            if (!req.body.jobId) { return; }
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }
           
            
            let jobaggrQuery

            if (roleName != '') {

                jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            'jobDetails.roleWiseAction.roleName': roleName, 
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },
                    {
                        $redact: {
                            $cond: {
                                if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction": "$jobDetails.roleWiseAction",

                        }
                    },
                    
                ]

            } else {
                if (roleId != '') {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                } else {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                }



            }

                    
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    //console.log(searchJob[0].employerId)
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    //============
                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
                    

                    //=====================
                  
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 
                    let aggrQuery=[
                        //=============
                        {
                            $geoNear: {
                               near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                               distanceField: "dist.calculated",
                               maxDistance: parseInt(distance),
                               includeLocs: "dist.location",
                               spherical: true
                            }
                        },

                        //==================  
                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            skills:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            profilePic:1,
                            jobOffers:1,
                            appliedJobs:1,
                            invitationToApplyJobs:1,
                            isExist:{
                                $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                            },
    
                        }},

                        { '$match': 
                            { 
                                'isExist': true,
                            }
                        },

                    
                       
                        { "$unwind": "$skills" }, 
                        {
                            "$match": {  $and: [
                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                            

                            ]}
                        },
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },
                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            profilePic:1,
                            jobOffers:1,
                            appliedJobs:1,
                            invitationToApplyJobs:1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }
    
                         }},
                         { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            profilePic:1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    
                         }},
                        
                         {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },
                              
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                                "profilePic": {
                                    $first: "$profilePic"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                },
                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },

                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "favouriteBy": {
                                    $first: "$favouriteBy"
                                },
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                favouriteBy:1,
                                skillDetails: 1,
                                isFavourite: 1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                }
                               
    
                            }
                         },
                        {
                            $facet:{
                                "TIER1":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        profilePic:1,
                                        mname: 1,
                                        lname:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1
                
                                     }},
                                     { "$unwind": "$favouriteBy" }, 
                                     {
                                        '$match': { 'favouriteBy': searchJob[0].employerId }
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                         "_id":0
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier1pageno * tier1perpage , tier1perpage
                                                ]
                                            }
                                        }
                                    }


                                ],
                                "TIER2":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        profilePic:1,
                                        isSentInvitation:1
                
                                     }},
                                     { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                    
                                     {
                                        '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "isSentInvitation": {
                                                $first: "$isSentInvitation"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                        "_id":0
                                       }
                                    },
                                    { '$match': 
                                        { 
                                            'rating': {"$gte":3},
                                            'isFavourite': false,
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier2pageno * tier2perpage , tier2perpage
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }

                       
                    ]



                    //==================
                     let aggrQuery1 = [
                        //=============
                        // {
                        //     $geoNear: {
                        //        near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                        //        distanceField: "dist.calculated",
                        //        maxDistance: parseInt(distance),
                        //        includeLocs: "dist.location",
                        //        spherical: true
                        //     }
                        // },

                        //==================

                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            skills:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            profilePic:1,
                            jobOffers:1,
                            appliedJobs:1,
                            invitationToApplyJobs:1,
                            isExist:{
                                $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                            },
    
                        }},

                        { '$match': 
                            { 
                                'isExist': true,
                            }
                        },
  
                       
                        { "$unwind": "$skills" }, 
                        {
                            "$match": {  $and: [
                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                            

                            ]}
                        },
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },
                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            profilePic:1,
                            jobOffers:1,
                            appliedJobs:1,
                            invitationToApplyJobs:1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }
    
                         }},
                         { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:searchJob[0].employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            profilePic:1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    
                         }},
                        
                         {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },
                              
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                                "profilePic": {
                                    $first: "$profilePic"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                },
                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },

                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "favouriteBy": {
                                    $first: "$favouriteBy"
                                },
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                favouriteBy:1,
                                skillDetails: 1,
                                isFavourite: 1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                }
                               
    
                            }
                         },
                        {
                            $facet:{
                                "TIER1":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        profilePic:1,
                                        mname: 1,
                                        lname:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1
                
                                     }},
                                     { "$unwind": "$favouriteBy" }, 
                                     {
                                        '$match': { 'favouriteBy': searchJob[0].employerId }
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                         "_id":0
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier1pageno * tier1perpage , tier1perpage
                                                ]
                                            }
                                        }
                                    }


                                ],
                                "TIER2":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        profilePic:1,
                                        isSentInvitation:1
                
                                     }},
                                     { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                    
                                     {
                                        '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "isSentInvitation": {
                                                $first: "$isSentInvitation"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            }
                                        }
                                    },
                                    { $project: {
                                        "_id":0
                                       }
                                    },
                                    { '$match': 
                                        { 
                                            'rating': {"$gte":3},
                                            'isFavourite': false,
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier2pageno * tier2perpage , tier2perpage
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                    //=====================


                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //================

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //================
                    //=======================================================================
                    

                    //================
                       // this function is used to fetch the candidate irrespective of location
                      async function fetchCandidatewithoutLocation() { 

                         
                            return new Promise(function(resolve, reject){
                                

                                models.candidate.aggregate(aggrQuery1, function (err, response) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "response": response,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } //async function fetchCandidatewithoutLocation() { 

                    //==================


                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {



                                           
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        }
                        else{

                           
                         
                               
                            let data;
                            
                            if (response[0]['TIER1'].length > 0 || response[0]['TIER2'].length > 0) {
                            // if (response[0].TIER1.length > 0 || response[0].TIER2.length > 0) {    
                                data = response;
                            } else {

                                let fetchCandidatewithoutLocationStatus = await fetchCandidatewithoutLocation();
                                
                                data = fetchCandidatewithoutLocationStatus.response;

                                if (data[0]['TIER1'].length < 1 && data[0]['TIER2'].length < 1) {
                                     res.json({
                                        isError: false,
                                        message: 'No Data Found',
                                        statuscode: 204,
                                   
                                        details:null
                                    })

                                    return; 
                                }    
                           
                                
                            }   

                           
                            if (data[0]['TIER1'].length > 0 || data[0]['TIER2'].length > 0) {
                            // if (data[0]['TIER1'][0].results.length > 0 || data[0]['TIER2'][0].results.length > 0) {
                                
                                // ==============================
                                if (data[0]['TIER1'].length > 0) {
                                    let offerJobStatus;
                                    let tier1Data = data[0]['TIER1'][0].results;
                                    let tier1DataTag;
                                    let sendNotificationEmailStatus;
                                    let sendNotificationSmsStatus;

                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                        
                                        if (tier1Data[tier1DataTag].isOffered == false) {                        
                                            offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                            

                                            if ( offerJobStatus.isError == false) {
                                                
                                                
                                                findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                                

                                                if (findMailStatus.isError == false) {
                                                    tier1Data[tier1DataTag].isOffered = true;
                                                    if (findMailStatus.isItemExist == true) {
                                                            
                                                            if(findMailStatus.item.mobileNo == ""){
                                                                sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                
                                                            }
                                                            else{
                                                                sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                
                                                            }

                                                    } // END if (findMailStatus.isItemExist == true) {
                                                } // END if (findMailStatus.isError == false) {
                                            } // END if ( offerJobStatus.isError == false) {

                                            
                                        }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    data[0]['TIER1'][0].results = tier1Data;
                                }
                                    // ================================
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                               
                                    details:{
                                        "TIER1":data[0]['TIER1'][0],
                                        "TIER2":data[0]['TIER2'][0],
                                       
                                    }
                                })

                                return;
                            }   


                            if (data[0]['TIER1'][0].results.length < 1 && data[0]['TIER2'][0].results.length < 1) {
                                res.json({
                                    isError: false,
                                    message: 'No Data Found',
                                    statuscode: 204,
                               
                                    details:null
                                })

                                return;
                            }  

                            res.json({
                                    isError: false,
                                message: 'No Data Found',
                                statuscode: 204,
                           
                                details:null
                            })                       
                            
                        }// END else{
                    })
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    // ============
    // this function is written to fetch the candidate list on the basis of and opperation
    //=============
    browseCandidateForOfferAnd: (req, res, next) => {
        try{
            let upperLimit;
            let lowerLimit;
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let rating = req.body.rating ? req.body.rating :'';
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let skillArrb = [];

            skillArrb = req.body.skillArrb ? req.body.skillArrb : [];
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }
           
            // if (!req.body.jobId ||!req.body.roleId) { return; }
            if (!req.body.jobId) { return; }
            
            switch(rating){
                case "":upperLimit=5;
                        lowerLimit=1;
                        break;
                case "1":upperLimit=5;
                         lowerLimit=1;
                         break;
                case "2":upperLimit=5;
                        lowerLimit=2;
                        break;
                case "3":upperLimit=5;
                        lowerLimit=3;
                        break;
                case "4":upperLimit=5;
                        lowerLimit=4;
                        break;
                default:upperLimit=5;
                        lowerLimit=1;
                        break; 
                        
            }
            
            let jobaggrQuery
            if  (roleName != "") {

                jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            'jobDetails.roleWiseAction.roleName': roleName, 
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },
                    {
                        $redact: {
                            $cond: {
                                if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction": "$jobDetails.roleWiseAction",

                        }
                    }
                ];

            } else {

                if (roleId != '') {

                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        }
                    ];
                      


                } else {

                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        }
                    ];
                    


                }
                

            }
            
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    if (searchJob.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    if (searchJob[0].roleWiseAction.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    


                    let empId=searchJob[0].employerId;

                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance

                    

                    let newSkillArrDb=[];
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    let isArrayEmpty;
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    }
                    let aggrQuery=[
                       {
                            $geoNear: {
                               near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                               distanceField: "dist.calculated",
                               maxDistance: parseInt(distance),
                               includeLocs: "dist.location",
                               spherical: true
                            }
                        },

                    { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:empId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        skills: 1,
                        invitationToApplyJobs:1,
                        jobOffers: 1,
                        favouriteBy: 1,
                        appliedJobs:1,
                        isExist:{
                            $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                        },

                    }},

                    { '$match': 
                            { 
                                'isExist': true,
                            }
                    },


                    { '$match': { $and: [ 
                            {'rating': {"$gte":lowerLimit}},
                            {'rating': {"$lte":upperLimit}},
                           
                        ]}
                    },
                    { "$unwind": "$skills" }, 
                    {
                        "$match": {  $and: [
                            isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                           

                        ]}
                    },
                    {
                        $lookup: {
                            from: "skills",
                            localField: "skills",
                            foreignField: "skillId",
                            as: "skillDetails"
                        }
                    },
                    { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:empId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        invitationToApplyJobs:1,
                        jobOffers: 1,
                        favouriteBy: 1,
                        appliedJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }

                     }},
                     { $project: {
                        jobId:1,
                        roleId:1,
                        candidateId: 1,
                       employerId:1,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        favouriteBy: 1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                        skillDetails: 1

                     }},
                    
                    
                    {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },
                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "candidateId": {
                                $first: "$candidateId"
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            // "employerId": {
                            //     $first: "$employerId"
                            // }, 
                            "skillDetails": {
                                $push: "$skillDetails"
                            }
                        }
                    },
                    { $project: 
                        { 
                            _id:0,
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            employerId:1,
                            skillDetails:1,
                            isFavourite : 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
            
                        }
                     },
                   
                    {
                        $group: {
                            _id: null,
                            total: {
                            $sum: 1
                            },
                            results: {
                            $push : '$$ROOT'
                            }
                        }
                    },
                    {
                        $project : {
                            '_id': 0,
                            'total': 1,
                            'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                            'results': {
                                $slice: [
                                    '$results', page * perPage , perPage
                                ]
                            }
                        }
                    }
                   

                    ]

                    //================
                    let aggrQuery1 = [
                       // {
                       //      $geoNear: {
                       //         near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                       //         distanceField: "dist.calculated",
                       //         maxDistance: parseInt(distance),
                       //         includeLocs: "dist.location",
                       //         spherical: true
                       //      }
                       //  },
                           { $project: {
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:empId,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                skills: 1,
                                invitationToApplyJobs:1,
                                jobOffers: 1,
                                favouriteBy: 1,
                                appliedJobs:1,
                                isExist:{
                                    $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                                },

                            }},

                            { '$match': 
                                    { 
                                        'isExist': true,
                                    }
                            },

                            { "$unwind": "$skills" }, 
                            {
                                "$match": {  $and: [
                                    isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                   

                                ]}
                            },
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            { $project: {
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:empId,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                invitationToApplyJobs:1,
                                jobOffers: 1,
                                favouriteBy: 1,
                                appliedJobs:1,
                                "skillDetails": {
                                    $arrayElemAt: ["$skillDetails", 0]
                                }

                             }},
                             { $project: {
                                jobId:1,
                                roleId:1,
                                candidateId: 1,
                               employerId:1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                favouriteBy: 1,
                                isFavourite:{
                                    $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                },
                                invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                                appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                                jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                                skillDetails: 1

                             }},
                    
                    
                            {
                                $group: {
                                    "_id": {
                                        "candidateId": "$candidateId",
                                    },
                                    "jobOffers": {
                                        $first: {
                                            $arrayElemAt: ["$jobOffers", 0]
                                        }
                                    },
                                    "appliedJobs": {
                                        $first: {
                                            $arrayElemAt: ["$appliedJobs", 0]
                                        }
                                    },
                                    "invitationToApplyJobs": {
                                        $first: {
                                            $arrayElemAt: ["$invitationToApplyJobs", 0]
                                        }
                                    },
                                    "profilePic": {
                                        $first: "$profilePic"
                                    },
                                    "employerId": {
                                        $first: "$employerId"
                                    },
                                    "candidateId": {
                                        $first: "$candidateId"
                                    },
                                   "fname": {
                                        $first: "$fname"
                                    },
                                    "mname": {
                                        $first: "$mname"
                                    },
                                    "lname": {
                                        $first: "$lname"
                                    },
                                    "isFavourite": {
                                        $first: "$isFavourite"
                                    },
                                    "geolocation": {
                                        $first: "$geolocation"
                                    },
                                    "rating": {
                                        $first: "$rating"
                                    },
                                    "jobId": {
                                        $first: "$jobId"
                                    },
                                    "roleId": {
                                        $first: "$roleId"
                                    },
                                    // "employerId": {
                                    //     $first: "$employerId"
                                    // }, 
                                    "skillDetails": {
                                        $push: "$skillDetails"
                                    }
                                }
                            },
                            
                            { $project: 
                                    { 
                                        _id:0,
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        profilePic:1,
                                        employerId:1,
                                        skillDetails:1,
                                        isFavourite : 1,
                                        isSentInvitation:{
                                            $cond: {
                                                "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                                "then": true,
                                                 "else": false
                                            }
                                        },
                                        isOffered:{
                                            $cond: {
                                                "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                                "then": true,
                                                 "else": false
                                            }
                                        }
                                       

                                    }
                                 },
                   

                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', page * perPage , perPage
                                            ]
                                        }
                                    }
                                }

                    ]
                    //=============================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 

                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                      //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================

                    // this function is used to fetch the hired details
                      async function fetchCandidatewithoutLocation() { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                
                                models.candidate.aggregate(aggrQuery1, function (err, response) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "response": response,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser


                    //=============


                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {

                        
                        
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        }
                        else{
                            //================
                            let data;

                            if (response.length < 1) {
                                
                                let fetchCandidatewithoutLocationStatus = await fetchCandidatewithoutLocation();
                               
                                data = fetchCandidatewithoutLocationStatus.response;

                                if (data.length < 1) {
                                    res.json({
                                        isError: false,
                                        message: "No Data Found",
                                        statuscode: 204,
                                        details: null
                                    }) 

                                    return;
                                }
                            } else {
                                data = response;
                            }
                         

                            if (data.length > 0) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details:{

                                       "TIER": data[0],
                                        // "TIER1":data[0],
                                        // "TIER2":{}
                                        // "TIER3":{},

                                    }
                                })
                            } 

                             
                            
                        } // END  else{
                    })
               }
            })
           
           
          
           
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //=============
    browseCandidateForOffer: (req, res, next) => {
        try{
            let upperLimit;
            let lowerLimit;
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let rating = req.body.rating ? req.body.rating :'';
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';
           
            // if (!req.body.jobId ||!req.body.roleId) { return; }
            if (!req.body.jobId) { return; }
            
            switch(rating){
                case "":upperLimit=5;
                        lowerLimit=1;
                        break;
                case "1":upperLimit=5;
                         lowerLimit=1;
                         break;
                case "2":upperLimit=5;
                        lowerLimit=2;
                        break;
                case "3":upperLimit=5;
                        lowerLimit=3;
                        break;
                case "4":upperLimit=5;
                        lowerLimit=4;
                        break;
                default:upperLimit=5;
                        lowerLimit=1;
                        break; 
                        
            }
            
            let jobaggrQuery
            if  (roleName != "") {

                jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            'jobDetails.roleWiseAction.roleName': roleName, 
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },
                    {
                        $redact: {
                            $cond: {
                                if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction": "$jobDetails.roleWiseAction",

                        }
                    }
                ];

            } else {

                if (roleId != '') {

                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        }
                    ];
                      


                } else {

                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        }
                    ];
                    


                }
                

            }
            
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    if (searchJob.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    if (searchJob[0].roleWiseAction.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    


                    let empId=searchJob[0].employerId;

                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance

                    

                    let newSkillArrDb=[];
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })

                    if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
                         skilldbbackup = newSkillArrDb
                    }

                    let isArrayEmpty;
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    }
                    let aggrQuery=[
                       {
                            $geoNear: {
                               near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                               distanceField: "dist.calculated",
                               maxDistance: parseInt(distance),
                               includeLocs: "dist.location",
                               spherical: true
                            }
                        },
                    { '$match': { $and: [ 
                            {'rating': {"$gte":lowerLimit}},
                            {'rating': {"$lte":upperLimit}},
                           
                        ]}
                    },
                    { "$unwind": "$skills" }, 
                    {
                        "$match": {  $and: [
                            isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                           

                        ]}
                    },
                    {
                        $lookup: {
                            from: "skills",
                            localField: "skills",
                            foreignField: "skillId",
                            as: "skillDetails"
                        }
                    },
                    { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:empId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        invitationToApplyJobs:1,
                        jobOffers: 1,
                        favouriteBy: 1,
                        appliedJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }

                     }},
                     { $project: {
                        jobId:1,
                        roleId:1,
                        candidateId: 1,
                       employerId:1,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        favouriteBy: 1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                        skillDetails: 1

                     }},
                    
                    
                    {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },
                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "candidateId": {
                                $first: "$candidateId"
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            // "employerId": {
                            //     $first: "$employerId"
                            // }, 
                            "skillDetails": {
                                $push: "$skillDetails"
                            }
                        }
                    },
                    { $project: 
                        { 
                            _id:0,
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            employerId:1,
                            skillDetails:1,
                            isFavourite : 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
                            // isOffered:{
                            //     $cond: {
                            //         "if": { "$eq": [ "$appliedJobs.jobId", jobId] }, 
                            //         "then": true,
                            //          "else": false
                            //     }
                            // }
                           

                        }
                     },
                    // if we want to avoid already offered candidates use this
                    //  { '$match':{
                    //      isOffered:false }},

                    {
                        $group: {
                            _id: null,
                            total: {
                            $sum: 1
                            },
                            results: {
                            $push : '$$ROOT'
                            }
                        }
                    },
                    {
                        $project : {
                            '_id': 0,
                            'total': 1,
                            'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                            'results': {
                                $slice: [
                                    '$results', page * perPage , perPage
                                ]
                            }
                        }
                    }
                    ]

                    //================
                    let aggrQuery1 = [
                       // {
                       //      $geoNear: {
                       //         near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                       //         distanceField: "dist.calculated",
                       //         maxDistance: parseInt(distance),
                       //         includeLocs: "dist.location",
                       //         spherical: true
                       //      }
                       //  },
                            { '$match': { $and: [ 
                                    {'rating': {"$gte":lowerLimit}},
                                    {'rating': {"$lte":upperLimit}},
                                   
                                ]}
                            },
                            { "$unwind": "$skills" }, 
                            {
                                "$match": {  $and: [
                                    isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                   

                                ]}
                            },
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            { $project: {
                                jobId:jobId,
                                roleId:roleId,
                                candidateId: 1,
                                employerId:empId,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                invitationToApplyJobs:1,
                                jobOffers: 1,
                                favouriteBy: 1,
                                appliedJobs:1,
                                "skillDetails": {
                                    $arrayElemAt: ["$skillDetails", 0]
                                }

                             }},
                             { $project: {
                                jobId:1,
                                roleId:1,
                                candidateId: 1,
                               employerId:1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                profilePic:1,
                                favouriteBy: 1,
                                isFavourite:{
                                    $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                },
                                invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                                appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                                jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                                skillDetails: 1

                             }},
                    
                    
                            {
                                $group: {
                                    "_id": {
                                        "candidateId": "$candidateId",
                                    },
                                    "jobOffers": {
                                        $first: {
                                            $arrayElemAt: ["$jobOffers", 0]
                                        }
                                    },
                                    "appliedJobs": {
                                        $first: {
                                            $arrayElemAt: ["$appliedJobs", 0]
                                        }
                                    },
                                    "invitationToApplyJobs": {
                                        $first: {
                                            $arrayElemAt: ["$invitationToApplyJobs", 0]
                                        }
                                    },
                                    "profilePic": {
                                        $first: "$profilePic"
                                    },
                                    "employerId": {
                                        $first: "$employerId"
                                    },
                                    "candidateId": {
                                        $first: "$candidateId"
                                    },
                                   "fname": {
                                        $first: "$fname"
                                    },
                                    "mname": {
                                        $first: "$mname"
                                    },
                                    "lname": {
                                        $first: "$lname"
                                    },
                                    "isFavourite": {
                                        $first: "$isFavourite"
                                    },
                                    "geolocation": {
                                        $first: "$geolocation"
                                    },
                                    "rating": {
                                        $first: "$rating"
                                    },
                                    "jobId": {
                                        $first: "$jobId"
                                    },
                                    "roleId": {
                                        $first: "$roleId"
                                    },
                                    // "employerId": {
                                    //     $first: "$employerId"
                                    // }, 
                                    "skillDetails": {
                                        $push: "$skillDetails"
                                    }
                                }
                            },
                            
                            { $project: 
                                    { 
                                        _id:0,
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        profilePic:1,
                                        employerId:1,
                                        skillDetails:1,
                                        isFavourite : 1,
                                        isSentInvitation:{
                                            $cond: {
                                                "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                                "then": true,
                                                 "else": false
                                            }
                                        },
                                        isOffered:{
                                            $cond: {
                                                "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                                "then": true,
                                                 "else": false
                                            }
                                        }
                                       

                                    }
                                 },
                   

                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', page * perPage , perPage
                                            ]
                                        }
                                    }
                                }
                    ]
                    //=============================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 

                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                      //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================

                    // this function is used to fetch the hired details
                      async function fetchCandidatewithoutLocation() { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                
                                models.candidate.aggregate(aggrQuery1, function (err, response) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "response": response,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser


                    //=============


                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        }
                        else{
                            //================
                            let data;

                            if (response.length < 1) {
                                // res.json({
                                //     isError: false,
                                //     message: "No Data Found",
                                //     statuscode: 204,
                                //     details: null
                                // })
                                let fetchCandidatewithoutLocationStatus = await fetchCandidatewithoutLocation();
                               
                                data = fetchCandidatewithoutLocationStatus.response;

                                if (data.length < 1) {
                                    res.json({
                                        isError: false,
                                        message: "No Data Found",
                                        statuscode: 204,
                                        details: null
                                    }) 

                                    return;
                                }
                            } else {
                                data = response;
                            }


                             /*
                            let k;
                            let keytoDelete = [];
                            
                            for (k = 0; k < data[0].results.length; k++) { // loop1
                                console.log('==>loop1');
                                let doDelete = 'NO';
                                //======================
                                // Here, we are seraching the details of hired jobs with respect to candidate
                                let fetchHiredDateStatus = await fetchHiredDate(data[0].results[k].candidateId);

                                if (fetchHiredDateStatus.isError == true) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });

                                    return;
                                }


                                let p;
                            
                                for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {
                                    console.log('==>loop2');

                                    let m;
                            
                                    for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
                                        console.log('==>loop3');
                                        let jobStartDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].startDate);
                                        let jobEndDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].endDate);
                                        
                                        let jobdate=new Date(jobStartDate).getDate();
                                        let jobmonth=new Date(jobStartDate).getMonth()+1;
                                        let jobyear=new Date(jobStartDate).getFullYear();

                                        let  jobIds = [];

                                        let i;
                                        for (i = 0; i < fetchHiredDateStatus.responseCandidate[0].hiredJobs.length; i++) {
                                            jobIds.push(fetchHiredDateStatus.responseCandidate[0].hiredJobs[i].jobId);
                                        }

                                        // Here, we are fetching set time 
                                        let fetchJobDetailStatus = await fetchJobDetail(jobIds);

                                        if (fetchJobDetailStatus.isError == true) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });

                                            return;
                                        }

                                        let j; 
                                        let startDate;
                                        let endDate;
                                        let startDateReadable

                                        for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                            console.log('==>loop4');
                                            startDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].startDate);
                                            endDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].endDate);
                                            //console.log(startDate + "=====" + endDate);

                                            if ((jobStartDate < startDate) && (startDate > jobEndDate)) {
                                                
                                                doDelete = 'YES';
                                                break;
                                            }

                                            if ((jobStartDate < endDate) && (endDate > jobEndDate)) {
                                               
                                                doDelete = 'YES';
                                                break;
                                            }

                                       
                                            let hiredate=new Date(startDate).getDate();
                                            let hiremonth=new Date(startDate).getMonth()+1;
                                            let hireyear=new Date(startDate).getFullYear();

                                            if ( (hiredate == jobdate) && (hiremonth == jobmonth) && (hireyear == jobyear) ) {
                                                doDelete = 'YES';
                                                break;
                                            }
                                          
                                                                            
                                        } // END for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {


                                    }// END for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {    

                                } // END for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {   
                            
                                if (doDelete == 'YES') {
                                    keytoDelete.push(k);    
                                }  

                            } // END for (k = 0; k < data[0].results.length; k++) { 

                            keytoDelete.sort();
                            keytoDelete.reverse();

                            let o;
                            for (o = 0; o < keytoDelete.length; o++) {
                                data[0].results.splice(keytoDelete[o], 1)
                            }
                           //================
                           */

                            if (data.length > 0) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details:{

                                       "TIER": data[0],
                                       "rrr":"rrr"
                                        // "TIER1":data[0],
                                        // "TIER2":{}
                                        // "TIER3":{},

                                    }
                                })
                            } 

                             
                            
                        } // END  else{
                    })
               }
            })
           
           
          
           
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    getRolesByJobId: async (req, res, next) => {
        let newRoleArrDb=[];
        try{
           
            let jobIds = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            // const routes = await getCalculatedRoutes(jobIds);
            // console.log(routes)
            var promisesArr = [];
            jobIds.map(jobId => {
                promisesArr.push(new Promise((resolve,reject) => {
                models.job.findOne({ 'jobId': jobId }, function (searchingerror, searchJob) {
                    if (searchingerror) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                        reject('error');
                    }
                    else {
                        // console.log(jobId, searchJob.jobDetails.roleWiseAction)
                        console.log('1111');
                        
                        // promisesArr.push(new Promise((resolve,reject) => {
                            console.log('222');
                            
                            searchJob.jobDetails.roleWiseAction.map(tt => {
                                console.log('333');
                                
                                newRoleArrDb.push({
                                    roleId: tt['roleId'],
                                    roleName: tt['roleName'],
                                    jobId: searchJob['jobId']
                                })
                                resolve(newRoleArrDb);
                            })
                        // }))
                            
                        // 
                    }
                })

}))
            })

            Promise.all(promisesArr).then(result => {
                // console.log('dddd',result);
                
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: result[result.length-1]
                  
                })
            })
           
            
           
           
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        } 
    },

    //======================================
    // here this function is written to fetch the skill on the basis of several ids
    getSkillsByManyJobRoleId: (req, res, next) =>{
        try{
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : [];
           
            if (!req.body.jobId ||!req.body.roleId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },

                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "employerId": 1,
                    
                        "industry": "$jobDetails.industry",
                        "roleWiseAction": "$jobDetails.roleWiseAction",
                    }

                },

                { $unwind: "$roleWiseAction" },

                {
                    $project: {
                        "_id": 0,
                        "roleId": "$roleWiseAction.roleId",
                        "roleName": "$roleWiseAction.roleName",
                        "skillIds": "$roleWiseAction.skills",
                    }

                },

                {
                    '$match': { 'roleId': { $in: roleId } }
                },

                { $unwind: "$skillIds" },

                {
                    $lookup: {
                        from: "skills",
                        localField: "skillIds",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project: {
                        "_id": 0,
                        "roleId":1,
                        "skillDetails": 1,
                    }

                },


                {
                    $project: {
                        "_id": 0,
                        "roleId":1,
                        "skillDetails": { $arrayElemAt: ["$skillDetails", 0] },
                    }

                },

            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {

              
                if (error) {

                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },



    //=====================================



    getSkillsByJobRoleId: (req, res, next) =>{
        try{
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
           
            if (!req.body.jobId ||!req.body.roleId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "jobDetails": { $arrayElemAt: ["$jobDetails.roleWiseAction", 0] },
                    }
                },
                { $unwind: "$jobDetails.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "roleId":"$jobDetails.roleId",
                        "skillDetails": { $arrayElemAt: ["$skillDetails", 0] },
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleId",
                        },
                        "skillDetails": {
                            $push: {
                                "skillName":"$skillDetails.skillName",
                                "skillId":"$skillDetails.skillId"
                            }
                        }
                    }
                },
                {
                    $project: {
                        "_id":0,
                        "skillDetails":1,
                    }
                },
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    aotOfferAll: (req, res, next) => {
        try{
            let offerData = req.body.offerData ? req.body.offerData : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Offer data",
                isError: true
            });
            // console.log('.......',offerData)
          offerData.map(async tt => {
              // console.log(tt['jobId'])
              // console.log(tt['employerId'])
              // console.log(tt['candidateId'])
              // console.log(tt['roleId'])
              // let updateWith={
              //   'roleId':tt['jobId'],
              //   'candidateId':tt['candidateId']
              //   }

             let updateWith={
                'roleId':tt['roleId'],
                'candidateId':tt['candidateId']
                }   
           
            // models.job.updateOne({ 'employerId': tt['employerId'],'jobId':tt['jobId'] },{$push: {'approvedTo':updateWith,'appiliedFrom':updateWith}},
            models.job.updateOne({ 'employerId': tt['employerId'],'jobId':tt['jobId'] },{$push: {'approvedTo':updateWith}},  
                await function (error, response) {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    let insertedValue={
                        "roleId":tt['roleId'],"employerId":tt['employerId'],
                        "jobId":tt['jobId'],"candidateId":tt['candidateId']
                    }
                    // models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, { $push: { "jobOffers":insertedValue,"appliedJobs":insertedValue } },
                    models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, { $push: { "jobOffers":insertedValue } },  
                        function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        if (updatedresponse.nModified == 1) {
                            models.authenticate.findOne({'userId':tt['candidateId']}, async function (err, item) {
                                if(item['mobileNo']==""){
                                    var result = await  sendNotificationEmail(item['EmailId'],tt['candidateId'],tt['jobId'],tt['roleId'])
                                    if(result){
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                }
                                else{
                                    var result = await  sendNotificationSms(item['mobileNo'],tt['candidateId'],tt['jobId'],tt['roleId'])
                                    if(result){
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                }
                            })
                        }
                        else{
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            }) 
                        }
                    })
                }
            })
          })
            

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    aotChooseWorker: (req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';

            let roleName = req.body.roleName ? req.body.roleName : '';
           
            if (!req.body.jobId) { return; }

            let jobaggrQuery

            if (roleName != "") {

           
                jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                'jobDetails.roleWiseAction.roleName': roleName 
                            }
                        },
                      
                        {
                            $project: {
                                "_id":0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        { $redact : {
                            $cond: {
                                if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                       }},
                       {
                        $project: {
                            "_id":0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction":"$jobDetails.roleWiseAction",
                            
                        }
                    }
                ]
            } else {
                if (roleId != '') {
                                jobaggrQuery = [
                                    {
                                        '$match': { 
                                            'jobId': jobId,
                                            
                                        }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "employerId": 1,
                                            "jobDetails": 1,
                                        }
                                    },

                                    { $redact : {
                                        $cond: {
                                            if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                                            then: "$$DESCEND",
                                            else: "$$PRUNE"
                                        }
                                   }},
                                   
                                   {
                                    $project: {
                                        "_id":0,
                                        "jobId": 1,
                                        "employerId": 1,
                                        "roleWiseAction":"$jobDetails.roleWiseAction",
                                        
                                    }
                                }
                            ]

                } else {
                            jobaggrQuery = [
                                {
                                    '$match': { 
                                        'jobId': jobId,
                                        
                                    }
                                },
                              
                                {
                                    $project: {
                                        "_id":0,
                                        "jobId": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                    }
                                },
                               
                               {
                                $project: {
                                    "_id":0,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "roleWiseAction":"$jobDetails.roleWiseAction",
                                    
                                }
                            }
                        ]
                }
                

            }    
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {
                
                
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                if (searchJob.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                if (searchJob[0].roleWiseAction.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                let newSkillArrDb=[];
                searchJob[0].roleWiseAction.map(async tt => {
                    await tt['skills'].map(item => {
                        newSkillArrDb.push(item)
                    })
                })


                let employerId=searchJob[0].employerId
                let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                let distance=searchJob[0].roleWiseAction[0].distance

                 
                let newaggrQuery=[                        
                        {
                            $geoNear: {
                               near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                               distanceField: "dist.calculated",
                               maxDistance: parseInt(distance),
                               includeLocs: "dist.location",
                               spherical: true
                            }
                        },
                        { "$match": { "rating": { "$gte": 4 } } } ,
                        { "$unwind": "$skills" },
                        {
                            "$match": {
                                "skills": { "$in": newSkillArrDb },

                            }
                        },
                        
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },

                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            appliedJobs:1,
                            jobOffers:1,
                            invitationToApplyJobs:1,
                            favouriteBy: 1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }

                         }},

                         { $project: {
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy: 1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            //jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] }] },
                            //jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $eq: ["$$jobOffers.jobId", "$jobId"] } } },
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            skillDetails: 1

                         }},
                        
         
                        {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },

                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },
                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                }, 
                                "profilePic": {
                                    $first: "$profilePic"
                                }, 
                                // "favouriteBy": 1,
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:1,
                                roleId:1,
                                candidateId: 1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                employerId:1,
                                profilePic:1,
                                skillDetails:1,
                                isFavourite:1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },

                                // isOffered:{
                                //     $cond: {
                                //         "if": { "$eq": [ "$appliedJobs.jobId", jobId] }, 
                                //         "then": true,
                                //          "else": false
                                //     }
                                // },

                            }
                         },
                        // if we want to avoid already offered candidates use this
                        //  { '$match':{
                        //      isOffered:false }},

                        {
                            $group: {
                                _id: null,
                                total: {
                                $sum: 1
                                },
                                results: {
                                $push : '$$ROOT'
                                }
                            }
                        },
                        {
                            $project : {
                                '_id': 0,
                                'total': 1,
                                'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                'results': {
                                    $slice: [
                                        '$results', page * perPage , perPage
                                    ]
                                }
                            }
                        }
                        
                       
                    ]

                    //====================================
                    let newaggrQueryforAllCandidate = [  

                        //  {
                        //     $geoNear: {
                        //        near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                        //        distanceField: "dist.calculated",
                        //        maxDistance: parseInt(distance),
                        //        includeLocs: "dist.location",
                        //        spherical: true
                        //     }
                        // },
                        { "$match": { "rating": { "$gte": 4 } } } ,
                        { "$unwind": "$skills" },
                        {
                            "$match": {
                                "skills": { "$in": newSkillArrDb },

                            }
                        },
                        
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },

                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            appliedJobs:1,
                            jobOffers:1,
                            invitationToApplyJobs:1,
                            favouriteBy: 1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }

                         }},

                         { $project: {
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy: 1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            //jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] }] },
                            //jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $eq: ["$$jobOffers.jobId", "$jobId"] } } },
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            skillDetails: 1

                         }},
                        
         
                        {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },

                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },
                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                }, 
                                "profilePic": {
                                    $first: "$profilePic"
                                }, 
                                // "favouriteBy": 1,
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:1,
                                roleId:1,
                                candidateId: 1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                employerId:1,
                                profilePic:1,
                                skillDetails:1,
                                isFavourite:1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },

                                // isOffered:{
                                //     $cond: {
                                //         "if": { "$eq": [ "$appliedJobs.jobId", jobId] }, 
                                //         "then": true,
                                //          "else": false
                                //     }
                                // },

                            }
                         },
                        // if we want to avoid already offered candidates use this
                        //  { '$match':{
                        //      isOffered:false }},

                        {
                            $group: {
                                _id: null,
                                total: {
                                $sum: 1
                                },
                                results: {
                                $push : '$$ROOT'
                                }
                            }
                        },
                        {
                            $project : {
                                '_id': 0,
                                'total': 1,
                                'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                'results': {
                                    $slice: [
                                        '$results', page * perPage , perPage
                                    ]
                                }
                            }
                        }                      
                        
                        
                       
                    ]
                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================


                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                     //=======================================================================
                    // this function is used to fetch all candidate irrespective of location
                      async function fetchAllCandidateDetail() { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                              console.log("kkkk333");
                                


                                models.candidate.aggregate(newaggrQueryforAllCandidate, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser  

                    //================
                    models.candidate.aggregate(newaggrQuery).exec( async (error, response) => {
                       
                        
                        
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else{


                            data = response;
                            

                            if (response.length < 1) {
                                
                                
   
                                let fetchAllCandidateDetailStatus = await fetchAllCandidateDetail();
                                 
                                let data = fetchAllCandidateDetailStatus.responseJob;

                                if (data.length < 1) {
                                    res.json({
                                        isError: true,
                                        message: 'Data Not Found',
                                        statuscode: 204,
                                        details: null
                                    });
                                    return;
                                
                                }  // if (response.length < 1) { 
                                
                                

                                if (data[0].results.length < 1) {
                                    res.json({
                                        isError: true,
                                        message: 'Data Not Found',
                                        statuscode: 204,
                                        details: null
                                    });
                                    return;
                                
                                }  // if (response.length < 1) { 

                                 // ==============================
                                let offerJobStatus;
                                let tier1Data = data[0].results;
                                let tier1DataTag;
                                let sendNotificationEmailStatus;
                                let sendNotificationSmsStatus;



                                for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    
                                    if (tier1Data[tier1DataTag].isOffered == false) {                        
                                        offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                        

                                        if ( offerJobStatus.isError == false) {
                                            
                                            
                                            findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                            

                                            if (findMailStatus.isError == false) {
                                                tier1Data[tier1DataTag].isOffered = true;
                                                if (findMailStatus.isItemExist == true) {
                                                        
                                                        if(findMailStatus.item.mobileNo == ""){
                                                            sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                            
                                                        }
                                                        else{
                                                            sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                            
                                                        }

                                                } // END if (findMailStatus.isItemExist == true) {
                                            } // END if (findMailStatus.isError == false) {
                                        } // END if ( offerJobStatus.isError == false) {

                                        
                                    }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                data[0].results = tier1Data;

                                // ================================     



                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details:{
                                         // "TIER":data[0],
                                        "TIER1":data[0],
                                        // "TIER2":{},
                                        // "TIER3":{},                                       
                                    }
                                })

                                return;
                             

                            }  // if (response.length < 1) {



                            
                            /*
                            let k;
                            let keytoDelete = [];
                            
                            for (k = 0; k < data[0].results.length; k++) { // loop1
                                
                                let doDelete = 'NO';
                                //======================
                                // Here, we are seraching the details of hired jobs with respect to candidate
                                let fetchHiredDateStatus = await fetchHiredDate(data[0].results[k].candidateId);

                                if (fetchHiredDateStatus.isError == true) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });

                                    return;
                                }

                                let p;
                            
                                for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {


                                        let m;
                                    
                                        for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
                                            
                                            let jobStartDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].startDate);
                                            let jobEndDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].endDate);
                                            
                                            let jobdate=new Date(jobStartDate).getDate();
                                            let jobmonth=new Date(jobStartDate).getMonth()+1;
                                            let jobyear=new Date(jobStartDate).getFullYear();

                                            let  jobIds = [];

                                            let i;
                                            for (i = 0; i < fetchHiredDateStatus.responseCandidate[0].hiredJobs.length; i++) {
                                                jobIds.push(fetchHiredDateStatus.responseCandidate[0].hiredJobs[i].jobId);
                                            }

                                            // Here, we are fetching set time 
                                            let fetchJobDetailStatus = await fetchJobDetail(jobIds);

                                            if (fetchJobDetailStatus.isError == true) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });

                                                return;
                                            }


                                            let j; 
                                            let startDate;
                                            let endDate;
                                            let startDateReadable

                                            for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                                
                                                startDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].startDate);
                                                endDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].endDate);
                                                //console.log(startDate + "=====" + endDate);

                                                if ((jobStartDate < startDate) && (startDate > jobEndDate)) {
                                                    
                                                    doDelete = 'YES';
                                                    break;
                                                }

                                                if ((jobStartDate < endDate) && (endDate > jobEndDate)) {
                                                   
                                                    doDelete = 'YES';
                                                    break;
                                                }

                                           
                                                let hiredate=new Date(startDate).getDate();
                                                let hiremonth=new Date(startDate).getMonth()+1;
                                                let hireyear=new Date(startDate).getFullYear();

                                                if ( (hiredate == jobdate) && (hiremonth == jobmonth) && (hireyear == jobyear) ) {
                                                    doDelete = 'YES';
                                                    break;
                                                }
                                              
                                                                                
                                            } // END for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
                                                if (doDelete == 'YES') {
                                                    break;   
                                                } 

                                               
                                           
                                        } // END for (m = 0; m < searchJob[0].roleWiseAction[0].setTime.length; m++) { 
                                            if (doDelete == 'YES') {
                                                    break;   
                                                } 

                                } // END for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {
                                
                                if (doDelete == 'YES') {
                                    keytoDelete.push(k);
     
                                }                  
                               
                             
                            } // END for (k = 0; k < data[0].results.length; k++) {
                           
                            keytoDelete.sort();
                            keytoDelete.reverse();

                            let o;
                            for (o = 0; o < keytoDelete.length; o++) {
                                data[0].results.splice(keytoDelete[o], 1)
                            }
                          
                            */
                            if (data.length > 0) {
                                if (data[0].results.length < 1) {

                                    res.json({
                                        isError: false,
                                        message: "No Data Found",
                                        statuscode: 204,
                                        details:null
                                    })

                                    return;
                                }

                            // ==============================
                            let offerJobStatus;
                            let tier1Data = data[0].results;
                            let tier1DataTag;
                            let sendNotificationEmailStatus;
                            let sendNotificationSmsStatus;



                            for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                
                                if (tier1Data[tier1DataTag].isOffered == false) {                        
                                    offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                    

                                    if ( offerJobStatus.isError == false) {
                                        
                                        
                                        findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                        

                                        if (findMailStatus.isError == false) {
                                            tier1Data[tier1DataTag].isOffered = true;
                                            if (findMailStatus.isItemExist == true) {
                                                    
                                                    if(findMailStatus.item.mobileNo == ""){
                                                        sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                        
                                                    }
                                                    else{
                                                        sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                        
                                                    }

                                            } // END if (findMailStatus.isItemExist == true) {
                                        } // END if (findMailStatus.isError == false) {
                                    } // END if ( offerJobStatus.isError == false) {

                                    
                                }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                            } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                            data[0].results = tier1Data;

                            // ================================    

                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details:{
                                         // "TIER":data[0],
                                        "TIER1":data[0],
                                        // "TIER2":{},
                                        // "TIER3":{},

                                        // "keytoDelete": keytoDelete
                                    }
                                })

                                return;
                            }

                            if (data.length < 1) {

                                res.json({
                                    isError: false,
                                    message: "No Data Found",
                                    statuscode: 204,
                                    details:null
                                })

                                return;
                            }

                            
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //=====================
    /*
       This function is written to fetch all the candidate on the basis of and
    */
    aotChooseWorkerAnd: (req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';

            let roleName = req.body.roleName ? req.body.roleName : '';

            let skillArrb = [];

            skillArrb = req.body.skillArrb ? req.body.skillArrb : [];
           
            if (!req.body.jobId) { return; }
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }

            let jobaggrQuery

            if (roleName != "") {

           
                jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                'jobDetails.roleWiseAction.roleName': roleName 
                            }
                        },
                      
                        {
                            $project: {
                                "_id":0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        { $redact : {
                            $cond: {
                                if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                       }},
                       {
                        $project: {
                            "_id":0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction":"$jobDetails.roleWiseAction",
                            
                        }
                    }
                ]
            } else {
                if (roleId != '') {
                                jobaggrQuery = [
                                    {
                                        '$match': { 
                                            'jobId': jobId,
                                            
                                        }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "employerId": 1,
                                            "jobDetails": 1,
                                        }
                                    },

                                    { $redact : {
                                        $cond: {
                                            if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                                            then: "$$DESCEND",
                                            else: "$$PRUNE"
                                        }
                                   }},
                                   
                                   {
                                    $project: {
                                        "_id":0,
                                        "jobId": 1,
                                        "employerId": 1,
                                        "roleWiseAction":"$jobDetails.roleWiseAction",
                                        
                                    }
                                }
                            ]

                } else {
                            jobaggrQuery = [
                                {
                                    '$match': { 
                                        'jobId': jobId,
                                        
                                    }
                                },
                              
                                {
                                    $project: {
                                        "_id":0,
                                        "jobId": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                    }
                                },
                               
                               {
                                $project: {
                                    "_id":0,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "roleWiseAction":"$jobDetails.roleWiseAction",
                                    
                                }
                            }
                        ]
                }
                

            }    
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {
                
                
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                if (searchJob.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                if (searchJob[0].roleWiseAction.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                let newSkillArrDb=[];
                searchJob[0].roleWiseAction.map(async tt => {
                    await tt['skills'].map(item => {
                        newSkillArrDb.push(item)
                    })
                })


                let employerId=searchJob[0].employerId
                let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                let distance=searchJob[0].roleWiseAction[0].distance

                 
                let newaggrQuery=[                        
                        {
                            $geoNear: {
                               near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                               distanceField: "dist.calculated",
                               maxDistance: parseInt(distance),
                               includeLocs: "dist.location",
                               spherical: true
                            }
                        },

                        { $project: {
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            skills:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            appliedJobs:1,
                            jobOffers:1,
                            invitationToApplyJobs:1,
                            favouriteBy: 1,
                            isExist:{
                                $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                            },
                            

                         }},

   
                   
                        { 
                            "$match": { 
                                "rating": { "$gte": 4 },
                                "isExist": true 
                            } 
                        },

                        { "$unwind": "$skills" },
                        {
                            "$match": {
                                "skills": { "$in": newSkillArrDb },


                            }
                        },
                        
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },

                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            appliedJobs:1,
                            jobOffers:1,
                            invitationToApplyJobs:1,
                            favouriteBy: 1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }

                         }},

                         { $project: {
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy: 1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            skillDetails: 1

                         }},
                        
         
                        {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },

                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },
                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                }, 
                                "profilePic": {
                                    $first: "$profilePic"
                                }, 
                                // "favouriteBy": 1,
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:1,
                                roleId:1,
                                candidateId: 1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                employerId:1,
                                profilePic:1,
                                skillDetails:1,
                                isFavourite:1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },

                               

                            }
                         },
                       

                        {
                            $group: {
                                _id: null,
                                total: {
                                $sum: 1
                                },
                                results: {
                                $push : '$$ROOT'
                                }
                            }
                        },
                        {
                            $project : {
                                '_id': 0,
                                'total': 1,
                                'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                'results': {
                                    $slice: [
                                        '$results', page * perPage , perPage
                                    ]
                                }
                            }
                        }
                        
                       
                    ]

                    //====================================
                    let newaggrQueryforAllCandidate = [  

                        { $project: {
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            skills:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            appliedJobs:1,
                            jobOffers:1,
                            invitationToApplyJobs:1,
                            favouriteBy: 1,
                            isExist:{
                                $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                            },
                            

                         }},

   
                   
                        { 
                            "$match": { 
                                "rating": { "$gte": 4 },
                                "isExist": true 
                            } 
                        },

                    
                        { "$unwind": "$skills" },
                        {
                            "$match": {
                                "skills": { "$in": newSkillArrDb },

                            }
                        },
                        
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },

                        { $project: {
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            appliedJobs:1,
                            jobOffers:1,
                            invitationToApplyJobs:1,
                            favouriteBy: 1,
                            "skillDetails": {
                                $arrayElemAt: ["$skillDetails", 0]
                            }

                         }},

                         { $project: {
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            employerId:employerId,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            profilePic:1,
                            geolocation:1,
                            rating:1,
                            favouriteBy: 1,
                            isFavourite:{
                                $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                            },
                            
                            invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                            
                            jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                            appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                            skillDetails: 1

                         }},
                        
         
                        {
                            $group: {
                                "_id": {
                                    "candidateId": "$candidateId",
                                },

                                "jobOffers": {
                                    $first: {
                                        $arrayElemAt: ["$jobOffers", 0]
                                    }
                                },
                                "appliedJobs": {
                                    $first: {
                                        $arrayElemAt: ["$appliedJobs", 0]
                                    }
                                },
                                "invitationToApplyJobs": {
                                    $first: {
                                        $arrayElemAt: ["$invitationToApplyJobs", 0]
                                    }
                                },
                                "candidateId": {
                                    $first: "$candidateId"
                                },
                               "fname": {
                                    $first: "$fname"
                                },
                                "mname": {
                                    $first: "$mname"
                                },
                                "lname": {
                                    $first: "$lname"
                                },
                                "isFavourite": {
                                    $first: "$isFavourite"
                                },
                                "geolocation": {
                                    $first: "$geolocation"
                                },
                                "rating": {
                                    $first: "$rating"
                                },
                                "jobId": {
                                    $first: "$jobId"
                                },
                                "roleId": {
                                    $first: "$roleId"
                                },
                                "employerId": {
                                    $first: "$employerId"
                                }, 
                                "profilePic": {
                                    $first: "$profilePic"
                                }, 
                                
                                "skillDetails": {
                                    $push: "$skillDetails"
                                }
                            }
                        },
                        { $project: 
                            { 
                                _id:0,
                                jobId:1,
                                roleId:1,
                                candidateId: 1,
                                fname: 1,
                                mname: 1,
                                lname:1,
                                geolocation:1,
                                rating:1,
                                employerId:1,
                                profilePic:1,
                                skillDetails:1,
                                isFavourite:1,
                                isSentInvitation:{
                                    $cond: {
                                        "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },
                                isOffered:{
                                    $cond: {
                                        "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                        "then": true,
                                         "else": false
                                    }
                                },

                                

                            }
                         },
                        

                        {
                            $group: {
                                _id: null,
                                total: {
                                $sum: 1
                                },
                                results: {
                                $push : '$$ROOT'
                                }
                            }
                        },
                        {
                            $project : {
                                '_id': 0,
                                'total': 1,
                                'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                'results': {
                                    $slice: [
                                        '$results', page * perPage , perPage
                                    ]
                                }
                            }
                        }                      
                        
                        
                       
                    ]
                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================


                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                     //=======================================================================
                    // this function is used to fetch all candidate irrespective of location
                      async function fetchAllCandidateDetail() { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                              console.log("kkkk333");
                                


                                models.candidate.aggregate(newaggrQueryforAllCandidate, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser  

                    //================
                    models.candidate.aggregate(newaggrQuery).exec( async (error, response) => {
                        
                        
                        
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else{


                            data = response;
                            

                            if (response.length < 1) {
                                
                                
   
                                let fetchAllCandidateDetailStatus = await fetchAllCandidateDetail();
                                 
                                let data = fetchAllCandidateDetailStatus.responseJob;


                                if (data.length < 1) {
                                    res.json({
                                        isError: true,
                                        message: 'Data Not Found',
                                        statuscode: 204,
                                        details: null
                                    });
                                    return;
                                
                                }  // if (response.length < 1) { 
                                
                                

                                if (data[0].results.length < 1) {
                                    res.json({
                                        isError: true,
                                        message: 'Data Not Found',
                                        statuscode: 204,
                                        details: null
                                    });
                                    return;
                                
                                }  // if (response.length < 1) { 

                                // ==============================
                                
                                let offerJobStatus;
                                let tier1Data = data[0].results;
                                let tier1DataTag;
                                let sendNotificationEmailStatus;
                                let sendNotificationSmsStatus;
                                

                                for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    
                                    if (tier1Data[tier1DataTag].isOffered == false) {                        
                                        offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                        

                                        if ( offerJobStatus.isError == false) {
                                            
                                            
                                            findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                            

                                            if (findMailStatus.isError == false) {
                                                tier1Data[tier1DataTag].isOffered = true;
                                                if (findMailStatus.isItemExist == true) {
                                                        
                                                        if(findMailStatus.item.mobileNo == ""){
                                                            sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                            
                                                        }
                                                        else{
                                                            sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                            
                                                        }

                                                } // END if (findMailStatus.isItemExist == true) {
                                            } // END if (findMailStatus.isError == false) {
                                        } // END if ( offerJobStatus.isError == false) {

                                        
                                    }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                data[0].results = tier1Data;

                                // ================================      

                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details:{
                                         // "TIER":data[0],
                                        "TIER1":data[0],
                                        // "TIER2":{},
                                        // "TIER3":{},                                       
                                    }
                                })

                                return;
                             

                            }  // if (response.length < 1) {



                            
                           
                            if (data.length > 0) {
                                if (data[0].results.length < 1) {

                                    res.json({
                                        isError: false,
                                        message: "No Data Found",
                                        statuscode: 204,
                                        details:null
                                    })

                                    return;
                                }

                                // ==============================
                                
                                let offerJobStatus;
                                let tier1Data = data[0].results;
                                let tier1DataTag;
                                let sendNotificationEmailStatus;
                                let sendNotificationSmsStatus;
                                

                                for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    
                                    if (tier1Data[tier1DataTag].isOffered == false) {                        
                                        offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                        

                                        if ( offerJobStatus.isError == false) {
                                            
                                            
                                            findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                            

                                            if (findMailStatus.isError == false) {
                                                tier1Data[tier1DataTag].isOffered = true;
                                                if (findMailStatus.isItemExist == true) {
                                                        
                                                        if(findMailStatus.item.mobileNo == ""){
                                                            sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                            
                                                        }
                                                        else{
                                                            sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                            
                                                        }

                                                } // END if (findMailStatus.isItemExist == true) {
                                            } // END if (findMailStatus.isError == false) {
                                        } // END if ( offerJobStatus.isError == false) {

                                        
                                    }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                data[0].results = tier1Data;

                                // ================================  

                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details:{
                                         // "TIER":data[0],
                                        "TIER1":data[0],
                                        // "TIER2":{},
                                        // "TIER3":{},

                                    }
                                })

                                return;
                            }

                            if (data.length < 1) {

                                res.json({
                                    isError: false,
                                    message: "No Data Found",
                                    statuscode: 204,
                                    details:null
                                })

                                return;
                            }

                            
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //==========================

    markUnFavourite:(req, res, next) => {
        try{
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - candidateId"
            }); 
            if(!req.body.employerId || !req.body.candidateId) { return; } 
            models.candidate.updateOne({"candidateId":candidateId}, { $pull: { "favouriteBy":employerId } }, function (pushederror, pushedresponse) {
                if(pushederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    }) 
                }
                else{
                    if (pushedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }

            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    markFavourite:(req, res, next) => {
        try{
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - candidateId"
            }); 
            if(!req.body.employerId || !req.body.candidateId) { return; } 
            models.candidate.find({"candidateId":candidateId,"favouriteBy": { $in: [employerId] } },  function (searchingerror, searchRating) {
               if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                   if(searchRating.length==0){
                        models.candidate.updateOne({"candidateId":candidateId}, { $push: { "favouriteBy":employerId } }, function (pushederror, pushedresponse) {
                            if(pushederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                }) 
                            }
                            else{
                                if (pushedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }

                        })
                   }
                   else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1032'],
                            statuscode: 1032,
                            details: null
                        })
                   }
               }

            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    getEmployedCandidateByName:(req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let serachText = req.body.searchText;
            if (!req.body.employerId) { return; } 
            let aggrQuery = [
                {
                    '$match': { 'employerId': employerId },
                    
                },
                { $unwind: "$hireList" },
                { $project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industry:"$jobDetails.industry",
                    jobDb:"$jobDetails"
                 }},
                 {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection: {$filter: {
                        input: '$jobDb.roleWiseAction',
                        as: 'jobCollection',
                        cond: {$eq: ['$$jobCollection.roleId', '$hireList.roleId']}
                    }},
                    _id: 0
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection:{ $arrayElemAt :["$jobCollection", 0]}
                }},
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                    }
                },
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    roleName:"$jobCollection.roleName",
                    paymentStatus:"$jobCollection.paymentStatus",
                    paymentStatus:"$jobCollection.paymentStatus",
                    description:"$jobCollection.description",
                    candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    roleName:1,
                    paymentStatus:1,
                    paymentStatus:1,
                    description:1,
                    fname: "$candidateDetails.fname",
                    mname: "$candidateDetails.mname",
                    lname: "$candidateDetails.lname",
                    profilePic: "$candidateDetails.profilePic",
                    isFavourite:{
                        $cond:[{$setIsSubset: [["$employerId"], "$candidateDetails.favouriteBy"]},true,false]
                    }
                    
                }},
                {
                    '$match': { 'roleName': { "$regex": serachText, "$options": "i" } }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    //==================

   

    //==================
    /*
        This function is written to fetch all the candidate for a particular employee
    */
     getAllCandidateForEmp:(req, res, next) => {

        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            let roleId = req.body.roleId ? req.body.roleId : '';

            let skillId = req.body.payloadSkill ? req.body.payloadSkill : '';

        

            if (!req.body.employerId) { return; } 

            let aggrQuery;

            if (roleId != '') {

                if (skillId != '') {
                    console.log("skillId==>"+skillId);
                    aggrQuery = [
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },

                            {
                                $project:{
                                    _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    isSkilled:{
                                        $cond: {
                                            "if": { "$eq": [ "$payloadSkill.jobRoleId", roleId] }, 
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                
                                },                    
                            },

                            {
                              $match: {
                                'selectedRole': { $in: roleId },
                                'skills': { $in: skillId }
                              }
                            },


                       
                            {
                                $group: {
                                    _id: null,
                                    total: {
                                    $sum: 1
                                    },
                                    results: {
                                    $push : '$$ROOT'
                                    }
                                }
                            },
                            {
                                $project : {
                                    '_id': 0,
                                    'total': 1,
                                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                    'results': {
                                        $slice: [
                                            '$results', page * perPage , perPage
                                        ]
                                    }
                                }
                            }

                        ]

                } else {

                    aggrQuery = [

                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },

                            {
                                $project:{
                                    _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    isSkilled:{
                                        $cond: {
                                            "if": { "$eq": [ "$payloadSkill.jobRoleId", roleId] }, 
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                
                                },                    
                            },

                            {
                              $match: {
                                'selectedRole': { $in: roleId },
                                
                              }
                            },


                       
                            {
                                $group: {
                                    _id: null,
                                    total: {
                                    $sum: 1
                                    },
                                    results: {
                                    $push : '$$ROOT'
                                    }
                                }
                            },
                            {
                                $project : {
                                    '_id': 0,
                                    'total': 1,
                                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                    'results': {
                                        $slice: [
                                            '$results', page * perPage , perPage
                                        ]
                                    }
                                }
                            }

                        ]

                }

            } else {

                    aggrQuery = [
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },

                        {
                            $project:{
                                 _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                            
                            },                    
                        },
                       
                        {
                            $group: {
                                _id: null,
                                total: {
                                $sum: 1
                                },
                                results: {
                                $push : '$$ROOT'
                                }
                            }
                        },
                        {
                            $project : {
                                '_id': 0,
                                'total': 1,
                                'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                'results': {
                                    $slice: [
                                        '$results', page * perPage , perPage
                                    ]
                                }
                            }
                        }

                    ]

            }

            models.candidate.aggregate(aggrQuery).exec((error, data) => {
   
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (data.length == 0) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        }) 

                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: data[0]
                        }) 
                    }

                    
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    

    },    


    //==================
     getFavouriteCandidateForEmp:(req, res, next) => {

        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            if (!req.body.employerId) { return; } 

            let aggrQuery = [
                {
                    $lookup: {
                        from: "skills",
                        localField: "skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project:{
                        _id:0,
                        favouriteBy:1,
                        candidateId:1,
                        workExperience:1,
                        geolocation: 1,
                        skills:1,
                        fname:1,
                        mname:1,
                        lname:1,
                        EmailId:1,
                        rating:1,
                        profilePic:1,
                        selectedRole:1,
                        skills: 1,
                        payloadSkill: 1,
                        skillDetails: 1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                    
                    },                    
                },
                {
                    '$match': { 'isFavourite': true },
                    
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }


            ]
            models.candidate.aggregate(aggrQuery).exec((error, data) => {
   
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (data.length == 0) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        }) 

                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: data[0]
                        }) 
                    }

                    
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    

    },    


    //=====================
    getCandidateToMarkFavourite:(req, res, next) => {

        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            if (!req.body.employerId) { return; } 

            let aggrQuery = [
                {
                    $project:{
                        _id:0,
                        favouriteBy:1,
                        candidateId:1,
                        fname:1,
                        mname:1,
                        lname:1,
                        EmailId:1,
                        rating:1,
                        profilePic:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        }
                    
                    },
                    
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }


            ]
            models.candidate.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    

    },    

    //====================
    getEmployedCandidate:(req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

           
            if (!req.body.employerId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': employerId },
                    
                },
                { $unwind: "$hireList" },
                { $project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industry:"$jobDetails.industry",
                    jobDb:"$jobDetails"
                 }},
                 {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection: {$filter: {
                        input: '$jobDb.roleWiseAction',
                        as: 'jobCollection',
                        cond: {$eq: ['$$jobCollection.roleId', '$hireList.roleId']}
                    }},
                    _id: 0
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection:{ $arrayElemAt :["$jobCollection", 0]}
                }},
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                    }
                },
            
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    
                    roleName:"$jobCollection.roleName",
                    paymentStatus:"$jobCollection.paymentStatus",
                    paymentStatus:"$jobCollection.paymentStatus",
                    description:"$jobCollection.description",
                    candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    roleName:1,
                    paymentStatus:1,
                    paymentStatus:1,
                    description:1,
                    geolocation:"$candidateDetails.geolocation",
                    fname: "$candidateDetails.fname",
                    mname: "$candidateDetails.mname",
                    lname: "$candidateDetails.lname",
                    profilePic: "$candidateDetails.profilePic",
                   
                    isFavourite:{
                        $cond:[{$setIsSubset: [["$employerId"], "$candidateDetails.favouriteBy"]},true,false]
                    }
                    
                }},
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
                

            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {

                
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    giveRating:(req, res, next) => {
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - candidateId"
            });
            let review = req.body.review ? req.body.review : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - review"
            });
            let rating = req.body.rating ? req.body.rating : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - rating"
            });
            if (!req.body.roleId ||!req.body.jobId ||!req.body.employerId ||!req.body.candidateId ||!req.body.rating ||!req.body.review ) { return; }
           
            models.rating.findOne({'roleId':roleId,'jobId':jobId,'employerId':employerId,"candidateId":candidateId},  function (searchingerror, searchRating) {
                if(searchingerror){
                    console.log('search error',searchingerror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if(searchRating==null){
                        console.log("no record found ,Its time to insert")
                        // no record found ,Its time to insert
                        let insertedvalue={
                            '_id': new mongoose.Types.ObjectId(),
                            "roleId":roleId,
                            "jobId":jobId,
                            "employerId":employerId,
                            "candidateId":candidateId,
                            "review":review,
                            "rating":rating
                        }
                        models.rating.create(insertedvalue, function (error, data) {
                            if(error){
                                console.log('create error',error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                               
                                let aggrQuery = [
                                    { $match : { 'candidateId': candidateId } },
                                    {
                                        $group :{
                                            _id :"$candidateId",
                                            allRatingAvg: { $avg: "$rating" },
                                        }
                                    }
                                ]
                                models.rating.aggregate(aggrQuery).exec((err, result) => {
                                    models.candidate.updateOne({'candidateId': candidateId},{'rating':result[0].allRatingAvg}, function (updatederror, updatedresponse) {
                                        if(updatederror){
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                        else{
                                            let employerName;
                                            if (updatedresponse.nModified == 1) {
                                                models.employer.find({"employerId":employerId },  function (error, data) {
                                                    console.log('......',data[0].companyName)
                                                    console.log('......',data[0].individualContactName)
                                                    if(data[0].companyName!==""){
                                                        employerName=data[0].companyName
                                                    }
                                                    else if(data[0].individualContactName!==""){
                                                        employerName=data[0].individualContactName
                                                    }
                                                    else{
                                                        employerName="Employer"
                                                    }
                                                models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken,employerName+" gives you "+rating+" rating ")
                                                    .then(function(nofify){
                                                        res.json({
                                                            isError: false,
                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                            statuscode: 200,
                                                            details: null
                                                        })
                                                    }) 
                                                  
                                                })
                                            })
                                                
                                               
                                            }
                                            else{
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                }) 
                                            }
                                        }
                                    })
                                  
                                })
                                
                            }
        
                        })
                    }
                       else{
                        console.log("record found ,Its time to update")
                            let updateWhere={
                                "roleId":roleId,
                                "jobId":jobId,
                                "employerId":employerId,
                                "candidateId":candidateId,
                            }
                            let updateWith={
                                "review":review,
                                "rating":rating
                            }
                            models.rating.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                                if(updatederror){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                                else{
                                    if (updatedresponse.nModified == 1) {
                                      
                                        let aggrQuery = [
                                            { $match : { 'candidateId': candidateId } },
                                            {
                                                $group :{
                                                    _id :"$candidateId",
                                                    allRatingAvg: { $avg: "$rating" },
                                                }
                                            }
                                        ]
                                        models.rating.aggregate(aggrQuery).exec((err, result) => {
                                            models.candidate.updateOne({'candidateId': candidateId},{'rating':result[0].allRatingAvg}, function (updatederror, updatedresponse) {
                                                if(updatederror){
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                                else{
                                                    let employerName1;
                                                    if (updatedresponse.nModified == 1) {
                                                        models.employer.find({"employerId":employerId },  function (error, data) {
                                                            
                                                            console.log('......',data[0].companyName)
                                                            console.log('......',data[0].individualContactName)
                                                            if(data[0].companyName!==""){
                                                                employerName1=data[0].companyName
                                                            }
                                                            else if(data[0].individualContactName!==""){
                                                                employerName1=data[0].individualContactName
                                                            }
                                                            else{
                                                                employerName1="Employer"
                                                            }
                                                        models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                            fcm.sendTocken(searchRating[0].Status.fcmTocken, employerName1 +" gives you "+rating+" rating ")
                                                            .then(function(nofify){
                                                                res.json({
                                                                    isError: false,
                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                    statuscode: 200,
                                                                    details:null
                                                                })
                                                            }) 
                                                          
                                                        })
                                                    })
                                                    }
                                                    else{
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        }) 
                                                    }
                                                }
                                            })
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                       }
                }
               
            })
          

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
     /**
     * @abstract 
     * Update Job
     */
    updateJob:(req, res, next) => {
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let jobDetails = req.body.jobDetails ? req.body.jobDetails : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - jobDetails"
            });
            if (!req.body.roleId || !req.body.jobId) { return; }

            console.log(req.body);
           
            let updateWith={
                "jobDetails.roleWiseAction.$.skills":jobDetails.skills,
                "jobDetails.roleWiseAction.$.roleId":jobDetails.roleId,
                "jobDetails.roleWiseAction.$.roleName": jobDetails.roleName,
                "jobDetails.roleWiseAction.$.jobType": jobDetails.jobType,
                "jobDetails.roleWiseAction.$.payType": jobDetails.payType,
                "jobDetails.roleWiseAction.$.pay": jobDetails.pay,
                "jobDetails.roleWiseAction.$.noOfStuff": jobDetails.noOfStuff,
                "jobDetails.roleWiseAction.$.uploadFile": jobDetails.uploadFile,
                "jobDetails.roleWiseAction.$.paymentStatus": jobDetails.paymentStatus,
                "jobDetails.roleWiseAction.$.status": jobDetails.status,
                "jobDetails.roleWiseAction.$.location": jobDetails.location,
                "jobDetails.roleWiseAction.$.payment": jobDetails.payment,
                "jobDetails.roleWiseAction.$.locationName": jobDetails.locationName,
                "jobDetails.roleWiseAction.$.description": jobDetails.description,
                "jobDetails.roleWiseAction.$.distance": jobDetails.distance,
                "jobDetails.roleWiseAction.$.setTime": jobDetails.setTime,
                "jobDetails.roleWiseAction.$.locationTrackingRadius": jobDetails.locationTrackingRadius,
                        
               
            }
            // console.log(jobDetails.setTime);
            // delete car.brand
            

            // console.log(req.body);
            // return;
            let updateWhere={
                "jobId":jobId,
                "jobDetails.roleWiseAction.roleId":roleId,
                
            }
            models.job.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     * Edit Job
     */
    editJob:(req, res, next) => {
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.roleId || !req.body.jobId) { return; } 
            // let findWhere={
            //     "jobId":jobId,
            //     "jobDetails.roleWiseAction.roleId":roleId,

            // }
            // models.job.findOne(findWhere,{ 'jobDetails.roleWiseAction.$': 1 }, function (err, item) {
            //     if(err){
            //         res.json({
            //             isError: true,
            //             message: errorMsgJSON['ResponseMsg']['1021'],
            //             statuscode: 1021,
            //             details: null
            //         });
            //     }
            //     else{
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details: item
            //         })
            //     }

            // })
            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                /********************** new Approach avoiding unwind, Starts******************** */
                {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                { $redact : {
                    $cond: {
                        if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                        then: "$$DESCEND",
                        else: "$$PRUNE"
                    }
               }},
               {
                    $project: {
                       
                        "industry": "$jobDetails.industry",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails"
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },

                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "employerId": 1,
                        "skillDetails": 1,
                        "jobDetails":{
                            "roleWiseAction":{ $arrayElemAt :["$jobDetails.roleWiseAction", 0]},
                            "industryDetails":{ $arrayElemAt :["$industryDetails", 0]}
                        },
                       
                    }
                }
                /********************** new Approach avoiding unwind ends******************** */
                // { $unwind: "$jobDetails.roleWiseAction" },
                // {
                //     $project: {
                //         "_id":0,
                //         "jobId": 1,
                //         "employerId": 1,
                //         "jobDetails": 1,
                //     }
                // },
                // {
                //     $group: {
                //         "_id": {
                //             "roleId": "$jobDetails.roleWiseAction.roleId",
                //             "jobId": "$jobId"
                //         },
                //         "jobDetails": {
                //             $first: "$jobDetails"
                //         },
                //         "jobId": {
                //             $first: "$jobId"
                //         },
                //         "employerId": {
                //             $first: "$employerId"
                //         },
                //     }
                // },
                // {
                //     $project: {
                //         "_id":0,
                //         "jobId": 1,
                //         "employerId": 1,
                //         "jobDetails": 1,
                //     }
                // },
                // { $match : { 'jobDetails.roleWiseAction.roleId': roleId } },
                // {
                //     $project: {
                       
                //         "industry": "$jobDetails.industry",
                //         "jobId": "$jobId",
                //         "employerId": "$employerId",
                //         "jobDetails": "$jobDetails"
                //     }
                // },
                // {
                //     $lookup: {
                //         from: "industries",
                //         localField: "industry",
                //         foreignField: "industryId",
                //         as: "industryDetails"
                //     }
                // },
                // {
                //     $project: {
                //         "_id":0,
                //         "jobId": 1,
                //         "employerId": 1,
                //         "jobDetails": 1,
                //         "industryDetails":{ $arrayElemAt :["$industryDetails", 0]}
                //     }
                // },
               
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     *HIRE CANDIDATE
     * According to logic after hire details of accept list in job collection must moved ito hire list in job collection
     * this is why first doing pull and then pull operation on job collection
     * to reflect in candidate section again the details of acceptedJobs in candidate collection
     * moved into hiredJobs array of candidate collection.
    */
    hireCandidate:(req, res, next) => {
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }
            models.job.updateOne({"jobId":jobId},{ "$pull": { "acceptList": { "candidateId": candidateId ,"roleId":roleId} }}, { safe: true, multi:true }, function (deletedderror, deletedresponse) {
                if (deletedderror) {
                    console.log(deletedderror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (deletedresponse.nModified == 1) {
                        let insertedpushValue={
                            "roleId": roleId,
                            "candidateId":candidateId
                        }
                        models.job.updateOne({"jobId":jobId}, { $push: { "hireList":insertedpushValue } }, function (pushederror, pushedresponse) {
                            if (pushederror) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                if (pushedresponse.nModified == 1) {
                                    models.candidate.updateOne({"candidateId":candidateId},{ "$pull": { "acceptedJobs": { "jobId": jobId ,"roleId":roleId,"employerId":employerId} }}, { safe: true, multi:true }, function (candidateDeleteddError, candidateDeletedResponse) {
                                      if(candidateDeleteddError){
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                      } 
                                      else{
                                            if (candidateDeletedResponse.nModified == 1) {
                                                let candidateInsertedValue={
                                                    "roleId": roleId,"employerId":employerId,"jobId":jobId,"candidateId":candidateId
                                                }
                                                models.candidate.updateOne({ 'candidateId': candidateId }, { $push: { "hiredJobs":candidateInsertedValue } }, function (candidateUpdatedError, candidateUpdatedResponse) {
                                                    if(candidateUpdatedError){
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                    else{
                                                        if (candidateUpdatedResponse.nModified == 1) {
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                statuscode: 200,
                                                                details: null
                                                            });
                                                        }
                                                        else{
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                statuscode: 1004,
                                                                details: null
                                                            })      
                                                        }
                                                    } 
                                                })
                                            }
                                            else{
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })   
                                            }
                                        } 
                                    }) 
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })   
                                }
                            }
                        })
                    }else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })  
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    serachOngingJobByName:(req, res, next) => {
        
        try{
            let date=Math.floor(Date.now() / 1000);
            let FinalDate=date*1000;
            console.log(FinalDate)
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $gt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$hireList" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$setTime" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":1,
                        "industryDetails":1
                    }
                },
                { '$match': { 
                    'hireListCount': {"$eq":false}}
                },
                {
                    '$match': { 'roleName': { "$regex": serachText, "$options": "i" } }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
               
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
        console.log(2,error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    /**
     * @abstract 
     * SEARCH BY JOB ROLE NAME
     * role wise separation jobs and match given role name with all existing roles in jobs and fetch 
     * the matched jobs 
     * HERE before search insert searched job role name(IF NOT EXIST SAME NAME IN ARRAY) in resentSearchDetails array in employer collection by calling 
     * inserSearchTextForHistory function.
    */
    serachJobByName1:(req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                         "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $project: {
                        "jobId": "$jobId",
                        "location": "$jobDetails.roleWiseAction.location",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime": "$jobDetails.roleWiseAction.setTime",
                        "payment": "$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus": "$jobDetails.roleWiseAction.paymentStatus",
                         "status": "$jobDetails.roleWiseAction.status",
                        "createdAt": "$createdAt",
                        "appiliedFrom": "$appiliedFrom",
                        "approvedTo": "$approvedTo",
                        "skillDetails": "$skillDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "roleId":1,
                        "roleName":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "roleId":1,
                        "roleName":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails": {$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    '$match': { 'jobDetails.roleWiseAction.roleName': { "$regex": serachText, "$options": "i" } }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* search job by name based on old structure*/ 
    serachJobByName:(req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": {$arrayElemAt : ["$payment",0]},
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails": {$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    '$match': { 'jobDetails.roleWiseAction.roleName': { "$regex": serachText, "$options": "i" } }
                },
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET LAST 10 SEARCHED JOB ROLE NAME 
     */
    getLastSearchNameDetails:(req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind : {path:"$resentSearchDetails",preserveNullAndEmptyArrays:true} },
                {
                    "$sort": {"resentSearchDetails.time": -1}
                },
                {
                   "$limit": 10
                },
                {
                    $project: {
                        resentSearchDetails:1,
                        _id:0,
                        employerId:1,
                    }
                },
                {
                    $group: {
                        "_id": {
                            "employerId": "$employerId",
                        },
                        "roleName": {
                            $push: "$resentSearchDetails.name"
                        }
                    }
                }
            ]
            models.employer.aggregate(aggrQuery).exec((err, result) => {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                   
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details:result[0]
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    searchPostedJob:(req, res, next) => {
        try{
            console.log('calling')
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let serachText = req.body.serachText ? req.body.serachText : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Search Text"
            });
            if (!req.body.employerId || !req.body.serachText) { return; }
            models.employer.find({'employerId': empId,"resentSearchDetails.name": { "$in":serachText } },
             function (err, item) {
                
                if(item.length==0){
                   
                    let updateWith={
                        "name": serachText,
                    }
                    models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
                else{
                    var searchValue = serachText.toLowerCase();
                    models.employer.updateOne({ 'employerId': empId },{ "$pull": { "resentSearchDetails": { "name": searchValue } }},{ safe: true }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                 
                                let updateWith={
                                    "name": serachText,
                                }
                                
                                models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, function (updatederror, updatedresponse) {
                                    if (updatederror) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                    else {
                                        if (updatedresponse.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * JOB APPROVE TO CANDIDATE 
     * check if job is approved to candidate earlier or not by checking roleId,candidateId,jobId from 
     * approveTo array.If not apparove earlier insert values (roleId,candidateId,jobId) in approvedTo array in job collection.
     * At the same time insert same values in candidate joboffers array.
     * Send mail or sms to candidate
     */
    jobApproveTo: (req, res, next) => {
        try{
                let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
                });

                let roleId = req.body.roleId ? req.body.roleId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
                });
                let employerId = req.body.employerId ? req.body.employerId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
                });
                let jobId = req.body.jobId ? req.body.jobId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
                });
                if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }
                let updateWith={
                    'roleId':roleId,
                    'candidateId':candidateId
                }
                let findwhere={
                    'approvedTo.roleId':roleId,
                    'approvedTo.candidateId':candidateId,
                    'jobId': jobId
                }
                models.job.findOne({'jobId': jobId }, function (error, itemjob){
                    if(error){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        }); 
                    }
                    else{
                        models.job.findOne(findwhere,{ 'approvedTo.$': 1 }, function (err, item) {
                            
                            if(err){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                if(item){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1030'],
                                        statuscode: 1030,
                                        details: null
                                    })
                                }
                                else{
                         
                                    models.job.updateOne({ 'employerId': employerId,'jobId':jobId },
                                        {$push: {'approvedTo':updateWith}}, 
                                        function (error, response) {
                                        if(error){
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        else{
                                            let insertedValue={
                                                "roleId": roleId,"employerId":employerId,
                                                "jobId":jobId,"candidateId":candidateId
                                                
                                            }
                                            models.candidate.updateOne({ 'candidateId': candidateId }, 
                                                { $push: { "jobOffers":insertedValue } }, 
                                                function (updatederror, updatedresponse) {
                                                if (updatederror) {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                if (updatedresponse.nModified == 1) {
                                                    let employerName;
                                                    models.authenticate.findOne({'userId':candidateId}, 
                                                        async function (err, item) {
                                                        if(item['mobileNo']==""){
                                                            var result = await  sendNotificationEmail(item['EmailId'],candidateId,jobId,roleId)
                                                            if(result){
                                                                models.employer.find({"employerId":employerId },  function (error, data) {
                                                                    console.log('......',data[0].companyName)
                                                                    console.log('......',data[0].individualContactName)
                                                                    if(data[0].companyName!==""){
                                                                        employerName=data[0].companyName
                                                                    }
                                                                    else if(data[0].individualContactName!==""){
                                                                        employerName=data[0].individualContactName
                                                                    }
                                                                    else{
                                                                        employerName="Employer"
                                                                    }
                                                                models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken,employerName+" approves your job application")
                                                                    .then(function(nofify){
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                                            statuscode: 200,
                                                                            details: null
                                                                        })
                                                                    }) 
                                                                  
                                                                })
                                                                })
                                                            }
                                                            else{
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                        else{
                                                            var result = await  sendNotificationSms(item['mobileNo'],candidateId,jobId,roleId)
                                                            if(result){
                                                                models.employer.find({"employerId":employerId },  function (error, data) {
                                                                    console.log('......',data[0].companyName)
                                                                    console.log('......',data[0].individualContactName)
                                                                    if(data[0].companyName!==""){
                                                                        employerName=data[0].companyName
                                                                    }
                                                                    else if(data[0].individualContactName!==""){
                                                                        employerName=data[0].individualContactName
                                                                    }
                                                                    else{
                                                                        employerName="Employer"
                                                                    }
                                                                models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken,employerName+" approves your job application")
                                                                    .then(function(nofify){
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                                            statuscode: 200,
                                                                            details: null
                                                                        })
                                                                    }) 
                                                                  
                                                                })
                                                                })
                                                            }
                                                            else{
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                    })
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    }) 
                                                }
                                            })
                                        }
                                    })
                                }
                            }
        
                        })
                    }
                })
            }
            catch (error) {
                console.log(error)
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }
    },
     /**
     * @abstract 
     * GET TEMPLATE
     * fetch templateby its id that was saved during job post 
     */
    getTemplate: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let templateId = req.body.templateId ? req.body.templateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Template Id"
            });
            if (!req.body.employerId || !req.body.templateId) { return; }
            // models.employer.findOne({ 'employerId': empId },
            //     function (err, item) {
            //         if (err) {
            //             res.json({
            //                 isError: true,
            //                 message: errorMsgJSON['ResponseMsg']['404'],
            //                 statuscode: 404,
            //                 details: null
            //             });
            //         }
            //         let itemTemplates = item['templates'];
            //         let filteredTemplate = itemTemplates.filter(eachTmp => {
            //             if (eachTmp.templateId == templateId) {
            //                 return eachTmp;
            //             }
            //         })
            //         delete item['templates'];
            //         item['templates'] = filteredTemplate;
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details:item['templates'][0]
            //         })
            //     })

            let aggrQuery=[
                {
                    '$match': { 'employerId': empId },
                },
                { $project: {
                    templates:1
                 }},
                 { $unwind: "$templates" },
                 { $unwind: "$templates.jobDetails.roleWiseAction" },

                {
                    $lookup: {
                        from: "skills",
                        localField: "templates.jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                
                 {
                    $group: {
                        "_id": {
                            "templateId": "$templates.templateId",

                          
                         },
                         "rolewiseSkills":{
                            $push: {
                                "updatedAt":"",
                                "createdAt":"",
                                "industryId":"$templates.jobDetails.industry",
                                "jobRoleId":"$templates.jobDetails.roleWiseAction.roleId",
                                "jobRoleName":"$templates.jobDetails.roleWiseAction.roleName",
                                "skillIds": "$templates.jobDetails.roleWiseAction.skills",
                               
                                
                            }
                         },
                         "skills":{
                            $push: {
                                
                                "skillDetails": "$skillDetails"                              
                            }
                         },
                         "locationTrackingRadius": {
                             $first: "$templates.locationTrackingRadius"
                         },
                         "location":{
                            $first: "$templates.jobDetails.roleWiseAction.location"
                         },
                         "locationName":{
                            $first: "$templates.jobDetails.roleWiseAction.locationName"
                         },
                        
                        "industry": {
                            $first: "$templates.jobDetails.industry"
                        },
                        "templateName": {
                            $first: "$templates.templateName"
                        },
                        "templateId": {
                            $first: "$templates.templateId"
                        },
                        "payment": {
                            $first: "$templates.jobDetails.roleWiseAction.payment"
                        },
                        "setTime": {
                            $first: "$templates.jobDetails.roleWiseAction.setTime"
                        },
                        "description": {
                            $first: "$templates.jobDetails.roleWiseAction.description"
                        },
                        "distance": {
                            $first: "$templates.jobDetails.roleWiseAction.distance"
                        },
                        "jobType": {
                            $first: "$templates.jobDetails.roleWiseAction.jobType"
                        },
                        "payType": {
                            $first: "$templates.jobDetails.roleWiseAction.payType"
                        },
                        "pay": {
                            $first: "$templates.jobDetails.roleWiseAction.pay"
                        },
                        "noOfStuff": {
                            $first: "$templates.jobDetails.roleWiseAction.noOfStuff"
                        },
                        "uploadFile": {
                            $first: "$templates.jobDetails.roleWiseAction.uploadFile"
                        },
                        "paymentStatus": {
                            $first: "$templates.jobDetails.roleWiseAction.paymentStatus"
                        },
                        "status": {
                            $first: "$templates.jobDetails.roleWiseAction.status"
                        },
                        
                    }
                },
                { $project: {
                        _id:0,
                        rolewiseSkills:1,
                        skills: 1,
                        locationTrackingRadius:1,
                        location:1,
                        industry:1,
                        templateName:1,
                        templateId:1,
                        payment:1,
                        setTime:1,
                        description:1,
                        distance:1,
                        jobType:1,
                        payType:1,
                        pay:1,
                        noOfStuff:1,
                        uploadFile:1,
                        paymentStatus:1,
                        status:1,
                        locationName:1
                       
                        
                    }
                },
               
                { $match : { 'templateId': templateId } }
                
            ]
            models.employer.aggregate(aggrQuery).exec((error, data) => {

                

                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * ADD TEMPLATE
     * Check if any template with given templte name is exist or not
     * If not add jobdetails as a new template
     */
    addTemplate: (req, res, next) => {
        try {
            let autoempid;
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let templateName = req.body.templateName ? req.body.templateName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Template Name"
            });
            let jobDetails = req.body.jobDetails ? req.body.jobDetails : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - JobDetails"
            });

            let locationTrackingRadius = req.body.jobDetails.locationTrackingRadius ? req.body.jobDetails.locationTrackingRadius :'';

            let type = "TEMPLATE";
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autoempid = data;
            })
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    models.employer.findOne({ 'employerId': empId, "templates.templateName": templateName }, function (err, item) {
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (item == null) {

                                let insertquery = {
                                    "templateName": templateName,
                                    "jobDetails": jobDetails, 
                                    "templateId":autoempid,
                                    "locationTrackingRadius": locationTrackingRadius
                                }

                                models.employer.updateOne({ 'employerId': empId }, { $push: { "templates": insertquery } }, function (updatederror, updatedresponse) {
                                    console.log('............',updatederror)
                                    if (updatederror) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                    else {
                                        if (updatedresponse.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1024'],
                                    statuscode: 1024,
                                    details: null
                                })
                            }
                        }
                    })
                 }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET APPLIED CANDIDATE
     * jobid and empId wise separate jobs
     * fetch appliedfrom array from each job
     * show only those object in the array whose roleId match the given roleId 
     */
   getAppliedCandidate1: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - JobId"
            });
            if (!req.body.employerId || !req.body.roleId || !req.body.jobId) { return; }
            let aggrQuery = [
                {
                    '$match':{ $and: [ { 'employerId': empId },
                                       {'jobId':jobId},
                                    ]
                             }
                },
                 { $unwind: "$jobDetails.roleWiseAction" },
                 { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "acceptList":1,
                        "approvedTo": 1,
                        "rejectList":1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $project:{
                        "skillDetails":"$skillDetails",
                        "rejectList":"$rejectList",
                        "approvedTo":"$approvedTo",
                        "acceptList":"$acceptList",
                        "hireList":"$hireList",
                        "appiliedFrom":"$appiliedFrom",
                        "createdAt":"$createdAt",
                        "jobId":"$jobId",
                        "employerId":"$employerId",
                        "jobDetails":"$jobDetails",
                        "location": "$jobDetails.roleWiseAction.location",
                        "description": "$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "status":"$jobDetails.roleWiseAction.status",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails":{
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "hireList":{
                            $first: "$hireList"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "acceptList": {
                            $first: "$acceptList"
                        },
                        "rejectList": {
                            $first: "$rejectList"
                        },
                        
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    },
                    
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                         "jobId": 1,
                         "location": 1,
                         "employerId": 1,
                         "description": 1,
                         "setTime": 1,
                         "payment": 1,
                         "uploadFile": 1,
                         "paymentStatus": 1,
                         "status": 1,
                         "createdAt": 1,
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "rejectList":{ $filter: { input: "$rejectList", as: "rejectList", cond: { $eq: ["$$rejectList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "acceptList":{ $filter: { input: "$acceptList", as: "acceptList", cond: { $eq: ["$$acceptList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1,
                        "roleId":1,
                        "roleName":1,
                        "jobType":1,
                        "payType":1,
                        "pay":1,
                        "noOfStuff": 1,
                        "distance":1,
                         "jobDetails":1
                        
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "approvedTo.candidateId",
                        foreignField: "candidateId",
                        as: "approvedTo"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "acceptList.candidateId",
                        foreignField: "candidateId",
                        as: "acceptList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "rejectList.candidateId",
                        foreignField: "candidateId",
                        as: "rejectList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "hireList"
                    }
                },
                {
                    $project: {
                        "approvedTo.candidateId": 1,
                        "approvedTo.fname": 1,
                        "approvedTo.mname": 1,
                        "approvedTo.lname": 1,
                        "approvedTo.profilePic":1,
                        "approvedTo.EmailId":1,
                        "approvedTo.mobileNo":1,
                        "approvedTo.memberSince":1,
                        "hireList.candidateId": 1,
                        "hireList.fname": 1,
                        "hireList.mname": 1,
                        "hireList.lname": 1,
                        "hireList.profilePic":1,
                        "hireList.EmailId":1,
                        "hireList.mobileNo":1,
                        "hireList.memberSince":1,
                        "rejectList.candidateId": 1,
                        "rejectList.fname": 1,
                        "rejectList.mname": 1,
                        "rejectList.lname": 1,
                        "rejectList.profilePic":1,
                        "rejectList.EmailId":1,
                        "rejectList.mobileNo":1,
                        "rejectList.memberSince":1,
                        "acceptList.candidateId": 1,
                        "acceptList.fname": 1,
                        "acceptList.mname": 1,
                        "acceptList.lname": 1,
                        "acceptList.profilePic":1,
                        "acceptList.EmailId":1,
                        "acceptList.mobileNo":1,
                        "acceptList.memberSince":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "appiliedFrom.profilePic":1,
                        "appiliedFrom.EmailId":1,
                        "appiliedFrom.mobileNo":1,
                        "appiliedFrom.memberSince":1,
                        "appiliedFrom.favouriteBy": 1,
                        // "hireList.isFavourite":{
                        //     $cond:[{$setIsSubset: [["$employerId"], "$hireList.favouriteBy"]},true,false]
                        // },
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        // "approvedTo": 1,
                        "roleId":1,
                        "roleName":1,
                        "jobType":1,
                        "payType":1,
                        "pay":1,
                        "noOfStuff": 1,
                        "distance":1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                
                { $match : { 'jobDetails.roleWiseAction.roleId': roleId } }
                
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /*getAppliedCandidate based on old structure*/
    getAppliedCandidate: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - JobId"
            });
            if (!req.body.employerId || !req.body.roleId || !req.body.jobId) { return; }
            let aggrQuery = [
                {
                    '$match':{ $and: [ { 'employerId': empId },
                                       {'jobId':jobId},
                                    ]
                             }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "acceptList":1,
                        "approvedTo": 1,
                        "rejectList":1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },

                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "hireList":{
                            $first: "$hireList"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "acceptList": {
                            $first: "$acceptList"
                        },
                        "rejectList": {
                            $first: "$rejectList"
                        },
                        
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    },
                    
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "rejectList":{ $filter: { input: "$rejectList", as: "rejectList", cond: { $eq: ["$$rejectList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "acceptList":{ $filter: { input: "$acceptList", as: "acceptList", cond: { $eq: ["$$acceptList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "acceptList.candidateId",
                        foreignField: "candidateId",
                        as: "acceptList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "rejectList.candidateId",
                        foreignField: "candidateId",
                        as: "rejectList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "hireList"
                    }
                },
                {
                    $project: {
                        "hireList.candidateId": 1,
                        "hireList.fname": 1,
                        "hireList.mname": 1,
                        "hireList.lname": 1,
                        "rejectList.candidateId": 1,
                        "rejectList.fname": 1,
                        "rejectList.mname": 1,
                        "rejectList.lname": 1,
                        "acceptList.candidateId": 1,
                        "acceptList.fname": 1,
                        "acceptList.mname": 1,
                        "acceptList.lname": 1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": {$arrayElemAt : ["$payment",0]},
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                { $match : { 'jobDetails.roleWiseAction.roleId': roleId } }
                
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getClosedJobs: (req, res, next) => {
        try{
            let date=Math.floor(Date.now() / 1000);
            let FinalDate=date*1000;
            console.log(FinalDate)
           let empId = req.body.employerId ? req.body.employerId : res.json({
               isError: true,
               message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
           });
           let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
           let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
           if (!req.body.employerId ) { return; }
           let aggrQuery = [
            {
                '$match': { 'employerId': empId }
            },
            { $unwind: "$jobDetails.roleWiseAction" },
            { $unwind: "$jobDetails.roleWiseAction.skills" },
            {
                $lookup: {
                    from: "skills",
                    localField: "jobDetails.roleWiseAction.skills",
                    foreignField: "skillId",
                    as: "skillDetails"
                }
            },
            {
                $project: {
                    "jobId": 1,
                    "employerId": 1,
                    "jobDetails": 1,
                    "createdAt": 1,
                    "appiliedFrom": 1,
                    "hireList":1,
                    "approvedTo": 1,
                    "skillDetails": {
                        $arrayElemAt: ["$skillDetails", 0]
                    }
                }
            },
            {
                "$project":{
                    "jobId":"$jobId",
                    "hireList":"$hireList",
                    "employerId":"$employerId",
                    "createdAt":"$createdAt",
                    "appiliedFrom":"$appiliedFrom",
                    "approvedTo":"$approvedTo",
                    "skillDetails":"$skillDetails",
                    "status":"$jobDetails.roleWiseAction.status",
                    "location":"$jobDetails.roleWiseAction.location",
                    "locationName":"$jobDetails.roleWiseAction.locationName",
                    "description":"$jobDetails.roleWiseAction.description",
                    "setTime":"$jobDetails.roleWiseAction.setTime",
                    "payment":"$jobDetails.roleWiseAction.payment",
                    "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                    "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                    "jobDetails":"$jobDetails",
                    "roleId":"$jobDetails.roleWiseAction.roleId",
                    "roleName":"$jobDetails.roleWiseAction.roleName",
                    "jobType": "$jobDetails.roleWiseAction.jobType",
                    "payType":"$jobDetails.roleWiseAction.payType",
                    "pay": "$jobDetails.roleWiseAction.pay",
                    "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                    "distance":"$jobDetails.roleWiseAction.distance",
                    }
            },
            {
                $group: {
                    "_id": {
                        "roleId": "$jobDetails.roleWiseAction.roleId",
                        "jobId": "$jobId"
                    },
                    "jobType": {
                        $first: "$jobType"
                    },
                    "payType": {
                        $first: "$payType"
                    },
                    "pay": {
                        $first: "$pay"
                    },
                    "noOfStuff": {
                        $first: "$noOfStuff"
                    },
                    "distance": {
                        $first: "$distance"
                    },
                    "jobDetails": {
                        $first: "$jobDetails"
                    },
                    "locationName":{
                        $first: "$locationName"
                    },
                    "location": {
                        $first: "$location"
                    },
                    "employerId": {
                        $first: "$employerId"
                    },
                    "jobId": {
                        $first: "$jobId"
                    },
                    "paymentStatus": {
                        $first: "$paymentStatus"
                    },
                    "appiliedFrom": {
                        $first: "$appiliedFrom"
                    },
                    "approvedTo": {
                        $first: "$approvedTo"
                    },
                    "hireList": {
                        $first: "$hireList"
                    },
                    "approvedTo": {
                        $first: "$approvedTo"
                    },
                    "createdAt": {
                        $first: "$createdAt"
                    },
                    "status": {
                        $first: "$status"
                    },
                    "uploadFile": {
                        $first: "$uploadFile"
                    },
                    "description": {
                        $first: "$description"
                    },
                    "setTime": {
                        $first: "$setTime"
                    },
                    "industry": {
                        $first: "$jobDetails.industry"
                    },
                    "payment": {
                        $first: "$payment"
                    },
                    "roleId": {
                        $first: "$roleId"
                    },
                    "roleName": {
                        $first: "$roleName"
                    },
                    "skillDetails": {
                        $push: "$skillDetails"
                    }
                }
            },
            {
                $lookup: {
                    from: "industries",
                    localField: "industry",
                    foreignField: "industryId",
                    as: "industryDetails"
                }
            },
            {
                $project: {
                    "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                    "jobType": 1,
                    "payType":1,
                    "pay": 1,
                    "noOfStuff": 1,
                    "distance":1,
                    "roleName":1,
                    "roleId":1,
                    "jobId": 1,
                    "location": 1,
                    "locationName":1,
                    "employerId": 1,
                    "description": 1,
                    "setTime": 1,
                    "payment": 1,
                    "uploadFile": 1,
                    "paymentStatus": 1,
                    "status": 1,
                    "createdAt": 1,
                    "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                    "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                    "skillDetails": 1,
                    "industryDetails": 1
                }
            },
            {
                $lookup: {
                    from: "candidates",
                    localField: "appiliedFrom.candidateId",
                    foreignField: "candidateId",
                    as: "appiliedFrom"
                }
            },
            {
                $project: {
                    "jobType": 1,
                    "payType":1,
                    "pay": 1,
                    "noOfStuff": 1,
                    "distance":1,
                    "roleName":1,
                    "roleId":1,
                    "appiliedFrom.candidateId": 1,
                    "appiliedFrom.fname": 1,
                    "appiliedFrom.mname": 1,
                    "appiliedFrom.lname": 1,
                    "jobId": 1,
                    "location": 1,
                    "locationName":1,
                    "employerId": 1,
                    "description": 1,
                    "setTime":1,
                    "originalsetTimeCount":{$size: "$setTime"},
                    "setTime1":  {
                        "$map": {
                          "input": {
                            "$filter": {
                              "input": "$setTime",
                              "as": "item",
                              "cond": { $lt: ["$$item.endDate", FinalDate] }
                            }
                          },
                          "as": "finalItem",
                          "in": {
                            "endDate": "$$finalItem.endDate",
                            
                          }
                        }
                      },
                    "payment": 1,
                    "uploadFile": 1,
                    "paymentStatus": 1,
                    "status": 1,
                    "createdAt": 1,
                    "skillDetails": 1,
                    "approvedTo": 1,
                    "hireList":1,
                    "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                }
            },
            {
                $project: {
                    "jobType": 1,
                    "payType":1,
                    "pay": 1,
                    "noOfStuff": 1,
                    "distance":1,
                    "roleName":1,
                    "roleId":1,
                    "appiliedFrom.candidateId": 1,
                    "appiliedFrom.fname": 1,
                    "appiliedFrom.mname": 1,
                    "appiliedFrom.lname": 1,
                    "jobId": 1,
                    "location": 1,
                    "locationName":1,
                    "employerId": 1,
                    "description": 1,
                    "setTime1":1,
                    "setTimeCount":{$size: "$setTime1"},
                    "setTime":1,
                    "originalsetTimeCount":1,
                    "payment": 1,
                    "uploadFile": 1,
                    "paymentStatus": 1,
                    "status": 1,
                    "createdAt": 1,
                    "skillDetails": 1,
                    "approvedTo": 1,
                    "hireList":1,
                    "industryDetails":1,
                   
                }
            },
            {$match:{$expr:{$eq:["$setTimeCount","$originalsetTimeCount"]}}},
           
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        models.job.aggregate(aggrQuery).exec((error, data) => {
            if (error) {
                console.log(error)
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }
            else {
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: data[0]
                })
            }
        })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    serachCompletedJobByName: (req, res, next) => {
        try{
            let date=Math.floor(Date.now() / 1000);
            let FinalDate=date*1000;
            console.log(FinalDate)
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireList":1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{$size: "$setTime"},
                        "setTime":1,
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireList":1,
                        "industryDetails":1
                    }
                },
                {$match:{$expr:{$eq:["$setTimeCount","$originalsetTimeCount"]}}},
                {
                    '$match': { 'roleName': { "$regex": serachText, "$options": "i" } }
                },
               
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
               
               
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getOngoingJobs: (req, res, next) => {
        try{
             let date=Math.floor(Date.now() / 1000);
             let FinalDate=date*1000;
             console.log(FinalDate)
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            if (!req.body.employerId ) { return; }
           
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$hireList" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{$size: "$setTime"},
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":1,
                        "industryDetails":1
                    }
                },
                { '$match': { 
                    'hireListCount': {"$eq":false}}
                },
                {$match:{$expr:{$ne:["$setTimeCount","$originalsetTimeCount"]}}},
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET ALL JOBS
     * empId wise separate jobs and get all jobs of an employer
     */
    getJobs1: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date=Math.floor(Date.now() / 1000);
            let FinalDate=date*1000;
            console.log(FinalDate)
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            if (!req.body.employerId ) { return; }
           
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                       // "setTime": 1,
                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{$size: "$setTime"},
                        "setTime":1,
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$hireList" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                { '$match': { 
                    'hireListCount': {"$eq":true}}
                },
                {$match:{$expr:{$ne:["$setTimeCount","$originalsetTimeCount"]}}},
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* get job in old structure*/ 
    getJobs: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId ) { return; }
           
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },

                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },

                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": {$arrayElemAt : ["$payment",0]},
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET  JOB BY ID
     * Fetch all details of a job by filter JobId.
     */
    GetJobById: (req, res, next) => {
        let jobId = req.body.jobId ? req.body.jobId : res.send({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
        });
        if (!req.body.jobId) { return; }
        models.job.find({ 'jobId': jobId }, function (err, item) {
            console.log(item)
            if (item.length == 0) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1021'],
                    statuscode: 1021,
                    details: null
                })
            }
            else {
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item
                })
            }
        })
    },
     /**
     * @abstract 
     * POST SINGLE JOB
     */
    PostJob: (req, res, next) => {
        try {
            // below code is based on old json structure
           let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let industry = req.body.industry ? req.body.industry : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry"
            });

            let jobType = req.body.jobType ? req.body.jobType : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Type"
            });
            let payType = req.body.payType ? req.body.payType : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Pay Type"
            });
            let pay = req.body.pay ? req.body.pay : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Pay"
            });
            let noOfStuff = req.body.noOfStuff ? req.body.noOfStuff : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - No Of Stuff"
            });
            let setTime = req.body.setTime ? req.body.setTime : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Set Time"
            });
            let uploadFile = req.body.uploadFile ? req.body.uploadFile : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Upload File"
            });
            let payment = req.body.payment ? req.body.payment : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Payment"
            });
            let description = req.body.description ? req.body.description : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Description"
            });
            let roleWiseAction = req.body.roleWiseAction ? req.body.roleWiseAction : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Wise Action"
            });
            let location = req.body.location ? req.body.location : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Location"
            });
            let getRoleWiseAction = roleWiseAction
            let setTimeDetails = setTime
            let paymentDetails = payment
            let locationDetails = location
            if (!req.body.description || !req.body.uploadFile || !req.body.employerId || !req.body.industry || !req.body.jobType || !req.body.payType || !req.body.pay || !req.body.noOfStuff) { return; }
            let type = "JOB";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let insertquery = {
                        '_id': new mongoose.Types.ObjectId(), "location": locationDetails,
                        "setTime": setTimeDetails, "payment": paymentDetails, "description": description,
                        "jobDetails.roleWiseAction": getRoleWiseAction, 'jobDetails.industry': industry, 'jobDetails.jobType': jobType,
                        'jobDetails.payType': payType, 'jobDetails.pay': pay, 'jobDetails.noOfStuff': noOfStuff, 'jobId': autouserid,
                        'employerId': empId, 'uploadFile': uploadFile, 'paymentStatus': 'unpaid', 'status': 'posted'
                    }
                    models.job.create(insertquery, function (err, data) {
                        if (err) {
                            console.log(err);
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: data
                            })
                        }
                    })
                }
            })
            // below code is based on New json structure .use it if necessary.Structure is given below.
            // let jobdetails = req.body.jobdetails ? req.body.jobdetails : res.json({
            //     message: errorMsgJSON['ResponseMsg']['303'] + " - Job Details",
            //     isError: true
            // });
            // models.job.insertMany(jobdetails, function (err, data) {
            //     if (err) {
            //         console.log(err);
            //         res.json({
            //             isError: true,
            //             message: errorMsgJSON['ResponseMsg']['404'],
            //             statuscode: 404,
            //             details: null
            //         });
            //     }
            //     else {
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details: data

            //         })
            //     }
            // })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    /*****************************************************
    * Post multiple job at one attempt[if needed use this]
    *****************************************************/
    /**
    * @abstract 
    * Add Multiple job
   */
    postMultipleJob1: async (req, res, next) => {
        try {
            let jobdetails = req.body.jobdetails ? req.body.jobdetails : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Details",
                isError: true
            });

        

            //console.log(req.body.jobdetails[0].jobDetails.roleWiseAction[0].setTime);
            // return;

            console.log("************************")
            console.log('log1',jobdetails)
            console.log("************************")
             
            let newJobArr = await jobdetails.map(tt => {
                tt['jobId'] = 'JOB' + new Date().getTime()+ parseInt(Math.random()*100)
                return tt;
            })
   
            console.log("************************")
            console.log('log2',newJobArr)
            console.log("************************")
            //===========================================
            let employerId = jobdetails[0].employerId;     
            let aggrQuery = [
              {
                $match: {
                  'employerId': employerId,
                } 
              },
              {
                $project: {
                    "Status": 1                            
                }
              }    
            ];

            models.employer.aggregate(aggrQuery, function (err, response) {
                 
                if (response[0].Status.isApprovedByAot == false) {
                    res.json({
                        isError: true,
                        message: "Employer is not approved by admin yet",
                        statuscode: 204,
                        details: null
                    })  
                    return; 
                } 
                    //====================================
                    models.job.insertMany(newJobArr, function (err, data) {
                        if (err) {
                            console.log(err);
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: data
                            })
                        }
                    }) // END models.job.insertMany(newJobArr, function (err, data) {
                    //===========================================
            
            }) //END models.employer.aggregate(aggrQuery, function (err, response) {      
            //===============================    
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* post multiple job in old structure */
    postMultipleJob: async (req, res, next) => {
        try {
            let jobdetails = req.body.jobdetails ? req.body.jobdetails : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Details",
                isError: true
            });

            console.log("************************")
            console.log('log1',jobdetails)
            console.log("************************")

            let newJobArr = await jobdetails.map(tt => {
                tt['jobId'] = 'JOB' + new Date().getTime()+ parseInt(Math.random()*100)
                return tt;
            })
            console.log("************************")
            console.log('log2',newJobArr)
            console.log("************************")
            models.job.insertMany(newJobArr, function (err, data) {
                if (err) {
                    console.log(err);
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data

                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    candidateList: async (req, res, next) => {
        try{
            var allcandidates = [];
            // var singlecand = [];
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            var candidates = await models.job.find({ 'employerId': empId });
            for(candidate of candidates) {
               //if(candidate.hireList.length > 0){
                    //var canddets = {};
                    var singlecand = await models.candidate.find({ 'candidateId': candidate.hireList[0].candidateId });

                    // allcandidates.push(candidate.jobId);
               //}
            }
            // console.log(candidates);
            res.send({
                isError: false,
                statuscode: 200,
                details: candidate.jobId
            });
        } catch {
            res.send({
                isError: true,
                statuscode: 404,
                details: "NOEN"
            });

        }
    },
    /**
     * @abstract 
     * GET ALL TEMPLATE
     * fetch all templates of an employer by using employerId
     */
    getAllTemplates: async (req, res, next) => {
        try{

            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId) { return; }
            // models.employer.findOne({ 'employerId': empId },
            //     function (err, item) {
            //         if (err) {
            //             res.json({
            //                 isError: true,
            //                 message: errorMsgJSON['ResponseMsg']['404'],
            //                 statuscode: 404,
            //                 details: null
            //             });
            //         }

                   
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details:item['templates']
            //         })
            //     })

            let aggrQuery=[
                {
                    '$match': { 'employerId': empId },
                    
                },
                { $project: {
                    templates:1
                 }},
                 { $unwind: "$templates" },
                 { $unwind: "$templates.jobDetails.roleWiseAction" },
                 {
                    $group: {
                        "_id": {
                            "templateId": "$templates.templateId",
                          
                         },
                         "templateId":{
                            $first: "$templates.templateId"
                         },
                         "templateName":{
                            $first: "$templates.templateName"
                         },
                       
                    }
                },
                { $project: {
                    templateId:1,
                    templateName:1,
                    _id:0
                    }
                },
               
                           
            ]
            models.employer.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    }
}

async function getCalculatedRoutes(JobIds){
   
    return await Promise.all(JobIds.map(item => item))
  
    
     
}

function sendNotificationEmail(emailId,candidateId,jobId,roleId){
    
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
           
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                globalVarObj.msgBody=`Dear, ${globalVarObj.fname} ${globalVarObj.mname} ${globalVarObj.lname}, your application for position ${globalVarObj.jobRoleName} and Job ID ${jobId}, have been approved.
Employer will contact with you shortly`;
                
                //==================
                let insertedValue={
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }

                models.candidate.updateOne({ 'candidateId': candidateId },
                    { $push: { "notification":insertedValue } }, 
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) { 
                //============== 

                        simplemailercontroller.viaGmail({ receiver: emailId, subject: '----JOB APPLICATION APPROVED----',msg:globalVarObj.msgBody}, (mailerErr, nodeMailerResponse) => {
                            if(mailerErr){
                                resolve('false');  
                            }
                            else{
                                resolve('true');
                            }
                        })

                }) // models.candidate.updateOne        
            })     
        })
    })
}



function sendNotificationSms(mobileNo,candidateId,jobId,roleId){
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                globalVarObj.msgBody=`Dear, ${globalVarObj.fname} ${globalVarObj.mname} ${globalVarObj.lname}, your application for position ${globalVarObj.jobRoleName} and Job ID ${jobId}, have been approved.
Employer will contact with you shortly`;

                //==================
                let insertedValue={
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }

                models.candidate.updateOne({ 'candidateId': candidateId },
                    { $push: { "notification":insertedValue } }, 
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) { 
                //============== 
                        sendsmscontroller.Sendsms(mobileNo, globalVarObj.notification,(twilioError, twilioResult) => {
                            console.log(twilioResult)
                            if(twilioError){
                                resolve('false'); 
                            }
                            else{
                                resolve('true');
                            }
                        }) 
                })  // models.candidate.updateOne          
            })
        })
    })
}

//=================== on 27-12-19



//===================================

function inserSearchTextForHistory(empId, serachText) {
    return new Promise(resolve => {
        models.employer.find({ 'employerId': empId, "resentSearchDetails.name": { "$in": serachText } },
            function (err, item) {
                if (item.length == 0) {
                    let updateWith = {
                        "name": serachText,
                    }
                    models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, async function (updatederror, updatedresponse) {
                        if (updatederror) {
                            resolve(false);
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                resolve(true);
                                
                            }
                            else {
                                resolve(false);
                            }
                        }
                    })
                }
                else {
                    var searchValue = serachText.toLowerCase();
                    models.employer.updateOne({ 'employerId': empId }, { "$pull": { "resentSearchDetails": { "name": searchValue } } }, { safe: true }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            resolve(false);
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                let updateWith = {
                                    "name": serachText,
                                }
                                models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, function (updatederror, updatedresponse) {
                                    if (updatederror) {
                                        resolve(false);
                                    }
                                    else {
                                        if (updatedresponse.nModified == 1) {
                                            resolve(true);
                                           
                                        }
                                        else {
                                            resolve(false);
                                        }
                                    }
                                })
                            }
                            else {
                                resolve(false);
                            }
                        }
                    })
                }
            })
    })
}



/******************************************************
 * Below is the new json structure for sending job post
        {
            "jobdetails": [
                {
                    "employerId": "EMP1560341566376",
                    "paymentStatus": "unpaid",
                    "status": "posted",
                    "setTime": [
                        {
                            "shift": "Morning",
                            "startDate": 1558607550915,
                            "endDate": 1558607550916,
                            "startTime": "10.00am",
                            "endTime": "6.00pm"
                        },
                        {
                            "shift": "Evening",
                            "startDate": 1558607550915,
                            "endDate": 1558607550915,
                            "startTime": "10.00am",
                            "endTime": "6.00pm"
                        }
                    ],
                    "uploadFile": "www.google.com",
                    "payment": [
                        {
                            "ratePerHour": 100,
                            "noOfStuff": 15,
                            "totalHour": 155,
                            "vat": 33,
                            "ni": 55,
                            "fee": 22,
                            "totalPayment": 666
                        }
                    ],
                    "jobDetails": {
                        "industry": "IND1558529767065",
                        "jobType": "fulltime",
                        "payType": "hour",
                        "pay": 5,
                        "noOfStuff": 5,
                        "roleWiseAction": [
                            {
                                "roleId": "ROLE1559285782576",
                                "roleName": "socialcare-Role4",
                                "skills": [
                                    "SKILL1559305685937",
                                    "SKILL1559299106852"
                                ]
                            },
                            {
                                "roleId": "ROLE1559285975687",
                                "roleName": "socialcare-Role5",
                                "skills": [
                                    "SKILL1559305685937",
                                    "SKILL1559626620098"
                                ]
                            }
                        ]
                    },
                    "description": "description",
                    "distance": 4,
                    "location": {
                        "type": "Point",
                        "coordinates": [
                            -110.8571443,
                            32.4586858
                        ]
                    }
                }
                
            ]
        }

 **********************************************************************/

/***********************************************************************
 * 
 *  "appiliedFrom": { "$setUnion": [ "$appiliedFrom", "$appiliedFrom" ] }
 ***********************************************************************/













  