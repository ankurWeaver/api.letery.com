const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const utilitycontroller = require('../service/utility');
const mailercontroller = require('../service/verification-mail');

//======================
const mailercontroller2 = require('../service/verification-mailForgetPassword');
//======================
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const config =  require('../config/env');
const sendsmscontroller = require('../service/TwilioSms');
var Request = require("request");

//====================================
// Added by Ankur
const EnvInfo = require('../model/envInfo');

module.exports = {

      //==========================================================================
    /*
        This function is written to get the number of cancelled job
    */
    getNoOfCancelledJob: (req, res, next) => {
        try {

            let aggrQuery = [
               
                {
                    $project : {
                        '_id' : 0,
                        'jobId': 1,
                        'cancelledJobs': 1
                    }
                },

                {
                    $match: { 
                        $or: [
                            {"cancelledJobs.cancelledBy": "Candidate"},
                            {"cancelledJobs.cancelledBy": "employerAllCandidate"},
                            {"cancelledJobs.cancelledBy": "employerOneCandidate"}
                        ] 
                        
                    }
                },

                {
                    $project : {
                        '_id' : 0,
                        'jobId': 1,
                        'cancelledJobsCandidate': {
                            $filter: {
                               input: "$cancelledJobs",
                               as: "cancelledJobsCandidate",
                               cond: { $eq: [ "$$cancelledJobsCandidate.cancelledBy", "Candidate" ] }
                            }
                        },
                        'cancelledJobsemployerAllCandidate': {
                            $filter: {
                               input: "$cancelledJobs",
                               as: "cancelledJobsCandidate",
                               cond: { $eq: [ "$$cancelledJobsCandidate.cancelledBy", "employerAllCandidate" ] }
                            }
                        },
                        'cancelledJobsemployerOneCandidate': {
                            $filter: {
                               input: "$cancelledJobs",
                               as: "cancelledJobsCandidate",
                               cond: { $eq: [ "$$cancelledJobsCandidate.cancelledBy", "employerOneCandidate" ] }
                            }
                        },
                    }
                },

                {
                    $project : {
                        'jobId': 1,
                        'numberOfCancelledJobsCandidate': { $size: "$cancelledJobsCandidate" },
                        'numberOfCancelledJobsemployerAllCandidate': { $size: "$cancelledJobsemployerAllCandidate" },
                        'numberOfCancelledJobsemployerOneCandidate': { $size: "$cancelledJobsemployerOneCandidate" },
                    }
                },

                {
                    $group: {
                        _id: null,
                        'numberOfCancelledJobsCandidate1' : {
                            $sum : '$numberOfCancelledJobsCandidate'
                        },
                        'numberOfCancelledJobsemployerAllCandidate1' : {
                            $sum : '$numberOfCancelledJobsemployerAllCandidate'
                        },
                        'numberOfCancelledJobsemployerOneCandidate1' : {
                            $sum : '$numberOfCancelledJobsemployerOneCandidate'
                        },
                    }
                },
               
            ];


            models.job.aggregate(aggrQuery).exec((err, item) => {

                if(err){
                   
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    })

                    return;
                } else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })

                    return;
                }

            });     
                

        } catch(error) {
            
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },


    //==========================================================================
    /*
        This function is written to redirect the admin to dashboard of the employer
    */
    authenticateAdmin: (req, res, next) => {
        try {
            let EmailId = req.body.EmailId ? req.body.EmailId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Email Id"
            });

            let password = req.body.password ? req.body.password : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Password"
            });

            let employerEmailId = req.body.employerEmailId ? req.body.employerEmailId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
            });

            if (!req.body.EmailId || !req.body.password || !req.body.employerEmailId) { return; }

            models.authenticate.findOne({"type":"ADMIN"}).exec(function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message:  "Error Occur In Fetching Admin Details",
                        statuscode:  404,
                        details: null
                    });

                    return;
                } 

                if (EmailId != item.EmailId) {
                    res.json({
                        isError: true,
                        message:  "Wrong Email Id Of Admin was given",
                        statuscode:  204,
                        details: null
                    });

                    return;
                }

                if (password != item.password) {
                    res.json({
                        isError: true,
                        message:  "Wrong Password Of Admin was given",
                        statuscode:  204,
                        details: null
                    });

                    return;
                }

                //============================================================================================
                
                let aggrQuery = [
                    {
                        $match: {  
                            "EmailId": employerEmailId
                        }
                    },
                    {
                        $project : {
                        '_id' : 0,
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            isVerified : {
                                $first : "$Status.isVerified"
                            },

                            //============================
                            isApprovedByAot : {
                                $first : "$Status.isApprovedByAot"
                            }, 
                            //=============================
                            results: {
                                $push : '$$ROOT'
                            }
                        }
                    },
                    {
                        $project: {
                            userDetails: {
                                $cond: { 
                                    if: { $eq: [ '$isVerified', true ] }, 
                                    then: "$results",
                                    else: []
                                } 
                            },
                        }
                    }
                ];
               
                models.employer.aggregate(aggrQuery).exec((err, item1) => {
                    if(err){
                       
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        })

                        return;
                    }
                    
                    if(item1.length == 0) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })

                        return;
                    } 

                    if (!item1[0].userDetails.length) {
                        res.json({
                            isError: true,
                            message: 'User not Verified.',
                            statuscode: 200,
                            details: null,
                            isVerified:false
                        })
                        
                        return;
                    } 
                               
                    const token = token_gen({
                        'employerId': (item1[0].userDetails[0].userType == 'EMPLOYER') ? item1[0].userDetails[0].employerId : item1[0].userDetails[0].candidateId,
                        'userType': item1[0].userDetails[0].userType
                    });
                    item1[0].userDetails[0].token = token;
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['204'],
                        statuscode: 204,
                        details: item1[0].userDetails[0],
                        isVerified:true,
                        isApprovedByAot: item1[0].userDetails[0].Status.isApprovedByAot
                    })                                        
                         
                })

            });    

        } catch(error) {
            
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },

      /*
        This function is written to fetch the details of Admin
    */
   updateAdminDetails: (req, res, next) => {
        try {
           

            let EmailId = req.body.EmailId ? req.body.EmailId : "";
            let password = req.body.password ? req.body.password : "";
            let emailForMailing = req.body.emailForMailing ? req.body.emailForMailing : "";
            let passwordForMailing = req.body.passwordForMailing ? req.body.passwordForMailing : "";
            let mobileNo = req.body.mobileNo ? req.body.mobileNo : "";

            if (EmailId == "" && password == "" && emailForMailing == "" && passwordForMailing == "") {
                res.json({
                    isError: false,
                    message: "You have not given any credentials to edit",
                    statuscode: 204,
                    details: null
                }) 

                return;
            }

            let updateItem = {};

            models.authenticate.findOne({"type":"ADMIN"}).exec(function (err, item) {

               
                if(err){
                    
                    res.json({
                        isError: true,
                        message: "Error Occur in fetching Admin Details",
                        statuscode: 404,
                        details: null
                    }) 
                } else { // Else if(err){

                    if (EmailId == "") {
                        EmailId = item.EmailId; 
                    }

                    if (password == "") {
                        password = item.password; 
                    }

                    if (emailForMailing == "") {
                        emailForMailing = item.emailForMailing; 
                    }

                    if (passwordForMailing == "") {
                        passwordForMailing = item.passwordForMailing; 
                    }

                    if (mobileNo == "") {
                        mobileNo = item.mobileNo; 
                    }


                    updateItem = { 
                        "EmailId" : EmailId,
                        "password" : password,
                        "emailForMailing" : emailForMailing,
                        "mobileNo": mobileNo,
                        "passwordForMailing" : passwordForMailing   
                    }; 
        
                    models.authenticate.updateOne({"type":"ADMIN"}, updateItem, 
                    function (err, updatedresponse) {
                    
                            if(err){
                                
                                res.json({
                                    isError: true,
                                    message: "Error Occur in Updating Admin Details",
                                    statuscode: 404,
                                    details: null
                                }) 
                            }
                            else{
                                res.json({
                                    isError: false,
                                    message: "Success",
                                    statuscode: 200,
                                    details: null

                                })  
                            }
                    })
                } // END Else if(err){    
            }) // END models.authenticate.findOne({"type":"ADMIN"}).exec(function (err, item) {
        } catch(error) {
            
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    }, 
    //==========================================================================
    /*
        This function is written to fetch the details of Admin
    */
    fetchAdminDetails: (req, res, next) => {
        try {
            

            models.authenticate.findOne({"type":"ADMIN"}).exec(function (err, item) {
                   
                    if(err){
                        
                        res.json({
                            isError: true,
                            message: "Error Occur in fetching Admin Details",
                            statuscode: 404,
                            details: null
                        }) 
                    }
                    else{
                        res.json({
                            isError: false,
                            message: "Success",
                            statuscode: 200,
                            details: item

                        })  
                    }
            })

           

        } catch(error) {
            
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },
    //==========================================================================
    /*
        this function is written to delete
        role for particular Id
    */
    deleteRole: (req, res, next) => {

        try {
            // Here, we are receiving Industry Id
            let industryId = req.body.industryId ? req.body.industryId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
            });

            // Here, we are receiving Role Id
            let jobRoleId = req.body.jobRoleId ? req.body.jobRoleId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });

            // avoid circular json
            if (!req.body.industryId || !req.body.jobRoleId) { return; }
            
            models.jobrole.deleteOne({ industryId: industryId ,jobRoleId: jobRoleId }, function (err) {  
           
                if (err) {
                    res.json({
                      isError: true,
                      message: errorMsgJSON['ResponseMsg']['1004'],
                      statuscode: 1004,
                      details: null
                    })
                } else {
                    res.json({
                      isError: false,
                      message: errorMsgJSON['ResponseMsg']['200'],
                      statuscode: 200,
                      details: null
                    })
                }
            }); 

        } catch(error) {
            
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }

    },    

    //====================================================================
    /*
        This function is written to add or update the static value of 
        payment breakdown
    */    
    setPaymentBreakDownStaticValue:(req, res, next) => {

        try {
            
            // Here, we are receiving ni
            let ni = req.body.ni ? req.body.ni : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - ni"
            });

            // Here, we are receiving vat
            let vat = req.body.vat ? req.body.vat : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - vat"
            });

            // Here, we are receiving fee
            let fee = req.body.fee ? req.body.fee : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - fee"
            });

            // avoid circular json
            if (!req.body.ni || !req.body.vat || !req.body.fee) { return; }

            EnvInfo.find(function(err, response){ // start of Find function EnvInfo

                if (err) { // When error occur
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });

                    return;
                }

                if (response) {

                    if (response.length < 1) { // Here, we have to save static values of payment breakdown
                        var envInfo = new EnvInfo({
                            _id: new mongoose.Types.ObjectId(),
                            ni: req.body.ni,
                            vat: req.body.vat,
                            fee: req.body.fee  
                        });

                        envInfo.save(function(err, result){
                            if (err) { // When error occur
                                res.json({
                                    isError: true,
                                    message:  errorMsgJSON['ResponseMsg']['404'],
                                    statuscode:  404,
                                    details: null
                                });

                                return;
                            } else {
                                res.json({
                                    isError: false,
                                    statuscode: 200,
                                    message: "VAT, NI and Fee Saved in the database successfully",
                                    detail: {
                                        ni: result.ni,
                                        vat: result.vat,
                                        fee: result.fee  
                                    }          
                                });

                                return;
                            }
                        });
                    }

                    if (response.length > 0) { // When static values of payment breakdown exist
                        let filter = { _id: response[0]._id };
                        let update = { 
                            ni: req.body.ni,
                            vat: req.body.vat,
                            fee: req.body.fee 
                        };

                        EnvInfo.findOneAndUpdate(filter, update, function(err, updateRes){

                            if (err) { // When error occur
                                res.json({
                                    isError: true,
                                    message:  errorMsgJSON['ResponseMsg']['404'],
                                    statuscode:  404,
                                    details: error
                                });

                                return;
                            }



                            if (updateRes) { // on successfull update
                                
                                EnvInfo.findOne({_id: updateRes._id}, function(err, resEnv){

                                    if (err) {
                                        res.json({
                                            isError: false,
                                            message:  errorMsgJSON['ResponseMsg']['404'],
                                            statuscode:  404,
                                            details:null
                                        }) 

                                        return;    
                                    }


                                    res.json({
                                    isError: false,
                                    message:  errorMsgJSON['ResponseMsg']['200'],
                                    statuscode:  200,
                                    details:{
                                        ni: resEnv.ni,
                                        vat: resEnv.vat,
                                        fee: resEnv.fee
                                    }, 
                                  
                                });

                            })

                                
                            }

                        });

                    }
                }
                
            }); // end of Find function EnvInfo
            
        } catch(error) {
            
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },

    //================================================================
    /*
        This function is used to fetch VAT, NI and FEE
    */    
    getPaymentBreakDownStaticValue:(req, res, next) => {
        
        try {

            EnvInfo.find()
            .select('ni vat fee')
            .exec()
            .then(result => {
                
                if (result.length>0) {
                    let response = {
                        message: "VAT, Ni and Fee successfully fetched",
                        isError: false,
                        statuscode: 200,     
                        detail: {
                            ni: result[0].ni,
                            vat: result[0].vat,
                            fee: result[0].fee
                        }    
                    };

                    res.json(response);
                } else {
                    res.json({
                        message: "VAT, Ni and Fee not exist in the system",
                        sError: false,
                        statuscode: 200,
                        detail: null
                    });
                }
            });

        } catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },

    //========================================================================
    
    
    getContact:(req, res, next)=>{
        try{
            models.contactus.findOne({}, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })  
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },
    addContact:(req, res, next)=>{
        try{
            let emailId = req.body.emailId ? req.body.emailId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Email Id"
            });
            let supportNo = req.body.supportNo ? req.body.supportNo : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Support No"
            });
            if (!req.body.emailId || !req.body.supportNo) { return; }
            let type = "contactId";
            let autocontactid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autocontactid = data;
            })
            let insertparams={
                '_id': new mongoose.Types.ObjectId(),
                'contactusId':autocontactid,
                'emailId':emailId,
                'supportNo':supportNo,
            }
            models.contactus.findOne({}, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if(searchitem==null){
                        models.contactus.create(insertparams, function (error, item) {
                            if (error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                })
                            }
                            else{
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                        })
                    }
                    else{
                        models.contactus.updateOne({'contactusId':searchitem.contactusId},{ 'emailId':emailId,'supportNo':supportNo,}, function (updatederror, updatedresponse) {
                            console.log(updatederror)
                            if(updatederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: updatederror
                                })
                            }
                            else{
                                if (updatedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }
                        })
                    }
                }
            })
           
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }
    },
    generateTocken:(req, res, next)=>{
        try{
            let type = req.body.type ? req.body.type : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Type"
            });
            let tocken = req.body.token ? req.body.token : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Token"
            });
            let id = req.body.id ? req.body.id : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Id"
            });
            if (!req.body.id || !req.body.token || !req.body.type) { return; }
            let updateWith={
                "Status.fcmTocken":tocken
            }
            let modelType;
            let updateWhere;
            if(type=='EMPLOYER'){
                modelType='employer';
                updateWhere={
                   "employerId":id
               }
            }
            else{
                modelType='candidate';
                updateWhere={
                    "candidateId":id
                }
            }
            models[modelType].updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                console.log(updatederror)
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: updatederror
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['210'],
                            statuscode: 210,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
            
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }

    },
    updateSlots:(req, res, next)=>{
        try{
            let slotId = req.body.slotId ? req.body.slotId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Slot Id"
            });
            let slotName = req.body.slotName ? req.body.slotName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Slot Name"
            });
            let starttime = req.body.starttime ? req.body.starttime : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Start Time"
            });
            let endTime = req.body.endTime ? req.body.endTime : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - End Time"
            });
            if (!req.body.slotName || !req.body.starttime || !req.body.endTime || !req.body.slotId ) { return; }
            let updatedItems={
                'slotName':slotName,
                'starttime':starttime,
                'endTime':endTime


            }
            models.timeslot.updateOne({'id':slotId},updatedItems, function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    getSlots:(req, res, next)=>{
        try{
            let slotId = req.body.slotId ? req.body.slotId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Slot Id"
            });
            models.timeslot.findOne({'id':slotId }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })  
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    addAllSlots:(req, res, next)=>{
        try{
            let globalVarObj={};
            let type = "Slot";
            let autoSlotId;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autoSlotId = data;
            })
            let slotName = req.body.slotName ? req.body.slotName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Slot Name"
            });
            let starttime = req.body.starttime ? req.body.starttime : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Start Time"
            });
            let endTime = req.body.endTime ? req.body.endTime : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - End Time"
            });
           if (!req.body.slotName || !req.body.starttime || !req.body.endTime   ) { return; }
            
           
                 
                    let insertparams=
                                {
                                    '_id': new mongoose.Types.ObjectId(),
                                     'slotName':slotName,
                                     'starttime': starttime,
                                     'endTime': endTime,
                                     'id':autoSlotId,
                                     
                                }
                               
                    models.timeslot.create(insertparams, function (error, item) {
                        if (error) {
                           
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            })
                        }
                        else{
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: item

                            })  
                        }
                    })      
                }
            catch(error){
                res.json({
                    isError: true,
                    message:  errorMsgJSON['ResponseMsg']['404'],
                    statuscode:  404,
                    details: null
                });
            }
    },
    getAboutUs:(req, res, next)=>{
       
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            if (!req.body.userType ) { return; } 
            console.log(userType)
            models.utility.findOne({'aboutUs.userType':userType }, function (error, searchitem) {
                console.log(searchitem)
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })

        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    getPolicy:(req, res, next)=>{
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            if (!req.body.userType ) { return; } 
            models.utility.findOne({'privacyPolicy.userType':userType }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })

        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    getCondition:(req, res, next)=>{
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            if (!req.body.userType ) { return; } 
            models.utility.findOne({'termsCondition.userType':userType }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })

        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }   
    },
    addCondition:(req, res, next)=>{
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            let conditionDetails = req.body.conditionDetails ? req.body.conditionDetails : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Terms & Condition Details"
            });
            if (!req.body.userType ||!req.body.conditionDetails  ) { return; }
            models.utility.findOne({'termsCondition.userType':userType }, function (error, searchitem) {
                console.log('.............',searchitem)
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if (searchitem == null) {
                        console.log('no terms & Condition is present for',userType,':insert operation starts')
                        let insertquery = {
                            'userType':userType,
                            'conditionDetails':conditionDetails
                        };
                       
                        models.utility.create({"termsCondition":insertquery}, function (error, data) {
                            if(error){
                                console.log('error during create',error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                }) 
                            }
                            else{
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: data
    
                                })   
                            }
                        })
                    }
                    else{
                        console.log('Terms And Conditions is present for',userType,':update operation starts')
                        let querywith = {
                            'userType':userType,
                            'conditionDetails':conditionDetails
                        };
                         models.utility.updateOne({'termsCondition.userType':userType},{"termsCondition":querywith}, function (updatederror, updatedresponse) {
                             if(updatederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                             }
                             else{
                                if (updatedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                } 
                             }
                            

                        })
                    }
                }
               
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }   
    },
    addPolicy:(req, res, next)=>{
       
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            let PolicyDetails = req.body.PolicyDetails ? req.body.PolicyDetails : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Privacy Policy Details"
            });
            if (!req.body.userType ||!req.body.PolicyDetails  ) { return; }
            models.utility.findOne({'privacyPolicy.userType':userType }, function (error, searchitem) {
                console.log('.............',searchitem)
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if (searchitem == null) {
                        console.log('no privacy is present for',userType,':insert operation starts')
                        let insertquery = {
                            'userType':userType,
                            'PolicyDetails':PolicyDetails
                        };
                       
                        models.utility.create({"privacyPolicy":insertquery}, function (error, data) {
                            if(error){
                                console.log('error during create',error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                }) 
                            }
                            else{
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: data
    
                                })   
                            }
                        })
                    }
                    else{
                        console.log('privacy is present for',userType,':update operation starts')
                        let querywith = {
                            'userType':userType,
                            'PolicyDetails':PolicyDetails
                        };
                         models.utility.updateOne({'privacyPolicy.userType':userType},{"privacyPolicy":querywith}, function (updatederror, updatedresponse) {
                             if(updatederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                             }
                             else{
                                if (updatedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                } 
                             }
                            

                        })
                    }
                }
               
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    addAboutUs:(req, res, next)=>{
        
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            let aboutUs = req.body.aboutUs ? req.body.aboutUs : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - About Us"
            });
            console.log(userType)
            if (!req.body.userType ||!req.body.aboutUs  ) { return; }
            models.utility.findOne({'aboutUs.userType':userType }, function (error, searchitem) {
                console.log('.............',searchitem)
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if (searchitem == null) {
                        console.log('no about us is present for',userType,':insert operation starts')
                        let insertquery = {
                            'userType':userType,
                            'aboutUsDetails':aboutUs
                        };
                       
                        models.utility.create({"aboutUs":insertquery}, function (error, data) {
                            if(error){
                                console.log('error during create',error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                }) 
                            }
                            else{
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: data
    
                                })   
                            }
                        })
                    }
                    else{
                        console.log('about us is present for',userType,':update operation starts')
                        let querywith = {
                            'userType':userType,
                            'aboutUsDetails':aboutUs
                        };
                         models.utility.updateOne({'aboutUs.userType':userType},{"aboutUs":querywith}, function (updatederror, updatedresponse) {
                             if(updatederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                             }
                             else{
                                if (updatedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                } 
                             }
                            

                        })
                    }
                }
               
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    /*
    getRole:(req, res, next)=>{
        let industryId = req.body.industryId ? req.body.industryId : res.send({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
        });
        if (!req.body.industryId) { return; }
        models.jobrole.find({'industryId':industryId}, function (err, item) {
            console.log(item)
            if (item.length==0) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1020'],
                    statuscode: 1020,
                    details: null
                })
            }
            else {
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item
                })
            }
        })
    },
    */

    getRole:(req, res, next)=>{
        let industryId = req.body.industryId ? req.body.industryId : res.send({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
        });
        if (!req.body.industryId) { return; }

        /*
            {
      "_id": "5ce552f7b3daf520ef4ecc59",
      "jobRoleName": "facility-Role4",
      "industryId": "IND1558529842701",
      "jobRoleId": "ROLE1558532855744",
      "createdAt": "2019-05-22T13:47:35.944Z",
      "updatedAt": "2019-06-04T05:47:58.242Z",
      "__v": 0,
      "skillIds": [
        "SKILL1559627214235",
        "SKILL1559627205797",
        "SKILL1559627198044"
      ],
      "industryDetail": [
        {
          "_id": "5ce54732d14953102c078aa4",
          "industryName": "Facilities Management",
          "industryId": "IND1558529842701",
          "createdAt": "2019-05-22T12:57:22.714Z",
          "updatedAt": "2020-04-23T09:55:12.442Z",
          "__v": 0
        }
      ]
    },
        */

        let aggregateQry = [
            { 
                $match: {
                    "industryId" : {
                        $in:  industryId 
                    }
                } 
            },
            {
                $lookup: {
                    from: 'industries',
                    localField: 'industryId',
                    foreignField: 'industryId',
                    as: 'industryDetail'
                }
            },

            { 
                $project: { 
                    "_id": 1, 
                    "jobRoleId": 1, 
                    "jobRoleName":1,
                    "industryId": 1, 
                    "jobRoleId": 1,
                    "jobRoleIcon": 1,
                    "createdAt": 1, 
                    "updatedAt": 1, 
                    "skillIds": 1, 
                    "industryName": "$industryDetail.industryName", 
                } 
            } 
            
            
        ];
        models.jobrole.aggregate(aggregateQry, 
            function (err, item) {
                
                if (item.length==0) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1020'],
                        statuscode: 1020,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
        })
    },

    getIndustry:(req, res, next)=>{
        try{
            models.industry.find({}, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    addIndustry:(req, res, next)=>{
        try{
            let type = "IND";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            let industryName = req.body.industryName ? req.body.industryName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Name"
            });
            if (!req.body.industryName) { return; }
            models.industry.findOne({'industryName':industryName }, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if(item){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1017'],
                            statuscode: 1017,
                            details: null
                        })  
                    }
                    else{
                        let insertquery = {
                            '_id': new mongoose.Types.ObjectId(),'industryName':industryName,
                            'industryId':autouserid
                           };
                           models.industry.create(insertquery, function (error, data) {
                                if(err){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    }) 
                                } 
                                else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: data
        
                                    })  
                                }  
                           })

                    }
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    getAllSlots:(req, res, next)=>{
        try{
            models.timeslot.find({}, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     * Get Languages
    */
    getLanguage:(req, res, next)=>{
        let mysort = {name: 1};
        try{
            models.language.find({}, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            }).sort(mysort);
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     * Verify Otp
     * First checkif ay data exists or not by checking mobileno ,and otp.
     * if exists  check isUsed true or false. if true otp is already used
     * If false its a fresh otp and verify otp is successful.
     * After that update isUsed=true so that same otp can not be used second time.
    */
    verifyOtp:(req, res, next)=>{
        try{
            let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Mobile No "
            });
            let otp = req.body.otp ? req.body.otp : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - otp "
            });
            if (!req.body.mobileNo || !req.body.otp) { return; }
            let aggrQuery = [
                {
                    $match: {
                        'mobileNo': mobileNo,
                        'otp': otp
                    }
                },
                {
                    $project: {
                        '_id': 0,
                    }
                },
                {
                    $group: {
                        _id: null,
                        isUsed: {
                            $first: "$isUsed"
                        },
                        results: {
                            $push: '$$ROOT'
                        }
                    }
                },

                {
                    $project: {
                        userDetails: {
                            $cond: {
                                if: { $eq: ['$isUsed', false] },
                                then: "$results",
                                else: []
                            }
                        }
                    }
                }
            ];
            models.timeTOLive.aggregate(aggrQuery).exec((err, item1) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    })
                } 
                else {
                    if (item1.length == 0) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    } else {
                        if (!item1[0].userDetails.length) {
                            res.json({
                                isError: true,
                                message: 'Otp is not valid.',
                                statuscode: 200,
                                details: null
                            })
                        } else {
                            models.timeTOLive.updateOne({ 'mobileNo': mobileNo }, { 'isUsed': true }, function (updatederror, updatedItem) {
                                if (updatederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (updatedItem.nModified == 1) {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: item1
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }  
    },
     /**
     * @abstract 
     * Send Otp
     *First  by mobileNo  check in authenticate collection that if record exists or not
     * If exists send otp to user given mobileno
     * After sending otp check mobileNo exists in timeToLive collection
     * If exists update otp,'isclicked':false,'isUsed':false etc in timetoLive Collection 
     * If not exists insert otp,'isclicked':false,'isUsed':false etc in timetoLive Collection 
     * It is important to set 'isUsed':false as during previously use otp was set to true to prevent the use of same otp
    */
    sendOtp:(req, res, next)=>{
        try{
            let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Mobile No "
            });
            if (!req.body.mobileNo) { return; }
            models.authenticate.findOne({'mobileNo':mobileNo}, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                        
                    });
                }
                else{
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    }
                    else{
                        var code = Math.floor((Math.random() * 999999) + 111111);
                        var finalcode = code.toString().substring(0, 6)
                        sendsmscontroller.Sendsms(mobileNo, finalcode, (err, callResponse) => {
                            if (err) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1016'],
                                    statuscode: 1016,
                                    details: null
                                })
                            }
                            if (callResponse) {
                                models.timeTOLive.findOne({'mobileNo':mobileNo}, function (findErr, findeditem) {
                                    if(findErr){
                                        res.json({
                                            isError: true,
                                            message:  errorMsgJSON['ResponseMsg']['404'],
                                            statuscode:  404,
                                            details: null
                                        });
                                    }
                                    else{
                                        if (findeditem == null) {
                                            console.log('No Record Found In TimeToLive... Its time to insert' )
                                            models.timeTOLive.create({'_id': new mongoose.Types.ObjectId(),'email':'','type':item['type'],'otp':finalcode,'tocken':'','isclicked':false,'isUsed':false,'mobileNo':mobileNo}, function (error, finalItem) {
                                                console.log('created')
                                                if(error){
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                else{
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    })
                                                }
                                            })
                                        }
                                        else{
                                            console.log('Record Found In TimeToLive... Its time to update' )
                                            models.timeTOLive.updateOne({'mobileNo':mobileNo},{'otp':finalcode,'email':'','type':item['type'],'tocken':'','isclicked':false,'isUsed':false,'mobileNo':mobileNo}, function (updatederror, updatedItem) {
                                                if(updatederror){
                                                    console.log('updated error',updatederror)
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                else {
                                                    if (updatedItem.nModified == 1) {
                                                        res.json({
                                                            isError: false,
                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                            statuscode: 200,
                                                            details: null
                                                        })
                                                    }
                                                    else{
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        }) 
                                                    }
                                                }
                                            })
                                        }
                                    }
                                }) 
                            }
                        })
                    }
                }
            })

        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }    
    },
    /**
     * @abstract 
     * Get City
     * collect countryid and filter cities according to countrId
    */
    getCity:(req, res, next)=>{
        try{
            let country_id =  req.body.country_id ? req.body.country_id : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - country_id "});
            if(!req.body.country_id ){return;}
            // models.Citie.find(
            //     {'country_id':country_id}, 
            //     {
            //         sort:{
            //             "name": 1 
            //         }
            //     },
            models.Citie.aggregate(
                [
                    { $match: { 'country_id': country_id } },
                    { $sort : { name : 1} }
                ],
                    function (err, item) {
                        if (item == null) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1008'],
                                statuscode: 1008,
                                details: null
                            })
                        }
                        else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: item
                            })
                        }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }    
    },

    /* 
        this api is written to fetch distinct city on the basis of state code
    */
   getCityDistinct:(req, res, next)=>{
    try{
            let country_id =  req.body.country_id ? req.body.country_id : res.json({ 
                isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - country_id "
            });
            
            let name =  req.body.name ? req.body.name : '';

            if(!req.body.country_id ){return;}

            let matchStatement;

            if (name == '') {
                matchStatement = {
                    $match: {
                    $and : [
                        {
                            'country_id': country_id
                        }           
                    ]          
                    }
                }; 
            } else {
                matchStatement = {
                    $match: {
                    $and : [
                        {
                            'country_id': country_id,
                            'name': { $regex: name, $options: "i" }
                        }           
                    ]          
                    }
                }; 
            }
           
            
            let aggrQuery = [
                
                matchStatement,
                {$group: { 
                     _id: {name: "$name",}, 
                     Id: {$first: "$_id"},
                     cityId: {$first: "$cityId"},
                     state_id: {$first: "$state_id"},
                     country_id: {$first: "$country_id"},
                    } 
                },
                { $sort : { '_id.name' : 1, 'cityId' : 1} },
                
                {
                    $project: {
                        "_id": "$Id",
                        "name": "$_id.name",
                        "state_id": 1,
                        "country_id": 1,
                        "cityId": 1,       
                    }
                },  

            ];
            models.Citie.aggregate(aggrQuery,
                    function (err, item) {
                        if (item == null) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1008'],
                                statuscode: 1008,
                                details: null
                            })
                        }
                        else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: item
                            })
                        }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }    
    },
   
    
    /**
     * @abstract 
     * Get Country
    */
    getCountry:(req, res, next)=>{
        try{


            let aggrQuery = [
                
                // { '$match':{'jobDetails.roleWiseAction.roleId':roleId}},
                {
                    $facet:{         
                        "allCountryExceptUK":[
                            {
                                "$project":{
                                   "countryId": 1,
                                   "sortname": 1,
                                   "name": 1,
                                   "phoneCode": 1
                                }
                            },
                            { '$match':{'name':{ $ne:'United Kingdom'}}},
                            { "$sort": { 'name': 1 } }
                        ],
                        "UK":[
                            { '$match':{'name':'United Kingdom'}},
                            {
                                "$project":{
                                   "countryId": 1,
                                   "sortname": 1,
                                   "name": 1,
                                   "phoneCode": 1
                                }
                            },
                        ]
                    }
                },
            ]

            models.Countrie.aggregate(aggrQuery).exec((err, result) => {
                
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{

                    let allCountry = [];
                    allCountry = result[0].allCountryExceptUK;
                    allCountry.unshift(result[0].UK[0]);

                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: allCountry
                    })
                }
            })


            // models.Countrie.find({}, function (err, item) {
            //     if (item == null) {
            //         res.json({
            //             isError: true,
            //             message: errorMsgJSON['ResponseMsg']['1008'],
            //             statuscode: 1008,
            //             details: null
            //         })
            //     }
            //     else {
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details: item
            //         })
            //     }
            // })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }  
    },
     /**
     * @abstract 
     * Change Password
     * First match tocken,ischecked and isused  from timeTolive collection to check if tocken is already used or not
     * If not used fetch usertype and email from timeToLive collection
     * Update new password respective userType Collection according to emailId
     * After successful updation again update 'isused=true' according to tocken from timeToLive collection
     * So that any one cannot change password with the used tocken unless and untill receiving a fresh tocken.
     * 
    */
    changePassword:(req, res, next)=>{
        let tocken =  req.body.tocken ? req.body.tocken : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Tocken "});
        let password =  req.body.password ? req.body.password : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Password "});
        if(!req.body.tocken || !req.body.password ){return;}
        try{
            models.timeTOLive.findOne({'tocken':tocken,'isclicked':true,'isUsed':false}, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1013'],
                            statuscode: 1013,
                            details: null
                        })
                    }
                    else{
                        
                        models[item['type'].toLowerCase()].updateOne({'EmailId':item['email']}, {'password':password}, function (error, response) {
                            if(error){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                if (response.nModified == 1) {
                                   
                                  models.timeTOLive.updateOne({'tocken':tocken},{'isUsed':true}, function (updatederror, updatedItem) {
                                    if(updatederror){
                                        console.log('updated error',updatederror)
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                    else {
                                        if (updatedItem.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else{
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            }) 
                                        }
                                    }
                                  })
                      
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    }) 
                                }
                            }
                        })
                    }
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Signin Candidate/Employer
     * First check user send phone or email as request
     * Then check any user (candidate/employer) exists in authenticate table
     * If exists fetch user type from authenticate table
     * Based on the type deferentiate in which table (candidate/employer) credential is matched from
     */
    signin:(req, res, next)=>{
        console.log('ok')
        let searchType;
        let emailTakenFromUser =  req.body.emailOrPhone ? req.body.emailOrPhone : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email or Phone No "});
        let password =  req.body.password ? req.body.password : res.json({ isError: true, message:  errorMsgJSON['ResponseMsg']['303'] + '- Password'});
        if(!req.body.emailOrPhone || !req.body.password ){return;}
        let email = emailTakenFromUser.toLowerCase().trim();
        utilitycontroller.checkEmailOrPhone(email,(err, data) => {
            if(err){
                res.json({
                    isError: true,
                    message: 'Invalid Format of Mobile/Email Id'
                })
            }

            switch(data){
                case '1': searchType= 'EmailId'
                        break;
                case '2':searchType= 'mobileNo'
                        break;
                default:break;
            }
           
        })
        try{
            models.authenticate.findOne({$or: [{'EmailId':email},{'mobileNo':email}]}).exec(function (err, item) {
                if(err){
                    console.log('find error',err)
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    }
                    else{
                       
                        let aggrQuery = [
                            {
                                $match: {  
                                    [searchType]: email,
                                    'password':password 
                                }
                            },
                            {
                                $project : {
                                '_id' : 0,
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    isVerified : {
                                        $first : "$Status.isVerified"
                                    },

                                    //============================
                                    isApprovedByAot : {
                                        $first : "$Status.isApprovedByAot"
                                    }, 
                                    //=============================
                                    results: {
                                        $push : '$$ROOT'
                                    }
                                }
                            },
                            {
                                $project: {
                                    userDetails: {
                                        $cond: { 
                                            if: { $eq: [ '$isVerified', true ] }, 
                                            then: "$results",
                                            else: []
                                        } 
                                    },

                                     //===================================
                                    // userDetailsApprovedByAot: {
                                    //     $cond: { 
                                    //         if: { $eq: [ '$isApprovedByAot', true ] }, 
                                    //         then: "$results",
                                    //         else: []
                                    //     } 
                                    // }
                                    //===================================
                                }
                            }
                        ];
                       
                        models[item['type'].toLowerCase()].aggregate(aggrQuery).exec((err, item1) => {
                            if(err){
                                console.log('aggregate error',err)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                })
                            }
                            else{
                                if(item1.length == 0) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1009'],
                                        statuscode: 1009,
                                        details: null
                                    })
                                } else {
                                    if (!item1[0].userDetails.length) {
                                        res.json({
                                            isError: true,
                                            message: 'User not Verified.',
                                            statuscode: 200,
                                            details: null,
                                            isVerified:false

                                        })                                        
                                    } else {

                                        // =========================================

                                        // if (!item1[0].userDetailsApprovedByAot.length) {
                                        //         res.json({
                                        //             isError: true,
                                        //             message: 'You are not approved by admin',
                                        //             statuscode: 200,
                                        //             details: null,
                                        //             isVerified:true,
                                        //             isApprovedByAot:false
                                        //         })  
                                        //         return;                                      
                                        // }
                                        //===========================================
                                        
                                        const token = token_gen({
                                            'employerId': (item1[0].userDetails[0].userType == 'EMPLOYER') ? item1[0].userDetails[0].employerId : item1[0].userDetails[0].candidateId,
                                            'userType': item1[0].userDetails[0].userType
                                        });
                                        item1[0].userDetails[0].token = token;
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['204'],
                                            statuscode: 204,
                                            details: item1[0].userDetails[0],
                                            isVerified:true,
                                            isApprovedByAot: item1[0].userDetails[0].Status.isApprovedByAot
                                        })                                        
                                    }
                                }
                            }
                        })
                    }
                }
            })
        }
        catch(error){
            console.log('outside error',error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * checkBeforeSignin
     * First fetch necessary data from sending social object
     * From authenticate table fetch if any user exists or not
     * If exist fetch value from Employer table using lookup
     * Else send blank value
     */
    checkBeforeSignin:(req, res, next)=>{
        try{
            let gloabalvarobj= {};
            let signedUpVia =  req.body.signedUpVia ? req.body.signedUpVia : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Sign up via "  });
            let signedUpObj = req.body.signedUpObj ? req.body.signedUpObj : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Social Details "  });
            if(!req.body.signedUpVia || !req.body.signedUpObj ){return;}
            switch (signedUpVia) {
                case 'facebook':if (signedUpObj.photoUrl) {
                                    gloabalvarobj.picFromSocial = signedUpObj.photoUrl;
                                } else {
                                    gloabalvarobj.picFromSocial = "";
                                }
                                gloabalvarobj.firstnameFromSocial = signedUpObj && signedUpObj.firstName ? signedUpObj.firstName : '';
                                gloabalvarobj.lastnameFromSocial = signedUpObj && signedUpObj.lastName ? signedUpObj.lastName : '';
                                gloabalvarobj.emailFromSocial = signedUpObj && signedUpObj.email ? signedUpObj.email : '';
                                gloabalvarobj.faceBookId= signedUpObj && signedUpObj.id ? signedUpObj.id : '';
                                gloabalvarobj.linkedinId= '';
                                break;
                case 'linkedin':gloabalvarobj.email='sourabhdey16@gmail.com'
                break;
                default:break;
            } 
            let aggrQuery = [
                {
                    $match: { 'EmailId': gloabalvarobj.emailFromSocial }
                },
                {
                    $project: {
                        // _id: 0,
                        type: { $toLower: "$type" },
                        EmailId: 1
                    }
                },
                {
                    $project: {
                        // _id: 0,
                        usertype: { $concat: [ "$type", "s" ] },
                        EmailId: 1
                    }
                },
                {
                    $lookup: {
                    from: "employers",
                    localField: "EmailId",
                    foreignField: "EmailId",
                    as: "employerDetails"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "EmailId",
                        foreignField: "EmailId",
                        as: "candidateDetails"
                    }
                },
                {
                    $project: {
                        userDetails: { 
                            $cond: { 
                                if: { $gt: [ { $size: "$employerDetails" }, 0 ] }, 
                                then: "$employerDetails", 
                                else: "$candidateDetails"
                            } 
                        }
                    }
                }
            ];
            models.authenticate.aggregate(aggrQuery).exec((err, item) => {
                if(item.length==0){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1009'],
                        statuscode: 1009,
                        details: null
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item[0].userDetails[0]
                    })
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    /**
    * @abstract 
    * Send verification Link via mail 
    * First check if employer exists or not by checking email in employer Collection
    * If exists create a link append with random generated tocken and employerId and send it with mail attachment
    * If mail send is successful store tocken in employer document.when link is clicked in mail '/api/common/verify-mail' is called in common.js and check if tocken is same or not. if same redirect to reset password page. 
    * After sending mail update 'isclicked==false'(that was made true during previos click) so that it canbe used.
   */
    sendConfirmationLink:(req, res, next)=>{
        let email =  req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email "});
        if(!req.body.email ){return;}
        try{
            models.authenticate.findOne({'EmailId':email}, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    }
                    else{
                        const tokgen = new TokenGenerator(512, TokenGenerator.BASE62);
                        let  rand=tokgen.generate()
                        //======================================
                        // mailercontroller.viaGmail({receiver:email,tocken:rand,key:'reset',path:'/common/verify-mail?id='},(err, data) => {
                        mailercontroller2.viaGmail({receiver:email,tocken:rand,key:'reset',path:'/common/verify-mail?id='},(err, data) => {
                        //=======================================
                            if(err){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                })
                            }
                            else{
                                models.timeTOLive.findOne({'email':email}, function (findErr, findeditem) {
                                    if(findErr){
                                        res.json({
                                            isError: true,
                                            message:  errorMsgJSON['ResponseMsg']['404'],
                                            statuscode:  404,
                                            details: null
                                        });
                                    }
                                    else{
                                        if (findeditem == null) {
                                            models.timeTOLive.create({'_id': new mongoose.Types.ObjectId(),'email':email,'type':item['type'],'tocken':rand,'isclicked':false,'isUsed':false}, function (error, finalItem) {
                                                console.log('created')
                                                if(error){
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                else{
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    })
                                                }
                                            })
                                        }
                                        else{
                                            models.timeTOLive.updateOne({'email':email},{'email':email,'type':item['type'],'tocken':rand,'isclicked':false,'isUsed':false}, function (updatederror, updatedItem) {
                                                console.log('updated')
                                                if(updatederror){
                                                    console.log('updated error',updatederror)
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                else {
                                                    if (updatedItem.nModified == 1) {
                                                        res.json({
                                                            isError: false,
                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                            statuscode: 200,
                                                            details: null
                                                        })
                                                    }
                                                    else{
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        }) 
                                                    }
                                                }
                                            })
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
   /**
     * @abstract 
     * Save Email Verification Tocken
     * First collect tocken coming with url after clicking link in email
     * Then check if this tocken is exists in timeTolive table and if this tocken is used already or no
     * by checking 'isclicked==false',
     * if satisfy redirect to reset password page otherwise redirect to error page
     * after success we have to update 'isclicked==true' so that user cannot use the tocken again
    */
    saveEmailVerificationTocken:(req, res, next)=>{
        console.log(req.query.id);
        models.timeTOLive.findOne({'tocken': req.query.id,'isclicked':false,'isUsed':false}, function (error, item) {
            if(error){
                console.log('error');
                console.log('here ut the code of redirecting alternative path');
                res.redirect(`${config.app.frontendUri}/500`);
            }
            else{
                if (item == null) {
                    console.log('candidate not exists');
                    console.log('here ut the code of redirecting alternative path');
                    res.redirect(`${config.app.frontendUri}/invalid-link`);
                } else {
                    models.timeTOLive.updateOne({'tocken': req.query.id},{'isclicked':true}, function (error, updateditem) {
                        if(error){
                            res.redirect(`${config.app.frontendUri}/500`);
                            // res.json({
                            //     isError: true,
                            //     message: errorMsgJSON['ResponseMsg']['404'],
                            //     statuscode: 404,
                            //     details: null
                            // });
                        }
                        else{
                            if (updateditem.nModified == 1) {
                              res.redirect(`${config.app.frontendUri}/reset-password/${req.query.id}`);
                              //  res.redirect('https://www.google.com/');
                            }
                            else{
                                res.redirect(`${config.app.frontendUri}/500`);
                                // res.json({
                                //     isError: true,
                                //     message: errorMsgJSON['ResponseMsg']['1004'],
                                //     statuscode: 1004,
                                //     details: null
                                // }) 
                            }
                        }
                    })
                }
            }
        })
    },
    resendConfirmationLink:(req, res, next) => {
        try{
            let email =  req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email "});
            if(!req.body.email ){return;}
            models.authenticate.findOne({'EmailId':email}, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    }
                    else{
                        const tokgen = new TokenGenerator(512, TokenGenerator.BASE62);
                        let  rand=tokgen.generate()
                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                              user: 'sourav.weavers@gmail.com',
                              pass: 'Sourav@weavers'
                            },
                            tls: {
                              rejectUnauthorized: false
                            }
                        });
                        //let link=`${config.app.FORMATTED_URL}/api/common/reverify-mail?id=${rand}&type=${item['type'].toLowerCase()}`;
                        
                        // let link=`https://api.letery.com/api/common/reverify-mail?id=${rand}&type=${item['type'].toLowerCase()}`;
                        let link=`${config.app.FORMATTED_URL_NEW}/api/common/reverify-mail?id=${rand}&type=${item['type'].toLowerCase()}`;
                        const mailoption = {
                            from: 'Seleckt<sourav.weavers@gmail.com>',
                            to: email,
                            html : "<br> Please Click on the link to verify your Account.<br><a clicktracking=off href="+link+">Click here to verify</a>" ,
                            // html : "<br> Welcome to Seleckt Staff, where you're able to fill your positions at the click of a button. Your registration is being processed and a member of our sales team will be in touch with you as soon as possible.<br><a clicktracking=off href="+link+">Click here to verify</a>" ,
                            subject:'Account Verification'
                        };
                        transporter.sendMail(mailoption, (err, info) => {
                            if(err){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1006'],
                                    statuscode: 1006,
                                    details: null
                                });
                            }
                            else{
                                let querywith = {
                                    'Status.confirmationTocken': rand,
                                };
                                var querywhere = {'EmailId': email};
                                models[item['type'].toLowerCase()].updateOne(querywhere, querywith, function (error, response) {
                                    console.log(response)
                                    if(error){
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                    else {
                                        if (response.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else{
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            }) 
                                        }
                                    }
                                })
                            }
                              
                        })
                        
                    }
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },
    revarify:(req, res, next) => {
        console.log(req.query.id)
        console.log(req.query.type)
        var querywhere = {'Status.confirmationTocken':req.query.id};
        models[req.query.type].findOne(querywhere, function (error, item) {
            if(error){
                console.log('error');
                console.log('here ut the code of redirecting alternative path');
                res.redirect(`${config.app.frontendUri}/500`);
            }
            else{
                if (item == null) {
                    console.log('candidate not exists');
                    console.log('here ut the code of redirecting alternative path');
                    res.redirect(`${config.app.frontendUri}/invalid-link`);
                } else {
                 if(item.Status.confirmationTocken===req.query.id){
                    console.log('same tocken')
                    let querywith={'Status.isVerified':true};
                    models[req.query.type].updateOne(querywhere, querywith, function (error, response) {
                        if(error){
                            res.redirect(`${config.app.frontendUri}/500`);
                           
                        }
                        else {
                            if (response.nModified == 1) {
                                // res.redirect(`${config.app.frontendUri}/account-verified`);
                                res.redirect(`${config.app.frontendUri}`);
                                
                            }
                            else{
                                res.redirect(`${config.app.frontendUri}/500`);
                               
                            }
                        }
                    })
                   
                 } else {
                     console.log('not same tocken');
                     console.log('here ut the code of redirecting alternative path');
                     res.redirect(`${config.app.frontendUri}/invalid-link`);
                 }
                }
            }
        })
    },

    verifyToken:(req, res, next) => {
        console.log('verify tocken')
        res.json({
            isError: false,
            message: 'Token verified',
            statuscode: 200,
            details: null
        })
    }
}