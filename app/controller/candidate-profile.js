const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/mailer');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const base64controller = require('../service/ImageUploadBase64');
const config =  require('../config/env');

//===================================================
const employerJobBusiness = require("../businessLogic/employer-job-business");
//===================================================

async function checkCandidatePresenceForApi(candidateId, jobId){
    var isCandidateAvailable = true;
    let candidateJobDetailList = [];

    let fetchCandidateJobScheduleStatus = await employerJobBusiness.fetchCandidateJobSchedule(candidateId);
    
    candidateJobDetailList = fetchCandidateJobScheduleStatus.response;
    let fetchJobTimeDetailStatus = await employerJobBusiness.fetchJobTimeDetail(jobId);

    let jobStartTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.startTimeStamp : 0;
    let jobSecondTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.secondTimeStamp : 0;

    let jobActualStartTime = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.actualStartTime : 0;
    jobActualStartTime = Number(jobActualStartTime);

    let jobActualEndTime = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.actualEndTime : 0;
    jobActualEndTime = Number(jobActualEndTime);

    //Get Date of the job
    var d = new Date(jobStartTimeStamp);
    var fullYrJob = d.getFullYear();
    var monthJob = d.getMonth();
    var dayJob = d.getDay();
   
    //************ */

    let i;
    let startTimeStamp;
    let secondTimeStamp;

    let actualStartTime;
    let actualEndTime;

    for (i = 0; i < candidateJobDetailList.length; i++) {
        
        startTimeStamp = candidateJobDetailList[i].startTimeStamp;
        secondTimeStamp = candidateJobDetailList[i].secondTimeStamp;

        actualStartTime = candidateJobDetailList[i].actualStartTime;
        actualStartTime = Number(actualStartTime);

        actualEndTime = candidateJobDetailList[i].actualEndTime;
        actualEndTime = Number(actualEndTime);

        //Get Date of each job of candidate
        var d = new Date(startTimeStamp);
        var fullYr = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDay();
        
        // Here we comparing the date
        if ( (jobStartTimeStamp >= startTimeStamp) && (jobStartTimeStamp <= secondTimeStamp)) {
            isCandidateAvailable = false; // candidate not available
            break;
        }

        if ( (jobSecondTimeStamp >= startTimeStamp) && (jobSecondTimeStamp <= secondTimeStamp)) {
            isCandidateAvailable = false; // candidate not available
            break;
        }

       //==END=====

        if (isCandidateAvailable == true) {  // comparing time
            if ((fullYr == fullYrJob) && (month == monthJob) && (day == dayJob)) {
                if ( (jobActualStartTime >= actualStartTime) && (jobActualStartTime <= actualEndTime)) {
                    isCandidateAvailable = false; // candidate not available
                    break;
                }
        
                if ( (jobActualEndTime >= actualStartTime) && (jobActualEndTime <= actualEndTime)) {
                    isCandidateAvailable = false; // candidate not available
                    break;
                }
            } // END  if ((fullYr == fullYrJob) && (month == monthJob) && (day == dayJob)) {
        } // END if (isCandidateAvailable == false) {
    } // END  for (i = 0; i < candidateJobDetailList.length; i++) {


    return isCandidateAvailable;

} // END function sendNotificationEmail(candidateId, jobId){


// const candidateProfileBusiness = require("../businessLogic/candidate-profile-business");

module.exports = {
    /*****************************************************
     * ADDING INFORMATION SEPARATELY FOT CANDIDATE PROFILE
     *****************************************************/

    /*
        This api is written to fetch candidate profile
    */
    viewCandidateProfile: async (req, res, next) => {
        try{

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });

            if (!req.body.candidateId) { return; }

            aggrQuery = [
                { $match : { 'candidateId': candidateId } } ,
                {
                    "$project":{
                        "_id": 0,
                        "candidateId":1,
                        "workExperience":1, 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1
                    }
                },

                {
                    "$project":{
                        "_id": 0,
                        "candidateId":1,
                        "workExperience":1, 
                        "experiences":"$workExperience.experiences", 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1
                    }
                },

                { $unwind: "$experiences" },

                {
                    "$project":{
                        "_id": 0,
                        "candidateId":1,
                        "experiences":1, 
                        "jobRole":"$experiences.jobRole", 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1
                    }
                },

              

                {
                    $lookup: {
                        from: "jobroles",
                        localField: "jobRole",
                        foreignField: "jobRoleId",
                        as: "jobRoleDetails"
                    }
                },

                {
                    "$project":{
                        "_id": 0,
                        "candidateId":1,
                        "experiences":1, 
                        "jobRole":"$experiences.jobRole", 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1,
                        "jobRoleDetails": { $arrayElemAt: [ "$jobRoleDetails", 0 ] }
                    }
                },

                {
                    "$project":{
                        "_id": 0,
                        "candidateId":1,
                        "experiences":1, 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1,
                        "jobRoleDetails": 1
                    }
                },
                
                {
                    $group: {
                        "_id":null,
                        "experiences": { $addToSet: "$experiences" },
                        "candidateId": { $first: "$candidateId" },
                        "geolocation": { $first: "$geolocation" }, 
                        "fname": { $first: "$fname" }, 
                        "mname": { $first: "$mname" }, 
                        "lname": { $first: "$lname" }, 
                        "EmailId":  { $first: "$EmailId" }, 
                        "userType":  { $first: "$userType" }, 
                        "memberSince":  { $first: "$memberSince" }, 
                        "distance":  { $first: "$distance" }, 
                        "dayoff":  { $first: "$dayoff" }, 
                        "docs":  { $first: "$docs" }, 
                        "dob":  { $first: "$dob" }, 
                        "kinDetails":  { $first: "$kinDetails" }, 
                        "location":  { $first: "$location" }, 
                        "secondaryEmailId":  { $first: "$secondaryEmailId" }, 
                        "email_address":  { $first: "$email_address" }, 
                        "home_address":  { $first: "$home_address" }, 
                        "payloadSkill":  { $first: "$payloadSkill" }, 
                        "selectedRole":  { $first: "$selectedRole" }, 
                        "skills":  { $first: "$skills" },
                        "language":   { $first: "$language" }, 
                        "favouriteBy":  { $first: "$favouriteBy" },
                        "availability":  { $first: "$availability" },
                        "jobRoleDetails": { $push: "$jobRoleDetails" }
                    }
                },
                
                {
                    "$project":{
                        "_id": 0,
                        "candidateId":1,
                        "experiences":1, 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1,
                        "jobRoleDetails": 1,
                    }
                },
                
                { $unwind: "$payloadSkill" },

                {
                    $lookup: {
                        from: "industries",
                        localField: "payloadSkill.industryId",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },

                {
                    "$project":{
                        // "_id": 0,
                        "candidateId":1,
                        "experiences":1, 
                        "geolocation":1, 
                        "fname":1, 
                        "mname":1, 
                        "lname":1, 
                        "EmailId":1, 
                        "userType":1, 
                        "memberSince":1, 
                        "distance":1, 
                        "dayoff":1, 
                        "docs":1, 
                        "dob":1, 
                        "kinDetails":1, 
                        "location":1, 
                        "secondaryEmailId":1, 
                        "email_address":1, 
                        "home_address":1, 
                        "payloadSkill":1, 
                        "selectedRole":1, 
                        "skills":1,
                        "language": 1, 
                        "favouriteBy":1,
                        "availability":1,
                        "jobRoleDetails": 1,
                        "industryDetails": { $arrayElemAt: [ "$industryDetails", 0 ] }
                    }
                },


                {
                    $group: {
                        "_id":null,
                        "experiences": { $first: "$experiences" },
                        "candidateId": { $first: "$candidateId" },
                        "geolocation": { $first: "$geolocation" }, 
                        "fname": { $first: "$fname" }, 
                        "mname": { $first: "$mname" }, 
                        "lname": { $first: "$lname" }, 
                        "EmailId":  { $first: "$EmailId" }, 
                        "userType":  { $first: "$userType" }, 
                        "memberSince":  { $first: "$memberSince" }, 
                        "distance":  { $first: "$distance" }, 
                        "dayoff":  { $first: "$dayoff" }, 
                        "docs":  { $first: "$docs" }, 
                        "dob":  { $first: "$dob" }, 
                        "kinDetails":  { $first: "$kinDetails" }, 
                        "location":  { $first: "$location" }, 
                        "secondaryEmailId":  { $first: "$secondaryEmailId" }, 
                        "email_address":  { $first: "$email_address" }, 
                        "home_address":  { $first: "$home_address" }, 
                        "payloadSkill":  { $addToSet: "$payloadSkill" }, 
                        "selectedRole":  { $first: "$selectedRole" }, 
                        "skills":  { $first: "$skills" },
                        "language":   { $first: "$language" }, 
                        "favouriteBy":  { $first: "$favouriteBy" },
                        "availability":  { $first: "$availability" },
                        "jobRoleDetails": { $first: "$jobRoleDetails" },
                        "industryDetails": { $push: "$industryDetails" },

                    }
                },
                
            ];

            

            models.candidate.aggregate(aggrQuery, 
                function (err, data) {	
                if (err) {
                    
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });

                    return;
                } else { 
                
                    if (data.length < 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        });
    
                        return;
                    }

                   
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })

                } 
            });    



        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }  
    },


    /*
        Delete Availability
    */
    deleteAvailability: (req, res, next) => {
        try{

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let availabilityId = req.body.availabilityId ? req.body.availabilityId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - availability Id",
                isError: true
            });

            let Timeslot = req.body.Timeslot ? req.body.Timeslot : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Timeslot",
                isError: true
            });

            let TimeSlotArr = [];

            if (!req.body.candidateId || !req.body.availabilityId || !req.body.Timeslot) { return; }

            aggrQuery = [
                { $match : { 'candidateId': candidateId } } ,
                {
                    "$project":{
                        "_id": 0,
                        "availability":1,
                    }
                },
                {   $unwind: "$availability" },
                { $match : { 'availability._id': mongoose.Types.ObjectId(availabilityId) } } ,
                
                {
                    "$project":{
                        "Timeslot":"$availability.Timeslot",
                        "ID":"$availability._id",
                    }
                },
                
            ];

            models.candidate.aggregate(aggrQuery).exec( (error, data) => {
                if (error) {
                    
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });

                    return;
                } else { 

                    if (data.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        });
    
                        return;
                    }

                    let prevTimeSlot = data[0].Timeslot
                    let timeSlotToInsert = [];
                    
                    let tag1;
                    for (tag1 = 0; tag1 < prevTimeSlot.length; tag1++) {
                        if ( prevTimeSlot[tag1] != Timeslot) {
                            console.log(prevTimeSlot[tag1]);
                            timeSlotToInsert.push(prevTimeSlot[tag1]);
                        }
                    }

                        models.candidate.updateOne({"candidateId":candidateId,"availability._id": availabilityId},
                        { "$set": { "availability.$.Timeslot": timeSlotToInsert }}, 
                        function (updatederror, updatedresponse) {
                                if (updatederror) {
                                    res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                    })
                                } else {
                                    res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                    })
                                }
                        });    
                }    
            });    

            

            
        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }   
    },

    /**
       Delete Dayoff     
    */
    deleteDayOff: (req, res, next) => {
        try{

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let dayOffId = req.body.dayOffId ? req.body.dayOffId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Day Off Id",
                isError: true
            });

            if (!req.body.candidateId || !req.body.dayOffId) { return; }

            models.candidate.updateOne({"candidateId":candidateId},
                { "$pull": { "dayoff": { "_id": dayOffId } }}, 
                //{ safe: true, multi:true }, function (updatederror, updatedresponse) {
                { safe: true }, function (updatederror, updatedresponse) {
                    if (updatederror) {
                        res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                        })
                    } else {
                        res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: null
                        })
                    }
            });    

        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }   
    },


     /**
     * @abstract 
     * Add DayOff
    */  
    deleteCandidateWorkExp: (req, res, next) => {
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let expId = req.body.expId ? req.body.expId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Experience Id",
                isError: true
            });

            if (!req.body.candidateId || !req.body.expId) { return; }
            
            models.candidate.updateOne({"candidateId":candidateId},
                { "$pull": { "workExperience.experiences": { "expId": expId } }}, 
                //{ safe: true, multi:true }, function (updatederror, updatedresponse) {
                { safe: true }, function (updatederror, updatedresponse) {
                if (updatederror) {
                    res.json({
                      isError: true,
                      message: errorMsgJSON['ResponseMsg']['1004'],
                      statuscode: 1004,
                      details: null
                    })
                } else {
                    res.json({
                      isError: false,
                      message: errorMsgJSON['ResponseMsg']['200'],
                      statuscode: 200,
                      details: null
                    })
                }
            });    


        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }   
    },

    addDayOff:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let dayoff = req.body.dayoff ? req.body.dayoff : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Day off",
                isError: true
            });
            
            if (!req.body.candidateId) { return; }
            let updateWhere={
                'candidateId':candidateId
            }
          
            let updateWith = {"dayoff":dayoff};
            models.candidate.updateOne(updateWhere,updateWith , function (updatederror, updatedresponse) {
                if(updatederror){
                    console.log(updatederror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
            
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Add Availability
    */                 
    addAvailability:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let availability = req.body.availability ? req.body.availability : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Availability",
                isError: true
            });
            
        
            if (!req.body.candidateId) { return; }
            let updateWhere={
                'candidateId':candidateId
            }
            let updateWith = {"availability":availability};
            models.candidate.updateOne(updateWhere,updateWith , function (updatederror, updatedresponse) {
                if(updatederror){
                    console.log(updatederror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
        
    },
    /**
     * @abstract 
     * Add Attachments for candidate
    */
    addAttachments:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let cv = req.body.cv;
            let video = req.body.video;
            let documents = req.body.documents;
            if (!req.body.candidateId) { return; }
            let updateWith = {"cv":cv,"video":video,"docs":documents};
            let updateWhere={
                'candidateId':candidateId
            }
            models.candidate.updateOne(updateWhere,updateWith , function (updatederror, updatedresponse) {
                if(updatederror){
                    console.log(updatederror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Add Location for candidate
    */
    addLocation:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let location = req.body.location ? req.body.location : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Location"
            });
            let distance = req.body.distance ? req.body.distance : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - distance"
            });

            let locationName = req.body.locationName ? req.body.locationName : '';
           
            if (!req.body.candidateId) { return; }
            let updateWith = {
                "geolocation":location,
                "distance":distance,
            };

            let updateWhere={
                'candidateId':candidateId
            }
            models.candidate.updateOne(updateWhere,updateWith , function (updatederror, updatedresponse) {
                if(updatederror){
                    console.log(updatederror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Add Selected Roles for candidate
    */
    
    addSelectedRoles: async (req, res, next)=>{
        try{

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });

            let payloadSkill = req.body.payloadSkill ? req.body.payloadSkill : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Details",
                isError: true
            });

            

            if (!req.body.candidateId || !req.body.payloadSkill) { return; }

            let skills = [];
            let selectedRole = [];
            let selectedSkills = [];

            let i;
            let j;
           
            // this function is used to add role for a particular skill Detail
            async function removeSkillDetail() { 
                // console.log(jobRoleId);
                return new Promise(function(resolve, reject){
                    models.candidate.update(
                        { 'candidateId': candidateId },
                        { $unset: 
                            { 
                                payloadSkill: "",
                                skills: "",
                                selectedRole: ""
                            } 
                        },            
                            function (updatederror, updatedresponse) {
                                if (updatederror) {
                                    resolve({   
                                        "ErrorStatus": true,
                                    });

                                } else {

                                    resolve({
                                        "ErrorStatus": false,
                                    });
                                }          
                    }) // END models.candidate.updateOne 
                }) // END of  return new Promise(function(resolve, reject)  
            } // start of removeSkillDetail

            //this function is used to add role for a particular skill Detail
            async function addSkillDetailJobInfo(skillDEtailToPush) { 
                
                return new Promise(function(resolve, reject){
                    models.candidate.updateOne({ 'candidateId': candidateId }, 
                        { $push: { "payloadSkill": skillDEtailToPush } }, 
                            function (updatederror, updatedresponse) {
                                if (updatederror) {
                                    resolve({   
                                        "ErrorStatus": true,
                                    });

                                } else {

                                    resolve({
                                        "ErrorStatus": false,
                                    });
                                }          
                    }) // END models.candidate.updateOne 
                }) // END of  return new Promise(function(resolve, reject)  
            } // start of addSkillDetail

            let removeSkillDetailStatus;
            let addSkillDetailStatus;
            
            let l;
            let skillDEtailToPush;
            /* 
            for (i = 0; i < payloadSkill.length; i++) {
                selectedRole.push(payloadSkill[i].jobRoleId);
               
                removeSkillDetailStatus = await removeSkillDetail();
                
                skillDEtailToPush = {
                    'jobRoleId': payloadSkill[i].jobRoleId,
                    'jobRoleName': payloadSkill[i].jobRoleName,
                    'industryId': payloadSkill[i].industryId,
                    'skillDetails': payloadSkill[i].skillDetails
                }

                addSkillDetailJobInfoStatus = await addSkillDetailJobInfo(skillDEtailToPush);    

                for (l = 0; l < payloadSkill[i].skillDetails.length; l++) {
                    skills.push(payloadSkill[i].skillDetails[l].skillId);
                }            
            }
            */
            removeSkillDetailStatus = await removeSkillDetail();
            let skillDetailsToInsert = [];

            for (i = 0; i < payloadSkill.length; i++) {
                selectedRole.push(payloadSkill[i].jobRoleId);
                
                skillDetailsToInsert = [];
                for (l = 0; l < payloadSkill[i].skillDetails.length; l++) {
                    if (payloadSkill[i].skillDetails[l].isSelected == true) {
                        skills.push(payloadSkill[i].skillDetails[l].skillId);
                        skillDetailsToInsert.push(payloadSkill[i].skillDetails[l]);     
                    }

                    // if (payloadSkill[i].skillDetails[l].isSelected == false) { 
                    //     payloadSkill[i].skillDetails.splice(l, 1);
                    // }     
                } // END for (l = 0; l < payloadSkill[i].skillDetails.length; l++) { 

                skillDEtailToPush = {
                    'jobRoleId': payloadSkill[i].jobRoleId,
                    'jobRoleName': payloadSkill[i].jobRoleName,
                    'industryId': payloadSkill[i].industryId,
                    // 'skillDetails': payloadSkill[i].skillDetails
                    'skillDetails': skillDetailsToInsert
                } 

                addSkillDetailJobInfoStatus = await addSkillDetailJobInfo(skillDEtailToPush);        
            } // END for (i = 0; i < payloadSkill.length; i++) {


        
            let updateWhere = {
                'candidateId':candidateId,                
            }

            models.candidate.updateOne(updateWhere,{$addToSet:   
                {"skills" : skills,"selectedRole": selectedRole}} , function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                        
                        
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
           

        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },    



    /*
        THis api is written by sourav but this has to be modified
    addSelectedRoles: async (req, res, next)=>{
        try{
            let selectedRole = req.body.roles ? req.body.roles : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Chosen Roles",
                isError: true
            }); 
            let skills = req.body.skills ? req.body.skills : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Chosen skills",
                isError: true
            }); 
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            if (!req.body.candidateId || !req.body.roles || !req.body.skills) { return; }
            // console.log(req.body);
            // return;
            // // this function is used to add role for a particular candidate
            // async function addRole(roleToAdd) { 
            //     return new Promise(function(resolve, reject){
            //         models.candidate.updateOne({ 'candidateId': candidateId }, 
            //             { $addToSet: { "selectedRole": roleToAdd } }, 
            //             function (updatederror, updatedresponse) {
            //                 if (updatederror) {
            //                     resolve({   
            //                         "ErrorStatus": true,
            //                     });

            //                 } else {

            //                     resolve({
            //                         "ErrorStatus": false,
            //                     });
            //                 }
                    
            //         }) 
                      
            //     }) // END of  return new Promise(function(resolve, reject)  
            // } // start of addRole

            let i;
            let roleToAdd;
            let addRoleStatus;
            let insertRoleStatus = [];
            let roleInfo;
            
            let updateWhere = {
                'candidateId':candidateId,                
            }

            models.candidate.updateOne(updateWhere,{$addToSet:   
                {"skills" : skills,"selectedRole": selectedRole}} , function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                        
                        
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
            

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    */
    
    /**
     * @abstract 
     * Add Experience
    */
    addExperience:(req, res, next)=>{
        let workObj = req.body.workObj ? req.body.workObj : res.send({
            message: errorMsgJSON['ResponseMsg']['303'] + " - work Object",
            isError: true,
        });
        let candidateId=workObj['candidateId']
        let experiences=workObj['experiences']
        
        let lastObjindex=experiences.length-1;
        let firstObjDate=(experiences[0].startDate).substring(0, 10);
        let lastObjDate=(experiences[lastObjindex].endDate).substring(0, 10);
       
        dt1 = new Date(firstObjDate);
        dt2 = new Date(lastObjDate);
        let days= Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
        console.log('.......',days)



        // experiences.map(a=>{
        //     if(a.startDate>a.endDate  ){
        //         console.log(1)
        //         res.json({
        //             isError: true,
        //             message: 'joining date is greater than enddate',
        //             statuscode: 1003,
        //             details: null
        //         })
        //         return;
        //     }
           
        // })
       
        models.candidate.findOne({'candidateId':candidateId }, function (err, item) {
            console.log(2)
            console.log('.......',item)
            if (item == null) {
                console.log('null')
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1003'],
                    statuscode: 1003,
                    details: null
                })
            }
            else {
                models.candidate.update({"candidateId":candidateId}, { $set: { "workExperience.experiences": [],"workExperience.expInMonths":'' }}, function(err, affected){
                    if(err){
                        res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                        })
                    }
                    else{
                        if (affected.nModified == 1) {
                            models.candidate.updateOne({"candidateId":candidateId},{$push:{"workExperience.experiences":experiences},"workExperience.expInMonths":days }, function (error, response1) {
                                if (response1.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                    
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                               })
                        }
                        else{
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                    }
                })
               
              
            }
        })
    },
   
    /**
     * @abstract 
     * Add Languages for candidate profile 
     * after collecting all languages in form of objects,
     * make previous allocated languages array emply by applying "$pull"
     * then push new langeages
     * n.b--> previous allocated languages will not be lost as by using 
     * get languages api old languages willbe append with new ones
    */
    addLanguage:(req, res, next)=>{
        try{
            let languageObj = req.body.languageObj ? req.body.languageObj : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - language Object",
                isError: true,
            });
            let candidateId=languageObj['candidateId']
            let languages=languageObj['languages']
            models.candidate.findOne({'candidateId':candidateId }, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else{
                        models.candidate.update({"candidateId":candidateId}, { $set: { "language": [] }}, function(err, affected){
                                if(err){
                                    res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                    })
                                }
                                else{
                                    if (affected.nModified == 1) {
                                        models.candidate.updateOne({"candidateId":candidateId},{$push:{"language":languages} }, function (error, response1) {
                                            console.log(response1)
                                            if(error){
                                                console.log(error)
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })
                                            }
                                            else{
                                                if (response1.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    })
                                                }
                                                else {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                            
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                } 
            })
            
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Add Personal Details
    */
    addPersonalDetails:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let dob = req.body.dob ? req.body.dob : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate date of birth",
                isError: true,
            });
            let EmailId = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - EmailId",
                isError: true,
            });
            let city = req.body.city ? req.body.city : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - City",
                isError: true,
            });
            let country = req.body.country ? req.body.country : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Country",
                isError: true,
            });
            let postCode = req.body.postCode ? req.body.postCode : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - PostCode",
                isError: true,
            });
            let address = req.body.address ? req.body.address : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Address",
                isError: true,
            });
            let contactNo = req.body.contactNo ? req.body.contactNo : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - ContactNo",
                isError: true,
            });
            let kinName = req.body.kinName ? req.body.kinName : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - kinName",
                isError: true,
            });
            let kinContactNo = req.body.kinContactNo ? req.body.kinContactNo : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - kinContactNo",
                isError: true,
            });
            let relationWithKin = req.body.relationWithKin ? req.body.relationWithKin : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Relation With Kin",
                isError: true,
            });
            let fname = req.body.firstname ? req.body.firstname : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - firstname",
                isError: true,
            });
            let lname = req.body.lastname ? req.body.lastname : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - lastname",
                isError: true,
            });
            let email_address = req.body.email_address ? req.body.email_address : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - email_address",
                isError: true,
            });
            let home_address = req.body.home_address ? req.body.home_address : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - lastname",
                isError: true,
            });
            
            if (!req.body.candidateId || !req.body.dob || !req.body.EmailId || !req.body.city ||!req.body.country ||!req.body.postCode ||!req.body.address ||!req.body.contactNo ||!req.body.kinName || !req.body.kinContactNo || !req.body.relationWithKin) { return; }
            if (!req.body.email_address || !req.body.home_address) { return; } 
            
            /**
             * here during entry of contact no and emailid  the unique ness of the mentioned two are not done 
             * changing mail and contact should reflect in timetolive and authentication table. not done yet. need discustion
             * here my idea is asking user if he keep caontact/email id as primary so that old email during registration will override with new one Or keep the new email/contact as secondary one
             * if user keerp it as primary one then old email/contact no do not work during login or reset password.
             * ANOTHER solution is remove emailid and contact no field
             * */
            models.candidate.findOne({'candidateId':candidateId }, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else{
                        let insertQuery={
                            'dob':dob,
                            'fname':fname,
                            'lname':lname,
                            'email_address':email_address,
                            'home_address':home_address,
                            'location.city':city,
                            'location.country':country,
                            'location.postCode':postCode, 
                            'location.address':address,
                            'secondaryEmailId':EmailId,
                            'contactNo':contactNo,
                            'kinDetails.kinName':kinName,
                            'kinDetails.kinContactNo':kinContactNo,
                            'kinDetails.relationWithKin':relationWithKin
                        }

                        models.candidate.updateOne({'candidateId':candidateId },insertQuery, function (error, item) {
                            if (error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                })
                            }
                            else{
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['201'],
                                    statuscode: 201,
                                    details: null
                                })
                            }
                        })

                    }
                }
            })

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

     /**
     * @abstract 
     * Profile Picture Upload
     * First check  candidate exist or not using empId
     * if exists upload pic in s3-bucket using multer
     * then store image link in candidate collection
    */
    profilePicUpload:(req, res, next)=>{
        
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let profilePic = req.body.profilePic ? req.body.profilePic : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Profile Picture",
                isError: true,
            });
            if (!req.body.candidateId || !req.body.profilePic) { return; }
            let querywhere = { 'candidateId': candidateId };
            models.candidate.findOne(querywhere, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {

                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else{
                        let updatewith = { 'profilePic': profilePic };
                        models.candidate.updateOne(querywhere, updatewith, function (error, response) {
                            if (error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                if (response.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    

    /*****************************************************
     * MODIFICATION AND FETCHING FOR CANDIDATE PROFILE
     *****************************************************/ 

     /**
     * @abstract 
     * Delete Document By Id
     * 
    */
    deleteDocById:(req, res, next)=>{
    try{
        let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
            message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
            isError: true
        }); 
        let docId = req.body.docId ? req.body.docId : res.json({
            message: errorMsgJSON['ResponseMsg']['303'] + " - Document Id",
            isError: true
        }); 
        if (!req.body.candidateId || !req.body.docId) { return; }
        let updateWhere={
            'candidateId':candidateId
        }
        models.candidate.updateOne(updateWhere,{ "$pull": { "docs": { "docId": docId } }}, { safe: true, multi:true }, function (updatederror, updatedresponse) {
            if(updatederror){
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1004'],
                    statuscode: 1004,
                    details: null
                })
            }
            else{
                if (updatedresponse.nModified == 1) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
            }
        })
    }
    catch (error) {
        res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['404'],
            statuscode: 404,
            details: null
        });
    }
    },
    /**
     * @abstract 
     * Delete Experience
     * pull specific experience in "workExperience.experiences" array by using expId and
     * CandidateId
    */
    deleteExperience:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let expId = req.body.expId ? req.body.expId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - experience Id",
                isError: true
            }); 
            if (!req.body.candidateId || !req.body.expId) { return; }
            let updateWhere={
                'candidateId':candidateId,
                
            }
            models.candidate.updateOne(updateWhere,{ "$pull": { "workExperience.experiences": { "expId": expId } }}, { safe: true, multi:true }, function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Update Experience
     * Based on candidateId and expId update specific experience in "workExperience.experiences" array
    */
    updateExperience:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let expId = req.body.expId ? req.body.expId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - experience Id",
                isError: true
            }); 
            let jobRole = req.body.jobRole ? req.body.jobRole : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " -Job Role",
                isError: true
            }); 
            let description = req.body.description ? req.body.description : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Description",
                isError: true
            }); 
            let startDate = req.body.startDate ? req.body.startDate : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Start Date",
                isError: true
            }); 
            let endDate = req.body.endDate ? req.body.endDate : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - End Date",
                isError: true
            }); 
            let companyName = req.body.companyName ? req.body.companyName : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Company Name",
                isError: true
            }); 
            let referenceName = req.body.referenceName ? req.body.referenceName : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Reference Name",
                isError: true
            });
            let referenceContact = req.body.referenceContact ? req.body.referenceContact : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Reference Contact",
                isError: true
            });  
            if ( !req.body.referenceContact||!req.body.referenceName||!req.body.companyName ||!req.body.endDate ||!req.body.startDate ||!req.body.description||!req.body.jobRole||!req.body.candidateId || !req.body.expId) { return; }
            let updateWith={
                'workExperience.experiences.$.jobRole':jobRole,
                'workExperience.experiences.$.description':description,
                'workExperience.experiences.$.startDate':startDate,
                'workExperience.experiences.$.endDate':endDate,
                'workExperience.experiences.$.companyName':companyName,
                'workExperience.experiences.$.referenceName':referenceName,
                'workExperience.experiences.$.referenceContact':referenceContact,
            }
            let updateWhere={
                'candidateId':candidateId,
                "workExperience.experiences.expId":expId,
            }
            models.candidate.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
         }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Get Experience Details by Exp Id
    */
    editExperience:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true
            });
            let expId = req.body.expId ? req.body.expId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - experience Id",
                isError: true
            }); 
            if (!req.body.candidateId || !req.body.expId) { return; }
            let findWhere={
                "candidateId":candidateId,
                "workExperience.experiences.expId":expId,

            }
            models.candidate.findOne(findWhere,{ 'workExperience.experiences.$': 1 }, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }

            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Get Candidate pre Selected Languages
    */
    getSelectedLanguage:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            if (!req.body.candidateId) { return; }
            models.candidate.findOne({'candidateId':candidateId }, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details:item.language
                        })
                    }
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getCandidateDetailsWithStatus: async (req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : '';

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            //if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
            if (!req.body.jobId || !req.body.candidateId ){return;}

            // Here, we are checking whether the candidate is available or not
            let isAvailable = await checkCandidatePresenceForApi(candidateId, jobId)
            console.log("isAvailable",isAvailable)

            let aggrQuery

            aggrQuery = [
                { $match : { 'candidateId': candidateId } } ,
                {
                    $facet:{
                        "invitationToApplyJobs":[
                            { $unwind: {path:"$invitationToApplyJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$invitationToApplyJobs.roleId",
                                    "employerId": "$invitationToApplyJobs.employerId",
                                    "jobId": "$invitationToApplyJobs.jobId",
                                    "candidateId": "$invitationToApplyJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "appliedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$appliedJobs.jobRoleName",
                                    "jobRoleIcon" : "$appliedJobs.jobRoleIcon",
                                    "industryId":"$appliedJobs.industryId",
                                    "jobRoleId":"$appliedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    _id:0,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobDetails.jobType",
                                    "payType":"$jobDetails.jobDetails.payType",
                                    "pay":"$jobDetails.jobDetails.pay",
                                    "noOfStuff":"$jobDetails.jobDetails.noOfStuff",
                                    "location":"$jobDetails.location.coordinates"
                                }
                            }
                        ],
                        "hiredJobs":[
                            { $unwind: {path:"$hiredJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$hiredJobs.roleId",
                                    "employerId": "$hiredJobs.employerId",
                                    "jobId": "$hiredJobs.jobId",
                                    "candidateId": "$hiredJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "appliedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$appliedJobs.jobRoleName",
                                    "jobRoleIcon" : "$appliedJobs.jobRoleIcon",
                                    "industryId":"$appliedJobs.industryId",
                                    "jobRoleId":"$appliedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    _id:0,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobDetails.jobType",
                                    "payType":"$jobDetails.jobDetails.payType",
                                    "pay":"$jobDetails.jobDetails.pay",
                                    "noOfStuff":"$jobDetails.jobDetails.noOfStuff",
                                    "location":"$jobDetails.location.coordinates"
                                }
                            },
                        ],
                        "appliedJobs":[
                            { $unwind: {path:"$appliedJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$appliedJobs.roleId",
                                    "employerId": "$appliedJobs.employerId",
                                    "jobId": "$appliedJobs.jobId",
                                    "candidateId": "$appliedJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "appliedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$appliedJobs.jobRoleName",
                                    "jobRoleIcon" : "$appliedJobs.jobRoleIcon",
                                    "industryId":"$appliedJobs.industryId",
                                    "jobRoleId":"$appliedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    _id:0,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobDetails.jobType",
                                    "payType":"$jobDetails.jobDetails.payType",
                                    "pay":"$jobDetails.jobDetails.pay",
                                    "noOfStuff":"$jobDetails.jobDetails.noOfStuff",
                                    "location":"$jobDetails.location.coordinates"
                                }
                            },
                        ],
                        "declinedJobs":[
                            { $unwind: {path:"$declinedJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$declinedJobs.roleId",
                                    "employerId": "$declinedJobs.employerId",
                                    "jobId": "$declinedJobs.jobId",
                                    "candidateId": "$declinedJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "declinedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    declinedJobs : { $arrayElemAt :["$declinedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$declinedJobs.jobRoleName",
                                    "jobRoleIcon" : "$declinedJobs.jobRoleIcon",
                                    "industryId":"$declinedJobs.industryId",
                                    "jobRoleId":"$declinedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },

                          
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobDetails.jobType",
                                    "payType":"$jobDetails.jobDetails.payType",
                                    "pay":"$jobDetails.jobDetails.pay",
                                    "jobType":"$jobDetails.jobDetails.jobType",
                                    "noOfStuff":"$jobDetails.jobDetails.noOfStuff",
                                    "location":"$jobDetails.location.coordinates"
                                }
                            },
                        ],
                        "acceptedJobs":[
                        { $unwind: {path:"$acceptedJobs",preserveNullAndEmptyArrays:false}},
                        {
                            "$project":{
                                "_id" :0,
                                "roleId": "$acceptedJobs.roleId",
                                "employerId": "$acceptedJobs.employerId",
                                "jobId": "$acceptedJobs.jobId",
                                "candidateId": "$acceptedJobs.candidateId",
                                "location":"$acceptedJobs.location",
                                "noOfStuff":"$acceptedJobs.noOfStuff",
                                "pay":"$acceptedJobs.pay",
                                "payType":"$acceptedJobs.payType",
                                "jobType":"$acceptedJobs.jobType"

                            }
                        },
                        {
                            $lookup: {
                                from: "jobroles",
                                localField: "roleId",
                                foreignField: "jobRoleId",
                                as: "acceptedJobs"
                            }
                        },
                        {
                            "$project":{
                                location: 1,
                                noOfStuff: 1,
                                pay: 1,
                                payType: 1,
                                jobType: 1,
                                jobId:1,
                                employerId:1,
                                candidateId:1,
                                acceptedJobs : { $arrayElemAt :["$acceptedJobs", 0]}
                            }
                        },
                        {
                            "$project":{
                                "location":"$location",
                                "noOfStuff":"$noOfStuff",
                                "pay":"$pay",
                                "payType":"$payType",
                                "jobType":"$jobType",
                                "jobRoleName" : "$acceptedJobs.jobRoleName",
                                "jobRoleIcon" : "$acceptedJobs.jobRoleIcon",
                                "industryId":"$acceptedJobs.industryId",
                                "jobRoleId":"$acceptedJobs.jobRoleId",
                                "jobId":"$jobId",
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                            }
                        },
                        {
                            $lookup: {
                                from: "industries",
                                localField: "industryId",
                                foreignField: "industryId",
                                as: "industryDetails"
                            }
                        },
                        {
                            "$project":{
                                location: 1,
                                noOfStuff: 1,
                                pay: 1,
                                payType: 1,
                                jobType: 1,
                                jobRoleName:1,
                                jobRoleIcon:1,
                                industryId:1,
                                jobRoleId:1,
                                jobId:1,
                                employerId:1,
                                candidateId:1,
                                industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                
                            }
                        },
                        {
                            "$project":{
                                "location":"$location",
                                "noOfStuff":"$noOfStuff",
                                "pay":"$pay",
                                "payType":"$payType",
                                "jobType":"$jobType",
                                "jobRoleName" : "$jobRoleName",
                                "jobRoleIcon" : "$jobRoleIcon",
                                "industryId":"$industryId",
                                "jobRoleId":"$jobRoleId",
                                "jobId":"$jobId",
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                                "industryName":"$industryDetails.industryName"
                            }
                        },
                        {
                            $lookup: {
                                from: "jobs",
                                localField: "jobId",
                                foreignField:"jobId",
                                as: "jobDetails"
                            }
                        },
                        {
                            "$project":{
                                location: 1,
                                noOfStuff: 1,
                                pay: 1,
                                payType: 1,
                                jobType: 1,
                                jobRoleName:1,
                                jobRoleIcon:1,
                                industryId:1,
                                jobRoleId:1,
                                jobId:1,
                                employerId:1,
                                candidateId:1,
                                industryName:1,
                                jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                
                            }
                        },
                        {
                            "$project":{
                                "jobRoleName" : "$jobRoleName",
                                "jobRoleIcon" : "$jobRoleIcon",
                                "industryId":"$industryId",
                                "jobRoleId":"$jobRoleId",
                                "jobId":"$jobId",
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                                "industryName":"$industryName",
                                "description":"$jobDetails.description",
                                "jobType":"$jobDetails.jobDetails.jobType",
                                "payType":"$jobDetails.jobDetails.payType",
                                "pay":"$jobDetails.jobDetails.pay",
                                "jobType":"$jobDetails.jobDetails.jobType",
                                "noOfStuff":"$jobDetails.jobDetails.noOfStuff",
                                "location":"$jobDetails.location.coordinates"
                            }
                        },
                       ],
                        "jobOffers":[
                            { $unwind: {path:"$jobOffers",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$jobOffers.roleId",
                                    "employerId": "$jobOffers.employerId",
                                    "jobId": "$jobOffers.jobId",
                                    "candidateId": "$jobOffers.candidateId",
                                    "location":"$jobOffers.location",
                                    "noOfStuff":"$jobOffers.noOfStuff",
                                    "pay":"$jobOffers.pay",
                                    "payType":"$jobOffers.payType",
                                    "jobType":"$jobOffers.jobType"


                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "jobOffers"
                                }
                            },
                            {
                                "$project":{
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    jobOffers : { $arrayElemAt :["$jobOffers", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobOffers.jobRoleName",
                                    "jobRoleIcon" : "$jobOffers.jobRoleIcon",
                                    "industryId":"$jobOffers.industryId",
                                    "jobRoleId":"$jobOffers.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "location":"$location",
                                    "noOfStuff":"$noOfStuff",
                                    "pay":"$pay",
                                    "payType":"$payType",
                                    "jobType":"$jobType"
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "location":"$location",
                                    "noOfStuff":"$noOfStuff",
                                    "pay":"$pay",
                                    "payType":"$payType",
                                    "jobType":"$jobType",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobDetails.jobType",
                                    "payType":"$jobDetails.jobDetails.payType",
                                    "pay":"$jobDetails.jobDetails.pay",
                                    "noOfStuff":"$jobDetails.jobDetails.noOfStuff",
                                    "location":"$jobDetails.location.coordinates",
                                    "description":"$jobDetails.description",
                                }
                            },
                            
                        ],
                        "locationDetails":[
                            {
                                $lookup: {
                                    from: "countries",
                                    localField: "location.country",
                                    foreignField: "countryId",
                                    as: "location.country"
                                }
                            },
                            {
                                $lookup: {
                                    from: "cities",
                                    localField: "location.city",
                                    foreignField: "cityId",
                                    as: "location.cityDetails"
                                }
                            },
                            {
                                $project : {
                                    address : "$location.address",
                                    city : {
                                        $arrayElemAt :["$location.cityDetails", 0]
                                    },
                                    country :{
                                        $arrayElemAt :["$location.country", 0]
                                    },
                                    postCode: "$location.postCode"
                                }
                            }
                        ],
                        "selRoleDetails":[                            
                            { $unwind: {path:"$jobOffers",preserveNullAndEmptyArrays:true}},
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "selectedRole",
                                    foreignField: "jobRoleId",
                                    as: "roleDetails"
                                }
                            }, 
                            {
                                "$project":{
                                   roleDetails : { $arrayElemAt :["$roleDetails", 0]}
                                }
                            },
                            {
                              $project :{
                                _id:0,
                                jobRoleId : "$roleDetails.jobRoleId",
                                jobRoleName : "$roleDetails.jobRoleName",
                                  jobRoleIcon : "$roleDetails.jobRoleIcon",
                                industryId : "$roleDetails.industryId",
                              }  
                            }                        
                        ],
                        "workExpDetails":[
                            {
                                $project : {
                                    "workExperience" : 1
                                }
                            },
                            { $unwind: {path:"$workExperience.experiences",preserveNullAndEmptyArrays:true}},
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "workExperience.experiences.jobRole",
                                    foreignField: "jobRoleId",
                                    as: "roleDetails"
                                }
                            },
                           
                            {
                              "$project":{
                                "workExperience.expInMonths" : 1,
                                "workExperience.experiences.referenceEmailAddress" : 1,
                                "workExperience.experiences.industryId" : 1,
                                "workExperience.experiences.expId" : 1,
                                "workExperience.experiences.description" : 1,
                                "workExperience.experiences.startDate" : 1,
                                "workExperience.experiences.companyName" : 1,
                                "workExperience.experiences.referenceName" : 1,
                                "workExperience.experiences.referenceContact" : 1,
                                "workExperience.experiences.endDate" : 1,
                                "workExperience.experiences.roleDetails" : { $arrayElemAt :["$roleDetails", 0]}
                                }
                            },
                            {
                              "$project":{
                                // "_id":0,
                                "expInMonths" : "$workExperience.expInMonths",
                                "experiences.referenceEmailAddress":"$workExperience.experiences.referenceEmailAddress",
                                "experiences.industryId" : "$workExperience.experiences.industryId",
                                "experiences.expId" : "$workExperience.experiences.expId",
                                "experiences.description" : "$workExperience.experiences.description",
                                "experiences.startDate" : "$workExperience.experiences.startDate",
                                "experiences.companyName" : "$workExperience.experiences.companyName",
                                "experiences.referenceName" : "$workExperience.experiences.referenceName",
                                "experiences.referenceContact" : "$workExperience.experiences.referenceContact",
                                "experiences.endDate" : "$workExperience.experiences.endDate",
                                "experiences.roleDetails" : "$workExperience.experiences.roleDetails"
                                }
                            },
                            {
                                $group : {
                                    _id : null,
                                    "expInMonths": {$first: "$expInMonths"},
                                     experiences : {
                                            $addToSet : {
                                                "industryId":"$experiences.industryId",
                                               "referenceEmailAddress":"$experiences.referenceEmailAddress",
                                                "expId":"$experiences.expId",
                                                "description":"$experiences.description",
                                                "startDate":"$experiences.startDate",
                                                "companyName":"$experiences.companyName",
                                                "referenceName":"$experiences.referenceName",
                                                "referenceContact":"$experiences.referenceContact",
                                                "endDate":"$experiences.endDate",
                                                "roleDetails":"$experiences.roleDetails",                                                                                               
                                            }
                                        }
                                    }                                
                                }
                          
                        ],
                        "availDetails":[
                            {
                                $project : {
                                    "availability" : 1
                                }
                            },
                            { $unwind: {path:"$availability",preserveNullAndEmptyArrays:false}},
                            { $unwind: {path:"$availability.Timeslot",preserveNullAndEmptyArrays:false}},
                            {
                                $lookup: {
                                    from: "timeslots",
                                    localField: "availability.Timeslot",
                                    foreignField: "id",
                                    as: "availability.timeSlotDetails"
                                }
                            },
                            {
                                $project : {
                                    _id : 0,
                                    TimeslotId : "$availability.Timeslot",
                                    day : "$availability.day",
                                    timeSlotDetails : {
                                        $arrayElemAt : ["$availability.timeSlotDetails",0]
                                    }
                                }
                            },
                            {
                                $group :{
                                    _id : "$day",
                                    "day": {
                                        $first: "$day"
                                    },
                                    "Timeslot": {
                                        $push: "$timeSlotDetails.id"
                                    },
                                    timeSlotDetails : {
                                        $addToSet : {
                                            "slotName":"$timeSlotDetails.slotName",
                                            "starttime":"$timeSlotDetails.starttime",
                                            "endTime":"$timeSlotDetails.endTime",
                                            "id":"$timeSlotDetails.id",
                                        }
                                    }
                                }
                            }
                        ],
                        "pureData":[
                            {"$project" : {
                                "_id" : 0,
                            }}
                        ],

                        "payloadSkills":[
                            {
                                "$project" : 
                                    {
                                        "_id" : 0,
                                        "payloadSkill": 1
                                    }
                            },
                            {
                                $lookup: {
                                    from: 'industries',			        
                                    localField: 'payloadSkill.industryId',
                                    foreignField: 'industryId',
                                    as: 'industryDetail'
                                }
                            },
                        ]
                    }
                },
                {
                    $project : {
                        invitationToApplyJobs:"$invitationToApplyJobs",
                        hiredJobs:"$hiredJobs",
                        appliedJobs:"$appliedJobs",
                        declinedJobs:"$declinedJobs",
                        acceptedJobs:"$acceptedJobs",
                        jobOffers:"$jobOffers",
                        locationDetails : "$locationDetails",
                        selRoleDetails : "$selRoleDetails",
                        workExpDetails : { $arrayElemAt : ["$workExpDetails",0]},
                        availDetails : "$availDetails",
                        pureData : { $arrayElemAt : ["$pureData",0]},
                        // pureData: "$pureData",
                        payloadSkills: "$payloadSkills"
                    }
                },
                {
                    $project : {
                        "_id":"$pureData._id",
                        "profilePic":"$pureData.profilePic",
                        "Status":"$pureData.Status",
                        "trainingDetails":"$pureData.trainingDetails",
                        "declinedJobs":"$declinedJobs",
                        "invitationToApplyJobs":"$invitationToApplyJobs",
                        "hiredJobs":"$hiredJobs",
                        "acceptedJobs":"$acceptedJobs",
                        "jobOffers":"$jobOffers",
                        "appliedJobs":"$appliedJobs",
                        // "pureData": "$pureData",
                        "selectedJobList":"$pureData.selectedJobList",
                        "candidateId":"$pureData.candidateId",
                        "fname":"$pureData.fname",
                        "mname":"$pureData.mname",
                        "lname":"$pureData.lname",
                        "rating":"$pureData.rating",
                        "email_address":"$pureData.email_address",
                        "home_address":"$pureData.home_address",
                        "EmailId":"$pureData.EmailId",
                        "password":"$pureData.password",
                        "userType":"$pureData.userType",
                        "memberSince":"$pureData.memberSince",
                        "createdAt":"$pureData.createdAt",
                        "updatedAt":"$pureData.updatedAt",
                        "contactNo":"$pureData.contactNo",
                        "dob":"$pureData.dob",
                        "kinDetails":"$pureData.kinDetails",
                        "location":{$arrayElemAt : ["$locationDetails",0]}, //
                        "secondaryEmailId":"$pureData.secondaryEmailId",
                        "dayoff":"$pureData.dayoff",
                        "cv":"$pureData.cv",
                        "docs":"$pureData.docs",
                        "video":"$pureData.video",
                        "distance":"$pureData.distance",
                        "geolocation":"$pureData.geolocation",
                        
                        "selectedRole":"$selRoleDetails", //
                        "workExperience":"$workExpDetails", //
                        "language":"$pureData.language",
                        "availability":"$availDetails", //
                        "payloadSkills": "$payloadSkills"
                    }
                }
                
 ]

            
            models.candidate.aggregate(aggrQuery).exec( (error, data) => {
               
                 
                if(error){
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (data == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else {
                        let aggrQuery1

                        if (roleId != "") {
                            // console.log("sec l1");
                        aggrQuery1 = [
                            { '$match': { $and: 
                                [ 
                                    { 'jobDetails.roleWiseAction.roleId': roleId },
                                    { 'jobId': jobId },
                                ]
                             }
                            },
                            { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                            { '$match':{'jobDetails.roleWiseAction.roleId':roleId}},
                            {
                                $facet:{
                                    "generalInfo":[
                                        {
                                            "$project":{
                                               
                                                "jobDetails" : 1,
                                                "jobId": 1,
                                                "employerId": 1,
                                                "approvedTo":1,
                                            }
                                        },
            
                                        {
                                            "$project":{
                                                "_id":0,
                                                "roleId" : "$jobDetails.roleWiseAction.roleId",
                                                "jobId" : "$jobId",
                                                "employerId" : "$employerId",
                                                "approvedTo":1,
                                                
                                            }
                                        },
                                        
                                        {
                                            "$project":{
                                                "_id":0,
                                                "roleId" : "$roleId",
                                                "jobId" : "$jobId",
                                                "employerId" : "$employerId",
                                                "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: {$and:[{ $eq: ["$$approvedTo.roleId", "$roleId"] },{ $eq: ["$$approvedTo.candidateId", candidateId] } ]} } },
                                            }
                                        },
                                        {
                                            "$project":{
                                                "_id":0,
                                               
                                                "roleId" : 1,
                                                "jobId" : 1,
                                                "employerId" : 1,
                                                "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                            }
                                        },
                                        {
                                            "$project":{
                                               
                                                "roleId" : 1,
                                                "jobId" : 1,
                                                "employerId" : 1,
                                                "candidateId":candidateId,
                                                "isOffered":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: "candidates",
                                                localField: "candidateId",
                                                foreignField: "candidateId",
                                                as: "candidateDetails"
                                            }
                                        },
                                        {
                                            "$project":{
                                                roleId:1,
                                                jobId : 1,
                                                employerId : 1,
                                                candidateId : 1,
                                                isOffered:1,
                                                candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":"$roleId",
                                                "jobId":"$jobId",
                                                "employerId":"$employerId",
                                                "candidateId":"$candidateId",
                                                "isOffered":"$isOffered",
                                                "declinedJobs":"$candidateDetails.declinedJobs",
                                                "acceptedJobs":"$candidateDetails.acceptedJobs",
                                                "appliedJobs":"$candidateDetails.appliedJobs",
                                                "hiredJobs":"$candidateDetails.hiredJobs"
                                                
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":1,
                                                "jobId":1,
                                                "employerId":1,
                                                "candidateId":1,
                                                "isOffered":1,
                                                "appliedJobsArray":{ $filter: { input: "$appliedJobs", as: "appliedJobs", cond: {$and:[{ $eq: ["$$appliedJobs.roleId", "$roleId"] },{ $eq: ["$$appliedJobs.jobId", "$jobId"] } ]} } },
                                                "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: {$and:[{ $eq: ["$$declinedJobs.roleId", "$roleId"] },{ $eq: ["$$declinedJobs.jobId", "$jobId"] } ]} } },
                                                "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: {$and:[{ $eq: ["$$acceptedJobs.roleId", "$roleId"] },{ $eq: ["$$acceptedJobs.jobId", "$jobId"] }  ]} } },
                                                "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: {$and:[{ $eq: ["$$hiredJobs.roleId", "$roleId"] },{ $eq: ["$$hiredJobs.jobId", "$jobId"] }  ]} } },
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":1,
                                                "jobId":1,
                                                "employerId":1,
                                                "candidateId":1,
                                                "isOffered":1,
                                                "appliedJobsArray": { $arrayElemAt :["$appliedJobsArray", 0]},
                                                "declinedJobsArray": { $arrayElemAt :["$declinedJobsArray", 0]},
                                                "acceptedJobsArray": { $arrayElemAt :["$acceptedJobsArray", 0]},
                                                "hiredJobsArray": { $arrayElemAt :["$hiredJobsArray", 0]}
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":1,
                                                "jobId":1,
                                                "employerId":1,
                                                "candidateId":1,
                                              
                                                "isOffered":1,
                                                "isApplied":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$appliedJobsArray.roleId", "$roleId"] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                                "isHired":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                                "isDeclined":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                                "isAccepted":{
                                                    $cond: {
                                                        "if": {$or:[{ "$eq": [ "$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                              
                                            }
                                        }
                                    ],
                                   
                                }
                            },
                            {
                                $project : {
                                    generalInfo:{ $arrayElemAt : ["$generalInfo",0]},
                                }
                            }
                        ]
                    } else {
                       // console.log("sec l2");
                       aggrQuery1 = [
                            { '$match': { $and: 
                                [ 
                                    
                                    { 'jobId': jobId },
                                ]
                             }
                            },
                            { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                            // { '$match':{'jobDetails.roleWiseAction.roleId':roleId}},
                            {
                                $facet:{
                                    "generalInfo":[
                                        {
                                            "$project":{
                                               
                                                "jobDetails" : 1,
                                                "jobId": 1,
                                                "employerId": 1,
                                                "approvedTo":1,
                                            }
                                        },
            
                                        {
                                            "$project":{
                                                "_id":0,
                                                "roleId" : "$jobDetails.roleWiseAction.roleId",
                                                "jobId" : "$jobId",
                                                "employerId" : "$employerId",
                                                "approvedTo":1,
                                                
                                            }
                                        },
                                        
                                        {
                                            "$project":{
                                                "_id":0,
                                                "roleId" : "$roleId",
                                                "jobId" : "$jobId",
                                                "employerId" : "$employerId",
                                                "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: {$and:[{ $eq: ["$$approvedTo.roleId", "$roleId"] },{ $eq: ["$$approvedTo.candidateId", candidateId] } ]} } },
                                            }
                                        },
                                        {
                                            "$project":{
                                                "_id":0,
                                               
                                                "roleId" : 1,
                                                "jobId" : 1,
                                                "employerId" : 1,
                                                "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                            }
                                        },
                                        {
                                            "$project":{
                                               
                                                "roleId" : 1,
                                                "jobId" : 1,
                                                "employerId" : 1,
                                                "candidateId":candidateId,
                                                "isOffered":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: "candidates",
                                                localField: "candidateId",
                                                foreignField: "candidateId",
                                                as: "candidateDetails"
                                            }
                                        },
                                        {
                                            "$project":{
                                                roleId:1,
                                                jobId : 1,
                                                employerId : 1,
                                                candidateId : 1,
                                                isOffered:1,
                                                candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":"$roleId",
                                                "jobId":"$jobId",
                                                "employerId":"$employerId",
                                                "candidateId":"$candidateId",
                                                "isOffered":"$isOffered",
                                                "declinedJobs":"$candidateDetails.declinedJobs",
                                                "acceptedJobs":"$candidateDetails.acceptedJobs",
                                                "appliedJobs":"$candidateDetails.appliedJobs",
                                                "hiredJobs":"$candidateDetails.hiredJobs"
                                                
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":1,
                                                "jobId":1,
                                                "employerId":1,
                                                "candidateId":1,
                                                "isOffered":1,
                                                "appliedJobsArray":{ $filter: { input: "$appliedJobs", as: "appliedJobs", cond: {$and:[{ $eq: ["$$appliedJobs.roleId", "$roleId"] },{ $eq: ["$$appliedJobs.jobId", "$jobId"] } ]} } },
                                                "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: {$and:[{ $eq: ["$$declinedJobs.roleId", "$roleId"] },{ $eq: ["$$declinedJobs.jobId", "$jobId"] } ]} } },
                                                "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: {$and:[{ $eq: ["$$acceptedJobs.roleId", "$roleId"] },{ $eq: ["$$acceptedJobs.jobId", "$jobId"] }  ]} } },
                                                "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: {$and:[{ $eq: ["$$hiredJobs.roleId", "$roleId"] },{ $eq: ["$$hiredJobs.jobId", "$jobId"] }  ]} } },
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":1,
                                                "jobId":1,
                                                "employerId":1,
                                                "candidateId":1,
                                                "isOffered":1,
                                                "appliedJobsArray": { $arrayElemAt :["$appliedJobsArray", 0]},
                                                "declinedJobsArray": { $arrayElemAt :["$declinedJobsArray", 0]},
                                                "acceptedJobsArray": { $arrayElemAt :["$acceptedJobsArray", 0]},
                                                "hiredJobsArray": { $arrayElemAt :["$hiredJobsArray", 0]}
                                            }
                                        },
                                        {
                                            "$project":{
                                                "roleId":1,
                                                "jobId":1,
                                                "employerId":1,
                                                "candidateId":1,
                                              
                                                "isOffered":1,
                                                "isApplied":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$appliedJobsArray.roleId", "$roleId"] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                                "isHired":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                                "isDeclined":{
                                                    $cond: {
                                                        "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                                "isAccepted":{
                                                    $cond: {
                                                        "if": {$or:[{ "$eq": [ "$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                                        "then": true,
                                                         "else": false
                                                    }
                                                },
                                              
                                            }
                                        }
                                    ],
                                   
                                }
                            },
                            {
                                $project : {
                                    generalInfo:{ $arrayElemAt : ["$generalInfo",0]},
                                }
                            }
                        ]

                    } // if (roleId != "") {
                        models.job.aggregate(aggrQuery1).exec((err, result) => {
                            console.log(err)
                            if(err){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                data[0].isAvailable = isAvailable;
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    status: result[0],
                                    details: data[0]
                                })
                            }
                        })  
                       
                    }
                }
               
            })
           
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    getCandidateDetails:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            if (!req.body.candidateId) { return; }
            let aggrQuery = [
                { $match : { 'candidateId': candidateId } } ,
                {
                    $facet:{
                        "invitationToApplyJobs":[
                            { $unwind: {path:"$invitationToApplyJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$invitationToApplyJobs.roleId",
                                    "employerId": "$invitationToApplyJobs.employerId",
                                    "jobId": "$invitationToApplyJobs.jobId",
                                    "candidateId": "$invitationToApplyJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "appliedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                                }
                            },
                           
                            {
                                "$project":{
                                    "jobRoleName" : "$appliedJobs.jobRoleName",
                                    "jobRoleIcon" : "$appliedJobs.jobRoleIcon",
                                    "industryId":"$appliedJobs.industryId",
                                    "jobRoleId":"$appliedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    _id:0,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails":{$filter: {
                                        input: '$jobDetails.jobDetails.roleWiseAction',
                                        as: 'item',
                                        cond: {$eq: ['$$item.roleId', '$jobRoleId']}
                                }},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                        
                                }
                            },

                            
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails": { $arrayElemAt :["$jobDetails", 0]},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                   
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobType",
                                    "payType":"$jobDetails.payType",
                                    "ratePerHour":"$jobDetails.payment.ratePerHour",
                                    "noOfStuff":"$jobDetails.payment.noOfStuff",
                                    "location":"$jobDetails.location.coordinates",
                                    "locationName":"$jobDetails.locationName",
                                    "isDeleted":"$jobDetails.isDeleted"
                                }
                            },
                        ],
                        "hiredJobs":[
                            { $unwind: {path:"$hiredJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$hiredJobs.roleId",
                                    "employerId": "$hiredJobs.employerId",
                                    "jobId": "$hiredJobs.jobId",
                                    "candidateId": "$hiredJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "appliedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$appliedJobs.jobRoleName",
                                    "jobRoleIcon" : "$appliedJobs.jobRoleIcon",
                                    "industryId":"$appliedJobs.industryId",
                                    "jobRoleId":"$appliedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    _id:0,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                             
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails":{$filter: {
                                        input: '$jobDetails.jobDetails.roleWiseAction',
                                        as: 'item',
                                        cond: {$eq: ['$$item.roleId', '$jobRoleId']}
                                }},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                   
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails": { $arrayElemAt :["$jobDetails", 0]},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "jobRoleNameExist": {
                                        $ifNull: [ '$jobRoleName', false ]
                                    }
                                   
                                }
                            },

                            {
                                "$match": {
                                    "jobRoleNameExist":{ $ne: false } 
                                }
                            },

                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobType",
                                    "payType":"$jobDetails.payType",
                                    "ratePerHour":"$jobDetails.payment.ratePerHour",
                                    "noOfStuff":"$jobDetails.payment.noOfStuff",
                                    "location":"$jobDetails.location.coordinates",
                                    "locationName":"$jobDetails.locationName"
                                }
                            },
                        ],
                        "appliedJobs":[
                            { $unwind: {path:"$appliedJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$appliedJobs.roleId",
                                    "employerId": "$appliedJobs.employerId",
                                    "jobId": "$appliedJobs.jobId",
                                    "candidateId": "$appliedJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "appliedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$appliedJobs.jobRoleName",
                                    "jobRoleIcon" : "$appliedJobs.jobRoleIcon",
                                    "industryId":"$appliedJobs.industryId",
                                    "jobRoleId":"$appliedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    _id:0,
                                    jobRoleName:1,
                                    jobRoleIcon: 1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails":{$filter: {
                                        input: '$jobDetails.jobDetails.roleWiseAction',
                                        as: 'item',
                                        cond: {$eq: ['$$item.roleId', '$jobRoleId']}
                                }},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                   
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails": { $arrayElemAt :["$jobDetails", 0]},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "jobRoleNameExist": {
                                        $ifNull: [ '$jobRoleName', false ]
                                    }
                                   
                                }
                            },

                            {
                                "$match": {
                                    "jobRoleNameExist":{ $ne: false } 
                                }
                            },


                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobType",
                                    "payType":"$jobDetails.payType",
                                    "ratePerHour":"$jobDetails.payment.ratePerHour",
                                    "noOfStuff":"$jobDetails.payment.noOfStuff",
                                    "location":"$jobDetails.location.coordinates",
                                    "locationName":"$jobDetails.locationName"
                                }
                            },
                           
                        ],
                        "declinedJobs":[
                            { $unwind: {path:"$declinedJobs",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :0,
                                    "roleId": "$declinedJobs.roleId",
                                    "employerId": "$declinedJobs.employerId",
                                    "jobId": "$declinedJobs.jobId",
                                    "candidateId": "$declinedJobs.candidateId"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "declinedJobs"
                                }
                            },
                            {
                                "$project":{
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    declinedJobs : { $arrayElemAt :["$declinedJobs", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$declinedJobs.jobRoleName",
                                    "jobRoleIcon" : "$declinedJobs.jobRoleIcon",
                                    "industryId":"$declinedJobs.industryId",
                                    "jobRoleId":"$declinedJobs.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },

                          
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon: 1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                    
                                }
                            },
                            
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails":{$filter: {
                                        input: '$jobDetails.jobDetails.roleWiseAction',
                                        as: 'item',
                                        cond: {$eq: ['$$item.roleId', '$jobRoleId']}
                                }},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                   
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails": { $arrayElemAt :["$jobDetails", 0]},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "jobRoleNameExist": {
                                        $ifNull: [ '$jobRoleName', false ]
                                    }
                                   
                                }
                            },

                            {
                                "$match": {
                                    "jobRoleNameExist":{ $ne: false } 
                                }
                            },

                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobType",
                                    "payType":"$jobDetails.payType",
                                    "ratePerHour":"$jobDetails.payment.ratePerHour",
                                    "noOfStuff":"$jobDetails.payment.noOfStuff",
                                    "location":"$jobDetails.location.coordinates",
                                    "locationName":"$jobDetails.locationName"
                                }
                            },
                        ],
                        "acceptedJobs":[
                        { $unwind: {path:"$acceptedJobs",preserveNullAndEmptyArrays:false}},
                        {
                            "$project":{
                                "_id" :0,
                                "roleId": "$acceptedJobs.roleId",
                                "employerId": "$acceptedJobs.employerId",
                                "jobId": "$acceptedJobs.jobId",
                                "candidateId": "$acceptedJobs.candidateId",
                                "location":"$acceptedJobs.location",
                                "noOfStuff":"$acceptedJobs.noOfStuff",
                                "pay":"$acceptedJobs.pay",
                                "payType":"$acceptedJobs.payType",
                                "jobType":"$acceptedJobs.jobType"

                            }
                        },
                        {
                            $lookup: {
                                from: "jobroles",
                                localField: "roleId",
                                foreignField: "jobRoleId",
                                as: "acceptedJobs"
                            }
                        },
                        {
                            "$project":{
                                location: 1,
                                noOfStuff: 1,
                                pay: 1,
                                payType: 1,
                                jobType: 1,
                                jobId:1,
                                employerId:1,
                                candidateId:1,
                                acceptedJobs : { $arrayElemAt :["$acceptedJobs", 0]}
                            }
                        },
                        {
                            "$project":{
                                "location":"$location",
                                "noOfStuff":"$noOfStuff",
                                "pay":"$pay",
                                "payType":"$payType",
                                "jobType":"$jobType",
                                "jobRoleName" : "$acceptedJobs.jobRoleName",
                                "jobRoleIcon" : "$acceptedJobs.jobRoleIcon",
                                "industryId":"$acceptedJobs.industryId",
                                "jobRoleId":"$acceptedJobs.jobRoleId",
                                "jobId":"$jobId",
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                            }
                        },
                        {
                            $lookup: {
                                from: "industries",
                                localField: "industryId",
                                foreignField: "industryId",
                                as: "industryDetails"
                            }
                        },
                        {
                            "$project":{
                                location: 1,
                                noOfStuff: 1,
                                pay: 1,
                                payType: 1,
                                jobType: 1,
                                jobRoleName:1,
                                jobRoleIcon:1,
                                industryId:1,
                                jobRoleId:1,
                                jobId:1,
                                employerId:1,
                                candidateId:1,
                                industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                
                            }
                        },
                        {
                            "$project":{
                                "location":"$location",
                                "noOfStuff":"$noOfStuff",
                                "pay":"$pay",
                                "payType":"$payType",
                                "jobType":"$jobType",
                                "jobRoleName" : "$jobRoleName",
                                "jobRoleIcon" : "$jobRoleIcon",
                                "industryId":"$industryId",
                                "jobRoleId":"$jobRoleId",
                                "jobId":"$jobId",
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                                "industryName":"$industryDetails.industryName"
                            }
                        },
                        {
                            $lookup: {
                                from: "jobs",
                                localField: "jobId",
                                foreignField:"jobId",
                                as: "jobDetails"
                            }
                        },
                        {
                            "$project":{
                                location: 1,
                                noOfStuff: 1,
                                pay: 1,
                                payType: 1,
                                jobType: 1,
                                jobRoleName:1,
                                jobRoleIcon:1,
                                industryId:1,
                                jobRoleId:1,
                                jobId:1,
                                employerId:1,
                                candidateId:1,
                                industryName:1,
                                jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                            }
                        },
                        {
                            "$project":{
                                "locationName":"$jobDetails.locationName",
                                "jobRoleName" : "$jobRoleName",
                                "jobRoleIcon" : "$jobRoleIcon",
                                "industryId":"$industryId",
                                "jobRoleId":"$jobRoleId",
                                "jobId":"$jobId",
                                "jobDetails":{$filter: {
                                    input: '$jobDetails.jobDetails.roleWiseAction',
                                    as: 'item',
                                    cond: {$eq: ['$$item.roleId', '$jobRoleId']}
                            }},
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                                "industryName":"$industryName",
                               
                            }
                        },
                        {
                            "$project":{
                                "locationName":"$jobDetails.locationName",
                                "jobRoleName" : "$jobRoleName",
                                "jobRoleIcon" : "$jobRoleIcon",
                                "industryId":"$industryId",
                                "jobRoleId":"$jobRoleId",
                                "jobId":"$jobId",
                                "jobDetails": { $arrayElemAt :["$jobDetails", 0]},
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                                "industryName":"$industryName",
                                "jobRoleNameExist": {
                                    $ifNull: [ '$jobRoleName', false ]
                                }
                               
                            }
                        },

                        {
                            "$match": {
                                "jobRoleNameExist":{ $ne: false } 
                            }
                        },

                        {
                            "$project":{
                                "locationName":"$jobDetails.locationName",
                                "jobRoleName" : "$jobRoleName",
                                "jobRoleIcon" : "$jobRoleIcon",
                                "industryId":"$industryId",
                                "jobRoleId":"$jobRoleId",
                                "jobId":"$jobId",
                                "employerId":"$employerId",
                                "candidateId":"$candidateId",
                                "industryName":"$industryName",
                                "description":"$jobDetails.description",
                                "jobType":"$jobDetails.jobType",
                                "payType":"$jobDetails.payType",
                                "ratePerHour":"$jobDetails.payment.ratePerHour",
                                "noOfStuff":"$jobDetails.payment.noOfStuff",
                                "location":"$jobDetails.location.coordinates",
                                "locationName":"$jobDetails.locationName"
                            }
                        },
                       ],
                        "jobOffers":[
                            { $unwind: {path:"$jobOffers",preserveNullAndEmptyArrays:false}},
                            {
                                "$project":{
                                    "_id" :1,
                                    "roleId": "$jobOffers.roleId",
                                    "employerId": "$jobOffers.employerId",
                                    "jobId": "$jobOffers.jobId",
                                    "candidateId": "$jobOffers.candidateId",
                                    "location":"$jobOffers.location",
                                    "noOfStuff":"$jobOffers.noOfStuff",
                                    "pay":"$jobOffers.pay",
                                    "payType":"$jobOffers.payType",
                                    "jobType":"$jobOffers.jobType"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "jobOffers"
                                }
                            },
                            {
                                "$project":{
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    jobOffers : { $arrayElemAt :["$jobOffers", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "jobRoleName" : "$jobOffers.jobRoleName",
                                    "jobRoleIcon" : "$jobOffers.jobRoleIcon",
                                    "industryId":"$jobOffers.industryId",
                                    "jobRoleId":"$jobOffers.jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "location":"$location",
                                    "noOfStuff":"$noOfStuff",
                                    "pay":"$pay",
                                    "payType":"$payType",
                                    "jobType":"$jobType"
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                    
                                }
                            },
                            {
                                "$project":{
                                    "location":"$location",
                                    "noOfStuff":"$noOfStuff",
                                    "pay":"$pay",
                                    "payType":"$payType",
                                    "jobType":"$jobType",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                }
                            },
                             {
                                "$project":{
                                    "location":"$location",
                                    "noOfStuff":"$noOfStuff",
                                    "pay":"$pay",
                                    "payType":"$payType",
                                    "jobType":"$jobType",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryDetails.industryName"
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobs",
                                    localField: "jobId",
                                    foreignField: "jobId",
                                    as: "jobDetails"
                                }
                            },
                            {
                                "$project":{
                                    location:1,
                                    noOfStuff:1,
                                    pay:1,
                                    payType:1,
                                    jobType:1,
                                    jobRoleName:1,
                                    jobRoleIcon:1,
                                    industryId:1,
                                    jobRoleId:1,
                                    jobId:1,
                                    employerId:1,
                                    candidateId:1,
                                    industryName:1,
                                    jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails":{$filter: {
                                        input: '$jobDetails.jobDetails.roleWiseAction',
                                        as: 'item',
                                        cond: {$eq: ['$$item.roleId', '$jobRoleId']}
                                }},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                   
                                }
                            },
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "jobDetails": { $arrayElemAt :["$jobDetails", 0]},
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "jobRoleNameExist": {
                                        $ifNull: [ '$jobRoleName', false ]
                                    }
                                   
                                }
                            },

                            {
                                "$match": {
                                    "jobRoleNameExist":{ $ne: false } 
                                }
                            },
                            
                            {
                                "$project":{
                                    "locationName":"$jobDetails.locationName",
                                    "jobRoleName" : "$jobRoleName",
                                    "jobRoleIcon" : "$jobRoleIcon",
                                    "industryId":"$industryId",
                                    "jobRoleId":"$jobRoleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "industryName":"$industryName",
                                    "description":"$jobDetails.description",
                                    "jobType":"$jobDetails.jobType",
                                    "payType":"$jobDetails.payType",
                                    "ratePerHour":"$jobDetails.payment.ratePerHour",
                                    "noOfStuff":"$jobDetails.payment.noOfStuff",
                                    "location":"$jobDetails.location.coordinates",
                                    "locationName":"$jobDetails.locationName",
                                    "isDeleted":"$jobDetails.isDeleted"
                                }
                            },
                           
                           
                            
                        ],
                        "locationDetails":[
                            {
                                $lookup: {
                                    from: "countries",
                                    localField: "location.country",
                                    foreignField: "countryId",
                                    as: "location.country"
                                }
                            },
                            {
                                $lookup: {
                                    from: "cities",
                                    localField: "location.city",
                                    foreignField: "cityId",
                                    as: "location.cityDetails"
                                }
                            },
                            {
                                $project : {
                                    address : "$location.address",
                                    city : {
                                        $arrayElemAt :["$location.cityDetails", 0]
                                    },
                                    country :{
                                        $arrayElemAt :["$location.country", 0]
                                    },
                                    postCode: "$location.postCode"
                                }
                            }
                        ],
                        "selRoleDetails":[                            
                            { $unwind: {path:"$jobOffers",preserveNullAndEmptyArrays:true}},
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "selectedRole",
                                    foreignField: "jobRoleId",
                                    as: "roleDetails"
                                }
                            }, 
                            {
                                "$project":{
                                   roleDetails : { $arrayElemAt :["$roleDetails", 0]}
                                }
                            },
                            {
                              $project :{
                                _id:0,
                                jobRoleId : "$roleDetails.jobRoleId",
                                jobRoleName : "$roleDetails.jobRoleName",
                                  jobRoleIcon : "$roleDetails.jobRoleIcon",
                                industryId : "$roleDetails.industryId",
                              }  
                            }                        
                        ],
                        "workExpDetails":[
                            {
                                $project : {
                                    "workExperience" : 1
                                }
                            },
                            { $unwind: {path:"$workExperience.experiences",preserveNullAndEmptyArrays:true}},
                            // { $unwind: {path:"$workExperience.experiences",preserveNullAndEmptyArrays:false}},
                            //{ $unwind: "$workExperience.experiences"},
                            // { $unwind: {path:"$workExperience.experiences",preserveNullAndEmptyArrays:false}},
                            // "bankDetails.icon" : { $cond: [ {$eq: ["$bankDetails.institution_icon", ""]}, "https://listedbank.s3.amazonaws.com/default/default_icon.png", "$bankDetails.institution_icon" ] },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "workExperience.experiences.jobRole",
                                    foreignField: "jobRoleId",
                                    as: "roleDetails"
                                }
                            },
                           
                            {
                              "$project":{
                                "workExperience.expInMonths" : 1,
                                "workExperience.experiences.referenceEmailAddress" : 1,
                                "workExperience.experiences.industryId" : 1,
                                "workExperience.experiences.expId" : 1,
                                "workExperience.experiences.description" : 1,
                                "workExperience.experiences.startDate" : 1,
                                "workExperience.experiences.companyName" : 1,
                                "workExperience.experiences.referenceName" : 1,
                                "workExperience.experiences.referenceContact" : 1,
                                "workExperience.experiences.referencePosition" : 1,
                                "workExperience.experiences.referenceAddressDetails" : 1,
                                "workExperience.experiences.endDate" : 1,
                                "workExperience.experiences.roleDetails" : { $arrayElemAt :["$roleDetails", 0]}
                                }
                            },
                            {
                              "$project":{
                                // "_id":0,
                                "expInMonths" : "$workExperience.expInMonths",
                                "experiences.referenceEmailAddress":"$workExperience.experiences.referenceEmailAddress",
                                "experiences.industryId" : "$workExperience.experiences.industryId",
                                "experiences.expId" : "$workExperience.experiences.expId",
                                "experiences.description" : "$workExperience.experiences.description",
                                "experiences.startDate" : "$workExperience.experiences.startDate",
                                "experiences.companyName" : "$workExperience.experiences.companyName",
                                "experiences.referenceName" : "$workExperience.experiences.referenceName",
                                "experiences.referenceContact" : "$workExperience.experiences.referenceContact",
                                "experiences.referencePosition" : "$workExperience.experiences.referencePosition",
                                "experiences.referenceAddressDetails" : "$workExperience.experiences.referenceAddressDetails",
                                "experiences.endDate" : "$workExperience.experiences.endDate",
                                "experiences.roleDetails" : "$workExperience.experiences.roleDetails"
                                }
                            },
                            {
                                $group : {
                                    _id : null,
                                    "expInMonths": {$first: "$expInMonths"},
                                     experiences : {
                                            $addToSet : {
                                                "industryId":"$experiences.industryId",
                                               "referenceEmailAddress":"$experiences.referenceEmailAddress",
                                                "expId":"$experiences.expId",
                                                "description":"$experiences.description",
                                                "startDate":"$experiences.startDate",
                                                "companyName":"$experiences.companyName",
                                                "referenceName":"$experiences.referenceName",
                                                "referenceContact":"$experiences.referenceContact",
                                                "referencePosition":"$experiences.referencePosition",
                                                "referenceAddressDetails":"$experiences.referenceAddressDetails",
                                                "endDate":"$experiences.endDate",
                                                "roleDetails":"$experiences.roleDetails",                                                                                               
                                            }
                                        }
                                    }                                
                                }
                          
                        ],
                        "availDetails":[
                            {
                                $project : {
                                    "availability" : 1
                                }
                            },
                            { $unwind: {path:"$availability",preserveNullAndEmptyArrays:false}},
                            { $unwind: {path:"$availability.Timeslot",preserveNullAndEmptyArrays:false}},
                            {
                                $lookup: {
                                    from: "timeslots",
                                    localField: "availability.Timeslot",
                                    foreignField: "id",
                                    as: "availability.timeSlotDetails"
                                }
                            },
                            {
                                $project : {
                                    _id : 0,
                                    TimeslotId : "$availability.Timeslot",
                                    day : "$availability.day",
                                    availabilityId : "$availability._id",
                                    timeSlotDetails : {
                                        $arrayElemAt : ["$availability.timeSlotDetails",0]
                                    }
                                }
                            },
                            {
                                $group :{
                                    _id : "$day",
                                    "day": {
                                        $first: "$day"
                                    },
                                    "availabilityId": {
                                        $first: "$availabilityId"
                                    },
                                    "Timeslot": {
                                        $push: "$timeSlotDetails.id"
                                    },
                                    timeSlotDetails : {
                                        $addToSet : {
                                            "slotName":"$timeSlotDetails.slotName",
                                            "starttime":"$timeSlotDetails.starttime",
                                            "endTime":"$timeSlotDetails.endTime",
                                            "id":"$timeSlotDetails.id",
                                        }
                                    }
                                }
                            }
                        ],
                        "pureData":[
                            {"$project" : {
                                "_id" : 0
                            }}
                        ],

                        "selRoleSkillDetails":[
                            {"$project" : {
                                "_id" : 0,
                                "selectedRole":1,
                                "skills":1
                            }},
                            
                        ]
                    }
                },
                {
                    $project : {
                        invitationToApplyJobs:"$invitationToApplyJobs",
                        hiredJobs:"$hiredJobs",
                        appliedJobs:"$appliedJobs",
                        declinedJobs:"$declinedJobs",
                        acceptedJobs:"$acceptedJobs",
                        jobOffers:"$jobOffers",
                        locationDetails : "$locationDetails",
                        selRoleDetails : "$selRoleDetails",
                        workExpDetails : { $arrayElemAt : ["$workExpDetails",0]},
                        availDetails : "$availDetails",
                        selRoleSkillDetails : "$selRoleSkillDetails",
                        pureData : { $arrayElemAt : ["$pureData",0]}
                    }
                },
                {
                    $project : {
                        "_id":"$pureData._id",
                        "profilePic":"$pureData.profilePic",
                        "Status":"$pureData.Status",
                        "rating":"$pureData.rating",
                        "trainingDetails":"$pureData.trainingDetails",
                        "declinedJobs":"$declinedJobs",
                        "hiredJobs":"$hiredJobs",
                        "invitationToApplyJobs":"$invitationToApplyJobs",
                        "acceptedJobs":"$acceptedJobs",
                        "jobOffers":"$jobOffers",
                        "appliedJobs":"$appliedJobs",
                        "selectedJobList":"$pureData.selectedJobList",
                        "candidateId":"$pureData.candidateId",
                        "fname":"$pureData.fname",
                        "mname":"$pureData.mname",
                        "lname":"$pureData.lname",
                        "email_address":"$pureData.email_address",
                        "home_address":"$pureData.home_address",
                        "EmailId":"$pureData.EmailId",
                        "password":"$pureData.password",
                        "userType":"$pureData.userType",
                        "memberSince":"$pureData.memberSince",
                        "createdAt":"$pureData.createdAt",
                        "updatedAt":"$pureData.updatedAt",
                        "contactNo":"$pureData.contactNo",
                        "dob":"$pureData.dob",
                        "kinDetails":"$pureData.kinDetails",
                        "location":{$arrayElemAt : ["$locationDetails",0]}, //
                        "secondaryEmailId":"$pureData.secondaryEmailId",
                        "dayoff":"$pureData.dayoff",
                        "cv":"$pureData.cv",
                        "docs":"$pureData.docs",
                        "video":"$pureData.video",
                        "distance":"$pureData.distance",
                        "geolocation":"$pureData.geolocation",
                        "selectedRole":"$selRoleDetails", //
                        "workExperience":"$workExpDetails", //
                        "language":"$pureData.language",
                        "availability":"$availDetails", //
                        "selRoleSkillDetails" : "$selRoleSkillDetails",
                    }
                }
                
 ]

 //====================

            async function fetchSkill() { 
                return new Promise(function(resolve, reject){

                    let aggrQuery = [
                        { $match : { 'candidateId': candidateId } } ,
                        {
                            $project: {
                                '_id': 0,
                                "skills":1,
                                "workExperience":1,
                                "payloadSkill":1
                            }
                        },
                        {
                            $lookup: {
                                from: 'industries',			        
                                localField: 'payloadSkill.industryId',
                                foreignField: 'industryId',
                                as: 'industryDetail'
                            }
                        },
                       
                      ];

                    models.candidate.aggregate(aggrQuery,function(err,response){
                        if (err) {
                            resolve({
                                "isError": true
                            });
                        } else {
                            
                            resolve({
                                "isError": false,
                                "response": response
                            });
                        }
                    })          
                }) // END return new Promise(function(resolve, reject){   
            } // start of fetchSkill

//==============================
        

        // const restructureAvailivility = (availability) => {
           
        //     let restructuredAvailbility = [];
        //     let timeSlotName;
        //     var i;
            
        //     restructuredAvailbility = availability.map( (availability) => {
        //         timeSlotName = availability.timeSlotDetails.map((timeSlotDetails) => {
        //             return {
        //                 "Day": availability.day,
        //                 "Slotname": timeSlotDetails.slotName
        //             };
        //         });

        //         // console.log(availability);
        //         return timeSlotName;
        //     });

        //     return restructuredAvailbility;
        // }              

 //=====================

            
            models.candidate.aggregate(aggrQuery).exec( async (error, data) => {
                if(error){
                   
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (data == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else {

                        // this function is used to check whether user exist or not
  
                        
                        // models.timeslot.find({}, async function (err, item) {
                        //     const msg = await fetchTimeSlot(item,data);
                            

                        // })
                        // data[0].availability.map(a=>{
                        //     a.Timeslot.map(b=>{
                        //         totalTimeSlotDb.map(c=>{
                        //             if(c.id==b){
                        //                 console.log(c.slotName)
                        //             }
                        //         })
                        //     })
                        // })
                        // for (x in data[0]){
                        //     data[0]['acceptedJobs'] =  Object.keys(data[0]['acceptedJobs']).length==0?[]:data[0]['acceptedJobs']
                        // }

                        // Here, we are checking whether user exist in this conference or not
                        let fetchSkillStatus = await fetchSkill();

                        if (fetchSkillStatus.isError == false) {
                            // data[0].skills = fetchSkillStatus.response[0].skills
                            // data[0].selectedRole[1] = fetchSkillStatus.response[0].skillDetails;
                            // data[0].payloadSkill = fetchSkillStatus.response[0].payloadSkill;

                            let payloadSkill = fetchSkillStatus.response[0].payloadSkill;
                            let industryDetail = fetchSkillStatus.response[0].industryDetail;

                            let tag1;
                            let tag2;

                            for (tag1 = 0; tag1 < payloadSkill.length; tag1++) {
                                for (tag2 = 0; tag2 < industryDetail.length; tag2++) {
                                   
                                    if (payloadSkill[tag1].industryId == industryDetail[tag2].industryId) {
                                        payloadSkill[tag1].industryName = industryDetail[tag2].industryName;
                                    }

                                }
                            } // END for (tag1 = 0; i < payloadSkill.length; tag1++) {
                            data[0].payloadSkill = payloadSkill;

                        } 

                        /* 0000000000000000000000000000000000000000000000000000000000000000000000 */
                        let industryIdList = [];
                        let industryIdNameList = [];

                        let tagInd;

                        for (tagInd = 0; tagInd < data[0].payloadSkill.length; tagInd++) {

                            if (!industryIdList.includes(data[0].payloadSkill[tagInd].industryId)) {                  
                                industryIdNameList.push({
                                    "industryId": data[0].payloadSkill[tagInd].industryId,
                                    "industryName": data[0].payloadSkill[tagInd].industryName,
                                });
                            }  
                            industryIdList.push(data[0].payloadSkill[tagInd].industryId);   
                        } // END for (tagInd = 0; tagInd < data[0].payloadSkill.length; tagInd++) {

                        data[0].industryIdNameList = industryIdNameList;
                        /* 0000000000000000000000000000000000000000000000000000000000000000000000 */
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: data[0]
                        })
                    }
                }
               
            })
           
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    /**
    * @abstract 
    * get Candidate address
    * First check  Candidate exist or not using CandidateId
    * If exist send details
   */
    getDetails:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            if (!req.body.candidateId) { return; }
            models.candidate.findOne({ 'candidateId': candidateId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1003'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }  

    },

    /*****************************************************
     * Verify Candidate
     *****************************************************/ 
    saveEmailVerificationTocken:(req, res, next)=>{
        console.log(req.query.id)
        var querywhere = {'Status.confirmationTocken':req.query.id};
        models.candidate.findOne(querywhere, function (error, item) {
            
              if(error){
                  console.log('error');
                  console.log('here ut the code of redirecting alternative path');
                  // res.redirect(`${config.app.frontendUri}/500`);
                  res.redirect(`${config.app.FORMATTED_URL}/500`);
              }
              else{
                  if (item == null) {
                      console.log('candidate not exists');
                      console.log('here ut the code of redirecting alternative path');
                      // res.redirect(`${config.app.frontendUri}/invalid-link`);
                      res.redirect(`${config.app.FORMATTED_URL}/invalid-link`);
                  } else {
                   if(item.Status.confirmationTocken===req.query.id){
                      console.log('same tocken')
                      let querywith={'Status.isVerified':true};
                      models.candidate.updateOne(querywhere, querywith, function (error, response) {
                          if(error){
                          	res.redirect(`${config.app.frontendUri}/500`);
	                          // res.json({
	                          //     isError: true,
	                          //     message: errorMsgJSON['ResponseMsg']['404'],
	                          //     statuscode: 404,
	                          //     details: null
	                          // });
                          }
                          else {
                              if (response.nModified == 1) {
                                  // res.redirect(`${config.app.frontendUri}/account-verified`);
                                  // res.redirect(`${config.app.FORMATTED_URL}/account-verified`);
                                  // res.redirect('https://www.google.com/');
                                  res.redirect(`${config.app.frontendUri}`);
                              }
                              else{
                              	res.redirect(`${config.app.frontendUri}/500`);
                                  // res.json({
                                  //     isError: true,
                                  //     message: errorMsgJSON['ResponseMsg']['1004'],
                                  //     statuscode: 1004,
                                  //     details: null
                                  // }) 
                              }
                          }
                      })
                     
                   } else {
                       console.log('not same tocken');
                       console.log('here ut the code of redirecting alternative path');
                       res.redirect(`${config.app.frontendUri}/invalid-link`);
                   }
                  }
              }
          })
    },


    /*****************************************************
     * If all details for profile creation are needed to store at one attempt
     * It is  fully completed.Use If Needed
    *****************************************************/ 

    addFullDetails:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let dob = req.body.dob ? req.body.dob : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate date of birth",
                isError: true,
            });
            let EmailId = req.body.EmailId ? req.body.EmailId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - EmailId",
                isError: true,
            });
            let city = req.body.city ? req.body.city : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - City",
                isError: true,
            });
            let country = req.body.country ? req.body.country : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Country",
                isError: true,
            });
            let postCode = req.body.postCode ? req.body.postCode : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - PostCode",
                isError: true,
            });
            let address = req.body.address ? req.body.address : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Address",
                isError: true,
            });
            let contactNo = req.body.contactNo ? req.body.contactNo : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - ContactNo",
                isError: true,
            });
            let kinName = req.body.kinName ? req.body.kinName : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - kinName",
                isError: true,
            });
            let kinContactNo = req.body.kinContactNo ? req.body.kinContactNo : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - kinContactNo",
                isError: true,
            });
            let relationWithKin = req.body.relationWithKin ? req.body.relationWithKin : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Relation With Kin",
                isError: true,
            });
            let dayoff = req.body.dayoff ? req.body.dayoff : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Day off",
                isError: true
            });
            let availability = req.body.availability ? req.body.availability : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Availability",
                isError: true
            });
            let cv = req.body.cv ? req.body.cv : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " -CV",
                isError: true
            });
            let video = req.body.video ? req.body.video : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Video",
                isError: true
            });
            let documents = req.body.documents ? req.body.documents : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Documents",
                isError: true
            });
            let location = req.body.location ? req.body.location : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Location"
            });
            let distance = req.body.distance ? req.body.distance : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - distance"
            });
            let roles = req.body.roles ? req.body.roles : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Chosen Roles",
                isError: true
            }); 
            let experiences = req.body.experiences ? req.body.experiences : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Experiences",
                isError: true,
            });
            let lastObjindex=experiences.length-1;
            let firstObjDate=(experiences[0].startDate).substring(0, 10);
            let lastObjDate=(experiences[lastObjindex].endDate).substring(0, 10);
           
            dt1 = new Date(firstObjDate);
            dt2 = new Date(lastObjDate);
            let days= Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
            console.log('.......',days);
            let languages = req.body.languages ? req.body.languages : res.send({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Languages",
                isError: true,
            });
            if (!req.body.candidateId || !req.body.dob || !req.body.EmailId || !req.body.city ||!req.body.country ||!req.body.postCode ||!req.body.address ||!req.body.contactNo ||!req.body.kinName || !req.body.kinContactNo || !req.body.relationWithKin) { return; }
           
            models.candidate.findOne({'candidateId':candidateId }, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1003'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else{
                        models.candidate.update({"candidateId":candidateId}, { $set: { "language": [] }}, function(err, affected){
                            if(err){
                                res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                                })
                            }
                            else{
                                if (affected.nModified == 1) {
                                    let insertQuery={
                                        'dob':dob,'location.city':city,'location.country':country,
                                        'location.postCode':postCode, 'location.address':address,'secondaryEmailId':EmailId,'contactNo':contactNo,
                                        'kinDetails.kinName':kinName,'kinDetails.kinContactNo':kinContactNo,'kinDetails.relationWithKin':relationWithKin,
                                        "dayoff":dayoff,"availability":availability,"cv":cv,"video":video,"docs":documents,
                                        "geolocation":location,"distance":distance,"selectedRole": roles,$push:{"workExperience.experiences":experiences},"workExperience.expInMonths":days ,
                                        "language":languages
                                    }
                                    models.candidate.updateOne({'candidateId':candidateId },insertQuery, function (error, item) {
                                        if (error) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            })
                                        }
                                        else{
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['201'],
                                                statuscode: 201,
                                                details: null
                                            })
                                        }
                                    })
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })  
                                }
                            } 
                        })
                    }
                }
            })

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getRatings:(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 5;
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            if (!req.body.candidateId) { return; }
            let aggrQuery = [
                { $match : { 'candidateId': candidateId } } ,
                {
                    $facet:{
                        "allRatings":[
                            {
                                "$project":{
                                    "_id":0,
                                    "roleId": 1,
                                    "jobId":1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "review": 1,
                                    "rating":1,
                                    "createdAt": 1,
                                   
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "roleDetails"
                                }
                            },
                            {
                                "$project":{
                                    roleId: 1,
                                    jobId:1,
                                    employerId: 1,
                                    candidateId: 1,
                                    review: 1,
                                    rating:1,
                                    createdAt: 1,
                                    roleDetails:{ $arrayElemAt :["$roleDetails", 0]}
                                }
                             },
                             {
                                "$project":{
                                    "roleId": "$roleId",
                                    "jobId":"$roleId",
                                    "employerId": "$employerId",
                                    "candidateId": "$candidateId",
                                    "review": "$review",
                                    "rating":"$rating",
                                    "createdAt": "$createdAt",
                                    "jobRoleName":"$roleDetails.jobRoleName",
                                    "industryId":"$roleDetails.industryId"
                                   
                                }
                             },
                             {
                                $lookup: {
                                    from: "industries",
                                    localField: "industryId",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                    roleId: 1,
                                    jobId:1,
                                    employerId: 1,
                                    candidateId: 1,
                                    review: 1,
                                    rating:1,
                                    createdAt: 1,
                                    jobRoleName:1,
                                    industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                }
                             },
                             {
                                "$project":{
                                    "roleId": "$roleId",
                                    "jobId":"$roleId",
                                    "employerId": "$employerId",
                                    "candidateId": "$candidateId",
                                    "review": "$review",
                                    "rating":"$rating",
                                    "createdAt": "$createdAt",
                                    "jobRoleName":"$jobRoleName",
                                    "industryName":"$industryDetails.industryName"
                                   
                                }
                             },
                             {
                                $lookup: {
                                    from: "employers",
                                    localField: "employerId",
                                    foreignField: "employerId",
                                    as: "employerDetails"
                                }
                            },
                            {
                                "$project":{
                                    roleId: 1,
                                    jobId:1,
                                    employerId: 1,
                                    candidateId: 1,
                                    review: 1,
                                    rating:1,
                                    createdAt: 1,
                                    jobRoleName:1,
                                    industryName:1,
                                    employerDetails:{ $arrayElemAt :["$employerDetails", 0]}
                                }
                             },
                             {
                                "$project":{
                                    "roleId": "$roleId",
                                    "jobId":"$roleId",
                                    "employerId": "$employerId",
                                    "candidateId": "$candidateId",
                                    "review": "$review",
                                    "rating":"$rating",
                                    "createdAt": "$createdAt",
                                    "jobRoleName":"$jobRoleName",
                                    "industryName":"$industryName",
                                    "profilePic":"$employerDetails.profilePic",
                                    "employerType":"$employerDetails.employerType",
                                    "companyName": "$employerDetails.companyName",
                                    "individualContactName": "$employerDetails.individualContactName"
                                   
                                }
                             },
                             {
                                $group: {
                                    _id: null,
                                    total: {
                                    $sum: 1
                                    },
                                    results: {
                                    $push : '$$ROOT'
                                    }
                                }
                            },
                            {
                                $project : {
                                    '_id': 0,
                                    'total': 1,
                                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                    'results': {
                                        $slice: [
                                            '$results', page * perPage , perPage
                                        ]
                                    }
                                }
                            }
                        ],
                        "roleWiseRatingAvg":[
                            {
                                "$project":{
                                    "_id":0,
                                    "roleId": 1,
                                    "jobId":1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "review": 1,
                                    "rating":1,
                                    "createdAt": 1,
                                   
                                }
                            },
                            {
                                $lookup: {
                                    from: "jobroles",
                                    localField: "roleId",
                                    foreignField: "jobRoleId",
                                    as: "roleDetails"
                                }
                            },
                            {
                                "$project":{
                                    roleId: 1,
                                    jobId:1,
                                    employerId: 1,
                                    candidateId: 1,
                                    review: 1,
                                    rating:1,
                                    createdAt: 1,
                                    roleDetails:{ $arrayElemAt :["$roleDetails", 0]}
                                }
                             },
                             {
                                "$project":{
                                    "roleId": "$roleId",
                                    "jobId":"$roleId",
                                    "employerId": "$employerId",
                                    "candidateId": "$candidateId",
                                    "review": "$review",
                                    "rating":"$rating",
                                    "createdAt": "$createdAt",
                                    "jobRoleName":"$roleDetails.jobRoleName",
                                    "industryId":"$roleDetails.industryId"
                                   
                                }
                             },
                            {
                                $group :{
                                    _id :"$roleId",
                                    "jobRoleName": {
                                        $first: "$jobRoleName"
                                    },
                                    
                                    rate: { $avg: "$rating" },
                                }
                            }
                        ],
                        "allRatingAvg":[
                            {
                                $group :{
                                    _id :"$candidateId",
                                    allRatingAvg: { $avg: "$rating" },
                                }
                            }
                        ]
                    }
                },
                {
                    $project : {
                        allRatings:{ $arrayElemAt : ["$allRatings",0]},
                        roleWiseRatingAvg:1,
                        allRatingAvg:{ $arrayElemAt : ["$allRatingAvg",0]}
                        
                    }
                }
            ]
            models.rating.aggregate(aggrQuery).exec(async(err, result) => {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details:result[0]
                    })
                }
               
            })

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    }
    
}

async function fetchTimeSlot(allSlotDetails,candidateDetails){
    let totalTimeSlot={};
    let totalTimeSlotDb=[];
   
    candidateDetails[0].availability.map(async data => {
        
    })
}



/**********************************************************************
 *Json structure for sending all details of candidate is given below
        {
            "candidateId": "CANDIDATE1558462185730",
            "dob": "2019-05-15T13:31:34.213Z",
            "EmailId": "another@gmail.com",
            "city": "kolkata",
            "country": "india",
            "postCode": "33443332",
            "address": "bidhannagar",
            "contactNo": "98090201193",
            "kinName": "subhasis",
            "kinContactNo": "9866554433",
            "relationWithKin": "brother",
            "dayoff": [
                {
                    "startDate": "22-3-2019",
                    "endEnd": "22-4-2019"
                },
                {
                    "startDate": "22-10-2019",
                    "endEnd": "22-11-2019"
                }
            ],
            "availability": [
                {
                    "day": "MON",
                    "Timeslot": [
                        "slot1",
                        "slot2",
                        "slot3"
                    ]
                },
                {
                    "day": "TUE",
                    "Timeslot": [
                        "slot3",
                        "slot7"
                    ]
                }
            ],
            "cv": {
                "cvName": "sourav.txt",
                "cvLink": "www.cv.com"
            },
            "video": {
                "videoName": "myvideo.mp4",
                "videoLink": "www.myvideo.com/asdsaxdas"
            },
            "documents": [
                {
                    "docName": "doc1",
                    "docLink": "www.doc1.com/353454543"
                },
                {
                    "docName": "doc2",
                    "docLink": "www.doc2.com/353454543"
                }
            ],
            "distance": 4,
            "location": {
                "type": "Point",
                "coordinates": [
                    -110.8571443,
                    32.4586858
                ]
            },
            "roles": [
                "1",
                "2"
            ],
            "experiences": [
                {
                    "jobRole": "yyyyyyyyyyyyyyy",
                    "description": "node developer",
                    "startDate": "2022-01-08T12:27:29.985Z",
                    "companyName": "tcs",
                    "referenceName": "Subhasis",
                    "referenceContact": "980020166",
                    "endDate": "2023-05-02T12:27:29.985Z"
                }
            ],
            "languages": [
                {
                    "langName": "Bengali",
                    "langCode": "Bn",
                    "rank": "Fluent"
                },
                {
                    "langName": "English",
                    "langCode": "En",
                    "rank": "Intermidiate"
                }
            ]
        }
 ****************************************************************************************/




 // {
                //     $lookup: {
                //         from: "countries",
                //         localField: "location.country",
                //         foreignField: "countryId",
                //         as: "location.country"
                //     }
                // },
                // {
                //     $project: {
                        
                //         "appliedJobList":1,
                //         "trainingDetails":1,
                //         "location.postCode":1,
                //         "location.address":1,
                //         "location.city":1,
                //         "location.country": { $arrayElemAt: [ "$location.country", 0 ] },
                //         "Status":1,
                //         "selectedRole":1,
                //         "candidateId":1,
                //         "availability":1,
                //         "workExperience":1,
                //         "selectedJobList":1,
                //         "fname":1,
                //         "mname":1,
                //         "lname":1,
                //         "EmailId":1,
                //         "password":1,
                //         "userType": 1,
                //         "memberSince": 1,
                //         "contactNo": 1,
                //         "dob":1,
                //         "kinDetails":1,
                //         "secondaryEmailId":1,
                //         "dayoff":1,
                //         "cv":1,
                //         "docs":1,
                //         "video":1,
                //         "distance":1,
                //         "geolocation":1,
                //         "language":1
                //     }
                // },
                // {
                //     $lookup: {
                //         from: "cities",
                //         localField: "location.city",
                //         foreignField: "cityId",
                //         as: "location.city"
                //     }
                // },
                // {
                //     $project: {
                //         "location.postCode":1,
                //         "location.address":1,
                //         "location.city":1,
                //         "location.country": 1,
                //         "location.city": { $arrayElemAt: [ "$location.city", 0 ] },
                //         "Status":1,
                //         "selectedRole":1,
                //         "candidateId":1,
                //         "availability":1,
                //         "workExperience":1,
                //         "trainingDetails":1,
                //         "appliedJobList":1,
                //         "selectedJobList":1,
                //         "fname":1,
                //         "mname":1,
                //         "lname":1,
                //         "EmailId":1,
                //         "password":1,
                //         "userType": 1,
                //         "memberSince": 1,
                //         "contactNo": 1,
                //         "dob":1,
                //         "kinDetails":1,
                //         "secondaryEmailId":1,
                //         "dayoff":1,
                //         "cv":1,
                //         "docs":1,
                //         "video":1,
                //         "distance":1,
                //         "geolocation":1,
                //         "language":1
                //     }
                // },
                // { $unwind: {path:"$selectedRole",preserveNullAndEmptyArrays:true}},
             
                // {
                //     $lookup: {
                //         from: "jobroles",
                //         localField: "selectedRole",
                //         foreignField: "jobRoleId",
                //         as: "roleDetails"
                //     }
                // },
                // {
                //     $project: {
                //         "location.postCode":1,
                //         "location.address":1,
                //         "location.city":1,
                //         "location.country": 1,
                //         "roleDetails": {
                //          $arrayElemAt: ["$roleDetails", 0]
                //         },
                        
                //         "Status":1,
                //         "candidateId":1,
                //         "availability":1,
                //         "workExperience":1,
                //         "trainingDetails":1,
                //         "appliedJobList":1,
                //         "selectedJobList":1,
                //         "fname":1,
                //         "mname":1,
                //         "lname":1,
                //         "EmailId":1,
                //         "password":1,
                //         "userType": 1,
                //         "memberSince": 1,
                //         "contactNo": 1,
                //         "dob":1,
                //         "kinDetails":1,
                //         "secondaryEmailId":1,
                //         "dayoff":1,
                //         "cv":1,
                //         "docs":1,
                //         "video":1,
                //         "distance":1,
                //         "geolocation":1,
                //         "language":1
                //     }
                // },
                // {
                //     $group: {
                //         "_id": {
                //             "candidateId": "$candidateId",
                //         },
                //         basicDetails : {
                //             $addToSet : {
                //                 "fname":"$fname",
                //                 "mname":"$mname",
                //                 "lname":"$lname",
                //                 "EmailId":"$EmailId",
                //                 "password":"$password",
                //                 "userType": "$userType",
                //                 "memberSince": "$memberSince",
                //                 "contactNo": "$contactNo",
                //                 "dob":"$dob",
                //                 "secondaryEmailId":"$secondaryEmailId"
                                
                //             }
                //         },
                //         "kinDetails": {
                //             $first: "$kinDetails"
                //         },
                       
                //         "postCode": {
                //             $first: "$location.postCode"
                //         },
                //         "address": {
                //             $first: "$location.address"
                //         },
                //         "city": {
                //             $first: "$location.city"
                //         },
                //         "country": {
                //             $first: "$location.country"
                //         },
                //         "Status": {
                //             $first: "$Status"
                //         },
                //         "candidateId": {
                //             $first: "$candidateId"
                //         },
                //         "roleDetails": {
                //             $push: "$roleDetails"
                //         },
                //         "availability": {
                //             $first: "$availability"
                //         },
                //         "workExperience": {
                //             $first: "$workExperience"
                //         },
                //         "trainingDetails": {
                //             $first: "$trainingDetails"
                //         },
                //         "appliedJobList": {
                //             $first: "$appliedJobList"
                //         },
                //         "selectedJobList": {
                //             $first: "$selectedJobList"
                //         },

                //         "dayoff":{
                //             $first: "$dayoff"  
                //         },
                //         "cv":{
                //             $first: "$cv"
                //         },
                //         "docs":{
                //             $first: "$docs"
                //         },
                //         "video":{
                //             $first: "$video"
                //         },
                //         "distance":{
                //             $first: "$distance"
                //         },
                //         "geolocation":{
                //             $first: "$geolocation"
                //         },
                //         "language":{
                //             $first: "$language"
                //         }
                //      }
                // },
                // { $unwind: {path:"$workExperience.experiences",preserveNullAndEmptyArrays:true}},
                // {
                //     $project: {
                //         "postCode":1,
                //         "address":1,
                //         "city":1,
                //         "country": 1,
                //         "roleDetails": 1,
                //         "Status":1,
                //         "candidateId":1,
                //         "availability":1,
                //         "workExperience.experiences":1,
                //         "workExperience.expInMonths":1,
                //         "trainingDetails":1,
                //         "appliedJobList":1,
                //         "selectedJobList":1,
                //         "basicDetails": {
                //             $arrayElemAt: ["$basicDetails", 0]
                //         },
                //         "kinDetails":1,
                //         "dayoff":1,
                //         "cv":1,
                //         "docs":1,
                //         "video":1,
                //         "distance":1,
                //         "geolocation":1,
                //         "language":1,
                //     }
                // },
                // {
                //     $lookup: {
                //         from: "industries",
                //         localField: "workExperience.experiences.industryId",
                //         foreignField: "industryId",
                //         as: "workExperience.experiences.industryDetails"
                //     }
                // },
                // {
                //     $lookup: {
                //         from: "jobroles",
                //         localField: "workExperience.experiences.jobRole",
                //         foreignField: "jobRoleId",
                //         as: "workExperience.experiences.roleDetails"
                //     }
                // },
                // {
                //     $group: {
                //             "_id": {
                //                 "candidateId": "$candidateId",
                //             },
                //             location : {
                //                 $addToSet : {
                //                     postCode : "$postCode",
                //                     address : "$address",
                //                     cityName : "$city.name",
                //                     cityId:"$city.cityId",
                //                     countryName : "$country.name",
                //                     countryId : "$country.countryId",
                                    
                //                 }
                //             },
                //             "Status": {
                //                 $first: "$Status"
                //             },
                //             "candidateId": {
                //                 $first: "$candidateId"
                //             },
                //             "roleDetails": {
                //                 $first: "$roleDetails"
                //             },
                //             "availability": {
                //                 $first: "$availability"
                //             },
                //             "expInMonths": {
                //                 $first: "$workExperience.expInMonths"
                //             },
                //             "workExperience": {
                //                 $push: "$workExperience.experiences"
                //             },
                //             "trainingDetails": {
                //                 $first: "$trainingDetails"
                //             },
                //             "appliedJobList": {
                //                 $first: "$appliedJobList"
                //             },
                //             "selectedJobList": {
                //                 $first: "$selectedJobList"
                //             },
                //             "basicDetails": {
                //                 $first: "$basicDetails"
                //             },
                //             "kinDetails": {
                //                 $first: "$kinDetails"
                //             },
                //             "dayoff":{
                //                 $first: "$dayoff"  
                //             },
                //             "cv":{
                //                 $first: "$cv"
                //             },
                //             "docs":{
                //                 $first: "$docs"
                //             },
                //             "video":{
                //                 $first: "$video"
                //             },
                //             "distance":{
                //                 $first: "$distance"
                //             },
                //             "geolocation":{
                //                 $first: "$geolocation"
                //             },
                //             "language":{
                //                 $first: "$language"
                //             }
                //         } 
                //     },
                //     {
                //         $project: {
                //             "roleDetails": 1,
                //             "Status":1,
                //             "candidateId":1,
                //             "availability":1,
                //             "workExperience":1,
                //             "trainingDetails":1,
                //             "appliedJobList":1,
                //             "selectedJobList":1,
                //             "basicDetails": 1,
                //             "kinDetails":1,
                //             "dayoff":1,
                //             "cv":1,
                //             "docs":1,
                //             "video":1,
                //             "distance":1,
                //             "geolocation":1,
                //             "language":1,
                //             "location":{
                //                 $arrayElemAt: ["$location", 0]
                //             },
                //         }
                //     },
                //     { $unwind: {path:"$availability",preserveNullAndEmptyArrays:true}},
                //     { $unwind: {path:"$availability.Timeslot",preserveNullAndEmptyArrays:true}},
                //     {
                //         $lookup: {
                //             from: "timeslots",
                //             localField: "availability.Timeslot",
                //             foreignField: "jobRoleId",
                //             as: "workExperience.experiences.roleDetails"
                //         }
                //     },
                