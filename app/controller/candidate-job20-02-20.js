const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const simplemailercontroller = require('../service/mailer');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const fcm = require('../service/fcm');
const sendsmscontroller = require('../service/TwilioSms');
const Moment = require('moment'); 
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
var Request = require("request");
const config =  require('../config/env');
require('twix');

const EnvInfo = require('../model/envInfo');


module.exports = {

    /*
        This function is written to fetch all the job related notification
        for a particular candidate
    */
    fetchNotificationforCandidate: (req, res, next) => {
        try{
            
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            if (!req.body.candidateId) { return; }

            let aggrQuery = [
                {
                    $match: {
                        'candidateId': candidateId
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'notification': 1
                    }
                },
                {
                    $unwind : "$notification"
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
         
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
                
            ];
            models.candidate.aggregate(aggrQuery).exec((err, response) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });

                    return;
                }

                if (response.length < 1) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['1009'],
                        statuscode: 1009,
                        details: null
                    });

                    return;
                }

                if (response.length > 0) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        detail: response
                    });

                    return;
                }
            })    

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    
    },

    applyAgainstinvitation: (req, res, next) => {
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }

            // this function is used to check whether candidate is authorised or not // by Ankur on 06-02-20
                  async function checkCandidateAuthorised(candidateId) {  // start of checkCandidateAuthorised
                      
                      return new Promise(function(resolve, reject){
                                      
                          let aggrQuery = [
                              {
                                $match: {
                                  'candidateId': candidateId,
                                } 
                              },
                              {
                                $project: {
                                    "Status": 1                            
                                }
                              },
                              
                          ];

                          models.candidate.aggregate(aggrQuery, function (err, response) {
                          
                              if (err) {
                                  resolve({"isError": true});
                              } else {

                                  if (response.length > 0) {
                                      resolve({
                                        "isError": false,
                                        "isExist": true, 
                                        "candidateDetail": response
                                      });
                                  } else{
                                      resolve({
                                        "isError": false,
                                        "isExist": false,
                                      });
                                  }
                              }
                              
                          }); 
                      })    
                  } // END of checkCandidateAuthorised
            // ====================================



            let updateWith={
                'roleId':roleId,
                'candidateId':candidateId
            }
            let findWhere={
                'appiliedFrom.roleId': roleId ,
                'appiliedFrom.candidateId': candidateId,
                'jobId': jobId
            }
            models.job.findOne(findWhere,
                { 'appiliedFrom.$': 1 }, 
                async function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{

                    // ====================================
                    // Coded by Ankur on 06-02-20
                    let checkCandidateAuthorisedStatus = await checkCandidateAuthorised(candidateId);
                    if (checkCandidateAuthorisedStatus.isError == true || checkCandidateAuthorisedStatus.isExist == false) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                        return; 
                    }

                    if (checkCandidateAuthorisedStatus.candidateDetail[0].Status.isApprovedByAot == false) {
                        res.json({
                            isError: true,
                            message: 'Candidate is not approved by admin yet',
                            statuscode: 1026,
                            details: null
                        });
                        return; 
                    }
                    //===========================================

                    if(item){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['1026'],
                            statuscode: 1026,
                            details: null
                        })
                    }
                    else{
                        models.job.updateOne({ 'employerId': employerId,'jobId':jobId },{$push: {'appiliedFrom':updateWith}}, function (error, response) {
                            if(error){
                                console.log("error 1",error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                let insertedValue={
                                    "roleId": roleId,"employerId":employerId,"jobId":jobId,"candidateId":candidateId
                                }
                                if (response.nModified == 1) {
                                    models.candidate.updateOne({ 'candidateId': candidateId }, { $push: { "appliedJobs":insertedValue },"$pull": { "invitationToApplyJobs": { "jobId": jobId ,"roleId":roleId} }},function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {
                                        if (appliedJobsUpdatedError) {
                                            console.log("error 2",appliedJobsUpdatedError)
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        if (appliedJobsUpdatedresponse.nModified == 1) {
                                           
                                            models.candidate.find({"candidateId":candidateId },  function (error, data) {
                                                                           
                                                models.employer.find({"employerId":employerId },  function (searchingerror, searchRating) {
                                                   
                                              
                                                fcm.sendTocken(searchRating[0].Status.fcmTocken,data[0].fname+" "+data[0].lname+" accepted your job")
                                                .then(function(nofify){
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    })
                                                }) 
                                                })
                                            })
                                        }
                                        else{
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            }) 
                                        }
                                    })
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    }) 
                                }
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    checkIsWorkingDay: (req, res, next) => {
        try{
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId || !req.body.date ){return;}
            let findWhere={
                "candidateId":candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date":date,
                }
                models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, function (err, item) {
                    if(err){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        }); 
                    }
                    if(item==null){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: {
                                isWorkingDay:false
                            }
                        })
                    }
                    else{
                        res.json({
                            isError: false,
                            message: "Today is your working day",
                            statuscode: 200,
                            details: {
                                isWorkingDay: true,
                                shiftTime:item.workingDays[0].shiftTime,
                              
                            }
                        })
                    }
                })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getTimeTracking: async (req, res, next) => {
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            if (!req.body.candidateId) { return; }
            let aggrQuery = [
                {
                    '$match': {'candidateId':candidateId}
                },
                
                { $project: {
                   jobId: 1,
                   roleId: 1,
                   total: { $sum: "$workingDays.totalTimeInMinutes"}
                }},

                {
                    $lookup: {
                        from: "jobs",
                        localField: "jobId",
                        foreignField: "jobId",
                        as: "jobDetails"
                    }
                },
                
                { $project: {
                    jobId: 1,
                    roleId: 1,
                    total: 1,
                    jobDb:{ $arrayElemAt :["$jobDetails", 0]}
                 }},
                
                 {$project: {
                    jobId: 1,
                    roleId: 1,
                    total: 1,
                    jobCollection: {$filter: {
                        input: '$jobDb.jobDetails.roleWiseAction',
                        as: 'jobCollection',
                        cond: {$eq: ['$$jobCollection.roleId', '$roleId']}
                    }},
                    _id: 0
                }},


                { $project: {
                    jobId: 1,
                    roleId: 1,
                    total: 1,
                    jobDb:{ $arrayElemAt :["$jobCollection", 0]}
                 }},
                 
                { $project: {
                    jobId: 1,
                    roleId: 1,
                    total: 1,
                    jobDb:1,
                    
                }},
               /*
                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        jobDb:1,
                       
                        totalFormatedMinute: {
                            
                            "$switch": {
                                branches: [
                                    {
                                        case: { $lt: ["$totalMinute", 10] },
                                        // then: { $concat: ["0", { $toString: "$totalMinute" }] }
                                        then: { $concat: ["0", { $toString: "$totalMinute" }] }
                                    },
                                    {
                                        case: { $gte: ["$totalMinute", 10] },
                                        then: { $toString: "$totalMinute" }
                                    },
                                ],
                                default: { $toString: "$totalMinute" }
                            }
                        },
                        totalFormatedHour: {
                            "$switch": {
                                branches: [
                                    {
                                        case: { $lt: ["$totalHour", 10] },
                                        then: { $concat: ["0", { $toString: "$totalHour" }] }
                                    },
                                    {
                                        case: { $gte: ["$totalHour", 10] },
                                        then: { $toString: "$totalHour" }
                                    },
                                ],
                                default: { $toString: "$totalHour" }
                            }
                        }
                    }
                },
                */

                ///=======
                
                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        location:"$jobDb.location",
                        locationName: "$jobDb.locationName",
                        description: "$jobDb.description",
                        roleName: "$jobDb.roleName",
                        jobType: "$jobDb.jobType",
                        setTime:"$jobDb.setTime",
                        paymentStatus: "$jobDb.paymentStatus",
                        status:"$jobDb.status",
                        payment:"$jobDb.payment", 
                        totalHours:{$floor:{ $divide:["$total", 60]}},
                        totalMinutes:{$mod:["$total", 60]}

                        // totalTime: { $concat: [{ $toString: "$totalFormatedHour" }, ":", { $toString: "$totalFormatedMinute" }] }
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
                           
            ]

            // ===============
            

            

            //================
            models.CalenderData.aggregate(aggrQuery).exec( async (error,response) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (response.length < 1) {
                        res.json({
                            isError: false,
                            message: "No data found",
                            statuscode: 404,
                            details: null
                        })
                    }

                    if (response.length > 0) {
                        let i;
                        let totalHours;
                        let totalMinutes;
                        let totalFormatedHour;
                        let totalFormatedMinute;
                        let totalTime;

                        for (i = 0; i < response[0].results.length; i++) {
                           
                           totalHours = response[0].results[i].totalHours;
                           totalMinutes = response[0].results[i].totalMinutes;

                           if (totalHours < 10) {
                               totalFormatedHour = "0"+totalHours;
                           } else {
                               totalFormatedHour = totalHours;
                           }
                           

                           if (totalMinutes < 10) {
                               totalFormatedMinute = "0"+totalMinutes;
                           } else {
                               totalFormatedMinute = totalMinutes;
                           }
                           

                           totalTime = totalFormatedHour+" : "+totalFormatedMinute;
                           response[0].results[i].totalTime = totalTime;

                        }

                        // This function is written to fetch the Vat, Ni and Fee
                        async function fetchVatNiFee() { 
                            console.log("..envinfo"); 
                            
                            return new Promise(function(resolve, reject){
                                
                                EnvInfo.find()
                                .select('ni vat fee')
                                .exec()
                                .then(result => {
                                    
                                    if (result.length>0) {
                                        let response = {
                                            ni: result[0].ni,
                                            vat: result[0].vat,
                                            fee: result[0].fee        
                                        };
                                        resolve({
                                            "isError": false,
                                            "vatNiFee": response
                                        });
                                        
                                    } else {
                                        resolve({
                                            "isError": true,
                                        });
                                    }
                                });
                            })  // END Promise  
                        } //  async function checkUser(toUser) {  

                        // Here, we are inserting the the details of sip call
                        let fetchVatNiFeeStatus = await fetchVatNiFee();

                        if (fetchVatNiFeeStatus.isError == true) {
                            res.json({
                                isError: true,
                                message: "VAT, NI and FEE Not Found",
                                statuscode: 404,
                                details: null
                            });

                            return;
                        } 

                        response[0].results[0].payment.vat = fetchVatNiFeeStatus.vatNiFee.vat;
                        response[0].results[0].payment.ni = fetchVatNiFeeStatus.vatNiFee.ni;
                        response[0].results[0].payment.fee = fetchVatNiFeeStatus.vatNiFee.fee;
                          
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: response[0]
                            // details: response[0].results[0].payment,            
                        })
                    }


                    
                }
            })

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    autometicChechOut: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            let checkOutTime = req.body.checkOutTime ? req.body.checkOutTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOutTime"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Latitude"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination longitude"
            });
            if (!req.body.destinationLat || !req.body.destinationLong || !req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.employerId || !req.body.checkOutTime) { return; }
            let aggrQuery1 = [
                {
                    '$match': {
                        $and: [
                            { 'jobId': jobId },
                            { 'jobDetails.roleWiseAction.roleId': roleId }
                        ]
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery1).exec((error, primaryResult) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                if (primaryResult.length == 0) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1021'],
                        statuscode: 1021,
                        details: null
                    })
                }
                else {
                    let aggrQuery = [
                        {
                            $geoNear: {
                                near: {
                                    type: "Point",
                                    coordinates: [parseFloat(destinationLong), parseFloat(destinationLat)]
                                },
                                distanceField: "dist.locationDistance",
                                maxDistance: primaryResult[0].jobDetails.roleWiseAction[0].locationTrackingRadius,
                                query: { type: "public" },
                                includeLocs: "dist.location",
                                query: {},
                                spherical: true
                            }
                        },
                        {
                            '$match': {
                                $and: [
                                    { 'jobId': jobId },
                                    { 'jobDetails.roleWiseAction.roleId': roleId }
                                ]
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        }
                    ]
                    models.job.aggregate(aggrQuery).exec((err, result) => {
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (result.length == 0) {
                                Request.post({
                                    "headers": { "content-type": "application/json" },
                                    "url": `${config.app.FORMATTED_URL}/api/candidate-job/onsite-checkout`,
                                    "body": JSON.stringify({
                                        "candidateId": candidateId,
                                        "jobId": jobId,
                                        "roleId": roleId,
                                        "employerId": empId,
                                        "checkOutTime": checkOutTime,
                                        "date": date,

                                    })
                                }, (error, response, body) => {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                    if (response) {
                                        res.json({
                                            isError: false,
                                            message: "You have successfully checked out @" + checkOutTime + ".A notification is sent to employer. ",
                                            statuscode: 200,
                                            details: {
                                                "isCheckedIn": false,
                                                "lastJobType": ""
                                            }
                                        })
                                    }
                                })
                            }
                            else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['209'],
                                    statuscode: 209,
                                    details: null
                                });
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getCandidateWiseAttendance:(req, res, next)=>{
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
            let findWhere={
                "candidateId":candidateId,
                "jobId": jobId,
                "roleId": roleId,
              }
                models.CalenderData.find(findWhere, function (err, item) {
                    if(err){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });  
                    }
                    else{
                        if(item==null){
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['207'],
                                statuscode: 207,
                                details: null
                            })
                        }
                        else{
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: item[0]
                            })  
                        }
                    }
                })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },
    getDateWiseCalenderData:(req, res, next)=>{
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
           
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            
           if (!req.body.date||!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
           let findWhere={
            "candidateId":candidateId,
            "jobId": jobId,
            "roleId": roleId,
            "workingDays.date":date,
            }
            models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
                }
                else{
                    if(item==null){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: item.workingDays[0]
                        })  
                    }
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    onlineCheckOut:(req, res, next)=>{
        console.log('in')
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            let checkOutTime = req.body.checkOutTime ? req.body.checkOutTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOutTime"
            });
           if (!req.body.date||!req.body.roleId ||!req.body.jobId || !req.body.candidateId ||!req.body.employerId ||!req.body.checkOutTime ){return;}
           let findWhere={
            "candidateId":candidateId,
            "jobId": jobId,
            "roleId": roleId,
            "workingDays.date":date,
            }
            models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, function (err, item) {
                if(item==null){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['207'],
                        statuscode: 207,
                        details: null
                    })
                }
                else{
                    let show;
                    if(isInteger(item.workingDays[0].finalCheckIn)){
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        show=finalCheckIn.concat(".00")
                    }
                    else{
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        let lengthOffinalCheckIn = finalCheckIn.split('.')[1].length;
                        
                        if(lengthOffinalCheckIn==1){
                            show=finalCheckIn.concat("0")
                        }
                        else{
                            show=item.workingDays[0].finalCheckIn.toString();
                        }
                    }
                    let startTime=Moment(show, "HH:mm:ss");
                    let endTime=Moment(checkOutTime.toString(), "HH:mm:ss");
                    let duration = Moment.duration(endTime.diff(startTime));
                    let hours = parseInt(duration.asHours());
                    let minutes = parseInt(duration.asMinutes())-hours*60;
                    let totalTime=hours + 'h'+ minutes+'m';
                    let totalCollectedMinute=(hours*60)+minutes;
                    let toSet;
                    item.workingDays[0].onSite.map((levelData, levelIndex)=>{
                        if(levelData.onSideId==item.workingDays[0].lastOnSideId){
                            toSet = 'workingDays.$.onSite.'+levelIndex+'.checkOut'
                            models.CalenderData.updateOne({"candidateId":candidateId,
                            "jobId": jobId,
                            "roleId": roleId,
                            "workingDays.date":date,"workingDays": { $elemMatch:{ "onSite": {$elemMatch:{"onSideId":item.workingDays[0].lastOnSideId}}}}}, {$set: {[`${toSet}`]:checkOutTime, "workingDays.$.slotType":'INACTIVE', "workingDays.$.finalCheckOut":checkOutTime,"isCheckedIn":false,"lastJobType":"","workingDays.$.totalTime":totalTime,"workingDays.$.totalTimeInMinutes":totalCollectedMinute} }, (err, doc)=>{
                                if(err){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                                else{
                                    if (doc.nModified == 1){
                                        res.json({
                                            isError: false,
                                            message:"You have successfully checked out @"+checkOutTime+".A notification is sent to employer. ",
                                            statuscode: 200,
                                            details: {
                                                "isCheckedIn": false,
                                                "lastJobType": ""
                                            }
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                           
                        }
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    offlineCheckOut:(req, res, next)=>{
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            let checkOutTime = req.body.checkOutTime ? req.body.checkOutTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOutTime"
            });
           if (!req.body.date||!req.body.roleId ||!req.body.jobId || !req.body.candidateId ||!req.body.employerId ||!req.body.checkOutTime ){return;}
            let findWhere={
                "candidateId":candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date":date,
            }
            
            models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, function (err, item) {
                if(item==null){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['207'],
                        statuscode: 207,
                        details: null
                    })
                }
                else{
                    let show;
                    if(isInteger(item.workingDays[0].finalCheckIn)){
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        show=finalCheckIn.concat(".00")
                    }
                    else{
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        let lengthOffinalCheckIn = finalCheckIn.split('.')[1].length;
                        
                        if(lengthOffinalCheckIn==1){
                            show=finalCheckIn.concat("0")
                        }
                        else{
                            show=item.workingDays[0].finalCheckIn.toString();
                        }
                    }
                    
                    let startTime=Moment(show, "HH:mm:ss");
                    let endTime=Moment(checkOutTime.toString(), "HH:mm:ss");
                    let duration = Moment.duration(endTime.diff(startTime));
                    let hours = parseInt(duration.asHours());
                    let minutes = parseInt(duration.asMinutes())-hours*60;
                    let totalTime=hours + 'h'+ minutes+'m';
                    let totalCollectedMinute=(hours*60)+minutes;
                    let toSet;
                    item.workingDays[0].offSite.map((levelData, levelIndex)=>{
                        if(levelData.offSideId==item.workingDays[0].lastOffSideId){
                            toSet = 'workingDays.$.offSite.'+levelIndex+'.checkOut'
                            models.CalenderData.updateOne({"candidateId":candidateId,
                            "jobId": jobId,
                            "roleId": roleId,
                            "workingDays.date":date,"workingDays": { $elemMatch:{ "offSite": {$elemMatch:{"offSideId":item.workingDays[0].lastOffSideId}}}}}, {$set: {[`${toSet}`]:checkOutTime, "workingDays.$.slotType":'INACTIVE', "workingDays.$.finalCheckOut":checkOutTime,"isCheckedIn":false,"lastJobType":"","workingDays.$.totalTime":totalTime,"workingDays.$.totalTimeInMinutes":totalCollectedMinute} }, (err, doc)=>{
                                if(err){
                                    
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                                else{
                                    if (doc.nModified == 1){
                                        res.json({
                                            isError: false,
                                            message:"You have successfully checked out @"+checkOutTime+".A notification is sent to employer. ",
                                            statuscode: 200,
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                           
                        }
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    offlineCheckIn:(req, res, next)=>{
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let destinationName = req.body.destinationName ? req.body.destinationName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Name"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Latitude"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination longitude"
            });
            let checkinTime = req.body.checkinTime ? req.body.checkinTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkinTime"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            
            if (!req.body.date||!req.body.roleId ||!req.body.jobId || !req.body.candidateId ||!req.body.employerId ||!req.body.destinationName ||!req.body.destinationLat ||!req.body.destinationLong ||!req.body.checkinTime ){return;}
            let findWhere={
                "candidateId":candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date":date
            }
            models.employer.findOne({"employerId":empId}, function (searchError, searchItem) {
                console.log(searchItem.Status.fcmTocken)
                models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, function (err, item) {
                    if(item==null){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: null
                        })
                    }
                    else{
                        let type = "OFF-SITE-ID";
                        let autoOffSiteId;
                        AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                            autoOffSiteId = data;
                        })
                        let offSiteDb;
                        offSiteDb=item.workingDays[0].offSite;
                        offSiteDb.push({
                            "offSideId":autoOffSiteId,
                            "checkIn": checkinTime,
                            "destinationName":destinationName,
                            "geolocation":{"type": "Point",        
                            "coordinates": [ parseFloat(destinationLong), parseFloat(destinationLat)]}
                        });
                        let offSiteData;
                        if(item.workingDays[0].finalCheckIn==0){
                            offSiteData={
                                "workingDays.$.slotType":'OFFSITE',
                                "workingDays.$.lastOffSideId":autoOffSiteId,
                                "workingDays.$.absentStatus":1,
                                "workingDays.$.offSite":offSiteDb,
                                "workingDays.$.finalCheckIn":checkinTime,
                                "isCheckedIn":true,
                                "lastJobType":'OFFSITE'
                            }
                        }
                        else{
                            offSiteData={
                                "workingDays.$.slotType":'OFFSITE',
                                "workingDays.$.lastOffSideId":autoOffSiteId,
                                "workingDays.$.absentStatus":1,
                                "workingDays.$.offSite":offSiteDb,
                                "isCheckedIn":true,
                                "lastJobType":'OFFSITE'
                            }
                        }
                        models.CalenderData.updateOne(findWhere, { $set: 
                            offSiteData}, function(err, affected){
                                if(err){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                                else{
                                    if (affected.nModified == 1) {
                                        //fcm('','',{})
                                        models.authenticate.findOne({'userId':empId}, async function (err, item) {
                                            if(item['mobileNo']==""){
                                                var result = await  sendCheckinNotificationEmail(item['EmailId'],candidateId,jobId,roleId,checkinTime,date,destinationName,"OFFSITE","in")
                                                if(result){
                                                    res.json({
                                                        isError: false,
                                                        message:"You have successfully checked in @"+checkinTime+".A notification is sent to employer. ",
                                                        statuscode: 200,
                                                    })
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                            }
                                            else{
                                                var result = await  sendCheckinNotificationSms(item['mobileNo'],candidateId,jobId,roleId,checkinTime,date,destinationName,"OFFSITE","in")
                                                if(result){
                                                    res.json({
                                                        isError: false,
                                                        message:"You have successfully checked in @"+checkinTime+".A notification is sent to employer. ",
                                                        statuscode: 200,
                                                    })
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                            }
                                        
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                        })
                    }
                })
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    onlineCheckIn:(req, res, next)=>{
        try{
           
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id1"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Latitude"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination longitude"
            });
            let checkinTime = req.body.checkinTime ? req.body.checkinTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkinTime"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            
            if (!req.body.date||!req.body.roleId ||!req.body.jobId || !req.body.candidateId ||!req.body.destinationLat ||!req.body.destinationLong ||!req.body.checkinTime ){return;}
            let aggrQuery = [
                {
                    $geoNear: {
                        near: {
                            type: "Point",
                            coordinates: [parseFloat(destinationLong), parseFloat(destinationLat)]
                        },
                        distanceField: "dist.locationDistance",
                        maxDistance: 50,
                        query: { type: "public" },
                        includeLocs: "dist.location",
                        query: {},
                        spherical: true
                    }
                },
                { '$match': { $and: [
                    {'jobId':jobId},
                    {'jobDetails.roleWiseAction.roleId':roleId}
                ]}}
            ]
            models.employer.findOne({"employerId":empId}, function (searchError, searchItem) {
                //console.log(searchItem.Status.fcmTocken)
                console.log(searchItem)
                models.job.aggregate(aggrQuery).exec((err, result) => {
                    console.log(result)
                    if(err){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    else{
                        if(result.length==0){
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['208'],
                                statuscode: 208,
                                details: null
                            });
                        }
                        else{
                            let findWhere={
                                "candidateId":candidateId,
                                "jobId": jobId,
                                "roleId": roleId,
                                "workingDays.date":date
                            }
                            models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, function (err, item) {
                                if(item==null){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['207'],
                                        statuscode: 207,
                                        details: null
                                    })
                                }
                                else{
                                    let type = "ON-SITE-ID";
                                    let autoOnSiteId;
                                    AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                                        autoOnSiteId = data;
                                    })
                                    let onSiteDb;
                                    onSiteDb=item.workingDays[0].onSite;
                                    onSiteDb.push({
                                        "onSideId":autoOnSiteId,
                                        "checkIn": checkinTime,
                                    
                                    });
                                    let onSiteData;
                                    if(item.workingDays[0].finalCheckIn==0){
                                        onSiteData={
                                            "workingDays.$.slotType":'ONSITE',
                                            "workingDays.$.lastOnSideId":autoOnSiteId,
                                            "workingDays.$.absentStatus":1,
                                            "workingDays.$.onSite":onSiteDb,
                                            "workingDays.$.finalCheckIn":checkinTime,
                                            "isCheckedIn":true,
                                            "lastJobType":"ONSITE"
                                        }
                                    }
                                    else{
                                        onSiteData={
                                            "workingDays.$.slotType":'ONSITE',
                                            "workingDays.$.lastOnSideId":autoOnSiteId,
                                            "workingDays.$.absentStatus":1,
                                            "workingDays.$.onSite":onSiteDb,
                                            "isCheckedIn":true,
                                            "lastJobType":"ONSITE"
                                        }
                                    }
                                    models.CalenderData.updateOne(findWhere, { $set: 
                                        onSiteData}, function(err, affected){
                                            if(err){
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })
                                            }
                                            else{
                                                if (affected.nModified == 1) {
                                                    models.authenticate.findOne({'userId':empId}, async function (err, item) {
                                                        if(item['mobileNo']==""){
                                                            var result = await  sendCheckinNotificationEmail(item['EmailId'],candidateId,jobId,roleId,checkinTime,date,"","ONSITE","in")
                                                            if(result){
                                                                res.json({
                                                                    isError: false,
                                                                    message:"You have successfully checked in @"+checkinTime+".A notification is sent to employer. ",
                                                                    statuscode: 200,
                                                                })
                                                            }
                                                            else{
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                        else{
                                                            var result = await  sendCheckinNotificationSms(item['mobileNo'],candidateId,jobId,roleId,checkinTime,date,"","ONSITE","in")
                                                            if(result){
                                                                res.json({
                                                                    isError: false,
                                                                    message:"You have successfully checked in @"+checkinTime+".A notification is sent to employer. ",
                                                                    statuscode: 200,
                                                                })
                                                            }
                                                            else{
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                    
                                                    })
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                        })

                                }
                            })
                        }
                    }
                })
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
       
    },
    populateCalenderData:(req, res, next)=>{
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            
            if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
            let aggrQuery = [
                { $match : { 'jobId': jobId } } ,
                { $unwind: {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:false}},
                {
                    "$project":{
                        "_id" :0,
                        "setTime": "$jobDetails.roleWiseAction.setTime",
                        "roleId": "$jobDetails.roleWiseAction.roleId",
                        "jobId":jobId,
                    }
                },
                { '$match':{'roleId':roleId}},
            ]
            let querywhere = {
                "jobId": jobId,
                "roleId": roleId,
                "candidateId":candidateId
            };
            /*
            * First Check any collection is created in database or not based on roleid and jobId
            */
            models.CalenderData.findOne(querywhere, function (searchError, searchItem) {
            
                if (searchError) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    /*
                    * if searchItem is null means that no collection is created and This is the first candidate
                    * who doing the job
                    */
                    if(searchItem==null){
                        models.job.aggregate(aggrQuery).exec((error, data) => {
                            /*
                            * Now fetch all dates from the date ranges allocated in job by employer and
                            * create suitable json to populate CalenderData collection
                            */
                            let arrDb=[];
                            data[0].setTime.map( tt => {
   
                                console.log("1",tt)
                                let startDate=tt['startDate']/1000;
                                let endDate=tt['endDate']/1000;
                                var itr = Moment.twix(new Date(Moment.unix(startDate).format("YYYY-MM-DD")),new Date(Moment.unix(endDate).format("YYYY-MM-DD"))).iterate("days");
                                console.log("2",tt,itr)
                                while(itr.hasNext()){
                                let finalDate=itr.next().format("YYYY-MM-DD");
                                console.log("3",finalDate)
                                let timestamp = new Date(finalDate).getTime();
                                console.log("4",timestamp)
                                let theDate = new Date(timestamp);
                                console.log("5",theDate)
                                let dateInGmtFormat = theDate.toGMTString();
                                    arrDb.push(
                                        {   "shiftTime":[{
                                               "shift":tt['shift'],
                                               "startTime":tt['startTime'],
                                               "endTime":tt['endTime']
                                            }
                                            
                                            ],
                                            "date":finalDate,
                                            "dateInTimestampFormat":timestamp,
                                            "dateInGMTFormat":dateInGmtFormat,
                                            "finalCheckIn":0,
                                            "slotType":"",
                                            "finalCheckOut": 0,
                                            "totalTime": "",
                                            "absentStatus":3,
                                            "onSite":[],
                                            "offSite":[],
                                            "extraTime":[]
                                        }
                                    )
                                }
                            })
                            let combinedShiftDb={};
                            for(var i=0;i<arrDb.length;i++){
                                for (var j = i + 1; j < arrDb.length;j++) {
                                    if(arrDb[i]['date']==arrDb[j]['date']){
                                        combinedShiftDb=arrDb[j]['shiftTime'][0];
                                        arrDb[i]['shiftTime'].push(combinedShiftDb)
                                        arrDb.splice(j,1);
                                    }
                                }
                            }
                             globalVarObj={
                                "_id": new mongoose.Types.ObjectId(),
                                "jobId": jobId,
                                "roleId": roleId,
                                "candidateId":candidateId,
                                "workingDays":arrDb,
                                "isCheckedIn":false,
                                "lastJobType":"",
                                "extraDays":[]
                            }
                            models.CalenderData.create(globalVarObj, function (error, data) {
                                if (error) {
                                    console.log(error)
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: "This is your First Day of job.Best Of Luck!!!",
                                        statuscode: 200,
                                        data:data,
                                        details:{
                                            
                                            "isCheckedIn": data.isCheckedIn,
                                            "lastJobType":data.lastJobType
                                          }
                                    })
                                }
                            })
                        })
                    }
                    else{
                        models.CalenderData.findOne(querywhere, function (searchError, searchItem) {
                            res.json({
                                isError: false,
                                message: 'Calender Data is already populated',
                                statuscode: 200,
                                details:{
                                  "isCheckedIn": searchItem.isCheckedIn,
                                  "lastJobType":searchItem.lastJobType
                                }
                            })
                        })
                        
                    }
                    
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    createCalenderData:(req, res, next)=>{
        try{
            let globalVarObj={};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            
            if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
            let aggrQuery = [
                { $match : { 'jobId': jobId } } ,
                { $unwind: {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:false}},
                {
                    "$project":{
                        "_id" :0,
                        "setTime": "$jobDetails.roleWiseAction.setTime",
                        "roleId": "$jobDetails.roleWiseAction.roleId",
                        "jobId":jobId,
                    }
                },
                { '$match':{'roleId':roleId}},
            ]
            let querywhere = {
                "jobId": jobId,
                "roleId": roleId,
            };
            /*
            * First Check any collection is created in database or not based on roleid and jobId
            */
            models.CalenderData.findOne(querywhere, function (searchError, searchItem) {
                if (searchError) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    /*
                    * if searchItem is null means that no collection is created and This is the first candidate
                    * who doing the job
                    */
                    if(searchItem==null){
                        models.job.aggregate(aggrQuery).exec((error, data) => {
                            /*
                            * Now fetch all dates from the date ranges allocated in job by employer and
                            * create suitable json to populate CalenderData collection
                            */
                            let arrDb=[];
                            data[0].setTime.map( tt => {
                                var itr = Moment.twix(new Date(Moment.unix(tt['startDate']).format("YYYY-MM-DD")),new Date(Moment.unix(tt['endDate']).format("YYYY-MM-DD"))).iterate("days");
                                while(itr.hasNext()){
                                let finalDate=itr.next().format("YYYY-MM-DD");
                                let timestamp = new Date(finalDate).getTime();
                                let theDate = new Date(timestamp);
                                let dateInGmtFormat = theDate.toGMTString();
                                    arrDb.push(
                                        {
                                            "date":finalDate,
                                            "dateInTimestampFormat":timestamp,
                                            "dateInGMTFormat":dateInGmtFormat,
                                            "finalCheckIn":0,
                                            "slotType":"",
                                            "finalCheckOut": 0,
                                            "totalTime": 0,
                                            "absentStatus":3,
                                            "onSite":[],
                                            "offSite":[],
                                            "extraTime":[]
                                        }
                                    )
                                }
                            })
                            globalVarObj={
                                "_id": new mongoose.Types.ObjectId(),
                                "jobId": jobId,
                                "roleId": roleId,
                                "attendanceTracking":[
                                    {
                                        "candidateId":candidateId,
                                        "workingDays":arrDb
                                    }
                                ],
                                "extraDays":[]
                            }
                            models.CalenderData.create(globalVarObj, function (error, data) {
                                if (error) {
                                    console.log(error)
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: "This is your First Day of job.Best Of Luck!!!",
                                        statuscode: 200,
                                        details:data
                                    })
                                }
                            })
                        })
                    }
                    else{
                        /*
                        * If it comes in else that means collection is already created and one of the hired candididates
                        * already started working. Now it is to check that the candidate who call the api is the existing 
                        * candidate or the new one.
                        */
                        models.CalenderData.find({'attendanceTracking.candidateId':candidateId }, function (error, searchitem) {
                            if (error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                /*
                                * If control enters here that means that this is new hired candidate apart from existing one.
                                * so we have to create suitable json along withnew candidateId so we can push new candidate details 
                                * with all preselected shift days in 'attendanceTracking' array in existing collection
                                */
                                if(!searchitem.length){
                                    models.job.aggregate(aggrQuery).exec((error, data) => {
                                        let arrDb=[];
                                        data[0].setTime.map( tt => {
                                            var itr = Moment.twix(new Date(Moment.unix(tt['startDate']).format("YYYY-MM-DD")),new Date(Moment.unix(tt['endDate']).format("YYYY-MM-DD"))).iterate("days");
                                            while(itr.hasNext()){
                                                let finalDate=itr.next().format("YYYY-MM-DD");
                                                let timestamp = new Date(finalDate).getTime();
                                                let theDate = new Date(timestamp);
                                                let dateInGmtFormat = theDate.toGMTString();
                                                arrDb.push(
                                                    {
                                                        "date":finalDate,
                                                        "dateInTimestampFormat":timestamp,
                                                        "dateInGMTFormat":dateInGmtFormat,
                                                        "finalCheckIn":0,
                                                        "slotType":"",
                                                        "finalCheckOut": 0,
                                                        "totalTime": 0,
                                                        "absentStatus":3,
                                                        "onSite":[],
                                                        "offSite":[],
                                                        "extraTime":[]
                                                    }
                                                )
                                            }
                                        })
                                        let anotherCandidateTrackingDetails={
                                            "candidateId":candidateId,
                                            "workingDays":arrDb
                                        }
                                        models.CalenderData.updateOne(querywhere, {$push:{'attendanceTracking':anotherCandidateTrackingDetails}}, function (updatedError, updatedResponse) {
                                            if(updatedError){
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })
                                            }
                                            else{
                                                if (updatedResponse.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message:"This is your First Day of job.Best Of Luck!",
                                                        statuscode: 200,
                                                    })
                                                }
                                                else {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                        })
                                    })
                                }
                                else{
                                    /*
                                    * If controls comes here it means the candidate who is responsible
                                    * for calling the api is already started jobs. therefore no action is needed
                                    * apart fron sending a success message
                                    */
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        //details:searchitem
                                    })
                                }
                            }

                        })
                    }
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getAppointments:(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
           
            if (!req.body.candidateId ){return;}
            let aggrQuery = [
                { $match : { 'candidateId': candidateId } } ,
                { $unwind: {path:"$hiredJobs",preserveNullAndEmptyArrays:false}},
                {
                    "$project":{
                        "_id" :0,
                        "roleId": "$hiredJobs.roleId",
                        "employerId": "$hiredJobs.employerId",
                        "jobId": "$hiredJobs.jobId",
                        "candidateId": "$hiredJobs.candidateId"
                    }
                },
                {
                    $lookup: {
                        from: "jobroles",
                        localField: "roleId",
                        foreignField: "jobRoleId",
                        as: "appliedJobs"
                    }
                },
                {
                    "$project":{
                        jobId:1,
                        employerId:1,
                        candidateId:1,
                        appliedJobs : { $arrayElemAt :["$appliedJobs", 0]}
                    }
                },
                {
                    "$project":{
                        "jobRoleName" : "$appliedJobs.jobRoleName",
                        "industryId":"$appliedJobs.industryId",
                        "jobRoleId":"$appliedJobs.jobRoleId",
                        "jobId":"$jobId",
                        "employerId":"$employerId",
                        "candidateId":"$candidateId",
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industryId",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    "$project":{
                        jobRoleName:1,
                        industryId:1,
                        jobRoleId:1,
                        jobId:1,
                        employerId:1,
                        candidateId:1,
                        industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                        
                    }
                },
                {
                    "$project":{
                        "jobRoleName" : "$jobRoleName",
                        "industryId":"$industryId",
                        "jobRoleId":"$jobRoleId",
                        "jobId":"$jobId",
                        "employerId":"$employerId",
                        "candidateId":"$candidateId",
                        "industryName":"$industryDetails.industryName"
                    }
                },
                {
                    $lookup: {
                        from: "jobs",
                        localField: "jobId",
                        foreignField: "jobId",
                        as: "jobDetails"
                    }
                },
                {
                    "$project":{
                        _id:0,
                        jobRoleName:1,
                        industryId:1,
                        jobRoleId:1,
                        jobId:1,
                        employerId:1,
                        candidateId:1,
                        industryName:1,
                        jobDetails:{ $arrayElemAt :["$jobDetails", 0]}
                        
                    }
                }
                ,
                {
                    "$project":{
                        _id:0,
                        jobRoleName:1,
                        industryId:1,
                        jobRoleId:1,
                        jobId:1,
                        employerId:1,
                        candidateId:1,
                        industryName:1,
                        roleWiseAction: {
                            $filter: {
                                input: "$jobDetails.jobDetails.roleWiseAction",
                                as: "item",
                                cond: { $eq: [ "$$item.roleId", "$jobRoleId" ] }
                             }
                        }
                        
                    }
                },
                   {
                    "$project":{
                        "jobRoleName" : "$jobRoleName",
                        "industryId":"$industryId",
                        "jobRoleId":"$jobRoleId",
                        "jobId":"$jobId",
                        "employerId":"$employerId",
                        "candidateId":"$candidateId",
                        "industryName":"$industryName",
                         jobDetails:{ $arrayElemAt :["$roleWiseAction", 0]}
                    }
                },
                {
                    "$project":{
                        "jobRoleName" : "$jobRoleName",
                        "industryId":"$industryId",
                        "jobRoleId":"$jobRoleId",
                        "jobId":"$jobId",
                        "employerId":"$employerId",
                        "candidateId":"$candidateId",
                        "industryName":"$industryName",
                        "description":"$jobDetails.description",
                        "locationName": "$jobDetails.locationName",
                        "location": "$jobDetails.location",
                        "setTime": "$jobDetails.setTime"
                       
                       
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.candidate.aggregate(aggrQuery).exec( (error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if(data.length>0){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: data[0]
                        })
                    }
                    else{
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: []
                        })
                    }
                  
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },
    /**
     * @abstract 
     * DECLINE JOBS
     * first confrim from where jobs details will be removed (joboffer array or jobaccepted array)
     * Based on that details  jobOffer or job accepted details will be moved to job-declined array of candidate collection.
     * paralelly appiliedFrom or acceptList details of job collection will be moved to rejectList array .
    */
    jobDecline:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let declinefrom1 = req.body.declinefrom ? req.body.declinefrom : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Declined From"
            });
            if (!req.body.declinefrom ||!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }
            let findWhere={}
            let declineFrom;
            let deleteFrom;
                switch(declinefrom1){
                case '1':declineFrom='jobOffers';
                         deleteFrom='appiliedFrom';
                        findWhere={
                            'jobOffers.jobId': jobId ,
                            'jobOffers.roleId': roleId ,
                            'candidateId': candidateId ,
                        }
                        break;
                case '2':declineFrom='acceptedJobs';
                         deleteFrom='acceptList';
                        findWhere={
                            'acceptedJobs.jobId': jobId ,
                            'acceptedJobs.roleId': roleId ,
                            'candidateId': candidateId ,
                        }
                        break;
                }
          let findWith = declineFrom+".$" ;
          models.candidate.findOne(findWhere,{ [findWith] : 1 }, function (err, item) {
            let  roleId=item[declineFrom][0].roleId;
            let  employerId=item[declineFrom][0].employerId;
            let  jobId=item[declineFrom][0].jobId;
            let  candidateId=item[declineFrom][0].candidateId;
            let insertedValue={
             "roleId": roleId,"employerId":employerId,"jobId":jobId,"candidateId":candidateId
             }
            models.candidate.updateOne({"candidateId":candidateId},{ "$pull": { [declineFrom]: { "jobId": jobId ,"roleId":roleId} }}, { safe: true, multi:true }, function (deletedderror, deletedresponse) {
                if (deletedderror) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                if (deletedresponse.nModified == 1) {
                    models.candidate.updateOne({ 'candidateId': candidateId }, { $push: { "declinedJobs":insertedValue } }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        if (updatedresponse.nModified == 1) {
                            models.job.updateOne({"jobId":jobId},{ "$pull": { [deleteFrom]: { "roleId": roleId ,"candidateId":candidateId} }}, { safe: true, multi:true }, function (pullederror, pulledresponse) {
                                if (pullederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    if (pulledresponse.nModified == 1) {
                                        let insertedpushValue={
                                            "roleId": roleId,
                                            "candidateId":candidateId
                                        }
                                        models.job.updateOne({"jobId":jobId}, { $push: { "rejectList":insertedpushValue } }, function (pushederror, pushedresponse) {
                                            if (pushederror) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                            else{
                                                if (pushedresponse.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    }) 
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })   
                                                }
                                            }
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })   
                                    }
                                }
                            })
                        }
                        else{
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })   
                        }
                    })
                }
                else{
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })  
                }
            })
        })
    }
    catch (error) {
        console.log(error)
        res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['404'],
            statuscode: 404,
            details: null
        });
        }
    },
    /**
     * @abstract 
     * ACCEPT JOBS
     * during job accept, details of job offers array must be moved to accepted-job array of candidate collection,
     * At the same time removed details of appiliedFrom to acceptList array of job collection
     * at last send sms or mail to employer 
    */
    jobAccept:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }
            let findWhere={
                 'jobOffers.jobId': jobId ,
                 'jobOffers.roleId': roleId ,
                 'candidateId': candidateId ,
            }
            models.candidate.findOne(findWhere,
            { 
                'jobOffers.$': 1,
                'Status': 1 
            }, 
            function (err, item) {

                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                    return;
                }
                
                if (item == null) { // No Item Found
                    res.json({
                        isError: true,
                        message: "No Candiate Found",
                        statuscode: 204,
                        details: null
                    });
                    return;
                }
    
                if (item.Status.isApprovedByAot == false) { 
                    res.json({
                        isError: true,
                        message: "Candidate is not approved by admin yet",
                        statuscode: 204,
                        details: null
                    });
                    return;
                }
                
               
               let  roleId=item.jobOffers[0].roleId;
               let  employerId=item.jobOffers[0].employerId;
               let  jobId=item.jobOffers[0].jobId;
               let  candidateId=item.jobOffers[0].candidateId;
               let insertedValue={
                "roleId": roleId,"employerId":employerId,"jobId":jobId,"candidateId":candidateId,
               }
               models.candidate.updateOne({"candidateId":candidateId},{ "$pull": { "jobOffers": { "jobId": jobId ,"roleId":roleId} }}, { safe: true, multi:true }, function (deletedderror, deletedresponse) {
                        if (deletedderror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        if (deletedresponse.nModified == 1) {
                            models.candidate.updateOne({ 'candidateId': candidateId }, { $push: { "acceptedJobs":insertedValue } }, function (updatederror, updatedresponse) {
                                if (updatederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                if (updatedresponse.nModified == 1) {

                                    models.job.updateOne({"jobId":jobId},{ "$pull": { "appiliedFrom": { "roleId": roleId ,"candidateId":candidateId} }}, { safe: true, multi:true }, function (pullederror, pulledresponse) {
                                        if (pullederror) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        else{
                                            if (pulledresponse.nModified == 1) {
                                                let insertedpushValue={
                                                    "roleId": roleId,
                                                    "candidateId":candidateId
                                                }
                                                models.job.updateOne({"jobId":jobId}, { $push: { "acceptList":insertedpushValue } }, function (pushederror, pushedresponse) {
                                                    if (pushederror) {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                    else{
                                                        if (pushedresponse.nModified == 1) {
                                                             models.authenticate.findOne({'userId':employerId}, async function (err, item) {
                                                                if(item['mobileNo']==""){
                                                                    var result = await  sendNotificationEmail(item['EmailId'],candidateId,jobId,roleId,employerId)
                                                                    if(result){
                                                                        models.candidate.find({"candidateId":candidateId },  function (error, data) {
                                                                           
                                                                            models.employer.find({"employerId":employerId },  function (searchingerror, searchRating) {
                                                                               
                                                                          
                                                                            fcm.sendTocken(searchRating[0].Status.fcmTocken,data[0].fname+" "+data[0].lname+" accepted your job offer")
                                                                            .then(function(nofify){
                                                                                res.json({
                                                                                    isError: false,
                                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                                    statuscode: 200,
                                                                                    details: null
                                                                                })
                                                                            }) 
                                                                            })
                                                                        })
                                                                    }
                                                                    else{
                                                                        res.json({
                                                                            isError: true,
                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                            statuscode: 404,
                                                                            details: null
                                                                        });
                                                                    }
                                                                }
                                                                else{
                                                                    var result = await  sendNotificationSms(item['mobileNo'],candidateId,jobId,roleId,employerId)
                                                                    if(result){
                                                                        models.candidate.find({"candidateId":candidateId },  function (error, data) {
                                                                            models.employer.find({"employerId":employerId },  function (searchingerror, searchRating) {
                                                                               
                                                                                fcm.sendTocken(searchRating[0].Status.fcmTocken,data[0].fname+" "+data[0].lname+" accepted your job offer")
                                                                                .then(function(nofify){
                                                                                    res.json({
                                                                                        isError: false,
                                                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                                                        statuscode: 200,
                                                                                        details: null
                                                                                    })
                                                                                }) 
                                                                           })
                                                                        })
                                                                    }
                                                                    else{
                                                                        res.json({
                                                                            isError: true,
                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                            statuscode: 404,
                                                                            details: null
                                                                        });
                                                                    }
                                                                }
                                                            })
                                                        }
                                                        else{
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                statuscode: 1004,
                                                                details: null
                                                            })  
                                                        }
                                                    }
                                                })
                                            }
                                            else{
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })   
                                            }
                                        }
                                    })
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })   
                                }
                            })
                        }
                        else{
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })  
                        }
                    })
             })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    candidateJobDetails1:(req, res, next)=>{
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
            let aggrQuery = [
                { '$match': { $and: 
                    [ 
                        { 'jobDetails.roleWiseAction.roleId': roleId },
                        { 'jobId': jobId },
                    ]
                 }
                },
                { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                { '$match':{'jobDetails.roleWiseAction.roleId':roleId}},
                {
                    $facet:{
                        "generalInfo":[
                            {
                                "$project":{
                                    "jobDetails" : 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "approvedTo":1,
                                }
                            },
                            {
                                "$project":{
                                    "_id":0,
                                    "roleId" : "$jobDetails.roleWiseAction.roleId",
                                    "jobId" : "$jobId",
                                    "employerId" : "$employerId",
                                    "approvedTo":1,
                                }
                            },
                            {
                                "$project":{
                                    "_id":0,
                                    "roleId" : "$roleId",
                                    "jobId" : "$jobId",
                                    "employerId" : "$employerId",
                                    "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: {$and:[{ $eq: ["$$approvedTo.roleId", "$roleId"] },{ $eq: ["$$approvedTo.candidateId", candidateId] } ]} } },
                                }
                            },
                            {
                                "$project":{
                                    "_id":0,
                                   
                                    "roleId" : 1,
                                    "jobId" : 1,
                                    "employerId" : 1,
                                    "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                }
                            },
                            {
                                "$project":{
                                   
                                    "roleId" : 1,
                                    "jobId" : 1,
                                    "employerId" : 1,
                                    "candidateId":candidateId,
                                    "isOffered":{
                                        $cond: {
                                            "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "candidates",
                                    localField: "candidateId",
                                    foreignField: "candidateId",
                                    as: "candidateDetails"
                                }
                            },
                            {
                                "$project":{
                                    roleId:1,
                                    jobId : 1,
                                    employerId : 1,
                                    candidateId : 1,
                                    isOffered:1,
                                    candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                                }
                             },
                            {
                                "$project":{
                                    "roleId":"$roleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "isOffered":"$isOffered",
                                    "declinedJobs":"$candidateDetails.declinedJobs",
                                    "acceptedJobs":"$candidateDetails.acceptedJobs",
                                    "appliedJobs":"$candidateDetails.appliedJobs",
                                    "hiredJobs":"$candidateDetails.hiredJobs",
                                }
                            },
                            {
                                "$project":{
                                    "roleId":1,
                                    "jobId":1,
                                    "employerId":1,
                                    "candidateId":1,
                                    "isOffered":1,
                                    "appliedJobsArray":{ $filter: { input: "$appliedJobs", as: "appliedJobs", cond: {$and:[{ $eq: ["$$appliedJobs.roleId", "$roleId"] },{ $eq: ["$$appliedJobs.jobId", "$jobId"] } ]} } },
                                    "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: {$and:[{ $eq: ["$$declinedJobs.roleId", "$roleId"] },{ $eq: ["$$declinedJobs.jobId", "$jobId"] } ]} } },
                                    "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: {$and:[{ $eq: ["$$acceptedJobs.roleId", "$roleId"] },{ $eq: ["$$acceptedJobs.jobId", "$jobId"] }  ]} } },
                                    "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: {$and:[{ $eq: ["$$hiredJobs.roleId", "$roleId"] },{ $eq: ["$$hiredJobs.jobId", "$jobId"] }  ]} } },
                                }
                             },
                            {
                                "$project":{
                                    "roleId":1,
                                    "jobId":1,
                                    "employerId":1,
                                    "candidateId":1,
                                    "isOffered":1,
                                    "appliedJobsArray": { $arrayElemAt :["$appliedJobsArray", 0]},
                                    "declinedJobsArray": { $arrayElemAt :["$declinedJobsArray", 0]},
                                    "acceptedJobsArray": { $arrayElemAt :["$acceptedJobsArray", 0]},
                                    "hiredJobsArray": { $arrayElemAt :["$hiredJobsArray", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "roleId":1,
                                    "jobId":1,
                                    "employerId":1,
                                    "candidateId":1,
                                    "isOffered":1,
                                    "isHired":{
                                        $cond: {
                                            "if": { "$eq": [ "$hiredJobsArray.roleId", "$roleId"] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                    "isApplied":{
                                        $cond: {
                                            "if": { "$eq": [ "$appliedJobsArray.roleId", "$roleId"] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                    "isDeclined":{
                                        $cond: {
                                            "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                    "isAccepted":{
                                        $cond: {
                                            "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                }
                            }
                        ],
                        "jobInfo":[
                            {
                                "$project":{
                                   "jobDetails" : 1,
                                }
                            },
                            {
                                "$project":{
                                   "_id":0,
                                   "skills": "$jobDetails.roleWiseAction.skills",
                                   "roleName" : "$jobDetails.roleWiseAction.roleName",
                                   "jobType" : "$jobDetails.roleWiseAction.jobType",
                                   "payType" : "$jobDetails.roleWiseAction.payType",
                                   "pay" : "$jobDetails.roleWiseAction.pay",
                                   "noOfStuff":"$jobDetails.roleWiseAction.noOfStuff",
                                   "industry":"$jobDetails.industry",
                                   "setTime":"$jobDetails.roleWiseAction.setTime",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industry",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                   skills:1,
                                   roleName : 1,
                                   jobType : 1,
                                   payType : 1,
                                   pay : 1,
                                   noOfStuff:1,
                                   industry:1,
                                   setTime:1,
                                   industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "setTime":"$setTime",
                                    "skills":"$skills",
                                   "roleName" : "$roleName",
                                   "jobType" : "$jobType",
                                   "payType" : "$payType",
                                   "pay" : "$pay",
                                   "noOfStuff":"$noOfStuff",
                                   "industry":"$industry",
                                   "industryName":"$industryDetails.industryName"
                                }
                            },
                            { $unwind:  {path:"$skills",preserveNullAndEmptyArrays:true} },
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            {
                                $project: {
                                    "setTime":1,
                                    "roleName": 1,
                                    "jobType": 1,
                                    "payType": 1,
                                    "pay": 1,
                                    "noOfStuff": 1,
                                    "industry": 1,
                                    "industryName": 1,
                                    "skillDetails": {
                                        $arrayElemAt: ["$skillDetails", 0]
                                    }
                                }
                            },
                            {
                                $group: {
                                    "_id": {
                                        "industryId": "$industry"
                                    },
                                    "setTime": {
                                        $first: "$setTime"
                                    },
                                    "roleName": {
                                        $first: "$roleName"
                                    },
                                    "jobType": {
                                        $first: "$jobType"
                                    },
                                    "payType": {
                                        $first: "$payType"
                                    },
                                    "pay": {
                                        $first: "$pay"
                                    },
                                    "noOfStuff": {
                                        $first: "$noOfStuff"
                                    },
                                    "industry": {
                                        $first: "$industry"
                                    },
                                    "industryName": {
                                        $first: "$industryName"
                                    },
                                    "skillDetails": {
                                        $push: "$skillDetails"
                                    }
                                }
                            }
                        ],
                        "locationInfo":[
                            {
                                "$project":{
                                   "jobDetails" : 1,
                                }
                            },
                            {
                                "$project":{
                                   "_id":0,
                                   "location" : "$jobDetails.roleWiseAction.location",
                                   "description":"$jobDetails.roleWiseAction.description",
                                  "locationName":"$jobDetails.roleWiseAction.locationName",
                                }
                            },
                        ]
                    }
                },
                {
                    $project : {
                        locationInfo:{ $arrayElemAt : ["$locationInfo",0]},
                        jobInfo:{ $arrayElemAt : ["$jobInfo",0]},
                        generalInfo:{ $arrayElemAt : ["$generalInfo",0]},
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((err, result) => {
                console.log(err)
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* candidate details based on old structure*/
    candidateJobDetails:(req, res, next)=>{
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            if (!req.body.roleId ||!req.body.jobId || !req.body.candidateId ){return;}
            let aggrQuery = [
                { '$match': { $and: 
                    [ 
                        { 'jobDetails.roleWiseAction.roleId': roleId },
                        { 'jobId': jobId },
                    ]
                 }
                },
                { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                { '$match':{'jobDetails.roleWiseAction.roleId':roleId}},
                {
                    $facet:{
                        "generalInfo":[
                            {
                                "$project":{
                                    "jobDetails" : 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "approvedTo":1,
                                }
                            },
                            {
                                "$project":{
                                    "_id":0,
                                    "roleId" : "$jobDetails.roleWiseAction.roleId",
                                    "jobId" : "$jobId",
                                    "employerId" : "$employerId",
                                    "approvedTo":1,
                                }
                            },
                            {
                                "$project":{
                                    "_id":0,
                                    "roleId" : "$roleId",
                                    "jobId" : "$jobId",
                                    "employerId" : "$employerId",
                                    "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: {$and:[{ $eq: ["$$approvedTo.roleId", "$roleId"] },{ $eq: ["$$approvedTo.candidateId", candidateId] } ]} } },
                                }
                            },
                            {
                                "$project":{
                                    "_id":0,
                                   
                                    "roleId" : 1,
                                    "jobId" : 1,
                                    "employerId" : 1,
                                    "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                }
                            },
                            {
                                "$project":{
                                   
                                    "roleId" : 1,
                                    "jobId" : 1,
                                    "employerId" : 1,
                                    "candidateId":candidateId,
                                    "isOffered":{
                                        $cond: {
                                            "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "candidates",
                                    localField: "candidateId",
                                    foreignField: "candidateId",
                                    as: "candidateDetails"
                                }
                            },
                            {
                                "$project":{
                                    roleId:1,
                                    jobId : 1,
                                    employerId : 1,
                                    candidateId : 1,
                                    isOffered:1,
                                    candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                                }
                             },
                            {
                                "$project":{
                                    "roleId":"$roleId",
                                    "jobId":"$jobId",
                                    "employerId":"$employerId",
                                    "candidateId":"$candidateId",
                                    "isOffered":"$isOffered",
                                    "declinedJobs":"$candidateDetails.declinedJobs",
                                    "acceptedJobs":"$candidateDetails.acceptedJobs",
                                    "appliedJobs":"$candidateDetails.appliedJobs",
                                    "hiredJobs":"$candidateDetails.hiredJobs",
                                }
                            },
                            {
                                "$project":{
                                    "roleId":1,
                                    "jobId":1,
                                    "employerId":1,
                                    "candidateId":1,
                                    "isOffered":1,
                                    "appliedJobsArray":{ $filter: { input: "$appliedJobs", as: "appliedJobs", cond: {$and:[{ $eq: ["$$appliedJobs.roleId", "$roleId"] },{ $eq: ["$$appliedJobs.jobId", "$jobId"] } ]} } },
                                    "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: {$and:[{ $eq: ["$$declinedJobs.roleId", "$roleId"] },{ $eq: ["$$declinedJobs.jobId", "$jobId"] } ]} } },
                                    "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: {$and:[{ $eq: ["$$acceptedJobs.roleId", "$roleId"] },{ $eq: ["$$acceptedJobs.jobId", "$jobId"] }  ]} } },
                                    "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: {$and:[{ $eq: ["$$hiredJobs.roleId", "$roleId"] },{ $eq: ["$$hiredJobs.jobId", "$jobId"] }  ]} } },
                                }
                            },
                            {
                                "$project":{
                                    "roleId":1,
                                    "jobId":1,
                                    "employerId":1,
                                    "candidateId":1,
                                    "isOffered":1,
                                    "appliedJobsArray": { $arrayElemAt :["$appliedJobsArray", 0]},
                                    "declinedJobsArray": { $arrayElemAt :["$declinedJobsArray", 0]},
                                    "acceptedJobsArray": { $arrayElemAt :["$acceptedJobsArray", 0]},
                                    "hiredJobsArray": { $arrayElemAt :["$hiredJobsArray", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "roleId":1,
                                    "jobId":1,
                                    "employerId":1,
                                    "candidateId":1,
                                    "isOffered":1,
                                    "isHired":{
                                        $cond: {
                                            "if": { "$eq": [ "$hiredJobsArray.roleId", "$roleId"] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                    "isApplied":{
                                        $cond: {
                                            "if": { "$eq": [ "$appliedJobsArray.roleId", "$roleId"] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                    "isDeclined":{
                                        $cond: {
                                            "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                    "isAccepted":{
                                        $cond: {
                                            "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                            "then": true,
                                             "else": false
                                        }
                                    },
                                }
                            }
                        ],
                        "jobInfo":[
                            {
                                "$project":{
                                   "jobDetails" : 1,
                                   "setTime":1
                                }
                            },
                            {
                                "$project":{
                                   "_id":0,
                                   "skills": "$jobDetails.roleWiseAction.skills",
                                   "roleName" : "$jobDetails.roleWiseAction.roleName",
                                   "jobType" : "$jobDetails.jobType",
                                   "payType" : "$jobDetails.payType",
                                   "pay" : "$jobDetails.pay",
                                   "noOfStuff":"$jobDetails.noOfStuff",
                                   "industry":"$jobDetails.industry",
                                   "setTime":"$setTime",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industry",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project":{
                                   skills:1,
                                   roleName : 1,
                                   jobType : 1,
                                   payType : 1,
                                   pay : 1,
                                   noOfStuff:1,
                                   industry:1,
                                   setTime:1,
                                   industryDetails:{ $arrayElemAt :["$industryDetails", 0]}
                                }
                            },
                            {
                                "$project":{
                                    "setTime":"$setTime",
                                    "skills":"$skills",
                                   "roleName" : "$roleName",
                                   "jobType" : "$jobType",
                                   "payType" : "$payType",
                                   "pay" : "$pay",
                                   "noOfStuff":"$noOfStuff",
                                   "industry":"$industry",
                                   "industryName":"$industryDetails.industryName"
                                }
                            },
                            { $unwind:  {path:"$skills",preserveNullAndEmptyArrays:true} },
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            {
                                $project: {
                                    "setTime":1,
                                    "roleName": 1,
                                    "jobType": 1,
                                    "payType": 1,
                                    "pay": 1,
                                    "noOfStuff": 1,
                                    "industry": 1,
                                    "industryName": 1,
                                    "skillDetails": {
                                        $arrayElemAt: ["$skillDetails", 0]
                                    }
                                }
                            },
                            {
                                $group: {
                                    "_id": {
                                        "industryId": "$industry"
                                    },
                                    "setTime": {
                                        $first: "$setTime"
                                    },
                                    "roleName": {
                                        $first: "$roleName"
                                    },
                                    "jobType": {
                                        $first: "$jobType"
                                    },
                                    "payType": {
                                        $first: "$payType"
                                    },
                                    "pay": {
                                        $first: "$pay"
                                    },
                                    "noOfStuff": {
                                        $first: "$noOfStuff"
                                    },
                                    "industry": {
                                        $first: "$industry"
                                    },
                                    "industryName": {
                                        $first: "$industryName"
                                    },
                                    "skillDetails": {
                                        $push: "$skillDetails"
                                    }
                                }
                            }
                        ],
                        "locationInfo":[
                            {
                                "$project":{
                                   "location" : 1,
                                   "description":1,
                                  "locationName":1,
                                }
                            },
                        ]
                    }
                },
                {
                    $project : {
                        locationInfo:{ $arrayElemAt : ["$locationInfo",0]},
                        jobInfo:{ $arrayElemAt : ["$jobInfo",0]},
                        generalInfo:{ $arrayElemAt : ["$generalInfo",0]},
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((err, result) => {
                console.log(err)
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * APPLY JOBS
     * during apply jobs stored details into appiliedFrom array in job collection
     * at the same time stored the same details into appliedJobs array in candidate collection .
     * at last send sms or mail to employer
    */
    applyJobs:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }
            let updateWith={
                'roleId':roleId,
                'candidateId':candidateId
            }
            let findWhere={
                'appiliedFrom.roleId': roleId ,
                'appiliedFrom.candidateId': candidateId,
                'jobId': jobId
            }

             // ==================================================
                  // this function is used to check whether candidate is authorised or not // by Ankur on 06-02-20
                  async function checkCandidateAuthorised(candidateId) {  // start of checkCandidateAuthorised
                      
                      return new Promise(function(resolve, reject){
                                      
                          let aggrQuery = [
                              {
                                $match: {
                                  'candidateId': candidateId,
                                } 
                              },
                              {
                                $project: {
                                    "Status": 1                            
                                }
                              },
                              
                          ];

                          models.candidate.aggregate(aggrQuery, function (err, response) {
                          
                              if (err) {
                                  resolve({"isError": true});
                              } else {

                                  if (response.length > 0) {
                                      resolve({
                                        "isError": false,
                                        "isExist": true, 
                                        "candidateDetail": response
                                      });
                                  } else{
                                      resolve({
                                        "isError": false,
                                        "isExist": false,
                                      });
                                  }
                              }
                              
                          }); 
                      })    
                  } // END of checkCandidateAuthorised

            // ===================================================
            models.job.findOne(findWhere,{ 'appiliedFrom.$': 1 }, async function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    // ====================================
                    // Coded by Ankur on 06-02-20
                    let checkCandidateAuthorisedStatus = await checkCandidateAuthorised(candidateId);
                    if (checkCandidateAuthorisedStatus.isError == true || checkCandidateAuthorisedStatus.isExist == false) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                        return; 
                    }

                    if (checkCandidateAuthorisedStatus.candidateDetail[0].Status.isApprovedByAot == false) {
                        res.json({
                            isError: true,
                            message: 'Candidate is not approved by admin yet',
                            statuscode: 1026,
                            details: null
                        });
                        return; 
                    }
                    //===========================================
                    
                    if(item){
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['1026'],
                            statuscode: 1026,
                            details: null
                        })
                    }
                    else{                    
                        models.job.updateOne(
                            { 'employerId': employerId,'jobId':jobId },
                            {$push: {'appiliedFrom':updateWith}}, 
                            function (error, response) {
                            if(error){
                                console.log("error 1",error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                let insertedValue={
                                    "roleId": roleId,
                                    "employerId":employerId,
                                    "jobId":jobId,
                                    "candidateId":candidateId
                                }
                                if (response.nModified == 1) {
                                    models.candidate.updateOne({ 'candidateId': candidateId }, 
                                        { $push: { "appliedJobs":insertedValue } }, 
                                        function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {
                                        if (appliedJobsUpdatedError) {
                                            console.log("error 2",appliedJobsUpdatedError)
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        if (appliedJobsUpdatedresponse.nModified == 1) {
                                            models.authenticate.findOne({'userId':employerId}, async function (err, item) {
                                                if(item['mobileNo']==""){
                                                    var result = await  sendNotificationEmail(item['EmailId'],candidateId,jobId,roleId,employerId)
                                                    if(result){
                                                        
                                                        models.candidate.find({"candidateId":candidateId },  function (error, data) {
                                                                           
                                                            models.employer.find({"employerId":employerId },  function (searchingerror, searchRating) {
                                                               
                                                          
                                                            fcm.sendTocken(searchRating[0].Status.fcmTocken,data[0].fname+" "+data[0].lname+" accepted your job")
                                                            .then(function(nofify){
                                                                res.json({
                                                                    isError: false,
                                                                    message: errorMsgJSON['ResponseMsg']['1029'],
                                                                    statuscode: 1029,
                                                                    details: null
                                                                })
                                                            }) 
                                                            })
                                                        })
                                                    }
                                                        else{
                                                        console.log("error 3",err)
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                }
                                                else{
                                                    var result = await  sendNotificationSms(item['mobileNo'],candidateId,jobId,roleId,employerId)
                                                    if(result){
                                                        models.candidate.find({"candidateId":candidateId },  function (error, data) {
                                                                           
                                                            models.employer.find({"employerId":employerId },  function (searchingerror, searchRating) {
                                                               
                                                          
                                                            fcm.sendTocken(searchRating[0].Status.fcmTocken,data[0].fname+" "+data[0].lname+" accepted your job")
                                                            .then(function(nofify){
                                                                res.json({
                                                                    isError: false,
                                                                    message: errorMsgJSON['ResponseMsg']['1029'],
                                                                    statuscode: 1029,
                                                                    details: null
                                                                })
                                                            }) 
                                                            })
                                                        })
                                                    }
                                                    else{
                                                        console.log("error 4",err)
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                }
                                            })
                                        }
                                        else{
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            }) 
                                        }
                                    })
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    }) 
                                }
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            console.log("error 5",error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * SEARCH JOBS
    */
    searchJobs1:(req, res, next)=>{
        try{
            console.log(req.body);
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let nearbypageno = req.body.nearbypageno ? (parseInt(req.body.nearbypageno == 0 ? 0 : parseInt(req.body.nearbypageno) - 1)) : 0;
            let nearbyperPage = req.body.nearbyperpage ? parseInt(req.body.nearbyperpage) : 10;
            let outsidepageno = req.body.outsidepageno ? (parseInt(req.body.outsidepageno == 0 ? 0 : parseInt(req.body.outsidepageno) - 1)) : 0;
            let outsideperPage = req.body.outsideperpage ? parseInt(req.body.outsideperpage) : 10;
            let industryId = req.body.industryId ? req.body.industryId :'';
            let roleId = req.body.roleId ? req.body.roleId :'';
            let jobType = req.body.jobType ? req.body.jobType : '';
            let payType = req.body.payType ? req.body.payType : '';
            let skillValues = req.body.skillValues ? req.body.skillValues : '';
            let candidateLat = req.body.candidateLat ? req.body.candidateLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Latitude"
            });;
            let candidateLong = req.body.candidateLong ? req.body.candidateLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate longitude"
            });;
            let roleIds= roleId;
            let skillValuesDetails = skillValues;
            let payTypeValues = payType;
            let jobTypeValues = jobType;
            console.log(payTypeValues);
            console.log(jobTypeValues)
            if (!req.body.candidateId){return;}
            models.candidate.find({'candidateId':candidateId}, function (err, item) {
                userPrefDist = item[0]['distance'];
                distanceInMeter=userPrefDist*1000;
                console.log(distanceInMeter)
                if(candidateLong==''||candidateLat==''){
                    candidateLongDetails=item[0]['geolocation']['coordinates'][0];
                    candidateLatDetails=item[0]['geolocation']['coordinates'][1];
                }

                else{
                    candidateLongDetails=candidateLong;
                    candidateLatDetails=candidateLat;
                }
                console.log(candidateLongDetails,candidateLatDetails,userPrefDist)
                let aggrQuery = [
                {
                    $geoNear: {
                        near: {
                            type: "Point",
                            coordinates: [parseFloat(candidateLongDetails), parseFloat(candidateLatDetails)]
                        },
                        distanceField: "dist.locationDistance",
                        maxDistance: parseInt(distanceInMeter),
                        query: { type: "public" },
                        includeLocs: "dist.location",
                        query: {},
                        spherical: true
                    }
                },
                { '$match': { $and: [ 
                        industryId ? { 'jobDetails.industry': industryId }:{},
                        roleIds?{'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{},
                        jobTypeValues? { 'jobDetails.roleWiseAction.jobType': {$in: jobTypeValues}} : {},
                        payTypeValues? { 'jobDetails.roleWiseAction.payType': {$in: payTypeValues}}:{},
                        skillValuesDetails?{ 'jobDetails.roleWiseAction.skills':{$in: skillValuesDetails } }:{}
                    ]}
                },
                { $unwind : "$jobDetails.roleWiseAction" },
                 { '$match':roleIds ? { 'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{}},
                {
                    $project : {
                    '_id' : 0,
                    'employerId':1,
                    'jobDetails':1,
                    'jobId': 1,
                    'appiliedFrom':1,
                    'approvedTo':1,
                    'dist':1
                    }
                },
                {
                    "$project":{
                        "employerId":"$employerId",
                        "jobDetails":"$jobDetails",
                        "jobId": "$jobId",
                        "dist":"$dist",
                        "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                    }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":candidateId,
                        "isOffered":{
                            $cond: {
                                "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                "then": true,
                                "else": false
                            }
                        }
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                    }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":candidateId,
                        "isOffered":1,
                        "candidateDetails":{ $arrayElemAt :["$candidateDetails", 0]}
                       
                    }
                },
                {
                    "$project":{
                        "employerId":"$employerId",
                        "jobDetails":"$jobDetails",
                        "jobId": "$jobId",
                        "dist":"$dist",
                        "candidateId":"$candidateId",
                        "isOffered":"$isOffered",
                        "declinedJobs":"$candidateDetails.declinedJobs",
                        "acceptedJobs":"$candidateDetails.acceptedJobs",
                        "appliedJobs":"$candidateDetails.appliedJobs",
                        "hiredJobs":"$candidateDetails.hiredJobs",
                    }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: { $eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: { $eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "appliedJobsArray": { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: { $eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                    }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "declinedJobsArray": { $filter: { input: "$declinedJobsArray", as: "declinedJobsArray", cond: { $eq: ["$$declinedJobsArray.jobId", "$jobId"] } } },
                        "acceptedJobsArray": { $filter: { input: "$acceptedJobsArray", as: "acceptedJobsArray", cond: { $eq: ["$$acceptedJobsArray.jobId", "$jobId"] } } },
                        "appliedJobsArray": { $filter: { input: "$appliedJobsArray", as: "appliedJobsArray", cond: { $eq: ["$$appliedJobsArray.jobId", "$jobId"] } } },
                        "hiredJobsArray": { $filter: { input: "$hiredJobsArray", as: "hiredJobsArray", cond: { $eq: ["$$hiredJobsArray.jobId", "$jobId"] } } },
                    }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "declinedJobsArray":{ $arrayElemAt :["$declinedJobsArray", 0]},
                        "acceptedJobsArray":{ $arrayElemAt :["$acceptedJobsArray", 0]},
                        "appliedJobsArray":{ $arrayElemAt :["$appliedJobsArray", 0]},
                        "hiredJobsArray":{ $arrayElemAt :["$hiredJobsArray", 0]}
                    }
                },
                {
                    "$project":{
                        "employerId":1,
                        "jobDetails":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "status":{
                            "isApplied":{
                                $cond: {
                                    "if": { "$eq": [ "$appliedJobsArray.candidateId", candidateId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                           "isHired":{
                            $cond: {
                                "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                "then": true,
                                 "else": false
                                }
                            },
                            "isOffered":"$isOffered",
                            "isDeclined":{
                                $cond: {
                                    "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            "isAccepted":{
                                $cond: {
                                    "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                    "then": true,
                                    "else": false
                                }
                            },
                        },
                    }
                },
                { 
                    $match: {
                        $or:[{
                            "status.isApplied": false,
                            "status.isHired": false,
                            "status.isOffered": false,
                            "status.isDeclined": false,
                            "status.isAccepted": false
                        }]
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", nearbyperPage ] } },
                        'results': {
                            $slice: [
                                '$results', nearbypageno * nearbyperPage , nearbyperPage
                            ]
                        }
                    }
                }
            ]
                models.job.aggregate(aggrQuery).exec((err, result) => {
                    console.log(result[0])
                    console.log(err)
                    if(err){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    else{
                        if(result.length!==0){
                            let aggrQuery1 = [
                                { '$match': { $and: [ 
                                        industryId ? { 'jobDetails.industry': industryId }:{},
                                        roleIds?{'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{},
                                        jobTypeValues? { 'jobDetails.roleWiseAction.jobType': {$in: jobTypeValues}} : {},
                                        payTypeValues? { 'jobDetails.roleWiseAction.payType': {$in: payTypeValues}}:{},
                                        skillValuesDetails?{ 'jobDetails.roleWiseAction.skills':{$in: skillValuesDetails } }:{}
                                    ]}
                                },
                                { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                               
                                { '$match':roleIds ? { 'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{}},
                                { $match :{ 'jobDetails.roleWiseAction.location.coordinates':{$nin:result[0].results[0].jobDetails.roleWiseAction['location']['coordinates']}}} ,
                                {
                                    $project : {
                                        '_id' : 0,
                                        'employerId':1,
                                        'jobDetails':1,
                                        'jobId': 1,
                                        'appiliedFrom':1,
                                        'approvedTo':1,
                                        'dist':1
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "jobId": "$jobId",
                                        "dist":"$dist",
                                        "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                       
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":candidateId,
                                        "isOffered":{
                                            $cond: {
                                                "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":candidateId,
                                        "isOffered":1,
                                        "candidateDetails":{ $arrayElemAt :["$candidateDetails", 0]}
                                       
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "jobId": "$jobId",
                                        "dist":"$dist",
                                        "candidateId":"$candidateId",
                                        "isOffered":"$isOffered",
                                        "declinedJobs":"$candidateDetails.declinedJobs",
                                        "acceptedJobs":"$candidateDetails.acceptedJobs",
                                        "appliedJobs":"$candidateDetails.appliedJobs",
                                        "hiredJobs":"$candidateDetails.hiredJobs",
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: { $eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: { $eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: { $eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobsArray", as: "declinedJobsArray", cond: { $eq: ["$$declinedJobsArray.jobId", "$jobId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobsArray", as: "acceptedJobsArray", cond: { $eq: ["$$acceptedJobsArray.jobId", "$jobId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobsArray", as: "appliedJobsArray", cond: { $eq: ["$$appliedJobsArray.jobId", "$jobId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobsArray", as: "hiredJobsArray", cond: { $eq: ["$$hiredJobsArray.jobId", "$jobId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray":{ $arrayElemAt :["$declinedJobsArray", 0]},
                                        "acceptedJobsArray":{ $arrayElemAt :["$acceptedJobsArray", 0]},
                                        "appliedJobsArray":{ $arrayElemAt :["$appliedJobsArray", 0]},
                                        "hiredJobsArray":{ $arrayElemAt :["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "status":{
                                            "isApplied":{
                                                $cond: {
                                                    "if": { "$eq": [ "$appliedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                           "isHired":{
                                            $cond: {
                                                "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                                "then": true,
                                                 "else": false
                                                }
                                            },
                                            "isOffered":"$isOffered",
                                            "isDeclined":{
                                                $cond: {
                                                    "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                            "isAccepted":{
                                                $cond: {
                                                    "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                { 
                                    $match: {
                                        $or:[{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", outsideperPage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage , outsideperPage
                                            ]
                                        }
                                    }
                                }
                                
                           ]
                            models.job.aggregate(aggrQuery1).exec((err, data) => {
                                console.log('............',data)
                                if(err){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: data[0],
                                        nearByJobs: result[0],
                                    })
                                }
                            })
                        }
                        else{
                            
                            let aggrQuery2 = [
                                { '$match': { $and: [ 
                                    industryId ? { 'jobDetails.industry': industryId }:{},
                                    roleIds?{'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{},
                                    jobTypeValues? { 'jobDetails.roleWiseAction.jobType': {$in: jobTypeValues}} : {},
                                    payTypeValues? { 'jobDetails.roleWiseAction.payType': {$in: payTypeValues}}:{},
                                    skillValuesDetails?{ 'jobDetails.roleWiseAction.skills':{$in: skillValuesDetails } }:{}
                                    ]}
                                },
                                { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                                { '$match':roleIds ? { 'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{}},
                                {
                                    $project : {
                                        '_id' : 0,
                                        'employerId':1,
                                        'jobDetails':1,
                                        'jobId': 1,
                                        'appiliedFrom':1,
                                        'approvedTo':1,
                                        'dist':1
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "jobId": "$jobId",
                                        "dist":"$dist",
                                        "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                       
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":candidateId,
                                        "isOffered":{
                                            $cond: {
                                                "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":candidateId,
                                        "isOffered":1,
                                        "candidateDetails":{ $arrayElemAt :["$candidateDetails", 0]}
                                       
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "jobId": "$jobId",
                                        "dist":"$dist",
                                        "candidateId":"$candidateId",
                                        "isOffered":"$isOffered",
                                        "declinedJobs":"$candidateDetails.declinedJobs",
                                        "acceptedJobs":"$candidateDetails.acceptedJobs",
                                        "appliedJobs":"$candidateDetails.appliedJobs",
                                        "hiredJobs":"$candidateDetails.hiredJobs",
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: { $eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: { $eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: { $eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobsArray", as: "declinedJobsArray", cond: { $eq: ["$$declinedJobsArray.jobId", "$jobId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobsArray", as: "acceptedJobsArray", cond: { $eq: ["$$acceptedJobsArray.jobId", "$jobId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobsArray", as: "appliedJobsArray", cond: { $eq: ["$$appliedJobsArray.jobId", "$jobId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobsArray", as: "hiredJobsArray", cond: { $eq: ["$$hiredJobsArray.jobId", "$jobId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray":{ $arrayElemAt :["$declinedJobsArray", 0]},
                                        "acceptedJobsArray":{ $arrayElemAt :["$acceptedJobsArray", 0]},
                                        "appliedJobsArray":{ $arrayElemAt :["$appliedJobsArray", 0]},
                                        "hiredJobsArray":{ $arrayElemAt :["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "employerId":1,
                                        "jobDetails":1,
                                        "jobId": 1,
                                        "dist":1,
                                        "candidateId":1,
                                        "status":{
                                            "isApplied":{
                                                $cond: {
                                                    "if": { "$eq": [ "$appliedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                           "isHired":{
                                            $cond: {
                                                "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                                "then": true,
                                                 "else": false
                                                }
                                            },
                                            "isOffered":"$isOffered",
                                            "isDeclined":{
                                                $cond: {
                                                    "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                            "isAccepted":{
                                                $cond: {
                                                    "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                { 
                                    $match: {
                                        $or:[{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", outsideperPage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage , outsideperPage
                                            ]
                                        }
                                    }
                                }
                            ]
                            models.job.aggregate(aggrQuery2).exec((error, outsidedata) => {
                                if(error){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: outsidedata[0],
                                        nearByJobs: result[0]
                                    })
                                }
                                
                            })
                        }
                    }
                })
            })            
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    test:(req, res, next)=>{
        console.log("hit")
      fcm.sendTocken("dhi5VZQDqKA:APA91bGJCOW_jz3EmAX4m78_scnkMV2nmGq0xCedTSEzmMXobsoeje0qtHa6Py3l9s0L9xK75FKHE2QbHrUr6PpVsQ0iRqOKAeeqxUEoShwRFAXQX6uhngrGbtojp3qqX7BZ-lyCEBF_")
      .then(function(nofify){
         console.log(nofify)
      })
     
    },
    /*
    search jobs for old structure is below
    */
    searchJobs:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let nearbypageno = req.body.nearbypageno ? (parseInt(req.body.nearbypageno == 0 ? 0 : parseInt(req.body.nearbypageno) - 1)) : 0;
            let nearbyperPage = req.body.nearbyperpage ? parseInt(req.body.nearbyperpage) : 10;
            let outsidepageno = req.body.outsidepageno ? (parseInt(req.body.outsidepageno == 0 ? 0 : parseInt(req.body.outsidepageno) - 1)) : 0;
            let outsideperPage = req.body.outsideperpage ? parseInt(req.body.outsideperpage) : 10;
            let industryId = req.body.industryId ? req.body.industryId :'';
            let roleId = req.body.roleId ? req.body.roleId :'';
            let jobType = req.body.jobType ? req.body.jobType : '';
            let payType = req.body.payType ? req.body.payType : '';
            let skillValues = req.body.skillValues ? req.body.skillValues : '';
            let candidateLat = req.body.candidateLat ? req.body.candidateLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Latitude"
            });;
            let candidateLong = req.body.candidateLong ? req.body.candidateLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate longitude"
            });;
            let roleIds= roleId;
            let skillValuesDetails = skillValues;
            let payTypeValues = payType;
            let jobTypeValues = jobType;
            if (!req.body.candidateId){return;}
            models.candidate.find({'candidateId':candidateId}, function (err, item) {
                userPrefDist = item[0]['distance'];
                distanceInMeter=userPrefDist*1000;
                console.log(distanceInMeter)
                if(candidateLong==''||candidateLat==''){
                    candidateLongDetails=item[0]['geolocation']['coordinates'][0];
                    candidateLatDetails=item[0]['geolocation']['coordinates'][1];
                }
                else{
                    candidateLongDetails=candidateLong;
                    candidateLatDetails=candidateLat;
                }
                console.log(candidateLongDetails,candidateLatDetails,userPrefDist)
                let aggrQuery = [
                {
                    $geoNear: {
                        near: {
                            type: "Point",
                            coordinates: [parseFloat(candidateLongDetails), parseFloat(candidateLatDetails)]
                        },
                        distanceField: "dist.locationDistance",
                        maxDistance: parseInt(distanceInMeter),
                        query: { type: "public" },
                        includeLocs: "dist.location",
                        query: {},
                        spherical: true
                    }
                },
                { '$match': { $and: [ 
                        industryId ? { 'jobDetails.industry': industryId }:{},
                        roleIds?{'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{},
                        jobTypeValues? { 'jobDetails.jobType': {$in: jobTypeValues}} : {},
                        payTypeValues? { 'jobDetails.payType': {$in: payTypeValues}}:{},
                        skillValuesDetails?{ 'jobDetails.roleWiseAction.skills':{$in: skillValuesDetails } }:{}
                    ]}
                },
                 { $unwind : "$jobDetails.roleWiseAction" },
                 { '$match':roleIds ? { 'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{}},
                {
                    $project : {
                    '_id' : 0,
                    'locationName':1,
                    'location':1,
                    'employerId':1,
                    'jobDetails':1,
                    'description': 1,
                    'distance': 1,
                    'jobId': 1,
                    'appiliedFrom':1,
                    'approvedTo':1,
                    'dist':1
                    }
                },
                {
                    "$project":{
                        "locationName":"$locationName",
                        "location":"$location",
                        "employerId":"$employerId",
                        "jobDetails":"$jobDetails",
                        "description": "$description",
                        "distance": "$distance",
                        "jobId": "$jobId",
                        "dist":"$dist",
                        "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        }
                },
                {
                    "$project":{
                        'locationName':1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                    }
                },
               
                {
                    "$project":{
                        'locationName':1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":candidateId,
                        "isOffered":{
                            $cond: {
                                "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                "then": true,
                                "else": false
                            }
                        }
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                    }
                },
                {
                    "$project":{
                        "locationName":1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "candidateDetails":{ $arrayElemAt :["$candidateDetails", 0]}
                       
                    }
                },
                {
                    "$project":{
                        "locationName":"$locationName",
                        "location":"$location",
                        "employerId":"$employerId",
                        "jobDetails":"$jobDetails",
                        "description": "$description",
                        "distance": "$distance",
                        "jobId": "$jobId",
                        "dist":"$dist",
                        "candidateId":"$candidateId",
                        "isOffered":"$isOffered",
                        "declinedJobs":"$candidateDetails.declinedJobs",
                        "acceptedJobs":"$candidateDetails.acceptedJobs",
                        "appliedJobs":"$candidateDetails.appliedJobs",
                        "hiredJobs":"$candidateDetails.hiredJobs",
                    }
                },
                {
                    "$project":{
                        "locationName":1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: { $eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: { $eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "appliedJobsArray": { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: { $eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                    }
                },
                {
                    "$project":{
                        "locationName":1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "declinedJobsArray": { $filter: { input: "$declinedJobsArray", as: "declinedJobsArray", cond: { $eq: ["$$declinedJobsArray.jobId", "$jobId"] } } },
                        "acceptedJobsArray": { $filter: { input: "$acceptedJobsArray", as: "acceptedJobsArray", cond: { $eq: ["$$acceptedJobsArray.jobId", "$jobId"] } } },
                        "appliedJobsArray": { $filter: { input: "$appliedJobsArray", as: "appliedJobsArray", cond: { $eq: ["$$appliedJobsArray.jobId", "$jobId"] } } },
                        "hiredJobsArray": { $filter: { input: "$hiredJobsArray", as: "hiredJobsArray", cond: { $eq: ["$$hiredJobsArray.jobId", "$jobId"] } } },
                    }
                },
                {
                    "$project":{
                        "locationName":1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "isOffered":1,
                        "declinedJobsArray":{ $arrayElemAt :["$declinedJobsArray", 0]},
                        "acceptedJobsArray":{ $arrayElemAt :["$acceptedJobsArray", 0]},
                        "appliedJobsArray":{ $arrayElemAt :["$appliedJobsArray", 0]},
                        "hiredJobsArray":{ $arrayElemAt :["$hiredJobsArray", 0]}
                    }
                },
                {
                    "$project":{
                        "locationName":1,
                        "location":1,
                        "employerId":1,
                        "jobDetails":1,
                        "description": 1,
                        "distance":1,
                        "jobId": 1,
                        "dist":1,
                        "candidateId":1,
                        "status":{
                            "isApplied":{
                                $cond: {
                                    "if": { "$eq": [ "$appliedJobsArray.candidateId", candidateId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                           "isHired":{
                            $cond: {
                                "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                "then": true,
                                 "else": false
                                }
                            },
                            "isOffered":"$isOffered",
                            "isDeclined":{
                                $cond: {
                                    "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            "isAccepted":{
                                $cond: {
                                    "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                    "then": true,
                                    "else": false
                                }
                            },
                        },
                    }
                },
                { 
                    $match: {
                        $or:[{
                            "status.isApplied": false,
                            "status.isHired": false,
                            "status.isOffered": false,
                            "status.isDeclined": false,
                            "status.isAccepted": false
                        }]
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", nearbyperPage ] } },
                        'results': {
                            $slice: [
                                '$results', nearbypageno * nearbyperPage , nearbyperPage
                            ]
                        }
                    }
                }
            ]
                models.job.aggregate(aggrQuery).exec((err, result) => {
                    console.log(err)
                    if(err){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    else{
                        if(result.length!==0){
                            let aggrQuery1 = [
                                { '$match': { $and: [ 
                                        industryId?{ 'jobDetails.industry': industryId }:{},
                                        roleIds?{'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{},
                                        jobTypeValues? { 'jobDetails.jobType': {$in: jobTypeValues}} : {},
                                        payTypeValues? { 'jobDetails.payType': {$in: payTypeValues}}:{},
                                        skillValuesDetails?{ 'jobDetails.roleWiseAction.skills':{$in: skillValuesDetails } }:{}
                                    ]}
                                },
                                { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                               
                                { '$match':roleIds ? { 'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{}},
                                { $match :{ 'location.coordinates':{$nin:result[0].results[0]['location']['coordinates']}}} ,
                                {
                                    $project : {
                                        '_id' : 0,
                                        'locationName':1,
                                        'location':1,
                                        'employerId':1,
                                        'jobDetails':1,
                                        'description': 1,
                                        'distance': 1,
                                        'jobId': 1,
                                        'appiliedFrom':1,
                                        'approvedTo':1,
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":"$locationName",
                                        "location":"$location",
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                       
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":candidateId,
                                        "isOffered":{
                                            $cond: {
                                                "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "candidateDetails":{ $arrayElemAt :["$candidateDetails", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":"$locationName",
                                        "location":"$location",
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "candidateId":"$candidateId",
                                        "isOffered":"$isOffered",
                                        "declinedJobs":"$candidateDetails.declinedJobs",
                                        "acceptedJobs":"$candidateDetails.acceptedJobs",
                                        "appliedJobs":"$candidateDetails.appliedJobs",
                                        "hiredJobs":"$candidateDetails.hiredJobs",
                                        
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: { $eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: { $eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: { $eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobsArray", as: "declinedJobsArray", cond: { $eq: ["$$declinedJobsArray.jobId", "$jobId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobsArray", as: "acceptedJobsArray", cond: { $eq: ["$$acceptedJobsArray.jobId", "$jobId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobsArray", as: "appliedJobsArray", cond: { $eq: ["$$appliedJobsArray.jobId", "$jobId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobsArray", as: "hiredJobsArray", cond: { $eq: ["$$hiredJobsArray.jobId", "$jobId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray":{ $arrayElemAt :["$declinedJobsArray", 0]},
                                        "acceptedJobsArray":{ $arrayElemAt :["$acceptedJobsArray", 0]},
                                        "appliedJobsArray":{ $arrayElemAt :["$appliedJobsArray", 0]},
                                        "hiredJobsArray":{ $arrayElemAt :["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "status":{
                                            "isApplied":{
                                                $cond: {
                                                    "if": { "$eq": [ "$appliedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                           "isHired":{
                                            $cond: {
                                                "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                                "then": true,
                                                 "else": false
                                                }
                                            },
                                            "isOffered":"$isOffered",
                                            "isDeclined":{
                                                $cond: {
                                                    "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                            "isAccepted":{
                                                $cond: {
                                                    "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                { 
                                    $match: {
                                        $or:[{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", outsideperPage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage , outsideperPage
                                            ]
                                        }
                                    }
                                }
                                
                           ]
                            models.job.aggregate(aggrQuery1).exec((err, data) => {
                                console.log('............',data)
                                if(err){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: data[0],
                                        nearByJobs: result[0]
                                    })
                                }
                            })
                        }
                        else{
                            
                            let aggrQuery2 = [
                                { '$match': { $and: [ 
                                        industryId?{ 'jobDetails.industry': industryId }:{},
                                        roleIds?{'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{},
                                        jobTypeValues? { 'jobDetails.jobType': {$in: jobTypeValues}} : {},
                                        payTypeValues? { 'jobDetails.payType': {$in: payTypeValues}}:{},
                                        skillValuesDetails?{ 'jobDetails.roleWiseAction.skills':{$in: skillValuesDetails } }:{}
                                    ]}
                                },
                                { $unwind : {path:"$jobDetails.roleWiseAction",preserveNullAndEmptyArrays:true} },
                                { '$match':roleIds ? { 'jobDetails.roleWiseAction.roleId':{$in: roleIds }}:{}},
                                {
                                    $project : {
                                        '_id' : 0,
                                        'locationName':1,
                                        'location':1,
                                        'employerId':1,
                                        'jobDetails':1,
                                        'description': 1,
                                        'distance': 1,
                                        'jobId': 1,
                                        'appiliedFrom':1,
                                        'approvedTo':1,
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":"$locationName",
                                        "location":"$location",
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "isOfferedArray": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                       
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "isOfferedArray":{ $arrayElemAt :["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":candidateId,
                                        "isOffered":{
                                            $cond: {
                                                "if": { "$eq": [ "$isOfferedArray.candidateId", candidateId] }, 
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "candidateDetails":{ $arrayElemAt :["$candidateDetails", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":"$locationName",
                                        "location":"$location",
                                        "employerId":"$employerId",
                                        "jobDetails":"$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "candidateId":"$candidateId",
                                        "isOffered":"$isOffered",
                                        "declinedJobs":"$candidateDetails.declinedJobs",
                                        "acceptedJobs":"$candidateDetails.acceptedJobs",
                                        "appliedJobs":"$candidateDetails.appliedJobs",
                                        "hiredJobs":"$candidateDetails.hiredJobs",
                                        
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobs", as: "declinedJobs", cond: { $eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobs", as: "acceptedJobs", cond: { $eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobs", as: "hiredJobs", cond: { $eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray": { $filter: { input: "$declinedJobsArray", as: "declinedJobsArray", cond: { $eq: ["$$declinedJobsArray.jobId", "$jobId"] } } },
                                        "acceptedJobsArray": { $filter: { input: "$acceptedJobsArray", as: "acceptedJobsArray", cond: { $eq: ["$$acceptedJobsArray.jobId", "$jobId"] } } },
                                        "appliedJobsArray": { $filter: { input: "$appliedJobsArray", as: "appliedJobsArray", cond: { $eq: ["$$appliedJobsArray.jobId", "$jobId"] } } },
                                        "hiredJobsArray": { $filter: { input: "$hiredJobsArray", as: "hiredJobsArray", cond: { $eq: ["$$hiredJobsArray.jobId", "$jobId"] } } },
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "isOffered":1,
                                        "declinedJobsArray":{ $arrayElemAt :["$declinedJobsArray", 0]},
                                        "acceptedJobsArray":{ $arrayElemAt :["$acceptedJobsArray", 0]},
                                        "appliedJobsArray":{ $arrayElemAt :["$appliedJobsArray", 0]},
                                        "hiredJobsArray":{ $arrayElemAt :["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project":{
                                        "locationName":1,
                                        "location":1,
                                        "employerId":1,
                                        "jobDetails":1,
                                        "description": 1,
                                        "distance":1,
                                        "jobId": 1,
                                        "candidateId":1,
                                        "status":{
                                            "isOffered":"$isOffered",
                                            "isApplied":{
                                                $cond: {
                                                    "if": { "$eq": [ "$appliedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                           "isHired":{
                                            $cond: {
                                                "if": { "$eq": [ "$hiredJobsArray.candidateId", candidateId] }, 
                                                "then": true,
                                                 "else": false
                                                }
                                            },
                                            "isDeclined":{
                                                $cond: {
                                                    "if": { "$eq": [ "$declinedJobsArray.candidateId", candidateId] }, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                            "isAccepted":{
                                                $cond: {
                                                    "if": {$or:[{ "$eq": ["$acceptedJobsArray.candidateId", candidateId] },{ "$eq": [ "$hiredJobsArray.candidateId", candidateId] }]}, 
                                                    "then": true,
                                                     "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                { 
                                    $match: {
                                        $or:[{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", outsideperPage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage , outsideperPage
                                            ]
                                        }
                                    }
                                }
                            ]
                            models.job.aggregate(aggrQuery2).exec((error, outsidedata) => {
                                if(error){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else{
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: outsidedata[0],
                                        nearByJobs: result[0]
                                    })
                                }
                                
                            })
                        }
                    }
                })
            })            
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    }
}

function sendNotificationEmail(emailId,candidateId,jobId,roleId,employerId){
    let globalVarObj={};

    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }

            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
           
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  has applied for position "+globalVarObj.jobRoleName+" and Job ID ="+jobId.substring(3);
                // globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  has applied for position "+globalVarObj.jobRoleName+" and Job ID ="+jobId;
                
                //-------------
                let insertedValue={
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }
                

                models.employer.updateOne({ 'employerId': employerId }, 
                    { $push: { "notification":insertedValue } }, 
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {

                //---------- 

                        simplemailercontroller.viaGmail({ receiver: emailId, subject: '----JOB APPLICATION----',msg:globalVarObj.msgBody}, (mailerErr, nodeMailerResponse) => {
                            if(mailerErr){
                                resolve('false');  
                            }
                            else{
                                resolve('true');
                   
                            }
                        })

                }) // END models.candidate.updateOne        
            })     
        })
    })
}


function sendCheckinNotificationEmail(emailId,candidateId,jobId,roleId,checkinTime,date,destinationName,jobtype,inout){
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
           
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                // globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  have checked " + inout +" successfully. Date : "+date+" Candidate Location : "+ destinationName +" Checkin Time : "+checkinTime+" Job Type : "+jobtype+" Role Name : "+globalVarObj.jobRoleName+" and Job ID : "+jobId;
                 globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  have checked " + inout +" successfully. Date : "+date+" Candidate Location : "+ destinationName +" Checkin Time : "+checkinTime+" Job Type : "+jobtype+" Role Name : "+globalVarObj.jobRoleName+" and Job ID : "+jobId.substring(3);
                simplemailercontroller.viaGmail({ receiver: emailId, subject: '----Checked In Logs----',msg:globalVarObj.msgBody}, (mailerErr, nodeMailerResponse) => {
                    if(mailerErr){
                        resolve('false');  
                    }
                    else{
                        resolve('true');
                    }
                })
            })     
        })
    })
}
function sendCheckinNotificationSms(mobileNo,candidateId,jobId,roleId,checkinTime,date,destinationName,jobtype,inout){
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                globalVarObj.notification=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  have checked" + inout +" successfully. Date : "+date+" Candidate Location : "+ destinationName +" Checkin Time : "+checkinTime+" Job Type : "+jobtype+" Role Name : "+globalVarObj.jobRoleName+" and Job ID : "+jobId.substring(3);
                sendsmscontroller.Sendsms(mobileNo, globalVarObj.notification,(twilioError, twilioResult) => {
                    console.log(twilioResult)
                    if(twilioError){
                        resolve('false'); 
                    }
                    else{
                        resolve('true');
                    }
                }) 
            })
        })
    })
}

function sendNotificationSms(mobileNo,candidateId,jobId,roleId,employerId){
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                globalVarObj.notification=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  has applied for position "+globalVarObj.jobRoleName+" and Job ID ="+jobId.substring(3);
                
                //-------------
                let insertedValue={
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.notification
                }
                console.log(globalVarObj.notification);


                models.employer.updateOne({ 'employerId': employerId }, 
                    { $push: { "notification":insertedValue } }, 
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {

                //----------        
                            sendsmscontroller.Sendsms(mobileNo, globalVarObj.notification,(twilioError, twilioResult) => {
                                //console.log(twilioResult)
                                if(twilioError){
                                    resolve('false'); 
                                }
                                else{  
                                    resolve('true');               
                                }
                            }) 
                }) // END models.candidate.updateOne           
            })
        })
    })
}


function getAllDates(startDate,endDate){
    return new Promise(resolve => {
        resolve(startDate); 
    })
}
function isInteger(x) { return typeof x === "number" && isFinite(x) && Math.floor(x) === x; }
function isFloat(x) { return !!(x % 1); }
