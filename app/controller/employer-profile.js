const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const config =  require('../config/env');
var Request = require("request");
const fs = require('fs');
const formidable = require('formidable');
const image2base64 = require('image-to-base64');
const multer = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const env = require('../env');


module.exports = {

    // this function is used to fetch all industry for a particular employer
    fetchIndustryForEmployer: async (req, res, next) => {
        try{
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id",
                isError: true,
            });

            if (!req.body.employerId) { return; }


            let aggrQry = [
                {
                    $match: { 
                        'employerId': employerId 
                    },	
                },

                {
                    $project: {
                        _id: 0,
                        industryId: 1
                    }
                },

                { $unwind: "$industryId" },

                {
                    $lookup: {
                        from: "industries",
                        localField: "industryId",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },

                {
                    $project: {
                        industryDetails: 1,
                        industryDetailsFirst: { $arrayElemAt: [ "$industryDetails", 0 ] }
                    }
                },

                {
                    $group:
                    {
                        _id: null,
                        industries: { $addToSet: "$industryDetailsFirst" }
                    }
                },


            ];    
    

            models.employer.aggregate(aggrQry).exec((err, response) => {            
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    // let responseLength = response[0].industries ? response[0].industries.length : 0;
                    let responseLength = response[0] ? response[0].industries.length : 0;

                    if (responseLength < 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        });
                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: {
                                response: response[0]
                            }
                        });
                    }
                   
                }	
            })



        } 
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },

    addSignupIndustry: (req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id",
                isError: true,
            });
            let individualContactName = req.body.individualContactName ? req.body.individualContactName :'';
            let industryId = req.body.industryId ? req.body.industryId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id",
                isError: true,
            });
           
           if (!req.body.employerId || !req.body.industryId) { return; }
            let inertedItems={
                'individualContactName':individualContactName,
                'industryId':industryId

            };
            models.employer.updateOne({'employerId':empId},inertedItems, function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
    * @abstract 
    * Profile picture upload as url
    * First check  employer exist or not using empId
    * If exist insert t url  according to empId
   */
    profilePicUrlUpload: (req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id",
                isError: true,
            });
            let profilePic = req.body.profilePic ? req.body.profilePic : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Profile Picture",
                isError: true,
            });
            if (!req.body.employerId || !req.body.profilePic) { return; }
            let querywhere = { 'employerId': empId };
            console.log(empId)
            console.log(profilePic)
            models.employer.findOne(querywhere, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    });
                }
                else{
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1008'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else{
                        let updatewith = { 'profilePic': profilePic };
                        let findwhere = { 'employerId': empId }
                        models.employer.updateOne(findwhere, updatewith, function (error, response) {
                            if (error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                });
                            }
                            else {
                                if (response.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }
                        })
                    } 
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
    * @abstract 
    * Add+update Company
    * First check  employer exist or not using empId
    * If exist add/update
   */
    addCompany: (req, res, next) => {
        let empId = req.body.employerId ? req.body.employerId : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
        });
        let companyName = req.body.companyName ? req.body.companyName : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Company Name "
        });
        let companyDescription = req.body.companyDescription ? req.body.companyDescription : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Country Description"
        });
        let brandName = req.body.brandName ? req.body.brandName : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Brand Name"
        });
        let companyNo = req.body.companyNo ? req.body.companyNo : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Company Number"
        });
        let vatNo = req.body.vatNo ? req.body.vatNo : "";

        let capacity = req.body.capacity ? req.body.capacity : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Company Capacity"
        });
        if (!req.body.employerId || !req.body.companyName || !req.body.companyDescription || !req.body.brandName || !req.body.companyNo || !req.body.capacity) { return; }
        try {
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let updateWith = {
                        'companyName': companyName,
                        'companyDescription': companyDescription,
                        'brandName': brandName,
                        'companyNo': companyNo,
                        'vatNo': vatNo,
                        'hiringCapacity': capacity
                    }
                    models.employer.updateOne({ 'employerId': empId }, updateWith, function (error, response) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (response.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Add+update address
     * First check  employer exist or not using empId
     * If exist add/update
    */
    addAddress: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let addressOne = req.body.addressOne ? req.body.addressOne : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - address One"
            });
            let addressTwo = req.body.addressTwo ? req.body.addressTwo : '';
            let city = req.body.city ? req.body.city : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - City"
            });
            let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
                isError: true,
                password: errorMsgJSON['ResponseMsg']['303'] + " - country Code"
            });
            let postCode = req.body.postCode ? req.body.postCode : res.json({
                isError: true,
                password: errorMsgJSON['ResponseMsg']['303'] + " - Post Code"
            });
            if (!req.body.addressOne || !req.body.city || !req.body.countryCode || !req.body.postCode) { return; }
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let updateWith = {
                        'address.addressLineOne': addressOne,
                        'address.addressLineTwo': addressTwo,
                        'address.countryCode': countryCode,
                        'address.city': city,
                        'address.postCode': postCode,
                    }
                    models.employer.updateOne({ 'employerId': empId }, updateWith, function (error, response) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (response.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Add+update Contact
     * First check  employer exist or not using empId
     * If exist add/update
    */
    addContact: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let contactName = req.body.contactName ? req.body.contactName : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Contact Name"
            });

            let contactTelephoneNo = req.body.contactTelephoneNo ? req.body.contactTelephoneNo : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Contact Telephone No"
            });
            let EmailId = req.body.EmailId ? req.body.EmailId : res.json({
                password: errorMsgJSON['ResponseMsg']['303'] + " - Email Id"
            });
            let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
                password: errorMsgJSON['ResponseMsg']['303'] + " - Mobile No"
            });
            if (!req.body.employerId || !req.body.contactName || !req.body.contactTelephoneNo || !req.body.EmailId || !req.body.mobileNo) { return; }
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let updateWith = {
                        'contactName': contactName,
                        'contactTelephoneNo': contactTelephoneNo,
                        'EmailId': EmailId,
                        'mobileNo': mobileNo,
                    }
                    models.authenticate.findOne({ "EmailId": EmailId, "userId": { $ne: empId } }, function (err, item) {
                        if (item == null) {
                            models.employer.updateOne({ 'employerId': empId }, updateWith, function (error, response) {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (response.nModified == 1) {
                                        let updateWith = {'EmailId': EmailId}
                                        models.authenticate.updateOne({"userId": empId},updateWith,function (err, item) {
                                            if (err) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                            else{
                                                if (item.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    })
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                        else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1011'],
                                statuscode: 1011,
                                details: null
                            });

                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
    * @abstract 
    * get Employer address
    * First check  employer exist or not using empId
    * If exist send details
   */
    getDetails: async (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId) { return; }

            //====================================================
            // this function is written to fetch the industryname
            const getIndustryDetails = (industryId) => {
                
                return new Promise(function (resolve, reject) {
                    
                    models.industry.find(
                        {
                            'industryId': { $in: industryId }
                        }, 
                        function (err, industryDetails) {
            
                            if (err) {
                                resolve({
                                   "isError": true,
                                });
                            } else {
            
                                if (industryDetails) {
                                    resolve({
                                       "isError": false,
                                       "isItemExist": true,
                                       "industryDetails" : industryDetails
                                    });
                                } else {
                                    resolve({
                                       "isError": false,
                                       "isItemExist": false
                                    });
                                }
            
                            }
                   
                    })
                }) // END Promise	
            }
            
            //====================================================

  
            models.employer.findOne({ 'employerId': empId }, async function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {

                    let cityId = item.address ? item.address.city : "NA";

                    // Here, We are fetching industry details
                    let industryId = item.industryId ? item.industryId : "NA";  

                    let industryDetails;
                    if (industryId !== "NA") {
                        let getIndustryDetailsStatus = await getIndustryDetails(industryId);

                        industryDetails = getIndustryDetailsStatus.industryDetails ? getIndustryDetailsStatus.industryDetails : "";           
                    }


                    if (cityId == "NA") {

                        let address = {
                                "addressLineOne": "",
                                "addressLineTwo": "",
                                "city": "",
                                "countryCode": "",
                                "postCode": ""
                        };
                       
                        item.address = address;
                        item.contactName = "";
                        item.contactTelephoneNo = "";
                        item.mobileNo = "";

                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: item,
                            industryDetails: industryDetails
                        })

                        return;
                    }
                    
                    models.Citie.findOne({ 'cityId': cityId }, 
                        function (err, cityDetails) {
                            if (err) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                }) 

                                return;
                            } else {
                                let country_id = parseInt(cityDetails.country_id);

                                models.Countrie.findOne({ 'countryId': country_id }, 
                                    function (err, countryDetails) {
                                        if (err) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            }) 
            
                                            return;
                                        } else {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: item,
                                                cityDetails: cityDetails,
                                                countryDetails: countryDetails,
                                                industryDetails: industryDetails
                                                
                                            })
        
                                            return; 

                                        }
                                    
                                })
                            }              
                    })        
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Change Password in Profile Section
     * Check if Any Employer exists by checking oldPassword and email address
     * if exists update old password with new one 
    */
    profileChangePassword: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let oldPassword = req.body.oldPassword ? req.body.oldPassword : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " -Old  Password " });
            let newPassword = req.body.newPassword ? req.body.newPassword : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " -New Password " });
            if (!req.body.employerId || !req.body.oldPassword || !req.body.newPassword) { return; }
            let querywhere = { 'employerId': empId, 'password': oldPassword };
            models.employer.findOne(querywhere, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let updatewith = { 'password': newPassword };
                    let findwhere = { 'employerId': empId }
                    models.employer.updateOne(findwhere, updatewith, function (error, response) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (response.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Profile Picture Upload
     * First check  employer exist or not using empId
     * if exists upload pic in s3-bucket using multer
     * then store image link in employer collection
    */
    profilePicUpload: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let profilePic = req.body.profilePic ? req.body.profilePic : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Profile Picture"
            });
            if (!req.body.employerId || !req.body.profilePic) { return; }
            let querywhere = { 'employerId': empId };
            console.log(empId)
            console.log(profilePic)
            models.employer.findOne(querywhere, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {

                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1008'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else {

                        let ProfileImgParams = {
                            imgName: empId,
                            picture: profilePic,
                            fileextension: 'png',
                            fldrName: 'EmployerPic'
                        }
                        base64controller.Base64ImgUpload(ProfileImgParams, (err, data) => {
                            if (err) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else {
                                let profileImageUrl = data.toString();
                                let updatewith = { 'profilePic': profileImageUrl };
                                let findwhere = { 'employerId': empId }
                                models.employer.updateOne(findwhere, updatewith, function (error, response) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        });
                                    }
                                    else {
                                        if (response.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Profile Picture Upload
     * implemantation of '/api/employer-profile/verify-emp' that is called from employer-authentication page
     * fetch the tocken coming with route path and check it in employer table and
     * decide the next path accordingly
     */
    
    saveEmailVerificationTocken: async (req, res) =>{

        var item = models.employer.findOne({'Status.confirmationTocken':req.query.id}, function (error, item) {
            if(error){
                console.log('error')
                console.log('here ut the code of redirecting alternative path')
                //res.redirect(`${config.app.FORMATTED_URL}/500`);
            }
            else{
                if (item == null) {
                    console.log('candidate not exists');
                    console.log('here ut the code of redirecting alternative path');
                    //res.redirect(`${config.app.FORMATTED_URL}/invalid-link`);
                } else {
                   
                    if(item.Status.confirmationTocken===req.query.id){
                        console.log('same tocken')
                        
                        models.employer.updateOne({'Status.confirmationTocken':req.query.id}, {'Status.isVerified':true}, function (error, response) {
                            if(error){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                                //res.redirect(`${config.app.FORMATTED_URL}/500`);
                            }
                            else {
                                if (response.nModified == 1) {
                                    // res.json({
                                    //     isError: false,
                                    //     message: "Successfull",
                                    //     statuscode: 200
                                    // });
                                    res.redirect(`${config.app.frontendUri}`);
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    }) 
                                   // res.redirect(`${config.app.FORMATTED_URL}/500`);
                                }
                            }
                        })
                    } else {
                        console.log('not same tocken');
                        console.log('here ut the code of redirecting alternative path');
                        // res.redirect(`${config.app.FORMATTED_URL}/invalid-link`);
                        // res.redirect(`https://api.letery.com/invalid-link`);
                        res.redirect(`${config.app.FORMATTED_URL_NEW}/invalid-link`);
                    }
                }
            }

        })
    },
    profilePicAsFileUpload : (req, res, next) =>{
        try {
            
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
               console.log(fields, files)
              
            
            });
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    }
}