const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');

const offsiteTracking = require('../model/offsiteTracking');

const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const simplemailercontroller = require('../service/mailer');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const fcm = require('../service/fcm');
const sendsmscontroller = require('../service/TwilioSms');
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
var Request = require("request");
const config = require('../config/env');

//=========================================================
const employerJobBusiness = require("../businessLogic/employer-job-business");
const jobProfileBusiness = require("../businessLogic/job-profile-business");
const candidateProfileBusiness = require("../businessLogic/candidate-profile-business");
const mailerBusiness = require("../businessLogic/mailer-business");
//=========================================================

require('twix');

const EnvInfo = require('../model/envInfo');


module.exports = {

    /*
        This Function is written to cancel a particular job
        for candidate
    */
    cancelJobForCandidate: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });


            let presentDate = req.body.today ? req.body.today : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present date"
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - local Time Zone"
            });

            if (!req.body.candidateId || !req.body.jobId || !req.body.roleId || !req.body.today || !req.body.presentTime) {
                return;
            }

            // let dateObj = jobProfileBusiness.getTimeAndDateWithTimeZone(presentDate, presentTime, localTimeZone);
            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);
            let presentTimeStamp = dateObj.realDateTimeStamp;

            // adding 24 hrs to timestamp
            let timeStampAfterDay = presentTimeStamp + (24 * 60 * 60 * 1000);

            let adminEmail = config.app.adminEmail;
            // Here, we are going to fetch the details of job
            let aggrQry = [
                {
                    $match: {
                        'jobId': jobId
                    },
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleWiseAction': '$jobDetails.roleWiseAction'
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleDetails': {
                            $filter: {
                                input: "$roleWiseAction",
                                as: "roleWiseAction",
                                cond: {$eq: ["$$roleWiseAction.roleId", roleId]}
                            }
                        }
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'startRealDate': '$roleDetails.startRealDate',
                        'startRealDateUtc': '$roleDetails.startRealDateUtc',
                        'startRealDateTimeStamp': '$roleDetails.startRealDateTimeStamp',
                        'roleName': '$roleDetails.roleName',
                        'jobType': '$roleDetails.jobType',
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'startRealDate': {$arrayElemAt: ['$startRealDate', 0]},
                        'startRealDateUtc': {$arrayElemAt: ['$startRealDateUtc', 0]},
                        'startRealDateTimeStamp': {$arrayElemAt: ['$startRealDateTimeStamp', 0]},
                        'roleName': {$arrayElemAt: ['$roleName', 0]},
                        'jobType': {$arrayElemAt: ['$jobType', 0]},
                    }
                }
            ];

            let fetchJobStatus = await jobProfileBusiness.fetchJob(aggrQry);

            if (fetchJobStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let jobStartRealDateTimeStamp = Number(fetchJobStatus.response[0].startRealDateTimeStamp);

            if (jobStartRealDateTimeStamp < timeStampAfterDay) {
                res.json({
                    isError: true,
                    message: "Job Can not be cancelled as it will be started within 24 hours",
                    statuscode: 204,
                    details: null
                });

                return;
            }

            let employerId = fetchJobStatus.response[0].employerId ? fetchJobStatus.response[0].employerId : '';
            let roleName = fetchJobStatus.response[0].roleName ? fetchJobStatus.response[0].roleName : '';

            let arggrQryForEmp = [
                {
                    $match: {
                        'employerId': employerId
                    },
                },
                {
                    $project: {
                        'employerId': 1,
                        'EmailId': 1
                    }
                }
            ];

            let fetchEmployerDetailsForJobCancellationStatus = await employerJobBusiness.fetchEmployerDetailsForJobCancellation(arggrQryForEmp);

            if (fetchEmployerDetailsForJobCancellationStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let empEmailId = fetchEmployerDetailsForJobCancellationStatus.response[0].EmailId ? fetchEmployerDetailsForJobCancellationStatus.response[0].EmailId : '';

            let arggrQryForCandidate = [
                {
                    $match: {
                        'candidateId': candidateId
                    },
                },
                {
                    $project: {
                        'candidateId': 1,
                        "fname": 1,
                        "mname": 1,
                        "lname": 1,
                        "EmailId": 1,
                    }
                }
            ];

            let fetchCandidateDetailsStatus = await candidateProfileBusiness.fetchCandidateDetails(arggrQryForCandidate);

            if (fetchCandidateDetailsStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let fname = fetchCandidateDetailsStatus.response[0].fname ? fetchCandidateDetailsStatus.response[0].fname : '';
            let mname = fetchCandidateDetailsStatus.response[0].mname ? fetchCandidateDetailsStatus.response[0].mname : '';
            let lname = fetchCandidateDetailsStatus.response[0].lname ? fetchCandidateDetailsStatus.response[0].lname : '';
            let candidateEmailId = fetchCandidateDetailsStatus.response[0].EmailId ? fetchCandidateDetailsStatus.response[0].EmailId : '';

            let updateHiredJobListOfCandidateStatus = await candidateProfileBusiness.updateHiredJobListOfCandidate({
                "candidateId": candidateId,
                "roleId": roleId,
                "jobId": jobId,
                "cancelledBy": "Candidate"
            });

            if (updateHiredJobListOfCandidateStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let cancelJobForParticularCandidateInHirelistStatus = await jobProfileBusiness.cancelJobForParticularCandidateInHirelist({
                "candidateId": candidateId,
                "roleId": roleId,
                "jobId": jobId,
                "cancelledBy": "Candidate"
            });

            if (cancelJobForParticularCandidateInHirelistStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let notificationMsg = `The candidate ${fname} ${lname}, having emailId ${candidateEmailId}, has cancelled the job having jobId ${jobId} for the position ${roleName}`;

            employerJobBusiness.updateNotificationForEmployer({
                "notificationMsg": notificationMsg,
                "employerId": employerId
            });

            let emailMessage = `The candidate ${fname} ${lname}, having emailId ${candidateEmailId}, has cancelled the job having jobId ${jobId} for the position ${roleName}`;
            let emailSubject = 'Regarding the job cancellation';

            mailerBusiness.sendEmailToEmployerForJobCancellation({
                'emailMessage': emailMessage,
                'emailSubject': emailSubject,
                'empEmailId': empEmailId
            });

            // res.json({
            //     updateHiredJobListOfCandidateStatus: updateHiredJobListOfCandidateStatus,
            //     cancelJobForParticularCandidateInHirelistStatus: cancelJobForParticularCandidateInHirelistStatus,
            //     requ: req.body
            // });
            // return;

            // Here, we are sending a mail to admin
            mailerBusiness.sendEmailToEmployerForJobCancellation({
                'emailMessage': emailMessage,
                'emailSubject': emailSubject,
                'empEmailId': adminEmail
            });


            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: null
            });
            return;

        } catch (error) {

            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    /*
        This Function is written to fetch the details of the candidate
    */
    fetchAuthenticityOfCandidate: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            if (!req.body.candidateId) {
                return;
            }

            let aggrQry = [
                {
                    $match: {
                        'candidateId': candidateId
                    },
                }
            ];

            let fetchCandidateInfoStatus = await employerJobBusiness.fetchCandidateInfo(aggrQry);

            if (fetchCandidateInfoStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            if (fetchCandidateInfoStatus.responseLength < 1) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let isAuthenticated = "True";
            let message = "Porperly filled";

            let home_address = fetchCandidateInfoStatus.response[0].home_address ? fetchCandidateInfoStatus.response[0].home_address : '';
            let socialLoginDetails = fetchCandidateInfoStatus.response[0].socialLoginDetails ? fetchCandidateInfoStatus.response[0].socialLoginDetails : '';
            let dob = fetchCandidateInfoStatus.response[0].dob ? fetchCandidateInfoStatus.response[0].dob : '';
            let age = fetchCandidateInfoStatus.response[0].dob ? fetchCandidateInfoStatus.response[0].age : '';
            let geolocation = fetchCandidateInfoStatus.response[0].geolocation ? fetchCandidateInfoStatus.response[0].geolocation : '';
            let profilePic = fetchCandidateInfoStatus.response[0].profilePic ? fetchCandidateInfoStatus.response[0].profilePic : '';
            let mobileNo = fetchCandidateInfoStatus.response[0].mobileNo ? fetchCandidateInfoStatus.response[0].mobileNo : '';
            let contactNo = fetchCandidateInfoStatus.response[0].contactNo ? fetchCandidateInfoStatus.response[0].contactNo : '';
            let language = fetchCandidateInfoStatus.response[0].language ? fetchCandidateInfoStatus.response[0].language : [];
            let qualification = fetchCandidateInfoStatus.response[0].qualification ? fetchCandidateInfoStatus.response[0].qualification : '';
            let payloadSkill = fetchCandidateInfoStatus.response[0].payloadSkill ? fetchCandidateInfoStatus.response[0].payloadSkill : '';

            let skills = fetchCandidateInfoStatus.response[0].skills ? fetchCandidateInfoStatus.response[0].skills : [];
            let selectedRole = fetchCandidateInfoStatus.response[0].selectedRole ? fetchCandidateInfoStatus.response[0].selectedRole : [];
            let kinDetails = fetchCandidateInfoStatus.response[0].kinDetails ? fetchCandidateInfoStatus.response[0].kinDetails : [];


            if (kinDetails == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter the details of kins";
                res.json({
                    isError: true,
                    message: "Profile was not filled properly",
                    statuscode: 404,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            if (selectedRole.length < 1) {
                isAuthenticated = "False";
                message = "Candidate has to enter role details";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }


            if (skills.length < 1) {
                isAuthenticated = "False";
                message = "Candidate has to enter skills";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }


            if (home_address == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter the home address";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            if (home_address == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter the home address";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            // if (socialLoginDetails == '') {
            //     isAuthenticated = "False";
            //     message = "Candidate has to enter the social login address";
            //     res.json({
            //         isError: false,
            //         message: "Profile was not filled properly",
            //         statuscode: 204,
            //         details: {
            //             isAuthenticated: isAuthenticated,
            //             message: message
            //         }
            //     });

            //     return;
            // }

            if (dob == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter the date of birth";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            if (age == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter age";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            if (geolocation == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter geolocation";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            if (profilePic == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter Profile Picture";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            // if (mobileNo == '') {
            //     isAuthenticated = "False";
            //     message = "Candidate has to enter mobile number";
            //     res.json({
            //         isError: false,
            //         message: "Profile was not filled properly",
            //         statuscode: 204,
            //         details: {
            //             isAuthenticated: isAuthenticated,
            //             message: message
            //         }
            //     });

            //     return;
            // }

            if (contactNo == '') {
                isAuthenticated = "False";
                message = "Candidate has to enter contact number";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }

            if (language.length < 0) {
                isAuthenticated = "False";
                message = "Candidate has to enter details of language";
                res.json({
                    isError: false,
                    message: "Profile was not filled properly",
                    statuscode: 204,
                    details: {
                        isAuthenticated: isAuthenticated,
                        message: message
                    }
                });

                return;
            }


            // if (qualification == '') {
            //     isAuthenticated = "False";
            //     message = "Candidate has to enter qualification details";
            //     res.json({
            //         isError: false,
            //         message: "Profile was not filled properly",
            //         statuscode: 204,
            //         details: {
            //             isAuthenticated: isAuthenticated,
            //             message: message
            //         }
            //     });

            //     return;
            // }


            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: {
                    isAuthenticated: isAuthenticated,
                    message: message
                }
            });
            return;


        } catch (error) {

            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    /*
        This Function is written to fetch Offsite Tracking of a candidate
    */
    fetchOffSiteTraching: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -job Id"
            });

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -roleId"
            });

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -employer Id"
            });

            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -date"
            });

            if (!req.body.candidateId || !req.body.jobId || !req.body.roleId || !req.body.employerId || !req.body.date) {
                return;
            }

            let fetchOfficeTrackinDetailsStatus = await employerJobBusiness.fetchOfficeTrackinDetails(candidateId, jobId, roleId, employerId, date);

            if (fetchOfficeTrackinDetailsStatus.isError == true) {
                res.json({
                    isError: true,
                    message: "Some Error Occur While Fetching Offsite Tracking Details",
                    statuscode: 404,
                    details: null
                });

                return;
            }

            if (fetchOfficeTrackinDetailsStatus.isExist == false) {
                res.json({
                    isError: false,
                    message: "No Data Found",
                    statuscode: 204,
                    details: null
                });

                return;
            }

            let offsiteDetail = fetchOfficeTrackinDetailsStatus.response;

            res.json({
                isError: false,
                message: "Success",
                statuscode: 200,
                details: offsiteDetail
            });


        } catch (error) {

            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    /*
        This function is written to track location and time in case of offsite checking
        of the candidate in every 15 minutes
    */
    offsiteTracking: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -job Id"
            });

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -roleId"
            });

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -employer Id"
            });

            let offSideId = req.body.offSideId ? req.body.offSideId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -offSide Id"
            });

            let destinationName = req.body.destinationName ? req.body.destinationName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Destination Name"
            });

            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -destination Latitude"
            });

            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Destination Longitude"
            });

            let checkinTime = req.body.checkinTime ? req.body.checkinTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -check In Time"
            });

            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -date"
            });


            if (!req.body.candidateId || !req.body.jobId || !req.body.roleId || !req.body.employerId) {
                return;
            }
            if (!req.body.offSideId || !req.body.destinationName || !req.body.destinationLat || !req.body.destinationLong) {
                return;
            }
            if (!req.body.checkinTime || !req.body.date) {
                return;
            }


            //*******************code for automatic checkout ************** */

            let presentTime = checkinTime;
            presentTime = Number(presentTime);

            // this code iswritten to check out the candidate the candidate automatically if shift ended
            let fetchShiftDetailsForCandidateStatus = await employerJobBusiness.fetchShiftDetailsForCandidate(candidateId, jobId, roleId, date);

            let shiftEndTime = fetchShiftDetailsForCandidateStatus.response.workingDays[0].shiftTime[0].endTime;
            shiftEndTime = Number(shiftEndTime);


            if (shiftEndTime < presentTime) {// Candidate Need to checkout

                Request.post({
                    "headers": {"content-type": "application/json"},
                    "url": `${config.app.FORMATTED_URL_NEW}/api/candidate-job/offline-checkout`,
                    "body": JSON.stringify({
                        "candidateId": candidateId,
                        "jobId": jobId,
                        "roleId": roleId,
                        "employerId": employerId,
                        "checkOutTime": presentTime,
                        "date": date,
                        "checkoutlocationName": destinationName,
                        "destinationLat": destinationLat,
                        "destinationLong": destinationLong

                    })
                }, (error, response, body) => {
                    if (error) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    if (response) {
                        res.json({
                            isError: false,
                            message: "You have successfully checked out at " + presentTime + ". A notification will be sent to your employer.",
                            statuscode: 200,
                            details: {
                                "isCheckedIn": false,
                                "lastJobType": ""
                            }
                        })
                    }
                })
            } // END if (shiftEndTime < presentTime) {// Candidate Need to checkout

            if (shiftEndTime < presentTime) {
                return;
            }


            //************** END ************************/
            let offsiteDataSingle = {
                "offSideId": offSideId,
                "checkinTime": checkinTime,
                "destinationName": destinationName,
                "geolocation": {
                    "type": "Point",
                    "coordinates": [parseFloat(destinationLong), parseFloat(destinationLat)]
                }
            };

            let fetchOfficeTrackinDetailsStatus = await employerJobBusiness.fetchOfficeTrackinDetails(candidateId, jobId, roleId, employerId, date);

            if (fetchOfficeTrackinDetailsStatus.isError == true) {
                res.json({
                    isError: true,
                    message: "Some Error Occur While Fetching Offsite Tracking Details",
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let tableId;
            if (fetchOfficeTrackinDetailsStatus.isExist == true) {

                tableId = fetchOfficeTrackinDetailsStatus.response[0].offsiteTrackingId;

                offsiteTracking.updateOne(
                    {"offsiteTrackingId": tableId},
                    {
                        $push: {
                            OffsiteData: offsiteDataSingle
                        }
                    },
                    function (updatederror, updatedresponse) {

                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: "Some Error Occur While updating data",
                                statuscode: 404,
                                details: null
                            });
                        } else {
                            res.json({
                                isError: false,
                                message: "Successfully Updated",
                                statuscode: 200,
                                details: null
                            });
                        }
                    });

            } else { // Else of if (fetchOfficeTrackinDetailsStatus.isExist == true) {

                // Now we are going to insert
                let milliseconds = new Date().getTime();
                offsiteTrackingId = "OST" + milliseconds;
                let OffsiteData = [];
                OffsiteData.push(offsiteDataSingle);
                let insertparams = {
                    '_id': new mongoose.Types.ObjectId(),
                    'offsiteTrackingId': offsiteTrackingId,
                    'candidateId': candidateId,
                    'jobId': jobId,
                    'roleId': roleId,
                    'employerId': employerId,
                    'date': date,
                    'OffsiteData': OffsiteData
                }

                offsiteTracking.create(insertparams, function (error, item) {

                    if (error) {
                        res.json({
                            isError: true,
                            message: "Some Error Occur While InsertinG data",
                            statuscode: 404,
                            details: null
                        });

                        return;
                    } else {
                        res.json({
                            isError: false,
                            message: "Successfully Inserted",
                            statuscode: 200,
                            details: item
                        });
                    }
                });
            } // END if (fetchOfficeTrackinDetailsStatus.isExist == true) {


        } catch (error) {

            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    /*
        This api is written to update IsPushStatus
    */
    setPushStatus: (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            if (!req.body.candidateId || !req.body.isPushNotificationStatus) {
                return;
            }

            let isPushNotificationStatus = true;
            if (req.body.isPushNotificationStatus == "false") {
                isPushNotificationStatus = false;
            }

            models.candidate.updateOne(
                {"candidateId": candidateId},
                {"isPushNotificationStatus": isPushNotificationStatus},
                function (err, response) {
                    if (err) {
                        res.json({
                            isError: true,
                            message: "some error occur while updating ",
                            statuscode: 404,
                            details: null
                        });
                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        });
                    }
                });


        } catch (error) {

            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },
    /*
         This function is used to the fetch isPushStatus
    */
    getPushStatus: (req, res, next) => {

        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            if (!req.body.candidateId) {
                return;
            }

            let aggrQuery = [
                {
                    $match: {
                        'candidateId': candidateId
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'isPushNotificationStatus': 1
                    }
                },

            ];

            models.candidate.aggregate(aggrQuery,
                function (err, response) {
                    if (err) {
                        res.json({
                            isError: true,
                            message: "Error Occur in Fetching the data",
                            statuscode: 404,
                            details: null
                        });
                    } else {
                        res.json({
                            isError: false,
                            message: "Success",
                            statuscode: 200,
                            details: response
                        });
                    }


                });

        } catch (error) {

            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },

    /*
        This function is written to delete the notification
        on the basis of notifi
    */
    deleteNotificationforCandidate: (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let notificationId = req.body.notificationId ? req.body.notificationId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Notification Id"
            });

            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            if (!req.body.candidateId || !req.body.notificationId) {
                return;
            }

            models.candidate.updateOne({"candidateId": candidateId},
                {"$pull": {"notification": {"_id": notificationId}}},
                {safe: true, multi: true},
                function (updatederror, updatedresponse) {
                    if (updatederror) {
                        res.json({
                            isError: true,
                            message: "Error Occur in deleting notification",
                            statuscode: 404,
                            details: null
                        })
                    } else {
                        if (updatedresponse.nModified == 1) {
                            res.json({
                                isError: false,
                                message: "notification successfuly deleted",
                                statuscode: 200,
                                details: null
                            });
                        } else {
                            res.json({
                                isError: false,
                                message: "Notification was not deleted",
                                statuscode: 405,
                                details: null
                            })

                        }
                    }
                }) // END models.candidate.updateOne({ "candidateId": candidateId }

        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    /*
        This function is written to fetch all the job related notification
        for a particular candidate
    */
    fetchNotificationforCandidate: (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            if (!req.body.candidateId) {
                return;
            }

            let aggrQuery = [
                {
                    $match: {
                        'candidateId': candidateId
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'notification': 1
                    }
                },
                {
                    $unwind: "$notification"
                },

                {
                    $project: {
                        'notification': 1,
                        'generatedId': '$notification._id',
                    }
                },

                {
                    $sort: {generatedId: -1}
                },

                {
                    $group: {
                        _id: null,
                        total: {
                            $sum: 1
                        },
                        results: {
                            $push: '$$ROOT'
                        }
                    }
                },

                {
                    $project: {
                        '_id': 0,
                        'total': 1,
                        'noofpage': {$ceil: {$divide: ["$total", perPage]}},
                        'results': {
                            $slice: [
                                '$results', page * perPage, perPage
                            ]
                        }
                    }
                }

            ];
            models.candidate.aggregate(aggrQuery).exec((err, response) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });

                    return;
                }

                if (response.length < 1) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['1009'],
                        statuscode: 1009,
                        details: null
                    });

                    return;
                }

                if (response.length > 0) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        detail: response
                    });

                    return;
                }
            })

        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    applyAgainstinvitation: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************


            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) {
                return;
            }

            let currentDate = new Date();
            let currentTimestamp = currentDate.getTime();


            //========================================================================
            // Here, we check whether the vacancy still remains or not
            let checkJobAvaibility = await jobProfileBusiness.checkJobAvaibility({
                "roleId": roleId,
                "jobId": jobId
            })

            let isJobAvailabel = checkJobAvaibility.isJobAvailabel;
            let endRealDate = checkJobAvaibility.roleDetails.endRealDate;
            let endRealDateTimeStamp = checkJobAvaibility.roleDetails.endRealDateTimeStamp;

            if (isJobAvailabel != true) {
                res.json({
                    isError: true,
                    message: "Requirement for this job has been fulfilled",
                    statuscode: 204,
                    details: null
                })

                return;
            }
            //========================================================================

            // this function is used to check whether candidate is authorised or not // by Ankur on 06-02-20
            async function checkCandidateAuthorised(candidateId) {  // start of checkCandidateAuthorised

                return new Promise(function (resolve, reject) {

                    let aggrQuery = [
                        {
                            $match: {
                                'candidateId': candidateId,
                            }
                        },
                        {
                            $project: {
                                "Status": 1
                            }
                        },

                    ];

                    models.candidate.aggregate(aggrQuery, function (err, response) {

                        if (err) {
                            resolve({"isError": true});
                        } else {

                            if (response.length > 0) {
                                resolve({
                                    "isError": false,
                                    "isExist": true,
                                    "candidateDetail": response
                                });
                            } else {
                                resolve({
                                    "isError": false,
                                    "isExist": false,
                                });
                            }
                        }

                    });
                })
            } // END of checkCandidateAuthorised
            // ====================================


            let updateWith = {
                'roleId': roleId,
                'candidateId': candidateId
            }
            let findWhere = {
                'appiliedFrom.roleId': roleId,
                'appiliedFrom.candidateId': candidateId,
                'jobId': jobId
            }
            models.job.findOne(findWhere,
                {'appiliedFrom.$': 1},
                async function (err, item) {
                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    } else {

                        // ====================================
                        // Coded by Ankur on 06-02-20
                        let checkCandidateAuthorisedStatus = await checkCandidateAuthorised(candidateId);
                        if (checkCandidateAuthorisedStatus.isError == true || checkCandidateAuthorisedStatus.isExist == false) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                            return;
                        }

                        if (checkCandidateAuthorisedStatus.candidateDetail[0].Status.isApprovedByAot == false) {
                            res.json({
                                isError: true,
                                message: 'Candidate is not approved by admin yet',
                                statuscode: 1026,
                                details: null
                            });
                            return;
                        }
                        //===========================================

                        if (item) {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['1026'],
                                statuscode: 1026,
                                details: null
                            })
                        } else {

                            let hireList = {
                                "currentTimestamp": currentTimestamp,
                                "currentDate": currentDate,
                                "roleId": roleId,
                                "employerId": employerId,
                                "jobId": jobId,
                                "candidateId": candidateId
                            };

                            let approvedTo = {
                                "roleId": roleId,
                                "candidateId": candidateId
                            }
                            models.job.updateOne({'employerId': employerId, 'jobId': jobId},
                                {
                                    $push: {
                                        // 'appiliedFrom':updateWith
                                        "hireList": hireList,
                                        "approvedTo": approvedTo
                                    }
                                },
                                function (error, response) {
                                    if (error) {

                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    } else {

                                        let insertedValue = {
                                            "endRealDate": endRealDate,
                                            "endRealDateTimeStamp": endRealDateTimeStamp,
                                            "currentTimestamp": currentTimestamp,
                                            "currentDate": currentDate,
                                            "roleId": roleId,
                                            "employerId": employerId,
                                            "jobId": jobId,
                                            "candidateId": candidateId
                                        }

                                        if (response.nModified == 1) {
                                            models.candidate.updateOne({'candidateId': candidateId},
                                                // { $push: { "appliedJobs":insertedValue }, // this field was removed to hire the candidate directly
                                                {
                                                    $push: {"hiredJobs": insertedValue},
                                                    "$pull": {
                                                        "invitationToApplyJobs": {
                                                            "jobId": jobId,
                                                            "roleId": roleId
                                                        }
                                                    }
                                                },
                                                function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {
                                                    if (appliedJobsUpdatedError) {
                                                        console.log("error 2", appliedJobsUpdatedError)
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                    if (appliedJobsUpdatedresponse.nModified == 1) {

                                                        models.candidate.find({"candidateId": candidateId}, function (error, data) {

                                                            models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                if (pushNotificationStatus == true) {
                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job")
                                                                        .then(function (nofify) {
                                                                            res.json({
                                                                                isError: false,
                                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                                statuscode: 200,
                                                                                details: null
                                                                            })
                                                                        })
                                                                } else {
                                                                    res.json({
                                                                        isError: false,
                                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                                        statuscode: 200,
                                                                        details: null
                                                                    })
                                                                }


                                                            })
                                                        })
                                                    } else {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        })
                                                    }
                                                })
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                        }
                    }
                })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    applyAgainstinvitationBkp: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************


            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) {
                return;
            }

            let currentDate = new Date();
            let currentTimestamp = currentDate.getTime();

            // this function is used to check whether candidate is authorised or not // by Ankur on 06-02-20
            async function checkCandidateAuthorised(candidateId) {  // start of checkCandidateAuthorised

                return new Promise(function (resolve, reject) {

                    let aggrQuery = [
                        {
                            $match: {
                                'candidateId': candidateId,
                            }
                        },
                        {
                            $project: {
                                "Status": 1
                            }
                        },

                    ];

                    models.candidate.aggregate(aggrQuery, function (err, response) {

                        if (err) {
                            resolve({"isError": true});
                        } else {

                            if (response.length > 0) {
                                resolve({
                                    "isError": false,
                                    "isExist": true,
                                    "candidateDetail": response
                                });
                            } else {
                                resolve({
                                    "isError": false,
                                    "isExist": false,
                                });
                            }
                        }

                    });
                })
            } // END of checkCandidateAuthorised
            // ====================================


            let updateWith = {
                'roleId': roleId,
                'candidateId': candidateId
            }
            let findWhere = {
                'appiliedFrom.roleId': roleId,
                'appiliedFrom.candidateId': candidateId,
                'jobId': jobId
            }
            models.job.findOne(findWhere,
                {'appiliedFrom.$': 1},
                async function (err, item) {
                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    } else {

                        // ====================================
                        // Coded by Ankur on 06-02-20
                        let checkCandidateAuthorisedStatus = await checkCandidateAuthorised(candidateId);
                        if (checkCandidateAuthorisedStatus.isError == true || checkCandidateAuthorisedStatus.isExist == false) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                            return;
                        }

                        if (checkCandidateAuthorisedStatus.candidateDetail[0].Status.isApprovedByAot == false) {
                            res.json({
                                isError: true,
                                message: 'Candidate is not approved by admin yet',
                                statuscode: 1026,
                                details: null
                            });
                            return;
                        }
                        //===========================================

                        if (item) {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['1026'],
                                statuscode: 1026,
                                details: null
                            })
                        } else {
                            let hireList = {
                                "currentTimestamp": currentTimestamp,
                                "currentDate": currentDate,
                                "roleId": roleId,
                                "employerId": employerId,
                                "jobId": jobId,
                                "candidateId": candidateId
                            };

                            let approvedTo = {
                                "roleId": roleId,
                                "candidateId": candidateId
                            }
                            models.job.updateOne({'employerId': employerId, 'jobId': jobId},
                                {
                                    $push: {
                                        // 'appiliedFrom':updateWith
                                        "hireList": hireList,
                                        "approvedTo": approvedTo
                                    }
                                },
                                function (error, response) {
                                    if (error) {
                                        console.log("error 1", error)
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    } else {


                                        let insertedValue = {
                                            "currentTimestamp": currentTimestamp,
                                            "currentDate": currentDate,
                                            "roleId": roleId,
                                            "employerId": employerId,
                                            "jobId": jobId,
                                            "candidateId": candidateId
                                        }
                                        if (response.nModified == 1) {
                                            models.candidate.updateOne({'candidateId': candidateId},
                                                // { $push: { "appliedJobs":insertedValue }, // this field was removed to hire the candidate directly
                                                {
                                                    $push: {"hiredJobs": insertedValue},
                                                    "$pull": {
                                                        "invitationToApplyJobs": {
                                                            "jobId": jobId,
                                                            "roleId": roleId
                                                        }
                                                    }
                                                },
                                                function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {
                                                    if (appliedJobsUpdatedError) {
                                                        console.log("error 2", appliedJobsUpdatedError)
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                    if (appliedJobsUpdatedresponse.nModified == 1) {

                                                        models.candidate.find({"candidateId": candidateId}, function (error, data) {

                                                            models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                if (pushNotificationStatus == true) {
                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job")
                                                                        .then(function (nofify) {
                                                                            res.json({
                                                                                isError: false,
                                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                                statuscode: 200,
                                                                                details: null
                                                                            })
                                                                        })
                                                                } else {
                                                                    res.json({
                                                                        isError: false,
                                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                                        statuscode: 200,
                                                                        details: null
                                                                    })
                                                                }


                                                            })
                                                        })
                                                    } else {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        })
                                                    }
                                                })
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                        }
                    }
                })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    checkIsWorkingDay: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.date) {
                return;
            }
            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date": date,
            }
            models.CalenderData.findOne(findWhere, {'workingDays.$': 1}, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                if (item == null) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['207'],
                        statuscode: 207,
                        details: {
                            isWorkingDay: false
                        }
                    })
                } else {
                    res.json({
                        isError: false,
                        message: "Today is your working day",
                        statuscode: 200,
                        details: {
                            isWorkingDay: true,
                            shiftTime: item.workingDays[0].shiftTime,

                        }
                    })
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    // This function is written to fetch the details fo checkin and checkout
    getTimeTrackingForDate: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id",
                isError: true,
            });

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - roleId Id",
                isError: true,
            });

            let date = req.body.date ? req.body.date : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - date",
                isError: true,
            });

            if (!req.body.candidateId) {
                return;
            }

            let aggrQuery = [
                {
                    '$match': {
                        $and: [
                            {'candidateId': candidateId},
                            {'jobId': jobId},
                            {'roleId': roleId}
                        ]
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        workingDays: 1
                    }
                },

                {$unwind: "$workingDays"},

                {
                    $project: {
                        jobId: "$jobId",
                        roleId: "$roleId",
                        workingDays: "$workingDays",
                        date: "$workingDays.date"
                    }
                },

                {
                    '$match': {'date': date}
                },

                {
                    $lookup: {
                        from: "jobs",
                        localField: "jobId",
                        foreignField: "jobId",
                        as: "jobDetails"
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        workingDays: 1,
                        date: 1,
                        jobDb: {$arrayElemAt: ["$jobDetails", 0]}
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        workingDays: 1,
                        date: 1,
                        // jobDb: 1,
                        jobCollection: {
                            $filter: {
                                input: '$jobDb.jobDetails.roleWiseAction',
                                as: 'jobCollection',
                                cond: {$eq: ['$$jobCollection.roleId', '$roleId']}
                            }
                        },
                        _id: 0
                    }
                },
            ];

            //================
            let i;
            models.CalenderData.aggregate(aggrQuery, function (err, response) {


                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    if (response.length < 1) {
                        res.json({
                            isError: false,
                            message: "No data found",
                            statuscode: 404,
                            details: null
                        })
                    } else {


                        //======================== FOrmatting of Checkin checkout
                        for (i = 0; i < response[0].workingDays.onSite.length; i++) {

                            // Formatting Onsite checkin and checkout
                            if (isInteger(response[0].workingDays.onSite[i].checkOut)) {
                                CheckOut = response[0].workingDays.onSite[i].checkOut.toString();
                                CheckOut = CheckOut.concat(".00")
                            } else {
                                CheckOut = response[0].workingDays.onSite[i].checkOut.toString();
                                lengthOfCheckOut = CheckOut.split('.')[1].length;

                                if (lengthOfCheckOut == 1) {
                                    CheckOut = CheckOut.concat("0")
                                }
                            }

                            if (CheckOut.length < 5) {
                                CheckOut = "0".concat(CheckOut);
                            }

                            response[0].workingDays.onSite[i].checkOut = CheckOut;

                            if (isInteger(response[0].workingDays.onSite[i].checkIn)) {
                                checkIn = response[0].workingDays.onSite[i].checkIn.toString();
                                checkIn = checkIn.concat(".00")
                            } else {
                                checkIn = response[0].workingDays.onSite[i].checkIn.toString();
                                lengthOfcheckIn = checkIn.split('.')[1].length;

                                if (lengthOfcheckIn == 1) {
                                    checkIn = checkIn.concat("0")
                                }
                            }

                            if (checkIn.length < 5) {
                                checkIn = "0".concat(checkIn);
                            }

                            response[0].workingDays.onSite[i].checkIn = checkIn;

                        } // END for (i = 0; i < response[0].workingDays.onSite.length; i++) {


                        for (i = 0; i < response[0].workingDays.offSite.length; i++) {

                            // Formatting OffSite checkin and checkout
                            if (isInteger(response[0].workingDays.offSite[i].CheckOut)) {
                                CheckOut = response[0].workingDays.offSite[i].checkOut.toString();
                                CheckOut = CheckOut.concat(".00")
                            } else {

                                CheckOut = response[0].workingDays.offSite[i].checkOut.toString();
                                if (CheckOut == 0) {
                                    CheckOut = "00.00";
                                } else {
                                    lengthOfCheckOut = Number(CheckOut.split('.')[1].length);

                                    if (lengthOfCheckOut == 1) {
                                        CheckOut = CheckOut.concat("0")
                                    }
                                }

                            }

                            if (CheckOut.length < 5) {
                                CheckOut = "0".concat(CheckOut);
                            }

                            response[0].workingDays.offSite[i].checkOut = CheckOut;


                            if (isInteger(response[0].workingDays.offSite[i].checkIn)) {
                                checkIn = response[0].workingDays.offSite[i].checkIn.toString();
                                checkIn = checkIn.concat(".00")
                            } else {
                                checkIn = response[0].workingDays.offSite[i].checkIn.toString();
                                lengthOfcheckIn = checkIn.split('.')[1].length;

                                if (lengthOfcheckIn == 1) {
                                    checkIn = checkIn.concat("0")
                                }
                            }

                            if (checkIn.length < 5) {
                                checkIn = "0".concat(checkIn);
                            }

                            response[0].workingDays.offSite[i].checkIn = checkIn;

                        } // END for (i = 0; i < response[0].workingDays.onSite.length; i++) {
                        //========================END ============

                        totalOnsite = response[0].workingDays.totalOnsiteCheckinTimeInMns;
                        totalOffsite = response[0].workingDays.totaloffSiteCheckinTimeInMns;
                        totalCheckinTimeInMin = Number(totalOnsite) + Number(totalOffsite);

                        totalHoursForCheckin = Math.floor(totalCheckinTimeInMin / 60);
                        totalMinutesForCheckIn = totalCheckinTimeInMin % 60;


                        ratePerHour = response[0].jobCollection[0].payment.ratePerHour;
                        totalHour = response[0].jobCollection[0].payment.totalHour;
                        totalPayment = response[0].jobCollection[0].payment.totalPayment;

                        paymentForHour = totalPayment / totalHour * totalHoursForCheckin;
                        paymentForMinute = totalPayment / totalHour * totalMinutesForCheckIn / 60;
                        exactPayment = (paymentForHour + paymentForMinute).toFixed(2);


                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: response[0],
                            exactPayment: exactPayment

                        })
                    }
                }

            });

        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    getTimeTracking: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 50;
            if (!req.body.candidateId) {
                return;
            }
            let aggrQuery = [
                {
                    '$match': {'candidateId': candidateId}
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        total: {$sum: "$workingDays.totalTimeInMinutes"},
                        totalOnsite: {$sum: "$workingDays.totalOnsiteCheckinTimeInMns"},
                        totalOffsite: {$sum: "$workingDays.totaloffSiteCheckinTimeInMns"},
                    }
                },

                {
                    $lookup: {
                        from: "jobs",
                        localField: "jobId",
                        foreignField: "jobId",
                        as: "jobDetails"
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        total: 1,
                        totalOnsite: 1,
                        totalOffsite: 1,
                        jobDb: {$arrayElemAt: ["$jobDetails", 0]}
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        total: 1,
                        totalOnsite: 1,
                        totalOffsite: 1,
                        jobCollection: {
                            $filter: {
                                input: '$jobDb.jobDetails.roleWiseAction',
                                as: 'jobCollection',
                                cond: {$eq: ['$$jobCollection.roleId', '$roleId']}
                            }
                        },
                        _id: 0
                    }
                },


                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        total: 1,
                        totalOnsite: 1,
                        totalOffsite: 1,
                        jobDb: {$arrayElemAt: ["$jobCollection", 0]}
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        total: 1,
                        totalOnsite: 1,
                        totalOffsite: 1,
                        jobDb: 1,

                    }
                },

                /*
                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        jobDb:1,

                        totalFormatedMinute: {

                            "$switch": {
                                branches: [
                                    {
                                        case: { $lt: ["$totalMinute", 10] },
                                        // then: { $concat: ["0", { $toString: "$totalMinute" }] }
                                        then: { $concat: ["0", { $toString: "$totalMinute" }] }
                                    },
                                    {
                                        case: { $gte: ["$totalMinute", 10] },
                                        then: { $toString: "$totalMinute" }
                                    },
                                ],
                                default: { $toString: "$totalMinute" }
                            }
                        },
                        totalFormatedHour: {
                            "$switch": {
                                branches: [
                                    {
                                        case: { $lt: ["$totalHour", 10] },
                                        then: { $concat: ["0", { $toString: "$totalHour" }] }
                                    },
                                    {
                                        case: { $gte: ["$totalHour", 10] },
                                        then: { $toString: "$totalHour" }
                                    },
                                ],
                                default: { $toString: "$totalHour" }
                            }
                        }
                    }
                },

                */

                ///=======

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        location: "$jobDb.location",
                        locationName: "$jobDb.locationName",
                        description: "$jobDb.description",
                        roleName: "$jobDb.roleName",
                        jobType: "$jobDb.jobType",
                        setTime: "$jobDb.setTime",
                        setTimefirst: {$arrayElemAt: ["$jobDb.setTime", 0]},
                        // setTimefirstEndDate: { "$setTimefirst.endDate" },
                        paymentStatus: "$jobDb.paymentStatus",
                        status: "$jobDb.status",
                        payment: "$jobDb.payment",
                        totalOnsite: "$totalOnsite",
                        totalOffsite: "$totalOffsite",

                        totalHours: {$floor: {$divide: ["$total", 60]}},
                        totalMinutes: {$mod: ["$total", 60]},

                        totalOnsiteHours: {$floor: {$divide: ["$totalOnsite", 60]}},
                        totalOnsiteMinutes: {$mod: ["$totalOnsite", 60]},

                        totalOffsiteHours: {$floor: {$divide: ["$totalOffsite", 60]}},
                        totalOffsiteMinutes: {$mod: ["$totalOffsite", 60]}

                        // totalTime: { $concat: [{ $toString: "$totalFormatedHour" }, ":", { $toString: "$totalFormatedMinute" }] }
                    }
                },

                {
                    $project: {
                        jobId: 1,
                        roleId: 1,
                        location: 1,
                        locationName: 1,
                        description: 1,
                        roleName: 1,
                        jobType: 1,
                        setTime: 1,
                        setTimefirst: 1,
                        setTimefirstEndDate: "$setTimefirst.endDate",
                        paymentStatus: 1,
                        status: 1,
                        payment: 1,
                        totalOnsite: 1,
                        totalOffsite: 1,

                        totalHours: 1,
                        totalMinutes: 1,

                        totalOnsiteHours: 1,
                        totalOnsiteMinutes: 1,

                        totalOffsiteHours: 1,
                        totalOffsiteMinutes: 1

                        // totalTime: { $concat: [{ $toString: "$totalFormatedHour" }, ":", { $toString: "$totalFormatedMinute" }] }
                    }
                },

                {$sort: {setTimefirstEndDate: -1}},

                {
                    $group: {
                        _id: null,
                        total: {
                            $sum: 1
                        },
                        results: {
                            $push: '$$ROOT'
                        }
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'total': 1,
                        'noofpage': {$ceil: {$divide: ["$total", perPage]}},
                        'results': {
                            $slice: [
                                '$results', page * perPage, perPage
                            ]
                        }
                    }
                }

            ]

            // ===============


            //================
            models.CalenderData.aggregate(aggrQuery).exec(async (error, response) => {

                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    if (response.length < 1) {
                        res.json({
                            isError: false,
                            message: "No data found",
                            statuscode: 404,
                            details: null
                        })
                    }

                    if (response.length > 0) {
                        let i;
                        let totalHours;
                        let totalMinutes;
                        let totalFormatedHour;
                        let totalFormatedMinute;
                        let totalTime;

                        let ratePerHour;
                        let exactPayment;
                        let paymentForHour;
                        let paymentForMinute;
                        let totalHour;
                        let totalPayment;

                        for (i = 0; i < response[0].results.length; i++) {

                            //    totalHours = response[0].results[i].totalHours;
                            //    totalMinutes = response[0].results[i].totalMinutes;
                            totalOnsite = response[0].results[i].totalOnsite;
                            totalOffsite = response[0].results[i].totalOffsite;
                            totalCheckinTimeInMin = Number(totalOnsite) + Number(totalOffsite);

                            totalHours = Math.floor(totalCheckinTimeInMin / 60);
                            totalMinutes = totalCheckinTimeInMin % 60;


                            // ***************calculation of exact payment *******************************
                            ratePerHour = response[0].results[i].payment.ratePerHour;
                            totalHour = response[0].results[i].payment.totalHour;
                            totalPayment = response[0].results[i].payment.totalPayment;

                            paymentForHour = totalPayment / totalHour * totalHours;
                            paymentForMinute = totalPayment / totalHour * totalMinutes / 60;
                            exactPayment = (paymentForHour + paymentForMinute).toFixed(2);

                            response[0].results[i].exactPayment = exactPayment;


                            //************************* END **********************************************
                            if (totalHours < 10) {
                                totalFormatedHour = "0" + totalHours;
                            } else {
                                totalFormatedHour = totalHours;
                            }


                            if (totalMinutes < 10) {
                                totalFormatedMinute = "0" + totalMinutes;
                            } else {
                                totalFormatedMinute = totalMinutes;
                            }


                            totalTime = totalFormatedHour + " : " + totalFormatedMinute;
                            response[0].results[i].totalTime = totalTime;

                        }

                        // This function is written to fetch the Vat, Ni and Fee
                        async function fetchVatNiFee() {

                            return new Promise(function (resolve, reject) {

                                EnvInfo.find()
                                    .select('ni vat fee')
                                    .exec()
                                    .then(result => {

                                        if (result.length > 0) {
                                            let response = {
                                                ni: result[0].ni,
                                                vat: result[0].vat,
                                                fee: result[0].fee
                                            };
                                            resolve({
                                                "isError": false,
                                                "vatNiFee": response
                                            });

                                        } else {
                                            resolve({
                                                "isError": true,
                                            });
                                        }
                                    });
                            })  // END Promise
                        } //  async function checkUser(toUser) {

                        // Here, we are inserting the the details of sip call
                        let fetchVatNiFeeStatus = await fetchVatNiFee();

                        if (fetchVatNiFeeStatus.isError == true) {
                            res.json({
                                isError: true,
                                message: "VAT, NI and FEE Not Found",
                                statuscode: 404,
                                details: null
                            });

                            return;
                        }

                        response[0].results[0].payment.vat = fetchVatNiFeeStatus.vatNiFee.vat;
                        response[0].results[0].payment.ni = fetchVatNiFeeStatus.vatNiFee.ni;
                        response[0].results[0].payment.fee = fetchVatNiFeeStatus.vatNiFee.fee;

                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: response[0]
                            // details: response[0].results[0].payment,
                        })
                    }


                }
            })

        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    autometicChechOut: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            let checkOutTime = req.body.checkOutTime ? req.body.checkOutTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOutTime"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Latitude"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination longitude"
            });
            if (!req.body.destinationLat || !req.body.destinationLong || !req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.employerId || !req.body.checkOutTime) {
                return;
            }
            let aggrQuery1 = [
                {
                    '$match': {
                        $and: [
                            {'jobId': jobId},
                            {'jobDetails.roleWiseAction.roleId': roleId}
                        ]
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: {$or: [{$eq: ["$roleId", roleId]}, {$not: "$roleId"}]},
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery1).exec((error, primaryResult) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                if (primaryResult.length == 0) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1021'],
                        statuscode: 1021,
                        details: null
                    })
                } else {
                    let aggrQuery = [
                        {
                            $geoNear: {
                                near: {
                                    type: "Point",
                                    coordinates: [parseFloat(destinationLong), parseFloat(destinationLat)]
                                },
                                distanceField: "dist.locationDistance",
                                // maxDistance: primaryResult[0].jobDetails.roleWiseAction[0].locationTrackingRadius,
                                maxDistance: 50,
                                query: {type: "public"},
                                includeLocs: "dist.location",
                                query: {},
                                spherical: true
                            }
                        },
                        {
                            '$match': {
                                $and: [
                                    {'jobId': jobId},
                                    {'jobDetails.roleWiseAction.roleId': roleId}
                                ]
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: {$or: [{$eq: ["$roleId", roleId]}, {$not: "$roleId"}]},
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        }
                    ]
                    models.job.aggregate(aggrQuery).exec((err, result) => {
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        } else {


                            if (result.length == 0) {
                                Request.post({
                                    "headers": {"content-type": "application/json"},
                                    // "url": `${config.app.FORMATTED_URL}/api/candidate-job/onsite-checkout`,
                                    "url": `${config.app.FORMATTED_URL_NEW}/api/candidate-job/onsite-checkout`,
                                    "body": JSON.stringify({
                                        "candidateId": candidateId,
                                        "jobId": jobId,
                                        "roleId": roleId,
                                        "employerId": empId,
                                        "checkOutTime": checkOutTime,
                                        "date": date,

                                    })
                                }, (error, response, body) => {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                    if (response) {
                                        res.json({
                                            isError: false,
                                            message: "You have successfully checked out at " + checkOutTime + ". A notification will be sent to your employer. ",
                                            statuscode: 200,
                                            details: {
                                                "isCheckedIn": false,
                                                "lastJobType": ""
                                            }
                                        })
                                    }
                                })
                            } else {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['209'],
                                    statuscode: 209,
                                    details: null
                                });
                            }

                        }
                    })
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    getCandidateWiseAttendance: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }
            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
            }
            models.CalenderData.find(findWhere, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: null
                        })
                    } else {
                        /*
                        let onSiteArr = item[0].workingDays[0].onSite;
                        let totalActiveTime = 0;
                        let onSiteCheckIn;
                        let onSiteCheckOut;
                        let durationBetCheckInCheckOut = 0;
                        let totalCheckedInTime = 0;

                        if (onSiteArr.length > 0) {
                            onSiteArr.map((onSite) => {
                                onSiteCheckIn = onSite.checkIn;
                                onSiteCheckIn = onSiteCheckIn.toString();
                                onSiteCheckIn = Moment(onSiteCheckIn, "HH:mm:ss");

                                onSiteCheckOut = onSite.checkOut;
                                onSiteCheckOut = onSiteCheckOut.toString();
                                onSiteCheckOut = Moment(onSiteCheckOut, "HH:mm:ss");

                                durationBetCheckInCheckOut = Moment.duration(onSiteCheckOut.diff(onSiteCheckIn));

                                totalCheckedInTime = totalCheckedInTime + durationBetCheckInCheckOut;
                            });
                        }

                        let totalOnSiteCheckedInTime = totalCheckedInTime/(60 * 1000);

                        let onsiteCheckinHrs = Math.floor(totalOnSiteCheckedInTime/60);
                        let onsiteCheckinMins = totalOnSiteCheckedInTime % 60;

                        let totalOnsiteTime = onsiteCheckinHrs+"h"+onsiteCheckinMins+"m";

                        let offSiteArr = item[0].workingDays[0].offSite;
                        let offSiteCheckIn;
                        let offSiteCheckOut;
                        totalCheckedInTime = 0;

                        if (offSiteArr.length > 0) {
                            offSiteArr.map((offSite) => {
                                offSiteCheckIn = offSite.checkIn;
                                offSiteCheckIn = offSiteCheckIn.toString();
                                offSiteCheckIn = Moment(offSiteCheckIn, "HH:mm:ss");

                                offSiteCheckOut = offSite.checkOut;
                                offSiteCheckOut = offSiteCheckOut.toString();
                                offSiteCheckOut = Moment(offSiteCheckOut, "HH:mm:ss");

                                durationBetCheckInCheckOut = Moment.duration(offSiteCheckOut.diff(offSiteCheckIn));

                                totalCheckedInTime = totalCheckedInTime + durationBetCheckInCheckOut;
                            });
                        }
                        let totalOffsiteCheckedInTime = totalCheckedInTime/(60 * 1000);

                        let offSiteCheckinHrs = Math.floor(totalOffsiteCheckedInTime/60);
                        let offSiteCheckinMins = totalOffsiteCheckedInTime % 60;

                        let totaloffSiteTime = offSiteCheckinHrs+"h"+offSiteCheckinMins+"m";

                        let totalTimeForcheckIn = totalOnSiteCheckedInTime + totalOffsiteCheckedInTime;
                        let totalTimeForcheckIninHrs = Math.floor(totalTimeForcheckIn/60);
                        let totalTimeForcheckIninMins = totalTimeForcheckIn % 60;
                        let totalTimeHrsMin = totalTimeForcheckIninHrs+"h"+totalTimeForcheckIninMins+"m";
                        */
                        if (item.length > 0) {


                            let onSiteArr = item[0].workingDays[0].onSite;
                            let finalCheckIn;
                            let lengthOffinalCheckIn;
                            let totalOnsiteCheckinTimeInMns = item[0].workingDays[0].totalOnsiteCheckinTimeInMns;
                            let totaloffSiteCheckinTimeInMns = item[0].workingDays[0].totaloffSiteCheckinTimeInMns;
                            let totaltimeForCheckInMns = totalOnsiteCheckinTimeInMns + totaloffSiteCheckinTimeInMns;

                            let totalCheckinHrs = Math.floor(totaltimeForCheckInMns / 60);
                            let totalCheckinMins = totaltimeForCheckInMns % 60;

                            let totaltimeForCheckIn = totalCheckinHrs + "h" + totalCheckinMins + "m";
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: item[0],
                                totaltimeForCheckInMns: totaltimeForCheckInMns,
                                totaltimeForCheckIn: totaltimeForCheckIn,

                            })

                        } else { // else if (item.length > 0) {
                            res.json({
                                isError: true,
                                message: "Job was not performed by the candidate, not even a single day",
                                statuscode: 204,
                                details: null

                            })
                        } // end if (item.length > 0) {
                    }
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },


    getCandidateWiseAttendanceBkp: async (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });


            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }


            let aggrQuery = [
                {
                    '$match': {'jobId': jobId}
                },

                {
                    $project:
                        {
                            "roleWiseAction": "$jobDetails.roleWiseAction",
                            "employerId": 1
                        }
                },

                {
                    $project:
                        {
                            "employerId": 1,
                            "roleDetails": {
                                $filter: {
                                    input: "$roleWiseAction",
                                    as: "roleWiseAction",
                                    cond: {$eq: ["$$roleWiseAction.roleId", roleId]}
                                }
                            }
                        }
                },

                {
                    $project:
                        {
                            "employerId": 1,
                            "roleDetails": {$arrayElemAt: ["$roleDetails", 0]}
                        }
                },
            ];

            let fetchJob = await jobProfileBusiness.fetchJob(aggrQuery);

            if (fetchJob.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                })

                return;
            }

            let jobDetails = fetchJob.response[0].roleDetails;
            let employerId = fetchJob.response[0].employerId;

            // res.json({
            //     fetchJob: fetchJob.response[0].roleDetails
            // });
            // return;

            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
            }


            models.CalenderData.find(findWhere, function (err, item) {

                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: null
                        })
                    } else {
                        if (item.length > 0) {

                            let onSiteArr = item[0].workingDays[0].onSite;
                            let totalActiveTime = 0;
                            let onSiteCheckIn;
                            let onSiteCheckOut;
                            let durationBetCheckInCheckOut = 0;
                            let totalCheckedInTime = 0;

                            if (onSiteArr.length > 0) {
                                onSiteArr.map((onSite) => {
                                    onSiteCheckIn = onSite.checkIn;
                                    onSiteCheckIn = onSiteCheckIn.toString();
                                    onSiteCheckIn = Moment(onSiteCheckIn, "HH:mm:ss");

                                    onSiteCheckOut = onSite.checkOut;
                                    onSiteCheckOut = onSiteCheckOut.toString();
                                    onSiteCheckOut = Moment(onSiteCheckOut, "HH:mm:ss");

                                    durationBetCheckInCheckOut = Moment.duration(onSiteCheckOut.diff(onSiteCheckIn));

                                    totalCheckedInTime = totalCheckedInTime + durationBetCheckInCheckOut;
                                });
                            }

                            let totalOnSiteCheckedInTime = totalCheckedInTime / (60 * 1000);

                            let onsiteCheckinHrs = Math.floor(totalOnSiteCheckedInTime / 60);
                            let onsiteCheckinMins = totalOnSiteCheckedInTime % 60;

                            let totalOnsiteTime = onsiteCheckinHrs + "h" + onsiteCheckinMins + "m";

                            let offSiteArr = item[0].workingDays[0].offSite;
                            let offSiteCheckIn;
                            let offSiteCheckOut;
                            totalCheckedInTime = 0;

                            if (offSiteArr.length > 0) {
                                offSiteArr.map((offSite) => {
                                    offSiteCheckIn = offSite.checkIn;
                                    offSiteCheckIn = offSiteCheckIn.toString();
                                    offSiteCheckIn = Moment(offSiteCheckIn, "HH:mm:ss");

                                    offSiteCheckOut = offSite.checkOut;
                                    offSiteCheckOut = offSiteCheckOut.toString();
                                    offSiteCheckOut = Moment(offSiteCheckOut, "HH:mm:ss");

                                    durationBetCheckInCheckOut = Moment.duration(offSiteCheckOut.diff(offSiteCheckIn));

                                    totalCheckedInTime = totalCheckedInTime + durationBetCheckInCheckOut;
                                });
                            }
                            let totalOffsiteCheckedInTime = totalCheckedInTime / (60 * 1000);

                            let offSiteCheckinHrs = Math.floor(totalOffsiteCheckedInTime / 60);
                            let offSiteCheckinMins = totalOffsiteCheckedInTime % 60;

                            let totaloffSiteTime = offSiteCheckinHrs + "h" + offSiteCheckinMins + "m";

                            let totalTimeForcheckIn = totalOnSiteCheckedInTime + totalOffsiteCheckedInTime;
                            let totalTimeForcheckIninHrs = Math.floor(totalTimeForcheckIn / 60);
                            let totalTimeForcheckIninMins = totalTimeForcheckIn % 60;
                            let totalTimeHrsMin = totalTimeForcheckIninHrs + "h" + totalTimeForcheckIninMins + "m";

                            onSiteArr = [];
                            let totalOnsiteCheckinTimeInMns = 0;
                            let totaloffSiteCheckinTimeInMns = 0;
                            let totaltimeForCheckInMns = 0;

                            if (item.length > 0) {
                                onSiteArr = item[0].workingDays[0].onSite;
                                totalOnsiteCheckinTimeInMns = item[0].workingDays[0].totalOnsiteCheckinTimeInMns;
                                totaloffSiteCheckinTimeInMns = item[0].workingDays[0].totaloffSiteCheckinTimeInMns;
                                totaltimeForCheckInMns = totalOnsiteCheckinTimeInMns + totaloffSiteCheckinTimeInMns;
                            }

                            let finalCheckIn;
                            let lengthOffinalCheckIn;
                            let totalCheckinHrs = Math.floor(totaltimeForCheckInMns / 60);
                            let totalCheckinMins = totaltimeForCheckInMns % 60;

                            let totaltimeForCheckIn = totalCheckinHrs + "h" + totalCheckinMins + "m";

                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: item[0],
                                totaltimeForCheckInMns: totaltimeForCheckInMns,
                                totaltimeForCheckIn: totaltimeForCheckIn,
                                employerId: employerId,
                                jobDetails: jobDetails
                            })
                        } else {// else if (item.length > 0) {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: null
                            })
                        }// end if (item.length > 0) {
                    }
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },
    getDateWiseCalenderData: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });

            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });

            if (!req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }
            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date": date,
            }
            models.CalenderData.findOne(findWhere, {'workingDays.$': 1}, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: null
                        })
                    } else {
                        /*
                        let onSiteArr = item.workingDays[0].onSite;
                        let totalActiveTime = 0;
                        let onSiteCheckIn;
                        let onSiteCheckOut;
                        let durationBetCheckInCheckOut = 0;
                        let totalCheckedInTime = 0;

                        if (onSiteArr.length > 0) {
                            onSiteArr.map((onSite) => {
                                onSiteCheckIn = onSite.checkIn;
                                onSiteCheckIn = onSiteCheckIn.toString();
                                onSiteCheckIn = Moment(onSiteCheckIn, "HH:mm:ss");

                                onSiteCheckOut = onSite.checkOut;
                                onSiteCheckOut = onSiteCheckOut.toString();
                                onSiteCheckOut = Moment(onSiteCheckOut, "HH:mm:ss");

                                durationBetCheckInCheckOut = Moment.duration(onSiteCheckOut.diff(onSiteCheckIn));

                                totalCheckedInTime = totalCheckedInTime + durationBetCheckInCheckOut;
                            });
                        }

                        let totalOnSiteCheckedInTime = totalCheckedInTime/(60 * 1000);

                        let onsiteCheckinHrs = Math.floor(totalOnSiteCheckedInTime/60);
                        let onsiteCheckinMins = totalOnSiteCheckedInTime % 60;

                        let totalOnsiteTime = onsiteCheckinHrs+"h"+onsiteCheckinMins+"m";

                        let offSiteArr = item.workingDays[0].offSite;
                        let offSiteCheckIn;
                        let offSiteCheckOut;
                        totalCheckedInTime = 0;

                        if (offSiteArr.length > 0) {
                            offSiteArr.map((offSite) => {
                                offSiteCheckIn = offSite.checkIn;
                                offSiteCheckIn = offSiteCheckIn.toString();
                                offSiteCheckIn = Moment(offSiteCheckIn, "HH:mm:ss");

                                offSiteCheckOut = offSite.checkOut;
                                offSiteCheckOut = offSiteCheckOut.toString();
                                offSiteCheckOut = Moment(offSiteCheckOut, "HH:mm:ss");

                                durationBetCheckInCheckOut = Moment.duration(offSiteCheckOut.diff(offSiteCheckIn));

                                totalCheckedInTime = totalCheckedInTime + durationBetCheckInCheckOut;
                            });
                        }
                        let totalOffsiteCheckedInTime = totalCheckedInTime/(60 * 1000);

                        let offSiteCheckinHrs = Math.floor(totalOffsiteCheckedInTime/60);
                        let offSiteCheckinMins = totalOffsiteCheckedInTime % 60;

                        let totaloffSiteTime = offSiteCheckinHrs+"h"+offSiteCheckinMins+"m";
                        */


                        let totalOnsiteCheckinTimeInMns = item.workingDays[0].totalOnsiteCheckinTimeInMns;
                        let totaloffSiteCheckinTimeInMns = item.workingDays[0].totaloffSiteCheckinTimeInMns;

                        let totalTimeForcheckIn = totalOnsiteCheckinTimeInMns + totaloffSiteCheckinTimeInMns;
                        let totalTimeForcheckIninHrs = Math.floor(totalTimeForcheckIn / 60);
                        let totalTimeForcheckIninMins = totalTimeForcheckIn % 60;
                        let totalTimeHrsMin = totalTimeForcheckIninHrs + "h" + totalTimeForcheckIninMins + "m";

                        //===================================================================================
                        let onSiteArr = item.workingDays[0].onSite;
                        let offSiteArr = item.workingDays[0].offSite;

                        let finalCheckIn;
                        let lengthOffinalCheckIn;

                        let onSiteArray = onSiteArr.map((onSite) => {

                            if (isInteger(onSite.checkIn)) {
                                finalCheckIn = onSite.checkIn.toString();
                                finalCheckIn = finalCheckIn.concat(".00")
                            } else {
                                finalCheckIn = onSite.checkIn.toString();
                                lengthOffinalCheckIn = finalCheckIn.split('.')[1].length;

                                if (lengthOffinalCheckIn == 1) {
                                    finalCheckIn = finalCheckIn.concat("0")
                                } else {
                                    finalCheckIn = onSite.checkIn.toString();
                                }
                            }

                            if (finalCheckIn.length < 5) {
                                finalCheckIn = "0".concat(finalCheckIn);
                            }

                            if (isInteger(onSite.checkOut)) {
                                finalcheckOut = onSite.checkOut.toString();
                                finalcheckOut = finalcheckOut.concat(".00")
                            } else {
                                finalcheckOut = onSite.checkOut.toString();
                                lengthOffinalCheckIn = finalcheckOut.split('.')[1].length;

                                if (lengthOffinalCheckIn == 1) {
                                    finalcheckOut = finalcheckOut.concat("0")
                                } else {
                                    finalcheckOut = onSite.checkOut.toString();
                                }
                            }

                            if (finalcheckOut.length < 5) {
                                finalcheckOut = "0".concat(finalcheckOut);
                            }

                            return {
                                CheckIn: finalCheckIn,
                                checkOut: finalcheckOut
                            };

                        });

                        let offSiteArray = offSiteArr.map((onSite) => {

                            if (isInteger(onSite.checkIn)) {
                                finalCheckIn = onSite.checkIn.toString();
                                finalCheckIn = finalCheckIn.concat(".00")
                            } else {
                                finalCheckIn = onSite.checkIn.toString();
                                lengthOffinalCheckIn = finalCheckIn.split('.')[1].length;

                                if (lengthOffinalCheckIn == 1) {
                                    finalCheckIn = finalCheckIn.concat("0")
                                } else {
                                    finalCheckIn = onSite.checkIn.toString();
                                }
                            }

                            if (finalCheckIn.length < 5) {
                                finalCheckIn = "0".concat(finalCheckIn);
                            }

                            if (isInteger(onSite.checkOut)) {
                                finalcheckOut = onSite.checkOut.toString();
                                finalcheckOut = finalcheckOut.concat(".00")
                            } else {
                                finalcheckOut = onSite.checkOut.toString();
                                lengthOffinalCheckIn = finalcheckOut.split('.')[1].length;

                                if (lengthOffinalCheckIn == 1) {
                                    finalcheckOut = finalcheckOut.concat("0")
                                } else {
                                    finalcheckOut = onSite.checkOut.toString();
                                }
                            }

                            if (finalcheckOut.length < 5) {
                                finalcheckOut = "0".concat(finalcheckOut);
                            }

                            return {
                                CheckIn: finalCheckIn,
                                checkOut: finalcheckOut
                            };

                        });
                        //===================================================================================
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: item.workingDays[0],
                            onSiteArray: onSiteArray,
                            offSiteArray: offSiteArray,
                            // totalOnSiteCheckedInTime:totalOnSiteCheckedInTime,
                            // totalOnsiteTime:totalOnsiteTime,
                            // totalOffsiteCheckedInTime: totalOffsiteCheckedInTime,
                            // totaloffSiteTime: totaloffSiteTime,
                            totalTimeForcheckIn: totalTimeForcheckIn,
                            totalTimeHrsMin: totalTimeHrsMin

                        })
                    }
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    onlineCheckOut: (req, res, next) => {

        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            let checkOutTime = req.body.checkOutTime ? req.body.checkOutTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOutTime"
            });

            //======================================================================
            let checkoutlocationName = req.body.checkoutlocationName ? req.body.checkoutlocationName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOut Location Name"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - destinationLat"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - destinationLong"
            });
            //=========================================================================

            if (!req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.employerId || !req.body.checkOutTime) {
                return;
            }

            if (!req.body.checkoutlocationName || !req.body.destinationLat || !req.body.destinationLong) {
                return;
            }


            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date": date,
            }
            models.CalenderData.findOne(findWhere, {'workingDays.$': 1}, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['207'],
                        statuscode: 207,
                        details: null
                    })
                } else {
                    let onSiteArr = item.workingDays[0].onSite;

                    let totalOnsiteCheckinTimeInMns = 0;
                    let checkinTime;
                    let checkoutTimeOnsite;
                    let diffInTime;
                    let onSiteCheckinHrs = 0;
                    let onSiteCheckinMins = 0;
                    let totalonSiteTime = 0;
                    let lengthOffinalCheckIn;

                    let tagonSiteArr = 1;
                    if (onSiteArr.length > 0) {
                        onSiteArr.map((onSite) => {
                            if (tagonSiteArr == onSiteArr.length) {

                                if (isInteger(onSite.checkIn)) {
                                    checkinTime = onSite.checkIn.toString();
                                    checkinTime = checkinTime.concat(".00")
                                } else {
                                    checkinTime = onSite.checkIn.toString();
                                    lengthOffinalCheckIn = checkinTime.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkinTime = checkinTime.concat("0")
                                    }
                                }

                                checkinTime = Moment(checkinTime, "HH:mm:ss");

                                if (isInteger(checkOutTime)) {
                                    checkoutTimeOnsite = checkOutTime.toString();
                                    checkoutTimeOnsite = checkoutTimeOnsite.concat(".00")
                                } else {
                                    checkoutTimeOnsite = checkOutTime.toString();
                                    lengthOffinalCheckIn = checkoutTimeOnsite.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkoutTimeOnsite = checkoutTimeOnsite.concat("0")
                                    }
                                }

                                checkoutTimeOnsite = Moment(checkoutTimeOnsite, "HH:mm:ss");

                                diffInTime = Moment.duration(checkoutTimeOnsite.diff(checkinTime));
                                totalOnsiteCheckinTimeInMns = totalOnsiteCheckinTimeInMns + diffInTime;
                            } else {
                                if (isInteger(onSite.checkIn)) {
                                    checkinTime = onSite.checkIn.toString();
                                    checkinTime = checkinTime.concat(".00")
                                } else {
                                    checkinTime = onSite.checkIn.toString();
                                    lengthOffinalCheckIn = checkinTime.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkinTime = checkinTime.concat("0")
                                    }
                                }

                                checkinTime = Moment(checkinTime, "HH:mm:ss");

                                if (isInteger(onSite.checkOut)) {
                                    checkoutTimeOnsite = onSite.checkOut.toString();
                                    checkoutTimeOnsite = checkoutTimeOnsite.concat(".00")
                                } else {
                                    checkoutTimeOnsite = onSite.checkOut.toString();
                                    lengthOffinalCheckIn = checkoutTimeOnsite.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkoutTimeOnsite = checkoutTimeOnsite.concat("0")
                                    }
                                }

                                checkoutTimeOnsite = Moment(checkoutTimeOnsite, "HH:mm:ss");

                                diffInTime = Moment.duration(checkoutTimeOnsite.diff(checkinTime));
                                totalOnsiteCheckinTimeInMns = totalOnsiteCheckinTimeInMns + diffInTime;
                            }

                            tagonSiteArr = tagonSiteArr + 1;
                        });

                        totalOnsiteCheckinTimeInMns = totalOnsiteCheckinTimeInMns / (60 * 1000);
                        onSiteCheckinHrs = Math.floor(totalOnsiteCheckinTimeInMns / 60);
                        onSiteCheckinMins = totalOnsiteCheckinTimeInMns % 60;

                        totalonSiteTime = onSiteCheckinHrs + "h" + onSiteCheckinMins + "m";

                    } // END if (onSiteArr.length > 0) {


                    let show;
                    if (isInteger(item.workingDays[0].finalCheckIn)) {
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        show = finalCheckIn.concat(".00")
                    } else {
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        let lengthOffinalCheckIn = finalCheckIn.split('.')[1].length;

                        if (lengthOffinalCheckIn == 1) {
                            show = finalCheckIn.concat("0")
                        } else {
                            show = item.workingDays[0].finalCheckIn.toString();
                        }
                    }
                    //=============================
                    let checkoutlocation = {
                        "type": "Point",
                        "coordinates": [parseFloat(destinationLong), parseFloat(destinationLat)]
                    };
                    //=============================
                    let startTime = Moment(show, "HH:mm:ss");
                    let endTime = Moment(checkOutTime.toString(), "HH:mm:ss");
                    let duration = Moment.duration(endTime.diff(startTime));
                    let hours = parseInt(duration.asHours());
                    let minutes = parseInt(duration.asMinutes()) - hours * 60;
                    let totalTime = hours + 'h' + minutes + 'm';
                    let totalCollectedMinute = (hours * 60) + minutes;
                    let toSet;

                    let toSetcheckoutlocationName;
                    let toSetcheckoutlocation;

                    item.workingDays[0].onSite.map((levelData, levelIndex) => {
                        if (levelData.onSideId == item.workingDays[0].lastOnSideId) {
                            toSet = 'workingDays.$.onSite.' + levelIndex + '.checkOut';
                            toSetcheckoutlocationName = 'workingDays.$.onSite.' + levelIndex + '.checkoutlocationName';
                            toSetcheckoutlocation = 'workingDays.$.onSite.' + levelIndex + '.checkoutlocation';

                            models.CalenderData.updateOne({
                                    "candidateId": candidateId,
                                    "jobId": jobId,
                                    "roleId": roleId,
                                    "workingDays.date": date, "workingDays":
                                        {
                                            $elemMatch: {
                                                "onSite":
                                                    {$elemMatch: {"onSideId": item.workingDays[0].lastOnSideId}}
                                            }
                                        }
                                },
                                {
                                    $set: {
                                        [`${toSet}`]: checkOutTime,
                                        [`${toSetcheckoutlocationName}`]: checkoutlocationName,
                                        [`${toSetcheckoutlocation}`]: checkoutlocation,
                                        "workingDays.$.slotType": 'INACTIVE',
                                        "workingDays.$.finalCheckOut": checkOutTime, "isCheckedIn": false,
                                        "lastJobType": "", "workingDays.$.totalTime": totalTime,
                                        "workingDays.$.totalOnsiteCheckinTimeInMns": totalOnsiteCheckinTimeInMns,
                                        "workingDays.$.totalonSiteTime": totalonSiteTime,
                                        "workingDays.$.totalTimeInMinutes": totalCollectedMinute
                                    }
                                },
                                (err, doc) => {
                                    if (err) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    } else {
                                        if (doc.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: "You have successfully checked out at " + checkOutTime + ". A notification will be sent to your employer",
                                                statuscode: 200,
                                                details: {
                                                    "isCheckedIn": false,
                                                    "lastJobType": ""
                                                }
                                            })
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })

                        }
                    })
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    offlineCheckOut: async (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });
            let checkOutTime = req.body.checkOutTime ? req.body.checkOutTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkOutTime"
            });

            //==========================================================================
            let checkoutlocationName = req.body.checkoutlocationName ? req.body.checkoutlocationName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Checkout location Name"
            });

            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - destinationLat"
            });

            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - destinationLong"
            });
            //=====================================================================


            if (!req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.employerId || !req.body.checkOutTime) {
                return;
            }

            if (!req.body.checkoutlocationName || !req.body.destinationLat || !req.body.destinationLong) {
                return;
            }
            //sendNotificationEmailCheckout
            // message:"You have successfully checked out @"+checkOutTime+".A notification is sent to employer. ",

            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date": date,
            }

            models.CalenderData.findOne(findWhere, {'workingDays.$': 1}, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['207'],
                        statuscode: 207,
                        details: null
                    })
                } else {


                    let offSiteArr = item.workingDays[0].offSite;

                    let totaloffSiteCheckinTimeInMns = 0;
                    let checkinTime;
                    let checkoutTimeoffSite;
                    let diffInTime;
                    let offSiteCheckinHrs = 0;
                    let offSiteCheckinMins = 0;
                    let totaloffSiteTime = 0;
                    let lengthOffinalCheckIn;

                    let tagoffSiteArr = 1;
                    if (offSiteArr.length > 0) {
                        offSiteArr.map((offSite) => {
                            if (tagoffSiteArr == offSiteArr.length) {

                                if (isInteger(offSite.checkIn)) {
                                    checkinTime = offSite.checkIn.toString();
                                    checkinTime = checkinTime.concat(".00")
                                } else {
                                    checkinTime = offSite.checkIn.toString();
                                    lengthOffinalCheckIn = checkinTime.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkinTime = checkinTime.concat("0")
                                    }
                                }

                                checkinTime = Moment(checkinTime, "HH:mm:ss");

                                if (isInteger(checkOutTime)) {
                                    checkoutTimeoffSite = checkOutTime.toString();
                                    checkoutTimeoffSite = checkoutTimeoffSite.concat(".00")
                                } else {
                                    checkoutTimeoffSite = checkOutTime.toString();
                                    lengthOffinalCheckIn = checkoutTimeoffSite.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkoutTimeoffSite = checkoutTimeoffSite.concat("0")
                                    }
                                }

                                checkoutTimeoffSite = Moment(checkoutTimeoffSite, "HH:mm:ss");

                                diffInTime = Moment.duration(checkoutTimeoffSite.diff(checkinTime));
                                totaloffSiteCheckinTimeInMns = totaloffSiteCheckinTimeInMns + diffInTime;
                            } else {
                                if (isInteger(offSite.checkIn)) {
                                    checkinTime = offSite.checkIn.toString();
                                    checkinTime = checkinTime.concat(".00")
                                } else {
                                    checkinTime = offSite.checkIn.toString();
                                    lengthOffinalCheckIn = checkinTime.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkinTime = checkinTime.concat("0")
                                    }
                                }

                                checkinTime = Moment(checkinTime, "HH:mm:ss");

                                if (isInteger(offSite.checkOut)) {
                                    checkoutTimeoffSite = offSite.checkOut.toString();
                                    checkoutTimeoffSite = checkoutTimeoffSite.concat(".00")
                                } else {
                                    checkoutTimeoffSite = offSite.checkOut.toString();
                                    lengthOffinalCheckIn = checkoutTimeoffSite.split('.')[1].length;

                                    if (lengthOffinalCheckIn == 1) {
                                        checkoutTimeoffSite = checkoutTimeoffSite.concat("0")
                                    }
                                }

                                checkoutTimeoffSite = Moment(checkoutTimeoffSite, "HH:mm:ss");

                                diffInTime = Moment.duration(checkoutTimeoffSite.diff(checkinTime));
                                totaloffSiteCheckinTimeInMns = totaloffSiteCheckinTimeInMns + diffInTime;
                            }

                            tagoffSiteArr = tagoffSiteArr + 1;
                        });

                        totaloffSiteCheckinTimeInMns = totaloffSiteCheckinTimeInMns / (60 * 1000);
                        offSiteCheckinHrs = Math.floor(totaloffSiteCheckinTimeInMns / 60);
                        offSiteCheckinMins = totaloffSiteCheckinTimeInMns % 60;

                        totaloffSiteTime = offSiteCheckinHrs + "h" + offSiteCheckinMins + "m";

                    } // END if (offSiteArr.length > 0) {


                    let show;
                    if (isInteger(item.workingDays[0].finalCheckIn)) {
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        show = finalCheckIn.concat(".00")
                    } else {
                        let finalCheckIn = item.workingDays[0].finalCheckIn.toString();
                        let lengthOffinalCheckIn = finalCheckIn.split('.')[1].length;

                        if (lengthOffinalCheckIn == 1) {
                            show = finalCheckIn.concat("0")
                        } else {
                            show = item.workingDays[0].finalCheckIn.toString();
                        }
                    }

                    let startTime = Moment(show, "HH:mm:ss");
                    let endTime = Moment(checkOutTime.toString(), "HH:mm:ss");
                    let duration = Moment.duration(endTime.diff(startTime));
                    let hours = parseInt(duration.asHours());
                    let minutes = parseInt(duration.asMinutes()) - hours * 60;
                    let totalTime = hours + 'h' + minutes + 'm';
                    let totalCollectedMinute = (hours * 60) + minutes;
                    let toSet;
                    let toSetcheckoutlocationName;
                    let toSetcheckoutlocation;
                    let checkoutlocation = {
                        "type": "Point",
                        "coordinates": [parseFloat(destinationLong), parseFloat(destinationLat)]
                    };

                    item.workingDays[0].offSite.map((levelData, levelIndex) => {
                        if (levelData.offSideId == item.workingDays[0].lastOffSideId) {
                            toSet = 'workingDays.$.offSite.' + levelIndex + '.checkOut';
                            toSetcheckoutlocationName = 'workingDays.$.offSite.' + levelIndex + '.checkoutlocationName';
                            toSetcheckoutlocation = 'workingDays.$.offSite.' + levelIndex + '.checkoutlocation';

                            models.CalenderData.updateOne({
                                    "candidateId": candidateId,
                                    "jobId": jobId,
                                    "roleId": roleId,
                                    "workingDays.date": date,
                                    "workingDays":
                                        {
                                            $elemMatch:
                                                {
                                                    "offSite":
                                                        {
                                                            $elemMatch: {"offSideId": item.workingDays[0].lastOffSideId}
                                                        }
                                                }
                                        }
                                },
                                {
                                    $set: {
                                        [`${toSet}`]: checkOutTime,
                                        [`${toSetcheckoutlocationName}`]: checkoutlocationName,
                                        [`${toSetcheckoutlocation}`]: checkoutlocation,
                                        "workingDays.$.slotType": 'INACTIVE',
                                        "workingDays.$.finalCheckOut": checkOutTime,
                                        "isCheckedIn": false, "lastJobType": "",
                                        "workingDays.$.totalTime": totalTime,
                                        "workingDays.$.totaloffSiteCheckinTimeInMns": totaloffSiteCheckinTimeInMns,
                                        "workingDays.$.totaloffSiteTime": totaloffSiteTime,
                                        "workingDays.$.totalTimeInMinutes": totalCollectedMinute
                                    }
                                },
                                (err, doc) => {
                                    if (err) {

                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    } else {
                                        if (doc.nModified == 1) {

                                            var sendNotificationEmailCheckoutResult = sendNotificationEmailCheckout(candidateId, checkOutTime, empId, roleId, jobId)

                                            res.json({
                                                isError: false,
                                                message: "You have successfully checked out at " + checkOutTime + ". A notification will be sent to your employer.",
                                                statuscode: 200,
                                            })
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })

                        }
                    })
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    offlineCheckIn: async (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let destinationName = req.body.destinationName ? req.body.destinationName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Name"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Latitude"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination longitude"
            });
            let checkinTime = req.body.checkinTime ? req.body.checkinTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkinTime"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });

            if (!req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.employerId || !req.body.destinationName || !req.body.destinationLat || !req.body.destinationLong || !req.body.checkinTime) {
                return;
            }

            //=======================================================================================================
            //=====================================================================
            let aggrQry = [
                {
                    '$match':
                        {
                            $and: [
                                {'jobId': jobId},
                                {'jobDetails.roleWiseAction.roleId': roleId}
                            ]
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "jobDetails.roleWiseAction.setTime": 1
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "setTime": "$jobDetails.roleWiseAction.setTime",
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "setTime1": {$arrayElemAt: ["$setTime", 0]},
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "setTime": {$arrayElemAt: ["$setTime1", 0]},
                        }
                },

            ]

            let fetchJobInfoStatus = await employerJobBusiness.fetchJobInfo(aggrQry);

            if (fetchJobInfoStatus.isError == true) {
                res.json({
                    isError: true,
                    message: "Error Occur in fetching the job",
                    statuscode: 404,
                    details: null
                });

                return;
            }

            if (fetchJobInfoStatus.jobDetails.length < 1) {
                res.json({
                    isError: false,
                    message: "No Job Found",
                    statuscode: 204,
                    details: null
                });

                return;
            }

            // we apply the restriction if candidate will check in before 15 minute of start time
            let jobStartTime = fetchJobInfoStatus.jobDetails[0].setTime.startTime;
            let splittedJobStartTime = jobStartTime.split(".");
            let jobStartTimeHrsFactor = Number(splittedJobStartTime[0]);
            let jobStartTimeMinsFactor = Number(splittedJobStartTime[1]);
            let jobStartTimeMins = (jobStartTimeHrsFactor * 60) + jobStartTimeMinsFactor;

            let allowedCheckInTimeInMins = jobStartTimeMins - 15;
            let allowedCheckInTimeInMinsFactor = allowedCheckInTimeInMins % 60;
            let allowedCheckInTimeInHrsFactor = Math.floor(allowedCheckInTimeInMins / 60);
            let allowedCheckInTime = allowedCheckInTimeInHrsFactor + (allowedCheckInTimeInMinsFactor / 100);

            let checkInTimeInNumber = Number(checkinTime);

            if (checkInTimeInNumber < allowedCheckInTime) {

                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1033'],
                    statuscode: 1033,
                    details: null
                });

                return;
            }

            //=======================================================================================================
            let findWhere = {
                "candidateId": candidateId,
                "jobId": jobId,
                "roleId": roleId,
                "workingDays.date": date
            }
            models.employer.findOne({"employerId": empId}, function (searchError, searchItem) {
                console.log(searchItem.Status.fcmTocken)
                models.CalenderData.findOne(findWhere, {'workingDays.$': 1}, function (err, item) {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['207'],
                            statuscode: 207,
                            details: null
                        })
                    } else {
                        let type = "OFF-SITE-ID";
                        let autoOffSiteId;
                        AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                            autoOffSiteId = data;
                        })
                        let offSiteDb;
                        offSiteDb = item.workingDays[0].offSite;
                        offSiteDb.push({
                            "offSideId": autoOffSiteId,
                            "checkIn": checkinTime,
                            "destinationName": destinationName,
                            "geolocation": {
                                "type": "Point",
                                "coordinates": [parseFloat(destinationLong), parseFloat(destinationLat)]
                            }
                        });
                        let offSiteData;
                        if (item.workingDays[0].finalCheckIn == 0) {
                            offSiteData = {
                                "workingDays.$.slotType": 'OFFSITE',
                                "workingDays.$.lastOffSideId": autoOffSiteId,
                                "workingDays.$.absentStatus": 1,
                                "workingDays.$.offSite": offSiteDb,
                                "workingDays.$.finalCheckIn": checkinTime,
                                "isCheckedIn": true,
                                "lastJobType": 'OFFSITE'
                            }
                        } else {
                            offSiteData = {
                                "workingDays.$.slotType": 'OFFSITE',
                                "workingDays.$.lastOffSideId": autoOffSiteId,
                                "workingDays.$.absentStatus": 1,
                                "workingDays.$.offSite": offSiteDb,
                                "isCheckedIn": true,
                                "lastJobType": 'OFFSITE'
                            }
                        }
                        models.CalenderData.updateOne(findWhere, {
                            $set:
                            offSiteData
                        }, function (err, affected) {
                            if (err) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            } else {
                                if (affected.nModified == 1) {
                                    //fcm('','',{})
                                    models.authenticate.findOne({'userId': empId}, async function (err, item) {
                                        if (item['mobileNo'] == "") {
                                            var result = await sendCheckinNotificationEmail(item['EmailId'], candidateId, jobId, roleId, checkinTime, date, destinationName, "OFFSITE", "in")
                                            if (result) {
                                                res.json({
                                                    isError: false,
                                                    message: "You have successfully checked in at " + checkinTime + ". A notification will be sent to your employer.",
                                                    statuscode: 200,
                                                })


                                            } else {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                        } else {
                                            var result = await sendCheckinNotificationSms(item['mobileNo'], candidateId, jobId, roleId, checkinTime, date, destinationName, "OFFSITE", "in")
                                            if (result) {
                                                res.json({
                                                    isError: false,
                                                    message: "You have successfully checked in at " + checkinTime + ". A notification will be sent to your employer.",
                                                    statuscode: 200,
                                                })
                                            } else {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                        }

                                    })
                                } else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }
                        })
                    }
                })
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    onlineCheckIn: async (req, res, next) => {
        try {

            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id1"
            });
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let destinationName = req.body.destinationName ? req.body.destinationName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Name"
            });
            let destinationLat = req.body.destinationLat ? req.body.destinationLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination Latitude"
            });
            let destinationLong = req.body.destinationLong ? req.body.destinationLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Destination longitude"
            });
            let checkinTime = req.body.checkinTime ? req.body.checkinTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - checkinTime"
            });
            let date = req.body.date ? req.body.date : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Date"
            });

            if (!req.body.date || !req.body.roleId || !req.body.jobId || !req.body.candidateId || !req.body.destinationLat || !req.body.destinationLong || !req.body.checkinTime || !req.body.destinationName) {
                return;
            }
            //=====================================================================
            let aggrQry = [
                {
                    '$match':
                        {
                            $and: [
                                {'jobId': jobId},
                                {'jobDetails.roleWiseAction.roleId': roleId}
                            ]
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "jobDetails.roleWiseAction.setTime": 1
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "setTime": "$jobDetails.roleWiseAction.setTime",
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "setTime1": {$arrayElemAt: ["$setTime", 0]},
                        }
                },

                {
                    $project:
                        {
                            "jobDetails.roleWiseAction.distance": 1,
                            "setTime": {$arrayElemAt: ["$setTime1", 0]},
                        }
                },

            ]

            let fetchJobInfoStatus = await employerJobBusiness.fetchJobInfo(aggrQry);

            if (fetchJobInfoStatus.isError == true) {
                res.json({
                    isError: true,
                    message: "Error Occur in fetching the job",
                    statuscode: 404,
                    details: null
                });

                return;
            }

            if (fetchJobInfoStatus.jobDetails.length < 1) {
                res.json({
                    isError: false,
                    message: "No Job Found",
                    statuscode: 204,
                    details: null
                });

                return;
            }

            // we apply the restriction if candidate will check in before 15 minute of start time
            let jobStartTime = fetchJobInfoStatus.jobDetails[0].setTime.startTime;
            let splittedJobStartTime = jobStartTime.split(".");
            let jobStartTimeHrsFactor = Number(splittedJobStartTime[0]);
            let jobStartTimeMinsFactor = Number(splittedJobStartTime[1]);
            let jobStartTimeMins = (jobStartTimeHrsFactor * 60) + jobStartTimeMinsFactor;

            let allowedCheckInTimeInMins = jobStartTimeMins - 15;
            let allowedCheckInTimeInMinsFactor = allowedCheckInTimeInMins % 60;
            let allowedCheckInTimeInHrsFactor = Math.floor(allowedCheckInTimeInMins / 60);
            let allowedCheckInTime = allowedCheckInTimeInHrsFactor + (allowedCheckInTimeInMinsFactor / 100);

            let checkInTimeInNumber = Number(checkinTime);

            if (checkInTimeInNumber < allowedCheckInTime) {

                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1033'],
                    statuscode: 1033,
                    details: null
                });

                return;
            }


            let maxDistance = 50;

            let aggrQuery;
            //=====================================================================
            aggrQuery = [
                {
                    $geoNear: {
                        near: {
                            type: "Point",
                            coordinates: [parseFloat(destinationLong), parseFloat(destinationLat)]
                        },
                        distanceField: "dist.locationDistance",
                        // maxDistance: 50,
                        maxDistance: maxDistance,
                        query: {type: "public"},
                        includeLocs: "dist.location",
                        query: {},
                        spherical: true
                    }
                },
                {
                    '$match': {
                        $and: [
                            {'jobId': jobId},
                            {'jobDetails.roleWiseAction.roleId': roleId}
                        ]
                    }
                }
            ]


            models.employer.findOne({"employerId": empId}, function (searchError, searchItem) {
                //console.log(searchItem.Status.fcmTocken)
                console.log(searchItem)
                models.job.aggregate(aggrQuery).exec((err, result) => {
                    console.log(result)
                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    } else {
                        if (result.length == 0) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['208'],
                                statuscode: 208,
                                details: null
                            });
                        } else {
                            let findWhere = {
                                "candidateId": candidateId,
                                "jobId": jobId,
                                "roleId": roleId,
                                "workingDays.date": date
                            }
                            models.CalenderData.findOne(findWhere, {'workingDays.$': 1}, function (err, item) {
                                if (item == null) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['207'],
                                        statuscode: 207,
                                        details: null
                                    })
                                } else {
                                    let type = "ON-SITE-ID";
                                    let autoOnSiteId;
                                    AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                                        autoOnSiteId = data;
                                    })
                                    let onSiteDb;
                                    onSiteDb = item.workingDays[0].onSite;
                                    onSiteDb.push({
                                        "onSideId": autoOnSiteId,
                                        "checkIn": checkinTime,
                                        "destinationName": destinationName,
                                        "geolocation": {
                                            "type": "Point",
                                            "coordinates": [parseFloat(destinationLong), parseFloat(destinationLat)]
                                        }

                                    });
                                    let onSiteData;
                                    if (item.workingDays[0].finalCheckIn == 0) {
                                        onSiteData = {
                                            "workingDays.$.slotType": 'ONSITE',
                                            "workingDays.$.lastOnSideId": autoOnSiteId,
                                            "workingDays.$.absentStatus": 1,
                                            "workingDays.$.onSite": onSiteDb,
                                            "workingDays.$.finalCheckIn": checkinTime,
                                            "isCheckedIn": true,
                                            "lastJobType": "ONSITE"
                                        }
                                    } else {
                                        onSiteData = {
                                            "workingDays.$.slotType": 'ONSITE',
                                            "workingDays.$.lastOnSideId": autoOnSiteId,
                                            "workingDays.$.absentStatus": 1,
                                            "workingDays.$.onSite": onSiteDb,
                                            "isCheckedIn": true,
                                            "lastJobType": "ONSITE"
                                        }
                                    }
                                    models.CalenderData.updateOne(findWhere, {
                                        $set:
                                        onSiteData
                                    }, function (err, affected) {
                                        if (err) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        } else {
                                            if (affected.nModified == 1) {
                                                models.authenticate.findOne({'userId': empId}, async function (err, item) {
                                                    if (item['mobileNo'] == "") {
                                                        var result = await sendCheckinNotificationEmail(item['EmailId'], candidateId, jobId, roleId, checkinTime, date, "", "ONSITE", "in")
                                                        if (result) {
                                                            res.json({
                                                                isError: false,
                                                                message: "You have successfully checked in at " + checkinTime + ". A notification will be sent to your employer.",
                                                                statuscode: 200,
                                                            })
                                                        } else {
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                                statuscode: 404,
                                                                details: null
                                                            });
                                                        }
                                                    } else {
                                                        var result = await sendCheckinNotificationSms(item['mobileNo'], candidateId, jobId, roleId, checkinTime, date, "", "ONSITE", "in")
                                                        if (result) {
                                                            res.json({
                                                                isError: false,
                                                                message: "You have successfully checked in at " + checkinTime + ". A notification will be sent to your employer.",
                                                                statuscode: 200,
                                                            })
                                                        } else {
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                                statuscode: 404,
                                                                details: null
                                                            });
                                                        }
                                                    }

                                                })
                                            } else {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })
                                            }
                                        }
                                    })

                                }
                            })
                        }
                    }
                })
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },
    populateCalenderData: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });

            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }
            let aggrQuery = [
                {$match: {'jobId': jobId}},
                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: false}},
                {
                    "$project": {
                        "_id": 0,
                        "setTime": "$jobDetails.roleWiseAction.setTime",
                        "roleId": "$jobDetails.roleWiseAction.roleId",
                        "jobId": jobId,
                    }
                },
                {'$match': {'roleId': roleId}},
            ]
            let querywhere = {
                "jobId": jobId,
                "roleId": roleId,
                "candidateId": candidateId
            };
            /*
            * First Check any collection is created in database or not based on roleid and jobId
            */
            models.CalenderData.findOne(querywhere, function (searchError, searchItem) {

                if (searchError) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    /*
                    * if searchItem is null means that no collection is created and This is the first candidate
                    * who doing the job
                    */
                    if (searchItem == null) {
                        models.job.aggregate(aggrQuery).exec((error, data) => {
                            /*
                            * Now fetch all dates from the date ranges allocated in job by employer and
                            * create suitable json to populate CalenderData collection
                            */
                            let arrDb = [];
                            data[0].setTime.map(tt => {

                                console.log("1", tt)
                                let startDate = tt['startDate'] / 1000;
                                let endDate = tt['endDate'] / 1000;
                                var itr = Moment.twix(new Date(Moment.unix(startDate).format("YYYY-MM-DD")), new Date(Moment.unix(endDate).format("YYYY-MM-DD"))).iterate("days");
                                console.log("2", tt, itr)
                                while (itr.hasNext()) {
                                    let finalDate = itr.next().format("YYYY-MM-DD");
                                    console.log("3", finalDate)
                                    let timestamp = new Date(finalDate).getTime();
                                    console.log("4", timestamp)
                                    let theDate = new Date(timestamp);
                                    console.log("5", theDate)
                                    let dateInGmtFormat = theDate.toGMTString();
                                    arrDb.push(
                                        {
                                            "shiftTime": [{
                                                "shift": tt['shift'],
                                                "startTime": tt['startTime'],
                                                "endTime": tt['endTime']
                                            }

                                            ],
                                            "date": finalDate,
                                            "dateInTimestampFormat": timestamp,
                                            "dateInGMTFormat": dateInGmtFormat,
                                            "finalCheckIn": 0,
                                            "slotType": "",
                                            "finalCheckOut": 0,
                                            "totalTime": "",
                                            "absentStatus": 3,
                                            "onSite": [],
                                            "offSite": [],
                                            "extraTime": []
                                        }
                                    )
                                }
                            })
                            let combinedShiftDb = {};
                            for (var i = 0; i < arrDb.length; i++) {
                                for (var j = i + 1; j < arrDb.length; j++) {
                                    if (arrDb[i]['date'] == arrDb[j]['date']) {
                                        combinedShiftDb = arrDb[j]['shiftTime'][0];
                                        arrDb[i]['shiftTime'].push(combinedShiftDb)
                                        arrDb.splice(j, 1);
                                    }
                                }
                            }
                            globalVarObj = {
                                "_id": new mongoose.Types.ObjectId(),
                                "jobId": jobId,
                                "roleId": roleId,
                                "candidateId": candidateId,
                                "workingDays": arrDb,
                                "isCheckedIn": false,
                                "lastJobType": "",
                                "extraDays": []
                            }
                            models.CalenderData.create(globalVarObj, function (error, data) {
                                if (error) {
                                    console.log(error)
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    res.json({
                                        isError: false,
                                        message: "This is your First Day of job.Best Of Luck!!!",
                                        statuscode: 200,
                                        data: data,
                                        details: {

                                            "isCheckedIn": data.isCheckedIn,
                                            "lastJobType": data.lastJobType
                                        }
                                    })
                                }
                            })
                        })
                    } else {
                        models.CalenderData.findOne(querywhere, function (searchError, searchItem) {
                            res.json({
                                isError: false,
                                message: 'Calender Data is already populated',
                                statuscode: 200,
                                details: {
                                    "isCheckedIn": searchItem.isCheckedIn,
                                    "lastJobType": searchItem.lastJobType
                                }
                            })
                        })

                    }

                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    createCalenderData: (req, res, next) => {
        try {
            let globalVarObj = {};
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });

            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }
            let aggrQuery = [
                {$match: {'jobId': jobId}},
                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: false}},
                {
                    "$project": {
                        "_id": 0,
                        "setTime": "$jobDetails.roleWiseAction.setTime",
                        "roleId": "$jobDetails.roleWiseAction.roleId",
                        "jobId": jobId,
                    }
                },
                {'$match': {'roleId': roleId}},
            ]
            let querywhere = {
                "jobId": jobId,
                "roleId": roleId,
            };
            /*
            * First Check any collection is created in database or not based on roleid and jobId
            */
            models.CalenderData.findOne(querywhere, function (searchError, searchItem) {
                if (searchError) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    /*
                    * if searchItem is null means that no collection is created and This is the first candidate
                    * who doing the job
                    */
                    if (searchItem == null) {
                        models.job.aggregate(aggrQuery).exec((error, data) => {
                            /*
                            * Now fetch all dates from the date ranges allocated in job by employer and
                            * create suitable json to populate CalenderData collection
                            */
                            let arrDb = [];
                            data[0].setTime.map(tt => {
                                var itr = Moment.twix(new Date(Moment.unix(tt['startDate']).format("YYYY-MM-DD")), new Date(Moment.unix(tt['endDate']).format("YYYY-MM-DD"))).iterate("days");
                                while (itr.hasNext()) {
                                    let finalDate = itr.next().format("YYYY-MM-DD");
                                    let timestamp = new Date(finalDate).getTime();
                                    let theDate = new Date(timestamp);
                                    let dateInGmtFormat = theDate.toGMTString();
                                    arrDb.push(
                                        {
                                            "date": finalDate,
                                            "dateInTimestampFormat": timestamp,
                                            "dateInGMTFormat": dateInGmtFormat,
                                            "finalCheckIn": 0,
                                            "slotType": "",
                                            "finalCheckOut": 0,
                                            "totalTime": 0,
                                            "absentStatus": 3,
                                            "onSite": [],
                                            "offSite": [],
                                            "extraTime": []
                                        }
                                    )
                                }
                            })
                            globalVarObj = {
                                "_id": new mongoose.Types.ObjectId(),
                                "jobId": jobId,
                                "roleId": roleId,
                                "attendanceTracking": [
                                    {
                                        "candidateId": candidateId,
                                        "workingDays": arrDb
                                    }
                                ],
                                "extraDays": []
                            }
                            models.CalenderData.create(globalVarObj, function (error, data) {
                                if (error) {
                                    console.log(error)
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    res.json({
                                        isError: false,
                                        message: "This is your First Day of job.Best Of Luck!!!",
                                        statuscode: 200,
                                        details: data
                                    })
                                }
                            })
                        })
                    } else {
                        /*
                        * If it comes in else that means collection is already created and one of the hired candididates
                        * already started working. Now it is to check that the candidate who call the api is the existing
                        * candidate or the new one.
                        */
                        models.CalenderData.find({'attendanceTracking.candidateId': candidateId}, function (error, searchitem) {
                            if (error) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            } else {
                                /*
                                * If control enters here that means that this is new hired candidate apart from existing one.
                                * so we have to create suitable json along withnew candidateId so we can push new candidate details
                                * with all preselected shift days in 'attendanceTracking' array in existing collection
                                */
                                if (!searchitem.length) {
                                    models.job.aggregate(aggrQuery).exec((error, data) => {
                                        let arrDb = [];
                                        data[0].setTime.map(tt => {
                                            var itr = Moment.twix(new Date(Moment.unix(tt['startDate']).format("YYYY-MM-DD")), new Date(Moment.unix(tt['endDate']).format("YYYY-MM-DD"))).iterate("days");
                                            while (itr.hasNext()) {
                                                let finalDate = itr.next().format("YYYY-MM-DD");
                                                let timestamp = new Date(finalDate).getTime();
                                                let theDate = new Date(timestamp);
                                                let dateInGmtFormat = theDate.toGMTString();
                                                arrDb.push(
                                                    {
                                                        "date": finalDate,
                                                        "dateInTimestampFormat": timestamp,
                                                        "dateInGMTFormat": dateInGmtFormat,
                                                        "finalCheckIn": 0,
                                                        "slotType": "",
                                                        "finalCheckOut": 0,
                                                        "totalTime": 0,
                                                        "absentStatus": 3,
                                                        "onSite": [],
                                                        "offSite": [],
                                                        "extraTime": []
                                                    }
                                                )
                                            }
                                        })
                                        let anotherCandidateTrackingDetails = {
                                            "candidateId": candidateId,
                                            "workingDays": arrDb
                                        }
                                        models.CalenderData.updateOne(querywhere, {$push: {'attendanceTracking': anotherCandidateTrackingDetails}}, function (updatedError, updatedResponse) {
                                            if (updatedError) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })
                                            } else {
                                                if (updatedResponse.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: "This is your First Day of job.Best Of Luck!",
                                                        statuscode: 200,
                                                    })
                                                } else {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                        })
                                    })
                                } else {
                                    /*
                                    * If controls comes here it means the candidate who is responsible
                                    * for calling the api is already started jobs. therefore no action is needed
                                    * apart fron sending a success message
                                    */
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        //details:searchitem
                                    })
                                }
                            }

                        })
                    }
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getAppointments: (req, res, next) => {
        try {
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id",
                isError: true,
            });

            if (!req.body.candidateId) {
                return;
            }
            let aggrQuery = [
                {$match: {'candidateId': candidateId}},
                {$unwind: {path: "$hiredJobs", preserveNullAndEmptyArrays: false}},


                {
                    "$project": {
                        "_id": 0,
                        "roleId": "$hiredJobs.roleId",
                        "employerId": "$hiredJobs.employerId",
                        "jobId": "$hiredJobs.jobId",
                        "candidateId": "$hiredJobs.candidateId",
                        "appointmentCurrentTimestamp": "$hiredJobs.currentTimestamp",
                        "appointmentCurrentDate": "$hiredJobs.currentDate"
                    }
                },

                {
                    $lookup: {
                        from: "jobroles",
                        localField: "roleId",
                        foreignField: "jobRoleId",
                        as: "appliedJobs"
                    }
                },
                {
                    "$project": {
                        jobId: 1,
                        employerId: 1,
                        candidateId: 1,
                        appointmentCurrentTimestamp: 1,
                        appointmentCurrentDate: 1,
                        appliedJobs: {$arrayElemAt: ["$appliedJobs", 0]}
                    }
                },

                {
                    "$project": {
                        "jobRoleName": "$appliedJobs.jobRoleName",
                        "industryId": "$appliedJobs.industryId",
                        "jobRoleId": "$appliedJobs.jobRoleId",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "candidateId": "$candidateId",
                        "appointmentCurrentTimestamp": "$appointmentCurrentTimestamp",
                        "appointmentCurrentDate": "$appointmentCurrentDate",
                    }
                },

                {
                    $lookup: {
                        from: "industries",
                        localField: "industryId",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    "$project": {
                        jobRoleName: 1,
                        industryId: 1,
                        jobRoleId: 1,
                        jobId: 1,
                        employerId: 1,
                        candidateId: 1,
                        appointmentCurrentTimestamp: 1,
                        appointmentCurrentDate: 1,
                        industryDetails: {$arrayElemAt: ["$industryDetails", 0]}

                    }
                },
                {
                    "$project": {
                        "jobRoleName": "$jobRoleName",
                        "industryId": "$industryId",
                        "jobRoleId": "$jobRoleId",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "candidateId": "$candidateId",
                        "appointmentCurrentTimestamp": "$appointmentCurrentTimestamp",
                        "appointmentCurrentDate": "$appointmentCurrentDate",
                        "industryName": "$industryDetails.industryName"
                    }
                },
                {
                    $lookup: {
                        from: "jobs",
                        localField: "jobId",
                        foreignField: "jobId",
                        as: "jobDetails"
                    }
                },
                {
                    "$project": {
                        _id: 0,
                        jobRoleName: 1,
                        industryId: 1,
                        jobRoleId: 1,
                        jobId: 1,
                        employerId: 1,
                        candidateId: 1,
                        industryName: 1,
                        appointmentCurrentTimestamp: 1,
                        appointmentCurrentDate: 1,
                        jobDetails: {$arrayElemAt: ["$jobDetails", 0]}

                    }
                },
                {
                    "$project": {
                        _id: 0,
                        jobRoleName: 1,
                        industryId: 1,
                        jobRoleId: 1,
                        jobId: 1,
                        employerId: 1,
                        candidateId: 1,
                        industryName: 1,
                        appointmentCurrentTimestamp: 1,
                        appointmentCurrentDate: 1,
                        roleWiseAction: {
                            $filter: {
                                input: "$jobDetails.jobDetails.roleWiseAction",
                                as: "item",
                                cond: {$eq: ["$$item.roleId", "$jobRoleId"]}
                            }
                        }

                    }
                },
                {
                    "$project": {
                        "jobRoleName": "$jobRoleName",
                        "industryId": "$industryId",
                        "jobRoleId": "$jobRoleId",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "candidateId": "$candidateId",
                        "industryName": "$industryName",
                        "appointmentCurrentTimestamp": "$appointmentCurrentTimestamp",
                        "appointmentCurrentDate": "$appointmentCurrentDate",
                        jobDetails: {$arrayElemAt: ["$roleWiseAction", 0]}
                    }
                },
                {
                    "$project": {
                        "jobRoleName": "$jobRoleName",
                        "industryId": "$industryId",
                        "jobRoleId": "$jobRoleId",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "candidateId": "$candidateId",
                        "industryName": "$industryName",
                        "appointmentCurrentTimestamp": "$appointmentCurrentTimestamp",
                        "appointmentCurrentDate": "$appointmentCurrentDate",
                        "description": "$jobDetails.description",
                        "locationName": "$jobDetails.locationName",
                        "location": "$jobDetails.location",
                        "setTime": "$jobDetails.setTime",
                        "setTimeExist": {
                            $ifNull: ['$jobRoleName', false]
                        }

                    }
                },

                {
                    "$match": {
                        "setTimeExist": {$ne: false}
                    }
                },

                {
                    "$project": {
                        "jobRoleName": 1,
                        "industryId": 1,
                        "jobRoleId": 1,
                        "jobId": 1,
                        "employerId": 1,
                        "candidateId": 1,
                        "industryName": 1,
                        "description": 1,
                        "locationName": 1,
                        "location": 1,
                        "setTime": 1,
                        "appointmentCurrentTimestamp": 1,
                        "appointmentCurrentDate": 1,
                        "setTimeFirst": {$arrayElemAt: ["$setTime", 0]},
                        "setTimeFirstTimeStamp": "$setTimeFirst.startDate",
                        "setTimeExist": {
                            $ifNull: ['$jobRoleName', false]
                        }

                    }
                },

                {
                    "$project": {
                        "jobRoleName": 1,
                        "industryId": 1,
                        "jobRoleId": 1,
                        "jobId": 1,
                        "employerId": 1,
                        "candidateId": 1,
                        "industryName": 1,
                        "description": 1,
                        "locationName": 1,
                        "location": 1,
                        "setTime": 1,
                        "setTimeFirst": 1,
                        "appointmentCurrentTimestamp": 1,
                        "appointmentCurrentDate": 1,
                        "setTimeFirstTimeStamp": "$setTimeFirst.startDate",
                        "setTimeExist": {
                            $ifNull: ['$jobRoleName', false]
                        }

                    }
                },

                {$sort: {appointmentCurrentTimestamp: -1}},


                {
                    $group: {
                        _id: null,
                        total: {
                            $sum: 1
                        },
                        results: {
                            $push: '$$ROOT'
                        }
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'total': 1,
                        'noofpage': {$ceil: {$divide: ["$total", perPage]}},
                        'results': {
                            $slice: [
                                '$results', page * perPage, perPage
                            ]
                        }
                    }
                }


            ]

            models.candidate.aggregate(aggrQuery,
                function (err, item) {

                    if (err) {
                        res.json({
                            isError: true,
                            message: "Error",
                            statuscode: 404,
                            details: null
                        });
                    } else {
                        res.json({
                            isError: false,
                            message: "Success",
                            statuscode: 200,
                            details: item
                        });

                    }

                })

        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },
    /**
     * @abstract
     * DECLINE JOBS
     * first confrim from where jobs details will be removed (joboffer array or jobaccepted array)
     * Based on that details  jobOffer or job accepted details will be moved to job-declined array of candidate collection.
     * paralelly appiliedFrom or acceptList details of job collection will be moved to rejectList array .
     */
    jobDecline: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let declinefrom1 = req.body.declinefrom ? req.body.declinefrom : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Declined From"
            });

            let reasonOfDecline = req.body.reasonOfDecline ? req.body.reasonOfDecline : 'No reason given';

            if (!req.body.declinefrom || !req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) {
                return;
            }


            // get the current time stamp
            let currentDate = new Date();
            let currentTimestamp = currentDate.getTime();
            /*
            let dateOffset = (24*60*60*1000) * 90; //5 days
            let threeMonthAgoTimeStamp = currentTimestamp - dateOffset;

            let dateThreeMonthBefore = new Date(threeMonthAgoTimeStamp);

            let attrForCandidate = {
                candidateId: candidateId,
                threeMonthAgoTimeStamp: threeMonthAgoTimeStamp
            };

            let fetchDetailsOfDeclinedJobOfCandidateStatus = await employerJobBusiness.fetchDetailsOfDeclinedJobOfCandidate(attrForCandidate);

            if (fetchDetailsOfDeclinedJobOfCandidateStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let responseLength = fetchDetailsOfDeclinedJobOfCandidateStatus.responseLength;
            let candidateRating = 9999; // when 9999 we dont have to change the rating

            // If Candidate declined three item then his rating has to be reduced to 3
            if (responseLength > 3) {
                candidateRating = fetchDetailsOfDeclinedJobOfCandidateStatus.response[0].rating ? fetchDetailsOfDeclinedJobOfCandidateStatus.response[0].rating : 9999;
                if ((candidateRating > 3) && (candidateRating != 9999)) {
                    candidateRating = 3;
                }
            } // END if (responseLength > 0) {
             */

            // res.json({
            //     currentDate: currentDate,
            //     currentTimestamp: currentTimestamp,
            //     threeMonthAgoTimeStamp: threeMonthAgoTimeStamp,
            //     dateThreeMonthBefore:dateThreeMonthBefore,
            //     responseLength: responseLength,
            //     candidateRating: candidateRating,
            //     fetchDetailsOfDeclinedJobOfCandidateStatus: fetchDetailsOfDeclinedJobOfCandidateStatus
            // });
            // return;

            let findWhere = {}
            let declineFrom;
            let deleteFrom;
            switch (declinefrom1) {
                case '1':
                    declineFrom = 'jobOffers';
                    deleteFrom = 'appiliedFrom';
                    findWhere = {
                        'jobOffers.jobId': jobId,
                        'jobOffers.roleId': roleId,
                        'candidateId': candidateId,
                    }
                    break;
                case '2':
                    declineFrom = 'acceptedJobs';
                    deleteFrom = 'acceptList';
                    findWhere = {
                        'acceptedJobs.jobId': jobId,
                        'acceptedJobs.roleId': roleId,
                        'candidateId': candidateId,
                    }
                    break;
            }


            let findWith = declineFrom + ".$";
            models.candidate.findOne(findWhere, {[findWith]: 1}, function (err, item) {

                if (declinefrom1 == "1") {
                    let roleId = item.jobOffers[0].roleId;
                    let employerId = item.jobOffers[0].employerId;
                    let jobId = item.jobOffers[0].jobId;
                    let candidateId = item.jobOffers[0].candidateId;
                    let declineFrom = 'jobOffers';
                } else {
                    let roleId = item.acceptedJobs[0].roleId;
                    let employerId = item.acceptedJobs[0].employerId;
                    let jobId = item.acceptedJobs[0].jobId;
                    let candidateId = item.acceptedJobs[0].candidateId;
                    let declineFrom = 'acceptedJobs';
                }

                let insertedValue = {
                    "roleId": roleId,
                    "employerId": employerId,
                    "jobId": jobId,
                    "candidateId": candidateId,
                    "reasonOfDecline": reasonOfDecline,
                    "declinedTimeStamp": currentTimestamp
                };

                models.candidate.updateOne({"candidateId": candidateId},
                    {
                        "$pull": {
                            [declineFrom]:
                                {"jobId": jobId, "roleId": roleId}
                        }
                    },
                    {safe: true, multi: true},

                    function (deletedderror, deletedresponse) {
                        if (deletedderror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        if (deletedresponse.nModified == 1) {

                            let updateCandidateField = {
                                $push: {"declinedJobs": insertedValue},
                                // "lastCancellationTime": currentTimestamp
                            };

                            // if (candidateRating != 9999) {
                            //     updateCandidateField = {
                            //         $push: { "declinedJobs":insertedValue },
                            //         // "lastCancellationTime": currentTimestamp,
                            //         // "rating": candidateRating
                            //     };
                            // }

                            models.candidate.updateOne(
                                {'candidateId': candidateId},
                                updateCandidateField,
                                function (updatederror, updatedresponse) {
                                    if (updatederror) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                    if (updatedresponse.nModified == 1) {
                                        models.job.updateOne({"jobId": jobId},
                                            {"$pull": {[deleteFrom]: {"roleId": roleId, "candidateId": candidateId}}},
                                            {safe: true, multi: true},
                                            function (pullederror, pulledresponse) {
                                                if (pullederror) {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                } else {
                                                    if (pulledresponse.nModified == 1) {
                                                        let insertedpushValue = {
                                                            "roleId": roleId,
                                                            "candidateId": candidateId
                                                        }
                                                        models.job.updateOne({"jobId": jobId},
                                                            {$push: {"rejectList": insertedpushValue}},
                                                            function (pushederror, pushedresponse) {
                                                                if (pushederror) {
                                                                    res.json({
                                                                        isError: true,
                                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                                        statuscode: 404,
                                                                        details: null
                                                                    });
                                                                } else {
                                                                    if (pushedresponse.nModified == 1) {
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                                            statuscode: 200,
                                                                            details: null
                                                                        })
                                                                    } else {
                                                                        res.json({
                                                                            isError: true,
                                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                                            statuscode: 1004,
                                                                            details: null
                                                                        })
                                                                    }
                                                                }
                                                            })
                                                    } else {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        })
                                                    }
                                                }
                                            })
                                    } else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                })
                        } else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                    })
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract
     * ACCEPT JOBS
     * during job accept, details of job offers array must be moved to accepted-job array of candidate collection,
     * At the same time removed details of appiliedFrom to acceptList array of job collection
     * at last send sms or mail to employer
     */

    jobAccept: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) {
                return;
            }

            //========================================================================
            // Here, we check whether the vacancy still remains or not
            let checkJobAvaibility = await jobProfileBusiness.checkJobAvaibility({
                "roleId": roleId,
                "jobId": jobId
            })

            let isJobAvailabel = checkJobAvaibility.isJobAvailabel;
            let endRealDateTimeStamp = checkJobAvaibility.roleDetails.endRealDateTimeStamp;
            let endRealDate = checkJobAvaibility.roleDetails.endRealDate;

            if (isJobAvailabel != true) {
                res.json({
                    isError: true,
                    message: "Requirement for this job has been fulfilled",
                    statuscode: 204,
                    details: null
                })

                return;
            }
            //========================================================================

            let currentDate = new Date();
            let currentTimestamp = currentDate.getTime();

            let candidateInsertedValue = {
                "endRealDate": endRealDate,
                "endRealDateTimeStamp": endRealDateTimeStamp,
                "currentTimestamp": currentTimestamp,
                "currentDate": currentDate,
                "roleId": roleId,
                "employerId": employerId,
                "jobId": jobId,
                "candidateId": candidateId
            }

            let jobInsertedValue = {
                "roleId": roleId,
                "candidateId": candidateId
            }

            let pushoperationCandidate;
            let pushoperationJob;

            pushoperationCandidate = {$push: {"hiredJobs": candidateInsertedValue}};
            pushoperationJob = {$push: {"hireList": jobInsertedValue}};

            let findWhere = {
                'jobOffers.jobId': jobId,
                'jobOffers.roleId': roleId,
                'candidateId': candidateId,
            }
            models.candidate.findOne(findWhere,
                {
                    'jobOffers.$': 1,
                    'Status': 1
                },
                function (err, item) {


                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                        return;
                    }

                    if (item == null) { // No Item Found
                        res.json({
                            isError: true,
                            message: "No Candiate Found",
                            statuscode: 204,
                            details: null
                        });
                        return;
                    }

                    if (item.Status.isApprovedByAot == false) {
                        res.json({
                            isError: true,
                            message: "Candidate is not approved by admin yet",
                            statuscode: 204,
                            details: null
                        });
                        return;
                    }


                    models.candidate.updateOne({"candidateId": candidateId},
                        {"$pull": {"jobOffers": {"jobId": jobId, "roleId": roleId}}},
                        {safe: true, multi: true},
                        function (deletedderror, deletedresponse) {


                            if (deletedderror) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            if (deletedresponse.nModified == 1) {


                                models.candidate.updateOne(
                                    {'candidateId': candidateId},
                                    pushoperationCandidate,
                                    function (updatederror, updatedresponse) {

                                        if (updatederror) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        if (updatedresponse.nModified == 1) {

                                            models.job.updateOne({"jobId": jobId},
                                                {
                                                    "$pull": {
                                                        "appiliedFrom": {
                                                            "roleId": roleId,
                                                            "candidateId": candidateId
                                                        }
                                                    }
                                                },
                                                {safe: true, multi: true},
                                                function (pullederror, pulledresponse) {

                                                    if (pullederror) {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    } else {
                                                        if (pulledresponse.nModified == 1) {

                                                            models.job.updateOne({"jobId": jobId},
                                                                pushoperationJob,
                                                                function (pushederror, pushedresponse) {


                                                                    if (pushederror) {
                                                                        res.json({
                                                                            isError: true,
                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                            statuscode: 404,
                                                                            details: null
                                                                        });
                                                                    } else {
                                                                        if (pushedresponse.nModified == 1) {

                                                                            models.authenticate.findOne({'userId': employerId}, async function (err, item) {

                                                                                if (err) {
                                                                                    res.json({
                                                                                        isError: false,
                                                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                                                        statuscode: 200,
                                                                                        details: null
                                                                                    })

                                                                                    return;
                                                                                }


                                                                                if (item['mobileNo'] == "") {
                                                                                    var result = await sendNotificationEmailForAcceptJob(item['EmailId'], candidateId, jobId, roleId, employerId)
                                                                                    if (result) {
                                                                                        models.candidate.find({"candidateId": candidateId}, function (error, data) {

                                                                                            models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                                                if (pushNotificationStatus == true) {
                                                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job offer")
                                                                                                }

                                                                                                res.json({
                                                                                                    isError: false,
                                                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                                                    statuscode: 200,
                                                                                                    details: null
                                                                                                })

                                                                                            })
                                                                                        })
                                                                                    } else {
                                                                                        res.json({
                                                                                            isError: true,
                                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                                            statuscode: 404,
                                                                                            details: null
                                                                                        });
                                                                                    }
                                                                                } else {
                                                                                    var result = await sendNotificationSms(item['mobileNo'], candidateId, jobId, roleId, employerId)
                                                                                    if (result) {
                                                                                        models.candidate.find({"candidateId": candidateId}, function (error, data) {
                                                                                            models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                                                if (pushNotificationStatus == true) {
                                                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job offer")
                                                                                                }


                                                                                                res.json({
                                                                                                    isError: false,
                                                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                                                    statuscode: 200,
                                                                                                    details: null
                                                                                                })

                                                                                            })
                                                                                        })
                                                                                    } else {
                                                                                        res.json({
                                                                                            isError: true,
                                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                                            statuscode: 404,
                                                                                            details: null
                                                                                        });
                                                                                    }
                                                                                }


                                                                            })
                                                                        } else {
                                                                            res.json({
                                                                                isError: true,
                                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                                statuscode: 1004,
                                                                                details: null
                                                                            })
                                                                        }
                                                                    }
                                                                })
                                                        } else {
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                statuscode: 1004,
                                                                details: null
                                                            })
                                                        }
                                                    }
                                                })
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    })
                            } else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        })
                })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    jobAcceptBKP: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) {
                return;
            }


            //=======================================================================
            // this function is used to fetch the hired details
            async function fetchHiredCandidate() {

                return new Promise(function (resolve, reject) {
                    let fetchQry = [
                        {
                            '$match': {'jobId': jobId}
                        },

                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "hireList": 1,

                            }
                        },


                    ]


                    models.job.aggregate(fetchQry, function (err, response) {

                        if (err) {
                            resolve({
                                "isError": true
                            });
                        } else {

                            resolve({
                                "isError": false,
                                "response": response,

                            });
                        }

                    }) // models.job.aggregate(candidateQry, function (err, responseCandidate) {

                }) // END return new Promise(function(resolve, reject){
            } // start of checkUser

            //================
            let fetchHiredCandidateStatus = await fetchHiredCandidate();

            if (fetchHiredCandidateStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null,

                });
                return;

            }

            let hiredListLength = fetchHiredCandidateStatus.response[0].hireList.length;


            let candidateInsertedValue = {
                "roleId": roleId,
                "employerId": employerId,
                "jobId": jobId,
                "candidateId": candidateId
            }

            let jobInsertedValue = {
                "roleId": roleId,
                "candidateId": candidateId
            }

            let pushoperationCandidate;
            let pushoperationJob;


            if (hiredListLength == 0) { // when accepting as first candidate
                pushoperationCandidate = {$push: {"hiredJobs": candidateInsertedValue}};
                pushoperationJob = {$push: {"hireList": jobInsertedValue}};

            } else {
                pushoperationCandidate = {$push: {"acceptedJobs": candidateInsertedValue}};
                pushoperationJob = {$push: {"acceptList": jobInsertedValue}};
            }


            let findWhere = {
                'jobOffers.jobId': jobId,
                'jobOffers.roleId': roleId,
                'candidateId': candidateId,
            }
            models.candidate.findOne(findWhere,
                {
                    'jobOffers.$': 1,
                    'Status': 1
                },
                function (err, item) {


                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                        return;
                    }

                    if (item == null) { // No Item Found
                        res.json({
                            isError: true,
                            message: "No Candiate Found",
                            statuscode: 204,
                            details: null
                        });
                        return;
                    }

                    if (item.Status.isApprovedByAot == false) {
                        res.json({
                            isError: true,
                            message: "Candidate is not approved by admin yet",
                            statuscode: 204,
                            details: null
                        });
                        return;
                    }

                    // let  roleId=item.jobOffers[0].roleId;
                    // let  employerId=item.jobOffers[0].employerId;
                    // let  jobId=item.jobOffers[0].jobId;
                    // let  candidateId=item.jobOffers[0].candidateId;

                    // res.json({
                    //     hiredListLength: hiredListLength,
                    //     jobInsertedValue:jobInsertedValue,
                    //     pushoperationCandidate:pushoperationCandidate,
                    //     pushoperationJob:pushoperationJob,
                    //     item:item,
                    //     roleId:roleId,
                    //     employerId:employerId,
                    //     jobId:jobId,
                    //     candidateId:candidateId

                    // })
                    // return;

                    models.candidate.updateOne({"candidateId": candidateId},
                        {"$pull": {"jobOffers": {"jobId": jobId, "roleId": roleId}}},
                        {safe: true, multi: true},
                        function (deletedderror, deletedresponse) {


                            if (deletedderror) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            if (deletedresponse.nModified == 1) {


                                models.candidate.updateOne(
                                    {'candidateId': candidateId},
                                    pushoperationCandidate,
                                    function (updatederror, updatedresponse) {

                                        // models.candidate.updateOne({ 'candidateId': candidateId }, { $push: { "acceptedJobs":insertedValue } }, function (updatederror, updatedresponse) {
                                        if (updatederror) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        if (updatedresponse.nModified == 1) {

                                            models.job.updateOne({"jobId": jobId},
                                                {
                                                    "$pull": {
                                                        "appiliedFrom": {
                                                            "roleId": roleId,
                                                            "candidateId": candidateId
                                                        }
                                                    }
                                                },
                                                {safe: true, multi: true},
                                                function (pullederror, pulledresponse) {

                                                    if (pullederror) {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    } else {
                                                        if (pulledresponse.nModified == 1) {
                                                            // let insertedpushValue1={
                                                            //     "roleId": roleId,
                                                            //     "candidateId":candidateId
                                                            // }
                                                            // models.job.updateOne({"jobId":jobId}, { $push: { "acceptList":insertedpushValue1 } }, function (pushederror, pushedresponse) {

                                                            models.job.updateOne({"jobId": jobId},
                                                                pushoperationJob,
                                                                function (pushederror, pushedresponse) {


                                                                    if (pushederror) {
                                                                        res.json({
                                                                            isError: true,
                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                            statuscode: 404,
                                                                            details: null
                                                                        });
                                                                    } else {
                                                                        if (pushedresponse.nModified == 1) {

                                                                            models.authenticate.findOne({'userId': employerId}, async function (err, item) {

                                                                                // res.json({
                                                                                //     isError: false,
                                                                                //     message: errorMsgJSON['ResponseMsg']['200'],
                                                                                //     statuscode: 200,
                                                                                //     details: null
                                                                                // })
                                                                                if (err) {
                                                                                    res.json({
                                                                                        isError: false,
                                                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                                                        statuscode: 200,
                                                                                        details: null
                                                                                    })

                                                                                    return;
                                                                                }


                                                                                if (item['mobileNo'] == "") {
                                                                                    var result = await sendNotificationEmailForAcceptJob(item['EmailId'], candidateId, jobId, roleId, employerId)
                                                                                    if (result) {
                                                                                        models.candidate.find({"candidateId": candidateId}, function (error, data) {

                                                                                            models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                                                if (pushNotificationStatus == true) {
                                                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job offer")
                                                                                                }

                                                                                                // .then(function(nofify){
                                                                                                res.json({
                                                                                                    isError: false,
                                                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                                                    statuscode: 200,
                                                                                                    details: null
                                                                                                })
                                                                                                // })
                                                                                            })
                                                                                        })
                                                                                    } else {
                                                                                        res.json({
                                                                                            isError: true,
                                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                                            statuscode: 404,
                                                                                            details: null
                                                                                        });
                                                                                    }
                                                                                } else {
                                                                                    var result = await sendNotificationSms(item['mobileNo'], candidateId, jobId, roleId, employerId)
                                                                                    if (result) {
                                                                                        models.candidate.find({"candidateId": candidateId}, function (error, data) {
                                                                                            models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                                                if (pushNotificationStatus == true) {
                                                                                                    fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job offer")
                                                                                                }

                                                                                                // .then(function(nofify){
                                                                                                res.json({
                                                                                                    isError: false,
                                                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                                                    statuscode: 200,
                                                                                                    details: null
                                                                                                })
                                                                                                // })
                                                                                            })
                                                                                        })
                                                                                    } else {
                                                                                        res.json({
                                                                                            isError: true,
                                                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                                                            statuscode: 404,
                                                                                            details: null
                                                                                        });
                                                                                    }
                                                                                }


                                                                            })
                                                                        } else {
                                                                            res.json({
                                                                                isError: true,
                                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                                statuscode: 1004,
                                                                                details: null
                                                                            })
                                                                        }
                                                                    }
                                                                })
                                                        } else {
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                statuscode: 1004,
                                                                details: null
                                                            })
                                                        }
                                                    }
                                                })
                                        } else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    })
                            } else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        })
                })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    candidateJobDetails1: (req, res, next) => {
        try {
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }
            let aggrQuery = [
                {
                    '$match': {
                        $and:
                            [
                                {'jobDetails.roleWiseAction.roleId': roleId},
                                {'jobId': jobId},
                            ]
                    }
                },
                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: true}},
                {'$match': {'jobDetails.roleWiseAction.roleId': roleId}},
                {
                    $facet: {
                        "generalInfo": [
                            {
                                "$project": {
                                    "jobDetails": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "approvedTo": 1,
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "roleId": "$jobDetails.roleWiseAction.roleId",
                                    "jobId": "$jobId",
                                    "employerId": "$employerId",
                                    "approvedTo": 1,
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "roleId": "$roleId",
                                    "jobId": "$jobId",
                                    "employerId": "$employerId",
                                    "isOfferedArray": {
                                        $filter: {
                                            input: "$approvedTo",
                                            as: "approvedTo",
                                            cond: {$and: [{$eq: ["$$approvedTo.roleId", "$roleId"]}, {$eq: ["$$approvedTo.candidateId", candidateId]}]}
                                        }
                                    },
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,

                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                                }
                            },
                            {
                                "$project": {

                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": candidateId,
                                    "isOffered": {
                                        $cond: {
                                            "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                            "then": true,
                                            "else": false
                                        }
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "candidates",
                                    localField: "candidateId",
                                    foreignField: "candidateId",
                                    as: "candidateDetails"
                                }
                            },
                            {
                                "$project": {
                                    roleId: 1,
                                    jobId: 1,
                                    employerId: 1,
                                    candidateId: 1,
                                    isOffered: 1,
                                    candidateDetails: {$arrayElemAt: ["$candidateDetails", 0]}
                                }
                            },
                            {
                                "$project": {
                                    "roleId": "$roleId",
                                    "jobId": "$jobId",
                                    "employerId": "$employerId",
                                    "candidateId": "$candidateId",
                                    "isOffered": "$isOffered",
                                    "declinedJobs": "$candidateDetails.declinedJobs",
                                    "acceptedJobs": "$candidateDetails.acceptedJobs",
                                    "appliedJobs": "$candidateDetails.appliedJobs",
                                    "hiredJobs": "$candidateDetails.hiredJobs",
                                }
                            },
                            {
                                "$project": {
                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "isOffered": 1,
                                    "appliedJobsArray": {
                                        $filter: {
                                            input: "$appliedJobs",
                                            as: "appliedJobs",
                                            cond: {$and: [{$eq: ["$$appliedJobs.roleId", "$roleId"]}, {$eq: ["$$appliedJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                    "declinedJobsArray": {
                                        $filter: {
                                            input: "$declinedJobs",
                                            as: "declinedJobs",
                                            cond: {$and: [{$eq: ["$$declinedJobs.roleId", "$roleId"]}, {$eq: ["$$declinedJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                    "acceptedJobsArray": {
                                        $filter: {
                                            input: "$acceptedJobs",
                                            as: "acceptedJobs",
                                            cond: {$and: [{$eq: ["$$acceptedJobs.roleId", "$roleId"]}, {$eq: ["$$acceptedJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                    "hiredJobsArray": {
                                        $filter: {
                                            input: "$hiredJobs",
                                            as: "hiredJobs",
                                            cond: {$and: [{$eq: ["$$hiredJobs.roleId", "$roleId"]}, {$eq: ["$$hiredJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                }
                            },
                            {
                                "$project": {
                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "isOffered": 1,
                                    "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                                    "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                                    "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                                    "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                                }
                            },
                            {
                                "$project": {
                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "isOffered": 1,
                                    "isHired": {
                                        $cond: {
                                            "if": {"$eq": ["$hiredJobsArray.roleId", "$roleId"]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                    "isApplied": {
                                        $cond: {
                                            "if": {"$eq": ["$appliedJobsArray.roleId", "$roleId"]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                    "isDeclined": {
                                        $cond: {
                                            "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                    "isAccepted": {
                                        $cond: {
                                            "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                }
                            }
                        ],
                        "jobInfo": [
                            {
                                "$project": {
                                    "jobDetails": 1,
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "skills": "$jobDetails.roleWiseAction.skills",
                                    "roleName": "$jobDetails.roleWiseAction.roleName",
                                    "jobRoleIcon": "$jobDetails.roleWiseAction.jobRoleIcon",
                                    "jobType": "$jobDetails.roleWiseAction.jobType",
                                    "payType": "$jobDetails.roleWiseAction.payType",
                                    "pay": "$jobDetails.roleWiseAction.pay",
                                    "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                                    "industry": "$jobDetails.industry",
                                    "setTime": "$jobDetails.roleWiseAction.setTime",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industry",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project": {
                                    skills: 1,
                                    roleName: 1,
                                    jobRoleIcon: 1,
                                    jobType: 1,
                                    payType: 1,
                                    pay: 1,
                                    noOfStuff: 1,
                                    industry: 1,
                                    setTime: 1,
                                    industryDetails: {$arrayElemAt: ["$industryDetails", 0]}
                                }
                            },
                            {
                                "$project": {
                                    "setTime": "$setTime",
                                    "skills": "$skills",
                                    "roleName": "$roleName",
                                    "jobRoleIcon": "$jobRoleIcon",
                                    "jobType": "$jobType",
                                    "payType": "$payType",
                                    "pay": "$pay",
                                    "noOfStuff": "$noOfStuff",
                                    "industry": "$industry",
                                    "industryName": "$industryDetails.industryName"
                                }
                            },
                            {$unwind: {path: "$skills", preserveNullAndEmptyArrays: true}},
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            {
                                $project: {
                                    "setTime": 1,
                                    "roleName": 1,
                                    "jobRoleIcon": 1,
                                    "jobType": 1,
                                    "payType": 1,
                                    "pay": 1,
                                    "noOfStuff": 1,
                                    "industry": 1,
                                    "industryName": 1,
                                    "skillDetails": {
                                        $arrayElemAt: ["$skillDetails", 0]
                                    }
                                }
                            },
                            {
                                $group: {
                                    "_id": {
                                        "industryId": "$industry"
                                    },
                                    "setTime": {
                                        $first: "$setTime"
                                    },
                                    "roleName": {
                                        $first: "$roleName"
                                    },
                                    "jobRoleIcon": {
                                        $first: "$jobRoleIcon"
                                    },
                                    "jobType": {
                                        $first: "$jobType"
                                    },
                                    "payType": {
                                        $first: "$payType"
                                    },
                                    "pay": {
                                        $first: "$pay"
                                    },
                                    "noOfStuff": {
                                        $first: "$noOfStuff"
                                    },
                                    "industry": {
                                        $first: "$industry"
                                    },
                                    "industryName": {
                                        $first: "$industryName"
                                    },
                                    "skillDetails": {
                                        $push: "$skillDetails"
                                    }
                                }
                            }
                        ],
                        "locationInfo": [
                            {
                                "$project": {
                                    "jobDetails": 1,
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "location": "$jobDetails.roleWiseAction.location",
                                    "description": "$jobDetails.roleWiseAction.description",
                                    "locationName": "$jobDetails.roleWiseAction.locationName",
                                }
                            },
                        ]
                    }
                },
                {
                    $project: {
                        locationInfo: {$arrayElemAt: ["$locationInfo", 0]},
                        jobInfo: {$arrayElemAt: ["$jobInfo", 0]},
                        generalInfo: {$arrayElemAt: ["$generalInfo", 0]},
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((err, result) => {
                console.log(err)
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* candidate details based on old structure*/
    candidateJobDetails: (req, res, next) => {
        try {
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            if (!req.body.roleId || !req.body.jobId || !req.body.candidateId) {
                return;
            }
            let aggrQuery = [
                {
                    '$match': {
                        $and:
                            [
                                {'jobDetails.roleWiseAction.roleId': roleId},
                                {'jobId': jobId},
                            ]
                    }
                },
                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: true}},
                {'$match': {'jobDetails.roleWiseAction.roleId': roleId}},
                {
                    $facet: {
                        "generalInfo": [
                            {
                                "$project": {
                                    "jobDetails": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "approvedTo": 1,
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "roleId": "$jobDetails.roleWiseAction.roleId",
                                    "jobId": "$jobId",
                                    "employerId": "$employerId",
                                    "approvedTo": 1,
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "roleId": "$roleId",
                                    "jobId": "$jobId",
                                    "employerId": "$employerId",
                                    "isOfferedArray": {
                                        $filter: {
                                            input: "$approvedTo",
                                            as: "approvedTo",
                                            cond: {$and: [{$eq: ["$$approvedTo.roleId", "$roleId"]}, {$eq: ["$$approvedTo.candidateId", candidateId]}]}
                                        }
                                    },
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,

                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                                }
                            },
                            {
                                "$project": {

                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": candidateId,
                                    "isOffered": {
                                        $cond: {
                                            "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                            "then": true,
                                            "else": false
                                        }
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "candidates",
                                    localField: "candidateId",
                                    foreignField: "candidateId",
                                    as: "candidateDetails"
                                }
                            },
                            {
                                "$project": {
                                    roleId: 1,
                                    jobId: 1,
                                    employerId: 1,
                                    candidateId: 1,
                                    isOffered: 1,
                                    candidateDetails: {$arrayElemAt: ["$candidateDetails", 0]}
                                }
                            },
                            {
                                "$project": {
                                    "roleId": "$roleId",
                                    "jobId": "$jobId",
                                    "employerId": "$employerId",
                                    "candidateId": "$candidateId",
                                    "isOffered": "$isOffered",
                                    "declinedJobs": "$candidateDetails.declinedJobs",
                                    "acceptedJobs": "$candidateDetails.acceptedJobs",
                                    "appliedJobs": "$candidateDetails.appliedJobs",
                                    "hiredJobs": "$candidateDetails.hiredJobs",
                                }
                            },
                            {
                                "$project": {
                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "isOffered": 1,
                                    "appliedJobsArray": {
                                        $filter: {
                                            input: "$appliedJobs",
                                            as: "appliedJobs",
                                            cond: {$and: [{$eq: ["$$appliedJobs.roleId", "$roleId"]}, {$eq: ["$$appliedJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                    "declinedJobsArray": {
                                        $filter: {
                                            input: "$declinedJobs",
                                            as: "declinedJobs",
                                            cond: {$and: [{$eq: ["$$declinedJobs.roleId", "$roleId"]}, {$eq: ["$$declinedJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                    "acceptedJobsArray": {
                                        $filter: {
                                            input: "$acceptedJobs",
                                            as: "acceptedJobs",
                                            cond: {$and: [{$eq: ["$$acceptedJobs.roleId", "$roleId"]}, {$eq: ["$$acceptedJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                    "hiredJobsArray": {
                                        $filter: {
                                            input: "$hiredJobs",
                                            as: "hiredJobs",
                                            cond: {$and: [{$eq: ["$$hiredJobs.roleId", "$roleId"]}, {$eq: ["$$hiredJobs.jobId", "$jobId"]}]}
                                        }
                                    },
                                }
                            },
                            {
                                "$project": {
                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "isOffered": 1,
                                    "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                                    "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                                    "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                                    "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                                }
                            },
                            {
                                "$project": {
                                    "roleId": 1,
                                    "jobId": 1,
                                    "employerId": 1,
                                    "candidateId": 1,
                                    "isOffered": 1,
                                    "isHired": {
                                        $cond: {
                                            "if": {"$eq": ["$hiredJobsArray.roleId", "$roleId"]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                    "isApplied": {
                                        $cond: {
                                            "if": {"$eq": ["$appliedJobsArray.roleId", "$roleId"]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                    "isDeclined": {
                                        $cond: {
                                            "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                    "isAccepted": {
                                        $cond: {
                                            "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                }
                            }
                        ],
                        "jobInfo": [
                            {
                                "$project": {
                                    "jobDetails": 1,
                                    "setTime": 1
                                }
                            },
                            {
                                "$project": {
                                    "_id": 0,
                                    "skills": "$jobDetails.roleWiseAction.skills",
                                    "roleName": "$jobDetails.roleWiseAction.roleName",
                                    "jobType": "$jobDetails.jobType",
                                    "payType": "$jobDetails.payType",
                                    "pay": "$jobDetails.pay",
                                    "noOfStuff": "$jobDetails.noOfStuff",
                                    "industry": "$jobDetails.industry",
                                    "setTime": "$setTime",
                                }
                            },
                            {
                                $lookup: {
                                    from: "industries",
                                    localField: "industry",
                                    foreignField: "industryId",
                                    as: "industryDetails"
                                }
                            },
                            {
                                "$project": {
                                    skills: 1,
                                    roleName: 1,
                                    jobType: 1,
                                    payType: 1,
                                    pay: 1,
                                    noOfStuff: 1,
                                    industry: 1,
                                    setTime: 1,
                                    industryDetails: {$arrayElemAt: ["$industryDetails", 0]}
                                }
                            },
                            {
                                "$project": {
                                    "setTime": "$setTime",
                                    "skills": "$skills",
                                    "roleName": "$roleName",
                                    "jobType": "$jobType",
                                    "payType": "$payType",
                                    "pay": "$pay",
                                    "noOfStuff": "$noOfStuff",
                                    "industry": "$industry",
                                    "industryName": "$industryDetails.industryName"
                                }
                            },
                            {$unwind: {path: "$skills", preserveNullAndEmptyArrays: true}},
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            {
                                $project: {
                                    "setTime": 1,
                                    "roleName": 1,
                                    "jobType": 1,
                                    "payType": 1,
                                    "pay": 1,
                                    "noOfStuff": 1,
                                    "industry": 1,
                                    "industryName": 1,
                                    "skillDetails": {
                                        $arrayElemAt: ["$skillDetails", 0]
                                    }
                                }
                            },
                            {
                                $group: {
                                    "_id": {
                                        "industryId": "$industry"
                                    },
                                    "setTime": {
                                        $first: "$setTime"
                                    },
                                    "roleName": {
                                        $first: "$roleName"
                                    },
                                    "jobType": {
                                        $first: "$jobType"
                                    },
                                    "payType": {
                                        $first: "$payType"
                                    },
                                    "pay": {
                                        $first: "$pay"
                                    },
                                    "noOfStuff": {
                                        $first: "$noOfStuff"
                                    },
                                    "industry": {
                                        $first: "$industry"
                                    },
                                    "industryName": {
                                        $first: "$industryName"
                                    },
                                    "skillDetails": {
                                        $push: "$skillDetails"
                                    }
                                }
                            }
                        ],
                        "locationInfo": [
                            {
                                "$project": {
                                    "location": 1,
                                    "description": 1,
                                    "locationName": 1,
                                }
                            },
                        ]
                    }
                },
                {
                    $project: {
                        locationInfo: {$arrayElemAt: ["$locationInfo", 0]},
                        jobInfo: {$arrayElemAt: ["$jobInfo", 0]},
                        generalInfo: {$arrayElemAt: ["$generalInfo", 0]},
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((err, result) => {
                console.log(err)
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract
     * APPLY JOBS
     * during apply jobs stored details into appiliedFrom array in job collection
     * at the same time stored the same details into appliedJobs array in candidate collection .
     * at last send sms or mail to employer
     */
    applyJobs: async (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) {
                return;
            }
            let updateWith = {
                'roleId': roleId,
                'candidateId': candidateId
            }
            let findWhere = {
                'appiliedFrom.roleId': roleId,
                'appiliedFrom.candidateId': candidateId,
                'jobId': jobId
            }

            // ==================================================
            // this function is used to check whether candidate is authorised or not // by Ankur on 06-02-20
            async function checkCandidateAuthorised(candidateId) {  // start of checkCandidateAuthorised

                return new Promise(function (resolve, reject) {

                    let aggrQuery = [
                        {
                            $match: {
                                'candidateId': candidateId,
                            }
                        },
                        {
                            $project: {
                                "Status": 1
                            }
                        },

                    ];

                    models.candidate.aggregate(aggrQuery, function (err, response) {

                        if (err) {
                            resolve({"isError": true});
                        } else {

                            if (response.length > 0) {
                                resolve({
                                    "isError": false,
                                    "isExist": true,
                                    "candidateDetail": response
                                });
                            } else {
                                resolve({
                                    "isError": false,
                                    "isExist": false,
                                });
                            }
                        }

                    });
                })
            } // END of checkCandidateAuthorised

            // ===================================================
            models.job.findOne(findWhere, {'appiliedFrom.$': 1}, async function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                } else {
                    // ====================================
                    // Coded by Ankur on 06-02-20
                    let checkCandidateAuthorisedStatus = await checkCandidateAuthorised(candidateId);
                    if (checkCandidateAuthorisedStatus.isError == true || checkCandidateAuthorisedStatus.isExist == false) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                        return;
                    }

                    if (checkCandidateAuthorisedStatus.candidateDetail[0].Status.isApprovedByAot == false) {
                        res.json({
                            isError: true,
                            message: 'Candidate is not approved by admin yet',
                            statuscode: 1026,
                            details: null
                        });
                        return;
                    }
                    //===========================================

                    if (item) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['1026'],
                            statuscode: 1026,
                            details: null
                        })
                    } else {
                        models.job.updateOne(
                            {'employerId': employerId, 'jobId': jobId},
                            {$push: {'appiliedFrom': updateWith}},
                            function (error, response) {
                                if (error) {
                                    console.log("error 1", error)
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    let insertedValue = {
                                        "roleId": roleId,
                                        "employerId": employerId,
                                        "jobId": jobId,
                                        "candidateId": candidateId
                                    }
                                    if (response.nModified == 1) {
                                        models.candidate.updateOne({'candidateId': candidateId},
                                            {$push: {"appliedJobs": insertedValue}},
                                            function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {
                                                if (appliedJobsUpdatedError) {
                                                    console.log("error 2", appliedJobsUpdatedError)
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                if (appliedJobsUpdatedresponse.nModified == 1) {
                                                    models.authenticate.findOne({'userId': employerId}, async function (err, item) {
                                                        if (item['mobileNo'] == "") {
                                                            var result = await sendNotificationEmail(item['EmailId'], candidateId, jobId, roleId, employerId)
                                                            if (result) {

                                                                models.candidate.find({"candidateId": candidateId}, function (error, data) {

                                                                    models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                        if (pushNotificationStatus == true) {
                                                                            fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job")
                                                                        }

                                                                        // .then(function(nofify){
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['1029'],
                                                                            statuscode: 1029,
                                                                            details: null
                                                                        })
                                                                        // })
                                                                    })
                                                                })
                                                            } else {
                                                                console.log("error 3", err)
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        } else {
                                                            var result = await sendNotificationSms(item['mobileNo'], candidateId, jobId, roleId, employerId)
                                                            if (result) {
                                                                models.candidate.find({"candidateId": candidateId}, function (error, data) {

                                                                    models.employer.find({"employerId": employerId}, function (searchingerror, searchRating) {

                                                                        if (pushNotificationStatus == true) {
                                                                            fcm.sendTocken(searchRating[0].Status.fcmTocken, data[0].fname + " " + data[0].lname + " accepted your job")
                                                                        }
                                                                        // .then(function(nofify){
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['1029'],
                                                                            statuscode: 1029,
                                                                            details: null
                                                                        })
                                                                        // })
                                                                    })
                                                                })
                                                            } else {
                                                                console.log("error 4", err)
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                    })
                                                } else {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            })
                                    } else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                    }
                }
            })
        } catch (error) {
            console.log("error 5", error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract
     * SEARCH JOBS
     */
    searchJobs1: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let presentDate = req.body.presentDate ? req.body.presentDate : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present date"
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);

            //  let date=Math.floor(Date.now() / 1000);
            //  let FinalDate=date*1000;
            let FinalDate = dateObj.realDateTimeStamp;
            let today = dateObj

            let dateUtc = new Date(FinalDate);
            let dateLocal = dateUtc.toLocaleString();

            // res.json({
            //     FinalDate: FinalDate,
            //     dateObj: dateObj
            // });
            // return;


            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************


            let nearbypageno = req.body.nearbypageno ? (parseInt(req.body.nearbypageno == 0 ? 0 : parseInt(req.body.nearbypageno) - 1)) : 0;
            let nearbyperPage = req.body.nearbyperpage ? parseInt(req.body.nearbyperpage) : 10;
            let outsidepageno = req.body.outsidepageno ? (parseInt(req.body.outsidepageno == 0 ? 0 : parseInt(req.body.outsidepageno) - 1)) : 0;
            let outsideperPage = req.body.outsideperpage ? parseInt(req.body.outsideperpage) : 10;
            let industryId = req.body.industryId ? req.body.industryId : '';
            let roleId = req.body.roleId ? req.body.roleId : '';
            let jobType = req.body.jobType ? req.body.jobType : '';
            let payType = req.body.payType ? req.body.payType : '';
            let skillValues = req.body.skillValues ? req.body.skillValues : '';
            let candidateLat = req.body.candidateLat ? req.body.candidateLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Latitude"
            });
            ;
            let candidateLong = req.body.candidateLong ? req.body.candidateLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate longitude"
            });
            ;
            let roleIds = roleId;
            let skillValuesDetails = skillValues;
            let payTypeValues = payType;
            let jobTypeValues = jobType;
            console.log(payTypeValues);
            console.log(jobTypeValues)
            if (!req.body.candidateId) {
                return;
            }
            models.candidate.find({'candidateId': candidateId}, function (err, item) {
                userPrefDist = item[0]['distance'];
                // distanceInMeter=userPrefDist*1000;
                distanceInMeter = userPrefDist;
                console.log(distanceInMeter)
                if (candidateLong == '' || candidateLat == '') {
                    candidateLongDetails = item[0]['geolocation']['coordinates'][0];
                    candidateLatDetails = item[0]['geolocation']['coordinates'][1];
                } else {
                    candidateLongDetails = candidateLong;
                    candidateLatDetails = candidateLat;
                }

                let aggrQuery = [
                    {
                        $geoNear: {
                            near: {
                                type: "Point",
                                coordinates: [parseFloat(candidateLongDetails), parseFloat(candidateLatDetails)]
                            },
                            distanceField: "dist.locationDistance",
                            maxDistance: parseInt(distanceInMeter),
                            query: {type: "public"},
                            includeLocs: "dist.location",
                            query: {},
                            spherical: true
                        }
                    },
                    {
                        '$match': {
                            $and: [
                                // industryId ? { 'jobDetails.industry': industryId }:{},
                                industryId ? {'jobDetails.industry': {$in: industryId}} : {},
                                roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {},
                                jobTypeValues ? {'jobDetails.roleWiseAction.jobType': {$in: jobTypeValues}} : {},
                                payTypeValues ? {'jobDetails.roleWiseAction.payType': {$in: payTypeValues}} : {},
                                skillValuesDetails ? {'jobDetails.roleWiseAction.skills': {$in: skillValuesDetails}} : {}
                            ]
                        }
                    },
                    {$unwind: "$jobDetails.roleWiseAction"},
                    {'$match': roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {}},
                    {
                        $project: {
                            '_id': 0,
                            'employerId': 1,
                            'jobDetails': 1,
                            'jobId': 1,
                            'appiliedFrom': 1,
                            'approvedTo': 1,
                            'dist': 1
                        }
                    },
                    {
                        "$project": {
                            "employerId": "$employerId",
                            "jobDetails": "$jobDetails",
                            "jobId": "$jobId",
                            "dist": "$dist",
                            "isOfferedArray": {
                                $filter: {
                                    input: "$approvedTo",
                                    as: "approvedTo",
                                    cond: {$eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": candidateId,
                            "isOffered": {
                                $cond: {
                                    "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                    "then": true,
                                    "else": false
                                }
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "candidates",
                            localField: "candidateId",
                            foreignField: "candidateId",
                            as: "candidateDetails"
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": candidateId,
                            "isOffered": 1,
                            "candidateDetails": {$arrayElemAt: ["$candidateDetails", 0]}

                        }
                    },
                    {
                        "$project": {
                            "employerId": "$employerId",
                            "jobDetails": "$jobDetails",
                            "jobId": "$jobId",
                            "dist": "$dist",
                            "candidateId": "$candidateId",
                            "isOffered": "$isOffered",
                            "declinedJobs": "$candidateDetails.declinedJobs",
                            "acceptedJobs": "$candidateDetails.acceptedJobs",
                            "appliedJobs": "$candidateDetails.appliedJobs",
                            "hiredJobs": "$candidateDetails.hiredJobs",
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "declinedJobsArray": {
                                $filter: {
                                    input: "$declinedJobs",
                                    as: "declinedJobs",
                                    cond: {$eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                            "acceptedJobsArray": {
                                $filter: {
                                    input: "$acceptedJobs",
                                    as: "acceptedJobs",
                                    cond: {$eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                            "appliedJobsArray": {
                                $filter: {
                                    input: "$appliedJobs",
                                    as: "appliedJobs",
                                    cond: {$eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                            "hiredJobsArray": {
                                $filter: {
                                    input: "$hiredJobs",
                                    as: "hiredJobs",
                                    cond: {$eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "declinedJobsArray": {
                                $filter: {
                                    input: "$declinedJobsArray",
                                    as: "declinedJobsArray",
                                    cond: {$eq: ["$$declinedJobsArray.jobId", "$jobId"]}
                                }
                            },
                            "acceptedJobsArray": {
                                $filter: {
                                    input: "$acceptedJobsArray",
                                    as: "acceptedJobsArray",
                                    cond: {$eq: ["$$acceptedJobsArray.jobId", "$jobId"]}
                                }
                            },
                            "appliedJobsArray": {
                                $filter: {
                                    input: "$appliedJobsArray",
                                    as: "appliedJobsArray",
                                    cond: {$eq: ["$$appliedJobsArray.jobId", "$jobId"]}
                                }
                            },
                            "hiredJobsArray": {
                                $filter: {
                                    input: "$hiredJobsArray",
                                    as: "hiredJobsArray",
                                    cond: {$eq: ["$$hiredJobsArray.jobId", "$jobId"]}
                                }
                            },
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                            "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                            "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                            "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                        }
                    },
                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "status": {
                                "isApplied": {
                                    $cond: {
                                        "if": {"$eq": ["$appliedJobsArray.candidateId", candidateId]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                                "isHired": {
                                    $cond: {
                                        "if": {"$eq": ["$hiredJobsArray.candidateId", candidateId]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                                "isOffered": "$isOffered",
                                "isDeclined": {
                                    $cond: {
                                        "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                                "isAccepted": {
                                    $cond: {
                                        "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                            },
                        }
                    },

                    {
                        "$project": {
                            "employerId": 1,
                            "jobDetails": 1,
                            "isDeleted": "$jobDetails.roleWiseAction.isDeleted",
                            "startRealDateTimeStamp": "$jobDetails.roleWiseAction.startRealDateTimeStamp",
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "status": 1,
                        }
                    },

                    {
                        $match: {
                            $and: [
                                {"isDeleted": {$ne: "NO"}},
                                {"startRealDateTimeStamp": {$gte: FinalDate}}
                            ]
                        }
                    },

                    {
                        $match: {
                            $or: [{
                                "status.isApplied": false,
                                "status.isHired": false,
                                "status.isOffered": false,
                                "status.isDeclined": false,
                                "status.isAccepted": false
                            }]
                        }
                    },

                    {$sort: {_id: -1}},

                    {
                        $group: {
                            _id: null,
                            total: {
                                $sum: 1
                            },
                            results: {
                                $push: '$$ROOT'
                            }
                        }
                    },
                    {
                        $project: {
                            '_id': 0,
                            'total': 1,
                            'noofpage': {$ceil: {$divide: ["$total", nearbyperPage]}},
                            'results': {
                                $slice: [
                                    '$results', nearbypageno * nearbyperPage, nearbyperPage
                                ]
                            }
                        }
                    }
                ]
                models.job.aggregate(aggrQuery).exec((err, result) => {

                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    } else {
                        if (result.length !== 0) {
                            let aggrQuery1 = [
                                {
                                    '$match': {
                                        $and: [
                                            // industryId ? { 'jobDetails.industry': industryId }:{},
                                            industryId ? {'jobDetails.industry': {$in: industryId}} : {},
                                            roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {},
                                            jobTypeValues ? {'jobDetails.roleWiseAction.jobType': {$in: jobTypeValues}} : {},
                                            payTypeValues ? {'jobDetails.roleWiseAction.payType': {$in: payTypeValues}} : {},
                                            skillValuesDetails ? {'jobDetails.roleWiseAction.skills': {$in: skillValuesDetails}} : {}
                                        ]
                                    }
                                },
                                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: true}},

                                {'$match': roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {}},
                                {$match: {'jobDetails.roleWiseAction.location.coordinates': {$nin: result[0].results[0].jobDetails.roleWiseAction['location']['coordinates']}}},
                                {
                                    $project: {
                                        '_id': 0,
                                        'employerId': 1,
                                        'jobDetails': 1,
                                        'jobId': 1,
                                        'appiliedFrom': 1,
                                        'approvedTo': 1,
                                        'dist': 1
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "jobId": "$jobId",
                                        "dist": "$dist",
                                        "isOfferedArray": {
                                            $filter: {
                                                input: "$approvedTo",
                                                as: "approvedTo",
                                                cond: {$eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },

                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": candidateId,
                                        "isOffered": {
                                            $cond: {
                                                "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": candidateId,
                                        "isOffered": 1,
                                        "candidateDetails": {$arrayElemAt: ["$candidateDetails", 0]}

                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "jobId": "$jobId",
                                        "dist": "$dist",
                                        "candidateId": "$candidateId",
                                        "isOffered": "$isOffered",
                                        "declinedJobs": "$candidateDetails.declinedJobs",
                                        "acceptedJobs": "$candidateDetails.acceptedJobs",
                                        "appliedJobs": "$candidateDetails.appliedJobs",
                                        "hiredJobs": "$candidateDetails.hiredJobs",
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobs",
                                                as: "declinedJobs",
                                                cond: {$eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobs",
                                                as: "acceptedJobs",
                                                cond: {$eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobs",
                                                as: "appliedJobs",
                                                cond: {$eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobs",
                                                as: "hiredJobs",
                                                cond: {$eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobsArray",
                                                as: "declinedJobsArray",
                                                cond: {$eq: ["$$declinedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobsArray",
                                                as: "acceptedJobsArray",
                                                cond: {$eq: ["$$acceptedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobsArray",
                                                as: "appliedJobsArray",
                                                cond: {$eq: ["$$appliedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobsArray",
                                                as: "hiredJobsArray",
                                                cond: {$eq: ["$$hiredJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                                        "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                                        "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                                        "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "status": {
                                            "isApplied": {
                                                $cond: {
                                                    "if": {"$eq": ["$appliedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isHired": {
                                                $cond: {
                                                    "if": {"$eq": ["$hiredJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isOffered": "$isOffered",
                                            "isDeclined": {
                                                $cond: {
                                                    "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isAccepted": {
                                                $cond: {
                                                    "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                {
                                    $match: {
                                        $or: [{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                            $sum: 1
                                        },
                                        results: {
                                            $push: '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project: {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': {$ceil: {$divide: ["$total", outsideperPage]}},
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage, outsideperPage
                                            ]
                                        }
                                    }
                                }

                            ]
                            models.job.aggregate(aggrQuery1).exec((err, data) => {

                                if (err) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: data[0],
                                        nearByJobs: result[0],
                                    })
                                }
                            })
                        } else {

                            let aggrQuery2 = [
                                {
                                    '$match': {
                                        $and: [
                                            // industryId ? { 'jobDetails.industry': industryId }:{},
                                            industryId ? {'jobDetails.industry': {$in: industryId}} : {},
                                            roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {},
                                            jobTypeValues ? {'jobDetails.roleWiseAction.jobType': {$in: jobTypeValues}} : {},
                                            payTypeValues ? {'jobDetails.roleWiseAction.payType': {$in: payTypeValues}} : {},
                                            skillValuesDetails ? {'jobDetails.roleWiseAction.skills': {$in: skillValuesDetails}} : {}
                                        ]
                                    }
                                },
                                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: true}},
                                {'$match': roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {}},
                                {
                                    $project: {
                                        '_id': 0,
                                        'employerId': 1,
                                        'jobDetails': 1,
                                        'jobId': 1,
                                        'appiliedFrom': 1,
                                        'approvedTo': 1,
                                        'dist': 1
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "jobId": "$jobId",
                                        "dist": "$dist",
                                        "isOfferedArray": {
                                            $filter: {
                                                input: "$approvedTo",
                                                as: "approvedTo",
                                                cond: {$eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },

                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": candidateId,
                                        "isOffered": {
                                            $cond: {
                                                "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": candidateId,
                                        "isOffered": 1,
                                        "candidateDetails": {$arrayElemAt: ["$candidateDetails", 0]}

                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "jobId": "$jobId",
                                        "dist": "$dist",
                                        "candidateId": "$candidateId",
                                        "isOffered": "$isOffered",
                                        "declinedJobs": "$candidateDetails.declinedJobs",
                                        "acceptedJobs": "$candidateDetails.acceptedJobs",
                                        "appliedJobs": "$candidateDetails.appliedJobs",
                                        "hiredJobs": "$candidateDetails.hiredJobs",
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobs",
                                                as: "declinedJobs",
                                                cond: {$eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobs",
                                                as: "acceptedJobs",
                                                cond: {$eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobs",
                                                as: "appliedJobs",
                                                cond: {$eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobs",
                                                as: "hiredJobs",
                                                cond: {$eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobsArray",
                                                as: "declinedJobsArray",
                                                cond: {$eq: ["$$declinedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobsArray",
                                                as: "acceptedJobsArray",
                                                cond: {$eq: ["$$acceptedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobsArray",
                                                as: "appliedJobsArray",
                                                cond: {$eq: ["$$appliedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobsArray",
                                                as: "hiredJobsArray",
                                                cond: {$eq: ["$$hiredJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                                        "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                                        "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                                        "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "jobId": 1,
                                        "dist": 1,
                                        "candidateId": 1,
                                        "status": {
                                            "isApplied": {
                                                $cond: {
                                                    "if": {"$eq": ["$appliedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isHired": {
                                                $cond: {
                                                    "if": {"$eq": ["$hiredJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isOffered": "$isOffered",
                                            "isDeclined": {
                                                $cond: {
                                                    "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isAccepted": {
                                                $cond: {
                                                    "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                {
                                    $match: {
                                        $or: [{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },

                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                            $sum: 1
                                        },
                                        results: {
                                            $push: '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project: {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': {$ceil: {$divide: ["$total", outsideperPage]}},
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage, outsideperPage
                                            ]
                                        }
                                    }
                                }
                            ]
                            models.job.aggregate(aggrQuery2).exec((error, outsidedata) => {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: outsidedata[0],
                                        nearByJobs: result[0]
                                    })
                                }

                            })
                        }
                    }
                })
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    test: (req, res, next) => {
        console.log("hit")
        fcm.sendTocken("dhi5VZQDqKA:APA91bGJCOW_jz3EmAX4m78_scnkMV2nmGq0xCedTSEzmMXobsoeje0qtHa6Py3l9s0L9xK75FKHE2QbHrUr6PpVsQ0iRqOKAeeqxUEoShwRFAXQX6uhngrGbtojp3qqX7BZ-lyCEBF_")
            .then(function (nofify) {
                console.log(nofify)
            })

    },
    /*
    search jobs for old structure is below
    */
    searchJobs: (req, res, next) => {
        try {
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });
            let nearbypageno = req.body.nearbypageno ? (parseInt(req.body.nearbypageno == 0 ? 0 : parseInt(req.body.nearbypageno) - 1)) : 0;
            let nearbyperPage = req.body.nearbyperpage ? parseInt(req.body.nearbyperpage) : 10;
            let outsidepageno = req.body.outsidepageno ? (parseInt(req.body.outsidepageno == 0 ? 0 : parseInt(req.body.outsidepageno) - 1)) : 0;
            let outsideperPage = req.body.outsideperpage ? parseInt(req.body.outsideperpage) : 10;
            let industryId = req.body.industryId ? req.body.industryId : '';
            let roleId = req.body.roleId ? req.body.roleId : '';
            let jobType = req.body.jobType ? req.body.jobType : '';
            // let payType = req.body.payType ? req.body.payType : '';
            let payType = req.body.payType ? req.body.payType : 'Hourly';
            let skillValues = req.body.skillValues ? req.body.skillValues : '';
            let candidateLat = req.body.candidateLat ? req.body.candidateLat : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Latitude"
            });
            ;
            let candidateLong = req.body.candidateLong ? req.body.candidateLong : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate longitude"
            });
            ;
            let roleIds = roleId;
            let skillValuesDetails = skillValues;
            let payTypeValues = payType;
            let jobTypeValues = jobType;
            if (!req.body.candidateId) {
                return;
            }
            models.candidate.find({'candidateId': candidateId}, function (err, item) {
                userPrefDist = item[0]['distance'];
                distanceInMeter = userPrefDist * 1000;
                console.log(distanceInMeter)
                if (candidateLong == '' || candidateLat == '') {
                    candidateLongDetails = item[0]['geolocation']['coordinates'][0];
                    candidateLatDetails = item[0]['geolocation']['coordinates'][1];
                } else {
                    candidateLongDetails = candidateLong;
                    candidateLatDetails = candidateLat;
                }
                console.log(candidateLongDetails, candidateLatDetails, userPrefDist)
                let aggrQuery = [
                    {
                        $geoNear: {
                            near: {
                                type: "Point",
                                coordinates: [parseFloat(candidateLongDetails), parseFloat(candidateLatDetails)]
                            },
                            distanceField: "dist.locationDistance",
                            maxDistance: parseInt(distanceInMeter),
                            query: {type: "public"},
                            includeLocs: "dist.location",
                            query: {},
                            spherical: true
                        }
                    },
                    {
                        '$match': {
                            $and: [
                                industryId ? {'jobDetails.industry': industryId} : {},
                                roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {},
                                jobTypeValues ? {'jobDetails.jobType': {$in: jobTypeValues}} : {},
                                payTypeValues ? {'jobDetails.payType': {$in: payTypeValues}} : {},
                                skillValuesDetails ? {'jobDetails.roleWiseAction.skills': {$in: skillValuesDetails}} : {}
                            ]
                        }
                    },
                    {$unwind: "$jobDetails.roleWiseAction"},
                    {'$match': roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {}},
                    {
                        $project: {
                            '_id': 0,
                            'locationName': 1,
                            'location': 1,
                            'employerId': 1,
                            'jobDetails': 1,
                            'description': 1,
                            'distance': 1,
                            'jobId': 1,
                            'appiliedFrom': 1,
                            'approvedTo': 1,
                            'dist': 1
                        }
                    },
                    {
                        "$project": {
                            "locationName": "$locationName",
                            "location": "$location",
                            "employerId": "$employerId",
                            "jobDetails": "$jobDetails",
                            "description": "$description",
                            "distance": "$distance",
                            "jobId": "$jobId",
                            "dist": "$dist",
                            "isOfferedArray": {
                                $filter: {
                                    input: "$approvedTo",
                                    as: "approvedTo",
                                    cond: {$eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                        }
                    },
                    {
                        "$project": {
                            'locationName': 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                        }
                    },

                    {
                        "$project": {
                            'locationName': 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": candidateId,
                            "isOffered": {
                                $cond: {
                                    "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                    "then": true,
                                    "else": false
                                }
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "candidates",
                            localField: "candidateId",
                            foreignField: "candidateId",
                            as: "candidateDetails"
                        }
                    },
                    {
                        "$project": {
                            "locationName": 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "candidateDetails": {$arrayElemAt: ["$candidateDetails", 0]}

                        }
                    },
                    {
                        "$project": {
                            "locationName": "$locationName",
                            "location": "$location",
                            "employerId": "$employerId",
                            "jobDetails": "$jobDetails",
                            "description": "$description",
                            "distance": "$distance",
                            "jobId": "$jobId",
                            "dist": "$dist",
                            "candidateId": "$candidateId",
                            "isOffered": "$isOffered",
                            "declinedJobs": "$candidateDetails.declinedJobs",
                            "acceptedJobs": "$candidateDetails.acceptedJobs",
                            "appliedJobs": "$candidateDetails.appliedJobs",
                            "hiredJobs": "$candidateDetails.hiredJobs",
                        }
                    },
                    {
                        "$project": {
                            "locationName": 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "declinedJobsArray": {
                                $filter: {
                                    input: "$declinedJobs",
                                    as: "declinedJobs",
                                    cond: {$eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                            "acceptedJobsArray": {
                                $filter: {
                                    input: "$acceptedJobs",
                                    as: "acceptedJobs",
                                    cond: {$eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                            "appliedJobsArray": {
                                $filter: {
                                    input: "$appliedJobs",
                                    as: "appliedJobs",
                                    cond: {$eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                            "hiredJobsArray": {
                                $filter: {
                                    input: "$hiredJobs",
                                    as: "hiredJobs",
                                    cond: {$eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                }
                            },
                        }
                    },
                    {
                        "$project": {
                            "locationName": 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "declinedJobsArray": {
                                $filter: {
                                    input: "$declinedJobsArray",
                                    as: "declinedJobsArray",
                                    cond: {$eq: ["$$declinedJobsArray.jobId", "$jobId"]}
                                }
                            },
                            "acceptedJobsArray": {
                                $filter: {
                                    input: "$acceptedJobsArray",
                                    as: "acceptedJobsArray",
                                    cond: {$eq: ["$$acceptedJobsArray.jobId", "$jobId"]}
                                }
                            },
                            "appliedJobsArray": {
                                $filter: {
                                    input: "$appliedJobsArray",
                                    as: "appliedJobsArray",
                                    cond: {$eq: ["$$appliedJobsArray.jobId", "$jobId"]}
                                }
                            },
                            "hiredJobsArray": {
                                $filter: {
                                    input: "$hiredJobsArray",
                                    as: "hiredJobsArray",
                                    cond: {$eq: ["$$hiredJobsArray.jobId", "$jobId"]}
                                }
                            },
                        }
                    },
                    {
                        "$project": {
                            "locationName": 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "isOffered": 1,
                            "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                            "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                            "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                            "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                        }
                    },
                    {
                        "$project": {
                            "locationName": 1,
                            "location": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                            "description": 1,
                            "distance": 1,
                            "jobId": 1,
                            "dist": 1,
                            "candidateId": 1,
                            "status": {
                                "isApplied": {
                                    $cond: {
                                        "if": {"$eq": ["$appliedJobsArray.candidateId", candidateId]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                                "isHired": {
                                    $cond: {
                                        "if": {"$eq": ["$hiredJobsArray.candidateId", candidateId]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                                "isOffered": "$isOffered",
                                "isDeclined": {
                                    $cond: {
                                        "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                                "isAccepted": {
                                    $cond: {
                                        "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                        "then": true,
                                        "else": false
                                    }
                                },
                            },
                        }
                    },
                    {
                        $match: {
                            $or: [{
                                "status.isApplied": false,
                                "status.isHired": false,
                                "status.isOffered": false,
                                "status.isDeclined": false,
                                "status.isAccepted": false
                            }]
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total: {
                                $sum: 1
                            },
                            results: {
                                $push: '$$ROOT'
                            }
                        }
                    },
                    {
                        $project: {
                            '_id': 0,
                            'total': 1,
                            'noofpage': {$ceil: {$divide: ["$total", nearbyperPage]}},
                            'results': {
                                $slice: [
                                    '$results', nearbypageno * nearbyperPage, nearbyperPage
                                ]
                            }
                        }
                    }
                ]
                models.job.aggregate(aggrQuery).exec((err, result) => {
                    console.log(err)
                    if (err) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    } else {
                        if (result.length !== 0) {
                            let aggrQuery1 = [
                                {
                                    '$match': {
                                        $and: [
                                            industryId ? {'jobDetails.industry': industryId} : {},
                                            roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {},
                                            jobTypeValues ? {'jobDetails.jobType': {$in: jobTypeValues}} : {},
                                            payTypeValues ? {'jobDetails.payType': {$in: payTypeValues}} : {},
                                            skillValuesDetails ? {'jobDetails.roleWiseAction.skills': {$in: skillValuesDetails}} : {}
                                        ]
                                    }
                                },
                                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: true}},

                                {'$match': roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {}},
                                {$match: {'location.coordinates': {$nin: result[0].results[0]['location']['coordinates']}}},
                                {
                                    $project: {
                                        '_id': 0,
                                        'locationName': 1,
                                        'location': 1,
                                        'employerId': 1,
                                        'jobDetails': 1,
                                        'description': 1,
                                        'distance': 1,
                                        'jobId': 1,
                                        'appiliedFrom': 1,
                                        'approvedTo': 1,
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": "$locationName",
                                        "location": "$location",
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "isOfferedArray": {
                                            $filter: {
                                                input: "$approvedTo",
                                                as: "approvedTo",
                                                cond: {$eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },

                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": candidateId,
                                        "isOffered": {
                                            $cond: {
                                                "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "candidateDetails": {$arrayElemAt: ["$candidateDetails", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": "$locationName",
                                        "location": "$location",
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "candidateId": "$candidateId",
                                        "isOffered": "$isOffered",
                                        "declinedJobs": "$candidateDetails.declinedJobs",
                                        "acceptedJobs": "$candidateDetails.acceptedJobs",
                                        "appliedJobs": "$candidateDetails.appliedJobs",
                                        "hiredJobs": "$candidateDetails.hiredJobs",

                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobs",
                                                as: "declinedJobs",
                                                cond: {$eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobs",
                                                as: "acceptedJobs",
                                                cond: {$eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobs",
                                                as: "appliedJobs",
                                                cond: {$eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobs",
                                                as: "hiredJobs",
                                                cond: {$eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobsArray",
                                                as: "declinedJobsArray",
                                                cond: {$eq: ["$$declinedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobsArray",
                                                as: "acceptedJobsArray",
                                                cond: {$eq: ["$$acceptedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobsArray",
                                                as: "appliedJobsArray",
                                                cond: {$eq: ["$$appliedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobsArray",
                                                as: "hiredJobsArray",
                                                cond: {$eq: ["$$hiredJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                                        "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                                        "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                                        "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "status": {
                                            "isApplied": {
                                                $cond: {
                                                    "if": {"$eq": ["$appliedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isHired": {
                                                $cond: {
                                                    "if": {"$eq": ["$hiredJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isOffered": "$isOffered",
                                            "isDeclined": {
                                                $cond: {
                                                    "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isAccepted": {
                                                $cond: {
                                                    "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                {
                                    $match: {
                                        $or: [{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                            $sum: 1
                                        },
                                        results: {
                                            $push: '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project: {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': {$ceil: {$divide: ["$total", outsideperPage]}},
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage, outsideperPage
                                            ]
                                        }
                                    }
                                }

                            ]
                            models.job.aggregate(aggrQuery1).exec((err, data) => {
                                console.log('............', data)
                                if (err) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: data[0],
                                        nearByJobs: result[0]
                                    })
                                }
                            })
                        } else {

                            let aggrQuery2 = [
                                {
                                    '$match': {
                                        $and: [
                                            industryId ? {'jobDetails.industry': industryId} : {},
                                            roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {},
                                            jobTypeValues ? {'jobDetails.jobType': {$in: jobTypeValues}} : {},
                                            payTypeValues ? {'jobDetails.payType': {$in: payTypeValues}} : {},
                                            skillValuesDetails ? {'jobDetails.roleWiseAction.skills': {$in: skillValuesDetails}} : {}
                                        ]
                                    }
                                },
                                {$unwind: {path: "$jobDetails.roleWiseAction", preserveNullAndEmptyArrays: true}},
                                {'$match': roleIds ? {'jobDetails.roleWiseAction.roleId': {$in: roleIds}} : {}},
                                {
                                    $project: {
                                        '_id': 0,
                                        'locationName': 1,
                                        'location': 1,
                                        'employerId': 1,
                                        'jobDetails': 1,
                                        'description': 1,
                                        'distance': 1,
                                        'jobId': 1,
                                        'appiliedFrom': 1,
                                        'approvedTo': 1,
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": "$locationName",
                                        "location": "$location",
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "isOfferedArray": {
                                            $filter: {
                                                input: "$approvedTo",
                                                as: "approvedTo",
                                                cond: {$eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },

                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "isOfferedArray": {$arrayElemAt: ["$isOfferedArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": candidateId,
                                        "isOffered": {
                                            $cond: {
                                                "if": {"$eq": ["$isOfferedArray.candidateId", candidateId]},
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "candidates",
                                        localField: "candidateId",
                                        foreignField: "candidateId",
                                        as: "candidateDetails"
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "candidateDetails": {$arrayElemAt: ["$candidateDetails", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": "$locationName",
                                        "location": "$location",
                                        "employerId": "$employerId",
                                        "jobDetails": "$jobDetails",
                                        "description": "$description",
                                        "distance": "$distance",
                                        "jobId": "$jobId",
                                        "candidateId": "$candidateId",
                                        "isOffered": "$isOffered",
                                        "declinedJobs": "$candidateDetails.declinedJobs",
                                        "acceptedJobs": "$candidateDetails.acceptedJobs",
                                        "appliedJobs": "$candidateDetails.appliedJobs",
                                        "hiredJobs": "$candidateDetails.hiredJobs",

                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobs",
                                                as: "declinedJobs",
                                                cond: {$eq: ["$$declinedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobs",
                                                as: "acceptedJobs",
                                                cond: {$eq: ["$$acceptedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobs",
                                                as: "appliedJobs",
                                                cond: {$eq: ["$$appliedJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobs",
                                                as: "hiredJobs",
                                                cond: {$eq: ["$$hiredJobs.roleId", "$jobDetails.roleWiseAction.roleId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {
                                            $filter: {
                                                input: "$declinedJobsArray",
                                                as: "declinedJobsArray",
                                                cond: {$eq: ["$$declinedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "acceptedJobsArray": {
                                            $filter: {
                                                input: "$acceptedJobsArray",
                                                as: "acceptedJobsArray",
                                                cond: {$eq: ["$$acceptedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "appliedJobsArray": {
                                            $filter: {
                                                input: "$appliedJobsArray",
                                                as: "appliedJobsArray",
                                                cond: {$eq: ["$$appliedJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                        "hiredJobsArray": {
                                            $filter: {
                                                input: "$hiredJobsArray",
                                                as: "hiredJobsArray",
                                                cond: {$eq: ["$$hiredJobsArray.jobId", "$jobId"]}
                                            }
                                        },
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "isOffered": 1,
                                        "declinedJobsArray": {$arrayElemAt: ["$declinedJobsArray", 0]},
                                        "acceptedJobsArray": {$arrayElemAt: ["$acceptedJobsArray", 0]},
                                        "appliedJobsArray": {$arrayElemAt: ["$appliedJobsArray", 0]},
                                        "hiredJobsArray": {$arrayElemAt: ["$hiredJobsArray", 0]}
                                    }
                                },
                                {
                                    "$project": {
                                        "locationName": 1,
                                        "location": 1,
                                        "employerId": 1,
                                        "jobDetails": 1,
                                        "description": 1,
                                        "distance": 1,
                                        "jobId": 1,
                                        "candidateId": 1,
                                        "status": {
                                            "isOffered": "$isOffered",
                                            "isApplied": {
                                                $cond: {
                                                    "if": {"$eq": ["$appliedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isHired": {
                                                $cond: {
                                                    "if": {"$eq": ["$hiredJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isDeclined": {
                                                $cond: {
                                                    "if": {"$eq": ["$declinedJobsArray.candidateId", candidateId]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                            "isAccepted": {
                                                $cond: {
                                                    "if": {$or: [{"$eq": ["$acceptedJobsArray.candidateId", candidateId]}, {"$eq": ["$hiredJobsArray.candidateId", candidateId]}]},
                                                    "then": true,
                                                    "else": false
                                                }
                                            },
                                        },
                                    }
                                },
                                {
                                    $match: {
                                        $or: [{
                                            "status.isApplied": false,
                                            "status.isHired": false,
                                            "status.isOffered": false,
                                            "status.isDeclined": false,
                                            "status.isAccepted": false
                                        }]
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                            $sum: 1
                                        },
                                        results: {
                                            $push: '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project: {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': {$ceil: {$divide: ["$total", outsideperPage]}},
                                        'results': {
                                            $slice: [
                                                '$results', outsidepageno * outsideperPage, outsideperPage
                                            ]
                                        }
                                    }
                                }
                            ]
                            models.job.aggregate(aggrQuery2).exec((error, outsidedata) => {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                } else {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        outSideJobs: outsidedata[0],
                                        nearByJobs: result[0]
                                    })
                                }

                            })
                        }
                    }
                })
            })
        } catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    // testsendNotificationEmail: async (req, res, next)=>{

    //     let emailId = req.body.emailId;
    //     let candidateId = req.body.candidateId;
    //     let jobId = req.body.jobId;
    //     let roleId = req.body.roleId;
    //     let employerId = req.body.employerId;

    //     let locationName = "";

    //     let fetchJobStatus = await employerJobBusiness.fetchJob(jobId);

    //     if (fetchJobStatus.isError == false) {
    //         locationName = fetchJobStatus.jobDetails[0].jobDetails.roleWiseAction[0].locationName;
    //     }

    //     res.json({
    //         locationName: locationName
    //     })

    //     sendNotificationEmail(emailId,candidateId,jobId,roleId,employerId)
    //     // res.json({
    //     //     request: req.body
    //     // })
    // },
}

function sendNotificationEmailCheckout(candidateId, checkOutTime, employerId, roleId, jobId) {

    return new Promise(resolve => {
        models.employer.findOne({'employerId': employerId},
            function (err, item) {
                if (err) {
                    resolve({
                        "isError": true
                    });
                } else {
                    let empEmailId = item.EmailId;

                    models.candidate.findOne({'candidateId': candidateId},
                        function (err, response) {
                            if (err) {
                                resolve({
                                    "isError": true
                                });
                            } else {
                                let candidateName = response.fname + " " + response.mname + " " + response.lname

                                models.jobrole.findOne({'jobRoleId': roleId},
                                    function (err, searchRole) {
                                        if (err) {
                                            resolve({
                                                "isError": true
                                            });
                                        } else {
                                            let today = new Date();
                                            let dd = today.getDate();

                                            let mm = today.getMonth() + 1;
                                            const yyyy = today.getFullYear();
                                            if (dd < 10) {
                                                dd = `0${dd}`;
                                            }

                                            if (mm < 10) {
                                                mm = `0${mm}`;
                                            }
                                            today = `${mm}-${dd}-${yyyy}`;

                                            // let msgForEmail = candidateName+" having candidateId "+candidateId+" and have job role name "+searchRole.jobRoleName+" and jobId "+jobId+" has checked out successfully at "+checkOutTime+" on date "+today;

                                            let msgForEmail = candidateName + " has checked out successfully at " + checkOutTime + " on Date: " + today + "   Candidate ID: " + candidateId + ". Position: " + searchRole.jobRoleName + " Job ID: " + jobId + ".";

                                            //Newton Sci has checked out successfully at 12.35 on Date: 20/07/2020    Candidate ID:1595226466448. Position: Bartender Job ID: 159522717561953

                                            simplemailercontroller.viaGmail({
                                                    receiver: empEmailId,
                                                    subject: '----Checked Out Logs----',
                                                    msg: msgForEmail
                                                },
                                                (mailerErr, nodeMailerResponse) => {
                                                    if (mailerErr) {
                                                        resolve({
                                                            "isError": true
                                                        });
                                                    } else {
                                                        resolve({
                                                            "isError": false
                                                        });

                                                    }
                                                })
                                        }
                                    });
                            }
                        });
                } //  function (err, item) {

            }) // END models.employer.findOne({'employerId':employerId},
    })
}

//=========================================================================
function sendNotificationEmailForAcceptJob(emailId, candidateId, jobId, roleId, employerId) {
    let globalVarObj = {};

    return new Promise(resolve => {
        models.candidate.findOne({'candidateId': candidateId}, function (err, item) {
            if (err) {
                resolve('false');
            }

            globalVarObj.fname = item['fname'];
            globalVarObj.mname = item['mname'];
            globalVarObj.lname = item['lname'];

            models.jobrole.findOne({'jobRoleId': roleId}, function (error, searchRole) {
                if (error) {
                    resolve('false');
                }
                globalVarObj.jobRoleName = searchRole['jobRoleName'];
                // globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  has accepted the position "+globalVarObj.jobRoleName+" and Job ID ="+jobId.substring(3);

                globalVarObj.msgBody = globalVarObj.fname + " " + globalVarObj.mname + " " + globalVarObj.lname + "  has accepted the " + globalVarObj.jobRoleName + " Position. Job ID " + jobId.substring(3);

                //-------------
                let insertedValue = {
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }


                models.employer.updateOne({'employerId': employerId},
                    {$push: {"notification": insertedValue}},
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {

                        //----------

                        simplemailercontroller.viaGmail({
                            receiver: emailId,
                            subject: '----JOB APPLICATION----',
                            msg: globalVarObj.msgBody
                        }, (mailerErr, nodeMailerResponse) => {
                            if (mailerErr) {
                                resolve('false');
                            } else {
                                resolve('true');

                            }
                        })

                    }) // END models.candidate.updateOne
            })
        })
    })
}

//==========================================================================


async function sendNotificationEmail(emailId, candidateId, jobId, roleId, employerId) {
    let globalVarObj = {};

    //===================================================================
    let locationName = "";

    let fetchJobStatus = await employerJobBusiness.fetchJob(jobId);

    if (fetchJobStatus.isError == false) {
        locationName = fetchJobStatus.jobDetails[0].jobDetails.roleWiseAction[0].locationName;
    }
    //====================================================================

    return new Promise(resolve => {
        models.candidate.findOne({'candidateId': candidateId}, function (err, item) {
            if (err) {
                resolve('false');
            }

            globalVarObj.fname = item['fname'];
            globalVarObj.mname = item['mname'];
            globalVarObj.lname = item['lname'];

            models.jobrole.findOne({'jobRoleId': roleId}, function (error, searchRole) {
                if (error) {
                    resolve('false');
                }
                globalVarObj.jobRoleName = searchRole['jobRoleName'];
                // globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  has applied for position "+globalVarObj.jobRoleName+" and Job ID ="+jobId.substring(3);


                globalVarObj.msgBody = globalVarObj.fname + " " + globalVarObj.mname + " " + globalVarObj.lname + "  has applied for the " + globalVarObj.jobRoleName + " position at " + locationName + " location. Job ID " + jobId.substring(3);


                //-------------
                let insertedValue = {
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }


                models.employer.updateOne({'employerId': employerId},
                    {$push: {"notification": insertedValue}},
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {

                        //----------

                        simplemailercontroller.viaGmail({
                            receiver: emailId,
                            subject: '----JOB APPLICATION----',
                            msg: globalVarObj.msgBody
                        }, (mailerErr, nodeMailerResponse) => {
                            if (mailerErr) {
                                resolve('false');
                            } else {
                                resolve('true');

                            }
                        })

                    }) // END models.candidate.updateOne
            })
        })
    })
}


function sendCheckinNotificationEmail(emailId, candidateId, jobId, roleId, checkinTime, date, destinationName, jobtype, inout) {
    let globalVarObj = {};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId': candidateId}, function (err, item) {
            if (err) {
                resolve('false');
            }
            globalVarObj.fname = item['fname'];
            globalVarObj.mname = item['mname'];
            globalVarObj.lname = item['lname'];

            models.jobrole.findOne({'jobRoleId': roleId}, function (error, searchRole) {
                if (error) {
                    resolve('false');
                }
                globalVarObj.jobRoleName = searchRole['jobRoleName'];

                //globalVarObj.msgBody=globalVarObj.fname+" "+globalVarObj.mname+" "+ globalVarObj.lname+"  have checked " + inout +" successfully. Date : "+date+" Candidate Location : "+ destinationName +" Checkin Time : "+checkinTime+" Job Type : "+jobtype+" Role Name : "+globalVarObj.jobRoleName+" and Job ID : "+jobId.substring(3);

                globalVarObj.msgBody = globalVarObj.fname + " " + globalVarObj.mname + " " + globalVarObj.lname + " has checked " + inout + " successfully. Checkin Time: " + checkinTime + ", Date: " + date + ". Position: " + globalVarObj.jobRoleName + ". Candidate Location: " + destinationName + ". Job Type: " + jobtype + ". Job ID: " + jobId.substring(3);

                simplemailercontroller.viaGmail({
                    receiver: emailId,
                    subject: '----Checked In Logs----',
                    msg: globalVarObj.msgBody
                }, (mailerErr, nodeMailerResponse) => {
                    if (mailerErr) {
                        resolve('false');
                    } else {
                        resolve('true');
                    }
                })
            })
        })
    })
}

function sendCheckinNotificationSms(mobileNo, candidateId, jobId, roleId, checkinTime, date, destinationName, jobtype, inout) {
    let globalVarObj = {};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId': candidateId}, function (err, item) {
            if (err) {
                resolve('false');
            }
            globalVarObj.fname = item['fname'];
            globalVarObj.mname = item['mname'];
            globalVarObj.lname = item['lname'];
            models.jobrole.findOne({'jobRoleId': roleId}, function (error, searchRole) {
                if (error) {
                    resolve('false');
                }
                globalVarObj.jobRoleName = searchRole['jobRoleName'];
                globalVarObj.notification = globalVarObj.fname + " " + globalVarObj.mname + " " + globalVarObj.lname + "  have checked" + inout + " successfully. Date : " + date + " Candidate Location : " + destinationName + " Checkin Time : " + checkinTime + " Job Type : " + jobtype + " Role Name : " + globalVarObj.jobRoleName + " and Job ID : " + jobId.substring(3);
                sendsmscontroller.Sendsms(mobileNo, globalVarObj.notification, (twilioError, twilioResult) => {
                    console.log(twilioResult)
                    if (twilioError) {
                        resolve('false');
                    } else {
                        resolve('true');
                    }
                })
            })
        })
    })
}

function sendNotificationSms(mobileNo, candidateId, jobId, roleId, employerId) {
    let globalVarObj = {};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId': candidateId}, function (err, item) {
            if (err) {
                resolve('false');
            }

            globalVarObj.fname = item['fname'];
            globalVarObj.mname = item['mname'];
            globalVarObj.lname = item['lname'];
            models.jobrole.findOne({'jobRoleId': roleId}, function (error, searchRole) {
                if (error) {
                    resolve('false');
                }
                globalVarObj.jobRoleName = searchRole['jobRoleName'];
                globalVarObj.notification = globalVarObj.fname + " " + globalVarObj.mname + " " + globalVarObj.lname + "  has applied for position " + globalVarObj.jobRoleName + " and Job ID =" + jobId.substring(3);

                //-------------
                let insertedValue = {
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.notification
                }
                console.log(globalVarObj.notification);


                models.employer.updateOne({'employerId': employerId},
                    {$push: {"notification": insertedValue}},
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) {

                        //----------
                        sendsmscontroller.Sendsms(mobileNo, globalVarObj.notification, (twilioError, twilioResult) => {
                            //console.log(twilioResult)
                            if (twilioError) {
                                resolve('false');
                            } else {
                                resolve('true');
                            }
                        })
                    }) // END models.candidate.updateOne
            })
        })
    })
}


function getAllDates(startDate, endDate) {
    return new Promise(resolve => {
        resolve(startDate);
    })
}

function isInteger(x) {
    return typeof x === "number" && isFinite(x) && Math.floor(x) === x;
}

function isFloat(x) {
    return !!(x % 1);
}
