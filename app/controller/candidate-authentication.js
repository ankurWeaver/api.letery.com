const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/mailer');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
var Request = require("request");
const utilitycontroller = require('../service/utility');
const sendsmscontroller = require('../service/TwilioSms');
var Request = require("request");
const https = require('https');
const config = require('../config/env');
module.exports = {
    /**
     * @abstract 
     * Signup Candidate
     * while adding Candidate check if Candidate already exists or not in authenticate table
     * if exists add candidate and save some details of candidate table in authenticate table for future checking
    */
    signup: (req, res, next) => {
        let type = "CANDIDATE";
        let autouserid;
        AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
            autouserid = data;
        })
        let fname = req.body.fname ? req.body.fname : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - First Name",

        });
        let mname = req.body.mname ? req.body.mname : '';
        let lname = req.body.lname ? req.body.lname : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Last Name"
        });
        let email = req.body.emailOrPhone ? req.body.emailOrPhone : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Email Address or Phone "
        });
        let password = req.body.password ? req.body.password : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Password "
        });
        let signedUpVia = req.body.signedUpVia ? req.body.signedUpVia : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Signed up via"
        });
        let allLowerCase = email.toLowerCase();
        if (!req.body.password || !req.body.emailOrPhone || !req.body.fname || !req.body.lname || !req.body.signedUpVia) { return; }
        utilitycontroller.checkEmailOrPhone(email, (err, data) => {
            if (err) {
                res.json({
                    isError: true,
                    message: 'Invalid Format of Mobile/Email Id'
                })
            }

            switch (data) {
                case '1': searchType = 'EmailId'

                    break;
                case '2':
                    searchType = 'mobileNo'
                    break;
                default: break;
            }

        })
        try {
            let querywhere = {
                'EmailId': email
            };
            let d = new Date();
            let timeAnddate = d.toISOString();
            let insertquery = {
                '_id': new mongoose.Types.ObjectId(), 'candidateId': autouserid,
                'fname': fname, 'mname': mname,
                'lname': lname, [searchType]: allLowerCase, 'password': password,
                'userType': type,
                'memberSince': timeAnddate, 'Status.isVerified': false,
                'Status.isActive': true, 'Status.isApprovedByAot': false,
                'Status.signedUpVia': signedUpVia, "distance": 4, 'geolocation': {
                    "type": "Point",
                    "coordinates": [0, 0]
                }
            };


            models.authenticate.findOne({ $or: [{ 'EmailId': allLowerCase }, { 'mobileNo': email }] }).exec(function (err, item) {


                if (item == null) {
                    models.candidate.create(insertquery, function (error, data) {
                        if (error) {
                            console.log('during candidate creation error', error)
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            })
                        }
                        else {
                            let candidateId = data['candidateId'];

                            let insertparams =
                            {
                                '_id': new mongoose.Types.ObjectId(),
                                'userId': candidateId,
                                'EmailId': (searchType == 'EmailId' ? allLowerCase : ''),
                                'mobileNo': (searchType == 'mobileNo' ? email : ''),
                                'type': type
                            }
                            models.authenticate.create(insertparams, function (error, item) {
                                console.log('during auth creation error', error)
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    })
                                }
                                else {
                                    if (searchType == 'EmailId') {
                                        Request.post({
                                            "headers": { "content-type": "application/json" },
                                            // "url": `${config.app.FORMATTED_URL}/api/candidate/send-verification-link`,
                                            // "url": `https://api.letery.com/api/candidate/send-verification-link`,
                                            "url": `${config.app.FORMATTED_URL_NEW}/api/candidate/send-verification-link`,
                                            "body": JSON.stringify({
                                                "email": allLowerCase,

                                            })
                                        }, (error, response, body) => {
                                            if (error) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                })
                                            }
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['1014'],
                                                statuscode: 1014,
                                                details: data
                                            })
                                        });
                                    }
                                    else {
                                        var code = Math.floor((Math.random() * 999999) + 111111);
                                        var finalcode = code.toString().substring(0, 6)

                                        sendsmscontroller.Sendsms(email, finalcode, (err, callResponse) => {
                                            if (err) {
                                                console.log(err)
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1016'],
                                                    statuscode: 1016,
                                                    details: null
                                                })
                                            }
                                            if (callResponse) {
                                                models.timeTOLive.create({ '_id': new mongoose.Types.ObjectId(), 'email': '', 'mobileNo': email, 'isUsed': false, 'otp': finalcode, 'type': 'CANDIDATE', 'tocken': '', 'isclicked': false }, function (error, inserteditem) {

                                                    if (error) {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: error
                                                        });
                                                    }
                                                    else {

                                                        res.json({
                                                            isError: false,
                                                            message: errorMsgJSON['ResponseMsg']['1015'],
                                                            statuscode: 1015,
                                                            details: data
                                                        })

                                                    }
                                                })
                                            }
                                        })
                                    }

                                }
                            })
                        }
                    })
                }
                else {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1010'],
                        statuscode: 1010,
                        details: null
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    /**
    * @abstract 
    * Send Otp to Candidate via nodemailer
    * After successfully send message insert otp  and message in Otp Document
    * Before inserting check if email exists update old otp with new one and if
    * email does not exist insert new email and otp in Otp Document
   */
    sendOtp: (req, res, next) => {
        let code = Math.floor((Math.random() * 999999) + 111111);
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        if (!req.body.email) { return; }
        mailercontroller.viaGmail({ receiver: email, subject: 'Seleckt Otp', msg: code }, (err, data) => {
            if (err) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                })
            }
            else {
                var querywhere = { 'EmailId': email };
                models.Otp.findOne(querywhere, function (error, item) {
                    if (error) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    else {
                        if (item == null) {
                            console.log('candidate email address does not exist. time to insert')
                            let insertquery = {
                                '_id': new mongoose.Types.ObjectId(),
                                'EmailId': email,
                                'otp': code
                            }
                            models.Otp.create(insertquery, function (err, data) {
                                if (err) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: data

                                    })
                                }
                            })
                        }
                        else {
                            console.log('candidate email address exists. time to update')
                            let querywhere = { 'EmailId': email }
                            let querywith = {
                                'otp': code
                            }
                            models.Otp.updateOne(querywhere, querywith, function (err, response) {
                                if (err) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (response.nModified == 1) {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                    }
                })
            }
        })
    },
    /**
    * @abstract 
    * Verify Otp
    * Match otp with email in Otp Document
   */
    verifyOtp: (req, res, next) => {
        let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Mobile No "
        });
        let otp = req.body.otp ? req.body.otp : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Otp"
        });
        if (!req.body.mobileNo || !req.body.otp) { return; }
        try {
            let aggrQuery = [
                {
                    $match: {
                        'mobileNo': mobileNo,
                        'otp': otp
                    }
                },
                {
                    $project: {
                        '_id': 0,
                    }
                },
                {
                    $group: {
                        _id: null,
                        isUsed: {
                            $first: "$isUsed"
                        },
                        results: {
                            $push: '$$ROOT'
                        }
                    }
                },

                {
                    $project: {
                        userDetails: {
                            $cond: {
                                if: { $eq: ['$isUsed', false] },
                                then: "$results",
                                else: []
                            }
                        }
                    }
                }
            ];
            models.timeTOLive.aggregate(aggrQuery).exec((err, item1) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    })
                }
                else {
                    if (item1.length == 0) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    } else {
                        if (!item1[0].userDetails.length) {
                            res.json({
                                isError: true,
                                message: 'Otp is not valid.',
                                statuscode: 200,
                                details: null
                            })
                        } else {
                            models.timeTOLive.updateOne({ 'mobileNo': mobileNo }, { 'isUsed': true }, function (updatederror, updatedItem) {
                                if (updatederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (updatedItem.nModified == 1) {
                                        models.candidate.updateOne({ 'mobileNo': mobileNo }, { 'Status.isVerified': true }, function (candidateerror, candidateItem) {
                                            if (candidateerror) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                            else {
                                                if (candidateItem.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: item1
                                                    })
                                                }
                                                else {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Change Password
     * First check if candidate exists or not by checking email in Candidate Document
     * if exists update old password with new one 
    */
    changePassword: (req, res, next) => {
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        let password = req.body.password ? req.body.password : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Password " });

        if (!req.body.password || !req.body.email) { return; }
        let querywhere = {
            'EmailId': email,

        };
        models.candidate.findOne(querywhere, function (err, item) {
            if (item == null) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1003'],
                    statuscode: 1003,
                    details: null
                })
            }
            else {
                let querywith = {
                    'password': password,
                };
                models.candidate.updateOne(querywhere, querywith, function (error, response) {
                    if (error) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    else {
                        if (response.nModified == 1) {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: null
                            })
                        }
                        else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                    }
                })
            }
        })
    },
    /**
    * @abstract 
    * Send verification Link via mail for Change Password
    * First check if candidate exists or not by checking email in Candidate Document
    * if exists create a link append with random generated tocken and candidateId and send it with mail attachment
    * if mail send is successful store tocken in candidate document.when link is clicked in mail '/api/verify' is called in app.js and check if tocken is same or not. if same redirect to reset password page. 
   */
    sendVerificationLink: (req, res, next) => {
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        if (!req.body.email) { return; }
        var querywhere = { 'EmailId': email };
        models.candidate.findOne(querywhere, function (error, item) {
            if (error) {
                console.log(error)
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }
            else {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1003'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let candidateId = item['candidateId'];
                    let fname = item['fname'];
                    let mname = item['mname'];
                    let lname = item['lname']
                    const tokgen = new TokenGenerator(512, TokenGenerator.BASE62);
                    let rand = tokgen.generate()
                    var transporter = nodemailer.createTransport({
                        service:'gmail',
                        auth: {
                          user: 'arindam.bhattacharyya@pkweb.in',
                          pass: 'Arindam@weavers'
                        }
                    });
                    //let link="http://localhost:3132/api"+"/verify?id="+rand+"&canref="+candidateId;
                    // let link="http://localhost:3132/api"+"/verify?id="+rand;
                    // let link = `${config.app.FORMATTED_URL}/api/candidate/verify-candidate?id=${rand}`;
                    // let link=`https://api.letery.com/api/candidate-profile/verify-candidate?id=`+rand;
                    let link=`${config.app.FORMATTED_URL_NEW}/api/candidate-profile/verify-candidate?id=`+rand;

                    // let messageBody = "Hello," + " " + fname + ' ' + mname + ' ' + lname +", Welcome to Seleckt Staff. Your application is being processed and a member of the team will be in touch with you as soon as possible to let you know what the next steps are.";
                    
                    let messageBody = "Welcome to Seleckt Staff. Your application is being processed and a member of the team will be in touch with you as soon as possible to let you know what the next steps are.";
 
                    const mailoption = {
                        from: 'Seleckt<arindam.bhattacharyya@pkweb.in>',
                        to: email,
                        // html: messageBody +"<br> Please Click on the link to verify your Account.<br><a clicktracking=off href=" + link + ">Click here to verify</a>",
                        
                        // html: messageBody +"<br> Welcome to Seleckt Staff, where you're able to fill your positions at the click of a button. Your registration is being processed and a member of our sales team will be in touch with you as soon as possible. <br><a clicktracking=off href=" + link + ">Click here to verify</a>",
                    
                        html: messageBody +" <br><a clicktracking=off href=" + link + ">Click here to verify</a>",
                        
                        subject: 'Account Verification'
                    };
                    transporter.sendMail(mailoption, (err, info) => {
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1006'],
                                statuscode: 1006,
                                details: null
                            });
                        }
                        else {
                            let querywith = {
                                'Status.confirmationTocken': rand,
                            };
                            models.candidate.updateOne(querywhere, querywith, function (error, response) {
                                if (error) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (response.nModified == 1) {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                    })
                }
            }
        })
    },
    /**
     * @abstract 
     * Signup Via Social
     * According to signedup type collect details of candidate
     * check if candidate exists or not in database using email 
     * if exist update candidate document vaue with social details other wise insert.
    */
    signupViaSocial: (req, res, next) => {
        let type = "CANDIDATE";
        let gloabalvarobj = {};
        // let globalVAr;
        let autouserid;
        AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
            autouserid = data;
        })
        let d = new Date();
        let timeAnddate = d.toISOString();
        let signedUpVia = req.body.signedUpVia ? req.body.signedUpVia : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Sign up via " });
        let signedUpObj = req.body.signedUpObj ? req.body.signedUpObj : '';
        let code = req.body.code ? req.body.code : '';
        let NewToken = token_gen({
            'candidateId': autouserid,
            'userType': 'CANDIDATE'
        });
        console.log(NewToken)

        try {
            switch (signedUpVia) {
                case 'facebook':
                    if (signedUpObj.photoUrl) {
                        gloabalvarobj.picFromSocial = signedUpObj.photoUrl;
                    } else {
                        gloabalvarobj.picFromSocial = "";
                    }
                    gloabalvarobj.firstnameFromSocial = signedUpObj && signedUpObj.firstName ? signedUpObj.firstName : '';
                    gloabalvarobj.lastnameFromSocial = signedUpObj && signedUpObj.lastName ? signedUpObj.lastName : '';
                    gloabalvarobj.emailFromSocial = signedUpObj && signedUpObj.email ? signedUpObj.email : '';
                    gloabalvarobj.faceBookId = signedUpObj && signedUpObj.id ? signedUpObj.id : '';
                    gloabalvarobj.linkedinId = '';
                    let insertquery = {
                        '_id': new mongoose.Types.ObjectId(), 'candidateId': autouserid, 'EmailId': gloabalvarobj.emailFromSocial, 'userType': type,
                        'memberSince': timeAnddate, 'Status.isVerified': true, 'Status.isActive': true, 'Status.signedUpVia': signedUpVia,
                        'fname': gloabalvarobj.firstnameFromSocial, 'lname': gloabalvarobj.lastnameFromSocial, 'profilePic': gloabalvarobj.picFromSocial,
                        'socialLoginDetails.facebookId': gloabalvarobj.faceBookId, 'socialLoginDetails.linedinId': gloabalvarobj.linkedinId, 'Status.isApprovedByAot': false,
                        'geolocation': { "type": "Point", "coordinates": [0, 0] }
                    };
                    models.authenticate.findOne({ 'EmailId': gloabalvarobj.emailFromSocial }, function (err, item) {
                        console.log(item)
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (item == null) {
                                models.candidate.create(insertquery, function (error, data) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        })
                                    }
                                    else {
                                        let insertparams = { '_id': new mongoose.Types.ObjectId(), 'userId': data['candidateId'], 'EmailId': gloabalvarobj.emailFromSocial, 'type': 'CANDIDATE' }
                                        models.authenticate.create(insertparams, function (error, item) {
                                            if (error) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                })
                                            }
                                            else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['204'],
                                                    statuscode: 204,
                                                    details:data, 
                                                    token: token_gen({
                                                            'candidateId': data['candidateId'],
                                                            'userType': 'CANDIDATE'
                                                        })
                                                    
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                            else {
                                models.candidate.findOne({ 'EmailId': gloabalvarobj.emailFromSocial }, function (err, searcheditem) {
                                        
                                    // searcheditem.tocken = NewToken;
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['1010'],
                                        statuscode: 1010,
                                        details: searcheditem,
                                        token: token_gen({
                                                'employerId': searcheditem.candidateId,
                                                'userType': 'CANDIDATE'
                                            })
                                           
                                    })

                                })
                            }
                        }

                    })
                    break;
                case 'linkedin':
                    var tokenPromise = new Promise((resolve, reject) => {
                        Request.post({
                            "headers": {
                                "content-type": "application/x-www-form-urlencoded",
                                "Accept": "application/json"
                            },
                            "url": `https://www.linkedin.com/oauth/v2/accessToken?client_id=86q6nae7g7x7i6&client_secret=4jg49HENZk1IuJaG&grant_type=authorization_code&redirect_uri=${config.app.frontendUri}/redirect&code=${code}`

                        }, (error, response, body) => {
                            if (error) {
                                reject(1)
                            }
                            let finalResult = JSON.parse(body)
                            if (finalResult.error) {
                                reject(1)
                            }
                            else {
                                gloabalvarobj.access_token = finalResult.access_token;
                                resolve(gloabalvarobj.access_token)
                            }
                        })
                    });
                    tokenPromise.then(async resolvedData => {
                        if (resolvedData == 1) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            })
                        }
                        var resultWithName = await getNameFromLinkedin(resolvedData)
                        gloabalvarobj.firstnameFromSocial = resultWithName.firstnameFromSocial,
                            gloabalvarobj.lastnameFromSocial = resultWithName.lastnameFromSocial,
                            console.log('hochhe', gloabalvarobj.firstnameFromSocial)
                        console.log('hochhe', gloabalvarobj.lastnameFromSocial)
                        var resultWithEmail = await getEmailFromLinkedin(resolvedData)
                        gloabalvarobj.emailFromSocial = resultWithEmail.emailFromSocial,
                            console.log('hochhe', gloabalvarobj.emailFromSocial)
                        gloabalvarobj.picFromSocial = "";
                        gloabalvarobj.faceBookId = "",
                            gloabalvarobj.linkedinId = '';
                        let insertquery = {
                            '_id': new mongoose.Types.ObjectId(), 'candidateId': autouserid, 'EmailId': gloabalvarobj.emailFromSocial, 'userType': type,
                            'memberSince': timeAnddate, 'Status.isVerified': true, 'Status.isActive': true, 'Status.signedUpVia': signedUpVia,
                            'fname': gloabalvarobj.firstnameFromSocial, 'lname': gloabalvarobj.lastnameFromSocial, 'profilePic': gloabalvarobj.picFromSocial,
                            'socialLoginDetails.facebookId': gloabalvarobj.faceBookId, 'socialLoginDetails.linedinId': gloabalvarobj.linkedinId, 'Status.isApprovedByAot': false,
                            'geolocation': { "type": "Point", "coordinates": [0, 0] }
                        };
                        models.authenticate.findOne({ 'EmailId': gloabalvarobj.emailFromSocial }, function (err, item) {
                            console.log(item)
                            if (err) {
                                console.log(1)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: err
                                });
                            }
                            else {
                                if (item == null) {
                                    models.candidate.create(insertquery, function (error, data) {
                                        if (error) {
                                            console.log(2)
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: error
                                            })
                                        }
                                        else {
                                            var _token = token_gen({
                                                'employerId': data['candidateId'],
                                                'userType': 'CANDIDATE'
                                            });
                                            let insertparams = { '_id': new mongoose.Types.ObjectId(), 'userId': data['candidateId'], 'EmailId': gloabalvarobj.emailFromSocial, 'type': 'CANDIDATE' }
                                            models.authenticate.create(insertparams, function (error, item) {
                                                if (error) {
                                                    console.log(3)
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: error
                                                    })
                                                }
                                                else {
                                                    // Request.post({
                                                    //     "headers": { "content-type": "application/json" },
                                                    //     "url": `${config.app.FORMATTED_URL}/api/common/verify-token`,
                                                    //     "body": {}
                                                    // }, (error, response, body) => {})
                                                    console.log('tocken', _token)
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: data,
                                                        token: _token
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                                else {
                                    models.candidate.findOne({ 'EmailId': gloabalvarobj.emailFromSocial }, function (err, searcheditem) {
                                        // Request.post({
                                        //     "headers": { "content-type": "application/json" },
                                        //     "url": `${config.app.FORMATTED_URL}/api/common/verify-token`,
                                        //     "body": {}
                                        // }, (error, response, body) => {})
                                        console.log('........', searcheditem.candidateId)
                                        console.log('tocken', token_gen({
                                            'employerId': searcheditem.candidateId,
                                            'userType': 'CANDIDATE'
                                        }))
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: searcheditem,

                                            token: token_gen({
                                                'employerId': searcheditem.candidateId,
                                                'userType': 'CANDIDATE'
                                            })

                                        })

                                    })

                                }
                            }

                        })
                    }).catch(err => {
                        res.send(err)
                    })
                    break;
                default: break;
            }
        }
        catch (error) {
            console.log(4)
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    /*********************************** Profile section starts**************************************************/

    /**
    * @abstract 
    * Change Password in Profile Section
    * Check if Any candidate exists by checking oldPassword and email Address
    * if exists update old password with new one 
   */
    profilechangePassword: (req, res, next) => {
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        let oldpassword = req.body.oldpassword ? req.body.oldpassword : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " -Old  Password " });
        let newpassword = req.body.newpassword ? req.body.newpassword : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " -New Password " });
        if (!req.body.email || !req.body.oldpassword || !req.body.newpassword) { return; }
        let querywhere = { 'EmailId': email, 'password': oldpassword };
        models.candidate.findOne(querywhere, function (error, item) {
            console.log(error)
            if (item == null) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1031'],
                    statuscode: 1031,
                    details: null
                })
            }
            else {
                let querywith = {
                    'password': newpassword,
                };
                models.candidate.updateOne(querywhere, querywith, function (error, response) {
                    if (error) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });
                    }
                    else {
                        if (response.nModified == 1) {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: null
                            })
                        }
                        else {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                    }
                })
            }
        })
    },
    addmultipleAddress: (req, res, next) => {
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        let querywhere = { 'email': email };
        let language = {
            'lang': 'eng',
            'rank': '1'
        };
        models.candidate.updateOne(querywhere, { $push: { 'language': language } }, function (error, response) {
            console.log(response)
            console.log(error)
        })
    }

}

function getNameFromLinkedin(resolvedData) {
    let globalVarObj = {};
    return new Promise(resolve => {
        const accessToken = resolvedData;
        const options = {
            host: 'api.linkedin.com',
            path: '/v2/me',
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'cache-control': 'no-cache',
                'X-Restli-Protocol-Version': '2.0.0'
            }
        };
        const profileRequest = https.request(options, function (res) {
            let data = '';
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                const profileData = JSON.parse(data);
                const userData = JSON.stringify(profileData, 0, 2);
                const profileDataDb = JSON.parse(userData);
                globalVarObj.firstnameFromSocial = profileDataDb.localizedFirstName;
                globalVarObj.lastnameFromSocial = profileDataDb.localizedLastName;
                resolve(globalVarObj);
            });
        });
        profileRequest.end();

    })
}

function getEmailFromLinkedin(resolvedData) {
    let globalVarObj = {};
    return new Promise(resolve => {
        const accessToken = resolvedData;
        const options = {
            host: 'api.linkedin.com',
            path: '/v2/emailAddress?q=members&projection=(elements*(handle~))',
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'cache-control': 'no-cache',
                'X-Restli-Protocol-Version': '2.0.0'
            }
        };
        const profileRequest = https.request(options, function (res) {
            let data = '';
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                const profileData = JSON.parse(data);
                const userData = JSON.stringify(profileData, 0, 2);
                const profileDataDb = JSON.parse(userData);
                globalVarObj.emailFromSocial = profileDataDb.elements[0]['handle~'].emailAddress
                resolve(globalVarObj);

            });
        });
        profileRequest.end();

    })
}

