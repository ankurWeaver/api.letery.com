const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const sendsmscontroller = require('../service/TwilioSms');
const simplemailercontroller = require('../service/mailer');
var http = require('http');
const config =  require('../config/env');
const fcm = require('../service/fcm');
let newRoleArrDb=[];

//------------------------------------------------------------------------------
const employerJobBusiness = require("../businessLogic/employer-job-business");
const jobProfileBusiness = require("../businessLogic/job-profile-business");
const candidateProfileBusiness = require("../businessLogic/candidate-profile-business");
const mailerBusiness = require("../businessLogic/mailer-business");
//------------------------------------------------------------------------------
const { relativeTimeRounding } = require('moment');




async function checkCandidatePresenceForApi(candidateId, jobId, roleId){
    var isCandidateAvailable = true;
    let jobArr = [];
    // console.log("roleId", roleId)

    let fetchCandidateJobScheduleStatus = await employerJobBusiness.fetchCandidateJobSchedule(candidateId);  
    jobArr = fetchCandidateJobScheduleStatus.response;

    let fetchJobTimeDetailStatus = await employerJobBusiness.fetchJobTimeDetail(jobId, roleId);

    let jobStartDateTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.jobStartDateTimeStamp : 0;
    let jobEndDateTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.jobEndDateTimeStamp : 0;
    jobStartDateTimeStamp = Number(jobStartDateTimeStamp);
    jobEndDateTimeStamp = Number(jobEndDateTimeStamp);

    // console.log("jobArr", jobArr);
    // console.log("jobStartDateTimeStamp", jobStartDateTimeStamp);
    // console.log("jobEndDateTimeStamp", jobEndDateTimeStamp);
   
    let k;
    let startTimeStamp;
    let endTimeStamp;
    let fourHrsInTimeStamp = 4*60*60*1000; // four hour in time stamp
    fourHrsInTimeStamp = Number(fourHrsInTimeStamp);
    
    for (k = 0; k < jobArr.length; k++) {
        startTimeStamp = Number(jobArr[k].jobStartDateTimeStamp); // it is the start time of individual job
        endTimeStamp = Number(jobArr[k].jobEndDateTimeStamp) + fourHrsInTimeStamp; // it is the end time of individual job
        // console.log("startTimeStamp", startTimeStamp);
        // console.log("endTimeStamp", endTimeStamp);

        if ( (jobStartDateTimeStamp >= startTimeStamp) && (jobStartDateTimeStamp <= endTimeStamp)) {
            isCandidateAvailable = false; // candidate not available
            break;
        }

        if ( (jobEndDateTimeStamp >= startTimeStamp) && (jobEndDateTimeStamp <= endTimeStamp)) {
            isCandidateAvailable = false; // candidate not available
            break;
        }   
    } // END  for (i = 0; i < jobArr.length; i++) {
    
    return isCandidateAvailable;
} // END function sendNotificationEmail(candidateId, jobId){


    

module.exports = {

    //=========================================================================================================

    /*
        This Function is written to fetch cancelled job list
    */
    fetchedCancelledJobs: async (req, res, next) => {
        try {
            let cancelledBy = req.body.cancelledBy ? req.body.cancelledBy : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Cancelled By"
            });

            let page = req.body.page ? (parseInt(req.body.page == 0 ? 0 : parseInt(req.body.page) - 1)) : 0;
            let perPage = req.body.perPage ? parseInt(req.body.perPage) : 20;

            if (!req.body.cancelledBy) { return; }

            let matchStmt =  {
                $match: { 
                    'cancelledJobs.cancelledBy': "Candidate" 
                }	
            };

            if (cancelledBy == "Candidate") {
                matchStmt =  {
                    $match: { 
                        'cancelledJobs.cancelledBy': 'Candidate' 
                    }	
                };
            }
            
            if (cancelledBy == "Employer") {
                matchStmt =  {
                    $match: { 
                        'cancelledJobs.cancelledBy': { $ne: 'Candidate' } 
                    }
                };
            }

            let aggrQry = [   
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1
                    }
                },

                // {
                //     $match: { 
                //         'cancelledJobs.cancelledBy': cancelledBy 
                //     }	
                // },
                // matchStmt,

                {
                    $lookup:
                      {
                        from: "employers",
                        localField: "employerId",
                        foreignField: "employerId",
                        as: "employerDetails"
                      }
                 },

                 {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'employerType': '$employerDetails.employerType',
                        'EmailId': '$employerDetails.EmailId',
                        'companyName': '$employerDetails.companyName',
                        'individualContactName': '$employerDetails.individualContactName'
                    }
                },

                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'employerType': { $arrayElemAt: [ '$employerType', 0 ] },
                        'EmailId': { $arrayElemAt: [ '$EmailId', 0 ] },
                        'companyName': { $arrayElemAt: [ '$companyName', 0 ] },
                        'individualContactName': { $arrayElemAt: [ '$individualContactName', 0 ] }
                    }
                },

                { $unwind: '$cancelledJobs' },
                
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'roleId': '$cancelledJobs.roleId',
                        'currentTimestamp': '$cancelledJobs.currentTimestamp',
                        'candidateId': '$cancelledJobs.candidateId',
                        'cancelledBy': '$cancelledJobs.cancelledBy',
                        'employerType': 1,
                        'EmailId': 1,
                        'companyName': 1,
                        'individualContactName':1
                    }
                },
                
                matchStmt,
                {
                    $lookup:
                      {
                        from: "candidates",
                        localField: "candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                      }
                },

                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'roleId': 1,
                        'currentTimestamp': 1,
                        'candidateId': 1,
                        'employerType': 1,
                        'EmailId': 1,
                        'companyName': 1,
                        'individualContactName':1,
                        'fname': '$candidateDetails.fname',
                        'mname': '$candidateDetails.mname',
                        'lname': '$candidateDetails.lname'
                    }
                },

                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'roleId': 1,
                        'currentTimestamp': 1,
                        'candidateId': 1,
                        'employerType': 1,
                        'EmailId': 1,
                        'companyName': 1,
                        'individualContactName':1,
                        'fname': { $arrayElemAt: [ '$fname', 0 ] },
                        'mname': { $arrayElemAt: [ '$mname', 0 ] },
                        'lname': { $arrayElemAt: [ '$lname', 0 ] },
                    }
                },

                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'roleId': 1,
                        'currentTimestamp': 1,
                        'candidateId': 1,
                        'employerType': 1,
                        'EmailId': 1,
                        'companyName': 1,
                        'individualContactName':1,
                        'fname': 1,
                        'mname': 1,
                        'lname': 1,
                    }
                },

                {
                    $lookup:
                      {
                        from: "jobroles",
                        localField: "roleId",
                        foreignField: "jobRoleId",
                        as: "roleDetails"
                      }
                },

                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'roleId': 1,
                        'currentTimestamp': 1,
                        'candidateId': 1,
                        'employerType': 1,
                        'EmailId': 1,
                        'companyName': 1,
                        'individualContactName':1,
                        'fname': 1,
                        'mname': 1,
                        'lname': 1,
                        'jobRoleName': '$roleDetails.jobRoleName'
                    }
                },

                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'cancelledJobs': 1,
                        'roleId': 1,
                        'currentTimestamp': 1,
                        'candidateId': 1,
                        'employerType': 1,
                        'EmailId': 1,
                        'companyName': 1,
                        'individualContactName':1,
                        'fname': 1,
                        'mname': 1,
                        'lname': 1,
                        'jobRoleName': { $arrayElemAt: [ '$jobRoleName', 0 ] }
                    }
                },
                
            
                { $sort : { 'currentTimestamp' : -1 } },

                {
                    $group: {
                        _id: null,
                        total: { $sum: 1 },
                        results: { $push : '$$ROOT' }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
                
            
            ];    

            let fetchJob = await jobProfileBusiness.fetchJob(aggrQry);

            res.json({
                cancelledBy: cancelledBy,
                fetchJob: fetchJob
            });
            return;

        } catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    /*
        This Function is written to dlete or cancel a particular job (role of a job)
        only for all candidate by employer
    */
    cancelJobForByEmployer: async (req, res, next) => {
        try {
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });

            if (!req.body.jobId || !req.body.roleId) { return; }

            let adminEmail = config.app.adminEmail;

             // Here, we are going to fetch the details of job
             let aggrQry = [
                {
                    $match: { 
                        'jobId': jobId 
                    },	
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleWiseAction': '$jobDetails.roleWiseAction',
                        'hireList': 1,
                        'approvedTo': 1
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleDetails': {
                            $filter: {
                               input: "$roleWiseAction",
                               as: "roleWiseAction",
                               cond: { $eq: [ "$$roleWiseAction.roleId", roleId ] }
                            }
                        },
                        'hireList': {
                            $filter: {
                               input: "$hireList",
                               as: "hireList",
                               cond: { $eq: [ "$$hireList.roleId", roleId ] }
                            }
                        },
                        'approvedTo': {
                            $filter: {
                               input: "$approvedTo",
                               as: "approvedTo",
                               cond: { $eq: [ "$$approvedTo.roleId", roleId ] }
                            }
                        }

                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleName': '$roleDetails.roleName',
                        'jobType': '$roleDetails.jobType',
                        'hireList': 1,
                        'approvedTo': 1
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleName': { $arrayElemAt: [ '$roleName', 0 ] },
                        'jobType': { $arrayElemAt:  [ '$jobType', 0 ] },
                        'hireList': 1,
                        'approvedTo': 1
                    }
                }
            ];
            
            let fetchJobStatus = await jobProfileBusiness.fetchJob(aggrQry);

            if (fetchJobStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let hireList = fetchJobStatus.response[0].hireList ? fetchJobStatus.response[0].hireList : [];
            let approvedTo = fetchJobStatus.response[0].approvedTo ? fetchJobStatus.response[0].approvedTo : [];
            let roleName = fetchJobStatus.response[0].roleName ? fetchJobStatus.response[0].roleName : '';
            let candidateIdList = []; // this hold the ids of hired candidate,
            let offeredCandidateIdList = [];
            let currentDate = new Date();
            let currentTimestamp = currentDate.getTime();

            let i;
            let cancelledJobs = [];
            for (i = 0; i < hireList.length; i++) {
                candidateIdList.push(hireList[i].candidateId)
                cancelledJobs.push({
                    "roleId" : roleId,
                    "jobId" : jobId,
                    "candidateId": hireList[i].candidateId,
                    "currentDate": currentDate,
                    "currentTimestamp": currentTimestamp,
                    "cancelledBy": "employerAllCandidate"
                })
            } // END for (i = 0; i < hireList.length; i++) {

            for (i = 0; i < approvedTo.length; i++) {
                offeredCandidateIdList.push(approvedTo[i].candidateId)
            } // END for (i = 0; i < approvedTo.length; i++) {

            

            let aggrCandidate = [
                {
                    $match: { 
                        'candidateId': { $in: candidateIdList } 
                    },	
                },
                {
                    $project: {
                        '_id': 0,
                        'EmailId': 1
                    }
                },
            ];

            let fetchCandidateDetailsStatus = await candidateProfileBusiness.fetchCandidateDetails(aggrCandidate);

            if (fetchCandidateDetailsStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let emailIdList = fetchCandidateDetailsStatus.response ? fetchCandidateDetailsStatus.response: [];
            let candidateIds = '';
            for (i = 0; i < emailIdList.length; i++) {
                console.log(emailIdList[i].EmailId)
                if (i == 0) {
                    candidateIds = emailIdList[i].EmailId;
                } else {
                    candidateIds = candidateIds+', '+emailIdList[i].EmailId; 
                }
                
            } // END for (i = 0; i < emailIdList.length; i++) {
 
            let updateHiredJobListOfManyCandidate = await candidateProfileBusiness.updateHiredJobListOfManyCandidate({
                "candidateIdList": candidateIdList,
                "jobId": jobId,
                "roleId": roleId
            })

            // res.json({
            //     fetchJobStatus: fetchJobStatus,
            //     offeredCandidateIdList: offeredCandidateIdList,
            //     updateHiredJobListOfManyCandidate: updateHiredJobListOfManyCandidate,
            //     cancelledJobs: cancelledJobs
            // });
            // return;

            if (updateHiredJobListOfManyCandidate.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            updateManyCandidate = candidateProfileBusiness.updateManyCandidate({
                "candidateId":{ $in: offeredCandidateIdList } 
            },{ 
                "$pull": { "jobOffers": { "roleId" : roleId, "jobId" : jobId } }
            })

            let updateWhere = {
                "jobId": jobId,
                "jobDetails.roleWiseAction": { $elemMatch: { roleId: { $eq: roleId } } }
            }; 
    
            let updateWith = { 
                $set: { "jobDetails.roleWiseAction.$.isDeleted" : "YES" },
                $pull: { 
					"hireList": { "roleId": roleId }
                }, 
                $pull: { 
					"approvedTo": { "roleId": roleId }
                }, 
                $push: { 
					"cancelledJobs": cancelledJobs
				}
            };

            let updateJobRoleToCancelStatus = await jobProfileBusiness.updateJobRoleToCancel(updateWith, updateWhere);

            if (updateJobRoleToCancelStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }
            
            // Sending Notification
            let notificationMsg = `The employer has cancelled the job having jobId ${jobId} for the position ${roleName}`;
            candidateProfileBusiness.updateNotificationForManyCandidate({
                "candidateIdList": candidateIdList,
                "notificationMsg": notificationMsg
            });

            // sending mail
            let emailMessage = `The employer has cancelled the job having jobId ${jobId} for the position ${roleName}`;
            mailerBusiness.sendEmailToCandidateForJobCancellation({
                'emailMessage': emailMessage,
                'emailSubject': 'Regarding the job cancellation',
                'candidateEmailId': candidateIds
            });

            // Here, we are sending a mail to admin
            mailerBusiness.sendEmailToEmployerForJobCancellation({
                'emailMessage': emailMessage,
                'emailSubject': 'Regarding the job cancellation',
                'empEmailId': adminEmail
            });

            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: null
            });
            return;

        } catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    /*
        This Function is written to cancel a particular job
        only for individual candidate by employer
    */
    cancelJobForCandidateByEmployer: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            }); 

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });

            let presentDate = req.body.today ? req.body.today : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present date"
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - local Time Zone"
            });

            if (!req.body.candidateId || !req.body.jobId || !req.body.roleId || !req.body.today || !req.body.presentTime) { return; }

            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);
            let presentTimeStamp = dateObj.realDateTimeStamp;

            // adding 24 hrs to timestamp
            let timeStampAfterDay = presentTimeStamp + (24*60*60*1000);

            let adminEmail = config.app.adminEmail;

            // Here, we are going to fetch the details of job
            let aggrQry = [
                {
                    $match: { 
                        'jobId': jobId 
                    },	
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleWiseAction': '$jobDetails.roleWiseAction'
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'roleDetails': {
                            $filter: {
                               input: "$roleWiseAction",
                               as: "roleWiseAction",
                               cond: { $eq: [ "$$roleWiseAction.roleId", roleId ] }
                            }
                        }
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'startRealDate': '$roleDetails.startRealDate',
                        'startRealDateUtc': '$roleDetails.startRealDateUtc',
                        'startRealDateTimeStamp': '$roleDetails.startRealDateTimeStamp',
                        'roleName': '$roleDetails.roleName',
                        'jobType': '$roleDetails.jobType',
                    }
                },
                {
                    $project: {
                        'employerId': 1,
                        'jobId': 1,
                        'startRealDate':  { $arrayElemAt: [ '$startRealDate', 0 ] },
                        'startRealDateUtc': { $arrayElemAt: [ '$startRealDateUtc', 0 ] },
                        'startRealDateTimeStamp': { $arrayElemAt: [ '$startRealDateTimeStamp', 0 ] },
                        'roleName': { $arrayElemAt: [ '$roleName', 0 ] },
                        'jobType': { $arrayElemAt:  [ '$jobType', 0 ] },
                    }
                }
            ];
            
            let fetchJobStatus = await jobProfileBusiness.fetchJob(aggrQry);

            let jobStartRealDateTimeStamp = Number(fetchJobStatus.response[0].startRealDateTimeStamp);
    
            if (jobStartRealDateTimeStamp < timeStampAfterDay) {
                res.json({
                    isError: true,
                    message: "Job Can not be cancelled as it will be started within 24 hours",
                    statuscode: 204,
                    details: null
                });

                return;
            }

            if (fetchJobStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let employerId = fetchJobStatus.response[0].employerId ? fetchJobStatus.response[0].employerId : '';
            let roleName = fetchJobStatus.response[0].roleName ? fetchJobStatus.response[0].roleName : '';

            let arggrQryForEmp = [
                {
                    $match: { 
                        'employerId': employerId 
                    },	
                },
                {
                    $project: {
                        'employerId': 1,
                        'EmailId': 1
                    }
                }
            ];

            let fetchEmployerDetailsForJobCancellationStatus = await employerJobBusiness.fetchEmployerDetailsForJobCancellation(arggrQryForEmp);

            if (fetchEmployerDetailsForJobCancellationStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let empEmailId = fetchEmployerDetailsForJobCancellationStatus.response[0].EmailId ? fetchEmployerDetailsForJobCancellationStatus.response[0].EmailId : '';

            let arggrQryForCandidate = [
                {
                    $match: { 
                        'candidateId': candidateId 
                    },	
                },
                {
                    $project: {
                        'candidateId': 1,
                        "fname": 1,
                        "mname": 1,
                        "lname": 1,
                        "EmailId": 1,
                    }
                }
            ];

            let fetchCandidateDetailsStatus = await candidateProfileBusiness.fetchCandidateDetails(arggrQryForCandidate);

            if (fetchCandidateDetailsStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let fname = fetchCandidateDetailsStatus.response[0].fname ? fetchCandidateDetailsStatus.response[0].fname : '';
            let mname = fetchCandidateDetailsStatus.response[0].mname ? fetchCandidateDetailsStatus.response[0].mname : '';
            let lname = fetchCandidateDetailsStatus.response[0].lname ? fetchCandidateDetailsStatus.response[0].lname : '';
            let candidateEmailId = fetchCandidateDetailsStatus.response[0].EmailId ? fetchCandidateDetailsStatus.response[0].EmailId : '';
           
            let updateHiredJobListOfCandidateStatus = await candidateProfileBusiness.updateHiredJobListOfCandidate({
                "candidateId": candidateId,
                "roleId":roleId,
                "jobId":jobId,
                "cancelledBy" : "employerOneCandidate"
            });

            if (updateHiredJobListOfCandidateStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let cancelJobForParticularCandidateInHirelistStatus = await jobProfileBusiness.cancelJobForParticularCandidateInHirelist({
                "candidateId": candidateId,
                "roleId":roleId,
                "jobId":jobId,
                "cancelledBy": "employerOneCandidate"
            });

            if (cancelJobForParticularCandidateInHirelistStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            let notificationMsg = `The employer has cancelled the job having jobId ${jobId} for the position ${roleName}`;
            
            candidateProfileBusiness.updateNotificationForCandidate({
                "notificationMsg": notificationMsg,
                "candidateId": candidateId
            });

            let emailMessage = `The employer has cancelled the job having jobId ${jobId} for the position ${roleName}`;
            let emailSubject = 'Regarding the job cancellation';
            mailerBusiness.sendEmailToCandidateForJobCancellation({
                'emailMessage': emailMessage,
                'emailSubject': emailSubject,
                'candidateEmailId': candidateEmailId
            });

            // Here, we are sending a mail to admin
            mailerBusiness.sendEmailToEmployerForJobCancellation({
                'emailMessage': emailMessage,
                'emailSubject': emailSubject,
                'empEmailId': adminEmail
            });
        
            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: null
            });
            return;
        } catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },



     /*
        This function is written to fetch the list of completed jobs of the candidate   
    */
   fetchAuthenticityOfEmployer: async (req, res, next) => {
        try {
            
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"                
            });

            if (!req.body.employerId) { return; }

            let fetchEmployerDetailsStatus = await employerJobBusiness.fetchEmployerDetailsForAuthentication(employerId);

            if (fetchEmployerDetailsStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            if (fetchEmployerDetailsStatus.responseLength < 0) {
                res.json({
                    isError: false,
                    message: "No data found",
                    statuscode: 204,
                    details: null
                });

                return;
            }

            let isEmployerAuthentic = "True";
            let message = 'Employer is valid';

            let employerType = fetchEmployerDetailsStatus.response[0].employerType ? fetchEmployerDetailsStatus.response[0].employerType : '';

            if (employerType != 'INDIVIDUAL') {
                var companyName = fetchEmployerDetailsStatus.response[0].companyName ? fetchEmployerDetailsStatus.response[0].companyName : '';
  
                if (companyName == '') {
                    isEmployerAuthentic = "False";
                    message = "Employer has to enter the company name";

                    res.json({
                        isError: false,
                        message: "Profile not complete",
                        statuscode: 204,
                        details: {
                            isEmployerAuthentic: isEmployerAuthentic,
                            message: message
                        }
                    });

                    return;
                }

                var companyNo = fetchEmployerDetailsStatus.response[0].companyNo ? fetchEmployerDetailsStatus.response[0].companyNo : '';
  
                if (companyNo == '') {
                    isEmployerAuthentic = "False";
                    message = "Employer has to enter the company number";

                    res.json({
                        isError: false,
                        message: "Profile not complete",
                        statuscode: 204,
                        details: {
                            isEmployerAuthentic: isEmployerAuthentic,
                            message: message
                        }
                    });

                    return;
                }

                var hiringCapacity = fetchEmployerDetailsStatus.response[0].hiringCapacity ? fetchEmployerDetailsStatus.response[0].hiringCapacity : '';
  
                if (hiringCapacity == '') {
                    isEmployerAuthentic = "False";
                    message = "Employer has to enter the hiring capacity";

                    res.json({
                        isError: false,
                        message: "Profile not complete",
                        statuscode: 204,
                        details: {
                            isEmployerAuthentic: isEmployerAuthentic,
                            message: message
                        }
                    });

                    return;
                }

            } // END if (employerType != 'INDIVIDUAL') {
            
            let profilePic = fetchEmployerDetailsStatus.response[0].profilePic ? fetchEmployerDetailsStatus.response[0].profilePic : '';
  
            if (profilePic == '') {
                isEmployerAuthentic = "False";
                message = "Employer has to upload profile picture";

                res.json({
                    isError: false,
                    message: "Profile not complete",
                    statuscode: 204,
                    details: {
                        isEmployerAuthentic: isEmployerAuthentic,
                        message: message
                    }
                });

                return;
            }

            let contactTelephoneNo = fetchEmployerDetailsStatus.response[0].contactTelephoneNo ? fetchEmployerDetailsStatus.response[0].contactTelephoneNo : '';
            
            if (contactTelephoneNo == '') {
                isEmployerAuthentic = "False";
                message = "Employer has to enter contact telephone number";

                res.json({
                    isError: false,
                    message: "Profile not complete",
                    statuscode: 204,
                    details: {
                        isEmployerAuthentic: isEmployerAuthentic,
                        message: message
                    }
                });

                return;
            }

            let address = fetchEmployerDetailsStatus.response[0].address ? fetchEmployerDetailsStatus.response[0].address : '';
            
            if (address == '') {
                isEmployerAuthentic = "False";
                message = "Employer has to enter address";

                res.json({
                    isError: false,
                    message: "Profile not complete",
                    statuscode: 204,
                    details: {
                        isEmployerAuthentic: isEmployerAuthentic,
                        message: message
                    }
                });

                return;
            }

            let mobileNo = fetchEmployerDetailsStatus.response[0].mobileNo ? fetchEmployerDetailsStatus.response[0].mobileNo : '';
            
            if (mobileNo == '') {
                isEmployerAuthentic = "False";
                message = "Employer has to enter contact mobile number";

                res.json({
                    isError: false,
                    message: "Profile not complete",
                    statuscode: 204,
                    details: {
                        isEmployerAuthentic: isEmployerAuthentic,
                        message: message
                    }
                });

                return;
            }


            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: {
                    isEmployerAuthentic: isEmployerAuthentic,
                    message: message
                }
            });
            return;
        


        }  catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }   
    },



    //=========================================================================================================

    testCheckCandidatePresenceForApi: async (req, res, next) => {
        try {

            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Candidate Id"                
            });

            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -job Id"                
            });

            //==================================================================

            var isCandidateAvailable = true;
            let candidateJobDetailList = [];
        
            let fetchCandidateJobScheduleStatus = await employerJobBusiness.fetchCandidateJobSchedule(candidateId);
            
            candidateJobDetailList = fetchCandidateJobScheduleStatus.response;
            let fetchJobTimeDetailStatus = await employerJobBusiness.fetchJobTimeDetail(jobId);
        
            let jobStartTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.startTimeStamp : 0;
            let jobSecondTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.secondTimeStamp : 0;
        
            let jobActualStartTime = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.actualStartTime : 0;
            jobActualStartTime = Number(jobActualStartTime);

            let jobActualEndTime = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.actualEndTime : 0;
            jobActualEndTime = Number(jobActualEndTime);

            //Get Date of the job
            var d = new Date(jobStartTimeStamp);
            var fullYrJob = d.getFullYear();
            var monthJob = d.getMonth();
            var dayJob = d.getDay();
           
            //************ */


            let i;
            let startTimeStamp;
            let secondTimeStamp;

            let actualStartTime;
            let actualEndTime;
        
            for (i = 0; i < candidateJobDetailList.length; i++) {
        
                startTimeStamp = candidateJobDetailList[i].startTimeStamp;
                secondTimeStamp = candidateJobDetailList[i].secondTimeStamp;

                actualStartTime = candidateJobDetailList[i].actualStartTime;
                actualStartTime = Number(actualStartTime);

                actualEndTime = candidateJobDetailList[i].actualEndTime;
                actualEndTime = Number(actualEndTime);

                //Get Date of each job of candidate
                var d = new Date(startTimeStamp);
                var fullYr = d.getFullYear();
                var month = d.getMonth();
                var day = d.getDay();
                
                // Here we comparing the date
                if ( (jobStartTimeStamp >= startTimeStamp) && (jobStartTimeStamp <= secondTimeStamp)) {
                    isCandidateAvailable = false; // candidate not available
                    break;
                }

                if ( (jobSecondTimeStamp >= startTimeStamp) && (jobSecondTimeStamp <= secondTimeStamp)) {
                    isCandidateAvailable = false; // candidate not available
                    break;
                }
        
               //==END=====
        
                if (isCandidateAvailable == true) {  // comparing time
                    if ((fullYr == fullYrJob) && (month == monthJob) && (day == dayJob)) {
                        if ( (jobActualStartTime >= actualStartTime) && (jobActualStartTime <= actualEndTime)) {
                            isCandidateAvailable = false; // candidate not available
                            break;
                        }
                
                        if ( (jobActualEndTime >= actualStartTime) && (jobActualEndTime <= actualEndTime)) {
                            isCandidateAvailable = false; // candidate not available
                            break;
                        }
                    } // END  if ((fullYr == fullYrJob) && (month == monthJob) && (day == dayJob)) {
                } // END if (isCandidateAvailable == false) {
            } // END  for (i = 0; i < candidateJobDetailList.length; i++) {
        
    
            //==================================================================

            res.json({
                fetchCandidateJobScheduleStatus:fetchCandidateJobScheduleStatus,
                fetchJobTimeDetailStatus: fetchJobTimeDetailStatus,
                jobActualStartTime:jobActualStartTime,
                jobActualEndTime: jobActualEndTime,
                isCandidateAvailable: isCandidateAvailable
            })
            return;



        }  catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }   

    },
    

    // This function is used to check whether the candidate is available for a particular job or not
    checkCandidatePresenceForJob: async (req, res, next) => {
            try {
    
                let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " -Candidate Id"                
                });
    
                let jobId = req.body.jobId ? req.body.jobId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " -job Id"                
                });

                let roleId = req.body.roleId ? req.body.roleId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " -role Id"                
                });
    
                var isCandidateAvailable = true; // this tag is used whether the candidate is available or not
    
    
                if (!req.body.candidateId || !req.body.jobId || !req.body.roleId) { return; }
    
                let fetchCandidateJobScheduleStatus = await employerJobBusiness.fetchCandidateJobSchedule(candidateId);

                if (fetchCandidateJobScheduleStatus.isError == true) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
    
                    return;
                }

                let jobArr = fetchCandidateJobScheduleStatus.response;

                // let candidateJobDetailList = fetchCandidateJobScheduleStatus.response;
                // let lastCancellationTime = 9999; 
            
                // // get the current time stamp
                // let currentDate = new Date();
                // let currentTimestamp = currentDate.getTime();
    
                // let dateOffset = (24*60*60*1000) * 2; //2 days (cooling Period)
                // let twoDayAgoTimeStamp = currentTimestamp - dateOffset;
                // let dateTwoDayBefore = new Date(twoDayAgoTimeStamp);
                
                // fetch Last Declined job time*****************************
                // let aggrQry = [
                //     {
                //         $match: { 
                //             'candidateId': candidateId 
                //         },	
                //     },
        
                //     { 
                //         $project: {
                //             "_id":0,
                //             "declinedJobs": 1,
                //             "lastCancellationTime": 1
                //         }
                //     },
        
                //     { $unwind: "$declinedJobs" },
        
                //     { 
                //         $sort : { 
                //             "declinedJobs.declinedTimeStamp" : -1 
                //         } 
                //     }
                // ];
    
                // let fetchCandidateInfoStatus = await employerJobBusiness.fetchCandidateInfo(aggrQry);
    
                // if (fetchCandidateInfoStatus.isError == true) {
                //     res.json({
                //         isError: true,
                //         message: errorMsgJSON['ResponseMsg']['404'],
                //         statuscode: 404,
                //         details: null
                //     });
    
                //     return;
                // }
    
                // let responseLength = fetchCandidateInfoStatus.responseLength ? fetchCandidateInfoStatus.responseLength : 0;
    
                // if (responseLength > 0) {
                //     lastCancellationTime = fetchCandidateInfoStatus.response[0].lastCancellationTime ? fetchCandidateInfoStatus.response[0].lastCancellationTime : 9999;
                
                //     if (lastCancellationTime != 9999) {
                //         lastCancellationTime = Number(lastCancellationTime);
                //         if (lastCancellationTime > twoDayAgoTimeStamp) {
                //             res.json({
                //                 isError: false,
                //                 message: errorMsgJSON['ResponseMsg']['200'],
                //                 statuscode: 200,
                //                 details: {
                //                     isCandidateAvailable: false
                //                 }
                //             });
            
                //             return;
                //         }
                //     }
                
                // } //End if (responseLength > 0) {
                // END // fetch Last Declined job time*****************************    
    
                let fetchJobTimeDetailStatus = await employerJobBusiness.fetchJobTimeDetail(jobId, roleId);

                if (fetchJobTimeDetailStatus.isError == true) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
    
                    return;
                }
    
                let jobStartDateTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.jobStartDateTimeStamp : 0;
                let jobEndDateTimeStamp = fetchJobTimeDetailStatus.response ? fetchJobTimeDetailStatus.response.jobEndDateTimeStamp : 0;
    
                let i;
                let startTimeStamp;
                let endTimeStamp;
                let fourHrsInTimeStamp = 4*60*60*1000;
                fourHrsInTimeStamp = Number(fourHrsInTimeStamp);

                let k;
                for (k = 0; k < jobArr.length; k++) {
                    startTimeStamp = Number(jobArr[k].jobStartDateTimeStamp); // it is the start time of individual job
                    endTimeStamp = Number(jobArr[k].jobEndDateTimeStamp) + fourHrsInTimeStamp; // it is the end time of individual job
                   
                    if ( (jobStartDateTimeStamp >= startTimeStamp) && (jobStartDateTimeStamp <= endTimeStamp)) {
                        isCandidateAvailable = false; // candidate not available
                        break;
                    }

                    if ( (jobEndDateTimeStamp >= startTimeStamp) && (jobEndDateTimeStamp <= endTimeStamp)) {
                        isCandidateAvailable = false; // candidate not available
                        break;
                    }
                } // END for (k = 0; k < jobArr.length; k++) {

                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: {
                        isCandidateAvailable: isCandidateAvailable
                    }
                });
                return;

            }  catch (error) {
                
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }   
    
    },


    /*
        This function is written to fetch the list of completed jobs of the candidate   
    */
    fetchCompletedJobOfCandidate: async (req, res, next) => {
        try {
            
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Candidate Id"                
            });

            let presentDate = req.body.today ? req.body.today : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Present Date"                
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Present Time"                
            });

            if (!req.body.candidateId || !req.body.today || !req.body.presentTime) { return; }

            
            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);
                
            let currentTimestamp = dateObj.realDateTimeStamp;

            let hiredJobDetails = [];

            let fetchJobOfCandidateStatus = await employerJobBusiness.fetchJobOfCandidate(candidateId, currentTimestamp);

            if (fetchJobOfCandidateStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }

            hiredJobDetails = fetchJobOfCandidateStatus.hiredJobDetails;            

            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: {
                    "hiredJobDetails": hiredJobDetails
                }
            });


        }  catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }   
    },

    /*
        This function is written to delete the notification sent by the employee
    */
    deleteNotificationforEmployer: (req, res, next) => {
        try{

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Employer Id"                
            });

            let notificationId = req.body.notificationId ? req.body.notificationId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Notification Id"                
            });

            if (!req.body.employerId || !req.body.notificationId) { return; }

            models.employer.updateOne({ "employerId": employerId }, 
                { "$pull": { "notification": { "_id": notificationId } } }, 
                { safe: true, multi: true }, 
                function (updatederror, updatedresponse) {
                          if (updatederror) {
                            res.json({
                              isError: true,
                              message: "Error Occur in deleting notification",
                              statuscode: 404,
                              details: null
                            })
                          }
                          else {
                            if (updatedresponse.nModified == 1) {
                              res.json({
                                isError: false,
                                message: "notification successfuly deleted",
                                statuscode: 200,
                                details: null
                              });
                            }
                            else {
                              res.json({
                                isError: false,
                                message: "Notification was not deleted",
                                statuscode: 405,
                                details: null
                              })

                            }
                          }
            }) // END models.employer.updateOne({ "employerId": employerId },

        }
        catch (error) {
            
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }     
    },    


    /*
        This function is written to fetch all the job related notification
        for a particular employer
    */
    fetchNotificationforEmployer: (req, res, next) => {
        try{

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Employer Id"                
            });

            let pageNo = req.body.pageNo ? (parseInt(req.body.pageNo == 0 ? 0 : parseInt(req.body.pageNo) - 1)) : 0;
            let perPage = req.body.perPage ? parseInt(req.body.perPage) : 10;

            if (!req.body.employerId) { return; }


            let aggrQuery = [
                {
                    $match: {
                        'employerId': employerId
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'notification': 1
                    }
                },
                {
                    $unwind : "$notification"
                },

                { 
                    $project: {
                        "notification": 1,
                        "generatedId": "$notification._id"
                    },

                },

                { $sort : { generatedId : -1} },

                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
         
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', pageNo * perPage , perPage
                            ]
                        }
                    }
                }
            ];

            models.employer.aggregate(aggrQuery).exec((err, response) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });

                    return;
                }

                if (response.length < 1) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['1009'],
                        statuscode: 1009,
                        details: null
                    });

                    return;
                }

                if (response.length > 0) {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: response
                    });

                    return;
                }
            })    

        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }     
    },    

    determineShiftnameLogic1: (req, res, next) => {

        let allSlotContainer=[];
        try{
            /*
            morning: 6.00-12.00
            Afternoon:12.00-18.00
            evening:18.00-24.00
            night:24.00-6.00
            */
           let startTime = req.body.startTime ? req.body.startTime : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Start Time"
            });
            let endTime = req.body.endTime ? req.body.endTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - End Time"
                });
            if (!req.body.startTime || !req.body.endTime) { return; }
            let givenStarttime=parseInt(startTime.replace('.',''));
            let givenEndtime=parseInt(endTime.replace('.',''));
            let startTimeExists='';
            let endTimeExists='';
            let lowerLimit=0;
            let upperLimit=0;
           models.timeslot.find({}, async function (err, item) {
               
                allSlotContainer=item;
                await allSlotContainer.map(time=>{
                    let startTime=parseInt(time.starttime.replace('.',''));
                    let endTime=parseInt(time.endTime.replace('.',''));
                    if(givenStarttime>=startTime && givenStarttime<endTime){
                        startTimeExists=time.slotName
                    }
                    if(givenEndtime>=startTime && givenEndtime<endTime){
                        endTimeExists=time.slotName
                    }
                })

                

                if(startTimeExists==''){
                    startTimeExists="Night" 
                }
                if(endTimeExists==''){
                    endTimeExists="Night" 
                }
                switch(startTimeExists){
                    case "Morning":lowerLimit=1;
                                    break;
                    case "Afternoon":lowerLimit=2;
                                        break;
                    case "Evening":lowerLimit=3;
                                    break;
                    case "Night":lowerLimit=4;
                                    break;
                }
                switch(endTimeExists){
                    case "Morning":upperLimit=1;
                                break;
                    case "Afternoon":upperLimit=2;
                                    break;
                    case "Evening":upperLimit=3;
                                break;
                    case "Night":upperLimit=4;
                                break;
                }

                //=====================
                //these code is written by ankur on  02-01-2020
                // if ( ( (givenStarttime >= 6)&&(givenStarttime < 12) ) && ( (givenEndtime >= 12)&&(givenEndtime < 24) )  ) {
                //     res.json({
                //         finalShift:"Full Day"
                //     })
                //     return;
                // }
                  

                //========================
                if(lowerLimit==upperLimit){
                    res.json({
                       finalShift:startTimeExists
                    })
                    return;
                }
                if(lowerLimit<upperLimit){
                    if(lowerLimit+1==upperLimit){
                        res.json({
                            finalShift:startTimeExists
                        })
                        return;
                    }
                    else{
                        res.json({
                            finalShift:"Full Day"
                        })
                        return;
                    }
                }
                else{
                    if(lowerLimit-2==upperLimit){
                        res.json({
                           finalShift:"Full Day"
                        })
                        return;
                    }
                    else if(lowerLimit-1==upperLimit){
                        res.json({
                           finalShift:"Full Day"
                        })
                        return;
                    }
                    else{
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            finalShift: startTimeExists
                          
                        })
                        return;
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    inviteToApply: (req, res, next) => {
        try{
            let inviteToApplyData = req.body.inviteToApplyData ? req.body.inviteToApplyData : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Invite to apply data",
                isError: true
            });
            inviteToApplyData.map(async tt => {
                console.log(tt['jobId'])
                console.log(tt['employerId'])
                console.log(tt['candidateId'])
                console.log(tt['roleId'])

               
                            let insertedValue={
                                "roleId":tt['roleId'],"employerId":tt['employerId'],
                                "jobId":tt['jobId'],"candidateId":tt['candidateId']
                            }
                            //---------------------------------------------------------------------
                            models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, 
                                { $push: { "invitationToApplyJobs":insertedValue} },  
                                function (updatederror, updatedresponse) {
                                    if (updatederror) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        }) 
                                    }
                                    if (updatedresponse.nModified == 1) {
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        }) 
                                    }
                            }) // END models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, 

                   
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    offerWithFavouriteOne: (req, res, next) => {
        try{
            let tier1pageno = req.body.tier1pageno ? (parseInt(req.body.tier1pageno == 0 ? 0 : parseInt(req.body.tier1pageno) - 1)) : 0;
            let tier1perpage = req.body.tier1perpage ? parseInt(req.body.tier1perpage) : 10;
            let tier2pageno = req.body.tier2pageno ? (parseInt(req.body.tier2pageno == 0 ? 0 : parseInt(req.body.tier2pageno) - 1)) : 0;
            let tier2perpage = req.body.tier2perpage ? parseInt(req.body.tier2perpage) : 10;
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;
            
            if (!req.body.jobId ) { return; }
            let jobaggrQuery

            if (roleName != '') {

                jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            'jobDetails.roleWiseAction.roleName': roleName, 
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },
                    {
                        $redact: {
                            $cond: {
                                if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction": "$jobDetails.roleWiseAction",

                        }
                    },
                    
                ]

            } else {
                if (roleId != '') {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                } else {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                }



            }

                    
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                   
                   
                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
            

                    if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
                        skilldbbackup = newSkillArrDb;
                       
                    }

                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 =  { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        invitationToApplyJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }};

                    let projectStmt2 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        skillDetails: 1,
                        profilePic:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },

                    }};

                    let groupStmt1 = {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                          
                            "candidateId": {
                                $first: "$candidateId"
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },

                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            "favouriteBy": {
                                $first: "$favouriteBy"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            }
                        }
                    };

                    let projectStmt3 = { $project: 
                        { 
                            _id:0,
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            isFavourite: 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
                           

                        }
                    };

                    let facetStmt1 = {
                        $facet:{
                            "TIER1":[
                                { $project: {
                                    jobId:1,
                                    roleId:1,
                                    candidateId: 1,
                                    employerId:1,
                                    fname: 1,
                                    profilePic:1,
                                    mname: 1,
                                    lname:1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    geolocation:1,
                                    rating:1,
                                    isOffered: 1,
                                    skillDetails:1,
                                    favouriteBy:1
            
                                 }},
                                 { "$unwind": "$favouriteBy" }, 
                                 {
                                    '$match': { 'favouriteBy': searchJob[0].employerId }
                                 },
                                 {
                                    $group: {
                                        "_id": {
                                            "candidateId": "$candidateId",
                                        },
                                      
                                        "candidateId": {
                                            $first: "$candidateId"
                                        },
                                        "employerId": {
                                            $first: "$employerId"
                                        },
                                       "fname": {
                                            $first: "$fname"
                                        },
                                        "mname": {
                                            $first: "$mname"
                                        },
                                        "lname": {
                                            $first: "$lname"
                                        },
                                        "isFavourite": {
                                            $first: "$isFavourite"
                                        },
                                        "geolocation": {
                                            $first: "$geolocation"
                                        },
                                        "rating": {
                                            $first: "$rating"
                                        },
                                        "jobId": {
                                            $first: "$jobId"
                                        },
                                        "roleId": {
                                            $first: "$roleId"
                                        },
                                        "favouriteBy": {
                                            $first: "$favouriteBy"
                                        },
                                        "isOffered": {
                                            $first: "$isOffered"
                                        },
                                        "profilePic": {
                                            $first: "$profilePic"
                                        },
                                        "skillDetails": {
                                            $first: "$skillDetails"
                                        }
                                    }
                                },
                                { $project: {
                                     "_id":0
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', tier1pageno * tier1perpage , tier1perpage
                                            ]
                                        }
                                    }
                                }


                            ],
                            "TIER2":[
                                { $project: {
                                    jobId:1,
                                    roleId:1,
                                    candidateId: 1,
                                    employerId:1,
                                    fname: 1,
                                    mname: 1,
                                    lname:1,
                                    geolocation:1,
                                    rating:1,
                                    isOffered: 1,
                                    skillDetails:1,
                                    favouriteBy:1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    profilePic:1,
                                    isSentInvitation:1
            
                                 }},
                                 { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                
                                 {
                                    '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                 },
                                 {
                                    $group: {
                                        "_id": {
                                            "candidateId": "$candidateId",
                                        },
                                      
                                        "candidateId": {
                                            $first: "$candidateId"
                                        },
                                        "profilePic": {
                                            $first: "$profilePic"
                                        },
                                        "employerId": {
                                            $first: "$employerId"
                                        },
                                       "fname": {
                                            $first: "$fname"
                                        },
                                        "mname": {
                                            $first: "$mname"
                                        },
                                        "lname": {
                                            $first: "$lname"
                                        },
                                        "isFavourite": {
                                            $first: "$isFavourite"
                                        },
                                        "geolocation": {
                                            $first: "$geolocation"
                                        },
                                        "rating": {
                                            $first: "$rating"
                                        },
                                        "jobId": {
                                            $first: "$jobId"
                                        },
                                        "roleId": {
                                            $first: "$roleId"
                                        },
                                        "favouriteBy": {
                                            $first: "$favouriteBy"
                                        },
                                        "isOffered": {
                                            $first: "$isOffered"
                                        },
                                        "isSentInvitation": {
                                            $first: "$isSentInvitation"
                                        },
                                        "skillDetails": {
                                            $first: "$skillDetails"
                                        }
                                    }
                                },
                                { $project: {
                                    "_id":0
                                   }
                                },
                                { '$match': 
                                    { 
                                        'rating': {"$gte":3},
                                        'isFavourite': false,
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', tier2pageno * tier2perpage , tier2perpage
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    };
                    
                    let aggrQuery=[
                        geoLocationStmt,
                        { "$unwind": "$skills" }, 
                        {
                            "$match": {  $and: [
                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                            ]}
                        },
                        // {
                        //     "$match": {  $and: [
                        //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                        //     ]}
                        // },  
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },
                        projectStmt1,
                        projectStmt2,
                        groupStmt1,
                        projectStmt3,
                        facetStmt1
                        
                    ];
                    
                    if (searchWithLocation == false) {
                        aggrQuery=[
                            
                            { "$unwind": "$skills" }, 
                            {
                                "$match": {  $and: [
                                    isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                ]}
                            },
                            // {
                            //     "$match": {  $and: [
                            //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                            //     ]}
                            // },  
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            projectStmt1,
                            projectStmt2,
                            groupStmt1,
                            projectStmt3,
                            facetStmt1
                            
                        ];
                    } // END  if (searchWithLocation == false) {

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //================

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //================

                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                                              
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        }
                        else{
                               
                            let data;
                            
                            if (response[0]['TIER1'].length > 0 || response[0]['TIER2'].length > 0) {
                                data = response;
                            } else {
                                
                                res.json({
                                    isError: false,
                                    message: 'No Data Found',
                                    statuscode: 204,
                                    details:null
                                });
                                return;
                                
                            } // END if (response[0]['TIER1'].length > 0 || response[0]['TIER2'].length > 0) {  
                         
                            if (data[0]['TIER1'].length > 0 || data[0]['TIER2'].length > 0) {
                                    
                                if(data[0]['TIER1'].length) {
                                    // ==============================
                                    let offerJobStatus;
                                    let tier1Data = data[0]['TIER1'][0].results;
                                    let tier1DataTag;
                                    let sendNotificationEmailStatus;
                                    let sendNotificationSmsStatus;
                                   

                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                        // we are checking whether the candidate is available for the job or not
                                        isCandidateAvailable = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId,jobId, roleId);
                                        tier1Data[tier1DataTag].isCandidateAvailable = isCandidateAvailable;

                                        if (isCandidateAvailable == true) {

                                            if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                

                                                if ( offerJobStatus.isError == false) {
                                                       
                                                    findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                                    if (findMailStatus.isError == false) {
                                                        tier1Data[tier1DataTag].isOffered = true;
                                                        if (findMailStatus.isItemExist == true) {
                                                                
                                                                if(findMailStatus.item.mobileNo == ""){
                                                                    sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                    
                                                                }
                                                                else{
                                                                    sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                    
                                                                }

                                                        } // END if (findMailStatus.isItemExist == true) {
                                                    } // END if (findMailStatus.isError == false) {
                                                } // END if ( offerJobStatus.isError == false) {     
                                            }  // END if (tier1Data[tier1DataTag].isOffered == false) {

                                        } // END if (isCandidateAvailable == false) {


                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    data[0]['TIER1'][0].results = tier1Data;

                                    // ================================ 
                                } // END if(data[0]['TIER1'].length) {
                                
                                if(data[0]['TIER2'].length) {

                                    var i;
                                    for (i=0; i < data[0]['TIER2'][0].results.length; i++) {
                                        isCandidateAvailable = await checkCandidatePresenceForApi(data[0]['TIER2'][0].results[i].candidateId,jobId, roleId);
                                        data[0]['TIER2'][0].results[i].isCandidateAvailable = isCandidateAvailable;
                                    }

                                } // END if(data[0]['TIER2'].length) {    



                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                               
                                    details:{
                                        "TIER1":data[0]['TIER1'][0],
                                        "TIER2":data[0]['TIER2'][0],
                                       
                                    }
                                })
                            }   


                            if (data[0]['TIER1'].length < 1 && data[0]['TIER2'].length < 1) {
                                res.json({
                                    isError: false,
                                    message: 'No Data Found',
                                    statuscode: 204,
                               
                                    details:null
                                })
                            }                         
                            
                        }// END else{
                    })
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //============================================================================
    offerWithFavouriteOneTier2: (req, res, next) => {
        try{
           
            let tier2pageno = req.body.tier2pageno ? (parseInt(req.body.tier2pageno == 0 ? 0 : parseInt(req.body.tier2pageno) - 1)) : 0;
            let tier2perpage = req.body.tier2perpage ? parseInt(req.body.tier2perpage) : 10;
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;
            
            if (!req.body.jobId ) { return; }
            
            let jobaggrQuery = [
                {
                    '$match': { 
                        'jobId': jobId,
                        
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction": "$jobDetails.roleWiseAction",

                    }
                },
                
            ];

                    
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
              
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                   
                   
                    let employerId=searchJob[0].employerId
                    let hireList=searchJob[0].hireList
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
            

                    if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
                        skilldbbackup = newSkillArrDb;
                       
                    }

                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 =  { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        selectedRole:1,
                        invitationToApplyJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        },
                        isEnrolled:{
                            $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                        },

                    }};

                    let projectStmt2 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        skillDetails: 1,
                        profilePic:1,
                        selectedRole:1,
                        isEnrolled:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },

                    }};

                    let groupStmt1 = {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                          
                            "candidateId": {
                                $first: "$candidateId"
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },

                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            "favouriteBy": {
                                $first: "$favouriteBy"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            },
                            "selectedRole": {
                                $first: "$selectedRole"
                            },
                            "isEnrolled": {
                                $first: "$isEnrolled"
                            }
                        }
                    };

                    let projectStmt3 = { $project: 
                        { 
                            _id:0,
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            isFavourite: 1,
                            selectedRole:1,
                            isEnrolled:1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
                           

                        }
                    };

                    let matchEnrolled = {
                        $match: { 
                            isEnrolled: "yes"
                        }
                    }

                    
                    
                    
                   

                    // ==================================================================
                    // This function is written to fetch Candidate List
                    async function fetchCandidateList(searchWithLocation, tier2pageno) { 

                        let facetStmt1 = {
                            $facet:{
                                "TIER2":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        favouriteBy:1,
                                        selectedRole:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        profilePic:1,
                                        isSentInvitation:1,
                                        isEnrolled:{
                                            $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                                        },
                
                                     }},
                                     { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                    
                                     {
                                        '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "isSentInvitation": {
                                                $first: "$isSentInvitation"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            },
                                            "selectedRole": {
                                                $first: "$selectedRole"
                                            },
                                            "isEnrolled": {
                                                $first: "$isEnrolled"
                                            }
                                        }
                                    },

                                    {
                                        $match: { 
                                            isEnrolled: "yes"
                                        }
                                    },
                
                                    { $project: {
                                        "_id":0
                                       }
                                    },
                                    { '$match': 
                                        { 
                                            'rating': {"$gte":3},
                                            'isFavourite': false,
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier2pageno * tier2perpage , tier2perpage
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        };


                        return new Promise(function(resolve, reject){

                                
                                let aggrQuery=[
                                    geoLocationStmt,
                                    { "$unwind": "$skills" }, 
                                    {
                                        "$match": {  $and: [
                                            isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                        ]}
                                    },
                                    // {
                                    //     "$match": {  $and: [
                                    //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                                    //     ]}
                                    // },  
                                    {
                                        $lookup: {
                                            from: "skills",
                                            localField: "skills",
                                            foreignField: "skillId",
                                            as: "skillDetails"
                                        }
                                    },
                                    projectStmt1,
                                    projectStmt2,
                                    groupStmt1,
                                    projectStmt3,
                                    matchEnrolled,
                                    facetStmt1
                                    
                                ];

                                if (searchWithLocation == false) {
                                    aggrQuery=[
                                        
                                        { "$unwind": "$skills" }, 
                                        {
                                            "$match": {  $and: [
                                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                            ]}
                                        },
                                        // {
                                        //     "$match": {  $and: [
                                        //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                                        //     ]}
                                        // },  
                                        {
                                            $lookup: {
                                                from: "skills",
                                                localField: "skills",
                                                foreignField: "skillId",
                                                as: "skillDetails"
                                            }
                                        },
                                        projectStmt1,
                                        projectStmt2,
                                        groupStmt1,
                                        projectStmt3,
                                        matchEnrolled,
                                        facetStmt1
                                        
                                    ];
                                } // END  if (searchWithLocation == false) {


                            models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                                              
                                if (error) {
                                    resolve({
                                        "isError": true,
                                        "statuscode": 404                                   
                                    });
                                   
                                } else {

                                    let data;
                                    var noOfAvailableCandidate = 0;

                                    if (response[0]['TIER2'].length > 0) {
                                        data = response;
                                    
                                        if(data[0]['TIER2'].length) {
        
                                            var i;
                                            for (i=0; i < data[0]['TIER2'][0].results.length; i++) {
                                                isCandidateAvailable = await checkCandidatePresenceForApi(data[0]['TIER2'][0].results[i].candidateId,jobId, roleId);
                                                data[0]['TIER2'][0].results[i].isCandidateAvailable = isCandidateAvailable;
        
                                                if (isCandidateAvailable == true) {
                                                    noOfAvailableCandidate++;
                                                }

                                                // it shows the hiring status of the candidate
                                                isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                                    "hireList": hireList,
                                                    "roleId": roleId,
                                                    "candidateId": data[0]['TIER2'][0].results[i].candidateId
                                                });
                                                data[0]['TIER2'][0].results[i].isHired = isHired;
                                            }
        
                                        } // END if(data[0]['TIER2'].length) {    
        
                                        resolve({
                                            "isError": false,
                                            "statuscode": 200,
                                            "noOfAvailableCandidate": noOfAvailableCandidate,
                                            "details":{    
                                                "TIER2":data[0]['TIER2'][0]
                                            }                                   
                                        });

                                        if(data[0]['TIER2'].length < 0) {
                                            resolve({
                                                "isError": false,
                                                "statuscode": 1009,
                                                "details": null                        
                                            });
                                        }    
    
                                    } else {

                                        resolve({
                                            "isError": false,
                                            "statuscode": 1009,
                                            "details": null                        
                                        });
                                        return;
                                        
                                    } // END if (response[0]['TIER1'].length > 0 ) { 
                                      
                                } // END else if (error) {
                            });    
                        }); // END return new Promise(function(resolve, reject){
                    } // END async function fetchCandidateList() { 
                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,             
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //================

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //================

                    var fetchCandidateListStatus; 
                
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier2pageno);
                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER2.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        tier2pageno++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (tier2pageno < noofpage) && (searchWithLocation == true) && (isError == false));

                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        tier2pageno = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier2pageno);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER2.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        statuscode: statuscode,
                        details: details
                    });
                    return;

                   
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //============================================================================

    offerWithFavouriteOneTier1: (req, res, next) => {
        try{
            let tier1pageno = req.body.tier1pageno ? (parseInt(req.body.tier1pageno == 0 ? 0 : parseInt(req.body.tier1pageno) - 1)) : 0;
            let tier1perpage = req.body.tier1perpage ? parseInt(req.body.tier1perpage) : 10;
            
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            // let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;
            
            if (!req.body.jobId || !req.body.roleId) { return; }
           
            let jobaggrQuery = [
                {
                    '$match': { 
                        'jobId': jobId,
                        
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction": "$jobDetails.roleWiseAction",

                    }
                },    
            ]
        
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
               
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                   
                   
                    let employerId=searchJob[0].employerId
                    let hireList=searchJob[0].hireList
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
            

                    if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
                        skilldbbackup = newSkillArrDb;
                       
                    }

                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 =  { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        selectedRole:1,
                        invitationToApplyJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        },
                        isEnrolled:{
                            $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                        },
                        

                    }};

                    let projectStmt2 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        skillDetails: 1,
                        profilePic:1,
                        selectedRole:1,
                        isEnrolled:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                       
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },

                    }};

                    let groupStmt1 = {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                          
                            "candidateId": {
                                $first: "$candidateId"
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },

                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            "favouriteBy": {
                                $first: "$favouriteBy"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            },
                            "selectedRole": {
                                $first: "$selectedRole"
                            },
                            "isEnrolled": {
                                $first: "$isEnrolled"
                            }
                        }
                    };

                    let projectStmt3 = { $project: 
                        { 
                            _id:0,
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            isFavourite: 1,
                            selectedRole: 1,
                            isEnrolled:1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
                           

                        }
                    };


                    let matchEnrolled = {
                        $match: { 
                            isEnrolled: "yes"
                        }
                    }

                    
                    
                    
                    

                    //=======================================================================
                    // THis function is written to fetch candidate list
                    async function fetchCandidateList(searchWithLocation, tier1pageno) { 
                        return new Promise(function(resolve, reject){
                                let facetStmt1 = {
                                    $facet:{
                                        "TIER1":[
                                            { $project: {
                                                jobId:1,
                                                roleId:1,
                                                candidateId: 1,
                                                employerId:1,
                                                fname: 1,
                                                profilePic:1,
                                                mname: 1,
                                                lname:1,
                                                isFavourite:{
                                                    $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                                },
                                                geolocation:1,
                                                rating:1,
                                                isOffered: 1,
                                                skillDetails:1,
                                                favouriteBy:1,
                                                selectedRole:1,
                                                isEnrolled:{
                                                    $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                                                },
                        
                                            }},
                                            { "$unwind": "$favouriteBy" }, 
                                            {
                                                '$match': { 'favouriteBy': searchJob[0].employerId }
                                            },
                                            {
                                                $group: {
                                                    "_id": {
                                                        "candidateId": "$candidateId",
                                                    },
                                                
                                                    "candidateId": {
                                                        $first: "$candidateId"
                                                    },
                                                    "employerId": {
                                                        $first: "$employerId"
                                                    },
                                                "fname": {
                                                        $first: "$fname"
                                                    },
                                                    "mname": {
                                                        $first: "$mname"
                                                    },
                                                    "lname": {
                                                        $first: "$lname"
                                                    },
                                                    "isFavourite": {
                                                        $first: "$isFavourite"
                                                    },
                                                    "geolocation": {
                                                        $first: "$geolocation"
                                                    },
                                                    "rating": {
                                                        $first: "$rating"
                                                    },
                                                    "jobId": {
                                                        $first: "$jobId"
                                                    },
                                                    "roleId": {
                                                        $first: "$roleId"
                                                    },
                                                    "favouriteBy": {
                                                        $first: "$favouriteBy"
                                                    },
                                                    "isOffered": {
                                                        $first: "$isOffered"
                                                    },
                                                    "profilePic": {
                                                        $first: "$profilePic"
                                                    },
                                                    "skillDetails": {
                                                        $first: "$skillDetails"
                                                    },
                                                    "selectedRole":{
                                                        $first: "$selectedRole"
                                                    },
                                                    "isEnrolled":{
                                                        $first: "$isEnrolled"
                                                    }
                                                }
                                            },

                                            {
                                                $match: { 
                                                    isEnrolled: "yes"
                                                }
                                            },

                                            { $project: {
                                                "_id":0
                                                }
                                            },
                                            {
                                                $group: {
                                                    _id: null,
                                                    total: {
                                                    $sum: 1
                                                    },
                                                    results: {
                                                    $push : '$$ROOT'
                                                    }
                                                }
                                            },
                                            {
                                                $project : {
                                                    '_id': 0,
                                                    'total': 1,
                                                    'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                                    'results': {
                                                        $slice: [
                                                            '$results', tier1pageno * tier1perpage , tier1perpage
                                                        ]
                                                    }
                                                }
                                            }
            
            
                                        ],
                                        
                                    }
                                };

                                let aggrQuery=[
                                    geoLocationStmt,
                                    { "$unwind": "$skills" }, 
                                    {
                                        "$match": {  $and: [
                                            isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                        ]}
                                    },
                                    // {
                                    //     "$match": {  $and: [
                                    //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                                    //     ]}
                                    // },  
                                    {
                                        $lookup: {
                                            from: "skills",
                                            localField: "skills",
                                            foreignField: "skillId",
                                            as: "skillDetails"
                                        }
                                    },
                                    projectStmt1,
                                    projectStmt2,
                                    groupStmt1,
                                    projectStmt3,
                                    // matchEnrolled,
                                    facetStmt1
                                    
                                ];

                                if (searchWithLocation == false) {
                                    aggrQuery=[
                                        
                                        { "$unwind": "$skills" }, 
                                        {
                                            "$match": {  $and: [
                                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                            ]}
                                        },
                                        // {
                                        //     "$match": {  $and: [
                                        //         isArrayEmpty?{'skills':{$in: newSkillArrDb}:{'skills':{$in: skilldbbackup }}
                                        //     ]}
                                        // },  
                                        {
                                            $lookup: {
                                                from: "skills",
                                                localField: "skills",
                                                foreignField: "skillId",
                                                as: "skillDetails"
                                            }
                                        },
                                        projectStmt1,
                                        projectStmt2,
                                        groupStmt1,
                                        projectStmt3,
                                        // matchEnrolled,
                                        facetStmt1
                                        
                                    ];
                                } // END  if (searchWithLocation == false) {
                            
                            models.candidate.aggregate(aggrQuery).exec( async (error, response) => {

                                if (error) {
                                    resolve({
                                        "isError": true,
                                        "statuscode": 404                                   
                                    });
                                } else {
                                            let data;
                                            var noOfAvailableCandidate = 0;
                                    
                                            if (response[0]['TIER1'].length > 0 ) {
                                                data = response;

                                                if(data[0]['TIER1'].length) {
                                                    // ==============================
                                                    let offerJobStatus;
                                                    let tier1Data = data[0]['TIER1'][0].results;
                                                    let tier1DataTag;
                                                    let sendNotificationEmailStatus;
                                                    let sendNotificationSmsStatus;
                                        

                                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                                        // we are checking whether the candidate is available for the job or not
                                                        isCandidateAvailable = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId,jobId, roleId);
                                                        tier1Data[tier1DataTag].isCandidateAvailable = isCandidateAvailable;
                                             
                                                        if (isCandidateAvailable == true) {
                                                            noOfAvailableCandidate++;

                                                            if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                                offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                                

                                                                if ( offerJobStatus.isError == false) {
                                                                    
                                                                    findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                                                    if (findMailStatus.isError == false) {
                                                                        tier1Data[tier1DataTag].isOffered = true;
                                                                        if (findMailStatus.isItemExist == true) {
                                                                                
                                                                                if(findMailStatus.item.mobileNo == ""){
                                                                                    sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                                    
                                                                                }
                                                                                else{
                                                                                    sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                                    
                                                                                }

                                                                        } // END if (findMailStatus.isItemExist == true) {
                                                                    } // END if (findMailStatus.isError == false) {
                                                                } // END if ( offerJobStatus.isError == false) {     
                                                            }  // END if (tier1Data[tier1DataTag].isOffered == false) {

                                                        } // END if (isCandidateAvailable == false) {

                                                        // it shows the hiring status of the candidate
                                                        isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                                            "hireList": hireList,
                                                            "roleId": roleId,
                                                            "candidateId": tier1Data[tier1DataTag].candidateId
                                                        });
                                                        tier1Data[tier1DataTag].isHired = isHired;

                                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                                    
                                                    data[0]['TIER1'][0].results = tier1Data;

                                                    resolve({
                                                        "isError": false,
                                                        "statuscode": 200,
                                                        "noOfAvailableCandidate": noOfAvailableCandidate,
                                                        "details":{
                                                            "TIER1":data[0]['TIER1'][0]
                                                        }                                   
                                                    });
                                                    
                                                    return; 
                                                } // END if(data[0]['TIER1'].length) { 

                                                if (data[0]['TIER1'].length < 1 ) {

                                                    resolve({
                                                        "isError": false,
                                                        "message": "No Data Found",
                                                        "statuscode": 1009,
                                                        "details":{
                                                            "TIER1":data[0]['TIER1'][0]
                                                        }                                   
                                                    });
                                                   
                                                    return; 
                                                }    
                                    
                                            } else { // END if (response[0]['TIER1'].length > 0 ) {

                                                resolve({
                                                    "isError": false,
                                                    "message": "No Data Found",
                                                    "statuscode": 1009,
                                                    "details":null                         
                                                });

                                                return;
                                        
                                            } // END ELSE if (response[0]['TIER1'].length > 0

                                } // END else if (err)

                            }) // END models.candidate.aggregate(aggrQuery).exec( async (error, response) => {    
                        }); // END  return new Promise(function(resolve, reject){
                    } // END async function fetchCandidateList() { 

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    
                    
                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //================
                    
                    var fetchCandidateListStatus; 
                   
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier1pageno);
                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        tier1pageno++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (tier1pageno < noofpage) && (searchWithLocation == true) && (isError == false));


                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        tier1pageno = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier1pageno);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    } // END if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                    

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        details: details
                    });
                    return;
                     
                    

                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                                              
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        } else {
                               
                                    let data;
                            
                                    if (response[0]['TIER1'].length > 0 ) {
                                        data = response;

                                        if(data[0]['TIER1'].length) {
                                            // ==============================
                                            let offerJobStatus;
                                            let tier1Data = data[0]['TIER1'][0].results;
                                            let tier1DataTag;
                                            let sendNotificationEmailStatus;
                                            let sendNotificationSmsStatus;
                                        

                                            for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                                // we are checking whether the candidate is available for the job or not
                                                isCandidateAvailable = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId,jobId, roleId);
                                                tier1Data[tier1DataTag].isCandidateAvailable = isCandidateAvailable;

                                                if (isCandidateAvailable == true) {

                                                    if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                        offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                        

                                                        if ( offerJobStatus.isError == false) {
                                                            
                                                            findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                                            if (findMailStatus.isError == false) {
                                                                tier1Data[tier1DataTag].isOffered = true;
                                                                if (findMailStatus.isItemExist == true) {
                                                                        
                                                                        if(findMailStatus.item.mobileNo == ""){
                                                                            sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                            
                                                                        }
                                                                        else{
                                                                            sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                            
                                                                        }

                                                                } // END if (findMailStatus.isItemExist == true) {
                                                            } // END if (findMailStatus.isError == false) {
                                                        } // END if ( offerJobStatus.isError == false) {     
                                                    }  // END if (tier1Data[tier1DataTag].isOffered == false) {

                                                } // END if (isCandidateAvailable == false) {


                                            } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                            data[0]['TIER1'][0].results = tier1Data;

                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                        
                                                details:{
                                                    "TIER1":data[0]['TIER1'][0],
                                            
                                                }
                                            })
                                            
                                            return; 
                                        } // END if(data[0]['TIER1'].length) { 

                                        if (data[0]['TIER1'].length < 1 ) {
                                            res.json({
                                                isError: false,
                                                message: 'No Data Found',
                                                statuscode: 204,
                                            
                                                details:null
                                            })

                                            return; 
                                        }    
                                    
                                    } else { // END if (response[0]['TIER1'].length > 0 ) {

                                    res.json({
                                        isError: false,
                                        message: 'No Data Found',
                                        statuscode: 204,
                                        details:null
                                    });
                                    return;
                                
                            } // END ELSE if (response[0]['TIER1'].length > 0   
                                                
                            
                        }// END else{


                    }) // END models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
               }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    //===========
    offerWithFavouriteOneAnd: (req, res, next) => {
        try{
            let tier1pageno = req.body.tier1pageno ? (parseInt(req.body.tier1pageno == 0 ? 0 : parseInt(req.body.tier1pageno) - 1)) : 0;
            let tier1perpage = req.body.tier1perpage ? parseInt(req.body.tier1perpage) : 10;
            let tier2pageno = req.body.tier2pageno ? (parseInt(req.body.tier2pageno == 0 ? 0 : parseInt(req.body.tier2pageno) - 1)) : 0;
            let tier2perpage = req.body.tier2perpage ? parseInt(req.body.tier2perpage) : 10;
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;

            let skillArrb = [];

            skillArrb = req.body.skillArrb ?  req.body.skillArrb : [];
           
            if (!req.body.jobId) { return; }
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }
           
            
            let jobaggrQuery

            if (roleName != '') {

                jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            'jobDetails.roleWiseAction.roleName': roleName, 
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },
                    {
                        $redact: {
                            $cond: {
                                if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                then: "$$DESCEND",
                                else: "$$PRUNE"
                            }
                        }
                    },
                    {
                        $project: {
                            "_id": 0,
                            "jobId": 1,
                            "employerId": 1,
                            "roleWiseAction": "$jobDetails.roleWiseAction",

                        }
                    },
                    
                ]

            } else {
                if (roleId != '') {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        {
                            $redact: {
                                $cond: {
                                    if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                                    then: "$$DESCEND",
                                    else: "$$PRUNE"
                                }
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                } else {
                    jobaggrQuery = [
                        {
                            '$match': { 
                                'jobId': jobId,
                                
                            }
                        },
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "jobDetails": 1,
                            }
                        },
                        
                        {
                            $project: {
                                "_id": 0,
                                "jobId": 1,
                                "employerId": 1,
                                "roleWiseAction": "$jobDetails.roleWiseAction",

                            }
                        },
                        
                    ]

                }



            }

                    
            models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    //console.log(searchJob[0].employerId)
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    //============
                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
                    

                    //=====================
                  
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        skills:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        invitationToApplyJobs:1,
                        isExist:{
                            $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                        },
                    }};

                    let projectStmt2 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        invitationToApplyJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }

                     }};


                    let projectStmt3 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        skillDetails: 1,
                        profilePic:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },

                    }}; 

                    let groupStmt1 =  {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                          
                            "candidateId": {
                                $first: "$candidateId"
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },

                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            "favouriteBy": {
                                $first: "$favouriteBy"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            }
                        }
                    };

                    let projectStmt4 = { $project: 
                        { 
                            _id:0,
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            isFavourite: 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
                           

                        }
                    };

                    let facetStmt1 = {
                        $facet:{
                            "TIER1":[
                                { $project: {
                                    jobId:1,
                                    roleId:1,
                                    candidateId: 1,
                                    employerId:1,
                                    fname: 1,
                                    profilePic:1,
                                    mname: 1,
                                    lname:1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    geolocation:1,
                                    rating:1,
                                    isOffered: 1,
                                    skillDetails:1,
                                    favouriteBy:1
            
                                 }},
                                 { "$unwind": "$favouriteBy" }, 
                                 {
                                    '$match': { 'favouriteBy': searchJob[0].employerId }
                                 },
                                 {
                                    $group: {
                                        "_id": {
                                            "candidateId": "$candidateId",
                                        },
                                      
                                        "candidateId": {
                                            $first: "$candidateId"
                                        },
                                        "employerId": {
                                            $first: "$employerId"
                                        },
                                       "fname": {
                                            $first: "$fname"
                                        },
                                        "mname": {
                                            $first: "$mname"
                                        },
                                        "lname": {
                                            $first: "$lname"
                                        },
                                        "isFavourite": {
                                            $first: "$isFavourite"
                                        },
                                        "geolocation": {
                                            $first: "$geolocation"
                                        },
                                        "rating": {
                                            $first: "$rating"
                                        },
                                        "jobId": {
                                            $first: "$jobId"
                                        },
                                        "roleId": {
                                            $first: "$roleId"
                                        },
                                        "favouriteBy": {
                                            $first: "$favouriteBy"
                                        },
                                        "isOffered": {
                                            $first: "$isOffered"
                                        },
                                        "profilePic": {
                                            $first: "$profilePic"
                                        },
                                        "skillDetails": {
                                            $first: "$skillDetails"
                                        }
                                    }
                                },
                                { $project: {
                                     "_id":0
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', tier1pageno * tier1perpage , tier1perpage
                                            ]
                                        }
                                    }
                                }


                            ],
                            "TIER2":[
                                { $project: {
                                    jobId:1,
                                    roleId:1,
                                    candidateId: 1,
                                    employerId:1,
                                    fname: 1,
                                    mname: 1,
                                    lname:1,
                                    geolocation:1,
                                    rating:1,
                                    isOffered: 1,
                                    skillDetails:1,
                                    favouriteBy:1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    profilePic:1,
                                    isSentInvitation:1
            
                                 }},
                                 { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                
                                 {
                                    '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                 },
                                 {
                                    $group: {
                                        "_id": {
                                            "candidateId": "$candidateId",
                                        },
                                      
                                        "candidateId": {
                                            $first: "$candidateId"
                                        },
                                        "profilePic": {
                                            $first: "$profilePic"
                                        },
                                        "employerId": {
                                            $first: "$employerId"
                                        },
                                       "fname": {
                                            $first: "$fname"
                                        },
                                        "mname": {
                                            $first: "$mname"
                                        },
                                        "lname": {
                                            $first: "$lname"
                                        },
                                        "isFavourite": {
                                            $first: "$isFavourite"
                                        },
                                        "geolocation": {
                                            $first: "$geolocation"
                                        },
                                        "rating": {
                                            $first: "$rating"
                                        },
                                        "jobId": {
                                            $first: "$jobId"
                                        },
                                        "roleId": {
                                            $first: "$roleId"
                                        },
                                        "favouriteBy": {
                                            $first: "$favouriteBy"
                                        },
                                        "isOffered": {
                                            $first: "$isOffered"
                                        },
                                        "isSentInvitation": {
                                            $first: "$isSentInvitation"
                                        },
                                        "skillDetails": {
                                            $first: "$skillDetails"
                                        }
                                    }
                                },
                                { $project: {
                                    "_id":0
                                   }
                                },
                                { '$match': 
                                    { 
                                        'rating': {"$gte":3},
                                        'isFavourite': false,
                                    }
                                },
                                {
                                    $group: {
                                        _id: null,
                                        total: {
                                        $sum: 1
                                        },
                                        results: {
                                        $push : '$$ROOT'
                                        }
                                    }
                                },
                                {
                                    $project : {
                                        '_id': 0,
                                        'total': 1,
                                        'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                        'results': {
                                            $slice: [
                                                '$results', tier2pageno * tier2perpage , tier2perpage
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    };

                    let aggrQuery=[
                     
                        geoLocationStmt,
                        projectStmt1,
                        { '$match': 
                            { 
                                'isExist': true,
                            }
                        },
                        { "$unwind": "$skills" }, 
                        {
                            "$match": {  $and: [
                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                            ]}
                        },
                        {
                            $lookup: {
                                from: "skills",
                                localField: "skills",
                                foreignField: "skillId",
                                as: "skillDetails"
                            }
                        },
                        projectStmt2,
                        projectStmt3,
                        groupStmt1,
                        projectStmt4,
                        facetStmt1                       
                    ];

                    
                    if (searchWithLocation == false) {
                        aggrQuery = [
                        
                            projectStmt1,
                            { '$match': 
                                { 
                                    'isExist': true,
                                }
                            },
                            { "$unwind": "$skills" }, 
                            {
                                "$match": {  $and: [
                                    isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                ]}
                            },
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            projectStmt2,
                            projectStmt3,
                            groupStmt1,
                            projectStmt4,
                            facetStmt1    
                        ];
                    } // END if (searchWithLocation == false) {

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //================

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //===============

                    models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                   
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: error
                            });
                        }
                        else{
                            let data;
                            
                            if (response[0]['TIER1'].length > 0 || response[0]['TIER2'].length > 0) {
                                data = response;
                            } else {
                                res.json({
                                    isError: false,
                                    message: 'No Data Found',
                                    statuscode: 204,
                                    details:null
                                })

                                return;
                            }   

                           
                            if (data[0]['TIER1'].length > 0 || data[0]['TIER2'].length > 0) {
                    
                                if (data[0]['TIER1'].length > 0) {
                                    let offerJobStatus;
                                    let tier1Data = data[0]['TIER1'][0].results;
                                    let tier1DataTag;
                                    let sendNotificationEmailStatus;
                                    let sendNotificationSmsStatus;

                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                        // We are checking whether the candidate is available or not for this job
                                        isCandidateAvailable = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId,jobId, roleId);
                                        tier1Data[tier1DataTag].isCandidateAvailable = isCandidateAvailable;

                                        if (isCandidateAvailable == true) {

                                            if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                
                                                if ( offerJobStatus.isError == false) {
                                                        
                                                    findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);

                                                    if (findMailStatus.isError == false) {
                                                        tier1Data[tier1DataTag].isOffered = true;
                                                        if (findMailStatus.isItemExist == true) {
                                                                
                                                                if(findMailStatus.item.mobileNo == ""){
                                                                    sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                    
                                                                }
                                                                else{
                                                                    sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                    
                                                                }

                                                        } // END if (findMailStatus.isItemExist == true) {
                                                    } // END if (findMailStatus.isError == false) {
                                                } // END if ( offerJobStatus.isError == false) {
                                            }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                        } // END if (isCandidateAvailable == true) {    
                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    data[0]['TIER1'][0].results = tier1Data;
                                } // END if (data[0]['TIER1'].length > 0) {

                                if(data[0]['TIER2'].length) {

                                    var i;
                                    for (i=0; i < data[0]['TIER2'][0].results.length; i++) {
                                        isCandidateAvailable = await checkCandidatePresenceForApi(data[0]['TIER2'][0].results[i].candidateId,jobId, roleId);
                                        data[0]['TIER2'][0].results[i].isCandidateAvailable = isCandidateAvailable;
                                    }

                                } // END if(data[0]['TIER2'].length) { 
                                    
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                               
                                    details:{
                                        "TIER1":data[0]['TIER1'][0],
                                        "TIER2":data[0]['TIER2'][0],
                                       
                                    }
                                })

                                return;
                            } // END if (data[0]['TIER1'].length > 0 || data[0]['TIER2'].length > 0) {   
                  
                            
                        }// END else{
                    })
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //===================================================================================
    offerWithFavouriteOneAndTier2: (req, res, next) => {
        try{
            
            let tier2pageno = req.body.tier2pageno ? (parseInt(req.body.tier2pageno == 0 ? 0 : parseInt(req.body.tier2pageno) - 1)) : 0;
            let tier2perpage = req.body.tier2perpage ? parseInt(req.body.tier2perpage) : 10;
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;

            let skillArrb = [];

            skillArrb = req.body.skillArrb ?  req.body.skillArrb : [];
           
            if (!req.body.jobId) { return; }
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }
           
                
            let jobaggrQuery = [
                {
                    '$match': { 
                        'jobId': jobId,
                        
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction": "$jobDetails.roleWiseAction",

                    }
                },
                
            ]
                    
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
              
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    //console.log(searchJob[0].employerId)
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    //============
                    let employerId=searchJob[0].employerId
                    let hireList=searchJob[0].hireList
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
                    

                    //=====================
                  
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        skills:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        selectedRole:1,
                        appliedJobs:1,
                        invitationToApplyJobs:1,
                        isExist:{
                            $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                        },
                    }};

                    let projectStmt2 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        selectedRole:1,
                        appliedJobs:1,
                        invitationToApplyJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }

                     }};


                    let projectStmt3 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        selectedRole:1,
                        skillDetails: 1,
                        profilePic:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        isEnrolled:{
                            $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },

                    }}; 

                    let groupStmt1 =  {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                          
                            "candidateId": {
                                $first: "$candidateId"
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },

                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            "favouriteBy": {
                                $first: "$favouriteBy"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            },
                            "selectedRole": {
                                $first: "$selectedRole"
                            },
                            "isEnrolled": {
                                $first: "$isEnrolled"
                            }
                        }
                    };

                    let projectStmt4 = { $project: 
                        { 
                            _id:0,
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            isFavourite: 1,
                            selectedRole: 1,
                            isEnrolled: 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            }
                        }
                    };

                    let matchEnrolled = {
                        $match: { 
                            isEnrolled: "yes"
                        }
                    }

                    

                    
                    
                    

                    //=======================================================================
                    // this function is used to fetch the list of candidate
                    async function fetchCandidateList(searchWithLocation, tier2pageno) { 

                        let facetStmt1 = {
                            $facet:{
                                "TIER2":[
                                    { $project: {
                                        jobId:1,
                                        roleId:1,
                                        candidateId: 1,
                                        employerId:1,
                                        fname: 1,
                                        mname: 1,
                                        lname:1,
                                        geolocation:1,
                                        rating:1,
                                        isOffered: 1,
                                        skillDetails:1,
                                        selectedRole:1,
                                        favouriteBy:1,
                                        isFavourite:{
                                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                        },
                                        isEnrolled:{
                                            $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                                        },
                                        profilePic:1,
                                        isSentInvitation:1
                
                                     }},
                                     { $unwind : {path:"$favouriteBy",preserveNullAndEmptyArrays:true} },
                                    
                                     {
                                        '$match': { 'favouriteBy':{ "$ne":searchJob[0].employerId }}
                                     },
                                     {
                                        $group: {
                                            "_id": {
                                                "candidateId": "$candidateId",
                                            },
                                          
                                            "candidateId": {
                                                $first: "$candidateId"
                                            },
                                            "profilePic": {
                                                $first: "$profilePic"
                                            },
                                            "employerId": {
                                                $first: "$employerId"
                                            },
                                           "fname": {
                                                $first: "$fname"
                                            },
                                            "mname": {
                                                $first: "$mname"
                                            },
                                            "lname": {
                                                $first: "$lname"
                                            },
                                            "isFavourite": {
                                                $first: "$isFavourite"
                                            },
                                            "geolocation": {
                                                $first: "$geolocation"
                                            },
                                            "rating": {
                                                $first: "$rating"
                                            },
                                            "jobId": {
                                                $first: "$jobId"
                                            },
                                            "roleId": {
                                                $first: "$roleId"
                                            },
                                            "favouriteBy": {
                                                $first: "$favouriteBy"
                                            },
                                            "isOffered": {
                                                $first: "$isOffered"
                                            },
                                            "isSentInvitation": {
                                                $first: "$isSentInvitation"
                                            },
                                            "skillDetails": {
                                                $first: "$skillDetails"
                                            },
                                            "selectedRole":{
                                                $first: "$selectedRole"
                                            },
                                            "isEnrolled": {
                                                $first: "$isEnrolled"
                                            }
                                        }
                                    },

                                    {
                                        $match: { 
                                            isEnrolled: "yes"
                                        }
                                    },

                                    { $project: {
                                        "_id":0
                                       }
                                    },
                                    { '$match': 
                                        { 
                                            'rating': {"$gte":3},
                                            'isFavourite': false,
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            total: {
                                            $sum: 1
                                            },
                                            results: {
                                            $push : '$$ROOT'
                                            }
                                        }
                                    },
                                    {
                                        $project : {
                                            '_id': 0,
                                            'total': 1,
                                            'noofpage': { $ceil :{ $divide: [ "$total", tier2perpage ] } },
                                            'results': {
                                                $slice: [
                                                    '$results', tier2pageno * tier2perpage , tier2perpage
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        };

                        return new Promise(function(resolve, reject){

                            let aggrQuery=[
                     
                                geoLocationStmt,
                                projectStmt1,
                                { '$match': 
                                    { 
                                        'isExist': true,
                                    }
                                },
                                { "$unwind": "$skills" }, 
                                {
                                    "$match": {  $and: [
                                        isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                    ]}
                                },
                                {
                                    $lookup: {
                                        from: "skills",
                                        localField: "skills",
                                        foreignField: "skillId",
                                        as: "skillDetails"
                                    }
                                },
                                projectStmt2,
                                projectStmt3,
                                groupStmt1,
                                projectStmt4,
                                matchEnrolled,
                                facetStmt1                       
                            ];

                            if (searchWithLocation == false) {
                                aggrQuery = [
                                
                                    projectStmt1,
                                    { '$match': 
                                        { 
                                            'isExist': true,
                                        }
                                    },
                                    { "$unwind": "$skills" }, 
                                    {
                                        "$match": {  $and: [
                                            isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                        ]}
                                    },
                                    {
                                        $lookup: {
                                            from: "skills",
                                            localField: "skills",
                                            foreignField: "skillId",
                                            as: "skillDetails"
                                        }
                                    },
                                    projectStmt2,
                                    projectStmt3,
                                    groupStmt1,
                                    projectStmt4,
                                    matchEnrolled,
                                    facetStmt1    
                                ];
                            } // END if (searchWithLocation == false) {
        

                            models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                   
                                if (error) {
                                    resolve({
                                        "isError": true,
                                        "statuscode": 404                                   
                                    });
                                }
                                else{
                                    let data;
                                    var noOfAvailableCandidate = 0;
                                    
                                    if (response[0]['TIER2'].length > 0) {
                                        data = response;
        
                                        if(data[0]['TIER2'].length) {
        
                                            var i;
                                            for (i=0; i < data[0]['TIER2'][0].results.length; i++) {
                                                isCandidateAvailable = await checkCandidatePresenceForApi(data[0]['TIER2'][0].results[i].candidateId,jobId, roleId);
                                                data[0]['TIER2'][0].results[i].isCandidateAvailable = isCandidateAvailable;
        
                                                if (isCandidateAvailable == true) {
                                                    noOfAvailableCandidate++;
                                                }

                                                // it shows the hiring status of the candidate
                                                isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                                    "hireList": hireList,
                                                    "roleId": roleId,
                                                    "candidateId": data[0]['TIER2'][0].results[i].candidateId
                                                });
                                                data[0]['TIER2'][0].results[i].isHired = isHired;
                                            } // END for (i=0; i < data[0]['TIER2'][0].results.length; i++) {

                                            resolve({
                                                "isError": false,
                                                "noOfAvailableCandidate": noOfAvailableCandidate,
                                                "statuscode": 200, 
                                                "details":{
                                                    "TIER2":data[0]['TIER2'][0],  
                                                }                                  
                                            });
        
                                        } // END if(data[0]['TIER2'].length) { 
        
        
                                        if(data[0]['TIER2'].length < 0) {     
                                            resolve({
                                                "isError": false,
                                                "statuscode": 1009, 
                                                "details": null                        
                                            });     
                                        }
             
                                    } else {
                                        resolve({
                                            "isError": false,
                                            "statuscode": 1009, 
                                            "details": null                        
                                        });
                                    }                
                                    
                                }// END else{
                            }) // END models.candidate.aggregate(aggrQuery).exec( async (error, response) => {


                        }); // END  return new Promise(function(resolve, reject){   
                    } // END async function fetchCandidateList() {     
                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //================

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //=============================

                    var fetchCandidateListStatus; 
                    
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier2pageno);
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER2.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        tier2pageno++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (tier2pageno < noofpage) && (searchWithLocation == true) && (isError == false));

                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        tier2pageno = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier2pageno);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER2.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        details: details
                    });

                    //=============================
               }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //===================================================================================
    offerWithFavouriteOneAndTier1: (req, res, next) => {
        try{
            let tier1pageno = req.body.tier1pageno ? (parseInt(req.body.tier1pageno == 0 ? 0 : parseInt(req.body.tier1pageno) - 1)) : 0;
            let tier1perpage = req.body.tier1perpage ? parseInt(req.body.tier1perpage) : 10;
           
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : '';
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;

            let skillArrb = [];

            skillArrb = req.body.skillArrb ?  req.body.skillArrb : [];
           
            if (!req.body.jobId) { return; }
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }
                   
            let jobaggrQuery = [
                {
                    '$match': { 
                        'jobId': jobId,
                        
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction": "$jobDetails.roleWiseAction",

                    }
                },
                
            ];
                    
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
             
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    let isArrayEmpty;
                    let newSkillArrDb=[];
                    //console.log(searchJob[0].employerId)
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    //============
                    let employerId=searchJob[0].employerId
                    let hireList=searchJob[0].hireList
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
                    

                    //=====================
                  
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    } 

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        skills:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        selectedRole:1,
                        invitationToApplyJobs:1,
                        isExist:{
                            $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                        },
                    }};

                    let projectStmt2 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        profilePic:1,
                        jobOffers:1,
                        appliedJobs:1,
                        selectedRole:1,
                        invitationToApplyJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        },
                        isEnrolled:{
                            $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                        },

                     }};


                    let projectStmt3 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:searchJob[0].employerId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        favouriteBy:1,
                        skillDetails: 1,
                        profilePic:1,
                        selectedRole:1,
                        isEnrolled:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },

                    }}; 

                    let groupStmt1 =  {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                          
                            "candidateId": {
                                $first: "$candidateId"
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },

                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            "favouriteBy": {
                                $first: "$favouriteBy"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            },
                            "selectedRole": {
                                $first: "$selectedRole"
                            },
                            "isEnrolled": {
                                $first: "$isEnrolled"
                            }
                        }
                    };

                    let projectStmt4 = { $project: 
                        { 
                            _id:0,
                            jobId:jobId,
                            roleId:roleId,
                            candidateId: 1,
                            employerId:1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            favouriteBy:1,
                            skillDetails: 1,
                            isFavourite: 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            selectedRole:1,
                            isEnrolled:1
                            

                        }
                    };

                    
                    let matchEnrolled = {
                        $match: { 
                            isEnrolled: "yes"
                        }
                    }
 
                    //=======================================================================
                    // this function is used to fetch the hired details
                    async function fetchCandidateList(searchWithLocation, tier1pageno) { 

                                let facetStmt1 = {
                                    $facet:{
                                        "TIER1":[
                                            { $project: {
                                                jobId:1,
                                                roleId:1,
                                                candidateId: 1,
                                                employerId:1,
                                                fname: 1,
                                                profilePic:1,
                                                mname: 1,
                                                lname:1,
                                                isFavourite:{
                                                    $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                                },
                                                geolocation:1,
                                                rating:1,
                                                isOffered: 1,
                                                skillDetails:1,
                                                favouriteBy:1,
                                                selectedRole:1,
                                                isEnrolled:{
                                                    $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                                                }
                        
                                            }},
                                            { "$unwind": "$favouriteBy" }, 
                                            {
                                                '$match': { 'favouriteBy': searchJob[0].employerId }
                                            },
                                            {
                                                $group: {
                                                    "_id": {
                                                        "candidateId": "$candidateId",
                                                    },
                                                
                                                    "candidateId": {
                                                        $first: "$candidateId"
                                                    },
                                                    "employerId": {
                                                        $first: "$employerId"
                                                    },
                                                "fname": {
                                                        $first: "$fname"
                                                    },
                                                    "mname": {
                                                        $first: "$mname"
                                                    },
                                                    "lname": {
                                                        $first: "$lname"
                                                    },
                                                    "isFavourite": {
                                                        $first: "$isFavourite"
                                                    },
                                                    "geolocation": {
                                                        $first: "$geolocation"
                                                    },
                                                    "rating": {
                                                        $first: "$rating"
                                                    },
                                                    "jobId": {
                                                        $first: "$jobId"
                                                    },
                                                    "roleId": {
                                                        $first: "$roleId"
                                                    },
                                                    "favouriteBy": {
                                                        $first: "$favouriteBy"
                                                    },
                                                    "isOffered": {
                                                        $first: "$isOffered"
                                                    },
                                                    "profilePic": {
                                                        $first: "$profilePic"
                                                    },
                                                    "skillDetails": {
                                                        $first: "$skillDetails"
                                                    },
                                                    "selectedRole": {
                                                        $first: "$selectedRole"
                                                    },
                                                    "isEnrolled": {
                                                        $first: "$isEnrolled"
                                                    }
                                                }
                                            },

                                            {
                                                $match: { 
                                                    isEnrolled: "yes"
                                                }
                                            },

                                            { $project: {
                                                "_id":0
                                                }
                                            },
                                            {
                                                $group: {
                                                    _id: null,
                                                    total: {
                                                    $sum: 1
                                                    },
                                                    results: {
                                                    $push : '$$ROOT'
                                                    }
                                                }
                                            },
                                            {
                                                $project : {
                                                    '_id': 0,
                                                    'total': 1,
                                                    'noofpage': { $ceil :{ $divide: [ "$total", tier1perpage ] } },
                                                    'results': {
                                                        $slice: [
                                                            '$results', tier1pageno * tier1perpage , tier1perpage
                                                        ]
                                                    }
                                                }
                                            }
            
                                        ]
                                    }
                                };
            
                        return new Promise(function(resolve, reject){

                                    let aggrQuery=[
                            
                                        geoLocationStmt,
                                        projectStmt1,
                                        { '$match': 
                                            { 
                                                'isExist': true,
                                            }
                                        },
                                        { "$unwind": "$skills" }, 
                                        {
                                            "$match": {  $and: [
                                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                            ]}
                                        },
                                        {
                                            $lookup: {
                                                from: "skills",
                                                localField: "skills",
                                                foreignField: "skillId",
                                                as: "skillDetails"
                                            }
                                        },
                                        projectStmt2,
                                        projectStmt3,
                                        groupStmt1,
                                        projectStmt4,
                                        matchEnrolled,
                                        facetStmt1                       
                                    ];

                                    if (searchWithLocation == false) {
                                        aggrQuery = [
                                        
                                            projectStmt1,
                                            { '$match': 
                                                { 
                                                    'isExist': true,
                                                }
                                            },
                                            { "$unwind": "$skills" }, 
                                            {
                                                "$match": {  $and: [
                                                    isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                                ]}
                                            },
                                            {
                                                $lookup: {
                                                    from: "skills",
                                                    localField: "skills",
                                                    foreignField: "skillId",
                                                    as: "skillDetails"
                                                }
                                            },
                                            projectStmt2,
                                            projectStmt3,
                                            groupStmt1,
                                            projectStmt4,
                                            matchEnrolled,
                                            facetStmt1    
                                        ];
                                    } // END if (searchWithLocation == false) {

                            models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                   
                                if (error) {
                                    resolve({
                                        "isError": true,
                                        "statuscode": 404,                                   
                                    });
                                    
                                }
                                else{
                                    let data;
                                    var noOfAvailableCandidate = 0;
                                    
                                    if (response[0]['TIER1'].length > 0) {
                                        data = response;
        
                                        if (data[0]['TIER1'].length > 0) {
                                            let offerJobStatus;
                                            let tier1Data = data[0]['TIER1'][0].results;
                                            let tier1DataTag;
                                            let sendNotificationEmailStatus;
                                            let sendNotificationSmsStatus;
        
                                            for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                                // We are checking whether the candidate is available or not for this job
                                                isCandidateAvailable = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId,jobId, roleId);
                                                tier1Data[tier1DataTag].isCandidateAvailable = isCandidateAvailable;
        
                                                if (isCandidateAvailable == true) {
        
                                                    noOfAvailableCandidate++;
                                                    if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                        offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                        
                                                        if ( offerJobStatus.isError == false) {
                                                                
                                                            findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
        
                                                            if (findMailStatus.isError == false) {
                                                                tier1Data[tier1DataTag].isOffered = true;
                                                                if (findMailStatus.isItemExist == true) {
                                                                        
                                                                        if(findMailStatus.item.mobileNo == ""){
                                                                            sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)     
                                                                        }
                                                                        else{
                                                                            sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)   
                                                                        }
        
                                                                } // END if (findMailStatus.isItemExist == true) {
                                                            } // END if (findMailStatus.isError == false) {
                                                        } // END if ( offerJobStatus.isError == false) {
                                                    }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                                } // END if (isCandidateAvailable == true) {  
                                                    
                                                // it shows the hiring status of the candidate
                                                isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                                    "hireList": hireList,
                                                    "roleId": roleId,
                                                    "candidateId": tier1Data[tier1DataTag].candidateId
                                                });
                                                tier1Data[tier1DataTag].isHired = isHired;

                                            } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                            data[0]['TIER1'][0].results = tier1Data;

                                            resolve({
                                                "isError": false,
                                                "statuscode": 200, 
                                                "noOfAvailableCandidate": noOfAvailableCandidate,
                                                "details":{
                                                    "TIER1":data[0]['TIER1'][0]
                                                }                                  
                                            });
                                        } // END if (data[0]['TIER1'].length > 0) {

                                        if(data[0]['TIER1'].length < 0) {     
                                            resolve({
                                                "isError": false,
                                                "statuscode": 1009, 
                                                "details": null                        
                                            });     
                                        }

                                    } else {
                                        
                                        resolve({
                                            "isError": false,
                                            "statuscode": 1009, 
                                            "details":null                         
                                        });
                                       
                                        return;
                                    }                          
                                }// END else{
                            }) // END models.candidate.aggregate(aggrQuery).exec( async (error, response) => {

                        });    
                    } // END async function fetchCandidateList() {  

                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // END async function fetchHiredDate(candidateId) { 

                    //======================================================================
                    // this function is used to fetch the candidate list
                      
                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of fetchJobDetail

                    //===============
                    var fetchCandidateListStatus; 
                    
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier1pageno);                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        tier1pageno++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (tier1pageno < noofpage) && (searchWithLocation == true) && (isError == false));

                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        tier1pageno = 0;
                        searchWithLocation = false;

                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation, tier1pageno);     
                        
                        statuscode = fetchCandidateListStatus.statuscode;
                       
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        details: details
                    });

                    
               } // END  if(searchingerror){
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    // ============
    // this function is written to fetch the candidate list on the basis of and opperation
    //=============
    browseCandidateForOfferAnd: async (req, res, next) => {
        try{
            let upperLimit;
            let lowerLimit;
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let rating = req.body.rating ? req.body.rating :'';
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - role Id"
            });
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;

            let skillArrb = [];
            let fetchCandidateRolebasisStatus;
          
            skillArrb = req.body.skillArrb ? req.body.skillArrb : [];

            let RollArr = [];
            RollArr.push(roleId);
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }
           
            // if (!req.body.jobId ||!req.body.roleId) { return; }
            if (!req.body.jobId || !req.body.roleId) { return; }
            
            switch(rating){
                case "":upperLimit=5;
                        lowerLimit=1;
                        break;
                case "1":upperLimit=5;
                         lowerLimit=1;
                         break;
                case "2":upperLimit=5;
                        lowerLimit=2;
                        break;
                case "3":upperLimit=5;
                        lowerLimit=3;
                        break;
                case "4":upperLimit=5;
                        lowerLimit=4;
                        break;
                default:upperLimit=5;
                        lowerLimit=1;
                        break; 
                        
            }
            
            let jobaggrQuery = [
                {
                    '$match': { 
                        'jobId': jobId,
                        
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction": "$jobDetails.roleWiseAction",

                    }
                }
            ];

            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    if (searchJob.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    if (searchJob[0].roleWiseAction.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    


                    let empId=searchJob[0].employerId;

                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance
                    let hireList=searchJob[0].hireList;

                    let newSkillArrDb=[];
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })
                    let isArrayEmpty;
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    }

                    let geoLocationStmt = {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 = { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:empId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        skills: 1,
                        invitationToApplyJobs:1,
                        jobOffers: 1,
                        favouriteBy: 1,
                        appliedJobs:1,
                        payloadSkill:1,
                        selectedRole:1,
                        isExist:{
                            $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                        },
                    }};

                    let projectStmt2 =  { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:empId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        invitationToApplyJobs:1,
                        jobOffers: 1,
                        favouriteBy: 1,
                        appliedJobs:1,
                        payloadSkill:1,
                        selectedRole:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }};

                    let projectStmt3 = { $project: {
                        jobId:1,
                        roleId:1,
                        candidateId: 1,
                        employerId:1,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        favouriteBy: 1,
                        payloadSkill:1,
                        selectedRole:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                        skillDetails: 1
                    }};

                    let groupStmt1 = {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },
                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "candidateId": {
                                $first: "$candidateId"
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            // "employerId": {
                            //     $first: "$employerId"
                            // }, 
                            "payloadSkill": {
                                $first: "$payloadSkill"
                            },
                            "skillDetails": {
                                $push: "$skillDetails"
                            },
                            "selectedRole": {
                                $first: "$selectedRole"
                            }
                        }
                    };

                    let projectStmt4 = { $project: 
                        { 
                            _id:0,
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            employerId:1,
                            payloadSkill:1,
                            skillDetails:1,
                            selectedRole: 1,
                            isFavourite : 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            isEnrolled:{
                                $cond:[{$setIsSubset: [RollArr, "$selectedRole"]},"yes","no"]
                            },
                            
            
                        }
                    };

                    let matchEnrolled = {
                        $match: { 
                            // $eq: [ "$isEnrolled", "yes" ]
                            isEnrolled: "yes"
                        }
                    }

                   

                

                     //==============================================================
                    // This function is used to fetch candidate list
                    async function fetchCandidateList(searchWithLocation, page) {


                            let groupStmt2 = {
                                $group: {
                                    _id: null,
                                    total: {
                                    $sum: 1
                                    },
                                    results: {
                                    $push : '$$ROOT'
                                    }
                                }
                            };
        
                            let projectStmt5 =  {
                                $project : {
                                    '_id': 0,
                                    'total': 1,
                                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                    'results': {
                                        $slice: [
                                            '$results', page * perPage , perPage
                                        ]
                                    }
                                }
                            };
    

                        return new Promise(function(resolve, reject){


                                let aggrQuery=[
                                    geoLocationStmt,
                                    projectStmt1,
            
                                // {
                                //     $match: {
                                //         'payloadSkill.jobRoleId': roleId
                                //     }
                                // },
                                    { '$match': 
                                        { 
                                            'isExist': true,
                                        }
                                    },
                                    { '$match': { $and: [ 
                                            {'rating': {"$gte":lowerLimit}},
                                            {'rating': {"$lte":upperLimit}},  
                                    ]}
                                    },
                                    { "$unwind": "$skills" }, 
                                    {
                                        "$match": {  $and: [
                                            isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                        ]}
                                    },
                                    {
                                        $lookup: {
                                            from: "skills",
                                            localField: "skills",
                                            foreignField: "skillId",
                                            as: "skillDetails"
                                        }
                                    },
                                    projectStmt2,
                                    projectStmt3,
                                    groupStmt1,
                                    projectStmt4,
                                    matchEnrolled,

                                    groupStmt2,
                                    projectStmt5,
                                ];


                                if (searchWithLocation == false) {
                                    aggrQuery = [
                                        projectStmt1,
        
                                        { '$match': 
                                            { 
                                                'isExist': true,
                                            }
                                        },
                                        { '$match': { $and: [ 
                                                {'rating': {"$gte":lowerLimit}},
                                                {'rating': {"$lte":upperLimit}},  
                                        ]}
                                        },
                                        { "$unwind": "$skills" }, 
                                        {
                                            "$match": {  $and: [
                                                isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                            ]}
                                        },
                                        {
                                            $lookup: {
                                                from: "skills",
                                                localField: "skills",
                                                foreignField: "skillId",
                                                as: "skillDetails"
                                            }
                                        },
                                        projectStmt2,
                                        projectStmt3,
                                        groupStmt1,
                                        projectStmt4,
                                        matchEnrolled,

                                        groupStmt2,
                                        projectStmt5
                                        
                                    ];
                            } // END  if (searchWithLocation == true) {

                            models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                        
                                if (error) {
                                    resolve({
                                        "isError": true,
                                        "statuscode": 404                                   
                                    });
                                }
                                else{
                                    if (response.length < 1) {       
                                        resolve({
                                            "isError": false,
                                            "message": "No Data Found",
                                            "statuscode": 1009                                   
                                        });
                                    } else {
        
                                        var i;
                                        let noOfAvailableCandidate = 0;

                                        for (i=0; i < response[0].results.length; i++) {
                                            isCandidateAvailable = await checkCandidatePresenceForApi(response[0].results[i].candidateId,jobId, roleId);
                                            response[0].results[i].isCandidateAvailable = isCandidateAvailable;

                                            if (isCandidateAvailable == true) {
                                                noOfAvailableCandidate++;
                                            }

                                            // it shows the hiring status of the candidate
                                            isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                                "hireList": hireList,
                                                "roleId": roleId,
                                                "candidateId": response[0].results[i].candidateId
                                            });
                                            response[0].results[i].isHired = isHired;
                                            
                                        } // END for (i=0; i < response[0].results.length; i++) {
        
                                        resolve({
                                            "isError": false,
                                            "message": "Success",
                                            "statuscode": 200,
                                            "noOfAvailableCandidate": noOfAvailableCandidate,
                                            "searchWithLocation": searchWithLocation,    
                                            "details":{
                                                "TIER": response[0]
                                             }                                   
                                        });
                                    }        
                                } // END  else{
                            }) // END  models.candidate.aggregate(aggrQuery).exec( async (error, response) => {

                        }); // END return new Promise(function(resolve, reject){
   
                    };    

                    
                    //=============================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 

                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                      //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================

                    var fetchCandidateListStatus; 

                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        console.log("page====",page);
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        page++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (page < noofpage) && (searchWithLocation == true) && (isError == false));
               
                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        page = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        details: details
                    });
                    
               
                }
            })
          
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    // browseCandidateForOfferAndBKP: async (req, res, next) => {
    //     try{
    //         let upperLimit;
    //         let lowerLimit;
    //         let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
    //         let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
    //         let rating = req.body.rating ? req.body.rating :'';
    //         let skilldbbackup = req.body.skills ? req.body.skills :'';
    //         let jobId = req.body.jobId ? req.body.jobId : res.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
    //         });
    //         let roleId = req.body.roleId ? req.body.roleId : '';
    //         let roleName = req.body.roleName ? req.body.roleName : '';

    //         let skillArrb = [];
    //         let fetchCandidateRolebasisStatus;
    //         //=============================================================
    //         // if (roleId != "") {
    //         //     fetchCandidateRolebasisStatus = await employerJobBusiness.fetchCandidateRolebasis(roleId); 

    //         //     res.json({
    //         //         fetchCandidateRolebasisStatus: fetchCandidateRolebasisStatus
    //         //     })
    //         // }  
    //         // return;

    //         //=============================================================

    //         skillArrb = req.body.skillArrb ? req.body.skillArrb : [];
            
    //         if (skillArrb.length == 0) {
    //             res.json({
    //                 isError: true,
    //                 message: "you forgot to enter skills" 
    //             });
    //             return;
    //         }
           
    //         // if (!req.body.jobId ||!req.body.roleId) { return; }
    //         if (!req.body.jobId) { return; }
            
    //         switch(rating){
    //             case "":upperLimit=5;
    //                     lowerLimit=1;
    //                     break;
    //             case "1":upperLimit=5;
    //                      lowerLimit=1;
    //                      break;
    //             case "2":upperLimit=5;
    //                     lowerLimit=2;
    //                     break;
    //             case "3":upperLimit=5;
    //                     lowerLimit=3;
    //                     break;
    //             case "4":upperLimit=5;
    //                     lowerLimit=4;
    //                     break;
    //             default:upperLimit=5;
    //                     lowerLimit=1;
    //                     break; 
                        
    //         }
            
    //         let jobaggrQuery
    //         if  (roleName != "") {

    //             jobaggrQuery = [
    //                 {
    //                     '$match': { 
    //                         'jobId': jobId,
    //                         'jobDetails.roleWiseAction.roleName': roleName, 
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         "_id": 0,
    //                         "jobId": 1,
    //                         "employerId": 1,
    //                         "jobDetails": 1,
    //                     }
    //                 },
    //                 {
    //                     $redact: {
    //                         $cond: {
    //                             if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
    //                             then: "$$DESCEND",
    //                             else: "$$PRUNE"
    //                         }
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         "_id": 0,
    //                         "jobId": 1,
    //                         "employerId": 1,
    //                         "roleWiseAction": "$jobDetails.roleWiseAction",

    //                     }
    //                 }
    //             ];

    //         } else {

    //             if (roleId != '') {

    //                 jobaggrQuery = [
    //                     {
    //                         '$match': { 
    //                             'jobId': jobId,
                                
    //                         }
    //                     },
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "jobDetails": 1,
    //                         }
    //                     },
    //                     {
    //                         $redact: {
    //                             $cond: {
    //                                 if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
    //                                 then: "$$DESCEND",
    //                                 else: "$$PRUNE"
    //                             }
    //                         }
    //                     },
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "roleWiseAction": "$jobDetails.roleWiseAction",

    //                         }
    //                     }
    //                 ];
                      


    //             } else {

    //                 jobaggrQuery = [
    //                     {
    //                         '$match': { 
    //                             'jobId': jobId,
                                
    //                         }
    //                     },
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "jobDetails": 1,
    //                         }
    //                     },
                        
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "roleWiseAction": "$jobDetails.roleWiseAction",

    //                         }
    //                     }
    //                 ];
                    


    //             }
                

    //         }
            
    //         models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                
    //             if(searchingerror){
    //                 res.json({
    //                     isError: true,
    //                     message: errorMsgJSON['ResponseMsg']['404'],
    //                     statuscode: 404,
    //                     details: null
    //                 });  
    //            }
    //            else{
    //                 if (searchJob.length < 1) {
    //                     res.json({
    //                         isError: false,
    //                         message: "No Data Found",
    //                         statuscode: 204,
    //                         details: null
    //                     })
    //                     return;

    //                 }

    //                 if (searchJob[0].roleWiseAction.length < 1) {
    //                     res.json({
    //                         isError: false,
    //                         message: "No Data Found",
    //                         statuscode: 204,
    //                         details: null
    //                     })
    //                     return;

    //                 }

                    


    //                 let empId=searchJob[0].employerId;

    //                 let employerId=searchJob[0].employerId
    //                 let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
    //                 let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
    //                 let distance=searchJob[0].roleWiseAction[0].distance

                    

    //                 let newSkillArrDb=[];
    //                 searchJob[0].roleWiseAction.map(async tt => {
    //                     await tt['skills'].map(item => {
    //                         newSkillArrDb.push(item)
    //                     })
    //                 })
    //                 let isArrayEmpty;
    //                 switch(skilldbbackup.length){
    //                     case 0:isArrayEmpty=true;
    //                             break;
    //                     default:isArrayEmpty=false;
    //                             break; 
    //                 }
    //                 let aggrQuery=[
    //                    {
    //                         $geoNear: {
    //                            near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
    //                            distanceField: "dist.calculated",
    //                            maxDistance: parseInt(distance),
    //                            includeLocs: "dist.location",
    //                            spherical: true
    //                         }
    //                     },

    //                 { $project: {
    //                     jobId:jobId,
    //                     roleId:roleId,
    //                     candidateId: 1,
    //                     employerId:empId,
    //                     fname: 1,
    //                     mname: 1,
    //                     lname:1,
    //                     geolocation:1,
    //                     rating:1,
    //                     profilePic:1,
    //                     skills: 1,
    //                     invitationToApplyJobs:1,
    //                     jobOffers: 1,
    //                     favouriteBy: 1,
    //                     appliedJobs:1,
    //                     payloadSkill:1,
    //                     isExist:{
    //                         $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
    //                     },

    //                 }},

    //                 // {
    //                 //     $match: {
    //                 //         'payloadSkill.jobRoleId': roleId
    //                 //     }
    //                 // },

    //                 { '$match': 
    //                         { 
    //                             'isExist': true,
    //                         }
    //                 },


    //                 { '$match': { $and: [ 
    //                         {'rating': {"$gte":lowerLimit}},
    //                         {'rating': {"$lte":upperLimit}},
                           
    //                     ]}
    //                 },
    //                 { "$unwind": "$skills" }, 
    //                 {
    //                     "$match": {  $and: [
    //                         isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                           

    //                     ]}
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "skills",
    //                         localField: "skills",
    //                         foreignField: "skillId",
    //                         as: "skillDetails"
    //                     }
    //                 },
    //                 { $project: {
    //                     jobId:jobId,
    //                     roleId:roleId,
    //                     candidateId: 1,
    //                     employerId:empId,
    //                     fname: 1,
    //                     mname: 1,
    //                     lname:1,
    //                     geolocation:1,
    //                     rating:1,
    //                     profilePic:1,
    //                     invitationToApplyJobs:1,
    //                     jobOffers: 1,
    //                     favouriteBy: 1,
    //                     appliedJobs:1,
    //                     payloadSkill:1,
    //                     "skillDetails": {
    //                         $arrayElemAt: ["$skillDetails", 0]
    //                     }

    //                  }},
    //                  { $project: {
    //                     jobId:1,
    //                     roleId:1,
    //                     candidateId: 1,
    //                    employerId:1,
    //                     fname: 1,
    //                     mname: 1,
    //                     lname:1,
    //                     geolocation:1,
    //                     rating:1,
    //                     profilePic:1,
    //                     favouriteBy: 1,
    //                     payloadSkill:1,
    //                     isFavourite:{
    //                         $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
    //                     },
    //                     invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
    //                     appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
    //                     jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    //                     skillDetails: 1

    //                  }},
                    
                    
    //                 {
    //                     $group: {
    //                         "_id": {
    //                             "candidateId": "$candidateId",
    //                         },
    //                         "jobOffers": {
    //                             $first: {
    //                                 $arrayElemAt: ["$jobOffers", 0]
    //                             }
    //                         },
    //                         "appliedJobs": {
    //                             $first: {
    //                                 $arrayElemAt: ["$appliedJobs", 0]
    //                             }
    //                         },
    //                         "invitationToApplyJobs": {
    //                             $first: {
    //                                 $arrayElemAt: ["$invitationToApplyJobs", 0]
    //                             }
    //                         },
    //                         "profilePic": {
    //                             $first: "$profilePic"
    //                         },
    //                         "employerId": {
    //                             $first: "$employerId"
    //                         },
    //                         "candidateId": {
    //                             $first: "$candidateId"
    //                         },
    //                        "fname": {
    //                             $first: "$fname"
    //                         },
    //                         "mname": {
    //                             $first: "$mname"
    //                         },
    //                         "lname": {
    //                             $first: "$lname"
    //                         },
    //                         "isFavourite": {
    //                             $first: "$isFavourite"
    //                         },
    //                         "geolocation": {
    //                             $first: "$geolocation"
    //                         },
    //                         "rating": {
    //                             $first: "$rating"
    //                         },
    //                         "jobId": {
    //                             $first: "$jobId"
    //                         },
    //                         "roleId": {
    //                             $first: "$roleId"
    //                         },
    //                         // "employerId": {
    //                         //     $first: "$employerId"
    //                         // }, 
    //                         "payloadSkill": {
    //                             $push: "$payloadSkill"
    //                         },
    //                         "skillDetails": {
    //                             $push: "$skillDetails"
    //                         }
    //                     }
    //                 },
    //                 { $project: 
    //                     { 
    //                         _id:0,
    //                         jobId:1,
    //                         roleId:1,
    //                         candidateId: 1,
    //                         fname: 1,
    //                         mname: 1,
    //                         lname:1,
    //                         geolocation:1,
    //                         rating:1,
    //                         profilePic:1,
    //                         employerId:1,
    //                         payloadSkill:1,
    //                         skillDetails:1,
    //                         isFavourite : 1,
    //                         isSentInvitation:{
    //                             $cond: {
    //                                 "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
    //                                 "then": true,
    //                                  "else": false
    //                             }
    //                         },
    //                         isOffered:{
    //                             $cond: {
    //                                 "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
    //                                 "then": true,
    //                                  "else": false
    //                             }
    //                         }
            
    //                     }
    //                 },

                    
                   
    //                 {
    //                     $group: {
    //                         _id: null,
    //                         total: {
    //                         $sum: 1
    //                         },
    //                         results: {
    //                         $push : '$$ROOT'
    //                         }
    //                     }
    //                 },
    //                 {
    //                     $project : {
    //                         '_id': 0,
    //                         'total': 1,
    //                         'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
    //                         'results': {
    //                             $slice: [
    //                                 '$results', page * perPage , perPage
    //                             ]
    //                         }
    //                     }
    //                 }
                   

    //                 ]

    //                 //================
    //                 let aggrQuery1 = [
    //                    // {
    //                    //      $geoNear: {
    //                    //         near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
    //                    //         distanceField: "dist.calculated",
    //                    //         maxDistance: parseInt(distance),
    //                    //         includeLocs: "dist.location",
    //                    //         spherical: true
    //                    //      }
    //                    //  },
    //                        { $project: {
    //                             jobId:jobId,
    //                             roleId:roleId,
    //                             candidateId: 1,
    //                             employerId:empId,
    //                             fname: 1,
    //                             mname: 1,
    //                             lname:1,
    //                             geolocation:1,
    //                             rating:1,
    //                             profilePic:1,
    //                             skills: 1,
    //                             invitationToApplyJobs:1,
    //                             jobOffers: 1,
    //                             favouriteBy: 1,
    //                             payloadSkill:1,
    //                             appliedJobs:1,
    //                             isExist:{
    //                                 $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
    //                             },

    //                         }},

    //                         // {
    //                         //     $match: {
    //                         //         'payloadSkill.jobRoleId': roleId
    //                         //     }
    //                         // },
        

    //                         { '$match': 
    //                                 { 
    //                                     'isExist': true,
    //                                 }
    //                         },

    //                         { "$unwind": "$skills" }, 
    //                         {
    //                             "$match": {  $and: [
    //                                 isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                   

    //                             ]}
    //                         },
    //                         {
    //                             $lookup: {
    //                                 from: "skills",
    //                                 localField: "skills",
    //                                 foreignField: "skillId",
    //                                 as: "skillDetails"
    //                             }
    //                         },
    //                         { $project: {
    //                             jobId:jobId,
    //                             roleId:roleId,
    //                             candidateId: 1,
    //                             employerId:empId,
    //                             fname: 1,
    //                             mname: 1,
    //                             lname:1,
    //                             geolocation:1,
    //                             rating:1,
    //                             profilePic:1,
    //                             invitationToApplyJobs:1,
    //                             jobOffers: 1,
    //                             favouriteBy: 1,
    //                             appliedJobs:1,
    //                             payloadSkill:1,
    //                             "skillDetails": {
    //                                 $arrayElemAt: ["$skillDetails", 0]
    //                             }

    //                          }},
    //                          { $project: {
    //                             jobId:1,
    //                             roleId:1,
    //                             candidateId: 1,
    //                            employerId:1,
    //                             fname: 1,
    //                             mname: 1,
    //                             lname:1,
    //                             geolocation:1,
    //                             rating:1,
    //                             profilePic:1,
    //                             favouriteBy: 1,
    //                             payloadSkill:1,
    //                             isFavourite:{
    //                                 $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
    //                             },
    //                             invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
    //                             appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
    //                             jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    //                             skillDetails: 1

    //                          }},
                    
                    
    //                         {
    //                             $group: {
    //                                 "_id": {
    //                                     "candidateId": "$candidateId",
    //                                 },
    //                                 "jobOffers": {
    //                                     $first: {
    //                                         $arrayElemAt: ["$jobOffers", 0]
    //                                     }
    //                                 },
    //                                 "appliedJobs": {
    //                                     $first: {
    //                                         $arrayElemAt: ["$appliedJobs", 0]
    //                                     }
    //                                 },
    //                                 "invitationToApplyJobs": {
    //                                     $first: {
    //                                         $arrayElemAt: ["$invitationToApplyJobs", 0]
    //                                     }
    //                                 },
    //                                 "profilePic": {
    //                                     $first: "$profilePic"
    //                                 },
    //                                 "employerId": {
    //                                     $first: "$employerId"
    //                                 },
    //                                 "candidateId": {
    //                                     $first: "$candidateId"
    //                                 },
    //                                "fname": {
    //                                     $first: "$fname"
    //                                 },
    //                                 "mname": {
    //                                     $first: "$mname"
    //                                 },
    //                                 "lname": {
    //                                     $first: "$lname"
    //                                 },
    //                                 "isFavourite": {
    //                                     $first: "$isFavourite"
    //                                 },
    //                                 "geolocation": {
    //                                     $first: "$geolocation"
    //                                 },
    //                                 "rating": {
    //                                     $first: "$rating"
    //                                 },
    //                                 "jobId": {
    //                                     $first: "$jobId"
    //                                 },
    //                                 "roleId": {
    //                                     $first: "$roleId"
    //                                 },
    //                                 // "employerId": {
    //                                 //     $first: "$employerId"
    //                                 // }, 
    //                                 "payloadSkill": {
    //                                      $push: "$payloadSkill"  
    //                                 },
    //                                 "skillDetails": {
    //                                     $push: "$skillDetails"
    //                                 }
    //                             }
    //                         },
                            
    //                         { $project: 
    //                                 { 
    //                                     _id:0,
    //                                     jobId:1,
    //                                     roleId:1,
    //                                     candidateId: 1,
    //                                     fname: 1,
    //                                     mname: 1,
    //                                     lname:1,
    //                                     geolocation:1,
    //                                     rating:1,
    //                                     profilePic:1,
    //                                     employerId:1,
    //                                     skillDetails:1,
    //                                     isFavourite : 1,
    //                                     payloadSkill: 1,
    //                                     isSentInvitation:{
    //                                         $cond: {
    //                                             "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
    //                                             "then": true,
    //                                              "else": false
    //                                         }
    //                                     },
    //                                     isOffered:{
    //                                         $cond: {
    //                                             "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
    //                                             "then": true,
    //                                              "else": false
    //                                         }
    //                                     }
                                       

    //                                 }
    //                              },
                   

    //                             {
    //                                 $group: {
    //                                     _id: null,
    //                                     total: {
    //                                     $sum: 1
    //                                     },
    //                                     results: {
    //                                     $push : '$$ROOT'
    //                                     }
    //                                 }
    //                             },
    //                             {
    //                                 $project : {
    //                                     '_id': 0,
    //                                     'total': 1,
    //                                     'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
    //                                     'results': {
    //                                         $slice: [
    //                                             '$results', page * perPage , perPage
    //                                         ]
    //                                     }
    //                                 }
    //                             }

    //                 ]
    //                 //=============================================
    //                 // this function is used to fetch the hired details
    //                   async function fetchHiredDate(candidateId) { 

                          
    //                       return new Promise(function(resolve, reject){
    //                             let candidateQry = [
    //                                 {
    //                                     '$match': { 'candidateId': candidateId }
    //                                 },
                                  
    //                                 {
    //                                     $project: {
    //                                         "_id":0,
    //                                         "candidateId": 1,
    //                                         "hiredJobs": 1,
                                            
    //                                     }
    //                                 }, 


    //                             ]


    //                             models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

    //                                   if (err) {
    //                                       resolve({
    //                                           "isError": true                                   
    //                                       });
    //                                   } else {
                                          
    //                                         resolve({
    //                                             "isError": false,
    //                                             "responseCandidate": responseCandidate,
                                                  
    //                                         });                  
    //                                   }

    //                           }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
    //                       }) // END return new Promise(function(resolve, reject){   
    //                   } // start of checkUser

    //                   //=======================================================================
    //                 // this function is used to fetch the hired details
    //                   async function fetchJobDetail(jobIds) { 
    //                       // console.log("====>"+jobIds);
    //                       return new Promise(function(resolve, reject){
    //                             let jobQry = [
    //                                 {
    //                                     '$match': { 'jobId': { $in: jobIds } }
    //                                 },
                                  
    //                                 {
    //                                     $project: {
    //                                         "_id":0,
    //                                         "jobId": 1,
    //                                         "jobDetails.roleWiseAction.setTime": 1,
                                            
    //                                     }
    //                                 },                       
    //                             ]


    //                             models.job.aggregate(jobQry, function (err, responseJob) {

    //                                   if (err) {
    //                                       resolve({
    //                                           "isError": true                                   
    //                                       });
    //                                   } else {
                                          
    //                                         resolve({
    //                                             "isError": false,
    //                                             "responseJob": responseJob,
                                                  
    //                                         });                  
    //                                   }

    //                           }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
    //                       }) // END return new Promise(function(resolve, reject){   
    //                   } // start of checkUser

    //                 //================

    //                 // this function is used to fetch the hired details
    //                   async function fetchCandidatewithoutLocation() { 
    //                       // console.log("====>"+jobIds);
    //                       return new Promise(function(resolve, reject){
                                
    //                             models.candidate.aggregate(aggrQuery1, function (err, response) {

    //                                   if (err) {
    //                                       resolve({
    //                                           "isError": true                                   
    //                                       });
    //                                   } else {
                                          
    //                                         resolve({
    //                                             "isError": false,
    //                                             "response": response,
                                                  
    //                                         });                  
    //                                   }

    //                           }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
    //                       }) // END return new Promise(function(resolve, reject){   
    //                   } // start of checkUser


    //                 //=============


    //                 models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
    //                     // res.json({
    //                     //     response: response
    //                     // });
    //                     // return;
                        
                        
    //                     if (error) {
    //                         res.json({
    //                             isError: true,
    //                             message: errorMsgJSON['ResponseMsg']['404'],
    //                             statuscode: 404,
    //                             details: error
    //                         });
    //                     }
    //                     else{
    //                         //================
    //                         let data;

    //                         if (response.length < 1) {
                                
    //                             let fetchCandidatewithoutLocationStatus = await fetchCandidatewithoutLocation();
                               
    //                             data = fetchCandidatewithoutLocationStatus.response;

    //                             if (data.length < 1) {
    //                                 res.json({
    //                                     isError: false,
    //                                     message: "No data found",
    //                                     statuscode: 204,
    //                                     details: null
    //                                 }) 

    //                                 return;
    //                             }
    //                         } else {
    //                             data = response;
    //                         }
                         

    //                         if (data.length > 0) {
    //                             res.json({
    //                                 isError: false,
    //                                 message: errorMsgJSON['ResponseMsg']['200'],
    //                                 statuscode: 200,
    //                                 details:{

    //                                    "TIER": data[0],
    //                                     // "TIER1":data[0],
    //                                     // "TIER2":{}
    //                                     // "TIER3":{},

    //                                 }
    //                             })
    //                         } 

                             
                            
    //                     } // END  else{
    //                 })
    //            }
    //         })
           
           
          
           
    //     }
    //     catch (error) {
    //         res.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['404'],
    //             statuscode: 404,
    //             details: null
    //         });
    //     } 
    // },

    //===========================================

    browseCandidateForOffer: async (req, res, next) => {
        try{
            let upperLimit;
            let lowerLimit;
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let rating = req.body.rating ? req.body.rating :'';
            let skilldbbackup = req.body.skills ? req.body.skills :'';
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;
           
            // if (!req.body.jobId ||!req.body.roleId) { return; }
            if (!req.body.jobId || !req.body.roleId) { return; }
            
            switch(rating){
                case "":upperLimit=5;
                        lowerLimit=1;
                        break;
                case "1":upperLimit=5;
                         lowerLimit=1;
                         break;
                case "2":upperLimit=5;
                        lowerLimit=2;
                        break;
                case "3":upperLimit=5;
                        lowerLimit=3;
                        break;
                case "4":upperLimit=5;
                        lowerLimit=4;
                        break;
                default:upperLimit=5;
                        lowerLimit=1;
                        break; 
                        
            }
            
            let jobaggrQuery = [
                {
                    '$match': { 
                        'jobId': jobId,     
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction": "$jobDetails.roleWiseAction",

                    }
                }
            ];
                        
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                    if (searchJob.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    if (searchJob[0].roleWiseAction.length < 1) {
                        res.json({
                            isError: false,
                            message: "No Data Found",
                            statuscode: 204,
                            details: null
                        })
                        return;

                    }

                    


                    let empId=searchJob[0].employerId;
                    let hireList=searchJob[0].hireList;

                    let employerId=searchJob[0].employerId
                    let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                    let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                    let distance=searchJob[0].roleWiseAction[0].distance

                    

                    let newSkillArrDb=[];
                    searchJob[0].roleWiseAction.map(async tt => {
                        await tt['skills'].map(item => {
                            newSkillArrDb.push(item)
                        })
                    })

                    if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
                         skilldbbackup = newSkillArrDb
                    }

                    let isArrayEmpty;
                    switch(skilldbbackup.length){
                        case 0:isArrayEmpty=true;
                                break;
                        default:isArrayEmpty=false;
                                break; 
                    }

                    let searchByLocation =  {
                        $geoNear: {
                           near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                           distanceField: "dist.calculated",
                           maxDistance: parseInt(distance),
                           includeLocs: "dist.location",
                           spherical: true
                        }
                    };

                    let projectStmt1 =  { $project: {
                        jobId:jobId,
                        roleId:roleId,
                        candidateId: 1,
                        employerId:empId,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        invitationToApplyJobs:1,
                        jobOffers: 1,
                        favouriteBy: 1,
                        payloadSkill: 1,
                        selectedRole: 1,
                        appliedJobs:1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }

                     }};

                    let projectStmt2 = { $project: {
                        jobId:1,
                        roleId:1,
                        candidateId: 1,
                       employerId:1,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        profilePic:1,
                        favouriteBy: 1,
                        payloadSkill: 1,
                        selectedRole:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                        invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                        appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                        jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                        skillDetails: 1

                     }}; 

                     let groupStmt1 =  {
                        $group: {
                            "_id": {
                                "candidateId": "$candidateId",
                            },
                            "jobOffers": {
                                $first: {
                                    $arrayElemAt: ["$jobOffers", 0]
                                }
                            },
                            "appliedJobs": {
                                $first: {
                                    $arrayElemAt: ["$appliedJobs", 0]
                                }
                            },
                            "invitationToApplyJobs": {
                                $first: {
                                    $arrayElemAt: ["$invitationToApplyJobs", 0]
                                }
                            },
                            "profilePic": {
                                $first: "$profilePic"
                            },
                            "employerId": {
                                $first: "$employerId"
                            },
                            "candidateId": {
                                $first: "$candidateId"
                            },
                           "fname": {
                                $first: "$fname"
                            },
                            "mname": {
                                $first: "$mname"
                            },
                            "lname": {
                                $first: "$lname"
                            },
                            "isFavourite": {
                                $first: "$isFavourite"
                            },
                            "geolocation": {
                                $first: "$geolocation"
                            },
                            "rating": {
                                $first: "$rating"
                            },
                            "jobId": {
                                $first: "$jobId"
                            },
                            "roleId": {
                                $first: "$roleId"
                            },
                            // "employerId": {
                            //     $first: "$employerId"
                            // }, 
                            "skillDetails": {
                                $push: "$skillDetails"
                            },
                            "selectedRole":{
                                $first: "$selectedRole"
                            }
                            
                        }
                    };

                    let projectStmt3 = { $project: 
                        { 
                            _id:0,
                            jobId:1,
                            roleId:1,
                            candidateId: 1,
                            fname: 1,
                            mname: 1,
                            lname:1,
                            geolocation:1,
                            rating:1,
                            profilePic:1,
                            employerId:1,
                            skillDetails:1,
                            isFavourite : 1,
                            selectedRole: 1,
                            isSentInvitation:{
                                $cond: {
                                    "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            isOffered:{
                                $cond: {
                                    "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                    "then": true,
                                     "else": false
                                }
                            },
                            isEnrolled:{
                                $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                            }
                        
                        }
                     };

                     
                        let matchEnrolled = {
                            $match: { 
                                isEnrolled: "yes"
                            }
                        };

                    
                      
                    //==============================================================
                    // This function is used to fetch candidate list
                    async function fetchCandidateList(searchWithLocation, page) {

                        let groupStmt2 =  {
                            $group: {
                                _id: null,
                                total: {
                                $sum: 1
                                },
                                results: {
                                $push : '$$ROOT'
                                }
                            }
                        }; 
    
                        let projectStmt4 = {
                            $project : {
                                '_id': 0,
                                'total': 1,
                                'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                                'results': {
                                    $slice: [
                                        '$results', page * perPage , perPage
                                    ]
                                }
                            }
                        };

                        let aggrQuery=[
                            searchByLocation,
                            { '$match': { $and: [ 
                                    {'rating': {"$gte":lowerLimit}},
                                    {'rating': {"$lte":upperLimit}},
                                
                                ]}
                            },
                            { "$unwind": "$skills" }, 
                            {
                                "$match": {  $and: [
                                    isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                ]}
                            },
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
    
                            projectStmt1,
                            projectStmt2,
                            groupStmt1,
                            projectStmt3,
                            matchEnrolled,
                            groupStmt2,
                            projectStmt4
                        ];

                        if (searchWithLocation == false) {

                            console.log("kkkdk")
                            aggrQuery = [
                                { '$match': { $and: [ 
                                        {'rating': {"$gte":lowerLimit}},
                                        {'rating': {"$lte":upperLimit}},
                                    
                                    ]}
                                },
                                { "$unwind": "$skills" }, 
                                {
                                    "$match": {  $and: [
                                        isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                    ]}
                                },
                                {
                                    $lookup: {
                                        from: "skills",
                                        localField: "skills",
                                        foreignField: "skillId",
                                        as: "skillDetails"
                                    }
                                },
        
                                projectStmt1,
                                projectStmt2,
                                groupStmt1,
                                projectStmt3,
                                matchEnrolled,
                                groupStmt2,
                                projectStmt4  
                                        
                            ];
                        } // END if (searchWithLocation == true) {

                        return new Promise(function(resolve, reject){

                            models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
                                if (error) {
                                    resolve({
                                        "isError": true,
                                        "statuscode": 404                                     
                                    });
                                }
                                else{
                                    
                                    if (response.length < 1) {
                                        resolve({
                                            "isError": false,
                                            "message": "No Data Found",
                                            "statuscode": 1009                                   
                                        });
                                    } else {
                                        var i;
                                        let noOfAvailableCandidate = 0;
        
                                        for (i=0; i < response[0].results.length; i++) {
                                            isCandidateAvailable = await checkCandidatePresenceForApi(response[0].results[i].candidateId,jobId, roleId);
                                            response[0].results[i].isCandidateAvailable = isCandidateAvailable;
        
                                            if (isCandidateAvailable == true) {
                                                noOfAvailableCandidate++;
                                            }

                                            // it shows the hiring status of the candidate
                                            isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                                "hireList": hireList,
                                                "roleId": roleId,
                                                "candidateId": response[0].results[i].candidateId
                                            });
                                            response[0].results[i].isHired = isHired;
                                            
                                        } // END for (i=0; i < response[0].results.length; i++) {

                                        resolve({
                                            "isError": false,
                                            "message": "Success",
                                            "statuscode": 200,
                                            "noOfAvailableCandidate": noOfAvailableCandidate,
                                            "searchWithLocation": searchWithLocation,    
                                            "details":{
                                                "TIER": response[0]
                                             }                                   
                                        });
                                    }
                                    
                                } // END  else{
                            }) // END models.candidate.aggregate(aggrQuery).exec( async (error, response) => {

                        }); // END return new Promise(function(resolve, reject){
                    } // END async function fetchCandidateList() {

                    
                    //=============================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 

                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                      //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser
                    
                    console.log("searchWithLocation ",searchWithLocation)  
                    //================
                    var fetchCandidateListStatus; 
                    // fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                     
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        page++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (page < noofpage) && (searchWithLocation == true) && (isError == false));
                    
                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        page = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        details: details
                    });
                       


               }
            })
       
           
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    

    //=============

    
    // browseCandidateForOffer: (req, res, next) => {
    //     try{
    //         let upperLimit;
    //         let lowerLimit;
    //         let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
    //         let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
    //         let rating = req.body.rating ? req.body.rating :'';
    //         let skilldbbackup = req.body.skills ? req.body.skills :'';
    //         let jobId = req.body.jobId ? req.body.jobId : res.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
    //         });
    //         let roleId = req.body.roleId ? req.body.roleId : '';
    //         let roleName = req.body.roleName ? req.body.roleName : '';
           
    //         // if (!req.body.jobId ||!req.body.roleId) { return; }
    //         if (!req.body.jobId) { return; }
            
    //         switch(rating){
    //             case "":upperLimit=5;
    //                     lowerLimit=1;
    //                     break;
    //             case "1":upperLimit=5;
    //                      lowerLimit=1;
    //                      break;
    //             case "2":upperLimit=5;
    //                     lowerLimit=2;
    //                     break;
    //             case "3":upperLimit=5;
    //                     lowerLimit=3;
    //                     break;
    //             case "4":upperLimit=5;
    //                     lowerLimit=4;
    //                     break;
    //             default:upperLimit=5;
    //                     lowerLimit=1;
    //                     break; 
                        
    //         }
            
    //         let jobaggrQuery
    //         if  (roleName != "") {

    //             jobaggrQuery = [
    //                 {
    //                     '$match': { 
    //                         'jobId': jobId,
    //                         'jobDetails.roleWiseAction.roleName': roleName, 
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         "_id": 0,
    //                         "jobId": 1,
    //                         "employerId": 1,
    //                         "jobDetails": 1,
    //                     }
    //                 },
    //                 {
    //                     $redact: {
    //                         $cond: {
    //                             if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
    //                             then: "$$DESCEND",
    //                             else: "$$PRUNE"
    //                         }
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         "_id": 0,
    //                         "jobId": 1,
    //                         "employerId": 1,
    //                         "roleWiseAction": "$jobDetails.roleWiseAction",

    //                     }
    //                 }
    //             ];

    //         } else {

    //             if (roleId != '') {

    //                 jobaggrQuery = [
    //                     {
    //                         '$match': { 
    //                             'jobId': jobId,
                                
    //                         }
    //                     },
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "jobDetails": 1,
    //                         }
    //                     },
    //                     {
    //                         $redact: {
    //                             $cond: {
    //                                 if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
    //                                 then: "$$DESCEND",
    //                                 else: "$$PRUNE"
    //                             }
    //                         }
    //                     },
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "roleWiseAction": "$jobDetails.roleWiseAction",

    //                         }
    //                     }
    //                 ];
                      


    //             } else {

    //                 jobaggrQuery = [
    //                     {
    //                         '$match': { 
    //                             'jobId': jobId,
                                
    //                         }
    //                     },
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "jobDetails": 1,
    //                         }
    //                     },
                        
    //                     {
    //                         $project: {
    //                             "_id": 0,
    //                             "jobId": 1,
    //                             "employerId": 1,
    //                             "roleWiseAction": "$jobDetails.roleWiseAction",

    //                         }
    //                     }
    //                 ];
                    


    //             }
                

    //         }
            
    //         models.job.aggregate(jobaggrQuery).exec((searchingerror, searchJob) => {

                
                
    //             if(searchingerror){
    //                 res.json({
    //                     isError: true,
    //                     message: errorMsgJSON['ResponseMsg']['404'],
    //                     statuscode: 404,
    //                     details: null
    //                 });  
    //            }
    //            else{
    //                 if (searchJob.length < 1) {
    //                     res.json({
    //                         isError: false,
    //                         message: "No Data Found",
    //                         statuscode: 204,
    //                         details: null
    //                     })
    //                     return;

    //                 }

    //                 if (searchJob[0].roleWiseAction.length < 1) {
    //                     res.json({
    //                         isError: false,
    //                         message: "No Data Found",
    //                         statuscode: 204,
    //                         details: null
    //                     })
    //                     return;

    //                 }

                    


    //                 let empId=searchJob[0].employerId;

    //                 let employerId=searchJob[0].employerId
    //                 let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
    //                 let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
    //                 let distance=searchJob[0].roleWiseAction[0].distance

                    

    //                 let newSkillArrDb=[];
    //                 searchJob[0].roleWiseAction.map(async tt => {
    //                     await tt['skills'].map(item => {
    //                         newSkillArrDb.push(item)
    //                     })
    //                 })

    //                 if (skilldbbackup.length == 0 && newSkillArrDb.length != 0) {
    //                      skilldbbackup = newSkillArrDb
    //                 }

    //                 let isArrayEmpty;
    //                 switch(skilldbbackup.length){
    //                     case 0:isArrayEmpty=true;
    //                             break;
    //                     default:isArrayEmpty=false;
    //                             break; 
    //                 }
    //                 let aggrQuery=[
    //                    {
    //                         $geoNear: {
    //                            near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
    //                            distanceField: "dist.calculated",
    //                            maxDistance: parseInt(distance),
    //                            includeLocs: "dist.location",
    //                            spherical: true
    //                         }
    //                     },
    //                 { '$match': { $and: [ 
    //                         {'rating': {"$gte":lowerLimit}},
    //                         {'rating': {"$lte":upperLimit}},
                           
    //                     ]}
    //                 },
    //                 { "$unwind": "$skills" }, 
    //                 {
    //                     "$match": {  $and: [
    //                         isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                           

    //                     ]}
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: "skills",
    //                         localField: "skills",
    //                         foreignField: "skillId",
    //                         as: "skillDetails"
    //                     }
    //                 },
    //                 { $project: {
    //                     jobId:jobId,
    //                     roleId:roleId,
    //                     candidateId: 1,
    //                     employerId:empId,
    //                     fname: 1,
    //                     mname: 1,
    //                     lname:1,
    //                     geolocation:1,
    //                     rating:1,
    //                     profilePic:1,
    //                     invitationToApplyJobs:1,
    //                     jobOffers: 1,
    //                     favouriteBy: 1,
    //                     appliedJobs:1,
    //                     "skillDetails": {
    //                         $arrayElemAt: ["$skillDetails", 0]
    //                     }

    //                  }},
    //                  { $project: {
    //                     jobId:1,
    //                     roleId:1,
    //                     candidateId: 1,
    //                    employerId:1,
    //                     fname: 1,
    //                     mname: 1,
    //                     lname:1,
    //                     geolocation:1,
    //                     rating:1,
    //                     profilePic:1,
    //                     favouriteBy: 1,
    //                     isFavourite:{
    //                         $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
    //                     },
    //                     invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
    //                     appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
    //                     jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    //                     skillDetails: 1

    //                  }},
                    
                    
    //                 {
    //                     $group: {
    //                         "_id": {
    //                             "candidateId": "$candidateId",
    //                         },
    //                         "jobOffers": {
    //                             $first: {
    //                                 $arrayElemAt: ["$jobOffers", 0]
    //                             }
    //                         },
    //                         "appliedJobs": {
    //                             $first: {
    //                                 $arrayElemAt: ["$appliedJobs", 0]
    //                             }
    //                         },
    //                         "invitationToApplyJobs": {
    //                             $first: {
    //                                 $arrayElemAt: ["$invitationToApplyJobs", 0]
    //                             }
    //                         },
    //                         "profilePic": {
    //                             $first: "$profilePic"
    //                         },
    //                         "employerId": {
    //                             $first: "$employerId"
    //                         },
    //                         "candidateId": {
    //                             $first: "$candidateId"
    //                         },
    //                        "fname": {
    //                             $first: "$fname"
    //                         },
    //                         "mname": {
    //                             $first: "$mname"
    //                         },
    //                         "lname": {
    //                             $first: "$lname"
    //                         },
    //                         "isFavourite": {
    //                             $first: "$isFavourite"
    //                         },
    //                         "geolocation": {
    //                             $first: "$geolocation"
    //                         },
    //                         "rating": {
    //                             $first: "$rating"
    //                         },
    //                         "jobId": {
    //                             $first: "$jobId"
    //                         },
    //                         "roleId": {
    //                             $first: "$roleId"
    //                         },
    //                         // "employerId": {
    //                         //     $first: "$employerId"
    //                         // }, 
    //                         "skillDetails": {
    //                             $push: "$skillDetails"
    //                         }
    //                     }
    //                 },
    //                 { $project: 
    //                     { 
    //                         _id:0,
    //                         jobId:1,
    //                         roleId:1,
    //                         candidateId: 1,
    //                         fname: 1,
    //                         mname: 1,
    //                         lname:1,
    //                         geolocation:1,
    //                         rating:1,
    //                         profilePic:1,
    //                         employerId:1,
    //                         skillDetails:1,
    //                         isFavourite : 1,
    //                         isSentInvitation:{
    //                             $cond: {
    //                                 "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
    //                                 "then": true,
    //                                  "else": false
    //                             }
    //                         },
    //                         isOffered:{
    //                             $cond: {
    //                                 "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
    //                                 "then": true,
    //                                  "else": false
    //                             }
    //                         }
    //                         // isOffered:{
    //                         //     $cond: {
    //                         //         "if": { "$eq": [ "$appliedJobs.jobId", jobId] }, 
    //                         //         "then": true,
    //                         //          "else": false
    //                         //     }
    //                         // }
                           

    //                     }
    //                  },
    //                 // if we want to avoid already offered candidates use this
    //                 //  { '$match':{
    //                 //      isOffered:false }},

    //                 {
    //                     $group: {
    //                         _id: null,
    //                         total: {
    //                         $sum: 1
    //                         },
    //                         results: {
    //                         $push : '$$ROOT'
    //                         }
    //                     }
    //                 },
    //                 {
    //                     $project : {
    //                         '_id': 0,
    //                         'total': 1,
    //                         'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
    //                         'results': {
    //                             $slice: [
    //                                 '$results', page * perPage , perPage
    //                             ]
    //                         }
    //                     }
    //                 }
    //                 ]

    //                 //================
    //                 let aggrQuery1 = [
    //                    // {
    //                    //      $geoNear: {
    //                    //         near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
    //                    //         distanceField: "dist.calculated",
    //                    //         maxDistance: parseInt(distance),
    //                    //         includeLocs: "dist.location",
    //                    //         spherical: true
    //                    //      }
    //                    //  },
    //                         { '$match': { $and: [ 
    //                                 {'rating': {"$gte":lowerLimit}},
    //                                 {'rating': {"$lte":upperLimit}},
                                   
    //                             ]}
    //                         },
    //                         { "$unwind": "$skills" }, 
    //                         {
    //                             "$match": {  $and: [
    //                                 isArrayEmpty?{}:{'skills':{$in: skilldbbackup }}
                                   

    //                             ]}
    //                         },
    //                         {
    //                             $lookup: {
    //                                 from: "skills",
    //                                 localField: "skills",
    //                                 foreignField: "skillId",
    //                                 as: "skillDetails"
    //                             }
    //                         },
    //                         { $project: {
    //                             jobId:jobId,
    //                             roleId:roleId,
    //                             candidateId: 1,
    //                             employerId:empId,
    //                             fname: 1,
    //                             mname: 1,
    //                             lname:1,
    //                             geolocation:1,
    //                             rating:1,
    //                             profilePic:1,
    //                             invitationToApplyJobs:1,
    //                             jobOffers: 1,
    //                             favouriteBy: 1,
    //                             appliedJobs:1,
    //                             "skillDetails": {
    //                                 $arrayElemAt: ["$skillDetails", 0]
    //                             }

    //                          }},
    //                          { $project: {
    //                             jobId:1,
    //                             roleId:1,
    //                             candidateId: 1,
    //                            employerId:1,
    //                             fname: 1,
    //                             mname: 1,
    //                             lname:1,
    //                             geolocation:1,
    //                             rating:1,
    //                             profilePic:1,
    //                             favouriteBy: 1,
    //                             isFavourite:{
    //                                 $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
    //                             },
    //                             invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
    //                             appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
    //                             jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
    //                             skillDetails: 1

    //                          }},
                    
                    
    //                         {
    //                             $group: {
    //                                 "_id": {
    //                                     "candidateId": "$candidateId",
    //                                 },
    //                                 "jobOffers": {
    //                                     $first: {
    //                                         $arrayElemAt: ["$jobOffers", 0]
    //                                     }
    //                                 },
    //                                 "appliedJobs": {
    //                                     $first: {
    //                                         $arrayElemAt: ["$appliedJobs", 0]
    //                                     }
    //                                 },
    //                                 "invitationToApplyJobs": {
    //                                     $first: {
    //                                         $arrayElemAt: ["$invitationToApplyJobs", 0]
    //                                     }
    //                                 },
    //                                 "profilePic": {
    //                                     $first: "$profilePic"
    //                                 },
    //                                 "employerId": {
    //                                     $first: "$employerId"
    //                                 },
    //                                 "candidateId": {
    //                                     $first: "$candidateId"
    //                                 },
    //                                "fname": {
    //                                     $first: "$fname"
    //                                 },
    //                                 "mname": {
    //                                     $first: "$mname"
    //                                 },
    //                                 "lname": {
    //                                     $first: "$lname"
    //                                 },
    //                                 "isFavourite": {
    //                                     $first: "$isFavourite"
    //                                 },
    //                                 "geolocation": {
    //                                     $first: "$geolocation"
    //                                 },
    //                                 "rating": {
    //                                     $first: "$rating"
    //                                 },
    //                                 "jobId": {
    //                                     $first: "$jobId"
    //                                 },
    //                                 "roleId": {
    //                                     $first: "$roleId"
    //                                 },
    //                                 // "employerId": {
    //                                 //     $first: "$employerId"
    //                                 // }, 
    //                                 "skillDetails": {
    //                                     $push: "$skillDetails"
    //                                 }
    //                             }
    //                         },
                            
    //                         { $project: 
    //                                 { 
    //                                     _id:0,
    //                                     jobId:1,
    //                                     roleId:1,
    //                                     candidateId: 1,
    //                                     fname: 1,
    //                                     mname: 1,
    //                                     lname:1,
    //                                     geolocation:1,
    //                                     rating:1,
    //                                     profilePic:1,
    //                                     employerId:1,
    //                                     skillDetails:1,
    //                                     isFavourite : 1,
    //                                     isSentInvitation:{
    //                                         $cond: {
    //                                             "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
    //                                             "then": true,
    //                                              "else": false
    //                                         }
    //                                     },
    //                                     isOffered:{
    //                                         $cond: {
    //                                             "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
    //                                             "then": true,
    //                                              "else": false
    //                                         }
    //                                     }
                                       

    //                                 }
    //                              },
                   

    //                             {
    //                                 $group: {
    //                                     _id: null,
    //                                     total: {
    //                                     $sum: 1
    //                                     },
    //                                     results: {
    //                                     $push : '$$ROOT'
    //                                     }
    //                                 }
    //                             },
    //                             {
    //                                 $project : {
    //                                     '_id': 0,
    //                                     'total': 1,
    //                                     'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
    //                                     'results': {
    //                                         $slice: [
    //                                             '$results', page * perPage , perPage
    //                                         ]
    //                                     }
    //                                 }
    //                             }
    //                 ]
    //                 //=============================================
    //                 // this function is used to fetch the hired details
    //                   async function fetchHiredDate(candidateId) { 

                          
    //                       return new Promise(function(resolve, reject){
    //                             let candidateQry = [
    //                                 {
    //                                     '$match': { 'candidateId': candidateId }
    //                                 },
                                  
    //                                 {
    //                                     $project: {
    //                                         "_id":0,
    //                                         "candidateId": 1,
    //                                         "hiredJobs": 1,
                                            
    //                                     }
    //                                 }, 


    //                             ]


    //                             models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

    //                                   if (err) {
    //                                       resolve({
    //                                           "isError": true                                   
    //                                       });
    //                                   } else {
                                          
    //                                         resolve({
    //                                             "isError": false,
    //                                             "responseCandidate": responseCandidate,
                                                  
    //                                         });                  
    //                                   }

    //                           }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
    //                       }) // END return new Promise(function(resolve, reject){   
    //                   } // start of checkUser

    //                   //=======================================================================
    //                 // this function is used to fetch the hired details
    //                   async function fetchJobDetail(jobIds) { 
    //                       // console.log("====>"+jobIds);
    //                       return new Promise(function(resolve, reject){
    //                             let jobQry = [
    //                                 {
    //                                     '$match': { 'jobId': { $in: jobIds } }
    //                                 },
                                  
    //                                 {
    //                                     $project: {
    //                                         "_id":0,
    //                                         "jobId": 1,
    //                                         "jobDetails.roleWiseAction.setTime": 1,
                                            
    //                                     }
    //                                 },                       
    //                             ]


    //                             models.job.aggregate(jobQry, function (err, responseJob) {

    //                                   if (err) {
    //                                       resolve({
    //                                           "isError": true                                   
    //                                       });
    //                                   } else {
                                          
    //                                         resolve({
    //                                             "isError": false,
    //                                             "responseJob": responseJob,
                                                  
    //                                         });                  
    //                                   }

    //                           }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
    //                       }) // END return new Promise(function(resolve, reject){   
    //                   } // start of checkUser

    //                 //================

    //                 // this function is used to fetch the hired details
    //                   async function fetchCandidatewithoutLocation() { 
    //                       // console.log("====>"+jobIds);
    //                       return new Promise(function(resolve, reject){
                                
    //                             models.candidate.aggregate(aggrQuery1, function (err, response) {

    //                                   if (err) {
    //                                       resolve({
    //                                           "isError": true                                   
    //                                       });
    //                                   } else {
                                          
    //                                         resolve({
    //                                             "isError": false,
    //                                             "response": response,
                                                  
    //                                         });                  
    //                                   }

    //                           }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
    //                       }) // END return new Promise(function(resolve, reject){   
    //                   } // start of checkUser


    //                 //=============


    //                 models.candidate.aggregate(aggrQuery).exec( async (error, response) => {
                        
    //                     if (error) {
    //                         res.json({
    //                             isError: true,
    //                             message: errorMsgJSON['ResponseMsg']['404'],
    //                             statuscode: 404,
    //                             details: error
    //                         });
    //                     }
    //                     else{
    //                         //================
    //                         let data;

    //                         if (response.length < 1) {
    //                             // res.json({
    //                             //     isError: false,
    //                             //     message: "No Data Found",
    //                             //     statuscode: 204,
    //                             //     details: null
    //                             // })
    //                             let fetchCandidatewithoutLocationStatus = await fetchCandidatewithoutLocation();
                               
    //                             data = fetchCandidatewithoutLocationStatus.response;

    //                             if (data.length < 1) {
    //                                 res.json({
    //                                     isError: false,
    //                                     message: "No Data Found",
    //                                     statuscode: 204,
    //                                     details: null
    //                                 }) 

    //                                 return;
    //                             }
    //                         } else {
    //                             data = response;
    //                         }


    //                          /*
    //                         let k;
    //                         let keytoDelete = [];
                            
    //                         for (k = 0; k < data[0].results.length; k++) { // loop1
    //                             console.log('==>loop1');
    //                             let doDelete = 'NO';
    //                             //======================
    //                             // Here, we are seraching the details of hired jobs with respect to candidate
    //                             let fetchHiredDateStatus = await fetchHiredDate(data[0].results[k].candidateId);

    //                             if (fetchHiredDateStatus.isError == true) {
    //                                 res.json({
    //                                     isError: true,
    //                                     message: errorMsgJSON['ResponseMsg']['404'],
    //                                     statuscode: 404,
    //                                     details: null
    //                                 });

    //                                 return;
    //                             }


    //                             let p;
                            
    //                             for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {
    //                                 console.log('==>loop2');

    //                                 let m;
                            
    //                                 for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {
    //                                     console.log('==>loop3');
    //                                     let jobStartDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].startDate);
    //                                     let jobEndDate = parseInt(searchJob[0].roleWiseAction[p].setTime[m].endDate);
                                        
    //                                     let jobdate=new Date(jobStartDate).getDate();
    //                                     let jobmonth=new Date(jobStartDate).getMonth()+1;
    //                                     let jobyear=new Date(jobStartDate).getFullYear();

    //                                     let  jobIds = [];

    //                                     let i;
    //                                     for (i = 0; i < fetchHiredDateStatus.responseCandidate[0].hiredJobs.length; i++) {
    //                                         jobIds.push(fetchHiredDateStatus.responseCandidate[0].hiredJobs[i].jobId);
    //                                     }

    //                                     // Here, we are fetching set time 
    //                                     let fetchJobDetailStatus = await fetchJobDetail(jobIds);

    //                                     if (fetchJobDetailStatus.isError == true) {
    //                                         res.json({
    //                                             isError: true,
    //                                             message: errorMsgJSON['ResponseMsg']['404'],
    //                                             statuscode: 404,
    //                                             details: null
    //                                         });

    //                                         return;
    //                                     }

    //                                     let j; 
    //                                     let startDate;
    //                                     let endDate;
    //                                     let startDateReadable

    //                                     for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {
    //                                         console.log('==>loop4');
    //                                         startDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].startDate);
    //                                         endDate = parseInt(fetchJobDetailStatus.responseJob[j].jobDetails.roleWiseAction[0].setTime[0].endDate);
    //                                         //console.log(startDate + "=====" + endDate);

    //                                         if ((jobStartDate < startDate) && (startDate > jobEndDate)) {
                                                
    //                                             doDelete = 'YES';
    //                                             break;
    //                                         }

    //                                         if ((jobStartDate < endDate) && (endDate > jobEndDate)) {
                                               
    //                                             doDelete = 'YES';
    //                                             break;
    //                                         }

                                       
    //                                         let hiredate=new Date(startDate).getDate();
    //                                         let hiremonth=new Date(startDate).getMonth()+1;
    //                                         let hireyear=new Date(startDate).getFullYear();

    //                                         if ( (hiredate == jobdate) && (hiremonth == jobmonth) && (hireyear == jobyear) ) {
    //                                             doDelete = 'YES';
    //                                             break;
    //                                         }
                                          
                                                                            
    //                                     } // END for (j = 0; j < fetchJobDetailStatus.responseJob.length; j++) {


    //                                 }// END for (m = 0; m < searchJob[0].roleWiseAction[p].setTime.length; m++) {    

    //                             } // END for (p = 0; p < searchJob[0].roleWiseAction.length; p++) {   
                            
    //                             if (doDelete == 'YES') {
    //                                 keytoDelete.push(k);    
    //                             }  

    //                         } // END for (k = 0; k < data[0].results.length; k++) { 

    //                         keytoDelete.sort();
    //                         keytoDelete.reverse();

    //                         let o;
    //                         for (o = 0; o < keytoDelete.length; o++) {
    //                             data[0].results.splice(keytoDelete[o], 1)
    //                         }
    //                        //================
    //                        */

    //                         if (data.length > 0) {
    //                             res.json({
    //                                 isError: false,
    //                                 message: errorMsgJSON['ResponseMsg']['200'],
    //                                 statuscode: 200,
    //                                 details:{

    //                                    "TIER": data[0],
    //                                     // "TIER1":data[0],
    //                                     // "TIER2":{}
    //                                     // "TIER3":{},

    //                                 }
    //                             })
    //                         } 

                             
                            
    //                     } // END  else{
    //                 })
    //            }
    //         })
           
           
          
           
    //     }
    //     catch (error) {
    //         res.json({
    //             isError: true,
    //             message: errorMsgJSON['ResponseMsg']['404'],
    //             statuscode: 404,
    //             details: null
    //         });
    //     } 
    // },
     

    //=============================================================
    getRolesByJobId: async (req, res, next) => {
        let newRoleArrDb=[];
        try{
           
            let jobIds = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            // const routes = await getCalculatedRoutes(jobIds);
            // console.log(routes)
            let isDeleted = 'NO';
            var promisesArr = [];
            jobIds.map(jobId => {
                promisesArr.push(new Promise((resolve,reject) => {
                    models.job.findOne({ 'jobId': jobId }, function (searchingerror, searchJob) {
                        if (searchingerror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                            reject('error');
                        }
                        else {
                                searchJob.jobDetails.roleWiseAction.map(tt => {
                                    
                                    isDeleted = tt['isDeleted'] ? tt['isDeleted'] : 'NO';

                                    if (isDeleted != 'YES') {
                                        newRoleArrDb.push({
                                            roleId: tt['roleId'],
                                            roleName: tt['roleName'],
                                            jobId: searchJob['jobId'],
                                            isDeleted: isDeleted, 
    
                                        })
                                    } // END if (isDeleted == 'YES') {
                                    
                                    resolve(newRoleArrDb);
                                })       
                        }
                    })
                }))
            })

            Promise.all(promisesArr).then(result => {
                
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: result[result.length-1]
                  
                })
            })
            
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        } 
    },

    //======================================
    // here this function is written to fetch the skill on the basis of several ids
    getSkillsByManyJobRoleId: (req, res, next) =>{
        try{
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : [];
           
            if (!req.body.jobId ||!req.body.roleId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },

                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "employerId": 1,
                    
                        "industry": "$jobDetails.industry",
                        "roleWiseAction": "$jobDetails.roleWiseAction",
                    }

                },

                { $unwind: "$roleWiseAction" },

                {
                    $project: {
                        "_id": 0,
                        "roleId": "$roleWiseAction.roleId",
                        "roleName": "$roleWiseAction.roleName",
                        "skillIds": "$roleWiseAction.skills",
                    }

                },

                {
                    '$match': { 'roleId': { $in: roleId } }
                },

                { $unwind: "$skillIds" },

                {
                    $lookup: {
                        from: "skills",
                        localField: "skillIds",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project: {
                        "_id": 0,
                        "roleId":1,
                        "skillDetails": 1,
                    }

                },


                {
                    $project: {
                        "_id": 0,
                        "roleId":1,
                        "skillDetails": { $arrayElemAt: ["$skillDetails", 0] },
                    }

                },

            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {

              
                if (error) {

                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },



    //=====================================



    getSkillsByJobRoleId: (req, res, next) =>{
        try{
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
           
            if (!req.body.jobId ||!req.body.roleId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                {
                    $project: {
                        "_id": 0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                {
                    $redact: {
                        $cond: {
                            if: { $or: [{ $eq: ["$roleId", roleId] }, { $not: "$roleId" }] },
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }
                },
                {
                    $project: {
                        "jobDetails": { $arrayElemAt: ["$jobDetails.roleWiseAction", 0] },
                    }
                },
                { $unwind: "$jobDetails.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "roleId":"$jobDetails.roleId",
                        "skillDetails": { $arrayElemAt: ["$skillDetails", 0] },
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleId",
                        },
                        "skillDetails": {
                            $push: {
                                "skillName":"$skillDetails.skillName",
                                "skillId":"$skillDetails.skillId"
                            }
                        }
                    }
                },
                {
                    $project: {
                        "_id":0,
                        "skillDetails":1,
                    }
                },
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    aotOfferAll: (req, res, next) => {
        try{
            let offerData = req.body.offerData ? req.body.offerData : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Offer data",
                isError: true
            });
            // console.log('.......',offerData)
          offerData.map(async tt => {
              // console.log(tt['jobId'])
              // console.log(tt['employerId'])
              // console.log(tt['candidateId'])
              // console.log(tt['roleId'])
              // let updateWith={
              //   'roleId':tt['jobId'],
              //   'candidateId':tt['candidateId']
              //   }

             let updateWith={
                'roleId':tt['roleId'],
                'candidateId':tt['candidateId']
                }   
           
            // models.job.updateOne({ 'employerId': tt['employerId'],'jobId':tt['jobId'] },{$push: {'approvedTo':updateWith,'appiliedFrom':updateWith}},
            models.job.updateOne({ 'employerId': tt['employerId'],'jobId':tt['jobId'] },{$push: {'approvedTo':updateWith}},  
                await function (error, response) {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    let insertedValue={
                        "roleId":tt['roleId'],"employerId":tt['employerId'],
                        "jobId":tt['jobId'],"candidateId":tt['candidateId']
                    }
                    // models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, { $push: { "jobOffers":insertedValue,"appliedJobs":insertedValue } },
                    models.candidate.updateOne({ 'candidateId': tt['candidateId'] }, { $push: { "jobOffers":insertedValue } },  
                        function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        if (updatedresponse.nModified == 1) {
                            models.authenticate.findOne({'userId':tt['candidateId']}, async function (err, item) {
                                if(item['mobileNo']==""){
                                    var result = await  sendNotificationEmail(item['EmailId'],tt['candidateId'],tt['jobId'],tt['roleId'])
                                    if(result){
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                }
                                else{
                                    var result = await  sendNotificationSms(item['mobileNo'],tt['candidateId'],tt['jobId'],tt['roleId'])
                                    if(result){
                                        res.json({
                                            isError: false,
                                            message: errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: null
                                        })
                                    }
                                    else{
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                }
                            })
                        }
                        else{
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            }) 
                        }
                    })
                }
            })
          })
            

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    aotChooseWorker: (req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            // let roleId = req.body.roleId ? req.body.roleId : '';

            let roleName = req.body.roleName ? req.body.roleName : '';

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;
           
            if (!req.body.jobId || !req.body.roleId) { return; }

            let jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            
                        }
                    },
                    
                    {
                        $project: {
                            "_id":0,
                            "jobId": 1,
                            "hireList": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },

                    { $redact : {
                        $cond: {
                            if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }},
                    
                    {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction":"$jobDetails.roleWiseAction",
                        
                    }
                }
            ]

               
       
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
               
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                if (searchJob.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                if (searchJob[0].roleWiseAction.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                let newSkillArrDb=[];
                searchJob[0].roleWiseAction.map(async tt => {
                    await tt['skills'].map(item => {
                        newSkillArrDb.push(item)
                    })
                })


                let employerId=searchJob[0].employerId
                let hireList=searchJob[0].hireList
                let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                let distance=searchJob[0].roleWiseAction[0].distance

                let geoLocationStmt = {
                    $geoNear: {
                       near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                       distanceField: "dist.calculated",
                       maxDistance: parseInt(distance),
                       includeLocs: "dist.location",
                       spherical: true
                    }
                };

                let projectStmt1 = { $project: {
                    jobId:jobId,
                    roleId:roleId,
                    candidateId: 1,
                    employerId:employerId,
                    fname: 1,
                    mname: 1,
                    lname:1,
                    profilePic:1,
                    geolocation:1,
                    rating:1,
                    appliedJobs:1,
                    jobOffers:1,
                    invitationToApplyJobs:1,
                    favouriteBy: 1,
                    "skillDetails": {
                        $arrayElemAt: ["$skillDetails", 0]
                    },
                    selectedRole:1,
                    isEnrolled:{
                        $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                    }

                }};

                let projectStmt2 = { $project: {
                    jobId:1,
                    roleId:1,
                    candidateId: 1,
                    employerId:employerId,
                    fname: 1,
                    mname: 1,
                    lname:1,
                    profilePic:1,
                    geolocation:1,
                    rating:1,
                    favouriteBy: 1,
                    isFavourite:{
                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                    },
                    
                    invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                    //jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] }] },
                    //jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $eq: ["$$jobOffers.jobId", "$jobId"] } } },
                    jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                    appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                    skillDetails: 1,
                    selectedRole:1,
                    isEnrolled:1

                }};

                let groupStmt1 = {
                    $group: {
                        "_id": {
                            "candidateId": "$candidateId",
                        },

                        "jobOffers": {
                            $first: {
                                $arrayElemAt: ["$jobOffers", 0]
                            }
                        },
                        "appliedJobs": {
                            $first: {
                                $arrayElemAt: ["$appliedJobs", 0]
                            }
                        },
                        "invitationToApplyJobs": {
                            $first: {
                                $arrayElemAt: ["$invitationToApplyJobs", 0]
                            }
                        },
                        "candidateId": {
                            $first: "$candidateId"
                        },
                       "fname": {
                            $first: "$fname"
                        },
                        "mname": {
                            $first: "$mname"
                        },
                        "lname": {
                            $first: "$lname"
                        },
                        "isFavourite": {
                            $first: "$isFavourite"
                        },
                        "geolocation": {
                            $first: "$geolocation"
                        },
                        "rating": {
                            $first: "$rating"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "employerId": {
                            $first: "$employerId"
                        }, 
                        "profilePic": {
                            $first: "$profilePic"
                        }, 
                        // "favouriteBy": 1,
                        "skillDetails": {
                            $push: "$skillDetails"
                        },
                        "selectedRole": {
                            $first: "$selectedRole"
                        },
                        "isEnrolled": {
                            $first: "$isEnrolled"
                        }
                    }
                };

                let projectStmt3 = { $project: 
                    { 
                        _id:0,
                        jobId:1,
                        roleId:1,
                        candidateId: 1,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        employerId:1,
                        profilePic:1,
                        skillDetails:1,
                        isFavourite:1,
                        isSentInvitation:{
                            $cond: {
                                "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        isOffered:{
                            $cond: {
                                "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        selectedRole:1,
                        isEnrolled:1

                        // isOffered:{
                        //     $cond: {
                        //         "if": { "$eq": [ "$appliedJobs.jobId", jobId] }, 
                        //         "then": true,
                        //          "else": false
                        //     }
                        // },

                    }
                };

                let groupStmt2 = {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                };

                let matchEnrolled = {
                    $match: { 
                        isEnrolled: "yes"
                    }
                };

                //===================================================================
                // This function is used to fetch candidate list
                async function fetchCandidateList(searchWithLocation,page) { 

                    let projectStmt4 = {
                        $project : {
                            '_id': 0,
                            'total': 1,
                            'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                            'results': {
                                $slice: [
                                    '$results', page * perPage , perPage
                                ]
                            }
                        }
                    };

                    return new Promise(function(resolve, reject){

                        let newaggrQuery=[                        
                                geoLocationStmt,
                                { "$match": { "rating": { "$gte": 4 } } } ,
                                { "$unwind": "$skills" },
                                {
                                    "$match": {
                                        "skills": { "$in": newSkillArrDb },
                                    }
                                }, 
                                {
                                    $lookup: {
                                        from: "skills",
                                        localField: "skills",
                                        foreignField: "skillId",
                                        as: "skillDetails"
                                    }
                                },
        
                                projectStmt1,
                                projectStmt2,
                                groupStmt1,
                                projectStmt3,
                                matchEnrolled,
                                groupStmt2,
                                projectStmt4
                                
                        ];


                        if (searchWithLocation == false) {
                            // console.log("searchWithLocation",searchWithLocation);
                            newaggrQuery = [  
                               
                                { "$match": { "rating": { "$gte": 4 } } } ,
                                { "$unwind": "$skills" },
                                {
                                    "$match": {
                                        "skills": { "$in": newSkillArrDb },
                                    }
                                }, 
                                {
                                    $lookup: {
                                        from: "skills",
                                        localField: "skills",
                                        foreignField: "skillId",
                                        as: "skillDetails"
                                    }
                                },
        
                                projectStmt1,
                                projectStmt2,
                                groupStmt1,
                                projectStmt3,
                                matchEnrolled,
                                groupStmt2,
                                projectStmt4
                               
                            ];
                        }

                        models.candidate.aggregate(newaggrQuery).exec( async (error, response) => {
                                   
                            if (error) {
                                resolve({
                                    "isError": true,
                                    "statuscode": 404,
                                    "details": null                                   
                                });
                            }
                            else{
    
                                data = response;
                                if (response.length < 1) {
                                        resolve({
                                            "isError": false,
                                            "statuscode": 1009,
                                            "details": null                                   
                                        });
                                       
                                        return;
                                   
                                }  // if (response.length < 1) {
    
    
    
                                if (data.length > 0) {
                                    if (data[0].results.length < 1) {
                                        resolve({
                                            "isError": false,
                                            "statuscode": 1009,
                                            "details": null                                   
                                        });
    
                                        return;
                                    }
    
                                    // ==============================
                                    let offerJobStatus;
                                    let tier1Data = data[0].results;
                                    let tier1DataTag;
                                    let sendNotificationEmailStatus;
                                    let sendNotificationSmsStatus;
                                    var noOfAvailableCandidate = 0;
    
                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                        
                                        candidateAvailityStatus = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId, jobId, roleId);
    
                                        if (candidateAvailityStatus == false) { // Here, we are checking whether the candidate is available for the job or not
                                            tier1Data[tier1DataTag].isCandidateAvailable = false;
                                        } else {
                                            tier1Data[tier1DataTag].isCandidateAvailable = true;
                                            noOfAvailableCandidate++;

                                                    if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                        offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                        
    
                                                        if ( offerJobStatus.isError == false) {
                                                            
                                                            
                                                            findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
    
                                                            
    
                                                            if (findMailStatus.isError == false) {
                                                                tier1Data[tier1DataTag].isOffered = true;
                                                                if (findMailStatus.isItemExist == true) {
                                                                        
                                                                        if(findMailStatus.item.mobileNo == ""){
                                                                            sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
       
                                                                        }
                                                                        else{
                                                                            sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                            
                                                                        }
    
                                                                } // END if (findMailStatus.isItemExist == true) {
                                                            } // END if (findMailStatus.isError == false) {
                                                        } // END if ( offerJobStatus.isError == false) {
    
                                                        
                                                    }  // END if (tier1Data[tier1DataTag].isOffered == false) {
    
                                        } // END if (candidateAvailityStatus == true) {  
                                            
                                        // it shows the hiring status of the candidate
                                        isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                            "hireList": hireList,
                                            "roleId": roleId,
                                            "candidateId": tier1Data[tier1DataTag].candidateId
                                        });
                                        tier1Data[tier1DataTag].isHired = isHired;

                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    data[0].results = tier1Data;
    
                                    resolve({
                                        "isError": false,
                                        "statuscode": 200,
                                        "noOfAvailableCandidate": noOfAvailableCandidate,
                                        "details": {
                                            "TIER1":data[0]
                                        }                                   
                                    });
                                    return;
                                } // END  if (data.length > 0) {
    
                                if (data.length < 1) {
                                    resolve({
                                        "isError": false,
                                        "statuscode": 1009,
                                        "details": null                        
                                    });
    
                                    return;
                                }
    
                                
                            }
                        }) // END models.candidate.aggregate(newaggrQuery).exec( async (error, response) => {

                    }); // END return new Promise(function(resolve, reject){
                } // END async function fetchCandidateList() { 
                //====================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================


                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //=====================================================
                    var fetchCandidateListStatus; 
                      
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        page++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (page < noofpage) && (searchWithLocation == true) && (isError == false));

                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        page = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        details: details
                    });
                    return;
                    
                      //======================================================
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //=====================
    /*
       This function is written to fetch all the candidate on the basis of and
    */
    aotChooseWorkerAnd: (req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            // let roleId = req.body.roleId ? req.body.roleId : '';
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });

            let searchWithLocation = req.body.searchWithLocation ? req.body.searchWithLocation : false;

            let roleName = req.body.roleName ? req.body.roleName : '';

            let skillArrb = [];

            skillArrb = req.body.skillArrb ? req.body.skillArrb : [];
           
            if (!req.body.jobId || !req.body.roleId) { return; }
            
            if (skillArrb.length == 0) {
                res.json({
                    isError: true,
                    message: "you forgot to enter skills" 
                });
                return;
            }

                
            let jobaggrQuery = [
                    {
                        '$match': { 
                            'jobId': jobId,
                            
                        }
                    },
                    
                    {
                        $project: {
                            "_id":0,
                            "jobId": 1,
                            "hireList": 1,
                            "employerId": 1,
                            "jobDetails": 1,
                        }
                    },

                    { $redact : {
                        $cond: {
                            if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                            then: "$$DESCEND",
                            else: "$$PRUNE"
                        }
                    }},
                    
                    {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "hireList": 1,
                        "employerId": 1,
                        "roleWiseAction":"$jobDetails.roleWiseAction",
                        
                    }
                }
            ]

                 
            models.job.aggregate(jobaggrQuery).exec( async (searchingerror, searchJob) => {
                
                
                if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                if (searchJob.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                if (searchJob[0].roleWiseAction.length < 1) {
                    res.json({
                        isError: true,
                        message: 'Data Not Found',
                        statuscode: 204,
                        details: null
                    });
                    return;

                }

                let newSkillArrDb=[];
                searchJob[0].roleWiseAction.map(async tt => {
                    await tt['skills'].map(item => {
                        newSkillArrDb.push(item)
                    })
                })


                let employerId=searchJob[0].employerId
                let hireList=searchJob[0].hireList
                let long=searchJob[0].roleWiseAction[0].location.coordinates[0]
                let lat=searchJob[0].roleWiseAction[0].location.coordinates[1]
                let distance=searchJob[0].roleWiseAction[0].distance

                let geoLocationStmt = {
                    $geoNear: {
                       near: { type: "Point", coordinates: [parseFloat(long), parseFloat(lat)] },
                       distanceField: "dist.calculated",
                       maxDistance: parseInt(distance),
                       includeLocs: "dist.location",
                       spherical: true
                    }
                };

                let projectStmt1 =  { $project: {
                    jobId:1,
                    roleId:1,
                    candidateId: 1,
                    employerId:employerId,
                    fname: 1,
                    mname: 1,
                    lname:1,
                    skills:1,
                    profilePic:1,
                    geolocation:1,
                    rating:1,
                    appliedJobs:1,
                    jobOffers:1,
                    invitationToApplyJobs:1,
                    selectedRole:1,
                    favouriteBy: 1,
                    isExist:{
                        $cond:[{$setIsSubset: [skillArrb, "$skills"]},true,false]
                    },
                    isEnrolled:{
                        $cond:[{$setIsSubset: [[roleId], "$selectedRole"]},"yes","no"]
                    }
                }};

                let projectStmt2 = { $project: {
                    jobId:jobId,
                    roleId:roleId,
                    candidateId: 1,
                    employerId:employerId,
                    fname: 1,
                    mname: 1,
                    lname:1,
                    profilePic:1,
                    geolocation:1,
                    rating:1,
                    appliedJobs:1,
                    jobOffers:1,
                    invitationToApplyJobs:1,
                    favouriteBy: 1,
                    "skillDetails": {
                        $arrayElemAt: ["$skillDetails", 0]
                    },
                    selectedRole: 1,
                    isEnrolled: 1
                }};

                let projectStmt3 = { $project: {
                    jobId:1,
                    roleId:1,
                    candidateId: 1,
                    employerId:employerId,
                    fname: 1,
                    mname: 1,
                    lname:1,
                    profilePic:1,
                    geolocation:1,
                    rating:1,
                    favouriteBy: 1,
                    isFavourite:{
                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                    },
                    
                    invitationToApplyJobs: { $filter: { input: "$invitationToApplyJobs", as: "invitationToApplyJobs", cond: { $eq: ["$$invitationToApplyJobs.jobId", "$jobId"] } } },
                    
                    jobOffers: { $filter: { input: "$jobOffers", as: "jobOffers", cond: { $and: [{ $eq: ["$$jobOffers.jobId", "$jobId"] }, { $eq: ["$$jobOffers.roleId", "$roleId"] } ]} } },
                    appliedJobs: { $filter: { input: "$appliedJobs", as: "appliedJobs", cond: { $eq: ["$$appliedJobs.jobId", "$jobId"] } } },
                    skillDetails: 1,
                    selectedRole: 1,
                    isEnrolled: 1

                }};

                let groupStmt1 =  {
                    $group: {
                        "_id": {
                            "candidateId": "$candidateId",
                        },

                        "jobOffers": {
                            $first: {
                                $arrayElemAt: ["$jobOffers", 0]
                            }
                        },
                        "appliedJobs": {
                            $first: {
                                $arrayElemAt: ["$appliedJobs", 0]
                            }
                        },
                        "invitationToApplyJobs": {
                            $first: {
                                $arrayElemAt: ["$invitationToApplyJobs", 0]
                            }
                        },
                        "candidateId": {
                            $first: "$candidateId"
                        },
                       "fname": {
                            $first: "$fname"
                        },
                        "mname": {
                            $first: "$mname"
                        },
                        "lname": {
                            $first: "$lname"
                        },
                        "isFavourite": {
                            $first: "$isFavourite"
                        },
                        "geolocation": {
                            $first: "$geolocation"
                        },
                        "rating": {
                            $first: "$rating"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "employerId": {
                            $first: "$employerId"
                        }, 
                        "profilePic": {
                            $first: "$profilePic"
                        }, 
                        // "favouriteBy": 1,
                        "skillDetails": {
                            $push: "$skillDetails"
                        },
                        "selectedRole": {
                            $first: "$selectedRole"
                        },
                        "isEnrolled": {
                            $first: "$isEnrolled"
                        }
                    }
                };

                let projectStmt4 = { $project: 
                    { 
                        _id:0,
                        jobId:1,
                        roleId:1,
                        candidateId: 1,
                        fname: 1,
                        mname: 1,
                        lname:1,
                        geolocation:1,
                        rating:1,
                        employerId:1,
                        profilePic:1,
                        skillDetails:1,
                        isFavourite:1,
                        isSentInvitation:{
                            $cond: {
                                "if": { "$eq": [ "$invitationToApplyJobs.jobId", jobId] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        isOffered:{
                            $cond: {
                                "if": { "$eq": [ "$jobOffers.jobId", jobId] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        selectedRole: 1,
                        isEnrolled: 1
                    }
                };

                let groupStmt2 = {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                };


                let matchEnrolled = {
                    $match: { 
                        isEnrolled: "yes"
                    }
                }

                //============================================================================
                // this function is written to fetch the list of candidate
                async function fetchCandidateList(searchWithLocation,page) {   
                    
                    let projectStmt5 = {
                        $project : {
                            '_id': 0,
                            'total': 1,
                            'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                            'results': {
                                $slice: [
                                    '$results', page * perPage , perPage
                                ]
                            }
                        }
                    };

                    return new Promise(function(resolve, reject){

                        let newaggrQuery=[                        
                            geoLocationStmt,
                            projectStmt1,
                            { 
                                "$match": { 
                                    "rating": { "$gte": 4 },
                                    "isExist": true 
                                } 
                            },
                            { "$unwind": "$skills" },
                            {
                                "$match": {
                                    "skills": { "$in": newSkillArrDb },
                                }
                            },    
                            {
                                $lookup: {
                                    from: "skills",
                                    localField: "skills",
                                    foreignField: "skillId",
                                    as: "skillDetails"
                                }
                            },
                            projectStmt2,
                            projectStmt3,
                            groupStmt1,
                            projectStmt4,
                            matchEnrolled,
                            groupStmt2,
                            projectStmt5
                                
                        ];

                        if (searchWithLocation == false) {
                            newaggrQuery = [  
                                projectStmt1,
                                { 
                                    "$match": { 
                                        "rating": { "$gte": 4 },
                                        "isExist": true 
                                    } 
                                },
                                { "$unwind": "$skills" },
                                {
                                    "$match": {
                                        "skills": { "$in": newSkillArrDb },
                                    }
                                },    
                                {
                                    $lookup: {
                                        from: "skills",
                                        localField: "skills",
                                        foreignField: "skillId",
                                        as: "skillDetails"
                                    }
                                },
                                projectStmt2,
                                projectStmt3,
                                groupStmt1,
                                projectStmt4,
                                matchEnrolled,
                                groupStmt2,
                                projectStmt5    
                            ];
                        }

                        models.candidate.aggregate(newaggrQuery).exec( async (error, response) => {
                        
                        
                        
                            if (error) {
                                resolve({
                                    "isError": true,
                                    "statuscode": 404,
                                    "details": null                                   
                                });
                               
                                return;
                            }
                            else{
    
                                data = response;
                                if (response.length < 1) {

                                    resolve({
                                        "isError": false,
                                        "statuscode": 1009,
                                        "details": null                                   
                                    });
                                    return;
                                
                                }  // if (response.length < 1) {
    
             
                                if (data.length > 0) {
                                    if (data[0].results.length < 1) {
    
                                        resolve({
                                            "isError": false,
                                            "statuscode": 1009,
                                            "details": null                                   
                                        });
                                        return;
                                    }
    
                                    // ==============================
                                    
                                    let offerJobStatus;
                                    let tier1Data = data[0].results;
                                    let tier1DataTag;
                                    let sendNotificationEmailStatus;
                                    let sendNotificationSmsStatus;
                                    var noOfAvailableCandidate = 0;
                                    
    
                                    for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                        
                                        candidateAvailityStatus = await checkCandidatePresenceForApi(tier1Data[tier1DataTag].candidateId, jobId, roleId);
    
                                        if (candidateAvailityStatus == false) { // Here, we are checking whether the candidate is available for the job or not
                                            tier1Data[tier1DataTag].isCandidateAvailable = false;
                                        } else {
                                            tier1Data[tier1DataTag].isCandidateAvailable = true;
                                            noOfAvailableCandidate++;
    
                                                if (tier1Data[tier1DataTag].isOffered == false) {                        
                                                    offerJobStatus = await employerJobBusiness.offerJob(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
                                                    
    
                                                    if ( offerJobStatus.isError == false) {
                                                        
                                                        
                                                        findMailStatus = await employerJobBusiness.findMail(tier1Data[tier1DataTag].candidateId, tier1Data[tier1DataTag].employerId, tier1Data[tier1DataTag].roleId, tier1Data[tier1DataTag].jobId);
    
                                                        
    
                                                        if (findMailStatus.isError == false) {
                                                            tier1Data[tier1DataTag].isOffered = true;
                                                            if (findMailStatus.isItemExist == true) {
                                                                    
                                                                    if(findMailStatus.item.mobileNo == ""){
                                                                        sendNotificationEmailStatus = sendNotificationEmail(findMailStatus.item.EmailId ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                        
                                                                    }
                                                                    else{
                                                                        sendNotificationSmsStatus = sendNotificationSms(findMailStatus.item.mobileNo ,tier1Data[tier1DataTag].candidateId ,tier1Data[tier1DataTag].jobId ,tier1Data[tier1DataTag].roleId)
                                                                        
                                                                    }
    
                                                            } // END if (findMailStatus.isItemExist == true) {
                                                        } // END if (findMailStatus.isError == false) {
                                                    } // END if ( offerJobStatus.isError == false) {
                                                    
                                                }  // END if (tier1Data[tier1DataTag].isOffered == false) {
                                        } // END if (candidateAvailityStatus == true) { 

                                        // it shows the hiring status of the candidate
                                        isHired = jobProfileBusiness.checkCandidateIsHiredOrNot({
                                            "hireList": hireList,
                                            "roleId": roleId,
                                            "candidateId": tier1Data[tier1DataTag].candidateId
                                        });
                                        tier1Data[tier1DataTag].isHired = isHired;
    
                                    } // for (tier1DataTag = 0; tier1DataTag < tier1Data.length; tier1DataTag++) {
                                    data[0].results = tier1Data; 

                                    resolve({
                                        "isError": false,
                                        "statuscode": 200,
                                        "noOfAvailableCandidate": noOfAvailableCandidate,
                                        "details": { 
                                            "TIER1":data[0]
                                        }                                   
                                    });
                                    return;
                                } // END if (data.length > 0) {
    
                                if (data.length < 1) {

                                    resolve({
                                        "isError": false,
                                        "statuscode": 1009,
                                        "details": null                                   
                                    });
                                    return;
                                }
    
                                
                            }
                        }) // END models.candidate.aggregate(newaggrQuery).exec( async (error, response) => {

                    }); // END return new Promise(function(resolve, reject){   
                } // END async function fetchCandidateList() {       
                //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchHiredDate(candidateId) { 
                          
                          return new Promise(function(resolve, reject){
                                let candidateQry = [
                                    {
                                        '$match': { 'candidateId': candidateId }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "candidateId": 1,
                                            "hiredJobs": 1,
                                            
                                        }
                                    }, 


                                ]


                                models.candidate.aggregate(candidateQry, function (err, responseCandidate) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseCandidate": responseCandidate,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                    //================


                    //=======================================================================
                    // this function is used to fetch the hired details
                      async function fetchJobDetail(jobIds) { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                                let jobQry = [
                                    {
                                        '$match': { 'jobId': { $in: jobIds } }
                                    },
                                  
                                    {
                                        $project: {
                                            "_id":0,
                                            "jobId": 1,
                                            "jobDetails.roleWiseAction.setTime": 1,
                                            
                                        }
                                    },                       
                                ]


                                models.job.aggregate(jobQry, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser

                     //=======================================================================
                    // this function is used to fetch all candidate irrespective of location
                      async function fetchAllCandidateDetail() { 
                          // console.log("====>"+jobIds);
                          return new Promise(function(resolve, reject){
                              

                                models.candidate.aggregate(newaggrQueryforAllCandidate, function (err, responseJob) {

                                      if (err) {
                                          resolve({
                                              "isError": true                                   
                                          });
                                      } else {
                                          
                                            resolve({
                                                "isError": false,
                                                "responseJob": responseJob,
                                                  
                                            });                  
                                      }

                              }) // models.candidate.aggregate(candidateQry, function (err, responseCandidate) { 
                                       
                          }) // END return new Promise(function(resolve, reject){   
                      } // start of checkUser  

                    //=======================
                    var fetchCandidateListStatus; 
                
                    do { // This the first search
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                       
                        statuscode = fetchCandidateListStatus.statuscode;
                        
                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;        
                        page++;
                    }
                    while ((statuscode == 200) && (noOfAvailableCandidate < 1) && (page < noofpage) && (searchWithLocation == true) && (isError == false));


                    if ((noOfAvailableCandidate < 1) && (searchWithLocation == true)) {
                        page = 0;
                        searchWithLocation = false;
                        fetchCandidateListStatus = await fetchCandidateList(searchWithLocation,page);
                        
                        statuscode = fetchCandidateListStatus.statuscode;

                        if (statuscode == 200) {
                            noOfAvailableCandidate = fetchCandidateListStatus.noOfAvailableCandidate;
                            noofpage = fetchCandidateListStatus.details.TIER1.noofpage;
                            details = fetchCandidateListStatus.details;
                        } else  if (statuscode == 1009) {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        } else {
                            noOfAvailableCandidate = 0;
                            noofpage = 0;
                            details = null;
                        }
                          
                        isError = fetchCandidateListStatus.isError;  
                    }

                    res.json({
                        isError: isError,
                        message: errorMsgJSON['ResponseMsg'][statuscode],
                        statuscode: statuscode,
                        noOfAvailableCandidate: noOfAvailableCandidate,
                        details: details
                    });
                    return;

                    //========================
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //==========================

    markUnFavourite:(req, res, next) => {
        try{
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - candidateId"
            }); 
            if(!req.body.employerId || !req.body.candidateId) { return; } 
            models.candidate.updateOne({"candidateId":candidateId}, { $pull: { "favouriteBy":employerId } }, function (pushederror, pushedresponse) {
                if(pushederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    }) 
                }
                else{
                    if (pushedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }

            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    markFavourite:(req, res, next) => {
        try{
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - candidateId"
            }); 
            if(!req.body.employerId || !req.body.candidateId) { return; } 
            models.candidate.find({"candidateId":candidateId,"favouriteBy": { $in: [employerId] } },  function (searchingerror, searchRating) {
               if(searchingerror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });  
               }
               else{
                   if(searchRating.length==0){
                        models.candidate.updateOne({"candidateId":candidateId}, { $push: { "favouriteBy":employerId } }, function (pushederror, pushedresponse) {
                            if(pushederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                }) 
                            }
                            else{
                                if (pushedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['200'],
                                        statuscode: 200,
                                        details: null
                                    })
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }

                        })
                   }
                   else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1032'],
                            statuscode: 1032,
                            details: null
                        })
                   }
               }

            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    getEmployedCandidateByName:(req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let serachText = req.body.searchText;
            if (!req.body.employerId) { return; } 
            let aggrQuery = [
                {
                    '$match': { 'employerId': employerId },
                    
                },
                { $unwind: "$hireList" },
                { $project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industry:"$jobDetails.industry",
                    jobDb:"$jobDetails"
                 }},
                 {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection: {$filter: {
                        input: '$jobDb.roleWiseAction',
                        as: 'jobCollection',
                        cond: {$eq: ['$$jobCollection.roleId', '$hireList.roleId']}
                    }},
                    _id: 0
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection:{ $arrayElemAt :["$jobCollection", 0]}
                }},
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                    }
                },
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    roleName:"$jobCollection.roleName",
                    paymentStatus:"$jobCollection.paymentStatus",
                    paymentStatus:"$jobCollection.paymentStatus",
                    description:"$jobCollection.description",
                    candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    roleName:1,
                    paymentStatus:1,
                    paymentStatus:1,
                    description:1,
                    fname: "$candidateDetails.fname",
                    mname: "$candidateDetails.mname",
                    lname: "$candidateDetails.lname",
                    profilePic: "$candidateDetails.profilePic",
                    isFavourite:{
                        $cond:[{$setIsSubset: [["$employerId"], "$candidateDetails.favouriteBy"]},true,false]
                    }
                    
                }},
                {
                    '$match': { 'roleName': { "$regex": serachText, "$options": "i" } }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },


    //==================

   

    //==================
    /*
        This function is written to fetch all the candidate for a particular employee
    */
     getAllCandidateForEmp:(req, res, next) => {

        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 20;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            let roleId = req.body.roleId ? req.body.roleId : '';

            let skillId = req.body.payloadSkill ? req.body.payloadSkill : '';

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let presentDate = req.body.presentDate ? req.body.presentDate : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present Date"
            });

            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - local Time Zone"
            });

            // let dateObj = jobProfileBusiness.getTimeAndDateWithTimeZone(presentDate, presentTime, localTimeZone);
            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);
            let endDate = dateObj.realDate;
            let endRealDateTimeStamp = dateObj.realDateTimeStamp;

            // res.json({
            //     dateObj: dateObj,
            //     endDate: endDate,
            //     endRealDateTimeStamp: endRealDateTimeStamp
            // });
            // return;

        

            if (!req.body.employerId) { return; } 

            let aggrQuery;

            let lookUpStmt1 =  {
                $lookup: {
                    from: "skills",
                    localField: "skills",
                    foreignField: "skillId",
                    as: "skillDetails"
                }
            };

            let groupPaginationStmt = {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            };

            let projectPaginationStmt = {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            };

            if (roleId != '') {
                console.log("h11")  
                if (skillId != '') {
                   
                    aggrQuery = [
                            lookUpStmt1,

                            {
                                $project:{
                                    _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    hiredJobs: 1,
                                    hiredJobNumber: { '$size': '$hiredJobs' },
                                    completedJob: {
                                        $filter: {
                                           input: "$hiredJobs",
                                           as: "hiredJobs",
                                           cond: { $lte: [ "$$hiredJobs.endRealDateTimeStamp", endRealDateTimeStamp ] }
                                        }
                                    },
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    isSkilled:{
                                        $cond: {
                                            "if": { "$eq": [ "$payloadSkill.jobRoleId", roleId] }, 
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                
                                },                    
                            },

                            {
                                $project:{
                                    _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    hiredJobs: 1,
                                    hiredJobNumber: { '$size': '$hiredJobs' },
                                    completedJob: 1,
                                    completedJobNumber: { '$size': '$completedJob' },
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    isSkilled:{
                                        $cond: {
                                            "if": { "$eq": [ "$payloadSkill.jobRoleId", roleId] }, 
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                
                                },                    
                            },



                            {
                              $match: {
                                'selectedRole': { $in: roleId },
                                'skills': { $in: skillId }
                              }
                            },
       
                            groupPaginationStmt,
                            projectPaginationStmt

                        ]

                } else {

                    aggrQuery = [

                            lookUpStmt1,

                            {
                                $project:{
                                    _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    hiredJobs: 1,
                                    hiredJobNumber: { '$size': '$hiredJobs' },
                                    completedJob: {
                                        $filter: {
                                           input: "$hiredJobs",
                                           as: "hiredJobs",
                                           cond: { $lte: [ "$$hiredJobs.endRealDateTimeStamp", endRealDateTimeStamp ] }
                                        }
                                    },
                                    // completedJobNumber: { '$size': '$completedJob' },
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    isSkilled:{
                                        $cond: {
                                            "if": { "$eq": [ "$payloadSkill.jobRoleId", roleId] }, 
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                
                                },                    
                            },

                            {
                                $project:{
                                    _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    hiredJobs: 1,
                                    hiredJobNumber: { '$size': '$hiredJobs' },
                                    completedJob:1,
                                    completedJobNumber: { '$size': '$completedJob' },
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                                    isSkilled:{
                                        $cond: {
                                            "if": { "$eq": [ "$payloadSkill.jobRoleId", roleId] }, 
                                            "then": true,
                                            "else": false
                                        }
                                    },
                                
                                },                    
                            },

                            {
                              $match: {
                                'selectedRole': { $in: roleId },
                                
                              }
                            },

                            groupPaginationStmt,
                            projectPaginationStmt

                        ]

                }

            } else {
                 
                    aggrQuery = [
                        lookUpStmt1,

                        {
                            $project:{
                                 _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    hiredJobs: 1,
                                    hiredJobNumber: { '$size': '$hiredJobs' },
                                    completedJob: {
                                        $filter: {
                                           input: "$hiredJobs",
                                           as: "hiredJobs",
                                           cond: { $lte: [ "$$hiredJobs.endRealDateTimeStamp", endRealDateTimeStamp ] }
                                        }
                                    },
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                            
                            },                    
                        },

                        {
                            $project:{
                                 _id:0,
                                    favouriteBy:1,
                                    candidateId:1,
                                    workExperience:1,
                                    geolocation: 1,
                                    skills:1,
                                    fname:1,
                                    mname:1,
                                    lname:1,
                                    EmailId:1,
                                    rating:1,
                                    profilePic:1,
                                    hiredJobs: 1,
                                    hiredJobNumber: { '$size': '$hiredJobs' },
                                    completedJob: 1,
                                    completedJobNumber: { '$size': '$completedJob' },
                                    selectedRole:1,
                                    skills: 1,
                                    payloadSkill: 1,
                                    skillDetails: 1,
                                    isFavourite:{
                                        $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                                    },
                            
                            },                    
                        },
                       
                        groupPaginationStmt,
                        projectPaginationStmt
                    ]

            }

            models.candidate.aggregate(aggrQuery).exec((error, data) => {
                
   
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (data.length == 0) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        }) 

                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: data[0],
                            endDate:endDate,
                            endRealDateTimeStamp: endRealDateTimeStamp
                        }) 
                    }

                    
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    

    },    


    //==================
     getFavouriteCandidateForEmp:(req, res, next) => {

        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            if (!req.body.employerId) { return; } 

            let aggrQuery = [
                {
                    $lookup: {
                        from: "skills",
                        localField: "skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project:{
                        _id:0,
                        favouriteBy:1,
                        candidateId:1,
                        workExperience:1,
                        geolocation: 1,
                        skills:1,
                        fname:1,
                        mname:1,
                        lname:1,
                        EmailId:1,
                        rating:1,
                        profilePic:1,
                        selectedRole:1,
                        skills: 1,
                        payloadSkill: 1,
                        skillDetails: 1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        },
                    
                    },                    
                },
                {
                    '$match': { 'isFavourite': true },
                    
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }


            ]
            models.candidate.aggregate(aggrQuery).exec((error, data) => {
   
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (data.length == 0) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        }) 

                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: data[0]
                        }) 
                    }

                    
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    

    },    


    //=====================
    getCandidateToMarkFavourite:(req, res, next) => {

        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            if (!req.body.employerId) { return; } 

            let aggrQuery = [
                {
                    $project:{
                        _id:0,
                        favouriteBy:1,
                        candidateId:1,
                        fname:1,
                        mname:1,
                        lname:1,
                        EmailId:1,
                        rating:1,
                        profilePic:1,
                        isFavourite:{
                            $cond:[{$setIsSubset: [[employerId], "$favouriteBy"]},true,false]
                        }
                    
                    },
                    
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }


            ]
            models.candidate.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }    

    },    



    //====================
    getEmployedCandidate: async (req, res, next) => {
        try{

            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

            if (!req.body.employerId) { return; } 

            let fetchEmployedCandidateforEmployeeStatus = await employerJobBusiness.fetchEmployedCandidateforEmployee(employerId, page, perPage);

            if (fetchEmployedCandidateforEmployeeStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });

                return;
            }


            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: fetchEmployedCandidateforEmployeeStatus.candidateList
            });

            
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },

    //============================


    /*
    getEmployedCandidate:(req, res, next) => {
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });

           
            if (!req.body.employerId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': employerId },
                    
                },
                { $unwind: "$hireList" },
                { $project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industry:"$jobDetails.industry",
                    jobDb:"$jobDetails"
                 }},
                 {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection: {$filter: {
                        input: '$jobDb.roleWiseAction',
                        as: 'jobCollection',
                        cond: {$eq: ['$$jobCollection.roleId', '$hireList.roleId']}
                    }},
                    _id: 0
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    jobCollection:{ $arrayElemAt :["$jobCollection", 0]}
                }},
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "candidateDetails"
                    }
                },
            
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    
                    roleName:"$jobCollection.roleName",
                    paymentStatus:"$jobCollection.paymentStatus",
                    paymentStatus:"$jobCollection.paymentStatus",
                    description:"$jobCollection.description",
                    candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
                }},
                {$project: {
                    hireList: 1,
                    jobId: 1,
                    employerId: 1,
                    industryId:1,
                    roleName:1,
                    paymentStatus:1,
                    paymentStatus:1,
                    description:1,
                    geolocation:"$candidateDetails.geolocation",
                    fname: "$candidateDetails.fname",
                    mname: "$candidateDetails.mname",
                    lname: "$candidateDetails.lname",
                    profilePic: "$candidateDetails.profilePic",
                   
                    isFavourite:{
                        $cond:[{$setIsSubset: [["$employerId"], "$candidateDetails.favouriteBy"]},true,false]
                    }
                    
                }},
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            

            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {

                
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
  
    */

    
    giveRating: async (req, res, next) => {
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - employerId"
            });
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - candidateId"
            });
            let review = req.body.review ? req.body.review : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - review"
            });
            let rating = req.body.rating ? req.body.rating : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - rating"
            });
            if (!req.body.roleId ||!req.body.jobId ||!req.body.employerId ||!req.body.candidateId ||!req.body.rating ||!req.body.review ) { return; }
            // for setting push notification
            let pushNotificationStatus = true;

            let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

            if (fetchPushStatus.isError == false) {
                if (fetchPushStatus.isExist) {
                    pushNotificationStatus = fetchPushStatus.pushNotification;
                }
            }
            //*******************
            models.rating.findOne({'roleId':roleId,'jobId':jobId,'employerId':employerId,"candidateId":candidateId},  function (searchingerror, searchRating) {
                if(searchingerror){
                    console.log('search error',searchingerror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if(searchRating==null){
                        console.log("no record found ,Its time to insert")
                        // no record found ,Its time to insert
                        let insertedvalue={
                            '_id': new mongoose.Types.ObjectId(),
                            "roleId":roleId,
                            "jobId":jobId,
                            "employerId":employerId,
                            "candidateId":candidateId,
                            "review":review,
                            "rating":rating
                        }
                        models.rating.create(insertedvalue, function (error, data) {
                            if(error){
                                console.log('create error',error)
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                               
                                let aggrQuery = [
                                    { $match : { 'candidateId': candidateId } },
                                    {
                                        $group :{
                                            _id :"$candidateId",
                                            allRatingAvg: { $avg: "$rating" },
                                        }
                                    }
                                ]
                                models.rating.aggregate(aggrQuery).exec((err, result) => {
                                    models.candidate.updateOne({'candidateId': candidateId},{'rating':result[0].allRatingAvg}, function (updatederror, updatedresponse) {
                                        if(updatederror){
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                        else{
                                            let employerName;
                                            if (updatedresponse.nModified == 1) {
                                                models.employer.find({"employerId":employerId },  function (error, data) {
                                                    console.log('......',data[0].companyName)
                                                    console.log('......',data[0].individualContactName)
                                                    if(data[0].companyName!==""){
                                                        employerName=data[0].companyName
                                                    }
                                                    else if(data[0].individualContactName!==""){
                                                        employerName=data[0].individualContactName
                                                    }
                                                    else{
                                                        employerName="Employer"
                                                    }
                                                models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                    if (pushNotificationStatus == true) {
                                                        fcm.sendTocken(searchRating[0].Status.fcmTocken,employerName+" gives you "+rating+" rating ");
                                                    }    
                                                    
                                                    // .then(function(nofify){
                                                    //     res.json({
                                                    //         isError: false,
                                                    //         message: errorMsgJSON['ResponseMsg']['200'],
                                                    //         statuscode: 200,
                                                    //         details: null
                                                    //     })
                                                    // })

                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: null
                                                    }) 
                                                  
                                                })
                                            })
                                                
                                               
                                            }
                                            else{
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                }) 
                                            }
                                        }
                                    })
                                  
                                })
                                
                            }
        
                        })
                    }
                       else{
                        console.log("record found ,Its time to update")
                            let updateWhere={
                                "roleId":roleId,
                                "jobId":jobId,
                                "employerId":employerId,
                                "candidateId":candidateId,
                            }
                            let updateWith={
                                "review":review,
                                "rating":rating
                            }
                            models.rating.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                                if(updatederror){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                                else{
                                    if (updatedresponse.nModified == 1) {
                                      
                                        let aggrQuery = [
                                            { $match : { 'candidateId': candidateId } },
                                            {
                                                $group :{
                                                    _id :"$candidateId",
                                                    allRatingAvg: { $avg: "$rating" },
                                                }
                                            }
                                        ]
                                        models.rating.aggregate(aggrQuery).exec((err, result) => {
                                            models.candidate.updateOne({'candidateId': candidateId},{'rating':result[0].allRatingAvg}, function (updatederror, updatedresponse) {
                                                if(updatederror){
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                                else{
                                                    let employerName1;
                                                    if (updatedresponse.nModified == 1) {
                                                        models.employer.find({"employerId":employerId },  function (error, data) {
                                                            
                                                            console.log('......',data[0].companyName)
                                                            console.log('......',data[0].individualContactName)
                                                            if(data[0].companyName!==""){
                                                                employerName1=data[0].companyName
                                                            }
                                                            else if(data[0].individualContactName!==""){
                                                                employerName1=data[0].individualContactName
                                                            }
                                                            else{
                                                                employerName1="Employer"
                                                            }
                                                        models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                            if (pushNotificationStatus == true) {
                                                                fcm.sendTocken(searchRating[0].Status.fcmTocken, employerName1 +" gives you "+rating+" rating ")
                                                            }
                                                                // .then(function(nofify){
                                                                res.json({
                                                                    isError: false,
                                                                    message: errorMsgJSON['ResponseMsg']['200'],
                                                                    statuscode: 200,
                                                                    details:null
                                                                })
                                                            // }) 
                                                          
                                                        })
                                                    })
                                                    }
                                                    else{
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                                            statuscode: 1004,
                                                            details: null
                                                        }) 
                                                    }
                                                }
                                            })
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                       }
                }
               
            })
          

        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
     /**
     * @abstract 
     * Update Job
     */
    updateJob: async (req, res, next) => {
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            let jobDetails = req.body.jobDetails ? req.body.jobDetails : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - jobDetails"
            });
            
            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                serverResponse: {
                  isError: true,
                  message: errorMsgJSON[lang]["303"] + " - local Time Zone",
                  statuscode: 303
                }
            });

            if (!req.body.roleId || !req.body.jobId || !req.body.localTimeZone) { return; }
        
            getDiffFetch = await jobProfileBusiness.getDifferenceBetweenTimeZone({
                localTimeZone: localTimeZone
            });

            let diffInTimeStamp = getDiffFetch.diffInTimeStamp;

            let updateWith={
                "jobDetails.roleWiseAction.$.skills":jobDetails.skills,
                "jobDetails.roleWiseAction.$.roleId":jobDetails.roleId,
                "jobDetails.roleWiseAction.$.roleName": jobDetails.roleName,
                "jobDetails.roleWiseAction.$.jobType": jobDetails.jobType,
                "jobDetails.roleWiseAction.$.payType": jobDetails.payType,
                "jobDetails.roleWiseAction.$.pay": jobDetails.pay,
                "jobDetails.roleWiseAction.$.noOfStuff": jobDetails.noOfStuff,
                "jobDetails.roleWiseAction.$.uploadFile": jobDetails.uploadFile,
                "jobDetails.roleWiseAction.$.paymentStatus": jobDetails.paymentStatus,
                "jobDetails.roleWiseAction.$.status": jobDetails.status,
                "jobDetails.roleWiseAction.$.location": jobDetails.location,
                "jobDetails.roleWiseAction.$.payment": jobDetails.payment,
                "jobDetails.roleWiseAction.$.locationName": jobDetails.locationName,
                "jobDetails.roleWiseAction.$.description": jobDetails.description,
                "jobDetails.roleWiseAction.$.distance": jobDetails.distance,
                "jobDetails.roleWiseAction.$.setTime": jobDetails.setTime,
                "jobDetails.roleWiseAction.$.locationTrackingRadius": jobDetails.locationTrackingRadius,  
            }
            
            let updateWhere = {
                "jobId":jobId,
                "jobDetails.roleWiseAction.roleId":roleId,      
            }

            let updatejobStatus = await jobProfileBusiness.updatejob(updateWith, updateWhere);

            if (updatejobStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1004'],
                    statuscode: 1004,
                    details: null
                })

                return;
            }

            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
            ];    

            let fetchJobStatus = await jobProfileBusiness.fetchJob(aggrQuery);

            if (fetchJobStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1004'],
                    statuscode: 1004,
                    details: null
                })

                return;
            }

            let roles = fetchJobStatus.response[0].jobDetails.roleWiseAction;
            let jobDetailsToUpdate = fetchJobStatus.response[0].jobDetails;
            roleStartTimes = [];
            roleEndTimes = [];

            //==================================================================
            let j;
            for (j = 0; j < roles.length; j++) {
                let setTime = roles[j].setTime;
                let serTimeObj = jobProfileBusiness.getStartAndEndTimeOfRole(setTime, diffInTimeStamp);

                roles[j].startRealDate = serTimeObj.startRealDate; 
                roles[j].startRealDateUtc = serTimeObj.startRealDateUtc; 
                roles[j].startRealDateTimeStamp = serTimeObj.startRealDateTimeStamp; 
                roles[j].endRealDate = serTimeObj.endRealDate; 
                roles[j].endRealDateUtc = serTimeObj.endRealDateUtc; 
                roles[j].endRealDateTimeStamp = serTimeObj.endRealDateTimeStamp;
                roles[j].setTime = serTimeObj.setTime;  

                roleStartTimes.push(roles[j].startRealDateTimeStamp);     
                roleEndTimes.push(roles[j].endRealDateTimeStamp);                    
            } // ENd for (j = 0; j < jobdetails.length; i++) {

            // reassigning the updated roles in rolewiseaction
            jobDetailsToUpdate.roleWiseAction = roles;

            jobStartDateTimeStamp = Math.min(...roleStartTimes); 
            jobStartDateUtc = new Date(jobStartDateTimeStamp);
            jobStartDateTimeStampLocal = jobStartDateUtc.toLocaleString();
            jobDetailsToUpdate.jobStartDateTimeStamp = jobStartDateTimeStamp;
            jobDetailsToUpdate.jobStartDateUtc = jobStartDateUtc;
            jobDetailsToUpdate.jobStartDateTimeStampLocal = jobStartDateTimeStampLocal;

            jobEndDateTimeStamp = Math.max(...roleEndTimes); 
            jobEndDateUtc = new Date(jobEndDateTimeStamp);
            jobEndDateTimeStampLocal = jobEndDateUtc.toLocaleString();
            jobDetailsToUpdate.jobEndDateTimeStamp = jobEndDateTimeStamp;
            jobDetailsToUpdate.jobEndDateUtc = jobEndDateUtc;
            jobDetailsToUpdate.jobEndDateTimeStampLocal = jobEndDateTimeStampLocal;

            updateWith={
                "jobDetails":jobDetailsToUpdate,
                "jobStartDateTimeStamp": jobStartDateTimeStamp,
                "jobStartDateUtc": jobStartDateUtc,
                "jobStartDateTimeStampLocal": jobStartDateTimeStampLocal,
                "jobEndDateTimeStamp": jobEndDateTimeStamp,
                "jobEndDateUtc": jobEndDateUtc,
                "jobEndDateTimeStampLocal": jobEndDateTimeStampLocal
            }
            
            updateWhere = {
                "jobId":jobId  
            }

            updatejobStatus = await jobProfileBusiness.updatejob(updateWith, updateWhere);
               
            if (updatejobStatus.isError == true) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1004'],
                    statuscode: 1004,
                    details: null
                })

                return;
            }

            res.json({
                isError: false,
                message: errorMsgJSON['ResponseMsg']['200'],
                statuscode: 200,
                details: null
            })
            return;
        
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     * Edit Job
     */
    editJob:(req, res, next) => {
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.roleId || !req.body.jobId) { return; } 
            // let findWhere={
            //     "jobId":jobId,
            //     "jobDetails.roleWiseAction.roleId":roleId,

            // }
            // models.job.findOne(findWhere,{ 'jobDetails.roleWiseAction.$': 1 }, function (err, item) {
            //     if(err){
            //         res.json({
            //             isError: true,
            //             message: errorMsgJSON['ResponseMsg']['1021'],
            //             statuscode: 1021,
            //             details: null
            //         });
            //     }
            //     else{
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details: item
            //         })
            //     }

            // })
            let aggrQuery = [
                {
                    '$match': { 'jobId': jobId }
                },
                /********************** new Approach avoiding unwind, Starts******************** */
                {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                    }
                },
                { $redact : {
                    $cond: {
                        if: { $or : [{ $eq: ["$roleId",roleId] }, { $not : "$roleId" }]},
                        then: "$$DESCEND",
                        else: "$$PRUNE"
                    }
               }},
               {
                    $project: {
                       
                        "industry": "$jobDetails.industry",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails"
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },

                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project: {
                        "_id":0,
                        "jobId": 1,
                        "employerId": 1,
                        "skillDetails": 1,
                        "jobDetails":{
                            "roleWiseAction":{ $arrayElemAt :["$jobDetails.roleWiseAction", 0]},
                            "industryDetails":{ $arrayElemAt :["$industryDetails", 0]}
                        },
                       
                    }
                }
                /********************** new Approach avoiding unwind ends******************** */
                // { $unwind: "$jobDetails.roleWiseAction" },
                // {
                //     $project: {
                //         "_id":0,
                //         "jobId": 1,
                //         "employerId": 1,
                //         "jobDetails": 1,
                //     }
                // },
                // {
                //     $group: {
                //         "_id": {
                //             "roleId": "$jobDetails.roleWiseAction.roleId",
                //             "jobId": "$jobId"
                //         },
                //         "jobDetails": {
                //             $first: "$jobDetails"
                //         },
                //         "jobId": {
                //             $first: "$jobId"
                //         },
                //         "employerId": {
                //             $first: "$employerId"
                //         },
                //     }
                // },
                // {
                //     $project: {
                //         "_id":0,
                //         "jobId": 1,
                //         "employerId": 1,
                //         "jobDetails": 1,
                //     }
                // },
                // { $match : { 'jobDetails.roleWiseAction.roleId': roleId } },
                // {
                //     $project: {
                       
                //         "industry": "$jobDetails.industry",
                //         "jobId": "$jobId",
                //         "employerId": "$employerId",
                //         "jobDetails": "$jobDetails"
                //     }
                // },
                // {
                //     $lookup: {
                //         from: "industries",
                //         localField: "industry",
                //         foreignField: "industryId",
                //         as: "industryDetails"
                //     }
                // },
                // {
                //     $project: {
                //         "_id":0,
                //         "jobId": 1,
                //         "employerId": 1,
                //         "jobDetails": 1,
                //         "industryDetails":{ $arrayElemAt :["$industryDetails", 0]}
                //     }
                // },
               
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     *HIRE CANDIDATE
     * According to logic after hire details of accept list in job collection must moved ito hire list in job collection
     * this is why first doing pull and then pull operation on job collection
     * to reflect in candidate section again the details of acceptedJobs in candidate collection
     * moved into hiredJobs array of candidate collection.
    */
    hireCandidate:(req, res, next) => {
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
            });

            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
            if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }
            models.job.updateOne({"jobId":jobId},{ "$pull": { "acceptList": { "candidateId": candidateId ,"roleId":roleId} }}, { safe: true, multi:true }, function (deletedderror, deletedresponse) {
                if (deletedderror) {
                    console.log(deletedderror)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    if (deletedresponse.nModified == 1) {
                        let insertedpushValue={
                            "roleId": roleId,
                            "candidateId":candidateId
                        }
                        models.job.updateOne({"jobId":jobId}, { $push: { "hireList":insertedpushValue } }, function (pushederror, pushedresponse) {
                            if (pushederror) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                if (pushedresponse.nModified == 1) {
                                    models.candidate.updateOne({"candidateId":candidateId},{ "$pull": { "acceptedJobs": { "jobId": jobId ,"roleId":roleId,"employerId":employerId} }}, { safe: true, multi:true }, function (candidateDeleteddError, candidateDeletedResponse) {
                                      if(candidateDeleteddError){
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                      } 
                                      else{
                                            if (candidateDeletedResponse.nModified == 1) {
                                                let candidateInsertedValue={
                                                    "roleId": roleId,"employerId":employerId,"jobId":jobId,"candidateId":candidateId
                                                }
                                                models.candidate.updateOne({ 'candidateId': candidateId }, { $push: { "hiredJobs":candidateInsertedValue } }, function (candidateUpdatedError, candidateUpdatedResponse) {
                                                    if(candidateUpdatedError){
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: null
                                                        });
                                                    }
                                                    else{
                                                        if (candidateUpdatedResponse.nModified == 1) {
                                                            res.json({
                                                                isError: false,
                                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                                statuscode: 200,
                                                                details: null
                                                            });
                                                        }
                                                        else{
                                                            res.json({
                                                                isError: true,
                                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                                statuscode: 1004,
                                                                details: null
                                                            })      
                                                        }
                                                    } 
                                                })
                                            }
                                            else{
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                                    statuscode: 1004,
                                                    details: null
                                                })   
                                            }
                                        } 
                                    }) 
                                }
                                else{
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })   
                                }
                            }
                        })
                    }else{
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })  
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        } 
    },
    serachOngingJobByName:(req, res, next) => {
        
        try{
            let date=Math.floor(Date.now() / 1000);
            let FinalDate=date*1000;
            console.log(FinalDate)
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $gt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$hireList" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$setTime" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":1,
                        "industryDetails":1
                    }
                },
                { '$match': { 
                    'hireListCount': {"$eq":false}}
                },
                {
                    '$match': { 'roleName': { "$regex": serachText, "$options": "i" } }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
               
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
        console.log(2,error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: error
            });
        }
    },
    /**
     * @abstract 
     * SEARCH BY JOB ROLE NAME
     * role wise separation jobs and match given role name with all existing roles in jobs and fetch 
     * the matched jobs 
     * HERE before search insert searched job role name(IF NOT EXIST SAME NAME IN ARRAY) in resentSearchDetails array in employer collection by calling 
     * inserSearchTextForHistory function.
    */
    serachJobByName1:(req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                         "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $project: {
                        "jobId": "$jobId",
                        "location": "$jobDetails.roleWiseAction.location",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime": "$jobDetails.roleWiseAction.setTime",
                        "payment": "$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus": "$jobDetails.roleWiseAction.paymentStatus",
                         "status": "$jobDetails.roleWiseAction.status",
                        "createdAt": "$createdAt",
                        "appiliedFrom": "$appiliedFrom",
                        "approvedTo": "$approvedTo",
                        "skillDetails": "$skillDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "roleId":1,
                        "roleName":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "roleId":1,
                        "roleName":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails": {$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    '$match': { 'jobDetails.roleWiseAction.roleName': { "$regex": serachText, "$options": "i" } }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* search job by name based on old structure*/ 
    serachJobByName:(req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": {$arrayElemAt : ["$payment",0]},
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails": {$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    '$match': { 'jobDetails.roleWiseAction.roleName': { "$regex": serachText, "$options": "i" } }
                },
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET LAST 10 SEARCHED JOB ROLE NAME 
     */
    getLastSearchNameDetails:(req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind : {path:"$resentSearchDetails",preserveNullAndEmptyArrays:true} },
                {
                    "$sort": {"resentSearchDetails.time": -1}
                },
                {
                   "$limit": 10
                },
                {
                    $project: {
                        resentSearchDetails:1,
                        _id:0,
                        employerId:1,
                    }
                },
                {
                    $group: {
                        "_id": {
                            "employerId": "$employerId",
                        },
                        "roleName": {
                            $push: "$resentSearchDetails.name"
                        }
                    }
                }
            ]
            models.employer.aggregate(aggrQuery).exec((err, result) => {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                   
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details:result[0]
                    });
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    searchPostedJob:(req, res, next) => {
        try{
            console.log('calling')
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let serachText = req.body.serachText ? req.body.serachText : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Search Text"
            });
            if (!req.body.employerId || !req.body.serachText) { return; }
            models.employer.find({'employerId': empId,"resentSearchDetails.name": { "$in":serachText } },
             function (err, item) {
                
                if(item.length==0){
                   
                    let updateWith={
                        "name": serachText,
                    }
                    models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
                else{
                    var searchValue = serachText.toLowerCase();
                    models.employer.updateOne({ 'employerId': empId },{ "$pull": { "resentSearchDetails": { "name": searchValue } }},{ safe: true }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                 
                                let updateWith={
                                    "name": serachText,
                                }
                                
                                models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, function (updatederror, updatedresponse) {
                                    if (updatederror) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                    else {
                                        if (updatedresponse.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * JOB APPROVE TO CANDIDATE 
     * check if job is approved to candidate earlier or not by checking roleId,candidateId,jobId from 
     * approveTo array.If not apparove earlier insert values (roleId,candidateId,jobId) in approvedTo array in job collection.
     * At the same time insert same values in candidate joboffers array.
     * Send mail or sms to candidate
     */
    jobApproveTo: async (req, res, next) => {
        try{
                let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " -candidate Id"
                });

                let roleId = req.body.roleId ? req.body.roleId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
                });
                let employerId = req.body.employerId ? req.body.employerId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
                });
                let jobId = req.body.jobId ? req.body.jobId : res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
                });
                if (!req.body.candidateId || !req.body.roleId || !req.body.employerId || !req.body.jobId) { return; }

                // for setting push notification
                let pushNotificationStatus = true;

                let fetchPushStatus = await employerJobBusiness.fetchPushNotification(candidateId);

                if (fetchPushStatus.isError == false) {
                    if (fetchPushStatus.isExist) {
                        pushNotificationStatus = fetchPushStatus.pushNotification;
                    }
                }
                //*******************

                let updateWith={
                    'roleId':roleId,
                    'candidateId':candidateId
                }
                let findwhere={
                    'approvedTo.roleId':roleId,
                    'approvedTo.candidateId':candidateId,
                    'jobId': jobId
                }
                models.job.findOne({'jobId': jobId }, function (error, itemjob){
                    if(error){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        }); 
                    }
                    else{
                        models.job.findOne(findwhere,{ 'approvedTo.$': 1 }, function (err, item) {
                            
                            if(err){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else{
                                if(item){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1030'],
                                        statuscode: 1030,
                                        details: null
                                    })
                                }
                                else{
                         
                                    models.job.updateOne({ 'employerId': employerId,'jobId':jobId },
                                        {$push: {'approvedTo':updateWith}}, 
                                        function (error, response) {
                                        if(error){
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            });
                                        }
                                        else{
                                            let insertedValue={
                                                "roleId": roleId,"employerId":employerId,
                                                "jobId":jobId,"candidateId":candidateId
                                                
                                            }
                                            models.candidate.updateOne({ 'candidateId': candidateId }, 
                                                { $push: { "jobOffers":insertedValue } }, 
                                                function (updatederror, updatedresponse) {
                                                if (updatederror) {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    });
                                                }
                                                if (updatedresponse.nModified == 1) {
                                                    let employerName;
                                                    models.authenticate.findOne({'userId':candidateId}, 
                                                        async function (err, item) {
                                                        if(item['mobileNo']==""){
                                                            var result = await  sendNotificationEmail(item['EmailId'],candidateId,jobId,roleId)
                                                            if(result){
                                                                models.employer.find({"employerId":employerId },  function (error, data) {
                                                                    console.log('......',data[0].companyName)
                                                                    console.log('......',data[0].individualContactName)
                                                                    if(data[0].companyName!==""){
                                                                        employerName=data[0].companyName
                                                                    }
                                                                    else if(data[0].individualContactName!==""){
                                                                        employerName=data[0].individualContactName
                                                                    }
                                                                    else{
                                                                        employerName="Employer"
                                                                    }
                                                                models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                                    if (pushNotificationStatus == true) {
                                                                        fcm.sendTocken(searchRating[0].Status.fcmTocken,employerName+" approves your job application")
                                                                    }
                                                                        // .then(function(nofify){
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                                            statuscode: 200,
                                                                            details: null
                                                                        })
                                                                    // }) 
                                                                  
                                                                })
                                                                })
                                                            }
                                                            else{
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                        else{
                                                            var result = await  sendNotificationSms(item['mobileNo'],candidateId,jobId,roleId)
                                                            if(result){
                                                                models.employer.find({"employerId":employerId },  function (error, data) {
                                                                    console.log('......',data[0].companyName)
                                                                    console.log('......',data[0].individualContactName)
                                                                    if(data[0].companyName!==""){
                                                                        employerName=data[0].companyName
                                                                    }
                                                                    else if(data[0].individualContactName!==""){
                                                                        employerName=data[0].individualContactName
                                                                    }
                                                                    else{
                                                                        employerName="Employer"
                                                                    }
                                                                models.candidate.find({"candidateId":candidateId },  function (searchingerror, searchRating) {
                                                                    if (pushNotificationStatus == true) {
                                                                        fcm.sendTocken(searchRating[0].Status.fcmTocken,employerName+" approves your job application")
                                                                    }
                                                                        // .then(function(nofify){
                                                                        res.json({
                                                                            isError: false,
                                                                            message: errorMsgJSON['ResponseMsg']['200'],
                                                                            statuscode: 200,
                                                                            details: null
                                                                        })
                                                                    // }) 
                                                                  
                                                                })
                                                                })
                                                            }
                                                            else{
                                                                res.json({
                                                                    isError: true,
                                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                                    statuscode: 404,
                                                                    details: null
                                                                });
                                                            }
                                                        }
                                                    })
                                                }
                                                else{
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    }) 
                                                }
                                            })
                                        }
                                    })
                                }
                            }
        
                        })
                    }
                })
            }
            catch (error) {
                console.log(error)
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }
    },
     /**
     * @abstract 
     * GET TEMPLATE
     * fetch templateby its id that was saved during job post 
     */
    getTemplate: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let templateId = req.body.templateId ? req.body.templateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Template Id"
            });
            if (!req.body.employerId || !req.body.templateId) { return; }
            // models.employer.findOne({ 'employerId': empId },
            //     function (err, item) {
            //         if (err) {
            //             res.json({
            //                 isError: true,
            //                 message: errorMsgJSON['ResponseMsg']['404'],
            //                 statuscode: 404,
            //                 details: null
            //             });
            //         }
            //         let itemTemplates = item['templates'];
            //         let filteredTemplate = itemTemplates.filter(eachTmp => {
            //             if (eachTmp.templateId == templateId) {
            //                 return eachTmp;
            //             }
            //         })
            //         delete item['templates'];
            //         item['templates'] = filteredTemplate;
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details:item['templates'][0]
            //         })
            //     })

            let aggrQuery=[
                {
                    '$match': { 'employerId': empId },
                },
                { $project: {
                    templates:1
                 }},
                 { $unwind: "$templates" },
                 { $unwind: "$templates.jobDetails.roleWiseAction" },

                {
                    $lookup: {
                        from: "skills",
                        localField: "templates.jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                
                 {
                    $group: {
                        "_id": {
                            "templateId": "$templates.templateId",

                          
                         },
                         "rolewiseSkills":{
                            $push: {
                                "updatedAt":"",
                                "createdAt":"",
                                "industryId":"$templates.jobDetails.industry",
                                "jobRoleId":"$templates.jobDetails.roleWiseAction.roleId",
                                "jobRoleName":"$templates.jobDetails.roleWiseAction.roleName",
                                "skillIds": "$templates.jobDetails.roleWiseAction.skills",
                               
                                
                            }
                         },
                         "skills":{
                            $push: {
                                
                                "skillDetails": "$skillDetails"                              
                            }
                         },
                         "locationTrackingRadius": {
                             $first: "$templates.locationTrackingRadius"
                         },
                         "location":{
                            $first: "$templates.jobDetails.roleWiseAction.location"
                         },
                         "locationName":{
                            $first: "$templates.jobDetails.roleWiseAction.locationName"
                         },
                        
                        "industry": {
                            $first: "$templates.jobDetails.industry"
                        },
                        "templateName": {
                            $first: "$templates.templateName"
                        },
                        "templateId": {
                            $first: "$templates.templateId"
                        },
                        "payment": {
                            $first: "$templates.jobDetails.roleWiseAction.payment"
                        },
                        "setTime": {
                            $first: "$templates.jobDetails.roleWiseAction.setTime"
                        },
                        "description": {
                            $first: "$templates.jobDetails.roleWiseAction.description"
                        },
                        "distance": {
                            $first: "$templates.jobDetails.roleWiseAction.distance"
                        },
                        "jobType": {
                            $first: "$templates.jobDetails.roleWiseAction.jobType"
                        },
                        "payType": {
                            $first: "$templates.jobDetails.roleWiseAction.payType"
                        },
                        "pay": {
                            $first: "$templates.jobDetails.roleWiseAction.pay"
                        },
                        "noOfStuff": {
                            $first: "$templates.jobDetails.roleWiseAction.noOfStuff"
                        },
                        "uploadFile": {
                            $first: "$templates.jobDetails.roleWiseAction.uploadFile"
                        },
                        "paymentStatus": {
                            $first: "$templates.jobDetails.roleWiseAction.paymentStatus"
                        },
                        "status": {
                            $first: "$templates.jobDetails.roleWiseAction.status"
                        },
                        
                    }
                },
                { $project: {
                        _id:0,
                        rolewiseSkills:1,
                        skills: 1,
                        locationTrackingRadius:1,
                        location:1,
                        industry:1,
                        templateName:1,
                        templateId:1,
                        payment:1,
                        setTime:1,
                        description:1,
                        distance:1,
                        jobType:1,
                        payType:1,
                        pay:1,
                        noOfStuff:1,
                        uploadFile:1,
                        paymentStatus:1,
                        status:1,
                        locationName:1
                       
                        
                    }
                },
               
                { $match : { 'templateId': templateId } }
                
            ]
            models.employer.aggregate(aggrQuery).exec((error, data) => {

                

                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * ADD TEMPLATE
     * Check if any template with given templte name is exist or not
     * If not add jobdetails as a new template
     */
    addTemplate: (req, res, next) => {
        try {
            let autoempid;
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let templateName = req.body.templateName ? req.body.templateName : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Template Name"
            });
            let jobDetails = req.body.jobDetails ? req.body.jobDetails : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - JobDetails"
            });

            let locationTrackingRadius = req.body.jobDetails.locationTrackingRadius ? req.body.jobDetails.locationTrackingRadius :'';

            let type = "TEMPLATE";
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autoempid = data;
            })
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    models.employer.findOne({ 'employerId': empId, "templates.templateName": templateName }, function (err, item) {
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (item == null) {

                                let insertquery = {
                                    "templateName": templateName,
                                    "jobDetails": jobDetails, 
                                    "templateId":autoempid,
                                    "locationTrackingRadius": locationTrackingRadius
                                }

                                models.employer.updateOne({ 'employerId': empId }, { $push: { "templates": insertquery } }, function (updatederror, updatedresponse) {
                                    console.log('............',updatederror)
                                    if (updatederror) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                    else {
                                        if (updatedresponse.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1024'],
                                    statuscode: 1024,
                                    details: null
                                })
                            }
                        }
                    })
                 }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET APPLIED CANDIDATE
     * jobid and empId wise separate jobs
     * fetch appliedfrom array from each job
     * show only those object in the array whose roleId match the given roleId 
     */
   getAppliedCandidate1: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - JobId"
            });
            if (!req.body.employerId || !req.body.roleId || !req.body.jobId) { return; }
            let aggrQuery = [
                {
                    '$match':{ $and: [ { 'employerId': empId },
                                       {'jobId':jobId},
                                    ]
                             }
                },
                 { $unwind: "$jobDetails.roleWiseAction" },
                 { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "acceptList":1,
                        "approvedTo": 1,
                        "rejectList":1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $project:{
                        "skillDetails":"$skillDetails",
                        "rejectList":"$rejectList",
                        "approvedTo":"$approvedTo",
                        "acceptList":"$acceptList",
                        "hireList":"$hireList",
                        "appiliedFrom":"$appiliedFrom",
                        "createdAt":"$createdAt",
                        "jobId":"$jobId",
                        "employerId":"$employerId",
                        "jobDetails":"$jobDetails",
                        "location": "$jobDetails.roleWiseAction.location",
                        "description": "$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "status":"$jobDetails.roleWiseAction.status",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobRoleIcon":"$jobDetails.roleWiseAction.jobRoleIcon",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "jobRoleIcon": {
                            $first: "$jobRoleIcon"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails":{
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "hireList":{
                            $first: "$hireList"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "acceptList": {
                            $first: "$acceptList"
                        },
                        "rejectList": {
                            $first: "$rejectList"
                        },
                        
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    },
                    
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                         "jobId": 1,
                         "location": 1,
                         "employerId": 1,
                         "description": 1,
                         "setTime": 1,
                         "payment": 1,
                         "uploadFile": 1,
                         "paymentStatus": 1,
                         "status": 1,
                         "createdAt": 1,
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "rejectList":{ $filter: { input: "$rejectList", as: "rejectList", cond: { $eq: ["$$rejectList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "acceptList":{ $filter: { input: "$acceptList", as: "acceptList", cond: { $eq: ["$$acceptList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1,
                        "roleId":1,
                        "roleName":1,
                        "jobRoleIcon": 1,
                        "jobType":1,
                        "payType":1,
                        "pay":1,
                        "noOfStuff": 1,
                        "distance":1,
                         "jobDetails":1
                        
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "approvedTo.candidateId",
                        foreignField: "candidateId",
                        as: "approvedTo"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "acceptList.candidateId",
                        foreignField: "candidateId",
                        as: "acceptList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "rejectList.candidateId",
                        foreignField: "candidateId",
                        as: "rejectList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "hireList"
                    }
                },
                {
                    $project: {
                        "approvedTo.candidateId": 1,
                        "approvedTo.fname": 1,
                        "approvedTo.mname": 1,
                        "approvedTo.lname": 1,
                        "approvedTo.profilePic":1,
                        "approvedTo.EmailId":1,
                        "approvedTo.mobileNo":1,
                        "approvedTo.memberSince":1,
                        "hireList.candidateId": 1,
                        "hireList.fname": 1,
                        "hireList.mname": 1,
                        "hireList.lname": 1,
                        "hireList.profilePic":1,
                        "hireList.EmailId":1,
                        "hireList.mobileNo":1,
                        "hireList.memberSince":1,
                        "rejectList.candidateId": 1,
                        "rejectList.fname": 1,
                        "rejectList.mname": 1,
                        "rejectList.lname": 1,
                        "rejectList.profilePic":1,
                        "rejectList.EmailId":1,
                        "rejectList.mobileNo":1,
                        "rejectList.memberSince":1,
                        "acceptList.candidateId": 1,
                        "acceptList.fname": 1,
                        "acceptList.mname": 1,
                        "acceptList.lname": 1,
                        "acceptList.profilePic":1,
                        "acceptList.EmailId":1,
                        "acceptList.mobileNo":1,
                        "acceptList.memberSince":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "appiliedFrom.profilePic":1,
                        "appiliedFrom.EmailId":1,
                        "appiliedFrom.mobileNo":1,
                        "appiliedFrom.memberSince":1,
                        "appiliedFrom.favouriteBy": 1,
                        // "hireList.isFavourite":{
                        //     $cond:[{$setIsSubset: [["$employerId"], "$hireList.favouriteBy"]},true,false]
                        // },
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        // "approvedTo": 1,
                        "roleId":1,
                        "roleName":1,
                        "jobRoleIcon":1,
                        "jobType":1,
                        "payType":1,
                        "pay":1,
                        "noOfStuff": 1,
                        "distance":1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                
                { $match : { 'jobDetails.roleWiseAction.roleId': roleId } }
                
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })

                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /*getAppliedCandidate based on old structure*/
    getAppliedCandidate: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - JobId"
            });
            if (!req.body.employerId || !req.body.roleId || !req.body.jobId) { return; }
            let aggrQuery = [
                {
                    '$match':{ $and: [ { 'employerId': empId },
                                       {'jobId':jobId},
                                    ]
                             }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "acceptList":1,
                        "approvedTo": 1,
                        "rejectList":1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },

                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "hireList":{
                            $first: "$hireList"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "acceptList": {
                            $first: "$acceptList"
                        },
                        "rejectList": {
                            $first: "$rejectList"
                        },
                        
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    },
                    
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "rejectList":{ $filter: { input: "$rejectList", as: "rejectList", cond: { $eq: ["$$rejectList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "acceptList":{ $filter: { input: "$acceptList", as: "acceptList", cond: { $eq: ["$$acceptList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "acceptList.candidateId",
                        foreignField: "candidateId",
                        as: "acceptList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "rejectList.candidateId",
                        foreignField: "candidateId",
                        as: "rejectList"
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "hireList.candidateId",
                        foreignField: "candidateId",
                        as: "hireList"
                    }
                },
                {
                    $project: {
                        "hireList.candidateId": 1,
                        "hireList.fname": 1,
                        "hireList.mname": 1,
                        "hireList.lname": 1,
                        "rejectList.candidateId": 1,
                        "rejectList.fname": 1,
                        "rejectList.mname": 1,
                        "rejectList.lname": 1,
                        "acceptList.candidateId": 1,
                        "acceptList.fname": 1,
                        "acceptList.mname": 1,
                        "acceptList.lname": 1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": {$arrayElemAt : ["$payment",0]},
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                { $match : { 'jobDetails.roleWiseAction.roleId': roleId } }
                
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getClosedJobs: (req, res, next) => {
        try{
            // let date=Math.floor(Date.now() / 1000);
            // let FinalDate=date*1000;
           
           let empId = req.body.employerId ? req.body.employerId : res.json({
               isError: true,
               message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
           });
           let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
           let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let presentDate = req.body.today ? req.body.today : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present date"
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);
       
            //  let date=Math.floor(Date.now() / 1000);
            //  let FinalDate=date*1000;
            let FinalDate = dateObj.realDateTimeStamp;
            let today = dateObj

            let dateUtc = new Date(FinalDate);
            let dateLocal = dateUtc.toLocaleString();

            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,

                        // "jobStartDateTimeStamp": 1,
                        // "jobStartDateUtc": 1,
                        // "jobStartDateTimeStampLocal": 1,
                        // "jobEndDateTimeStamp": 1,
                        // "jobEndDateUtc": 1,
                        // "jobEndDateTimeStampLocal": 1,

                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                
                {
                    "$project":{
                        "jobId":"$jobId",

                        "isDeleted": "$jobDetails.roleWiseAction.isDeleted",
                        "jobStartDateTimeStamp": "$jobDetails.roleWiseAction.startRealDateTimeStamp",
                        "jobStartDateUtc": "$jobDetails.roleWiseAction.startRealDateUtc",
                        "jobStartDateTimeStampLocal": "$jobDetails.roleWiseAction.startRealDate",
                        "jobEndDateTimeStamp": "$jobDetails.roleWiseAction.endRealDateTimeStamp",
                        "jobEndDateUtc": "$jobDetails.roleWiseAction.endRealDateUtc",
                        "jobEndDateTimeStampLocal": "$jobDetails.roleWiseAction.endRealDate",
                            
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },

                        "isDeleted": {
                            $first: "$isDeleted"
                        },

                        "jobStartDateTimeStamp": {
                            $first: "$jobStartDateTimeStamp"
                        },
                        "jobStartDateUtc": {
                            $first: "$jobStartDateUtc"
                        },
                        "jobStartDateTimeStampLocal": {
                            $first: "$jobStartDateTimeStampLocal"
                        },
                        "jobEndDateTimeStamp":{
                            $first: "$jobEndDateTimeStamp"
                        },
                        "jobEndDateUtc": {
                            $first: "$jobEndDateUtc"
                        },
                        "jobEndDateTimeStampLocal": {
                            $first: "$jobEndDateTimeStampLocal"
                        },

                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
            
                
                {
                    $project: {
                        "jobType": 1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,

                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "firstSetTime": 1,
                        "startDate": {$toDate: "$firstSetTime.startDate"},
                        "startTime": "$firstSetTime.startTime",
                        "setTime":1,
                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime1":  {
                            "$map": {
                            "input": {
                                "$filter": {
                                "input": "$setTime",
                                "as": "item",
                                "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                            },
                            "as": "finalItem",
                            "in": {
                                "endDate": "$$finalItem.endDate",
                                
                            }
                            }
                        },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireList":1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                
                {
                    $project: {
                        "jobType": 1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,

                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "firstSetTime": 1,
                        "startDate": 1,
                        "startTime": 1,
                        "setTime1":1,
                        "setTimeCount":{$size: "$setTime1"},
                        "setTime":1,
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireList":1,
                        "industryDetails":1,
                    
                    }
                },

                { $sort : { createdAt : -1 } },

                {
                    $match:{
                        $and: [
                            { 'isDeleted': { $ne: "YES"} },
                            // { 'jobEndDateTimeStampLocal': { $lt: dateLocal} },
                            { 'jobEndDateTimeStamp': { $lt: FinalDate} },
                        ]
                    } 
                },  
                
                // {$match:{$expr:{$eq:["$setTimeCount","$originalsetTimeCount"]}}},
                
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            

        ]
        models.job.aggregate(aggrQuery).exec((error, data) => {

            if (error) {
                console.log(error)
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['404'],
                    statuscode: 404,
                    details: null
                });
            }
            else {
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: data[0],
                    today: today,
                    dateLocal: dateLocal,
                    FinalDate: FinalDate
                })
            }
        })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    serachCompletedJobByName: (req, res, next) => {
        try{
            let date=Math.floor(Date.now() / 1000);
            let FinalDate=date*1000;
            console.log(FinalDate)
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let serachText = req.body.searchText;
            if (!req.body.employerId ) { return; }
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireList":1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
                {
                    $project: {
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{$size: "$setTime"},
                        "setTime":1,
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireList":1,
                        "industryDetails":1
                    }
                },
                {$match:{$expr:{$eq:["$setTimeCount","$originalsetTimeCount"]}}},
                {
                    '$match': { 'roleName': { "$regex": serachText, "$options": "i" } }
                },
               
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggrQuery).exec(async(err, result) => {
               
               
                var resultfromFunction = await  inserSearchTextForHistory(empId,serachText);
                if(resultfromFunction){
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: result[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    getOngoingJobs: (req, res, next) => {

        try{

            let presentDate = req.body.today ? req.body.today : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present date"
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - local Time Zone"
            });

            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime)
           
            //  let date=Math.floor(Date.now() / 1000);
            //  let FinalDate=date*1000;
            let FinalDate = dateObj.realDateTimeStamp;
            let today = dateObj
    
            let dateUtc = new Date(FinalDate);
            let dateLocal = dateUtc.toLocaleString();

            let diffArr = jobProfileBusiness.getDifferenceBetweenTimeZone({
                "localTimeZone": localTimeZone
            });
            let diffInTimeStamp = diffArr.diffInTimeStamp;

            let convertedTimeStamp = Number(FinalDate) + Number(diffInTimeStamp);
            // FinalDate = convertedTimeStamp;

            // let today = new Date();
            let yesterday = new Date(today)
            yesterday.setDate(yesterday.getDate() - 1)

            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            if (!req.body.employerId ) { return; }
           
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        // "jobStartDateTimeStamp": 1,
                        // "jobStartDateUtc": 1,
                        // "jobStartDateTimeStampLocal": 1,
                        // "jobEndDateTimeStamp": 1,
                        // "jobEndDateUtc": 1,
                        // "jobEndDateTimeStampLocal": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                
                
                {
                    "$project":{
                        "jobId":"$jobId",
                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",

                        "isDeleted": "$jobDetails.roleWiseAction.isDeleted",
                        "jobStartDateTimeStamp": "$jobDetails.roleWiseAction.startRealDateTimeStamp",
                        "jobStartDateUtc": "$jobDetails.roleWiseAction.startRealDateUtc",
                        "jobStartDateTimeStampLocal": "$jobDetails.roleWiseAction.startRealDate",
                        "jobEndDateTimeStamp": "$jobDetails.roleWiseAction.endRealDateTimeStamp",
                        "jobEndDateUtc": "$jobDetails.roleWiseAction.endRealDateUtc",
                        "jobEndDateTimeStampLocal": "$jobDetails.roleWiseAction.endRealDate",

                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },


                {
                    "$project":{
                        "jobId":1,
                        "hireList":1,
                        "employerId":1,
                        "createdAt":1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        
                        "appiliedFrom":1,
                        "approvedTo":1,
                        "skillDetails":1,
                        "status":1,
                        "location":1,
                        "locationName":1,
                        "description":1,
                        "setTime":1,
                        "payment":1,
                        "uploadFile":1,
                        "paymentStatus":1,
                        "jobDetails":1,
                        "roleId":1,
                        "roleName":1,
                        "jobType":1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        }
                },

                {
                    "$project":{
                        "jobId":1,
                        "hireList":1,
                        // "hiredLength":{$size: "$hireList"},
                        "employerId":1,
                        "createdAt":1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,

                        "appiliedFrom":1,
                        "approvedTo":1,
                        "skillDetails":1,
                        "status":1,
                        "location":1,
                        "locationName":1,
                        "description":1,
                        "setTime":1,

                        "firstSetTime": { $arrayElemAt: [ "$setTime", 0 ] },
                        "firstDate": { $toDate: "$firstSetTime.startDate" },

                        "secondSetTime": { $arrayElemAt: [ "$setTime", -1 ] },
                        "secondDate": { $toDate: "$secondSetTime.endDate" },

                        "payment":1,
                        "uploadFile":1,
                        "paymentStatus":1,
                        "jobDetails":1,
                        "roleId":1,
                        "roleName":1,
                        "jobType":1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        }
                },

                {
                    "$project":{
                        "jobId":1,
                        "hireList":1,
                        "employerId":1,
                        "createdAt":1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        
                        "appiliedFrom":1,
                        "approvedTo":1,
                        "skillDetails":1,
                        "status":1,
                        "location":1,
                        "locationName":1,
                        "description":1,
                        "setTime":1,

                        "firstSetTime":1,
                        // "startDate": {$toDate: "$firstSetTime.startDate"},
                        "startTime": "$firstSetTime.startTime",
                        // "firstDate": { $toDate: "$firstSetTime.startDate" },
                        // "firstDateTimeStamp":"$firstSetTime.startDate",

                        // "secondSetTime":1,
                        "endTime": "$firstSetTime.endTime",
                        // "secondDate": { $toDate: "$secondSetTime.endDate" },
                        // "SecondDateTimeStamp":"$secondSetTime.endDate",

                        "payment":1,
                        "uploadFile":1,
                        "paymentStatus":1,
                        "jobDetails":1,
                        "roleId":1,
                        "roleName":1,
                        "jobType":1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        }
                },
            
                // {
                //     $match:{
                //         $and: [
                //             { 'firstDateTimeStamp': { $lte: FinalDate} },
                //             { 'SecondDateTimeStamp': { $gt: FinalDate} }
                //         ]
                //     } 
                // },    
                
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },

                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },

                        "jobStartDateTimeStamp": {
                            $first: "$jobStartDateTimeStamp"
                        },

                        "isDeleted": {
                            $first: "$isDeleted"
                        },

                        "jobStartDateUtc": {
                            $first: "$jobStartDateUtc"
                        },
                        "jobStartDateTimeStampLocal": {
                            $first: "$jobStartDateTimeStampLocal"
                        },
                        "jobEndDateTimeStamp": {
                            $first: "$jobEndDateTimeStamp"
                        },
                        "jobEndDateUtc": {
                            $first: "$jobEndDateUtc"
                        },
                        "jobEndDateTimeStampLocal": {
                            $first: "$jobEndDateTimeStampLocal"
                        },
                       
                        "startTime": {
                            $first: "$startTime"
                        },

                        "endTime": {
                            $first: "$endTime"
                        },

                        "setTime": {
                            $first: "$setTime"
                        },

                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        },

                        // "firstDateTimeStamp": {
                        //     $first: "$firstDateTimeStamp"
                        // },

                        // "SecondDateTimeStamp": {
                        //     $first: "$SecondDateTimeStamp"
                        // },
                    }
                },

                
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "hiredLength":{$size: "$hireList"},
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime": 1,
                        "endTime": 1,


                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "startTime": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1,
                        "firstDateTimeStamp": 1,
                        "SecondDateTimeStamp": 1
                    }
                },

                {
                    $project: {
                        "hireList":1,
                        "hiredLength":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime": 1,
                        "endTime": 1,

                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1,
                        "firstDateTimeStamp": 1,
                        "SecondDateTimeStamp": 1
                    }
                },


            
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },

                
                {
                    $project: {
                        "jobType": 1,
                        "hiredLength":1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime": 1,
                        "endTime": 1,

                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$hireList" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]},
                        "firstDateTimeStamp": 1,
                        "SecondDateTimeStamp": 1
                    }
                },
                
                {
                    $project: {
                        "jobType": 1,
                        "hiredLength":1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{$size: "$setTime"},
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":1,
                        "industryDetails":1,
                        "firstDateTimeStamp": 1,
                        "SecondDateTimeStamp": 1,
                        
                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime": 1,
                        "endTime": 1,

                        // "startDate": { 
                        //     $toDate: '$firstDateTimeStamp'
                        // },

                        // "endDate": { 
                        //     $toDate: '$SecondDateTimeStamp'
                        // }
                
                    }
                },

               
                
                {
                    $match:{
                        $and: [
                            { 'isDeleted': { $ne: "YES"} },
                            // { 'jobStartDateTimeStampLocal': { $lte: dateLocal} },
                            // { 'jobEndDateTimeStampLocal': { $gt: dateLocal} },
                            { 'jobStartDateTimeStamp': { $lte: FinalDate} },
                            { 'jobEndDateTimeStamp': { $gt: FinalDate} },
                        ]
                    } 
                },  

                { $sort : { createdAt : -1 } },

                // {
                //     $match:{
                //         'endDate': { $gt: tomorrow} 
                //     } 
                // },  
                
                // {
                //     $match:{
                //         $and: [
                //             { 'firstDateTimeStamp': { $lte: today} },
                //             // { 'SecondDateTimeStamp': { $gte: FinalDate} }
                //         ]
                //     } 
                // },  
                
            
                // { '$match': { 
                //     'hireListCount': {"$eq":false}}
                // },
                // {$match:{$expr:{$ne:["$setTimeCount","$originalsetTimeCount"]}}},
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
                
            
            
            ];

            
            
            models.job.aggregate(aggrQuery).exec((error, data) => {

                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    
                    
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0],
                        FinalDate:FinalDate,
                        today: today,
                        dateLocal:dateLocal,
                        convertedTimeStamp: convertedTimeStamp,
                        diffInTimeStamp: diffInTimeStamp
                        // details: data[0]
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET ALL JOBS
     * empId wise separate jobs and get all jobs of an employer
     */
    getJobs1: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
   
            let presentDate = req.body.today ? req.body.today : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present date"
            });

            let presentTime = req.body.presentTime ? req.body.presentTime : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - present time"
            });

            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - local Time Zone"
            });

            // let dateObj = jobProfileBusiness.getTimeAndDateWithTimeZone(presentDate, presentTime, localTimeZone);
            let dateObj = jobProfileBusiness.getTimeAndDate(presentDate, presentTime);
            
            let FinalDate = dateObj.realDateTimeStamp;
            let today = dateObj;
            let dateUtc = new Date(FinalDate);
            let dateLocal = dateUtc.toLocaleString();
            let realDate = dateObj.realDate;
            let realDateTimeStamp = dateObj.realDateTimeStamp;

            // res.json({
            //     dateObj: dateObj,
            //     dateLocal: dateLocal,
            //     realDate: realDate,
            //     dateUtc: dateUtc
            // });
            // return;

            let diffArr = jobProfileBusiness.getDifferenceBetweenTimeZone({
                "localTimeZone": localTimeZone
            });
            let diffInTimeStamp = diffArr.diffInTimeStamp;

            let convertedTimeStamp = Number(FinalDate) + Number(diffInTimeStamp);
            FinalDate = convertedTimeStamp;
            let convertedDate = new Date(convertedTimeStamp).toLocaleString("en-US", { timeZone: localTimeZone });
            // let convertedDateUtc = dateObj.convertedDateUtc;
             
            
            // res.json({
            //     dateLocal: dateLocal,
            //     dateObj:dateObj,
            //     convertedDate: convertedDate,
            //     diffArr: diffArr,
            //     diffInTimeStamp: diffInTimeStamp,
            //     convertedDate: convertedDate
            // });
            // return;

            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            if (!req.body.employerId ) { return; }
           
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
            
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                    $project: {
                        "jobId": 1,
                        // "jobStartDateTimeStamp": 1,
                        // "jobStartDateUtc": 1,
                        // "jobStartDateTimeStampLocal": 1,
                        // "jobEndDateTimeStamp": 1,
                        // "jobEndDateUtc": 1,
                        // "jobEndDateTimeStampLocal": 1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "hireList":1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },

                {
                    "$project":{
                        "jobId":"$jobId",
                        "isDeleted": "$jobDetails.roleWiseAction.isDeleted",
                        "jobStartDateTimeStamp": "$jobDetails.roleWiseAction.startRealDateTimeStamp",
                        "jobStartDateUtc": "$jobDetails.roleWiseAction.startRealDateUtc",
                        "jobStartDateTimeStampLocal": "$jobDetails.roleWiseAction.startRealDate",
                        "jobEndDateTimeStamp": "$jobDetails.roleWiseAction.endRealDateTimeStamp",
                        "jobEndDateUtc": "$jobDetails.roleWiseAction.endRealDateUtc",
                        "jobEndDateTimeStampLocal": "$jobDetails.roleWiseAction.endRealDate",

                        "hireList":"$hireList",
                        "employerId":"$employerId",
                        "createdAt":"$createdAt",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "skillDetails":"$skillDetails",
                        "status":"$jobDetails.roleWiseAction.status",
                        "location":"$jobDetails.roleWiseAction.location",
                        "locationName":"$jobDetails.roleWiseAction.locationName",
                        "description":"$jobDetails.roleWiseAction.description",
                        "setTime":"$jobDetails.roleWiseAction.setTime",
                        "payment":"$jobDetails.roleWiseAction.payment",
                        "uploadFile":"$jobDetails.roleWiseAction.uploadFile",
                        "paymentStatus":"$jobDetails.roleWiseAction.paymentStatus",
                        "jobDetails":"$jobDetails",
                        "roleId":"$jobDetails.roleWiseAction.roleId",
                        "roleName":"$jobDetails.roleWiseAction.roleName",
                        "jobType": "$jobDetails.roleWiseAction.jobType",
                        "payType":"$jobDetails.roleWiseAction.payType",
                        "pay": "$jobDetails.roleWiseAction.pay",
                        "noOfStuff": "$jobDetails.roleWiseAction.noOfStuff",
                        "distance":"$jobDetails.roleWiseAction.distance",
                        }
                },
                
                {
                    "$project":{
                        "jobId":1,
                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,

                        "hireList":1,
                        "employerId":1,
                        "createdAt":1,
                        "appiliedFrom":1,
                        "approvedTo":1,
                        "skillDetails":1,
                        "status":1,
                        "location":1,
                        "locationName":1,
                        "description":1,
                        "setTime":1,
                        "firstSetTime": { $arrayElemAt: [ "$setTime", 0 ] },
                        "payment":1,
                        "uploadFile":1,
                        "paymentStatus":1,
                        "jobDetails":1,
                        "roleId":1,
                        "roleName":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        }
                },

                
                {
                    "$project":{
                        "jobId":1,
                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime":"$firstSetTime.startTime",
                        "endTime":"$firstSetTime.endTime",

                        "hireList":1,
                        "hireListSize":{ $size: "$hireList" },
                        "employerId":1,
                        "createdAt":1,
                        "appiliedFrom":1,
                        "approvedTo":1,
                        "skillDetails":1,
                        "status":1,
                        "location":1,
                        "locationName":1,
                        "description":1,
                        "setTime":1,
                        "firstSetTime": 1,
                        "firstDateTimeStamp":"$firstSetTime.startDate",
                        "payment":1,
                        "uploadFile":1,
                        "paymentStatus":1,
                        "jobDetails":1,
                        "roleId":1,
                        "roleName":1,
                        "jobType": 1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        }
                },
                
                
                
                // {
                //     $match:{
                //         $and: [
                //             // { 'firstDateTimeStamp': { $lte: FinalDate} },
                //             { 'firstDateTimeStamp': { $gt: FinalDate} }
                //         ]
                //     } 
                // },    
                
                
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },

                        "isDeleted": {
                            $first: "$isDeleted"
                        },

                        "jobStartDateTimeStamp": {
                            $first: "$jobStartDateTimeStamp"
                        },

                        "jobStartDateUtc": {
                            $first: "$jobStartDateUtc"
                        },
                        "jobStartDateTimeStampLocal": {
                            $first: "$jobStartDateTimeStampLocal"
                        },

                        "jobEndDateTimeStamp": {
                            $first: "$jobEndDateTimeStamp"
                        },

                        "jobEndDateUtc": {
                            $first: "$jobEndDateUtc"
                        },

                        "jobEndDateTimeStampLocal": {
                            $first: "$jobEndDateTimeStampLocal"
                        },
                        "startTime":{
                            $first: "$startTime"
                        },

                        "endTime":{
                            $first: "$endTime"
                        },

                        "jobType": {
                            $first: "$jobType"
                        },
                        "payType": {
                            $first: "$payType"
                        },
                        "pay": {
                            $first: "$pay"
                        },
                        "noOfStuff": {
                            $first: "$noOfStuff"
                        },
                        "distance": {
                            $first: "$distance"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },
                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },
                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "hireList": {
                            $first: "$hireList"
                        },

                        "hireListSize": {
                            $first: "$hireListSize"
                        },

                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "firstSetTime": {
                            $first: "$firstSetTime"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "roleId": {
                            $first: "$roleId"
                        },
                        "roleName": {
                            $first: "$roleName"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                
                
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                
                {
                    $project: {
                        "hireList1":"$hireList",
                        "hireList":{ $filter: { input: "$hireList", as: "hireList", cond: { $eq: ["$$hireList.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "hireListSize":1,
                        "jobType": 1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime":1,
                        "endTime":1,

                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "firstSetTime": 1,
                        // "setTime": 1,
                        "originalsetTimeCount":{$size: "$setTime"},
                        "setTime":  {
                            "$map": {
                              "input": {
                                "$filter": {
                                  "input": "$setTime",
                                  "as": "item",
                                  "cond": { $lt: ["$$item.endDate", FinalDate] }
                                }
                              },
                              "as": "finalItem",
                              "in": {
                                "endDate": "$$finalItem.endDate",
                                
                              }
                            }
                          },
                        
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },

                
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },

                
                {
                    $project: {
                        "hireList1":1,
                        "jobType": 1,
                        "hireListSize":1,
                        "payType":1,
                        "pay": 1,
                        "noOfStuff": 1,
                        "distance":1,
                        "roleName":1,
                        "roleId":1,

                        "isDeleted": 1,
                        "jobStartDateTimeStamp": 1,
                        "jobStartDateUtc": 1,
                        "jobStartDateTimeStampLocal": 1,
                        "jobEndDateTimeStamp": 1,
                        "jobEndDateUtc": 1,
                        "jobEndDateTimeStampLocal": 1,
                        "startTime":1,
                        "endTime":1,

                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTimeCount":{$size: "$setTime"},
                        // "firstSetTime": 1,
                        // "startDate": {$toDate: "$firstSetTime.startDate"},
                        // "startTime": "$firstSetTime.startTime",
                        "setTime":1,
                        "originalsetTimeCount":1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "hireListCount":{
                            $cond: {
                                "if": { "$eq": [ {$size: "$hireList" }, 0 ] }, 
                                "then": true,
                                 "else": false
                            }
                        },
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },

                {
                    $match:{
                        $and: [
                            // { 'jobStartDateTimeStampLocal': { $gte: convertedDate} },
                            // { 'jobStartDateTimeStampLocal': { $gte: dateLocal} },
                            { 'isDeleted': { $ne: "YES"} },
                            { 'jobStartDateTimeStamp': { $gte: realDateTimeStamp} },
                        ]
                    } 
                },  
                
                { $sort : { createdAt : -1 } },
                
                // { '$match': { 
                //     'hireListCount': {"$eq":true}}
                // },
                {$match:{$expr:{$ne:["$setTimeCount","$originalsetTimeCount"]}}},
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            
            
            ]
            models.job.aggregate(aggrQuery).exec( async (error, data) => {

                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {

                    if (data.length < 1) {
                        res.json({
                            isError: true,
                            message: "No job found",
                            statuscode: 204,
                            details: null
                        });

                        return;
                    }

                    let hireList = data[0].results[0].hireList1; 

                    var i; 
                    var j;
                    var hireListSize = 0;
                    for (i = 0; i < data[0].results.length; i++) {
                        
                        hireList1 = data[0].results[i].hireList1;
                        hireListSize = 0;
                        for (j = 0; j < hireList1.length; j++) {
                            
                            if (hireList1[j].roleId == data[0].results[i].roleId) {
                                hireListSize++;
                            }
                        } // END for (j = 0; j < hireList.length; j++) {

                        data[0].results[i].hireListSize = hireListSize;    
                    } // END for (i = 0; i < data[0].results.length.length; i++) {


                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data[0],
                        today: today,
                        dateLocal:dateLocal,
                        FinalDate: FinalDate,
                        convertedDate: convertedDate,
                        convertedTimeStamp:convertedTimeStamp
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* get job in old structure*/ 
    getJobs: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId ) { return; }
           
            let aggrQuery = [
                {
                    '$match': { 'employerId': empId }
                },
                { $unwind: "$jobDetails.roleWiseAction" },
                { $unwind: "$jobDetails.roleWiseAction.skills" },
                {
                    $lookup: {
                        from: "skills",
                        localField: "jobDetails.roleWiseAction.skills",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": 1,
                        "approvedTo": 1,
                        "skillDetails": {
                            $arrayElemAt: ["$skillDetails", 0]
                        }
                    }
                },
                {
                    $group: {
                        "_id": {
                            "roleId": "$jobDetails.roleWiseAction.roleId",
                            "jobId": "$jobId"
                        },
                        "jobDetails": {
                            $first: "$jobDetails"
                        },
                        "locationName":{
                            $first: "$locationName"
                        },
                        "location": {
                            $first: "$location"
                        },
                        "employerId": {
                            $first: "$employerId"
                        },
                        "jobId": {
                            $first: "$jobId"
                        },

                        "paymentStatus": {
                            $first: "$paymentStatus"
                        },

                        "appiliedFrom": {
                            $first: "$appiliedFrom"
                        },
                        "approvedTo": {
                            $first: "$approvedTo"
                        },
                        "createdAt": {
                            $first: "$createdAt"
                        },
                        "status": {
                            $first: "$status"
                        },
                        "uploadFile": {
                            $first: "$uploadFile"
                        },
                        "description": {
                            $first: "$description"
                        },
                        "setTime": {
                            $first: "$setTime"
                        },
                        "industry": {
                            $first: "$jobDetails.industry"
                        },
                        "payment": {
                            $first: "$payment"
                        },
                        "skillDetails": {
                            $push: "$skillDetails"
                        }
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "jobDetails": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "status": 1,
                        "createdAt": 1,
                        "appiliedFrom": { $filter: { input: "$appiliedFrom", as: "appiliedFrom", cond: { $eq: ["$$appiliedFrom.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "approvedTo": { $filter: { input: "$approvedTo", as: "approvedTo", cond: { $eq: ["$$approvedTo.roleId", "$jobDetails.roleWiseAction.roleId"] } } },
                        "skillDetails": 1,
                        "skillDetails": 1,
                        "industryDetails": 1
                    }
                },
                {
                    $lookup: {
                        from: "candidates",
                        localField: "appiliedFrom.candidateId",
                        foreignField: "candidateId",
                        as: "appiliedFrom"
                    }
                },
                {
                    $project: {
                        "appiliedFrom.candidateId": 1,
                        "appiliedFrom.fname": 1,
                        "appiliedFrom.mname": 1,
                        "appiliedFrom.lname": 1,
                        "jobDetails": 1,
                        "jobId": 1,
                        "location": 1,
                        "locationName":1,
                        "employerId": 1,
                        "description": 1,
                        "setTime": 1,
                        "payment": 1,
                        "uploadFile": 1,
                        "paymentStatus": 1,
                        "payment": {$arrayElemAt : ["$payment",0]},
                        "status": 1,
                        "createdAt": 1,
                        "skillDetails": 1,
                        "approvedTo": 1,
                        "industryDetails":{$arrayElemAt : ["$industryDetails",0]}
                    }
                },
            ]
            models.job.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    console.log(error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
     /**
     * @abstract 
     * GET  JOB BY ID
     * Fetch all details of a job by filter JobId.
     */
    GetJobById: (req, res, next) => {
        let jobId = req.body.jobId ? req.body.jobId : res.send({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
        });
        if (!req.body.jobId) { return; }
        models.job.find({ 'jobId': jobId }, function (err, item) {
            console.log(item)
            if (item.length == 0) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1021'],
                    statuscode: 1021,
                    details: null
                })
            }
            else {
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: item
                })
            }
        })
    },
     /**
     * @abstract 
     * POST SINGLE JOB
     */
    PostJob: (req, res, next) => {
        try {
            // below code is based on old json structure
           let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let industry = req.body.industry ? req.body.industry : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry"
            });

            let jobType = req.body.jobType ? req.body.jobType : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Type"
            });
            let payType = req.body.payType ? req.body.payType : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Pay Type"
            });
            let pay = req.body.pay ? req.body.pay : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Pay"
            });
            let noOfStuff = req.body.noOfStuff ? req.body.noOfStuff : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - No Of Stuff"
            });
            let setTime = req.body.setTime ? req.body.setTime : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Set Time"
            });
            let uploadFile = req.body.uploadFile ? req.body.uploadFile : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Upload File"
            });
            let payment = req.body.payment ? req.body.payment : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Payment"
            });
            let description = req.body.description ? req.body.description : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Description"
            });
            let roleWiseAction = req.body.roleWiseAction ? req.body.roleWiseAction : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Wise Action"
            });
            let location = req.body.location ? req.body.location : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Location"
            });
            let getRoleWiseAction = roleWiseAction
            let setTimeDetails = setTime
            let paymentDetails = payment
            let locationDetails = location
            if (!req.body.description || !req.body.uploadFile || !req.body.employerId || !req.body.industry || !req.body.jobType || !req.body.payType || !req.body.pay || !req.body.noOfStuff) { return; }
            let type = "JOB";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    let insertquery = {
                        '_id': new mongoose.Types.ObjectId(), "location": locationDetails,
                        "setTime": setTimeDetails, "payment": paymentDetails, "description": description,
                        "jobDetails.roleWiseAction": getRoleWiseAction, 'jobDetails.industry': industry, 'jobDetails.jobType': jobType,
                        'jobDetails.payType': payType, 'jobDetails.pay': pay, 'jobDetails.noOfStuff': noOfStuff, 'jobId': autouserid,
                        'employerId': empId, 'uploadFile': uploadFile, 'paymentStatus': 'unpaid', 'status': 'posted'
                    }
                    models.job.create(insertquery, function (err, data) {
                        if (err) {
                            console.log(err);
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: data
                            })
                        }
                    })
                }
            })
            // below code is based on New json structure .use it if necessary.Structure is given below.
            // let jobdetails = req.body.jobdetails ? req.body.jobdetails : res.json({
            //     message: errorMsgJSON['ResponseMsg']['303'] + " - Job Details",
            //     isError: true
            // });
            // models.job.insertMany(jobdetails, function (err, data) {
            //     if (err) {
            //         console.log(err);
            //         res.json({
            //             isError: true,
            //             message: errorMsgJSON['ResponseMsg']['404'],
            //             statuscode: 404,
            //             details: null
            //         });
            //     }
            //     else {
            //         res.json({
            //             isError: false,
            //             message: errorMsgJSON['ResponseMsg']['200'],
            //             statuscode: 200,
            //             details: data

            //         })
            //     }
            // })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },

    /*****************************************************
    * Post multiple job at one attempt[if needed use this]
    *****************************************************/
    /**
    * @abstract 
    * Add Multiple job
   */
    postMultipleJob1: async (req, res, next) => {
        try {
            let jobdetails = req.body.jobdetails ? req.body.jobdetails : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Details",
                isError: true
            });

            let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
                serverResponse: {
                  isError: true,
                  message: errorMsgJSON[lang]["303"] + " - local Time Zone",
                  statuscode: 303
                }
            });
        
            getDiffFetch = await jobProfileBusiness.getDifferenceBetweenTimeZone({
                localTimeZone: localTimeZone
            });

            let diffInTimeStamp = getDiffFetch.diffInTimeStamp;

            
             
            roleStartTimes = [];
            roleEndTimes = [];
     
            let i,j;
            for (i = 0; i < jobdetails.length; i++) {
                let roles = jobdetails[i].jobDetails.roleWiseAction;
                for (j = 0; j < roles.length; j++) {
                    let setTime = roles[j].setTime;
                   
                    let serTimeObj = jobProfileBusiness.getStartAndEndTimeOfRole(setTime,diffInTimeStamp);

                    roles[j].startRealDate = serTimeObj.startRealDate; 
                    roles[j].startRealDateUtc = serTimeObj.startRealDateUtc; 
                    roles[j].startRealDateTimeStamp = serTimeObj.startRealDateTimeStamp; 
                    roles[j].endRealDate = serTimeObj.endRealDate; 
                    roles[j].endRealDateUtc = serTimeObj.endRealDateUtc; 
                    roles[j].endRealDateTimeStamp = serTimeObj.endRealDateTimeStamp; 
                    roles[j].setTime = serTimeObj.setTime;

                    roleStartTimes.push(roles[j].startRealDateTimeStamp);     
                    roleEndTimes.push(roles[j].endRealDateTimeStamp);                    
                } // ENd for (j = 0; j < jobdetails.length; i++) {

                // reassigning the updated roles in rolewiseaction
                jobdetails[i].jobDetails.roleWiseAction = roles;

                jobStartDateTimeStamp = Math.min(...roleStartTimes); 
                jobStartDateUtc = new Date(jobStartDateTimeStamp);
                jobStartDateTimeStampLocal = jobStartDateUtc.toLocaleString();
                jobdetails[i].jobStartDateTimeStamp = jobStartDateTimeStamp;
                jobdetails[i].jobStartDateUtc = jobStartDateUtc;
                jobdetails[i].jobStartDateTimeStampLocal = jobStartDateTimeStampLocal;
    
                jobEndDateTimeStamp = Math.max(...roleEndTimes); 
                jobEndDateUtc = new Date(jobEndDateTimeStamp);
                jobEndDateTimeStampLocal = jobEndDateUtc.toLocaleString();
                jobdetails[i].jobEndDateTimeStamp = jobEndDateTimeStamp;
                jobdetails[i].jobEndDateUtc = jobEndDateUtc;
                jobdetails[i].jobEndDateTimeStampLocal = jobEndDateTimeStampLocal;
               
            } // ENd  for (i = 0; i < jobdetails.length; i++) {

            let newJobArr = await jobdetails.map(tt => {
                tt['jobId'] = 'JOB' + new Date().getTime()+ parseInt(Math.random()*100)
                return tt;
            })

            // res.json({
            //     newJobArr: newJobArr,
            //     getDiffFetch: getDiffFetch,
            //     localTimeZone: localTimeZone,
            //     diffInTimeStamp:diffInTimeStamp
            // });
            // return;
             
            // res.json({
            //     newJobArr: newJobArr,
            //     dateUtc:dateUtc,
            //     dateLocal: dateLocal
            // });
            // return;
            //===========================================
            let employerId = jobdetails[0].employerId;     
            let aggrQuery = [
              {
                $match: {
                  'employerId': employerId,
                } 
              },
              {
                $project: {
                    "Status": 1                            
                }
              }    
            ];

            models.employer.aggregate(aggrQuery, function (err, response) {
                 
                if (response[0].Status.isApprovedByAot == false) {
                    res.json({
                        isError: true,
                        message: "Employer is not approved by admin yet",
                        statuscode: 204,
                        details: null
                    })  
                    return; 
                } 
                    //====================================
                    models.job.insertMany(newJobArr, function (err, data) {
                        if (err) {
                            console.log(err);
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            res.json({
                                isError: false,
                                message: errorMsgJSON['ResponseMsg']['200'],
                                statuscode: 200,
                                details: data
                            })
                        }
                    }) // END models.job.insertMany(newJobArr, function (err, data) {
                    //===========================================
            
            }) //END models.employer.aggregate(aggrQuery, function (err, response) {      
            //===============================    
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /* post multiple job in old structure */
    postMultipleJob: async (req, res, next) => {
        try {
            let jobdetails = req.body.jobdetails ? req.body.jobdetails : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Details",
                isError: true
            });

            console.log("************************")
            console.log('log1',jobdetails)
            console.log("************************")

            let newJobArr = await jobdetails.map(tt => {
                tt['jobId'] = 'JOB' + new Date().getTime()+ parseInt(Math.random()*100)
                return tt;
            })
            console.log("************************")
            console.log('log2',newJobArr)
            console.log("************************")
            models.job.insertMany(newJobArr, function (err, data) {
                if (err) {
                    console.log(err);
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data

                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    candidateList: async (req, res, next) => {
        try{
            var allcandidates = [];
            // var singlecand = [];
            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            var candidates = await models.job.find({ 'employerId': empId });
            for(candidate of candidates) {
               //if(candidate.hireList.length > 0){
                    //var canddets = {};
                    var singlecand = await models.candidate.find({ 'candidateId': candidate.hireList[0].candidateId });

                    // allcandidates.push(candidate.jobId);
               //}
            }
            // console.log(candidates);
            res.send({
                isError: false,
                statuscode: 200,
                details: candidate.jobId
            });
        } catch {
            res.send({
                isError: true,
                statuscode: 404,
                details: "NOEN"
            });

        }
    },


    deleteTemplate: async (req, res, next) => {
        try{

            let employerId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });

            let templateId = req.body.templateId ? req.body.templateId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - template Id"
            });


            if (!req.body.employerId || !req.body.templateId) { return; }

            
            models.employer.updateOne(
                { "employerId": employerId }, 
                { "$pull": 
                    { "templates": 
                        { "templateId": templateId } 
                    } 
                }, 
                function (updatederror, updatedresponse) {

                    if (updatederror) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        });

                        return;
                    } else {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        });
   
                    }
            });




        } catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }

    },

    
    /**
     * @abstract 
     * GET ALL TEMPLATE
     * fetch all templates of an employer by using employerId
     */
    getAllTemplates: async (req, res, next) => {
        try{

            let empId = req.body.employerId ? req.body.employerId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId) { return; }
           
            let aggrQuery=[
                {
                    '$match': { 'employerId': empId },
                    
                },
                { $project: {
                    templates:1
                 }},
                 { $unwind: "$templates" },
                 { $unwind: "$templates.jobDetails.roleWiseAction" },
                 {
                    $group: {
                        "_id": {
                            "templateId": "$templates.templateId",
                          
                         },
                         "templateId":{
                            $first: "$templates.templateId"
                         },
                         "templateName":{
                            $first: "$templates.templateName"
                         },
                       
                    }
                },
                { $project: {
                    templateId:1,
                    templateName:1,
                    _id:0
                    }
                },
               
                           
            ]
            models.employer.aggregate(aggrQuery).exec((error, data) => {
                if (error) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data
                    })
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },


    // This function is used for testing perpose
    forTesting: async (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        try{
    
          let localTimeZone = req.body.localTimeZone ? req.body.localTimeZone : res.json({
            serverResponse: {
              isError: true,
              message: errorMsgJSON[lang]["303"] + " - local Time Zone",
              statuscode: 303
            }
          });

           let attribute = {
            localTimeZone: localTimeZone
           }
    
          let date = new Date();
          let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    
         getDiffFetch = await jobProfileBusiness.getDifferenceBetweenTimeZone(attribute);
          
          res.json({
            "getDiffFetch": getDiffFetch,
            "timeZone": timeZone
          })
    
        } catch (e) {
          res.json({
            serverResponse: {
              isError: true,
              message: errorMsgJSON[lang]["500"],
              statuscode: 500
            }
          });
        }
      },

    // testsendNotificationEmail: async (req, res, next) => {
        
    //     let emailId = req.body.emailId;
    //     let candidateId = req.body.candidateId;
    //     let jobId = req.body.jobId;
    //     let roleId = req.body.roleId;

    //     // let employerId = "";
    //     // let fetchEmployerStatus;
    //     // let locationName = "";

    //     // let fetchJobStatus = await employerJobBusiness.fetchJob(jobId);

    //     // if (fetchJobStatus.isError == false) {
    //     //     employerId = fetchJobStatus.jobDetails[0].employerId;
    //     //     locationName = fetchJobStatus.jobDetails[0].jobDetails.roleWiseAction[0].locationName;
    //     //     fetchEmployerStatus = await employerJobBusiness.fetchEmployer(employerId);
    //     // }

    //     sendNotificationEmail(emailId,candidateId,jobId,roleId)

       

    //     // sendNotificationEmail(emailId,candidateId,jobId,roleId)
    //     res.json({
    //         requ: req.body
    //         // // fetchJobStatus: fetchJobStatus,
    //         // fetchEmployerStatus: fetchEmployerStatus,
    //         // // employerId: employerId,
    //         // locationName: locationName
    //     });
    // },
}

async function getCalculatedRoutes(JobIds){
   
    return await Promise.all(JobIds.map(item => item))
  
    
     
}

async function sendNotificationEmail(emailId,candidateId,jobId,roleId){

    //======================================================
    let employerId = "";
    let fetchEmployerStatus = "";
    let companyName = "";

    let fetchJobStatus = await employerJobBusiness.fetchJob(jobId);

    if (fetchJobStatus.isError == false) {
        employerId = fetchJobStatus.jobDetails[0].employerId;
        fetchEmployerStatus = await employerJobBusiness.fetchEmployer(employerId);

        if (fetchEmployerStatus.isError == false) {
            companyName = fetchEmployerStatus.response[0].companyName;
        }
    }

    
    //======================================================
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
           
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
//                 globalVarObj.msgBody=`Dear, ${globalVarObj.fname} ${globalVarObj.mname} ${globalVarObj.lname}, your application for position ${globalVarObj.jobRoleName} and Job ID ${jobId}, have been approved.
// Employer will contact with you shortly`;

                globalVarObj.msgBody=`Congratulations, ${globalVarObj.fname} ${globalVarObj.mname} ${globalVarObj.lname}, your application for the ${globalVarObj.jobRoleName} position for ${companyName}  company has been successful. Please refer to the app for any additional information. (Include date, start/end time of the shift as well as location).`;
                
                //==================
                let insertedValue={
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }

                models.candidate.updateOne({ 'candidateId': candidateId },
                    { $push: { "notification":insertedValue } }, 
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) { 
                //============== 

                        simplemailercontroller.viaGmail({ receiver: emailId, subject: '----JOB APPLICATION APPROVED----',msg:globalVarObj.msgBody}, (mailerErr, nodeMailerResponse) => {
                            if(mailerErr){
                                resolve('false');  
                            }
                            else{
                                resolve('true');
                            }
                        })

                }) // models.candidate.updateOne        
            })     
        })
    })
}



function sendNotificationSms(mobileNo,candidateId,jobId,roleId){
    let globalVarObj={};
    return new Promise(resolve => {
        models.candidate.findOne({'candidateId':candidateId},  function (err, item) {
            if(err){
                resolve('false'); 
            }
            globalVarObj.fname=item['fname'];
            globalVarObj.mname=item['mname'];
            globalVarObj.lname=item['lname'];
            models.jobrole.findOne({'jobRoleId':roleId},  function (error, searchRole) {
                if(error){
                    resolve('false'); 
                }
                globalVarObj.jobRoleName=searchRole['jobRoleName'];
                globalVarObj.msgBody=`Dear, ${globalVarObj.fname} ${globalVarObj.mname} ${globalVarObj.lname}, your application for position ${globalVarObj.jobRoleName} and Job ID ${jobId}, have been approved.
Employer will contact with you shortly`;

                //==================
                let insertedValue={
                    "generatedAt": Date.now(),
                    "notificationMsg": globalVarObj.msgBody
                }

                models.candidate.updateOne({ 'candidateId': candidateId },
                    { $push: { "notification":insertedValue } }, 
                    function (appliedJobsUpdatedError, appliedJobsUpdatedresponse) { 
                //============== 
                        sendsmscontroller.Sendsms(mobileNo, globalVarObj.notification,(twilioError, twilioResult) => {
                            console.log(twilioResult)
                            if(twilioError){
                                resolve('false'); 
                            }
                            else{
                                resolve('true');
                            }
                        }) 
                })  // models.candidate.updateOne          
            })
        })
    })
}

//=================== on 27-12-19



//===================================

function inserSearchTextForHistory(empId, serachText) {
    return new Promise(resolve => {
        models.employer.find({ 'employerId': empId, "resentSearchDetails.name": { "$in": serachText } },
            function (err, item) {
                if (item.length == 0) {
                    let updateWith = {
                        "name": serachText,
                    }
                    models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, async function (updatederror, updatedresponse) {
                        if (updatederror) {
                            resolve(false);
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                resolve(true);
                                
                            }
                            else {
                                resolve(false);
                            }
                        }
                    })
                }
                else {
                    var searchValue = serachText.toLowerCase();
                    models.employer.updateOne({ 'employerId': empId }, { "$pull": { "resentSearchDetails": { "name": searchValue } } }, { safe: true }, function (updatederror, updatedresponse) {
                        if (updatederror) {
                            resolve(false);
                        }
                        else {
                            if (updatedresponse.nModified == 1) {
                                let updateWith = {
                                    "name": serachText,
                                }
                                models.employer.update({ 'employerId': empId }, { $push: { "resentSearchDetails": updateWith } }, function (updatederror, updatedresponse) {
                                    if (updatederror) {
                                        resolve(false);
                                    }
                                    else {
                                        if (updatedresponse.nModified == 1) {
                                            resolve(true);
                                           
                                        }
                                        else {
                                            resolve(false);
                                        }
                                    }
                                })
                            }
                            else {
                                resolve(false);
                            }
                        }
                    })
                }
            })
    })
}



/******************************************************
 * Below is the new json structure for sending job post
        {
            "jobdetails": [
                {
                    "employerId": "EMP1560341566376",
                    "paymentStatus": "unpaid",
                    "status": "posted",
                    "setTime": [
                        {
                            "shift": "Morning",
                            "startDate": 1558607550915,
                            "endDate": 1558607550916,
                            "startTime": "10.00am",
                            "endTime": "6.00pm"
                        },
                        {
                            "shift": "Evening",
                            "startDate": 1558607550915,
                            "endDate": 1558607550915,
                            "startTime": "10.00am",
                            "endTime": "6.00pm"
                        }
                    ],
                    "uploadFile": "www.google.com",
                    "payment": [
                        {
                            "ratePerHour": 100,
                            "noOfStuff": 15,
                            "totalHour": 155,
                            "vat": 33,
                            "ni": 55,
                            "fee": 22,
                            "totalPayment": 666
                        }
                    ],
                    "jobDetails": {
                        "industry": "IND1558529767065",
                        "jobType": "fulltime",
                        "payType": "hour",
                        "pay": 5,
                        "noOfStuff": 5,
                        "roleWiseAction": [
                            {
                                "roleId": "ROLE1559285782576",
                                "roleName": "socialcare-Role4",
                                "skills": [
                                    "SKILL1559305685937",
                                    "SKILL1559299106852"
                                ]
                            },
                            {
                                "roleId": "ROLE1559285975687",
                                "roleName": "socialcare-Role5",
                                "skills": [
                                    "SKILL1559305685937",
                                    "SKILL1559626620098"
                                ]
                            }
                        ]
                    },
                    "description": "description",
                    "distance": 4,
                    "location": {
                        "type": "Point",
                        "coordinates": [
                            -110.8571443,
                            32.4586858
                        ]
                    }
                }
                
            ]
        }

 **********************************************************************/

/***********************************************************************
 * 
 *  "appiliedFrom": { "$setUnion": [ "$appiliedFrom", "$appiliedFrom" ] }
 ***********************************************************************/













  