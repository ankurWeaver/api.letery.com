const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const utilitycontroller = require('../service/utility');
const sendsmscontroller = require('../service/TwilioSms');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
var Request = require("request");
const https = require('https');
const config =  require('../config/env');
module.exports = {
     /**
     * @abstract 
     * Verify Otp
     * First checkif ay data exists or not by checking mobileno ,and otp.
     * if exists  check isUsed true or false. if true otp is already used
     * If false its a fresh otp and verify otp is successful.
     * After that update isUsed=true so that same otp can not be used second time.
     * And Update 'Status.isVerified': true in employer collection so that user can login
    */
    verifyOtp: (req, res, next) => {
        let mobileNo = req.body.mobileNo ? req.body.mobileNo : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Mobile No "
        });
        let otp = req.body.otp ? req.body.otp : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Otp"
        });
        if (!req.body.mobileNo || !req.body.otp) { return; }
        try {
            let aggrQuery = [
                {
                    $match: {
                        'mobileNo': mobileNo,
                        'otp': otp
                    }
                },
                {
                    $project: {
                        '_id': 0,
                    }
                },
                {
                    $group: {
                        _id: null,
                        isUsed: {
                            $first: "$isUsed"
                        },
                        results: {
                            $push: '$$ROOT'
                        }
                    }
                },

                {
                    $project: {
                        userDetails: {
                            $cond: {
                                if: { $eq: ['$isUsed', false] },
                                then: "$results",
                                else: []
                            }
                        }
                    }
                }
            ];
            models.timeTOLive.aggregate(aggrQuery).exec((err, item1) => {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    })
                }
                else {
                    if (item1.length == 0) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1009'],
                            statuscode: 1009,
                            details: null
                        })
                    } else {
                        if (!item1[0].userDetails.length) {
                            res.json({
                                isError: true,
                                message: 'Otp is not valid.',
                                statuscode: 200,
                                details: null
                            })
                        } else {
                            models.timeTOLive.updateOne({ 'mobileNo': mobileNo }, { 'isUsed': true }, function (updatederror, updatedItem) {
                                if (updatederror) {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    });
                                }
                                else {
                                    if (updatedItem.nModified == 1) {
                                        models.employer.updateOne({ 'mobileNo': mobileNo }, { 'Status.isVerified': true }, function (employererror, employerItem) {
                                            if (employererror) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                });
                                            }
                                            else {
                                                if (employerItem.nModified == 1) {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: item1
                                                    })
                                                }
                                                else {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                                        statuscode: 1004,
                                                        details: null
                                                    })
                                                }
                                            }
                                        })
                                    }
                                    else {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['1004'],
                                            statuscode: 1004,
                                            details: null
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
      * @abstract 
      * Signup Employer
      * while adding Employer check if Employer already exists or not in authenticate table
      * if exists add Employer and save some details of Employer table in authenticate table for future checking
     */
    signup: (req, res, next) => {
        let type = "EMP";
        let searchType;
        let autoempid;
        AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
            autoempid = data;
        })

        let email = req.body.emailOrPhone ? req.body.emailOrPhone : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Email Or Phone "
        });
        let password = req.body.password ? req.body.password : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Password "
        });
        let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Country Code"
        });
        let empType = req.body.empType ? req.body.empType : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Type"
        });
        let signedUpVia = req.body.signedUpVia ? req.body.signedUpVia : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Signed up via"
        });
        let allLowerCase=email.toLowerCase();

        // console.log(req.body);
        // return;
        
        if (!req.body.emailOrPhone || !req.body.password || !req.body.countryCode || !req.body.empType || !req.body.signedUpVia) 
            { return; }
        utilitycontroller.checkEmailOrPhone(email, (err, data) => {
            if (err) {
                res.json({
                    isError: true,
                    message: 'Invalid Format of Mobile/Email Id'
                })
            }

            switch (data) {
                case '1': searchType = 'EmailId'

                    break;
                case '2':
                    searchType = 'mobileNo'
                    break;
                default: break;
            }

        })




        try {
            let d = new Date();
            let timeAnddate = d.toISOString();

            let insertquery = {
                '_id': new mongoose.Types.ObjectId(),
                'employerId': autoempid,
                [searchType]: allLowerCase,
                'password': password, 'countryCode': countryCode,
                'employerType': empType, 'userType': 'EMPLOYER',
                'memberSince': timeAnddate,
                'Status.isVerified': false,
                'Status.isActive': true,
                'Status.signedUpVia': signedUpVia,
                'Status.isApprovedByAot': false,
                
            };
            console.log(insertquery)





            models.authenticate.findOne({ $or: [{ 'EmailId': allLowerCase },{ 'mobileNo': email }] }).exec(function (err, item) {
                // res.json({
                //     request: req.body,
                //     item: item,
                //     searchType: searchType
                // });
                // return;
                   
                if (item == null) {
                    models.employer.create(insertquery, 
                        function (error, data) {

                        if (error) {
                            console.log(1)
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            })
                        }
                        else {
                            console.log(2)
                            let employeeId = data['employerId'];
                            let insertparams = {
                                '_id': new mongoose.Types.ObjectId(),
                                'userId': employeeId,
                                'EmailId': (searchType == 'EmailId' ? allLowerCase : ''),
                                'mobileNo': (searchType == 'mobileNo' ? email : ''),
                                'type': 'EMPLOYER'
                            }

                            models.authenticate.create(insertparams, function (error, item) {
                                if (error) {
                                    console.log(3)
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['404'],
                                        statuscode: 404,
                                        details: null
                                    })
                                }
                                else {
                                    if (searchType == 'EmailId') {
                                        Request.post({
                                            "headers": { "content-type": "application/json" },
                                            // "url": `${config.app.FORMATTED_URL}/api/employer/send-link`,
                                            // "url": `http://localhost:3132/api/employer/send-link`,
                                            // "url": `https://api.letery.com/api/employer/send-link`,
                                            "url": `${config.app.FORMATTED_URL_NEW}/api/employer/send-link`,
                                            "body": JSON.stringify({
                                                "email": allLowerCase,

                                            })
                                        }, (error, response, body) => {
                                            if (error) {
                                                console.log(4)
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                })
                                            }
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['1014'],
                                                statuscode: 1014,
                                                details: data
                                            })
                                        });
                                    }
                                    else {
                                        var code = Math.floor((Math.random() * 999999) + 111111);
                                        var finalcode = code.toString().substring(0, 6)

                                        sendsmscontroller.Sendsms(email, finalcode, (err, callResponse) => {
                                            if (err) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['1016'],
                                                    statuscode: 1016,
                                                    details: null
                                                })
                                            }
                                            if (callResponse) {
                                                models.timeTOLive.create({ '_id': new mongoose.Types.ObjectId(), 'email': '', 'mobileNo': email, 'isUsed': false, 'otp': finalcode, 'type': 'EMPLOYER', 'tocken': '', 'isclicked': false }, function (error, inserteditem) {

                                                    if (error) {
                                                        res.json({
                                                            isError: true,
                                                            message: errorMsgJSON['ResponseMsg']['404'],
                                                            statuscode: 404,
                                                            details: error
                                                        });
                                                    }
                                                    else {

                                                        res.json({
                                                            isError: false,
                                                            message: errorMsgJSON['ResponseMsg']['1015'],
                                                            statuscode: 1015,
                                                            details: data
                                                        })

                                                    }
                                                })
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    })
                }
                else {
                    console.log(6)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1010'],
                        statuscode: 1010,
                        details: null
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Change Password
     * First check employer exists or not by email id
     * If exists update old password with new one
     * */
    changePassword: (req, res, next) => {
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        let password = req.body.password ? req.body.password : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Password " });
        if (!req.body.password || !req.body.email) { return; }
        try {
            models.employer.findOne({ 'EmailId': email }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else {
                    models.employer.updateOne({ 'EmailId': email }, { 'password': password }, function (error, response) {
                        if (error) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (response.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
    * @abstract 
    * Send verification Link via mail for Change Password
    * First check if employer exists or not by checking email in employer Document
    * if exists create a link append with random generated tocken and employerId and send it with mail attachment
    * if mail send is successful store tocken in employer document.when link is clicked in mail '/api/employer-profile/verify-emp' is called in employer-profile.js and check if tocken is same or not. if same redirect to reset password page. 
   */
    sendConfirmationLink: (req, res, next) => {
        let email = req.body.email ? req.body.email : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Email " });
        if (!req.body.email) { return; }
        try {
            models.employer.findOne({ 'EmailId': email }, function (err, item) {
                if (err) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    });
                }
                else {
                    if (item == null) {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1008'],
                            statuscode: 1003,
                            details: null
                        })
                    }
                    else {
                        const tokgen = new TokenGenerator(512, TokenGenerator.BASE62);
                        let rand = tokgen.generate()
                        console.log(rand);
                        mailercontroller.viaGmail({ receiver: email, tocken: rand ,key:'sign', path: '/employer-profile/verify-emp?id=' }, async (err, data) => {
                            if (err) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                })
                            }
                            else {

                               await models.employer.updateOne({ 'EmailId': email }, { 'Status.confirmationTocken': rand }, function (error, response) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        });
                                    }
                                    else {
                                        if (response.nModified == 1) {
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: null
                                            })
                                        }
                                        else {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['1004'],
                                                statuscode: 1004,
                                                details: null
                                            })
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    /**
     * @abstract 
     * Signup Via Social
     * collect social object and fetch necessary items from that
     * insert employer table
     * after that insert some data in authenticate table for future checking
    */
    signupViaSocial: (req, res, next) => {
        let type = "EMP";
        let gloabalvarobj = {};
        let autouserid;
        AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
            autouserid = data;
        })
        let d = new Date();
        let timeAnddate = d.toISOString();
        let signedUpVia = req.body.signedUpVia ? req.body.signedUpVia : res.json({ isError: true, message: errorMsgJSON['ResponseMsg']['303'] + " - Sign up via " });
        let signedUpObj = req.body.signedUpObj ? req.body.signedUpObj : '';
        let code = req.body.code ? req.body.code : '';
        let NewToken = token_gen({
            'employerId':autouserid,
            'userType': 'EMPLOYER'
        });
       // if (!req.body.signedUpVia || !req.body.signedUpObj) { return; }
        try {
            switch (signedUpVia) {
                case 'facebook': if (signedUpObj.photoUrl) {
                    gloabalvarobj.picFromSocial = signedUpObj.photoUrl;
                } else {
                    gloabalvarobj.picFromSocial = "";
                }
                    gloabalvarobj.firstnameFromSocial = signedUpObj && signedUpObj.firstName ? signedUpObj.firstName : '';
                    gloabalvarobj.lastnameFromSocial = signedUpObj && signedUpObj.lastName ? signedUpObj.lastName : '';
                    gloabalvarobj.emailFromSocial = signedUpObj && signedUpObj.email ? signedUpObj.email : '';
                    gloabalvarobj.faceBookId = signedUpObj && signedUpObj.id ? signedUpObj.id : '';
                    gloabalvarobj.linkedinId = '';
                    let insertquery = {
                        '_id': new mongoose.Types.ObjectId(), 'employerId': autouserid, 'EmailId': gloabalvarobj.emailFromSocial, 'userType': 'EMPLOYER',
                        'memberSince': timeAnddate, 'Status.isVerified': true, 'Status.isActive': true, 'Status.signedUpVia': signedUpVia,
                        'contactName': gloabalvarobj.firstnameFromSocial + ' ' + gloabalvarobj.lastnameFromSocial, 'profilePic': gloabalvarobj.picFromSocial,
                        'socialLoginDetails.facebookId': gloabalvarobj.faceBookId, 'socialLoginDetails.linedinId': gloabalvarobj.linkedinId, 'Status.isApprovedByAot': false,
                        'geolocation':{"type": "Point","coordinates": [ 0, 0]}
                    };
                    models.authenticate.findOne({ 'EmailId': gloabalvarobj.emailFromSocial }, function (err, item) {
                        if (err) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            });
                        }
                        else {
                            if (item == null) {
                                models.employer.create(insertquery, function (error, data) {
                                    if (error) {
                                        res.json({
                                            isError: true,
                                            message: errorMsgJSON['ResponseMsg']['404'],
                                            statuscode: 404,
                                            details: null
                                        })
                                    }
                                    else {
                                        let insertparams = { '_id': new mongoose.Types.ObjectId(), 'userId': data['employerId'], 'EmailId': gloabalvarobj.emailFromSocial, 'type': 'EMPLOYER' }
                                        models.authenticate.create(insertparams, function (error, item) {
                                            if (error) {
                                                res.json({
                                                    isError: true,
                                                    message: errorMsgJSON['ResponseMsg']['404'],
                                                    statuscode: 404,
                                                    details: null
                                                })
                                            }
                                            else {
                                                res.json({
                                                    isError: false,
                                                    message: errorMsgJSON['ResponseMsg']['204'],
                                                    statuscode: 204,
                                                    details: data,
                                                    token: token_gen({'employerId':data['employerId'],
                                                        'userType': 'EMPLOYER'})   
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                            else {
                               
                                models.employer.findOne({'EmailId':gloabalvarobj.emailFromSocial}, function (err, searcheditem) {
                                    
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['1010'],
                                        statuscode: 1010,
                                        details:searcheditem,
                                        // token:NewToken 
                                        token: token_gen({'employerId':searcheditem.employerId,
                                                        'userType': 'EMPLOYER'})   
                                    }) 

                                })
                            }
        
                        }
        
                    })
                    break;
                case 'linkedin':  
                    var tokenPromise = new Promise((resolve, reject) => {
                        Request.post({
                            "headers": {
                                "content-type": "application/x-www-form-urlencoded",
                                "Accept": "application/json"
                            },
                            "url": `https://www.linkedin.com/oauth/v2/accessToken?client_id=86q6nae7g7x7i6&client_secret=4jg49HENZk1IuJaG&grant_type=authorization_code&redirect_uri=${config.app.frontendUri}/redirect&code=${code}`

                        }, (error, response, body) => {
                            if (error) {
                                reject(1)
                            }
                            let finalResult = JSON.parse(body)
                            if (finalResult.error) {
                                reject(1)
                            }
                            else {
                                gloabalvarobj.access_token = finalResult.access_token;
                                resolve(gloabalvarobj.access_token)
                            }
                        })
                    });
                    tokenPromise.then(async resolvedData => {
                        if (resolvedData == 1) {
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['404'],
                                statuscode: 404,
                                details: null
                            })
                        }
                        var resultWithName = await getNameFromLinkedin(resolvedData)
                        gloabalvarobj.firstnameFromSocial = resultWithName.firstnameFromSocial,
                        gloabalvarobj.lastnameFromSocial = resultWithName.lastnameFromSocial,
                        console.log('hochhe', gloabalvarobj.firstnameFromSocial)
                        console.log('hochhe', gloabalvarobj.lastnameFromSocial)
                        var resultWithEmail = await getEmailFromLinkedin(resolvedData)
                        gloabalvarobj.emailFromSocial = resultWithEmail.emailFromSocial,
                        console.log('hochhe', gloabalvarobj.emailFromSocial)
                        gloabalvarobj.picFromSocial = "";
                        gloabalvarobj.faceBookId = "",
                        gloabalvarobj.linkedinId = '';
                        let insertquery = {
                            '_id': new mongoose.Types.ObjectId(), 'employerId': autouserid, 'EmailId': gloabalvarobj.emailFromSocial, 'userType': 'EMPLOYER',
                            'memberSince': timeAnddate, 'Status.isVerified': true, 'Status.isActive': true, 'Status.signedUpVia': signedUpVia,
                            'contactName': gloabalvarobj.firstnameFromSocial + ' ' + gloabalvarobj.lastnameFromSocial, 'profilePic': gloabalvarobj.picFromSocial,
                            'socialLoginDetails.facebookId': gloabalvarobj.faceBookId, 'socialLoginDetails.linedinId': gloabalvarobj.linkedinId, 'Status.isApprovedByAot': false,
                            'geolocation':{"type": "Point","coordinates": [ 0, 0]}
                        };
                        models.authenticate.findOne({ 'EmailId': gloabalvarobj.emailFromSocial }, function (err, item) {
                            if (err) {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                });
                            }
                            else {
                                if (item == null) {
                                    models.employer.create(insertquery, function (error, data) {
                                        if (error) {
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            })
                                        }
                                        else {
                                            let insertparams = { '_id': new mongoose.Types.ObjectId(), 'userId': data['employerId'], 'EmailId': gloabalvarobj.emailFromSocial, 'type': 'EMPLOYER' }
                                            models.authenticate.create(insertparams, function (error, item) {
                                                if (error) {
                                                    res.json({
                                                        isError: true,
                                                        message: errorMsgJSON['ResponseMsg']['404'],
                                                        statuscode: 404,
                                                        details: null
                                                    })
                                                }
                                                else {
                                                    res.json({
                                                        isError: false,
                                                        message: errorMsgJSON['ResponseMsg']['200'],
                                                        statuscode: 200,
                                                        details: data,
                                                        token: token_gen({'employerId':data['employerId'],
                                                        'userType': 'EMPLOYER'})
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                                else {
                                    models.employer.findOne({'EmailId':gloabalvarobj.emailFromSocial}, function (err, searcheditem) {
                                        res.json({
                                            isError: false,
                                            message:  errorMsgJSON['ResponseMsg']['200'],
                                            statuscode: 200,
                                            details: searcheditem,
                                            token:NewToken,
                                        }) 
    
                                    })
                                }
            
                            }
            
                        })
                       
                    }).catch(err => {
                        res.send(err)
                    })
                    break;
                default: break;
            }
          
           
        }
        catch (error) {
            console.log(error)
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    }
}


function getNameFromLinkedin(resolvedData){
    let globalVarObj={};
    return new Promise(resolve => {
        const accessToken = resolvedData;
        const options = {
        host: 'api.linkedin.com',
        path: '/v2/me',
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${accessToken}`,
            'cache-control': 'no-cache',
            'X-Restli-Protocol-Version': '2.0.0'
        }
        };
        const profileRequest = https.request(options, function(res) {
        let data = '';
        res.on('data', (chunk) => {
            data += chunk;
        });

        res.on('end', () => {
            const profileData = JSON.parse(data);
            const userData=JSON.stringify(profileData, 0, 2);
            const profileDataDb= JSON.parse(userData);
            globalVarObj.firstnameFromSocial=profileDataDb.localizedFirstName;
            globalVarObj.lastnameFromSocial=profileDataDb.localizedLastName;
            resolve(globalVarObj);
        });
        });
        profileRequest.end();
        
    })
}

function getEmailFromLinkedin(resolvedData){
    let globalVarObj={};
    return new Promise(resolve => {
            const accessToken = resolvedData;
                    const options = {
                    host: 'api.linkedin.com',
                    path:'/v2/emailAddress?q=members&projection=(elements*(handle~))',
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${accessToken}`,
                        'cache-control': 'no-cache',
                        'X-Restli-Protocol-Version': '2.0.0'
                    }
                    };
                    const profileRequest = https.request(options, function(res) {
                    let data = '';
                    res.on('data', (chunk) => {
                        data += chunk;
                    });
    
                    res.on('end', () => {
                        const profileData = JSON.parse(data);
                        const userData=JSON.stringify(profileData, 0, 2);
                        const profileDataDb= JSON.parse(userData);
                        globalVarObj.emailFromSocial=profileDataDb.elements[0]['handle~'].emailAddress
                        resolve(globalVarObj);
                       
                    });
                    });
                    profileRequest.end();
        
    })
}










