const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');
const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const Env = require("../env");
const multer = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
aws.config.update({
    secretAccessKey: Env.secretAccessKey,
    accessKeyId: Env.accessKeyId
});
const s3 = new aws.S3();

const employerJobBusiness = require("../businessLogic/employer-job-business");

module.exports = {


    uploadImage: (req, res, next) => {
        let upload = multer({
            storage: multerS3({
                s3: s3,
                bucket: Env.bucket,
                key: function (req, file, cb) {
                    let extension = file.mimetype.split('/');       // split file to extract file extension
                    cb(null, (Date.now() + '.' + extension[1]).toString());
                }
            })
        }).array('file', 10);    // upload maximum 10 file
        upload(req, res, function (err) {
            if (!req.files) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: "Please send a file for upload.",
                        statusCode: 404
                    }
                });
                return true;
            }
            if (err) {
                res.json({
                    serverResponse: {
                        isError: true,
                        message: "Error: " + err,
                        statusCode: 500
                    },
                });
            } else {
                if (req.files) {
                    let imageArray = [];
                    let allImage = req.files;
                    allImage.forEach((image) => {
                        let imageObj = { sourceUrl: image.location, sourceType: image.mimetype }
                        imageArray.push(imageObj);
                    });
                    //    Image.insertMany(imageArray,function(err, result){
                    res.json({
                        serverResponse: {
                            isError: false,
                            message: "File uploaded successfully.",
                            statusCode: 200
                        },
                        result: {
                            url: imageArray
                        }
                    });
                    //     });

                } else {
                    res.json({
                        serverResponse: {
                            isError: true,
                            message: "File not found",
                            statusCode: 404
                        }
                    });
                }
            }

        });

    },

    /*
        This function is used to delete an industry
    */
    deleteIndustry: (req, res, next)=>{
        try{
            let industryId = req.body.industryId ? req.body.industryId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
            });

            if (!req.body.industryId) { return; }

            
            models.industry.deleteOne({ industryId: industryId  }, function (err) {  
           
                if (err) {
                    res.json({
                      isError: true,
                      message: errorMsgJSON['ResponseMsg']['1004'],
                      statuscode: 1004,
                      details: null
                    })
                } else {
                    res.json({
                      isError: false,
                      message: errorMsgJSON['ResponseMsg']['200'],
                      statuscode: 200,
                      details: null
                    })
                }
            }); 

        }  catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }  

    },

    //====================================================

    fetchRolewiseSkill:  async (req, res, next)=>{
        try{
            
            let jobRole = req.body.jobRole ? req.body.jobRole : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Role"
            });

            let candidateId = req.body.candidateId ? req.body.candidateId : "";

            let payloadSkill = [];
            let selectedSkillDetails = [];
            let payloadSkillList = [];
            var i;
            var j;

            if (candidateId != "") {
                // this function is used to fetch the skills selected by candidate
                fetchSelectedSkillOfCandidateStatus = await employerJobBusiness.fetchSelectedSkillOfCandidate(candidateId);

                if (fetchSelectedSkillOfCandidateStatus.isError == true) {
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }

                payloadSkill = fetchSelectedSkillOfCandidateStatus.candidateDetails[0].payloadSkill; 

                for (i = 0; i < payloadSkill.length; i++) {
                
                    payloadSkillList = []; 
                    for (j = 0; j < payloadSkill[i].skillDetails.length; j++) {
                        payloadSkillList.push(payloadSkill[i].skillDetails[j].skillId);
                    }

                    selectedSkillDetails.push({
                        "jobRoleId": payloadSkill[i].jobRoleId,
                        "payloadSkillList": payloadSkillList
                    });
                   
                } // END for (i = 0; i < payloadSkill.length; i++) {

            } // if (candidateId != "") {
          
            //let totaljobRole=JSON.parse(jobRole)
            let totaljobRole=jobRole;
            let aggrQuery = [
                { '$match': 
                    { 'jobRoleId': { '$in': totaljobRole } },
                },
               
                
                {
                  $project: {
                      "_id":0,
                      "jobRoleId": 1,
                      "skillIds": 1,
                      "jobRoleName": 1,
                      "industryId": 1

                  }   
                },

                // {
                //     $unwind : "$skillIds" 
                // },

                {
                    $lookup: {
                        from: "skills",
                        localField: "skillIds",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },

                {
                  $project: {
                      "skillDetails._id":0,
                      "skillDetails.__v":0,
                      "skillDetails.status":0,
                      "skillDetails.createdAt":0,
                      "skillDetails.updatedAt":0,
                      "skillIds": 0,
                  }   
                },

                {
                    $lookup: {
                        from: "industries",
                        localField: "industryId",
                        foreignField: "industryId",
                        as: "industryDetail"
                    }
                },

                {
                    $project: {
                        "jobRoleName": 1,
                        "industryId": 1,
                        "jobRoleId": 1,
                        "skillDetails": 1,
                        "industryDet": { $arrayElemAt: [ "$industryDetail", 0 ] },
                    }   
                },

                {
                    $project: {
                        "jobRoleName": 1,
                        "industryId": 1,
                        "jobRoleId": 1,
                        "skillDetails": 1,
                        "industryName": "$industryDet.industryName"
                    }   
                },
                
            ]

            

            models.jobrole.aggregate(aggrQuery).exec((error, item1) => {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: error
                    }) 
                }
                else{
                    //console.log(item1);

                    let i;
                    let j;
                    let k;
                    let onlySelectedSkillList = [];

                    for (i = 0; i < item1.length; i++) {
                        onlySelectedSkillList = [];
                        if (candidateId != "") {
                            for (k = 0; k < selectedSkillDetails.length; k++) {
                                if (item1[i].jobRoleId == selectedSkillDetails[k].jobRoleId) {       
                                    onlySelectedSkillList =  selectedSkillDetails[k].payloadSkillList;
                                } 
                            }
                        }    
                        
                        for (j = 0; j < item1[i].skillDetails.length; j++) {
                            
                                if (candidateId != "") {
                                    if (onlySelectedSkillList.length != 0) {
                                        item1[i].skillDetails[j].isSelected = onlySelectedSkillList.includes(item1[i].skillDetails[j].skillId);
                                    } else {
                                        item1[i].skillDetails[j].isSelected = false;
                                    }   
                                } else {
                                    item1[i].skillDetails[j].isSelected = false;
                                }       
                        } 
                        
                    } // END for (i = 0; i < item1.length; i++) {
                    
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item1
                    })
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }  
    },

    //====================================================
    getUnverifiedEmployer: (req, res, next) => {
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        console.log('........')
        let aggrQuery = [
            {
                $match:{
                    'Status.isVerified' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        try{
            models.employer.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }  
    },
    getUnverifiedCandidate: (req, res, next) => {
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQuery = [
            {
                $match:{
                    'Status.isVerified' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        try{
            models.candidate.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //===============================================================================

    getUnverifiedCandidateNew: (req, res, next) => {
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQueryWithPagination = [
            {
                $match:{
                    'Status.isVerified' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },

            { $sort : { createdAt : -1} },

            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ];

        let aggrQueryWithOutPagination = [
            {
                $match:{
                    'Status.isVerified' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            { $sort : { createdAt : -1} },
        ];

        let aggrQueryFull = [
            {
                $facet: {
                    "dataWithPagination": aggrQueryWithPagination,
                    "dataWithOutPagination": aggrQueryWithOutPagination,
                }    
            }
        ];

        try{
            models.candidate.aggregate(aggrQueryFull).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //===============================================================================
    getCommission: (req, res, next) => {
        let empId = req.body.employerId ? req.body.employerId : res.json({
            message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
        });
        if (!req.body.employerId) { return; }
        models.employer.findOne({ 'employerId': empId }, function (err, item) {
            if (item == null) {
                res.json({
                    isError: true,
                    message: errorMsgJSON['ResponseMsg']['1008'],
                    statuscode: 1008,
                    details: null
                })
            }
            else {
                res.json({
                    isError: false,
                    message: errorMsgJSON['ResponseMsg']['200'],
                    statuscode: 200,
                    details: {
                        employerId:item.employerId,
                        adminProvidedJobCommission:item.adminProvidedJobCommission
                    }
                })
            }
        })
    },
    setCommision: (req, res, next) => {
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let commissionAmount = req.body.commissionAmount ? req.body.commissionAmount : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " -Commission Amount"
            });
            if (!req.body.employerId || !req.body.commissionAmount ) { return; }
            let updateWhere={
                "employerId":empId
            }
            let updateWith={
                "adminProvidedJobCommission":commissionAmount
            }
            models.employer.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                if(updatederror){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedresponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['211'],
                            statuscode: 211,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    getEmployerDetails: (req, res, next) => {
        try {
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            if (!req.body.employerId) { return; }
            models.employer.findOne({ 'employerId': empId }, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['404'],
                statuscode: 404,
                details: null
            });
        }
    },
    givePayment:(req, res, next)=>{
        try{
            let roleId = req.body.roleId ? req.body.roleId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobId = req.body.jobId ? req.body.jobId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Id"
            });
          
            if (!req.body.roleId || !req.body.jobId) { return; }
            let updateWith={
                "jobDetails.roleWiseAction.$.paymentStatus":"PAID",
            }
            let updateWhere={
                "jobId":jobId,
                "jobDetails.roleWiseAction.roleId":roleId,
            }
            models.job.findOne(updateWhere,{ 'jobDetails.roleWiseAction.$': 1 }, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if(item.jobDetails.roleWiseAction[0].paymentStatus=='PAID'){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['206'],
                            statuscode: 206,
                            details: null
                        })
                    }
                    else{
                        models.job.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
                            if(updatederror){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                            else{
                                if (updatedresponse.nModified == 1) {
                                    res.json({
                                        isError: false,
                                        message: errorMsgJSON['ResponseMsg']['205'],
                                        statuscode: 205,
                                        details: null
                                    })
                                }
                                else {
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1004'],
                                        statuscode: 1004,
                                        details: null
                                    })
                                }
                            }
                        })
                    }
                }
            })
           
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },
    getJobs:(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            let paymentStatus = req.body.paymentStatus ? req.body.paymentStatus : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Payment Status"
            });
            let paymentStatusDb=['PAID','UNPAID']
            let aggerQuery = [
                {
                    $match:{}
                },
                { $unwind : "$jobDetails.roleWiseAction" },
                {
                    $project : {
                    '_id' : 0,
                    'employerId':1,
                    'jobDetails':1,
                    'jobId': 1,
                    'appiliedFrom':1,
                    'approvedTo':1,
                    
                    }
                },
                {
                    $project: {
                       
                        "industry": "$jobDetails.industry",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                    }
                },
                {
                    $lookup: {
                        from: "industries",
                        localField: "industry",
                        foreignField: "industryId",
                        as: "industryDetails"
                    }
                },
                {
                    $project: {
                       
                        "industry": "$jobDetails.industry",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "industryDetails":{ $arrayElemAt :["$industryDetails", 0]},
                    }
                },
                {
                    $lookup: {
                        from: "employers",
                        localField: "employerId",
                        foreignField: "employerId",
                        as: "employerDetails"
                    }
                },
                {
                    $project: {
                       
                        "industry": "$jobDetails.industry",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "industryDetails":"$industryDetails",
                        "employerDetails":{ $arrayElemAt :["$employerDetails", 0]},
                    }
                },
                {
                    $project: {
                       
                        "industry": "$jobDetails.industry",
                        "jobId": "$jobId",
                        "employerId": "$employerId",
                        "jobDetails": "$jobDetails",
                        "appiliedFrom":"$appiliedFrom",
                        "approvedTo":"$approvedTo",
                        "industryDetails":"$industryDetails",
                        "companyName":"$employerDetails.companyName",
                        "coontactName":"$employerDetails.individualContactName",
                        "memberSince":"$employerDetails.memberSince",
                        "emailId":"$employerDetails.EmailId",
                    }
                },
                {
                    $match:{ $and: [ 
                        paymentStatus=='ALLJOBS'? ({'jobDetails.roleWiseAction.paymentStatus':{$in: paymentStatusDb }}) : (
                        paymentStatus=='PAID'?{'jobDetails.roleWiseAction.paymentStatus':'PAID'}:
                        paymentStatus=='UNPAID'?{'jobDetails.roleWiseAction.paymentStatus':'UNPAID'}:{})
                    ]}
                },
               
                {
                    "$sort": {"createdAt": -1}
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ]
            models.job.aggregate(aggerQuery).exec((error, searchitem) => {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem
                    }) 
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },
    getIndustryById:(req, res, next)=>{
        try{
            let industryId = req.body.industryId ? req.body.industryId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
            });
            if (!req.body.industryId) { return; } 
            models.industry.find({'industryId':industryId}, function (err, item) {
                if(err){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    })
                }
                else {
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item[0]
                    })
                }
            }) 
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },
    editIndustry:(req, res, next)=>{
        try{
            let industryId = req.body.industryId ? req.body.industryId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
            });
            let industryName = req.body.industryName ? req.body.industryName : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Name"
            });

            if (!req.body.industryId || !req.body.industryName  ) { return; }
            let querywhere={
               'industryId':industryId 
            }
            let updatewith={
                'industryName':industryName
            }
            models.industry.updateOne(querywhere,updatewith , function (updatedError, updatedResponse) {
                if(updatedError){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1004'],
                        statuscode: 1004,
                        details: null
                    })
                }
                else{
                    if (updatedResponse.nModified == 1) {
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: null
                        })
                    }
                    else {
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1004'],
                            statuscode: 1004,
                            details: null
                        })
                    }
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },
    updateRole:(req, res, next)=>{
        try{
            let jobRoleId = req.body.jobRoleId ? req.body.jobRoleId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            });
            let jobRoleName = req.body.jobRoleName ? req.body.jobRoleName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Role Name"
            });
           
            if (!req.body.jobRoleId || !req.body.jobRoleName ) { return; }
            let querywhere={
                'jobRoleId':jobRoleId
            }
            let aggerQuery = [
                {
                    $match:
                    {"jobRoleId":jobRoleId}
                }
            ]
            models.jobrole.aggregate(aggerQuery).exec((error, item) => {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    let updatewith={
                        'jobRoleName':jobRoleName,
                    }
                    models.jobrole.updateOne(querywhere,updatewith , function (updatedError, updatedResponse) {
                        if(updatedError){
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else{
                            if (updatedResponse.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    getroleById:(req, res, next)=>{
        try{
            let jobRoleId = req.body.jobRoleId ? req.body.jobRoleId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Role Id"
            }); 
            if (!req.body.jobRoleId ) { return; } 
            models.jobrole.find({'jobRoleId':jobRoleId }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })

        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    changeSkillStatus:(req, res, next)=>{
        try{
            let skillId = req.body.skillId ? req.body.skillId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Id"
            });
            let status = req.body.status ? req.body.status : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Status"
            });
            if (!req.body.skillId || !req.body.status ) { return; }
            let gloabalvarobj={};
            switch (status) {
                case 'false':  gloabalvarobj.statusValue = 'false';                      
                    break;
               case 'true':  gloabalvarobj.statusValue =  'true';
                    break;
                default: gloabalvarobj.statusValue =  'false';
                    break;
            }
            let serachOption =  { 'skillId':  skillId };
            models.skill.findOne(serachOption, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1023'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {
                    let updateFieldsWith =  {
                        $set :  {
                            'status' :  gloabalvarobj.statusValue
                        } 
                    }
                    models.skill.updateOne(serachOption, updateFieldsWith , function(error, response){
                        if(error){
                            res.json({
                                isError: true,
                                message:  errorMsgJSON['ResponseMsg']['404'],
                                statuscode:  404,
                                details: null
                            });
                        }
                        else{
                            if (response.nModified > 0) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['1012'],
                                    statuscode: 1012,
                                    details: null
                                })
                            }
                            else{
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },
    updateSkill:  (req, res, next)=>{
        try{
            let skillId = req.body.skillId ? req.body.skillId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Id"
            });
            let skillName = req.body.skillName ? req.body.skillName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " -Skill Name"
            });
           
            if (!req.body.skillId || !req.body.skillName ) { return; }
            let querywhere={
                'skillId':skillId
               
            }
          
            let aggerQuery = [
                {
                    $match:
                    
                       
                                {"skillId":skillId}
                               
                           
                    
                }
            ]
            models.skill.aggregate(aggerQuery).exec((error, item) => {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    // console.log(item[0].faq[0].position)
                    // console.log(typeof(item[0].faq[0].position))
                    // console.log(item[0].faq[0].questionId)
                    let updatewith={
                        'skillName':skillName,
                       
        
                    }
                    models.skill.updateOne(querywhere,updatewith , function (updatedError, updatedResponse) {
                        if(updatedError){
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else{
                            if (updatedResponse.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            

            })
            
           
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }  
    },
//==============================================================
// made by Ankur on 07-01-2020
/*
    This api is written to delete the skill from the database
*/
    deleteSkillById: (req, res, next) => {
        
        try {
            let skillId = req.body.skillId ? req.body.skillId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Id"
            }); 
            if (!req.body.skillId ) { return; }  
            
            models.skill.remove({'skillId':skillId }, function (err) {
                if(err){
                    
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: null

                    })   
                }
            })

        } catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },

//================================================================    
    getSkillById:  (req, res, next)=>{
        try{
            let skillId = req.body.skillId ? req.body.skillId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Id"
            }); 
            if (!req.body.skillId ) { return; } 
            models.skill.find({'skillId':skillId }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })

        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }   
    },
    getSkill:  (req, res, next)=>{
        try{
            let aggrQuery;
            let pagination = req.body.pagination ? req.body.pagination : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - pagination"
            });
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
            if(pagination=='true'){
             aggrQuery = [
                    {
                        $project : {
                        '_id' : 0,
                        }
                    },
                    {
                        $sort :  {
                          'createdAt' : -1
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total: {
                            $sum: 1
                            },
                            results: {
                            $push : '$$ROOT'
                            }
                        }
                    },
                    {
                        $project : {
                            '_id': 0,
                            'total': 1,
                            'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                            'results': {
                                $slice: [
                                    '$results', page * perPage , perPage
                                ]
                            }
                        }
                    }
                ]
            }
            else{
                aggrQuery = [
                    {
                        $project : {
                        '_id' : 0,
                        }
                    },
                    {
                        $sort :  {
                          'createdAt' : -1
                        }
                    }
                ]
            }
            models.skill.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })   
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }    
    },
    editFaqById:  (req, res, next)=>{
        try{
            let questionId = req.body.questionId ? req.body.questionId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Question Id"
            }); 
            if (!req.body.questionId ) { return; } 
            models.utility.find({'faq.questionId':questionId }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }    
    },
    editFaq:  (req, res, next)=>{
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            let questionId = req.body.questionId ? req.body.questionId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Question Id"
            });
            let question = req.body.question ? req.body.question : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Question"
            });
            let answer = req.body.answer ? req.body.answer : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Answer"
            });
            if (!req.body.userType || !req.body.questionId || !req.body.question || !req.body.answer) { return; }
            let querywhere={
                'faq.userType':userType,
                'faq.questionId':questionId
            }
          
            let aggerQuery = [
                {
                    $match:
                    {
                        $and:
                            [
                                {"faq.userType":userType},
                                {"faq.questionId":questionId}
                            ]
                    }
                }
            ]
            models.utility.aggregate(aggerQuery).exec((error, item) => {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    // console.log(item[0].faq[0].position)
                    // console.log(typeof(item[0].faq[0].position))
                    // console.log(item[0].faq[0].questionId)
                    let updatewith={
                        'question':question,
                        'answer':answer,
                        'userType':userType,
                        'position':item[0].faq[0].position,
                        'questionId':item[0].faq[0].questionId,
        
                    }
                    models.utility.updateOne(querywhere,{'faq':updatewith} , function (updatedError, updatedResponse) {
                        if(updatedError){
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else{
                            if (updatedResponse.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            

            })
            
           
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }   
    },
    getFaq:  (req, res, next)=>{
        try{
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            if (!req.body.userType ) { return; } 
            models.utility.find({'faq.userType':userType }, function (error, searchitem) {
                if(error){
                    console.log('error during search',error)
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: searchitem

                    })   
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }   
    },
    addFaq:  (req, res, next)=>{
        try{
            let globalVarObj={};
            let type = "FAQ";
            let autoFaqId;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autoFaqId = data;
            })
            let userType = req.body.userType ? req.body.userType : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - User Type"
            });
            let question = req.body.question ? req.body.question : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Question"
            });
            let answer = req.body.answer ? req.body.answer : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Answer"
            });
            if (!req.body.userType || !req.body.question || !req.body.answer) { return; }
            let aggerQuery = [
                {
                    $match:{"faq.userType":userType}
                }
            ]
            models.utility.aggregate(aggerQuery).exec((err, item) => {
                globalVarObj.countposition=item.length+1;
                let insertquery = {
                    'userType':userType,
                    'questionId':autoFaqId,
                    'position':globalVarObj.countposition,
                    'question':question,
                    'answer':answer

                };
                models.utility.create({"faq":insertquery}, function (insertedError, insertedData) {
                    if(insertedError){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['404'],
                            statuscode: 404,
                            details: null
                        }) 
                    }
                    else{
                        res.json({
                            isError: false,
                            message: errorMsgJSON['ResponseMsg']['200'],
                            statuscode: 200,
                            details: insertedData

                        })  
                    }
                })
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }   
    },

    /* 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 */
    // This function is written to fetch to get distinct skills on the basis of roles
    getDistinctSkillOnRoleBasis:  (req, res, next)=>{
        try{
            
            let jobRole = req.body.jobRole ? req.body.jobRole : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Role"
            });

            let skillIdList = [];
            let skillDetailList = [];
            
            let totaljobRole=jobRole;
            let aggrQuery = [
                { '$match': 
                    { 'jobRoleId': { '$in': totaljobRole } },
                },
                {
                    $unwind : "$skillIds" 
                },
                {
                    $lookup: {
                        from: "skills",
                        localField: "skillIds",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                  $project: {
                      "jobRoleName": 1,
                      "industryId": 1,
                      "jobRoleId": 1,
                      "skillDetails": {
                          $arrayElemAt: ["$skillDetails", 0]
                      }
                  }   
                },
                {
                    $group: {
                        "_id": "$jobRoleId",
                        "jobRoleName": {
                            $first: "$jobRoleName"
                        },
                        "industryId": {
                            $first: "$industryId"
                        },
                        "jobRoleId": {
                            $first: "$jobRoleId"
                        },
                        "details": {
                            $push: "$skillDetails"
                        }
                    }
                }
            ]
            models.jobrole.aggregate(aggrQuery).exec((error, item) => {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: error
                    }) 
                }
                else{

                    var i;
                    var j;
                    for (i = 0; i < item.length; i++) {
                        
                        for (j = 0; j < item[i].details.length; j++) {

                            if (!skillIdList.includes(item[i].details[j].skillId)) {
                                skillDetailList.push({
                                    "skillName": item[i].details[j].skillName,
                                    "skillId": item[i].details[j].skillId
                                });   
                            }
                            skillIdList.push(item[i].details[j].skillId);
                           
                        }    
                    } // END for (i = 0; i < item.length; i++) {

                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: skillDetailList
                        // details: item,
                    })
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }  

    },
    /* 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 */
    getRoleSkill:  (req, res, next)=>{
        try{
            
            let jobRole = req.body.jobRole ? req.body.jobRole : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Role"
            });
         
            //let totaljobRole=JSON.parse(jobRole)
            let totaljobRole=jobRole;
            let aggrQuery = [
                { '$match': 
                    { 'jobRoleId': { '$in': totaljobRole } },
                },
                {
                    $unwind : "$skillIds" 
                },
                {
                    $lookup: {
                        from: "skills",
                        localField: "skillIds",
                        foreignField: "skillId",
                        as: "skillDetails"
                    }
                },
                {
                  $project: {
                      "jobRoleName": 1,
                      "jobRoleIcon": 1,
                      "industryId": 1,
                      "jobRoleId": 1,
                      "skillDetails": {
                          $arrayElemAt: ["$skillDetails", 0]
                      }
                  }   
                },
                {
                    $group: {
                        "_id": "$jobRoleId",
                        "jobRoleName": {
                            $first: "$jobRoleName"
                        },
                        "jobRoleIcon": {
                            $first: "$jobRoleIcon"
                        },
                        "industryId": {
                            $first: "$industryId"
                        },
                        "jobRoleId": {
                            $first: "$jobRoleId"
                        },
                        "details": {
                            $push: "$skillDetails"
                        }
                    }
                }
            ]
            models.jobrole.aggregate(aggrQuery).exec((error, item1) => {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: error
                    }) 
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item1
                    })
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: error
            });
        }  
    },
    addRoleSkill:(req, res, next)=>{
        try{
            let jobRoleId = req.body.jobRoleId ? req.body.jobRoleId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Role Id"
            });
            let skillIds = req.body.skillIds ? req.body.skillIds : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Ids"
            });
            if (!req.body.jobRoleId) { return; }
            console.log(typeof(skillIds))
            let totalskillIdsArr=skillIds;
            models.jobrole.find({'jobRoleId':jobRoleId}, function (err, item) {
                console.log(item)
                if (item.length==0) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1020'],
                        statuscode: 1020,
                        details: null
                    })
                }
                else {
                    models.jobrole.updateOne({'jobRoleId':jobRoleId},{"skillIds":totalskillIdsArr}, function (updatederror, updatedresponse) {
                        if(updatederror){
                            console.log(updatederror)
                            res.json({
                                isError: true,
                                message: errorMsgJSON['ResponseMsg']['1004'],
                                statuscode: 1004,
                                details: null
                            })
                        }
                        else{
                            if (updatedresponse.nModified == 1) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['200'],
                                    statuscode: 200,
                                    details: null
                                })
                            }
                            else {
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })
                }
            })
        }
        catch(error){
            console.log(error)
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    addSkill:(req, res, next)=>{
        try{
            let skillName = req.body.skillName ? req.body.skillName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Skill Name"
            });
            // let skillInLowerCase=skillName.toLowerCase();
            let skillInLowerCase=skillName;
            let type = "SKILL";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            let insertquery = {
                '_id': new mongoose.Types.ObjectId(),'skillName':skillInLowerCase,
                'skillId':autouserid,'status':true
               };
            models.skill.findOne({'skillName':skillInLowerCase }, function (error, data) {
                console.log()
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if(data==null){
                           models.skill.create(insertquery, function (error, data) {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                } 
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: data

                    })  
                }  
            })
                    }
                    else{
                        res.json({
                            isError: true,
                            message:  errorMsgJSON['ResponseMsg']['1022'],
                            statuscode:  1022,
                            details: null
                        });
                    }
                }
            })
         
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    addJobRole:(req, res, next)=>{
        try{
            let jobRoleName = req.body.jobRoleName ? req.body.jobRoleName : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Job Role Name"
            });
            let industryId = req.body.industryId ? req.body.industryId : res.send({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Industry Id"
            });
            if (!req.body.industryId || !req.body.jobRoleName) { return; }
            let type = "ROLE";
            let autouserid;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autouserid = data;
            })
            models.industry.findOne({'industryId':industryId }, function (error, data) {
                if(error){
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['404'],
                        statuscode: 404,
                        details: null
                    }) 
                }
                else{
                    if(data==null){
                        res.json({
                            isError: true,
                            message: errorMsgJSON['ResponseMsg']['1019'],
                            statuscode: 1019,
                            details: null
                        }) 
                    }
                    else{
                        models.jobrole.findOne({'industryId':industryId ,'jobRoleName':jobRoleName}, function (err, item) {
                            if(err){
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['404'],
                                    statuscode: 404,
                                    details: null
                                }) 
                            }
                            else{
                                if(item){
                                    res.json({
                                        isError: true,
                                        message: errorMsgJSON['ResponseMsg']['1018'],
                                        statuscode: 1018,
                                        details: null
                                    })  
                                }
                                else{
                                    let insertquery = {
                                        '_id': new mongoose.Types.ObjectId(),'jobRoleName':jobRoleName, 'jobRoleIcon': req.body.jobRoleIcon,
                                        'industryId':industryId,'jobRoleId':autouserid
                                       };
            
                                       models.jobrole.create(insertquery, function (error, data) {
                                        if(err){
                                            res.json({
                                                isError: true,
                                                message: errorMsgJSON['ResponseMsg']['404'],
                                                statuscode: 404,
                                                details: null
                                            }) 
                                        } 
                                        else{
                                            res.json({
                                                isError: false,
                                                message: errorMsgJSON['ResponseMsg']['200'],
                                                statuscode: 200,
                                                details: data
                                            })  
                                        }  
                                   })
                                }
                            }    
                        })
                    }
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        } 
    },
    /**
     * @abstract 
     * Get Employer Details
    */
    getEmpDetails:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQuery = [
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $sort :  {
                  'createdAt' : -1
                }
            },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            },
        ]
        try{
            models.employer.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //======================================================================================================

    getEmpDetailsByName:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

        let searchText = req.body.searchText ? req.body.searchText : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Search Text",

        });

        if (!req.body.searchText) {
            return;
        }

        let aggrQuery = [
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $sort :  {
                  'createdAt' : -1
                }
            },
            {
                $project: {
                    "Status": 1,
                    "adminProvidedJobCommission": 1,
                    "companyName": 1,
                    "individualContactName": 1,
                    "industryId": 1,
                    "profilePic": 1,
                    "employerId": 1,
                    "EmailId": 1,
                    "password": 1,
                    "employerType": 1,
                    "userType": 1,
                    "memberSince": 1,
                    "resentSearchDetails": 1,
                    "notification": 1,
                    "templates": 1,
                    "createdAt": 1,
                    "updatedAt": 1

                }
            },

            {
                $match: {
                    $or: [
                        {companyName: { "$regex": searchText, "$options": "i" }},
                        {individualContactName: { "$regex": searchText, "$options": "i" }}
                    ]
                     
                }
            },

            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            },
        ]
        try{
            models.employer.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //======================================================================================================
    getEmpDetailsNew:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQueryWithPagination = [
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $sort :  {
                  'createdAt' : -1
                }
            },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            },
        ];

        let aggrQueryWithOutPagination = [
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $sort :  {
                  'createdAt' : -1
                }
            }
        ];

        let aggrQueryFull = [
            {
                $facet: {
                    "dataWithPagination": aggrQueryWithPagination,
                    "dataWithOutPagination": aggrQueryWithOutPagination,
                }    
            }
        ];

        try{
            models.employer.aggregate(aggrQueryFull).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //======================================================================================================
    /**
     * @abstract 
     * Set approval for Employer
     * first check if employer exists or not by employerId
     * if exists update with user given value
    */
    setEmpApproval:(req, res, next)=>{
        try{
            let empId = req.body.employerId ? req.body.employerId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Employer Id"
            });
            let status = req.body.status ? req.body.status : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Status"
            });
            if (!req.body.employerId || !req.body.status ) { return; }
            let gloabalvarobj={};
            switch (status) {
                case 'false':  gloabalvarobj.statusValue = 'false';                      
                    break;
               case 'true':  gloabalvarobj.statusValue =  'true';
                    break;
                default: gloabalvarobj.statusValue =  'false';
                    break;
            }
            let serachOption =  { 'employerId':  empId };
            models.employer.findOne(serachOption, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1008'],
                        statuscode: 1008,
                        details: null
                    })
                }
                else {
                    let updateFieldsWith =  {
                        $set :  {
                            'Status.isApprovedByAot' :  gloabalvarobj.statusValue
                        } 
                    }
                    models.employer.updateOne(serachOption, updateFieldsWith , function(error, response){
                        if(error){
                            res.json({
                                isError: true,
                                message:  errorMsgJSON['ResponseMsg']['404'],
                                statuscode:  404,
                                details: null
                            });
                        }
                        else{
                            if (response.nModified > 0) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['1012'],
                                    statuscode: 1012,
                                    details: null
                                })
                            }
                            else{
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }
                    })

                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },


     /**
     * @abstract 
     * Get Candidate Details
    */
    getCandidateDetails:(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let aggrQuery = [ 
                {
                    $project : {
                    '_id' : 0,
                    }
                },
                {
                    $sort :  {
                      'createdAt' : -1
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ];
            
            models.candidate.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //=============================================================================================================
    getCandidateDetailsByName:(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let searchText = req.body.searchText ? req.body.searchText : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - searchText"
            });
            
            if (!req.body.searchText) { return; }

            let aggrQuery = [ 
                {
                    $project : {
                    '_id' : 0,
                    }
                },
                {
                    $sort :  {
                      'createdAt' : -1
                    }
                },
                {
                    $project : {
                        "Status": 1,
                        "workExperience": 1,
                        "geolocation": 1,
                        "isPushNotificationStatus": 1,
                        "profilePic": 1,
                        "trainingDetails": 1,
                        "skills": 1,
                        "rating": 1,
                        "jobOffers": 1,
                        "acceptedJobs": 1,
                        "declinedJobs": 1,
                        "appliedJobs": 1,
                        "hiredJobs": 1,
                        "invitationToApplyJobs": 1,
                        "selectedRole": 1,
                        "favouriteBy": 1,
                        "candidateId": 1,
                        "fname": 1,
                        "mname": 1,
                        "lname": 1,
                        "fullname": { $concat: [ "$fname"," ", "$mname","$lname" ] },
                        "EmailId": 1,
                        "password": 1,
                        "userType": "CANDIDATE",
                        "language": 1,
                        "dayoff": 1,
                        "memberSince": 1,
                        "distance": 1,
                        "notification": 1,
                        "docs": 1,
                        "availability": 1,
                        "payloadSkill": 1,
                        "createdAt": 1,
                        "updatedAt": 1,
                    }
                },
                {
                    $match: {
                        fullname: { "$regex": searchText, "$options": "i" } 
                    }
                },
                

                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ];
            
            models.candidate.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //=============================================================================================================
    getCandidateDetailsNew:(req, res, next)=>{
        try{
            let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
            let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

            let aggrQueryWithPagination = [ 
                {
                    $project : {
                    '_id' : 0,
                    }
                },
                {
                    $sort :  {
                      'createdAt' : -1
                    }
                },
                {
                    $group: {
                        _id: null,
                        total: {
                        $sum: 1
                        },
                        results: {
                        $push : '$$ROOT'
                        }
                    }
                },
                {
                    $project : {
                        '_id': 0,
                        'total': 1,
                        'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                        'results': {
                            $slice: [
                                '$results', page * perPage , perPage
                            ]
                        }
                    }
                }
            ];
            

            let aggrQueryWithOutPagination = [ 
                {
                    $project : {
                    '_id' : 0,
                    }
                },
                {
                    $sort :  {
                      'createdAt' : -1
                    }
                }
            ];
            

            let aggrQueryFull = [
                {
                    $facet: {
                        "dataWithPagination": aggrQueryWithPagination,
                        "dataWithOutPagination": aggrQueryWithOutPagination,
                    }    
                }
            ];

            models.candidate.aggregate(aggrQueryFull).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },


    //=============================================================================================================
     /**
     * @abstract 
     * Set approval for Candidate
     * first check if Candidate exists or not by CandidateId
     * if exists update with user given value
    */
    setCandidateApproval:(req, res, next)=>{
        try{
            let candidateId = req.body.candidateId ? req.body.candidateId : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Candidate Id"
            });
            let status = req.body.status ? req.body.status : res.json({
                message: errorMsgJSON['ResponseMsg']['303'] + " - Status"
            });
            if (!req.body.candidateId || !req.body.status ) { return; }
            let gloabalvarobj={};
            switch (status) {
                case 'false':  gloabalvarobj.statusValue = 'false';                      
                    break;
               case 'true':  gloabalvarobj.statusValue =  'true';
                    break;
                default: gloabalvarobj.statusValue =  'false';
                    break;
            }
            let serachOption =  { 'candidateId': candidateId };
            models.candidate.findOne(serachOption, function (err, item) {
                if (item == null) {
                    res.json({
                        isError: true,
                        message: errorMsgJSON['ResponseMsg']['1003'],
                        statuscode: 1003,
                        details: null
                    })
                }
                else{
                    let updateFieldsWith =  {
                        $set :  {
                            'Status.isApprovedByAot' : gloabalvarobj.statusValue
                        } 
                    }
                    models.candidate.updateOne(serachOption, updateFieldsWith , function(error, response){
                        if(error){
                            res.json({
                                isError: true,
                                message:  errorMsgJSON['ResponseMsg']['404'],
                                statuscode:  404,
                                details: null
                            });
                        }
                        else{
                            if (response.nModified > 0) {
                                res.json({
                                    isError: false,
                                    message: errorMsgJSON['ResponseMsg']['1012'],
                                    statuscode: 1012,
                                    details: null
                                })
                            }
                            else{
                                res.json({
                                    isError: true,
                                    message: errorMsgJSON['ResponseMsg']['1004'],
                                    statuscode: 1004,
                                    details: null
                                })
                            }
                        }   
                    })
                }
            })
        }
        catch(error){
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            }); 
        }
    },


    getUnapprovedEmpDetails:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQuery = [
            {
                $match:{
                    'Status.isApprovedByAot' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        try{
            models.employer.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //============================================================================================

    getUnapprovedEmpDetailsByName:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

        let searchText = req.body.searchText ? req.body.searchText : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Search Text "
        });

        if (!req.body.searchText) {
            return;
        }

        let aggrQuery = [
            {
                $match:{
                    // 'Status.isApprovedByAot' : false
                    'Status.isVerified' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },

            {
                $project : {
                    "Status": 1,
                    "adminProvidedJobCommission": 1,
                    "companyName": 1,
                    "individualContactName": 1,
                    "industryId": 1,
                    "profilePic": 1,
                    "employerId": 1,
                    "EmailId": 1,
                    "password": 1,
                    "employerType": 1,
                    "userType": 1,
                    "memberSince": 1,
                    "resentSearchDetails": 1,
                    "notification": 1,
                    "templates": 1,
                    "createdAt": 1,
                    "updatedAt": 1
                }
            },

            {
                $match: {
                    $or: [
                        {companyName: { "$regex": searchText, "$options": "i" }},
                        {individualContactName: { "$regex": searchText, "$options": "i" }}
                    ]
                    
                }
            },

            
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        try{
            models.employer.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //==========================================================================================

    getUnapprovedEmpDetailsNew:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

        let aggrQueryWithPagination = [
            {
                $match:{
                    'Status.isApprovedByAot' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            { $sort : { "createdAt" : -1 } },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ];

        let aggrQueryWithOutPagination = [
            {
                $match:{
                    'Status.isApprovedByAot' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            { $sort : { "createdAt" : -1 } },
        ];

        
        let aggrQueryFull = [
            {
                $facet: {
                    "dataWithPagination": aggrQueryWithPagination,
                    "dataWithOutPagination": aggrQueryWithOutPagination,
                }    
            }
        ];


        try{
            models.employer.aggregate(aggrQueryFull).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },


    //==========================================================================================
    getUnapprovedCandidateDetails:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQuery = [
            {
                $match:{
                    'Status.isApprovedByAot' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        try{
            models.candidate.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    //==================================================================================================
    getUnapprovedCandidateDetailsByName:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;

        let searchText = req.body.searchText ? req.body.searchText : res.json({
            isError: true,
            message: errorMsgJSON['ResponseMsg']['303'] + " - Search Text "
        });

        if (!req.body.searchText) {
            return;
        }
        
        let aggrQuery = [
            {
                $match:{
                    // 'Status.isApprovedByAot' : false
                    'Status.isVerified' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },

            {
                $project: {
                    
                    "Status": 1,
                    "workExperience": 1,
                      "geolocation": 1,
                      "profilePic": 1,
                      "trainingDetails": 1,
                      "skills": 1,
                      "rating": 1,
                      "jobOffers": 1,
                      "acceptedJobs": 1,
                      "declinedJobs": 1,
                      "appliedJobs": 1,
                      "hiredJobs": 1,
                      "invitationToApplyJobs": 1,
                      "selectedRole": 1,
                      "favouriteBy": 1,
                      "candidateId": 1,
                      "fname": 1,
                      "mname": 1,
                      "lname": 1,
                      "fullname": { $concat: [ "$fname", " ","$mname", "$lname" ] }, 
                      "EmailId": 1,
                      "password": 1,
                      "userType": 1,
                      "memberSince": 1,
                      "distance": 1,
                      "language": 1,
                      "dayoff": 1,
                      "notification": 1,
                      "docs": 1,
                      "availability": 1,
                      "payloadSkill": 1,
                      "createdAt": 1,
                      "updatedAt": 1
                }     
            },

            {
                $match: {
                    fullname: { "$regex": searchText, "$options": "i" } 
                }
            },

            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ]
        try{
            models.candidate.aggregate(aggrQuery).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    },

    getUnapprovedCandidateDetailsNew:(req, res, next)=>{
        let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
        let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
        let aggrQueryWithPagination = [
            {
                $match:{
                    'Status.isApprovedByAot' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },

            { $sort : { createdAt : -1 } },

            {
                $group: {
                    _id: null,
                    total: {
                    $sum: 1
                    },
                    results: {
                    $push : '$$ROOT'
                    }
                }
            },
            {
                $project : {
                    '_id': 0,
                    'total': 1,
                    'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
                    'results': {
                        $slice: [
                            '$results', page * perPage , perPage
                        ]
                    }
                }
            }
        ];

        let aggrQueryWithOutPagination = [
            {
                $match:{
                    'Status.isApprovedByAot' : false
                }
            },
            {
                $project : {
                '_id' : 0,
                }
            },
            { $sort : { createdAt : -1 } },
        ];


        let aggrQueryFull = [
            {
                $facet: {
                    "dataWithPagination": aggrQueryWithPagination,
                    "dataWithOutPagination": aggrQueryWithOutPagination,
                }    
            }
        ];


        try{
            models.candidate.aggregate(aggrQueryFull).exec(function(err, item){
                if(err){
                    res.json({
                        isError: true,
                        message:  errorMsgJSON['ResponseMsg']['404'],
                        statuscode:  404,
                        details: null
                    });
                }
                else{
                    res.json({
                        isError: false,
                        message: errorMsgJSON['ResponseMsg']['200'],
                        statuscode: 200,
                        details: item
                    })
                }
            })
        }
        catch (error) {
            res.json({
                isError: true,
                message:  errorMsgJSON['ResponseMsg']['404'],
                statuscode:  404,
                details: null
            });
        }
    }












//===========END Controller ================================================================               
}