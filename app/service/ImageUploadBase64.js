const multer = require('multer');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');
const env = require('../env');
const AutogenerateIdcontroller = require('./AutogenerateId')




const Base64ImgUploadmodule= {
    Base64ImgUpload:(request,callback)=>{
      console.log('key from environment',env.secretAccessKey)
      console.log('id from environment',env.accessKeyId)
      console.log('region from environment',env.region)
        let fldrName=request.fldrName;
        var type1 = request.imgName+"_";
        let autouserid;
        AutogenerateIdcontroller.autogenerateId(type1, (err, data) => {
            autouserid = data;
        })
      
        buf = new Buffer(request.picture.replace(/^data:image\/\w+;base64,/, ""),'base64')
        console.log(buf)
        const type = request.picture.split(';')[0].split('/')[1];
        
        console.log("...",type)
       


        let bucketName = 'aot-ara/'+ fldrName;

     

        const s3 = new aws.S3({
          accessKeyId: env.accessKeyId,
          secretAccessKey: env.secretAccessKey,
          region: env.region,
        });
       
        var params = {
            ACL: 'public-read',
            Bucket: bucketName, 
            Key: autouserid+'.'+request.fileextension,            
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: `image/${type}`            
          };
          s3.putObject(params, function(err, data){
              if (err) { 
                console.log("ooooooooo",err)
                callback(err,null)
              } else {
                let imageUrl="https://s3.us-east-2.amazonaws.com/aot-ara/"+fldrName+"/"+autouserid+"."+request.fileextension;
               
                callback(null,imageUrl.toString())
            }
          });





     }
}

module.exports = Base64ImgUploadmodule;
