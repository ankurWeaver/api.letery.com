const nodemailer = require('nodemailer');
const config =  require('../config/env');
const employerJobBusiness = require("../businessLogic/employer-job-business");

module.exports = {
  
  viaGmail: async (data,callback) => {  
    let htmlSubject;
    let mainSubject;

    // Here we are retrieving the mail of admin for sending mail
    let fetchAdminEmailForMailingStatus = await employerJobBusiness.fetchAdminEmailForMailing();
    
    let emailForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.emailForMailing : "";
    let passwordForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.passwordForMailing : "";
    
    if(data.key=='sign'){
     htmlSubject='Please Click on the link to verify your Account';
     mainSubject='Account Verification'
    }
    else{
      htmlSubject=' Please click on the link to verify your email';
      mainSubject='Verification for Reset Password'
    }
   
    var transporter = nodemailer.createTransport({
        service:'gmail',
        auth: {
          // user: 'arindam.bhattacharyya@pkweb.in',
          // pass: 'Arindam@weavers'
          user: emailForMailing,
          pass: passwordForMailing
        }
    }); 
   
    // let link="http://localhost:3132/api"+data.path+data.tocken;
    // let link=`${config.app.FORMATTED_URL}/api${data.path}${data.tocken}`;https://api.letery.com/api/employer-profile/verify-emp?id=
    //let link=`https://api.letery.com/api/common/verify-mail?id=`+data.tocken;
    let link=`${config.app.FORMATTED_URL_NEW}/api/common/verify-mail?id=`+data.tocken;
    const mailoption = {
      // from: 'Seleckt<ankur.weavers@gmail.com>',
      from: 'Seleckt<'+emailForMailing+'>',
      to:data.receiver,
      html : "<br>"+htmlSubject+"<br><a clicktracking=off href="+link+">Click here to verify</a>" ,
      subject:mainSubject
     
    };

    transporter.sendMail(mailoption, (err, info) => {
      if (err) {
        console.log(err)
        callback(err,null)
      } else {
        callback(null,info)
      }
   });
   

  }
 
}