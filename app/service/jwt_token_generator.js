const jwt = require('jsonwebtoken');
const env = require('../env');

module.exports = (payload) => {
    console.log('payload',payload);
    
    return jwt.sign(
        payload,
        env.JWT_KEY,
        {
            expiresIn: "12h"
        }
    )
}