
const AutogenerateIdcontroller = require('../services/AutogenerateId');
const Transaction = require('../release/v1/models/transaction');
const stripe_s_key = 'sk_test_h7EGCSjkpdzY27zI1USUhvlD';
const stripe = require('stripe')(stripe_s_key);
const mongoose = require('mongoose');

//TODO : should be saved in .env file before prod deployment



module.exports ={

    createCharge : (rrid,amount,currency="usd",source,desc,txnFrom,txnTo)=>{

      return new Promise((resolve, reject) => {
        let amountInCents = amount*100;
        stripe.charges.create({
          amount: amountInCents,
          currency: currency,
          customer: source,        //"tok_visa", // obtained with Stripe.js
          description: desc
        }, function(err, charged) {
          if (err) {
            // error in charging 
            reject({
                isError: true,
                message: 'Problem in charging payment.', //TODO
                statuscode: 404,
                details: null
            })
          }
          else{
            var type = "TXN";
            let autoTxnId;
            AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                autoTxnId = data;
            })
            // Create transaction log
            let transactionObj = {
                '_id': new mongoose.Types.ObjectId(),
                // 'userRef': userId,
                'rrid': rrid,
                'transactionId': autoTxnId,
                'transactionAmount': amount,
                'transactionType': 'DEBIT',
                'transactionFrom' : txnFrom,
                'transactionTo' : txnTo,
                'stripeDetails': charged
            }
            Transaction.create(transactionObj, function (err, txnResponse) {
              if (err) {
                reject({
                    isError: true,
                    message: 'Error occured when creating transaction', //TODO
                    statuscode: 404,
                    details: null
                })
              }
              if (txnResponse) {
                resolve(amount)
              }
            })
          }
        });
      })


    },

    createTransfer : (amount,currency="usd",destinationId,transferGroup,txnFrom,txnTo)=>{

      return new Promise((resolve, reject) => {
         let amountInCents = amount*100;
        stripe.transfers.create({
            amount: amountInCents,
            currency: currency,
            destination: destinationId,   // "acct_1032D82eZvKYlo2C"
            // transfer_group: transferGroup
          }, function(err, transfer) {
            if (err) {
              // error in charging 
              console.log("Stripe transfer error : ",err);
              reject({
                  isError: true,
                  message: 'Problem in charging payment.', //TODO
                  statuscode: 404,
                  details: null
              })
            }
            else{
              var type = "TXN";
              let autoTxnId;
              AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
                  autoTxnId = data;
              })
              // Create transaction log
              let transactionObj = {
                  '_id': new mongoose.Types.ObjectId(),
                  // 'userRef': userId,
                  'transactionId': autoTxnId,
                  'transactionAmount': amount,
                  'transactionType': 'TRANSFER',
                  'transactionFrom' : txnFrom,
                  'transactionTo' : txnTo,
                  'stripeDetails': transfer
              }
              Transaction.create(transactionObj, function (err, txnResponse) {
                if (err) {
                  reject({
                      isError: true,
                      message: 'Error occured when creating transaction', //TODO
                      statuscode: 404,
                      details: null
                  })
                }
                if (txnResponse) {
                  resolve(amount)
                }
              })
            }
          });
      })
       
    }

    
}



