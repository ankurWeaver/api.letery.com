var admin = require("firebase-admin");

var serviceAccount = require("../../cred/twelftest-firebase-adminsdk-q9b40-99df644bff.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://twelftest.firebaseio.com"
});

const db = admin.firestore();

module.exports={

    // Add data
    addDataToFireStore(collectionName,docName,valueJSON){
        var docRef = db.collection(collectionName).doc(docName);
        var setAda = docRef.set(valueJSON)
        return setAda
    },

    //Read data
    readDataFromFireStore(collectionName,docName)
    {
        db.collection(collectionName).get()
        .then((snapshot) => {
            snapshot.forEach((doc) => {
            console.log(doc.id, ':', doc.data());
            return snapshot
            });
        })
        .catch((err) => {
            console.log('Error getting documents', err);
            return ''
        });
    }




   
}

