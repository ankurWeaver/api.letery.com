var admin = require("firebase-admin");

var fcmAccount = require("../../aot-seleckt-firebase-adminsdk-tfmkw-b7381c4cb9.json");

admin.initializeApp({
  credential: admin.credential.cert(fcmAccount),
  databaseURL: "https://aot-seleckt.firebaseio.com"
});


module.exports =  {
 
  
  sendTocken: (regtoken, title = "Welcome to Seleckt", body , data = {}) => {
  return new Promise((resolve, reject) => {
    var registrationToken = regtoken;
   
    var message = {
      token: registrationToken,
      notification: {
        title,
        body
      },
      data,
      "android": {
        "ttl": 86400,
        "notification": {
          "click_action": "OPEN_ACTIVITY_1"
        }
      },
      "apns": {
        "headers": {
          "apns-priority": "5",
        },
        "payload": {
          "aps": {
            "category": "NEW_MESSAGE_CATEGORY"
          }
        }
      },
      "webpush": {
        "headers": {
          "TTL": "86400"
        }
      }
    }



    admin.messaging().send(message)
      .then((response) => {
        console.log('Successfully sent message(shopper):', response);
        resolve(true)
      //resolve(response)
      })
      .catch((error) => {
        // console.log('Error sending message(shopper):', error);
         //reject(error)
        resolve(false)


      }) 
   })
}
}
