const nodemailer = require('nodemailer');
const config =  require('../config/env');
const employerJobBusiness = require("../businessLogic/employer-job-business");

module.exports = {
  viaGmail: async (data,callback) => {
    let htmlSubject;
    let mainSubject;
    if(data.key=='sign'){
        //  htmlSubject='Please Click on the link to verify your Account';
        htmlSubject="Welcome to Seleckt Staff, where you're able to fill your positions at the click of a button. Your registration is being processed and a member of our sales team will be in touch with you as soon as possible.";
        mainSubject='Account Verification'
    }
    else{
      htmlSubject=' Please click on the link to verify your email';
      mainSubject='Verification for Reset Password'
    }

    // Here we are retrieving the mail of admin for sending mail
    let fetchAdminEmailForMailingStatus = await employerJobBusiness.fetchAdminEmailForMailing();  
    let emailForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.emailForMailing : "";
    let passwordForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.passwordForMailing : "";
   
    var transporter = nodemailer.createTransport({
        service:'gmail',
        auth: {
          // user: 'arindam.bhattacharyya@pkweb.in',
          // pass: 'Arindam@weavers'
          user: emailForMailing,
          pass: passwordForMailing
        }
    //     tls: {
    //       rejectUnauthorized: false
    //     },
    //     proxy: 'http://localhost:3000'
    }); 
   
   
    // let link=`${config.app.FORMATTED_URL}/api${data.path}${data.tocken}`;https://api.letery.com/api/employer-profile/verify-emp?id=
    //let link=`https://api.letery.com/api/employer-profile/verify-emp?id=`+data.tocken;
    let link=`${config.app.FORMATTED_URL_NEW}/api/employer-profile/verify-emp?id=`+data.tocken;
    const mailoption = {
      from: 'Seleckt<'+emailForMailing+'>',
      to:data.receiver,
      html : "<br>"+htmlSubject+"<br><a clicktracking=off href="+link+">Click here to verify</a>" ,
      subject:mainSubject
     
    };

    transporter.sendMail(mailoption, (err, info) => {
      if (err) {
        console.log(err)
        callback(err,null)
      } else {
        callback(null,info)
      }
   });
   

  }
 
}