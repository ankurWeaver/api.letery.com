const nodemailer = require('nodemailer');
const employerJobBusiness = require("../businessLogic/employer-job-business");

module.exports = {
  viaGmail: async (data,callback) => {

    // Here we are retrieving the mail of admin for sending mail
    let fetchAdminEmailForMailingStatus = await employerJobBusiness.fetchAdminEmailForMailing();  
    let emailForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.emailForMailing : "";
    let passwordForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.passwordForMailing : "";
    
   
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        // user: 'sourav.weavers@gmail.com',
        // pass: 'Sourav@weavers'
        user: emailForMailing,
        pass: passwordForMailing
      },
      tls: {
        rejectUnauthorized: false
    }
    }); 

    const mailoption = {
      // from: 'Seleckt<sourav.weavers@gmail.com>',
      from: 'Seleckt<'+emailForMailing+'>',
      to: data.receiver,
      subject: data.subject,
      text:data.msg.toString(),
     
    };

    transporter.sendMail(mailoption, (err, info) => {
      if (err) {
        console.log(err)
        callback(err,null)
      } else {
        callback(null,info)
      }
   });
   

  }
 
}