
const mongoose = require('mongoose');

const config =  require('../config/env');


mongoose.connect(config.app.dbURI,{
    useNewUrlParser:true,
    useCreateIndex: true
});

// mongoose.connect('mongodb://localhost:27017/Seleckt',{
//   useNewUrlParser:true
// });
// mongoose.connect('mongodb://aotstaging:rA)qN5ft%26C@34.254.105.101:27017/aotstaging',{
//   useNewUrlParser:true
// });

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {  
    console.log(' Mongoose default connection open to ' +  config.app.dbURI);
  }); 

  // If the connection throws an error
mongoose.connection.on('error',function (err) {  
    console.log('Mongoose default connection error: ' + err);
  });
  // When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
    console.log('Mongoose default connection disconnected'); 
  });

  // If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
      console.log('Mongoose default connection disconnected through app termination'); 
      process.exit(0); 
    }); 
  });
   

