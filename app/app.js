const  appRoutes =  require('./routes');

const models = require('./model/index');
const express = require('express');
const app = express();
const morgan =  require('morgan');
const bodyParser =  require('body-parser');
const dbConnection = require('./service/db-connection');
const errorMsgJSON = require('./service/errors.json');
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');    
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, Content-Type, Authorization');


    if(req.method === 'OPTIONS'){
        res.header("Access-Control-Allow-Methods" , 'PUT, POST, PATCH, DELETE, GET');     
        return res.status(200).json({});   
    }
    next();
});

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.use('/api', appRoutes);

app.use('/api2',function (res, res) {
    res.json({
        nam:"ankur"
    });
});





app.use((err, req, res, next)=>{
   console.log(res)
    res.json({
        isError : true,
        message : 'Server Faault',
        statuscode : '500',
        details : res

    });
});



module.exports = app;