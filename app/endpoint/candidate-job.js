
const canjobCtrl = require('../controller/candidate-job');
const express =  require('express');
const canjobRoutes =  express.Router();
/**************************************************************
 * Employer job post  Related route
***************************************************************/
canjobRoutes.post('/check-isWorking-day', canjobCtrl.checkIsWorkingDay);
canjobRoutes.post('/get-time-tracking', canjobCtrl.getTimeTracking);


canjobRoutes.post('/fetch-authenticity-of-candidate', canjobCtrl.fetchAuthenticityOfCandidate);
canjobRoutes.post('/cancel-job-for-candidate', canjobCtrl.cancelJobForCandidate);

canjobRoutes.post('/get-time-tracking-for-date', canjobCtrl.getTimeTrackingForDate);

canjobRoutes.post('/automatic-checkout', canjobCtrl.autometicChechOut);
canjobRoutes.post('/get-candidatewise-attendance', canjobCtrl.getCandidateWiseAttendance);
canjobRoutes.post('/get-datewise-calender-data', canjobCtrl.getDateWiseCalenderData);
canjobRoutes.post('/offline-checkout', canjobCtrl.offlineCheckOut);
canjobRoutes.post('/offline-checkin', canjobCtrl.offlineCheckIn);
canjobRoutes.post('/onsite-checkin', canjobCtrl.onlineCheckIn);
canjobRoutes.post('/onsite-checkout', canjobCtrl.onlineCheckOut);
canjobRoutes.post('/populate-calender-data', canjobCtrl.populateCalenderData);
canjobRoutes.post('/create-calender-data', canjobCtrl.createCalenderData);
canjobRoutes.post('/get-appointments', canjobCtrl.getAppointments);
canjobRoutes.post('/job-decline', canjobCtrl.jobDecline);
canjobRoutes.post('/job-accept', canjobCtrl.jobAccept);
canjobRoutes.post('/get-jobs1', canjobCtrl.searchJobs1);// changed according to new job schema structure
//canjobRoutes.post('/get-candidatelist', canjobCtrl.candidateList);// changed according to new job schema structure
canjobRoutes.post('/apply-jobs', canjobCtrl.applyJobs);//Tested,working fine ,dont need to change according to new job structure
canjobRoutes.post('/job-details1', canjobCtrl.candidateJobDetails1);// changed according to new job schema structure
canjobRoutes.post('/apply-invitation', canjobCtrl.applyAgainstinvitation);

canjobRoutes.post('/test', canjobCtrl.test);

/* old apis' for old structure*/
canjobRoutes.post('/get-jobs', canjobCtrl.searchJobs);
canjobRoutes.post('/job-details', canjobCtrl.candidateJobDetails);
canjobRoutes.post('/fetch-notification-for-candidate', canjobCtrl.fetchNotificationforCandidate);
canjobRoutes.post('/delete-notification-for-candidate', canjobCtrl.deleteNotificationforCandidate);

/* Push Notification Api */
canjobRoutes.post('/get-push-status', canjobCtrl.getPushStatus);
canjobRoutes.post('/set-push-status', canjobCtrl.setPushStatus);
canjobRoutes.post('/offsite-tracking', canjobCtrl.offsiteTracking);
canjobRoutes.post('/fetch-office-trackin-details', canjobCtrl.fetchOffSiteTraching);

module.exports = canjobRoutes;