
const candidateProfileCtrl = require('../controller/candidate-profile');
const express =  require('express');
const candidateProfileRoutes =  express.Router();
/**************************************************************
 * Candidate Profile Creation route
***************************************************************/
candidateProfileRoutes.post('/add-dayoff', candidateProfileCtrl.addDayOff);
candidateProfileRoutes.post('/add-avilability', candidateProfileCtrl.addAvailability);
candidateProfileRoutes.post('/add-attachments', candidateProfileCtrl.addAttachments);
candidateProfileRoutes.post('/add-location', candidateProfileCtrl.addLocation);
candidateProfileRoutes.post('/add-roles', candidateProfileCtrl.addSelectedRoles);
candidateProfileRoutes.post('/add-personal-details', candidateProfileCtrl.addPersonalDetails);
candidateProfileRoutes.post('/add-language', candidateProfileCtrl.addLanguage);
candidateProfileRoutes.post('/add-experience', candidateProfileCtrl.addExperience);
candidateProfileRoutes.post('/profile-pic-upload', candidateProfileCtrl.profilePicUpload);
/*************************************************************************
 * If all details for profile creation are needed to store at one attempt, the api route is below
 **************************************************************************/
candidateProfileRoutes.post('/add-full-details', candidateProfileCtrl.addFullDetails);

/******************************************************************
 * Candidate profile modification 
 ******************************************************************/
candidateProfileRoutes.post('/get-selected-languages', candidateProfileCtrl.getSelectedLanguage);
candidateProfileRoutes.post('/delete-experience', candidateProfileCtrl.deleteExperience);
candidateProfileRoutes.post('/update-experience', candidateProfileCtrl.updateExperience);
candidateProfileRoutes.post('/edit-experience', candidateProfileCtrl.editExperience);
candidateProfileRoutes.post('/get-details', candidateProfileCtrl.getDetails);
candidateProfileRoutes.post('/get-candidate-details', candidateProfileCtrl.getCandidateDetails);
candidateProfileRoutes.post('/get-candidate-details-withstatus', candidateProfileCtrl.getCandidateDetailsWithStatus);
candidateProfileRoutes.post('/delete-Doc', candidateProfileCtrl.deleteDocById);
candidateProfileRoutes.post('/get-ratings', candidateProfileCtrl.getRatings);
candidateProfileRoutes.post('/delete-candidate-work-exp', candidateProfileCtrl.deleteCandidateWorkExp);
candidateProfileRoutes.post('/delete-day-off', candidateProfileCtrl.deleteDayOff);
candidateProfileRoutes.post('/delete-availability', candidateProfileCtrl.deleteAvailability);


candidateProfileRoutes.post('/view-candidate-profile', candidateProfileCtrl.viewCandidateProfile);

/****************************************************************
 * Verify Candidate Route 
 ****************************************************************/
candidateProfileRoutes.get('/verify-candidate', candidateProfileCtrl.saveEmailVerificationTocken);
module.exports = candidateProfileRoutes;