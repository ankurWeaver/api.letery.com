
const candidateCtrl = require('../controller/candidate-authentication');
const express =  require('express');
const candidateRoutes =  express.Router();
/**************************************************************
 * Login Registration route
***************************************************************/
candidateRoutes.post('/signup', candidateCtrl.signup);
candidateRoutes.post('/signup-via-social', candidateCtrl.signupViaSocial);
candidateRoutes.post('/send-otp', candidateCtrl.sendOtp);
candidateRoutes.post('/verify-otp', candidateCtrl.verifyOtp);
candidateRoutes.post('/change-password', candidateCtrl.changePassword);
candidateRoutes.post('/send-verification-link', candidateCtrl.sendVerificationLink);
candidateRoutes.post('/test', candidateCtrl.addmultipleAddress);

/**************************************************************
 * Profile section route
***************************************************************/
candidateRoutes.post('/profile-change-password', candidateCtrl.profilechangePassword);
module.exports = candidateRoutes;