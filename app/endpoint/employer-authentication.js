
const empCtrl = require('../controller/employer-authentication');
const express =  require('express');
const empRoutes =  express.Router();
/**************************************************************
 * Registration and verification related route
***************************************************************/
empRoutes.post('/signup', empCtrl.signup);
empRoutes.post('/verify-otp', empCtrl.verifyOtp);
empRoutes.post('/change-password', empCtrl.changePassword);
empRoutes.post('/send-link', empCtrl.sendConfirmationLink);
empRoutes.post('/signup-via-social', empCtrl.signupViaSocial);

module.exports = empRoutes;