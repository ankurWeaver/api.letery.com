
const empjobCtrl = require('../controller/employer-job');
const express =  require('express');
const empjobRoutes =  express.Router();
/**************************************************************
 * Employer job post  Related route
***************************************************************/
empjobRoutes.post('/fetch-authenticity-of-employer', empjobCtrl.fetchAuthenticityOfEmployer);
empjobRoutes.post('/get-employed-candidate-byname', empjobCtrl.getEmployedCandidateByName);
empjobRoutes.post('/get-employed-candidate', empjobCtrl.getEmployedCandidate);
empjobRoutes.post('/mark-favourite', empjobCtrl.markFavourite);//D
empjobRoutes.post('/mark-unfavourite', empjobCtrl.markUnFavourite);//D
empjobRoutes.post('/get-apllied-candidate1', empjobCtrl.getAppliedCandidate1);// changed according to new job schema structure
empjobRoutes.post('/job-approve', empjobCtrl.jobApproveTo);//D
empjobRoutes.post('/get-closed-jobs', empjobCtrl.getClosedJobs);//D
empjobRoutes.post('/get-jobs1', empjobCtrl.getJobs1);//D// changed according to new job schema structure
empjobRoutes.post('/get-onging-jobs', empjobCtrl.getOngoingJobs);//D// changed according to new job schema structure
empjobRoutes.post('/post-job', empjobCtrl.PostJob);
empjobRoutes.post('/get-jobById', empjobCtrl.GetJobById);
empjobRoutes.post('/add-template', empjobCtrl.addTemplate);//D// changed according to new job schema structure
empjobRoutes.post('/get-all-template', empjobCtrl.getAllTemplates);//D// changed according to new job schema structure
empjobRoutes.post('/get-template', empjobCtrl.getTemplate);//D// changed according to new job schema structure
empjobRoutes.post('/post-multi-job1', empjobCtrl.postMultipleJob1);//D// changed according to new job schema structure
//empjobRoutes.post('/seach-posted-job', empjobCtrl.searchPostedJob);
empjobRoutes.post('/get-candidatelist', empjobCtrl.candidateList);//D// changed according to new job schema structure
empjobRoutes.post('/get-last-search-name', empjobCtrl.getLastSearchNameDetails);//D// changed according to new job schema structure
empjobRoutes.post('/search-job-byName1', empjobCtrl.serachJobByName1);//D// changed according to new job schema structure
empjobRoutes.post('/hire-candidate', empjobCtrl.hireCandidate);//D
empjobRoutes.post('/edit-job', empjobCtrl.editJob);//D
empjobRoutes.post('/update-job', empjobCtrl.updateJob);//D
empjobRoutes.post('/give-rating', empjobCtrl.giveRating);//D
empjobRoutes.post('/search-Ongoingjob-byName', empjobCtrl.serachOngingJobByName);//D
empjobRoutes.post('/search-completedjob-byName', empjobCtrl.serachCompletedJobByName);//D
empjobRoutes.post('/get-favourite-candidate-for-emp', empjobCtrl.getFavouriteCandidateForEmp);//D

/* old apis' for old structure*/

empjobRoutes.post('/post-multi-job', empjobCtrl.postMultipleJob);
empjobRoutes.post('/get-jobs', empjobCtrl.getJobs);
empjobRoutes.post('/search-job-byName', empjobCtrl.serachJobByName);
empjobRoutes.post('/get-apllied-candidate', empjobCtrl.getAppliedCandidate);

/* suggestion after job post */
empjobRoutes.post('/aot-offer-favourite', empjobCtrl.offerWithFavouriteOne); // not in use
empjobRoutes.post('/aot-offer-favourite-tier1', empjobCtrl.offerWithFavouriteOneTier1);
empjobRoutes.post('/aot-offer-favourite-tier2', empjobCtrl.offerWithFavouriteOneTier2);


empjobRoutes.post('/aot-offer-favourite-and', empjobCtrl.offerWithFavouriteOneAnd); // not in use
empjobRoutes.post('/aot-offer-favourite-and-tier1', empjobCtrl.offerWithFavouriteOneAndTier1);
empjobRoutes.post('/aot-offer-favourite-and-tier2', empjobCtrl.offerWithFavouriteOneAndTier2);


empjobRoutes.post('/aot-offer-all', empjobCtrl.aotOfferAll);

empjobRoutes.post('/aot-choose-workers', empjobCtrl.aotChooseWorker);
empjobRoutes.post('/aot-choose-workers-and', empjobCtrl.aotChooseWorkerAnd);

empjobRoutes.post('/browse-candidate-offer', empjobCtrl.browseCandidateForOffer);
empjobRoutes.post('/browse-candidate-offer-and', empjobCtrl.browseCandidateForOfferAnd);

empjobRoutes.post('/get-roles-ByJobId', empjobCtrl.getRolesByJobId);
empjobRoutes.post('/get-skills-ByJobroleId', empjobCtrl.getSkillsByJobRoleId);
empjobRoutes.post('/get-skills-By-many-JobroleId', empjobCtrl.getSkillsByManyJobRoleId);

empjobRoutes.post('/fetched-cancelled-jobs', empjobCtrl.fetchedCancelledJobs);
empjobRoutes.post('/cancel-job-for-by-employer', empjobCtrl.cancelJobForByEmployer);
empjobRoutes.post('/cancel-job-for-candidate-by-employer', empjobCtrl.cancelJobForCandidateByEmployer);
empjobRoutes.post('/invite-to-apply', empjobCtrl.inviteToApply);
// empjobRoutes.post('/invite-to-apply', empjobCtrl.inviteToApply);

empjobRoutes.post('/fetch-notification-for-employer', empjobCtrl.fetchNotificationforEmployer);
empjobRoutes.post('/delete-notification-for-employer', empjobCtrl.deleteNotificationforEmployer);

empjobRoutes.post('/fetch-completed-job-of-candidate', empjobCtrl.fetchCompletedJobOfCandidate);
empjobRoutes.post('/delete-template', empjobCtrl.deleteTemplate);


// edit
empjobRoutes.post('/get-candidate-to-mark-favourite', empjobCtrl.getCandidateToMarkFavourite);
empjobRoutes.post('/check-candidate-presence', empjobCtrl.checkCandidatePresenceForJob);

empjobRoutes.post('/get-all-candidate-for-emp', empjobCtrl.getAllCandidateForEmp);
// empjobRoutes.post('/get-all-candidate-for-emp-for-role-and-skill', empjobCtrl.getAllCandidateForEmpForRoleAndSkill);



empjobRoutes.post('/determine-shiftname-logic1', empjobCtrl.determineShiftnameLogic1);//D
empjobRoutes.post('/for-testing', empjobCtrl.forTesting);//D



module.exports = empjobRoutes;



























