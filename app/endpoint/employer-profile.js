
const empprofileCtrl = require('../controller/employer-profile');
const express =  require('express');
const empprofileRoutes =  express.Router();
const checkAuth = require('../middleware/check-auth');
/**************************************************************
 * Employer Profile Related route
***************************************************************/
empprofileRoutes.post('/add-signup-industry', empprofileCtrl.addSignupIndustry);
empprofileRoutes.post('/add-company',checkAuth, empprofileCtrl.addCompany);
empprofileRoutes.post('/add-address',checkAuth, empprofileCtrl.addAddress);
empprofileRoutes.post('/add-contact',checkAuth, empprofileCtrl.addContact);
// empprofileRoutes.post('/get-details',checkAuth, empprofileCtrl.getDetails);
empprofileRoutes.post('/get-details', empprofileCtrl.getDetails);

empprofileRoutes.post('/fetch-industry-for-employer', empprofileCtrl.fetchIndustryForEmployer);

empprofileRoutes.post('/pic-upload', empprofileCtrl.profilePicUrlUpload);//d
empprofileRoutes.post('/profile-change-password', checkAuth,empprofileCtrl.profileChangePassword);//d
empprofileRoutes.post('/profile-pic-upload', empprofileCtrl.profilePicUpload);//d
empprofileRoutes.post('/pic-As-File-upload', empprofileCtrl.profilePicAsFileUpload);
empprofileRoutes.get('/verify-emp', empprofileCtrl.saveEmailVerificationTocken);
module.exports = empprofileRoutes;