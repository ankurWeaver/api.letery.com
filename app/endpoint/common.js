
const commonCtrl = require('../controller/common');
const express =  require('express');
const commonRoutes =  express.Router();
const checkAuth = require('../middleware/check-auth');
/**************************************************************
 * Login  route for both candidate and employer
***************************************************************/
commonRoutes.post('/get-no-of-cancelled-job',  commonCtrl.getNoOfCancelledJob);//D
commonRoutes.post('/get-contactus',  commonCtrl.getContact);//D
commonRoutes.post('/add-contactus',  commonCtrl.addContact);//D
commonRoutes.post('/generate-tocken',  commonCtrl.generateTocken);//D
commonRoutes.post('/get-aboutus',  commonCtrl.getAboutUs);//D
commonRoutes.post('/get-policy',  commonCtrl.getPolicy);//D
commonRoutes.post('/get-condition',  commonCtrl.getCondition);//D
commonRoutes.post('/add-condition',  commonCtrl.addCondition);//D
commonRoutes.post('/add-policy',  commonCtrl.addPolicy);//D
commonRoutes.post('/add-aboutus',  commonCtrl.addAboutUs);//D
commonRoutes.post('/get-role',  commonCtrl.getRole);//D
commonRoutes.post('/get-industry',  commonCtrl.getIndustry);//D
commonRoutes.post('/add-industry',  commonCtrl.addIndustry);//D
commonRoutes.post('/verify-token', checkAuth, commonCtrl.verifyToken);
commonRoutes.post('/update-slots-byId', commonCtrl.updateSlots);//d
commonRoutes.post('/get-slots-byId', commonCtrl.getSlots);//D
commonRoutes.post('/get-all-slots', commonCtrl.getAllSlots);//d
commonRoutes.post('/add-all-slots', commonCtrl.addAllSlots);//D
commonRoutes.post('/verify-otp', commonCtrl.verifyOtp);//d
commonRoutes.post('/send-otp', commonCtrl.sendOtp);//d
commonRoutes.post('/get-cities', commonCtrl.getCity);//D
commonRoutes.post('/get-cities-distinct', commonCtrl.getCityDistinct);//D
commonRoutes.post('/get-countries', commonCtrl.getCountry);//d
commonRoutes.post('/get-language', commonCtrl.getLanguage);//D
commonRoutes.post('/change-password', commonCtrl.changePassword);//d
commonRoutes.post('/signin', commonCtrl.signin);//d
commonRoutes.post('/check', commonCtrl.checkBeforeSignin);
commonRoutes.post('/send-link', commonCtrl.sendConfirmationLink);//d
commonRoutes.get('/verify-mail', commonCtrl.saveEmailVerificationTocken);//d
commonRoutes.post('/resend-link', commonCtrl.resendConfirmationLink);//d
commonRoutes.get('/reverify-mail', commonCtrl.revarify);//d

//===============================================================
// Done by Ankur
commonRoutes.post('/get-payment-breakDown-static-value', commonCtrl.getPaymentBreakDownStaticValue);//d
commonRoutes.post('/set-payment-breakDown-static-value', commonCtrl.setPaymentBreakDownStaticValue);//d
commonRoutes.post('/delete-role',  commonCtrl.deleteRole);//D
commonRoutes.post('/fetch-admin-details',  commonCtrl.fetchAdminDetails);
commonRoutes.post('/authenticate-admin',  commonCtrl.authenticateAdmin);
commonRoutes.post('/update-admin-details',  commonCtrl.updateAdminDetails);

//===============================================================


module.exports = commonRoutes;