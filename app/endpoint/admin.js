
const adminCtrl = require('../controller/admin');
const express =  require('express');
const adminRoutes =  express.Router();
/**************************************************************
 * Employer job post  Related route
***************************************************************/
adminRoutes.post('/upload-file',  adminCtrl.uploadImage);
adminRoutes.post('/get-commission',  adminCtrl.getCommission);
adminRoutes.post('/set-commission',  adminCtrl.setCommision);
adminRoutes.post('/get-emp-details',  adminCtrl.getEmployerDetails);
adminRoutes.post('/get-Jobs',  adminCtrl.getJobs);
adminRoutes.post('/edit-industry',  adminCtrl.getIndustryById);
adminRoutes.post('/update-industry',  adminCtrl.editIndustry);
adminRoutes.post('/update-role', adminCtrl.updateRole);
adminRoutes.post('/get-roleBy-id', adminCtrl.getroleById);
adminRoutes.post('/change-skill-status', adminCtrl.changeSkillStatus);
adminRoutes.post('/update-skill', adminCtrl.updateSkill);
adminRoutes.post('/get-skill-byId', adminCtrl.getSkillById);
adminRoutes.post('/get-skill', adminCtrl.getSkill);
adminRoutes.post('/get-faq-byId', adminCtrl.editFaqById);
adminRoutes.post('/edit-faq', adminCtrl.editFaq);
adminRoutes.post('/get-faq', adminCtrl.getFaq);
adminRoutes.post('/add-faq', adminCtrl.addFaq);
adminRoutes.post('/get-rolewise-skill', adminCtrl.getRoleSkill);
adminRoutes.post('/add-rolewise-skill', adminCtrl.addRoleSkill);
adminRoutes.post('/add-skill', adminCtrl.addSkill);
adminRoutes.post('/add-role', adminCtrl.addJobRole);

adminRoutes.post('/get-emp', adminCtrl.getEmpDetails);
adminRoutes.post('/get-emp-by-name', adminCtrl.getEmpDetailsByName);
adminRoutes.post('/get-emp-new', adminCtrl.getEmpDetailsNew);

adminRoutes.post('/get-unapproved-emp', adminCtrl.getUnapprovedEmpDetails);
adminRoutes.post('/get-unapproved-emp-by-name', adminCtrl.getUnapprovedEmpDetailsByName);
adminRoutes.post('/get-unapproved-emp-new', adminCtrl.getUnapprovedEmpDetailsNew);

adminRoutes.post('/emp-approval', adminCtrl.setEmpApproval);

adminRoutes.post('/get-candidate', adminCtrl.getCandidateDetails);
adminRoutes.post('/get-candidate-new', adminCtrl.getCandidateDetailsNew);
adminRoutes.post('/get-candidate-by-name', adminCtrl.getCandidateDetailsByName);

adminRoutes.post('/candidate-approval', adminCtrl.setCandidateApproval);

adminRoutes.post('/get-unapproved-candidate', adminCtrl.getUnapprovedCandidateDetails);
adminRoutes.post('/get-unapproved-candidate-by-name', adminCtrl.getUnapprovedCandidateDetailsByName);
adminRoutes.post('/get-unapproved-candidate-new', adminCtrl.getUnapprovedCandidateDetailsNew);

adminRoutes.post('/give-payment', adminCtrl.givePayment);
adminRoutes.post('/get-unverified-candidate', adminCtrl.getUnverifiedCandidate);
adminRoutes.post('/get-unverified-emp', adminCtrl.getUnverifiedEmployer);

adminRoutes.post('/delete-skill-by-id', adminCtrl.deleteSkillById);
adminRoutes.post('/delete-industry', adminCtrl.deleteIndustry);
adminRoutes.post('/get-distinct-skill-on-role-basis', adminCtrl.getDistinctSkillOnRoleBasis);

//=================
// made by ankur on 18-01-20
adminRoutes.post('/fetch-rolewise-skill', adminCtrl.fetchRolewiseSkill);
//============================


module.exports = adminRoutes;