
const env = process.env.NODE_ENV || 'development';



// Application configuration - Confidential data to be here.

/**
 *  env =  development
 */

let development =  {
    app:{
        'SERVER_URL': 'https://api.letery.com',
        'PORT': '3000',
        'frontendUri': 'https://staging.letery.com',
        // 'dbURI': 'mongodb://aotstaging:rA)qN5ft%26C@34.254.105.101:27017/aotstaging',
        'dbURI': 'mongodb://accountUser_letery_usr:988_mL3%2FDb5%7B%2AEY%26%3F4Reu@52.17.123.220:27017/aotstaging',
        'FORMATTED_URL': 'https://staging.letery.com/loading',
        'FORMATTED_URL_NEW': 'https://api.letery.com'
        //'FORMATTED_URL' : 'https://localhost:3132/api'
    }
  }
let localhost =  {
    app:{
        'SERVER_URL': 'http://localhost',
        'PORT': '3132',
        'frontendUri': 'http://localhost:4200',
        'dbURI': 'mongodb://localhost:27017/aotstaging',
        //'dbURI': 'mongodb://aotstaging:rA)qN5ft%26C@52.17.123.220:27017/aotstaging',
        // 'dbURI': 'mongodb://aotstaging:rA)qN5ft%26C@34.254.105.101:27017/aotstaging',
        // 'FORMATTED_URL': 'https://letery.com/loading'
        'FORMATTED_URL' : 'https://localhost:3132/loading'
    }
  
}


/**
 *  env =  test
 */

let test =  {
    app:{
        'PORT': '<test port>',
        'dbURI': '<test dbURI>',
    }
  
}

const config = {
    development,
    localhost,
    test
};

module.exports = config[env]