const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');

const offsiteTracking = require('../model/offsiteTracking');
const employer = require('../model/employer');

const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const sendsmscontroller = require('../service/TwilioSms');
const simplemailercontroller = require('../service/mailer');
var http = require('http');
const config =  require('../config/env');
const fcm = require('../service/fcm');


//==============================================================================================

// This function is used send a notification to the employer
exports.updateNotificationForEmployer = (attribute) => {
	let employerId = attribute.employerId;
	let notificationMsg = attribute.notificationMsg;

	let currentRimeStamp = new Date().getTime();
	let notification = {
		"generatedAt" : currentRimeStamp,
		"notificationMsg" : notificationMsg
	};
	
    return new Promise(function(resolve, reject){
		models.employer.updateOne(
			{"employerId":employerId},
			{ "$push": { 
					"notification" : notification
				}
			}, 
			{ safe: true, multi:true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{
					
					if (updatedresponse.nModified == 1) {
						resolve({   
							"isError": false,
							"message": "notification sent",
							"notification": notification
						});
					}
					else {
						resolve({   
							"isError": true,
							"message": "Error Occur"
						});
					}
				}
		}) // END models.employer.updateOne(		

	}); // END return new Promise(function(resolve, reject){	
}, // END let updateHiredJobListOfCandidate = (attribute) => {


// This function is written to fetch the detail of Employer
exports.fetchEmployerDetailsForAuthentication = async (employerId) => {
	
    return new Promise(function(resolve, reject){

		let aggrQry = [
			{
				$match: { 
					'employerId': employerId 
				},	
			}
		];	

		models.employer.aggregate(aggrQry).exec((err, response) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {

				let responseLength = response.length;

			    resolve({   
					"isError": false,
					"response": response,
					"responseLength":responseLength
				});
			}	
		});
	}) // END of  return new Promise(function(resolve, reject)  		
},	

// This function is written to fetch the detail of Employer
exports.fetchEmployerDetailsForJobCancellation = async (aggrQry) => {
	
    return new Promise(function(resolve, reject){

		models.employer.aggregate(aggrQry).exec((err, response) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {

				let responseLength = response.length;

			    resolve({   
					"isError": false,
					"response": response,
					"responseLength":responseLength
				});
			}	
		});
	}) // END of  return new Promise(function(resolve, reject)  		
},	


//==============================================================================================
// This function is written to fetch the detail of candidate
exports.fetchCandidateInfo = async (aggrQry) => {
	
    return new Promise(function(resolve, reject){

		models.candidate.aggregate(aggrQry).exec((err, response) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {

				let responseLength = response.length;

			    resolve({   
					"isError": false,
					"response": response,
					"responseLength":responseLength
				});
			}	
		});
	}) // END of  return new Promise(function(resolve, reject)  		
},	



//===============================================================================================
// This function is written to fetch email for sending mail
exports.fetchDetailsOfDeclinedJobOfCandidate = async (attribute) => {

	let { candidateId } = attribute;
	let { threeMonthAgoTimeStamp } = attribute;
	
    return new Promise(function(resolve, reject){

		let aggrQry = [
			{
				$match: { 
					'candidateId': candidateId 
				},	
			},

			{ 
				$project: {
					"_id":0,
					"declinedJobs": 1,
					"lastCancellationTime": 1,
					"rating": 1
				}
			},

			{ $unwind: "$declinedJobs" },

			{
				$match: { 
					'declinedJobs.declinedTimeStamp': { $gte: threeMonthAgoTimeStamp } 
				},	
			},

			{ 
				$sort : { 
					"declinedJobs.declinedTimeStamp" : -1 
				} 
			}
		];

		models.candidate.aggregate(aggrQry).exec((err, response) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {

				let responseLength = response.length;

			    resolve({   
					"isError": false,
					"response": response,
					"responseLength":responseLength
				});
			}	
		});
	}) // END of  return new Promise(function(resolve, reject)  		
},	


//===============================================================================================
// This function is written to fetch email for sending mail
exports.fetchAdminEmailForMailing = async () => {
    return new Promise(function(resolve, reject){
		models.authenticate.findOne({"type":"ADMIN"}).exec(function (err, response) {
			if (err) {
				resolve({   
					"isError": true,
				});

			} else {
				resolve({   
					"isError": false,
					"response": response
				});

			}

		});  
	}) // END of  return new Promise(function(resolve, reject)  		
},	
//===============================================================================================
// This function is written to fetch all the candidate which are employed
exports.fetchJobTimeDetail = async (jobId, roleId) => {
    return new Promise(function(resolve, reject){
		let aggrQry = [
			{
				$match: { 
					'jobId': jobId 
				},	
			},

			{ 
				$project: {
					"_id":0,
					"jobId": 1,
					"jobDetails": 1
				}
			},

			{ 
				$project: {
					"jobId": 1,
					'roleWiseAction': '$jobDetails.roleWiseAction'
				}
			},

			{ 
				$project: {
					"jobId": 1,
					'roleWiseAction': {
						$filter: {
						   input: "$roleWiseAction",
						   as: "roleWiseAction",
						   cond: { $eq: [ "$$roleWiseAction.roleId", roleId ] }
						}
					}
				}
			},

			{ 
				$project: {
					"jobId": 1,
					'roleWiseAction': { $arrayElemAt: [ "$roleWiseAction", 0 ] }
				}
			},
            
			{ 
				$project: {
					"jobStartDateTimeStamp": "$roleWiseAction.startRealDateTimeStamp",
					"jobStartDateUtc": "$roleWiseAction.startRealDateUtc",
					"jobStartDateTimeStampLocal": "$roleWiseAction.startRealDate",
					"jobEndDateTimeStamp": "$roleWiseAction.endRealDateTimeStamp",
					"jobEndDateUtc": "$roleWiseAction.endRealDateUtc",
					"jobEndDateTimeStampLocal": "$roleWiseAction.endRealDate",
				}
			}
		];

       
		models.job.aggregate(aggrQry).exec((err, response) => {            
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"response": response[0]
				});
			}	
		})

	}) // END of  return new Promise(function(resolve, reject)  	
},

//===============================================================================================
// This function is written to fetch all the candidate which are employed
exports.fetchCandidateJobSchedule = async (candidateId) => {
    return new Promise(function(resolve, reject){
		let aggrQry = [
			{
				$match: { 
					'candidateId': candidateId 
				},	
			},

			{ 
				$project: {
					_id:0,
					hiredJobs: 1
				}
			},

			{ $unwind: "$hiredJobs" },

			{ 
				$project: {
					"hiredJobs": 1,
					"jobId": "$hiredJobs.jobId",
					"roleId": "$hiredJobs.roleId"
				}
			},


			{
				$lookup: {
					from: "jobs",
					localField: "jobId",
					foreignField: "jobId",
					as: "jobDetails"
				}
			},
            
			{ 
				$project: {
					"hiredJobs": 1,
					"jobId": 1,
					"roleId": 1,
					"jobDetails": { $arrayElemAt: [ "$jobDetails", 0 ] }
				}
			},

			{ 
				$project: {
					"hiredJobs": 1,
					"roleId": 1,
					"jobId": 1,
					"jobId2": "$jobDetails.jobId",
					'roleWiseAction': '$jobDetails.jobDetails.roleWiseAction'
				}
			},

			{ 
				$project: {
					"hiredJobs": 1,
					"jobId": 1,
					"roleId": 1,
					"jobId2": "$jobDetails.jobId",
					'roleWiseAction': {
						$filter: {
						   input: "$roleWiseAction",
						   as: "roleWiseAction",
						   cond: { $eq: [ "$$roleWiseAction.roleId", "$roleId" ] }
						}
					 }
				}
			},

			{ 
				$project: {
					"hiredJobs": 1,
					"jobId": 1,
					"roleId": 1,
					"jobId2": "$jobDetails.jobId",
					'roleWiseAction': { $arrayElemAt: [ "$roleWiseAction", 0 ] },
				}
			},

			{ 
				$project: {
					"hiredJobs": 1,
					"jobId": 1,
					"roleId": 1,
					"jobId2": "$jobDetails.jobId",
					"roleWiseAction": 1,
					"jobStartDateTimeStamp":"$roleWiseAction.startRealDateTimeStamp",
					"jobStartDateUtc": "$roleWiseAction.startRealDateUtc",
					"jobStartDateTimeStampLocal": "$roleWiseAction.startRealDate",
					"jobEndDateTimeStamp": "$roleWiseAction.endRealDateTimeStamp",
					"jobEndDateUtc": "$roleWiseAction.endRealDateUtc",
					"jobEndDateTimeStampLocal": "$roleWiseAction.endRealDate",
				}
			},

			{ 
				$project: {
					"hiredJobs": 1,
					"jobId": 1,
					"roleId": 1,
					"jobId2": "$jobDetails.jobId",
					"jobStartDateTimeStamp":1,
					"jobStartDateUtc": 1,
					"jobStartDateTimeStampLocal": 1,
					"jobEndDateTimeStamp": 1,
					"jobEndDateUtc": 1,
					"jobEndDateTimeStampLocal":1,
				}
			}

		];	


		models.candidate.aggregate(aggrQry).exec((err, response) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"response": response
				});
			}	
		});



	}) // END of  return new Promise(function(resolve, reject)  	
},



//===============================================================================================
// This function is written to fetch all the candidate which are employed
exports.fetchEmployedCandidateforEmployee = async (employerId, page, perPage) => {
    return new Promise(function(resolve, reject){

		let aggrQry = [
			{
				$match: { 
					'employerId': employerId 
				},	
			},

			{ $unwind: "$hireList" },
			{ 
				$project: {
					hireList: 1,
					jobId: 1,
					employerId: 1,
					industry:"$jobDetails.industry",
					jobDb:"$jobDetails"
				}
			},
           
			{  
				$project: {
						hireList: 1,
						candidateId: "$hireList.candidateId",
						jobId: 1,
						employerId: 1,
						industryId:1,
						jobCollection: {$filter: {
							input: '$jobDb.roleWiseAction',
							as: 'jobCollection',
							cond: {$eq: ['$$jobCollection.roleId', '$hireList.roleId']}
						}
					},
					_id: 0
				}
			},

			{
				$project: {
					hireList: 1,
					candidateId: 1,
					jobId: 1,
					employerId: 1,
					industryId:1,
					jobCollection:{ $arrayElemAt :["$jobCollection", 0]}
				}
			},

			{
				$project: {
					hireList: 1,
					candidateId: 1,
					jobId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName: "$jobCollection.locationName",
					jobCollection:1
				}
			},
            
			{
				$lookup: {
					from: "candidates",
					localField: "hireList.candidateId",
					foreignField: "candidateId",
					as: "candidateDetails"
				}
			},
            
			{  
				$project: {
					hireList: 1,
					candidateId: 1,
					jobId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName:1,
					
					roleName:"$jobCollection.roleName",
					paymentStatus:"$jobCollection.paymentStatus",
					paymentStatus:"$jobCollection.paymentStatus",
					description:"$jobCollection.description",
					candidateDetails:{ $arrayElemAt :["$candidateDetails", 0]}
				}
			},
            
			{
				$project: {
					hireList: 1,
					jobId: 1,
					candidateId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName:1,
					roleName:1,
					paymentStatus:1,
					paymentStatus:1,
					description:1,
					geolocation:"$candidateDetails.geolocation",
					rating: "$candidateDetails.rating",
					fname: "$candidateDetails.fname",
					mname: "$candidateDetails.mname",
					lname: "$candidateDetails.lname",
					profilePic: "$candidateDetails.profilePic",
				
					isFavourite:{
						$cond:[{$setIsSubset: [["$employerId"], "$candidateDetails.favouriteBy"]},true,false]
					}
					
				}
			},
            
			{
				$lookup:
				{
					from: "calenderdatas",
					localField: "jobId",
					foreignField: "jobId",
					as: "calenderDataDetails"
				}
			},

			{
				$project: {
					hireList: 1,
					jobId: 1,
					candidateId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName:1,
					roleName:1,
					paymentStatus:1,
					paymentStatus:1,
					description:1,
					geolocation: 1,
					rating: 1,
					fname: 1,
					mname: 1,
					lname: 1,
					profilePic: 1,	
					isFavourite: 1,
					calenderDataDetails: {
						$filter: {
							input: "$calenderDataDetails",
							as: "calenderDataDetails",
							cond: { $eq: [ "$$calenderDataDetails.candidateId", "$candidateId" ] }
						}
					}
					
				}
			},

			{
				$project: {
					hireList: 1,
					jobId: 1,
					candidateId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName:1,
					roleName:1,
					paymentStatus:1,
					paymentStatus:1,
					description:1,
					geolocation: 1,
					rating: 1,
					fname: 1,
					mname: 1,
					lname: 1,
					profilePic: 1,	
					isFavourite: 1,
					calenderDataDetailsfirst: { $arrayElemAt: [ "$calenderDataDetails", 0 ] },
					calenderDataDetailsId: '$calenderDataDetailsfirst._id',
					
				}
			},

			{
				$project: {
					hireList: 1,
					jobId: 1,
					candidateId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName:1,
					roleName:1,
					paymentStatus:1,
					paymentStatus:1,
					description:1,
					geolocation: 1,
					rating: 1,
					fname: 1,
					mname: 1,
					lname: 1,
					profilePic: 1,	
					isFavourite: 1,
					calenderDataDetailsId: '$calenderDataDetailsfirst._id',
					
				}
			},

			{
				$project: {
					hireList: 1,
					jobId: 1,
					candidateId: 1,
					employerId: 1,
					industryId:1,
					JoblocationName:1,
					roleName:1,
					paymentStatus:1,
					paymentStatus:1,
					description:1,
					geolocation: 1,
					rating: 1,
					fname: 1,
					mname: 1,
					lname: 1,
					profilePic: 1,	
					isFavourite: 1,
					calenderDataDetailsId: 1,
					
				}
			},

			{
				$match: {
					'calenderDataDetailsId' : { '$exists' : true }
				}
			},


			{
				$group: {
					_id: null,
					total: {
					$sum: 1
					},
					results: {
					$push : '$$ROOT'
					}
				}
			},
			{
				$project : {
					'_id': 0,
					'total': 1,
					'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
					'results': {
						$slice: [
							'$results', page * perPage , perPage
						]
					}
				}
			}
		
			

		];	


		models.job.aggregate(aggrQry).exec((err, data) => {            
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"candidateList": data[0]
				});
			}	
		})
	
	}) // END of  return new Promise(function(resolve, reject)  	
},

//===============================================================================================


// This function is written to fetch all the skills of a candidate
exports.fetchJobOfCandidate = async (candidateId, currentTimestampFromApi = 0) => {
	return new Promise(function(resolve, reject){

		let pageNo = 0;
		let perPage = 20;
		let currentTime = Math.floor(Date.now());
        
		let aggrQry = [
			{
				$match: {
					'candidateId': candidateId
				}
			},
			
			{
				$project: {
					'_id': 1,
					'candidateId': 1,
					// 'selectedJobs1': '$hiredJobs',
					'selectedJobs': {
						$filter: {
						   input: "$hiredJobs",
						   as: "hiredJobs",
						   cond: { $lte: [ "$$hiredJobs.endRealDateTimeStamp", currentTimestampFromApi ] }
						}
					 }
				}
			},
			
            
			{ $unwind : "$selectedJobs" },

			{
				$project: {
					'jobId': '$selectedJobs.jobId',
					'roleId': '$selectedJobs.roleId',
					'currentTimestamp': '$selectedJobs.currentTimestamp',
					'currentDate': '$selectedJobs.currentDate',
					'endRealDate': '$selectedJobs.endRealDate',
					'endRealDateTimeStamp': '$selectedJobs.endRealDateTimeStamp',
				}
			},
		   
			{
				$lookup:
				{
					from: "jobs",
					localField: "jobId",
					foreignField: "jobId",
					as: "jobDetails"
				}
			},
            
			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'employerId': '$jobDetails.employerId',
					'roleWiseAction': '$jobDetails.jobDetails.roleWiseAction'			
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'employerId': 1,
					'roleWiseAction': { $arrayElemAt: [ '$roleWiseAction', 0 ] }			
				}
			},


			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'employerId': { $arrayElemAt: [ '$employerId', 0 ] },
					'roleWiseAction': {
						$filter: {
						   input: "$roleWiseAction",
						   as: "roleWiseAction",
						   cond: { $eq: [ "$$roleWiseAction.roleId", "$roleId" ] }
						}
					 }			
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'employerId': 1,
					'roleWiseAction': { $arrayElemAt: [ '$roleWiseAction', 0 ] },		
				}
			},
			
		
			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction': 1,	
					"startRealDate": "$roleWiseAction.startRealDate",
					"startRealDateUtc": "$roleWiseAction.startRealDateUtc",
					"startRealDateTimeStamp": "$roleWiseAction.startRealDateTimeStamp",
					"endRealDate": "$roleWiseAction.endRealDate",
					"endRealDateUtc": "$roleWiseAction.endRealDateUtc",
					"endRealDateTimeStamp": "$roleWiseAction.endRealDateTimeStamp"
				}
			},
            
			{
				$lookup:
				{
					from: "employers",
					localField: "employerId",
					foreignField: "employerId",
					as: "employerDetails"
				}
			},
            
			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': '$employerDetails.companyName'
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': { $arrayElemAt: [ "$companyName", 0 ] }	
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': 1	
				}
			},
            { 
				$lookup:
					{
						from: "ratings",
						let: { 
							candidate_roleId: "$roleId", 
							candidate_employerId: "$employerId",
							candidate_jobId: "$jobId"  
						},
						pipeline: [
							{ $match:
								{ $expr:
									{ $and:
									[
										{ $eq: [ "$roleId",  "$$candidate_roleId" ] },
										{ $eq: [ "$employerId", "$$candidate_employerId" ] },
										{ $eq: [ "$jobId", "$$candidate_jobId" ] }
									]
									}
								}
							},
							{ $project: { _id: 0, rating: 1 } }
						],
						as: "ratingDetails"
					}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': 1,
					'ratingDetails': { $arrayElemAt: [ "$ratingDetails", 0 ] }		
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': 1,
					'rating': '$ratingDetails.rating'	
				}
			},
			
			{ $sort : { endRealDateTimeStamp : -1 } },
            
			{
				$group: {
					_id: null,
					total: {
					$sum: 1
					},
					results: {
					$push : '$$ROOT'
					}
				}
			},
	 
			{
				$project : {
					'_id': 0,
					'total': 1,
					'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
					'results': {
						$slice: [
							'$results', pageNo * perPage , perPage
						]
					}
				}
			},
            
			
			
		];  
		
		models.candidate.aggregate(aggrQry).exec((err, result) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"hiredJobDetails": result
				});
			}	
		});


	}) // END of  return new Promise(function(resolve, reject)  	
},
// This function is written to fetch all the skills of a candidate (Not in use)
exports.fetchJobOfCandidateBKP = async (candidateId, currentTimestampFromApi = 0) => {
	return new Promise(function(resolve, reject){

		let pageNo = 0;
		let perPage = 20;
		let currentTime = Math.floor(Date.now());
        
		let aggrQry = [
			{
				$match: {
					'candidateId': candidateId
				}
			},
			
			{
				$project: {
					'_id': 1,
					'candidateId': 1,
					'selectedJobs': '$hiredJobs'
				}
			},
            
			{ $unwind : "$selectedJobs" },
            
			{
				$project: {
					'jobId': '$selectedJobs.jobId',
					'roleId': '$selectedJobs.roleId',
					'currentTimestamp': '$selectedJobs.currentTimestamp',
            		'currentDate': '$selectedJobs.currentDate'
				}
			},
			
			{
				$lookup:
				{
					from: "calenderdatas",
					localField: "jobId",
					foreignField: "jobId",
					as: "calenderDataDetails"
				}
			},
            
			{
				$project: {
					'_id': 1,
					'candidateId': 1,
					'jobId': 1,
					'roleId': 1,
					// 'currentTimestamp': 1,
					// 'currentDate': 1,
					'calenderDataDetails': {
						$filter: {
							input: "$calenderDataDetails",
							as: "calenderDataDetails",
							cond: { $eq: [ "$$calenderDataDetails.candidateId", candidateId ] }
						}
					}
				}
			},
		    

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'candidateId':1,
					// 'currentTimestamp': 1,
					// 'currentDate': 1,
					'calenderDataDetailsfirst': { $arrayElemAt: [ "$calenderDataDetails", 0 ] },
					'calenderDataDetailsId': '$calenderDataDetailsfirst._id',
				}
			},
     
			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': '$calenderDataDetailsfirst._id',
					
				}
			},

			{
				$match: {
					'calenderDataDetailsId' : { '$exists' : true }
				}
			},

			{
				$lookup:
				{
					from: "jobs",
					localField: "jobId",
					foreignField: "jobId",
					as: "jobDetails"
				}
			},
            
			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					// 'currentTimestamp': 1,
					// 'currentDate': 1,
					'calenderDataDetailsId': 1,
					'employerId':'$jobDetails.employerId',
					// 'jobStartDateTimeStamp': '$jobDetails.jobStartDateTimeStamp',
        			// 'jobStartDateUtc': '$jobDetails.jobStartDateUtc',
        			// 'jobStartDateTimeStampLocal':'$jobDetails.jobStartDateTimeStampLocal',
					// 'jobEndDateTimeStamp': '$jobDetails.jobEndDateTimeStamp',
        			// 'jobEndDateUtc': '$jobDetails.jobEndDateUtc',
        			// 'jobEndDateTimeStampLocal':'$jobDetails.jobEndDateTimeStampLocal',
					'roleWiseAction': '$jobDetails.jobDetails.roleWiseAction'			
				}
			},
			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId':1,
					'roleWiseAction': { $arrayElemAt: [ "$roleWiseAction", 0 ] }			
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId':1,
					// 'roleWiseAction': { $arrayElemAt: [ "$roleWiseAction", 0 ] },
					'roleWiseAction': {
						$filter: {
						   input: "$roleWiseAction",
						   as: "roleWiseAction",
						   cond: { $eq: [ "$$roleWiseAction.roleId", "$roleId" ] }
						}
					 }		
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': { $arrayElemAt: [ "$employerId", 0 ] },
					'roleWiseAction': { $arrayElemAt: [ "$roleWiseAction", 0 ] },	
					"startRealDate": "$roleWiseAction.startRealDate",
					"startRealDateUtc": "$roleWiseAction.startRealDateUtc",
					"startRealDateTimeStamp": "$roleWiseAction.startRealDateTimeStamp",
					"endRealDate": "$roleWiseAction.endRealDate",
					"endRealDateUtc": "$roleWiseAction.endRealDateUtc",
					"endRealDateTimeStamp": "$roleWiseAction.endRealDateTimeStamp"
				}
			},

			// {
			// 	$match: {
			// 		'employerId': employerId
			// 	}
			// },


			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': '$roleWiseAction.roleName',
					'locationName': '$roleWiseAction.locationName',	
					"startRealDate": { $arrayElemAt: [ "$startRealDate", 0 ] },	
					"startRealDateUtc": { $arrayElemAt: [ "$startRealDateUtc", 0 ] },	
					"startRealDateTimeStamp":{ $arrayElemAt: [ "$startRealDateTimeStamp", 0 ] },	
					"endRealDate": { $arrayElemAt: [ "$endRealDate", 0 ] },	
					"endRealDateUtc": { $arrayElemAt: [ "$endRealDateUtc", 0 ] },	
					"endRealDateTimeStamp": { $arrayElemAt: [ "$endRealDateTimeStamp", 0 ] }
				}
			},

			{
				$match: {
					'endRealDateTimeStamp': { $lte: currentTimestampFromApi }
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1
				}
			},

			{
				$lookup:
				{
					from: "employers",
					localField: "employerId",
					foreignField: "employerId",
					as: "employerDetails"
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': '$employerDetails.companyName'
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': { $arrayElemAt: [ "$companyName", 0 ] }	
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': 1	
				}
			},
            { 
				$lookup:
					{
						from: "ratings",
						let: { 
							candidate_roleId: "$roleId", 
							candidate_employerId: "$employerId",
							candidate_jobId: "$jobId"  
						},
						pipeline: [
							{ $match:
								{ $expr:
									{ $and:
									[
										{ $eq: [ "$roleId",  "$$candidate_roleId" ] },
										{ $eq: [ "$employerId", "$$candidate_employerId" ] },
										{ $eq: [ "$jobId", "$$candidate_jobId" ] }
									]
									}
								}
							},
							{ $project: { _id: 0, rating: 1 } }
						],
						as: "ratingDetails"
					}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': 1,
					'ratingDetails': { $arrayElemAt: [ "$ratingDetails", 0 ] }		
				}
			},

			{
				$project: {
					'jobId': 1,
					'roleId': 1,
					'calenderDataDetailsId': 1,
					'employerId': 1,
					'roleWiseAction':1,	
					'roleName': 1,
					'locationName': 1,
					"startRealDate": 1,	
					"startRealDateUtc": 1,	
					"startRealDateTimeStamp":1,	
					"endRealDate": 1,	
					"endRealDateUtc": 1,	
					"endRealDateTimeStamp": 1,
					'companyName': 1,
					'rating': '$ratingDetails.rating'	
				}
			},
			
			// { $sort : { currentTimestamp : -1 } },
            
			{
				$group: {
					_id: null,
					total: {
					$sum: 1
					},
					results: {
					$push : '$$ROOT'
					}
				}
			},
	 
			{
				$project : {
					'_id': 0,
					'total': 1,
					'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
					'results': {
						$slice: [
							'$results', pageNo * perPage , perPage
						]
					}
				}
			},

			
			
		];  
		
		models.candidate.aggregate(aggrQry).exec((err, result) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"hiredJobDetails": result
				});
			}	
		});


	}) // END of  return new Promise(function(resolve, reject)  	
},



//===============================================================================================
// This function is written to fetch all the skills of a candidate
exports.fetchSelectedSkillOfCandidate = async (candidateId) => {
	return new Promise(function(resolve, reject){
		let aggrQry = [
			{
				$match: {
					'candidateId': candidateId
				}
			},
			{
				$project: {
					'_id': 0,
					'payloadSkill': 1
				}
			},
		];  
		
		models.candidate.aggregate(aggrQry).exec((err, result) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"candidateDetails": result
				});
			}	
		});


	}) // END of  return new Promise(function(resolve, reject)  	
},



//===============================================================================================
// This function is written to fetch employer details
exports.fetchJob = async (jobId) => {
	return new Promise(function(resolve, reject){
		let aggrQry = [
			{
				$match: {
					'jobId': jobId
				}
			},
			{
				$project: {
					'_id': 0,
					'jobDetails': 1,
					'employerId': 1
				}
			},
		];  
		
		models.job.aggregate(aggrQry).exec((err, result) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"jobDetails": result
				});
			}	
		});


	}) // END of  return new Promise(function(resolve, reject)  	
},

//===============================================================================================
// This function is written to fetch employer details
exports.fetchEmployer = async (employerId) => {
	return new Promise(function(resolve, reject){
		let aggrQuery = [
			{
				$match: {
					'employerId': employerId
				}
			},
			{
				$project: {
					'_id': 0,
					'companyName': 1
				}
			},
		];  
		
		employer.aggregate(aggrQuery, 
			function (err, response) {	

				if(err){
					resolve({   
						"isError": true,
					});

				} else {
					if (response.length < 1) {
						resolve({   
							"isError": false,
							"isExist": false
						});
					} else {
						resolve({   
							"isError": false,
							"isExist": true,
							"response": response
						});
					}	
				}	
		});


	}) // END of  return new Promise(function(resolve, reject)  	
},
//================================================================================================

// This function is used to fetch the details of shift
exports.fetchShiftDetailsForCandidate = async (candidateId,jobId,roleId,date) => {
    return new Promise(function(resolve, reject){
		let findWhere={
			"candidateId":candidateId,
			"jobId": jobId,
			"roleId": roleId,
			"workingDays.date":date,
		}
		models.CalenderData.findOne(findWhere,{ 'workingDays.$': 1 }, 
		    function (err, item) {
				if (err) {
					resolve({   
						"isError": true
					});
				} else {
					resolve({   
						"isError": false,
						"response": item
					});
				}
		});
	}) // END of  return new Promise(function(resolve, reject)  	
},
// this function is written to fetch the pushNotification status of candidate 
exports.fetchOfficeTrackinDetails = async (candidateId,jobId,roleId,employerId,date) => {
	return new Promise(function(resolve, reject){

		let aggrQuery = [
			{
				$match: {
					$and: [
						{'candidateId': candidateId},
						{'jobId': jobId},
						{'roleId': roleId},
						{'employerId': employerId},
						{'date': date}
					]		
				}
			},
			{
				$project: {
					'_id': 0,
					'offsiteTrackingId':1,
					'OffsiteData': 1
				}
			},

		];    
       
		offsiteTracking.aggregate(aggrQuery, 
			function (err, response) {	

				if(err){
					resolve({   
						"isError": true,
					});

				} else {
					if (response.length < 1) {
						resolve({   
							"isError": false,
							"isExist": false
						});
					} else {
						resolve({   
							"isError": false,
							"isExist": true,
							"response": response
						});
					}	
				}	
		});
	    
	}) // END of  return new Promise(function(resolve, reject)  
}

// this function is written to fetch the pushNotification status of candidate 
exports.fetchPushNotification = async (candidateId) => {
	return new Promise(function(resolve, reject){

		let aggrQuery = [
			{
				$match: {
					'candidateId': candidateId
				}
			},
			{
				$project: {
					'_id': 0,
					'isPushNotificationStatus': 1
				}
			},

		];    

		models.candidate.aggregate(aggrQuery, 
			function (err, response) {	

				if(err){
					resolve({   
						"isError": true,
					});

				} else {
					if (response.length < 1) {
						resolve({   
							"isError": false,
							"isExist": false
						});
					} else {
						resolve({   
							"isError": false,
							"isExist": true,
							"pushNotification": response[0].isPushNotificationStatus
						});
					}	
				}	
		});
	    
	}) // END of  return new Promise(function(resolve, reject)  
}

// this function is written to fetch the details of job
exports.fetchJobInfo = async (aggrQry) => {
	return new Promise(function(resolve, reject){

		models.job.aggregate(aggrQry).exec((err, result) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {
			    resolve({   
					"isError": false,
					"jobDetails": result
				});
			}	
		});
	    
	}) // END of  return new Promise(function(resolve, reject)  
}

//================================================================================================



exports.offerJob = async (candidateId, employerId, roleId, jobId) => {
    console.log("candidateId==>"+candidateId);

    let updateWith={
        'roleId': roleId,
        'candidateId': candidateId
    }

    let insertedValue={
        "roleId": roleId,
        "employerId": employerId,
        "jobId": jobId,
        "candidateId": candidateId
    }  
	
	return new Promise(function (resolve, reject) {
        // console.log("candidateId==>"+candidateId);
        models.job.updateOne(
        	{ 
        		'employerId': employerId,
        	    'jobId': jobId 
        	},
        	{ 
        		$push: {'approvedTo':updateWith}
        	},  
            function (error, response) {
               console.log("jobId==>"+jobId);
                if(error){

                    resolve({
		               "isError": true,
		            });
                } else {

                	models.candidate.updateOne(
	            		{ 'candidateId': candidateId }, 
	            		{ $push: { "jobOffers":insertedValue } },  
	                    function (updatederror, updatedresponse) {

	                    	if (updatederror) {
	                    		resolve({
					               "isError": true,
					            });
	                    	} else {
	                    		resolve({
					               "isError": false,
					            });
	                    	}
	            
	                }) // END models.candidate.updateOne(	

                } // if(error){
              
        }) // End models.job.updateOne(    	
		

	}) // END Promise	
}

//=======================================================================================================

exports.findMail = async (candidateId, employerId, roleId, jobId) => {
    console.log("candidateId==>"+employerId);
	
	return new Promise(function (resolve, reject) {

		models.authenticate.findOne(
			{
				'userId':candidateId
			}, 
			function (err, item) {

				if (err) {
					resolve({
		               "isError": true,
		            });
				} else {

					if (item) {
                        resolve({
			               "isError": false,
			               "isItemExist": true,
			               "item" : item
			            });
					} else {
						resolve({
			               "isError": false,
			               "isItemExist": false,
			               "item" : null
			            });
					}

				}
       
        })
	}) // END Promise	
}

