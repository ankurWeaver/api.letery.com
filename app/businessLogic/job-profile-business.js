const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');

const offsiteTracking = require('../model/offsiteTracking');
const employer = require('../model/employer');

const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const sendsmscontroller = require('../service/TwilioSms');
const simplemailercontroller = require('../service/mailer');
var http = require('http');
const config =  require('../config/env');
const fcm = require('../service/fcm');
const { AvailablePhoneNumberCountryContext } = require('twilio/lib/rest/api/v2010/account/availablePhoneNumber');


// This function is used to cancel a job for particular candidate
let cancelJobForParticularCandidateInHirelist = (attribute) => {
	let candidateId = attribute.candidateId.trim();
	let roleId = attribute.roleId.trim();
    let jobId = attribute.jobId.trim();
    let cancelledBy = attribute.cancelledBy.trim();


	let hireList = {
		"roleId" : roleId,
		"candidateId" : candidateId
    };
    
    let currentDate = new Date();
    let currentTimestamp = currentDate.getTime();
    
	let cancelledJobs = {
		"roleId" : roleId,
		"jobId" : jobId,
		"candidateId" : candidateId,
		"currentDate": currentDate,
		"currentTimestamp": currentTimestamp,
		"cancelledBy": cancelledBy
	};
	

    return new Promise(function(resolve, reject){
		models.job.updateOne(
			{"jobId":jobId},
			{ 
                "$pull": { 
					"hireList" : hireList
                },
                "$push": { 
					"cancelledJobs" : cancelledJobs
                },
                
			}, 
			{ safe: true, multi:true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{
					
					if (updatedresponse.nModified == 1) {
						resolve({   
							"isError": false,
							"message": "successfully cancelled"
						});
					}
					else {
						resolve({   
							"isError": true,
							"message": "Either the candidate cancelled the job previously or some error occur"
						});
					}
				}
		}) // END models.job.updateOne(		
	}); // END return new Promise(function(resolve, reject){	  
} // END let cancelJobForParticularCandidateInHirelist = (attribute) => {


// This function is used to check whether the candidate is hired for a particular job or not
let checkCandidateIsHiredOrNot = (attribute) => {
    let isHired = false;
    let hireList = attribute.hireList ? attribute.hireList: [];
    let candidateId = attribute.candidateId ? attribute.candidateId: '';
    let roleId = attribute.roleId ? attribute.roleId: '';

    var i;
    for (i = 0; i < hireList.length; i++) {
        if ((hireList[i].roleId == roleId) && (hireList[i].candidateId == candidateId)) {
            isHired = true;
            break;
        } // END if (hireList[i].roleId == roleId) {
    } // END for (i = 0; i < hireList.length; i++) {

    return isHired;
}

// This function is used to get the converted date according to server
let getDifferenceBetweenTimeZone = (attribute) => {
    localTimeZone = attribute.localTimeZone;
    
    currentDate = new Date(); 
    let currentDateTimeStamp =  new Date(currentDate).getTime(); 

    // converting the current Time to the time on the given time zone 
    convertedDate = new Date(currentDate).toLocaleString("en-US", { timeZone: localTimeZone });
    convertedDateUtc = new Date(convertedDate);
    let convertedDateTimeStamp =  new Date(convertedDateUtc).getTime(); 

    // getting the difference between the time of server and the time on the given time zone
    let diffStamp = Number(currentDateTimeStamp) - Number(convertedDateTimeStamp);
   
    diffStamp = diffStamp / 10000;
    diffStamp = Math.floor(diffStamp) * 10000;

    differenceInSec = diffStamp / 1000; 
    differenceInMin = differenceInSec / 60;
    differenceInHrs = differenceInMin / 60;

    let diffArr = {
        "diffInTimeStamp": diffStamp,
        "differenceInSec": differenceInSec,
        "differenceInMin": differenceInMin,
        "differenceInHrs": differenceInHrs
    }
    
    return diffArr;
}





let getTimeAndDate = (date, time) => {
   
    if (Number(time) > 12) {
        timeLocal = time.toString();
        timeLocalArr  = timeLocal.split(".");
        timeLocalArr[0] = Number(timeLocalArr[0])-12;
        timeLocal = timeLocalArr[0]+":"+timeLocalArr[1]+":00 PM"
    } else {
        timeLocal = time.toString();
        timeLocalArr  = timeLocal.split(".");
        timeLocal = timeLocalArr[0]+":"+timeLocalArr[1]+":00 AM"
    }

    let realDate = date+" "+timeLocal;

    let realDateUtc = new Date(realDate);
    let realDateTimeStamp =  new Date(realDateUtc).getTime(); 
    let dateObj = {
        "realDate": realDate,
        "realDateUtc": realDateUtc,
        "realDateTimeStamp": realDateTimeStamp
    }

    return dateObj;
}

let getTimeAndDateWithTimeZone = (date, time, localTimeZone) => {
   
    if (Number(time) > 12) {
        timeLocal = time.toString();
        timeLocalArr  = timeLocal.split(".");
        timeLocalArr[0] = Number(timeLocalArr[0])-12;
        timeLocal = timeLocalArr[0]+":"+timeLocalArr[1]+":00 PM"
    } else {
        timeLocal = time.toString();
        timeLocalArr  = timeLocal.split(".");
        timeLocal = timeLocalArr[0]+":"+timeLocalArr[1]+":00 AM"
    }

    let realDate = date+" "+timeLocal;

    let realDateUtc = new Date(realDate);
    let realDateTimeStamp =  new Date(realDateUtc).getTime(); 

    let diffArr = getDifferenceBetweenTimeZone({
        "localTimeZone": localTimeZone
    })

    let tempConvertedDate = new Date(realDate).toLocaleString("en-US", { timeZone: localTimeZone });
    let tempConvertedDateTimeStamp = new Date(tempConvertedDate).getTime();

    let diffInTimeStamp = diffArr.diffInTimeStamp;
    let convertedDateTimeStamp = Number(tempConvertedDateTimeStamp) + Number(diffInTimeStamp);
    let convertedDateUtc = new Date(convertedDateTimeStamp);
    let convertedDate = new Date(convertedDateUtc).toString();;

    let dateObj = {
        "realDate": realDate,
        "realDateUtc": realDateUtc,
        "realDateTimeStamp": realDateTimeStamp,
        "convertedDateTimeStamp": convertedDateTimeStamp,
        "convertedDateUtc": convertedDateUtc,
        "convertedDate": convertedDate
    }

    return dateObj;
}

let getRealDate = (timeStamp, time, diffInTimeStamp = 0) => {
    // console.log("timeStamp1==",timeStamp)
    // console.log("diffInTimeStamp==",diffInTimeStamp)
    timeStamp = timeStamp - Number(diffInTimeStamp);
    dateUtc = new Date(timeStamp);
    dateLocal = dateUtc.toLocaleString();
    dateLocalArr  = dateLocal.split(" ");
    
    if (Number(time) > 12) {
        timeLocal = time.toString();
        timeLocalArr  = timeLocal.split(".");
        timeLocalArr[0] = Number(timeLocalArr[0])-12;
        timeLocal = timeLocalArr[0]+":"+timeLocalArr[1]+":00 PM"
    } else {
        timeLocal = time.toString();
        timeLocalArr  = timeLocal.split(".");
        timeLocal = timeLocalArr[0]+":"+timeLocalArr[1]+":00 AM"
    }

    let realDate = dateLocalArr[0]+" "+timeLocal;

    let realDateUtc = new Date(realDate);
    let realDateTimeStamp =  new Date(realDateUtc).getTime(); 

    let dateObj = {
        "realDate": realDate,
        "realDateUtc": realDateUtc,
        "realDateTimeStamp": realDateTimeStamp
    }

    return dateObj;
}

let getDateFromTimeStamp = (timeStamp) => {
    dateUtc = new Date(timeStamp);
    dateLocal = dateUtc.toLocaleString();

    let dateObj = {
        "realDate": dateLocal,
        "realDateUtc": dateUtc,
        "realDateTimeStamp": timeStamp
    }

    return dateObj;
}


let getStartAndEndTimeOfRole = (setTime, diffInTimeStamp) => {
    let i;
    let startTime = setTime[0].startTime;
    let endTime = setTime[0].endTime;
   
    let startTimeArr = [];
    let endTimeArr = [];

    let realStartRimeArr = [];
    let realEndRimeArr = [];

    for (i = 0; i < setTime.length; i++) {
    
        let realIdividualStartTime = getRealDate(setTime[i].startDate, setTime[i].startTime, diffInTimeStamp);
        realIdividualStartTimeStamp = realIdividualStartTime.realDateTimeStamp;
        realStartRimeArr.push(realIdividualStartTimeStamp)
        setTime[i].realIdividualStartTimeStamp = realIdividualStartTimeStamp;

        let realIdividualEndTime = getRealDate(setTime[i].endDate, setTime[i].endTime, diffInTimeStamp);
        realIdividualEndTimeStamp = realIdividualEndTime.realDateTimeStamp;
        realEndRimeArr.push(realIdividualEndTimeStamp)
        setTime[i].realIdividualEndTimeStamp = realIdividualEndTimeStamp;

    } // End  for (i = 0; i < setTime.length; i++) {
 
    startDateTimeStamp = Math.min(...realStartRimeArr); 
    let startTimeObj = getDateFromTimeStamp(startDateTimeStamp);
    let startRealDate = startTimeObj.realDate;
    let startRealDateUtc = startTimeObj.realDateUtc;
    let startRealDateTimeStamp = startTimeObj.realDateTimeStamp;

    endDateTimeStamp = Math.max(...realEndRimeArr); 
    let endTimeObj = getDateFromTimeStamp(endDateTimeStamp);
    let endRealDate = endTimeObj.realDate;
    let endRealDateUtc = endTimeObj.realDateUtc;
    let endRealDateTimeStamp = endTimeObj.realDateTimeStamp;

    return {
        "startRealDate": startRealDate,
        "startRealDateUtc": startRealDateUtc,
        "startRealDateTimeStamp": startRealDateTimeStamp,
        "endRealDate": endRealDate,
        "endRealDateUtc": endRealDateUtc,
        "endRealDateTimeStamp": endRealDateTimeStamp,
        "setTime": setTime
    }


} // End let getStartAndEndTimeOfRole = (setTime) => {

// This function is written to update job
let updatejob = async (updateWith, updateWhere) => {
    return new Promise(function(resolve, reject){

        models.job.updateOne(updateWhere,updateWith, function (updatederror, updatedresponse) {
            if(updatederror){
                resolve({   
					"isError": true,
				});
            }
            else{
                console.log("kkkk")
                if (updatedresponse.nModified == 1) {
                    resolve({   
                        "isError": false,
                    });
                }
                else {
                    resolve({   
                        "isError": true,
                    });
                }
            }
        })		
    });    
}    


// This function is written to update job
let fetchJob = async (aggrQuery) => {
    return new Promise(function(resolve, reject){

        models.job.aggregate(aggrQuery).exec((error, response) => {
            if (error) {
                resolve({   
					"isError": true,
				});
            }
            else {
                resolve({   
                    "isError": false,
                    "response": response
				});
            }
        })
    }); // END return new Promise(function(resolve, reject){   
}

// this function is written to check wether the candidate requirement for the job is fullfilled
let checkJobAvaibility = async (attribute) => {
    let roleId = attribute.roleId;
    let jobId = attribute.jobId;
    
    let aggrQuery = [
        { 
            $match : { 'jobId': jobId } 
        },
        
        {
            $project: {
                '_id': 0,
                'jobDetails': 1,
                'hireList': 1
            }
        },
    ]; 
    
    let numberOfHiredCandidate = 0;
    let numberOfStaffRequired = 0;
    let isJobAvailabel = false;
    let roleWiseAction = [];

    let jobDefetchJobStatus = await fetchJob(aggrQuery);

    let hireList = jobDefetchJobStatus.response[0].hireList;
    roleWiseAction = jobDefetchJobStatus.response[0].jobDetails.roleWiseAction;
    let roleDetails;

    let i;
    for (i = 0; i < roleWiseAction.length; i++) {
        if (roleId == roleWiseAction[i].roleId) {
            numberOfStaffRequired = Number(roleWiseAction[i].noOfStuff);
            roleDetails = roleWiseAction[i];
        } // END if (roleId == roleWiseAction[i].roleId) {
    }// End for (i = 0; i < roleWiseAction.length; i++) {

    for (i = 0; i < hireList.length; i++) {
        console.log("roleId", hireList[i].roleId);
        if (hireList[i].roleId == roleId) {
            numberOfHiredCandidate = numberOfHiredCandidate + 1;
        }
    }// End for (i = 0; i < hireList.length; i++) {

    if (numberOfHiredCandidate < numberOfStaffRequired) {
        isJobAvailabel = true;
    } else {
        isJobAvailabel = false;
    }

    return new Promise(function(resolve, reject){
        resolve({   
            "isError": false,
            "numberOfStaffRequired": numberOfStaffRequired,
            "numberOfHiredCandidate":numberOfHiredCandidate,
            "roleDetails": roleDetails,
            "isJobAvailabel": isJobAvailabel
        });
    }); // END return new Promise(function(resolve, reject){       

} // END let checkJobAvaibility = async (attribute) => {


// This function is written to delete a particular role
let updateJobRoleToCancel = async (updateWith, updateWhere) => {
   
    return new Promise(function(resolve, reject){
    
        models.job.updateOne(updateWhere,updateWith,
            { safe: true, multi:true },  
            function (updatederror, updatedresponse) {
            if(updatederror){
                resolve({   
					"isError": true,
				});
            }
            else{
                if (updatedresponse.nModified == 1) {
                    resolve({   
                        "isError": false,
                        "message": "job deleted successfully"
                    });
                }
                else {
                    resolve({   
                        "isError": true,
                    });
                }
            }
        })		
    });    
}    


module.exports = {
    getStartAndEndTimeOfRole,
    updateJobRoleToCancel,
    getTimeAndDate,
    updatejob,
    fetchJob,
    checkCandidateIsHiredOrNot,
    getDifferenceBetweenTimeZone,
    getTimeAndDateWithTimeZone,
    checkJobAvaibility,
    cancelJobForParticularCandidateInHirelist
}