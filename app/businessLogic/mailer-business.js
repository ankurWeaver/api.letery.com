const nodemailer = require('nodemailer');
const employerJobBusiness = require("./employer-job-business");

// This function is used to send mail for cancellation of job to employer
let sendEmailToEmployerForJobCancellation = async (attribute) => {

    let { emailMessage, emailSubject, empEmailId} = attribute;

    let fetchAdminEmailForMailingStatus = await employerJobBusiness.fetchAdminEmailForMailing();  
    let emailForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.emailForMailing : "";
    let passwordForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.passwordForMailing : "";
              
    let mail_credential = {
		"user": emailForMailing,
        "pass": passwordForMailing
    };

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: mail_credential
    });
    
	return new Promise(function (resolve, reject) {
 
        var mailOptions = {
            from: 'AOT',
            to: empEmailId,
            subject: emailSubject,
            html: `Hi <br> ${emailMessage}`
        };
          
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log("Error Occur In sending mail");
            } else {
                console.log('Email sent sucessfully');
            }
        });
        
	}) // END return new Promise(function (resolve, reject) {
}


// This function is used to send mail for cancellation of job to candidate
let sendEmailToCandidateForJobCancellation = async (attribute) => {

    let { emailMessage, emailSubject, candidateEmailId} = attribute;

    let fetchAdminEmailForMailingStatus = await employerJobBusiness.fetchAdminEmailForMailing();  
    let emailForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.emailForMailing : "";
    let passwordForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.passwordForMailing : "";
              
    let mail_credential = {
		"user": emailForMailing,
        "pass": passwordForMailing
    };

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: mail_credential
    });
    
	return new Promise(function (resolve, reject) {
 
        var mailOptions = {
            from: 'AOT',
            to: candidateEmailId,
            subject: emailSubject,
            html: `Hi <br> ${emailMessage}`
        };
          
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log("Error Occur In sending mail");
            } else {
                console.log('Email sent sucessfully');
            }
        });
        
	}) // END return new Promise(function (resolve, reject) {
}

// This function is used to send mail for cancellation of job to candidate
let sendEmailToCandidateForJobCancellationToAdmin = async (attribute) => {

    let { emailMessage, emailSubject} = attribute;

    let fetchAdminEmailForMailingStatus = await employerJobBusiness.fetchAdminEmailForMailing();  
    let emailForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.emailForMailing : "";
    let passwordForMailing = fetchAdminEmailForMailingStatus.response ? fetchAdminEmailForMailingStatus.response.passwordForMailing : "";
              
    let mail_credential = {
		"user": emailForMailing,
        "pass": passwordForMailing
    };

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: mail_credential
    });
    
	return new Promise(function (resolve, reject) {
 
        var mailOptions = {
            from: 'AOT',
            to: emailForMailing,
            subject: emailSubject,
            html: `Hi <br> ${emailMessage}`
        };
          
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log("Error Occur In sending mail");
            } else {
                console.log('Email sent sucessfully');
            }
        });
        
	}) // END return new Promise(function (resolve, reject) {
}


module.exports = {
    sendEmailToEmployerForJobCancellation,
    sendEmailToCandidateForJobCancellation
}