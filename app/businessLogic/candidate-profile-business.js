const bcrypt = require('bcrypt');
const token_gen = require('../service/jwt_token_generator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const models = require('../model');

const offsiteTracking = require('../model/offsiteTracking');
const employer = require('../model/employer');

const errorMsgJSON = require('../service/errors.json');
const AutogenerateIdcontroller = require('../service/AutogenerateId');
const mailercontroller = require('../service/verification-mail');
const base64controller = require('../service/ImageUploadBase64');
const nodemailer = require('nodemailer');
const TokenGenerator = require('uuid-token-generator');
const sendsmscontroller = require('../service/TwilioSms');
const simplemailercontroller = require('../service/mailer');
var http = require('http');
const config =  require('../config/env');
const fcm = require('../service/fcm');
const { AvailablePhoneNumberCountryContext } = require('twilio/lib/rest/api/v2010/account/availablePhoneNumber');

// This function is written to fetch the detail of Employer
let fetchCandidateDetails = async (aggrQry) => {
	
    return new Promise(function(resolve, reject){

		models.candidate.aggregate(aggrQry).exec((err, response) => {
			if(err){
				resolve({   
					"isError": true,
				});

			} else {

				let responseLength = response.length;

			    resolve({   
					"isError": false,
					"response": response,
					"responseLength":responseLength
				});
			}	
		});
	}) // END of  return new Promise(function(resolve, reject)  		
}


// This function is used cancel the job for a particular candidate
let updateHiredJobListOfCandidate = (attribute) => {
	let candidateId = attribute.candidateId.trim();
	let roleId = attribute.roleId.trim();
	let jobId = attribute.jobId.trim();
	let cancelledBy = attribute.cancelledBy.trim();

	
	let hiredJobs = {
		"roleId" : roleId,
		"jobId" : jobId,
		"candidateId" : candidateId
	};

	let currentDate = new Date();
	let currentTimestamp = currentDate.getTime();

	let cancelledJobs = {
		"roleId" : roleId,
		"jobId" : jobId,
		"candidateId" : candidateId,
		"currentDate": currentDate,
		"currentTimestamp": currentTimestamp,
		"cancelledBy": cancelledBy
	};

    return new Promise(function(resolve, reject){
		models.candidate.updateOne(
			{"candidateId":candidateId},
			{ 
				"$pull": { 
					"hiredJobs" : hiredJobs
				},
				"$push": { 
					"cancelledJobs" : cancelledJobs
				}
			},
			{ safe: true, multi:true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{
					
					if (updatedresponse.nModified == 1) {
						resolve({   
							"isError": false,
							"message": "successfully cancelled"
						});
					}
					else {
						resolve({   
							"isError": true,
							"message": "Either the candidate cancelled the job previously or some error occur"
						});
					}
				}
		}) // END models.candidate.updateOne(		

	}); // END return new Promise(function(resolve, reject){	
} // END let updateHiredJobListOfCandidate = (attribute) => {

// This function is used send a notification to the candidate
let updateNotificationForCandidate = (attribute) => {
	let candidateId = attribute.candidateId;
	let notificationMsg = attribute.notificationMsg;

	let currentRimeStamp = new Date().getTime();
	let notification = {
		"generatedAt" : currentRimeStamp,
		"notificationMsg" : notificationMsg
	};
	
    return new Promise(function(resolve, reject){
		models.candidate.updateOne(
			{"candidateId":candidateId},
			{ "$push": { 
					"notification" : notification
				}
			}, 
			{ safe: true, multi:true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{
					
					if (updatedresponse.nModified == 1) {
						resolve({   
							"isError": false,
							"message": "notification sent",
							"notification": notification
						});
					}
					else {
						resolve({   
							"isError": true,
							"message": "Error Occur"
						});
					}
				}
		}) // END models.employer.updateOne(		

	}); // END return new Promise(function(resolve, reject){	
} // END let updateHiredJobListOfCandidate = (attribute) => {


// This function is used send a notification to many candidate
let updateNotificationForManyCandidate = (attribute) => {
	let candidateIdList = attribute.candidateIdList;
	let notificationMsg = attribute.notificationMsg;

	let currentRimeStamp = new Date().getTime();
	let notification = {
		"generatedAt" : currentRimeStamp,
		"notificationMsg" : notificationMsg
	};
	
    return new Promise(function(resolve, reject){
		models.candidate.updateMany(
			{"candidateId": { $in: candidateIdList } },
			{ "$push": { 
					"notification" : notification
				}
			}, 
			{ safe: true, multi:true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{
					
					if (updatedresponse.nModified == 1) {
						resolve({   
							"isError": false,
							"message": "notification sent",
							"notification": notification
						});
					}
					else {
						resolve({   
							"isError": true,
							"message": "Error Occur"
						});
					}
				}
		}) // END models.employer.updateOne(		

	}); // END return new Promise(function(resolve, reject){	
} // END let updateHiredJobListOfCandidate = (attribute) => {


// This function is used cancel the job for a all candidate of a particular job
let updateHiredJobListOfManyCandidate = (attribute) => {
	let candidateIdList = attribute.candidateIdList;
	let roleId = attribute.roleId.trim();
	let jobId = attribute.jobId.trim();
	let currentDate = new Date();
	let currentTimestamp = currentDate.getTime();
	// let cancelledJobs = attribute.cancelledJobs;

	let hiredJobs = {
		"roleId" : roleId,
		"jobId" : jobId
	};

	let cancelledJobs = {
		"roleId" : roleId,
		"jobId" : jobId,
		"currentDate": currentDate,
		"currentTimestamp": currentTimestamp,
		"cancelledBy": "employerAllCandidate"
	};

	// console.log("cancelledJobs", cancelledJobs);

    return new Promise(function(resolve, reject){

		models.candidate.updateMany(
			{"candidateId":{ $in: candidateIdList } },
			{ 
				"$pull": { 
					"hiredJobs": hiredJobs
				},
				"$push": { 
					"cancelledJobs": cancelledJobs
				}
			}, 
			{ safe: true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{	
					resolve({   
						"isError": false,
						"message": "Successfully Cancelled"
					});
				}
		}) // END models.candidate.updateOne(		

	}); // END return new Promise(function(resolve, reject){	
} // END let updateHiredJobListOfCandidate = (attribute) => {

// This function is used update candidate of a particular job
let updateManyCandidate = (updateWhere, updateWith) => {
	
    return new Promise(function(resolve, reject){

		models.candidate.updateMany(
			updateWhere,
			updateWith, 
			{ safe: true }, 
			function (updatederror, updatedresponse) {
				if(updatederror){
					resolve({   
						"isError": true,
					});
				}
				else{	
					resolve({   
						"isError": false,
						"message": "Successfully Cancelled"
					});
				}
		}) // END models.candidate.updateOne(		

	}); // END return new Promise(function(resolve, reject){	
} // END let updateHiredJobListOfCandidate = (attribute) => {



module.exports = {
	updateHiredJobListOfCandidate,
	updateHiredJobListOfManyCandidate,
	updateManyCandidate,
	updateNotificationForManyCandidate,
	updateNotificationForCandidate,
	fetchCandidateDetails
}