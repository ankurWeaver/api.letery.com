const mongoose = require('mongoose');
const locationSchema = new mongoose.Schema({
    name : String,
    addressLineOne:String,
    addressLineTwo:String,
    city : String,
    state : String,
    countryCode:String,
    postCode : String    
})
const employerSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    employerId : String,
    adminProvidedJobCommission:{
                        type:Number,
                        default:0
    }, 
    Status :{ 
        signedUpVia:String,
        isVerified :Boolean,
        isOnline :Boolean,
        isApprovedByAot :Boolean,
        isActive :Boolean,
        fcmTocken:{
            type:String,
            default:""
        },
        confirmationTocken :String
    }, 
    socialLoginDetails:{
        facebookId:String,
        linedinId:String
    },  
    companyName :{
        type:String,
        default:""
    },
    password:String,
    employerType:String,
    companyDescription :String,
    brandName :String,
    contactName :String,
    individualContactName :{
        type:String,
        default:""
    },
    industryId :[],
    profilePic : {
        type:String,
        default:""
    },
    contactTelephoneNo :String,
    companyNo:String, 
    vatNo :String,
    address :locationSchema,
    hiringCapacity :Number,
    EmailId :String,
    mobileNo:String,
    memberSince : String,
    userType:String,
    resentSearchDetails : [{
        name:{ type: String, lowercase: true },
        time:{ type: Date, default:new Date()}
        },
        
    ],

    //=====
    // added by ankur on 26-12-19
    notification:[{
        generatedAt:String,
        notificationMsg:String
    }],
    //=====

    templates: [{
        templateId: { type: String},
        templateName: String,
        locationTrackingRadius: {
            type:String,
            default:""
        },
        jobDetails: {
            industry: String,
            roleWiseAction: [
                {
                    roleId: String,
                    roleName: String,
                    skills: [],
                    jobType: String,
                    payType: String,
                    pay: String,
                    noOfStuff: String,
                    uploadFile: String,
                    paymentStatus: String,
                    status: String,
                    
                    payment: {
                        ratePerHour: Number,
                        noOfStuff: Number,
                        totalHour: Number,
                        vat: Number,
                        ni: Number,
                        fee:Number,
                        totalPayment: Number
                    },
                    setTime: [
                        {
                            shift:String,
                            startDate:Number,
                            endDate:Number,
                            startTime: String,
                            endTime: String,
                            totalHour: String 

                        },
                        
                    ],
                    locationName: String,
                    description: String,
                    distance: Number,
                    location:{
                        type: {
                                type: String, // Don't do `{ location: { type: String } }`
                                enum: ['Point'], // 'location.type' must be 'Point'
                                // required: true
                            },        
                            coordinates: {
                                type: [Number],
                            },
                         
                        },
                },
            ]
        }

    }],

    
  
},
{
    timestamps: true
})


const Employer = mongoose.model("Employer", employerSchema)

module.exports = Employer;