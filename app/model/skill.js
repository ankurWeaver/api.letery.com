const mongoose = require('mongoose');
const skillSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    skillName: String,    // Convention : "model_name"+"Id"
    skillId: String,
    status:Boolean
   
},
{
    timestamps: true 
})
const Skill = mongoose.model("skill", skillSchema)
module.exports = Skill