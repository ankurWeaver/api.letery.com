const mongoose = require('mongoose');

const jobRoleSchema = new mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        industryId: String,
        jobRoleId: String,
        skillIds: [],
        jobRoleName: String,
        jobRoleIcon: {type: String, default: ""}

    },
    {
        timestamps: true
    })


const Jobrole = mongoose.model("jobrole", jobRoleSchema)

module.exports = Jobrole;

