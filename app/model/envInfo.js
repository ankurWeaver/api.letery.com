const mongoose = require('mongoose');

const envInfoSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    ni: Number,
    vat: Number,
    fee: Number
   
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const EnvInfo = mongoose.model("EnvInfo", envInfoSchema)

module.exports = EnvInfo;