const mongoose = require('mongoose');
//type: String, default: "DOC"+(new Date()).getTime()
function toLower (v) {
    return v.toLowerCase();
}
const qualificationSchema = new mongoose.Schema({
    degreeName : String,
    passingYear : String,
    marks : Number,
    institute : String
})

const candidateSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    candidateId : String,    // Convention : "model_name"+"Id"
    fname : String,
    mname : String,
    lname : String,
    email_address : String,
    home_address : String,

    isPushNotificationStatus: {
        type: Boolean,
        enum: [true, false],
        default: true
    },

    EmailId:{
    type: String, set: toLower },
    secondaryEmailId:{
        type: String, set: toLower },
    Status :{ 
        signedUpVia:String,
        isVerified :Boolean,
        isOnline :Boolean,
        isApprovedByAot :Boolean,
        isActive :Boolean,
        fcmTocken:{
            type:String,
            default:""
        },
        confirmationTocken :String
    },
    socialLoginDetails:{
        facebookId:String,
        linedinId:String
    }, 
    password:String,
    dob:String,
    age:Number,
    profilePic :{
        type:String,
        default:""
    },
    mobileNo : String,
    contactNo:String,
    language:[{name:String,code:String,rank:String,nativeName:String}],
   
    location : {
        city : String,
        country : Number, 
        postCode:String,
        address:String,
    },
    qualification : qualificationSchema,
    trainingDetails : [],
   
 
    workExperience:{
        expInMonths : String,
        experiences:[{
            industryId:String,
            jobRole : String,
            description : String,
            companyName : String,
            startDate : String,
            endDate : String,
            referenceName:String,
            referenceContact:String ,
            referenceEmailAddress:String,
            referencePosition:String,
            referenceAddressDetails:String,
            expId:{ type: String, default: "EXP"+(new Date()).getTime() },
        }]
        
    },
    dayoff:[{
        startDate:String,
        endDate:String
    }],
    //=====
    // added by ankur
    notification:[{
        generatedAt:String,
        notificationMsg:String
    }],
    //=====
    cv : { cvName :String,
           cvLink:String
        },
    docs : [{
        docName:String,
        docLink:String,
        docId:{ 
            type:mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
            auto: true,
         },
    }],
    video : {
        videoName :String,
        videoLink:String,
    },
    availability:[{
        day : String,
        Timeslot: []
    }],
    skills : [],
    
    rating : {
        type:Number,
        default:5
    },

    lastCancellationTime : {
        type:Number,
        default:0
    },

    memberSince : String,
    profileSummery : String,
    experienceInMonths : Number,
    jobOffers : [],
    acceptedJobs : [],
    declinedJobs : [],
    appliedJobs : [],
    hiredJobs : [],
    cancelledJobs : [],
    invitationToApplyJobs : [],
  
    selectedRole:[],

    //================
    payloadSkill:[{
        jobRoleId:String,
        jobRoleName:String,
        industryId:String,   
        skillDetails:[{
            skillId: String,
            skillName: String,
            isSelected : { 
                type : Boolean,
                enum : [true,false],
                default : false
            },
        }]    
    }],
    //================

    userType:String,
    distance:Number,
    kinDetails:{
        kinName:String,
        kinContactNo:String,
        relationWithKin:String
    },
    ratingAvg:Number,
    favouriteBy:[],

    

    geolocation:{
        type: {
            type: String, // Don't do `{ location: { type: String } }`
            enum: ['Point'], // 'location.type' must be 'Point'
            // required: true
        },        
        coordinates: {
            type: [Number],
        },
        locationName: String
        
    },

  
       
},

{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const Candidate = mongoose.model("Candidate", candidateSchema)

module.exports = Candidate;