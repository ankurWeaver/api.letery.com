const mongoose = require('mongoose');

const citySchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    citiesid:String,
    country_id:String,
    state_id:String,
    name:String
   
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const Citie = mongoose.model("Citie", citySchema)

module.exports = Citie;