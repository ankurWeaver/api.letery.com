const mongoose = require('mongoose');

const calenderDataSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    jobId: String,
    roleId: String,
    candidateId:String,
    isCheckedIn:Boolean,
    lastJobType:String,
    workingDays:[
        {
                date:String,
                shiftTime:[{
                    shift:String,
                    startTime:String,
                    endTime:String
                }],
                totalTimeInMinutes:{
                    type:Number,
                    default:0
                },
                //====================================== 
                totalOnsiteCheckinTimeInMns:{
                    type:Number,
                    default:0
                },

                totalonSiteTime:{
                    type:String,
                    default:''
                },

                totaloffSiteCheckinTimeInMns:{
                    type:Number,
                    default:0
                },

                totaloffSiteTime:{
                    type:String,
                    default:''
                },
                //==============================


                dateInTimestampFormat:Number,
                dateInGMTFormat:String,
                finalCheckIn:Number,
                slotType:String,
                finalCheckOut:Number,
                totalTime: {
                    type:String,
                    default:""
                },
                absentStatus:Number,
                lastOffSideId:{
                    type:String,
                    default:""
                },
                lastOnSideId:{
                    type:String,
                    default:""
                },
                onSite:[{
                    "onSideId":{
                        type:String,
                        default:""
                    },
                    "checkIn": {
                        type:Number,
                        default:0
                    },

                    //==================================================
                    "checkoutlocationName":{
                        type:String,
                        default:""
                    },
                    checkoutlocation:{
                        type: {
                                type: String, // Don't do `{ location: { type: String } }`
                                enum: ['Point'], // 'location.type' must be 'Point'
                            },        
                            coordinates: {
                                type: [Number],
                            },
                            
                        },
                    //===================================================

                    "checkOut": {
                        type:Number,
                        default:0
                        // default:23.5
                    },

                    "destinationName":{
                        type:String,
                        default:""
                    },
                    geolocation:{
                        type: {
                                type: String, // Don't do `{ location: { type: String } }`
                                enum: ['Point'], // 'location.type' must be 'Point'
                                // required: true
                            },        
                            coordinates: {
                                type: [Number],
                            },
                            
                    }, 

                }],
                offSite:[{
                    "offSideId":{
                        type:String,
                        default:""
                    },
                    "checkIn": {
                        type:Number,
                        default:0
                    },
                    //==================================================
                    "checkoutlocationName":{
                        type:String,
                        default:""
                    },
                    checkoutlocation:{
                        type: {
                                type: String, // Don't do `{ location: { type: String } }`
                                enum: ['Point'], // 'location.type' must be 'Point'
                                // required: true
                            },        
                            coordinates: {
                                type: [Number],
                            },
                            
                        },
                    //===================================================
                    "checkOut": {
                        type:Number,
                        // default:23.5
                        default:0
                    },
                    "destinationName":{
                        type:String,
                        default:""
                    },
                    geolocation:{
                        type: {
                                type: String, // Don't do `{ location: { type: String } }`
                                enum: ['Point'], // 'location.type' must be 'Point'
                                // required: true
                            },        
                            coordinates: {
                                type: [Number],
                            },
                            
                        },
                }],
                extraTime:[],
                
        }
    ],
    extraDays:[]
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const CalenderData = mongoose.model("CalenderData", calenderDataSchema)

module.exports = CalenderData;