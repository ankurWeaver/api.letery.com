const mongoose = require('mongoose');
const otpSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    email: String,    // Convention : "model_name"+"Id"
    otp : String,
   
},
{
    timestamps: true 
})
const Otp = mongoose.model("Otp", otpSchema)
module.exports = Otp