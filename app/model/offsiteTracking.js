const mongoose = require('mongoose');
function toLower (v) {
    return v.toLowerCase();
}
const offsiteTrackingSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    offsiteTrackingId: String,
    candidateId:String,
    jobId:String,
    roleId:String,
    employerId:String,
    date: String,

    OffsiteData:[{
        "offSideId":{
            type:String,
            default:""
        },
        "checkinTime": {
            type:String,
            default:""
        },
       
        "destinationName":{
            type:String,
            default:""
        },
        geolocation:{
            type: {
                    type: String, // Don't do `{ location: { type: String } }`
                    enum: ['Point'], // 'location.type' must be 'Point'
                },        
                coordinates: {
                    type: [Number],
                },
                
        },
    }],
   
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const OffsiteTracking = mongoose.model("OffsiteTracking", offsiteTrackingSchema)

module.exports = OffsiteTracking;

