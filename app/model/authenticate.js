const mongoose = require('mongoose');
function toLower (v) {
    return v.toLowerCase();
}
const authenticateSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    userId:String,
    EmailId:{
        type: String, set: toLower },
    type:String,
    mobileNo:String,
    password:String,
    emailForMailing:String,
    passwordForMailing:String


        
   
   
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const Authenticate = mongoose.model("Authenticate", authenticateSchema)

module.exports = Authenticate;