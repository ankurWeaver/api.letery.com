const mongoose = require('mongoose');
const ratingSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    employerId : String,
	candidateId :String,
    roleId:String,
    review:String,
    rating:Number,
    jobId:String
   
},
{
    timestamps: true 
})
const rating = mongoose.model("rating", ratingSchema)
module.exports = rating