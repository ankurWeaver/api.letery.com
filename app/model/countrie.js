const mongoose = require('mongoose');

const countrySchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    countryId:Number,
    sortname:String,
    name:String,
    phoneCode:String
   
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const Countrie = mongoose.model("Countrie", countrySchema)

module.exports = Countrie;