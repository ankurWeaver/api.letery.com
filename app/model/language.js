const mongoose = require('mongoose');

const languageSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    code:String,
    name:String,
    nativeName:String
   
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const Language = mongoose.model("Language", languageSchema)

module.exports = Language;