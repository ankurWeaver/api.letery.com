const mongoose = require('mongoose');

const industrySchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    industryId:String,
    industryName:{ type: String, unique: true },
   
},
{
    timestamps: true
})


const Industry = mongoose.model("industry", industrySchema)

module.exports = Industry;

