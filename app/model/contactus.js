const mongoose = require('mongoose');

const contactusSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    contactusId:String,
    emailId:String,
    supportNo:String,
},
{
    timestamps: true // add created_at , updated_at at the time of insert/update
})


const contactus = mongoose.model("contactus", contactusSchema)

module.exports = contactus;