

const candidateModel =  require('./candidate');
const otpModel =  require('./otp');
const empModel =  require('./employer');
const authenticateModel =  require('./authenticate');
const timeToLiveModel =  require('./timeToLive');
const countryModel =  require('./countrie');
const cityModel =  require('./city');
const languageModel =  require('./language');
const timeSloteModel =  require('./timeSlot');
const jobModel =  require('./job');
const industryModel =  require('./industry');
const jobRoleModel =  require('./jobRole');
const skillModel =  require('./skill');
const utilityModel =  require('./utility');
const ratingModel =  require('./rating');
const calenderDataModel =  require('./calenderData');
const contactusDataModel =  require('./contactus');


module.exports = {
    'candidate' : candidateModel,
    'Otp' : otpModel,
    'employer':empModel,
    'authenticate':authenticateModel,
    'timeTOLive':timeToLiveModel,
    'Countrie':countryModel,
    'Citie':cityModel,
    'language':languageModel,
    'timeslot':timeSloteModel,
    'job':jobModel,
    'industry':industryModel,
    'jobrole':jobRoleModel,
    'skill':skillModel,
    'utility':utilityModel,
    'rating':ratingModel,
    'CalenderData': calenderDataModel,
    'contactus':contactusDataModel
    

}