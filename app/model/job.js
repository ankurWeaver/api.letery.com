const mongoose = require('mongoose');
//type: String, default: "JOB"+(new Date()).getTime()
const jobSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    jobId: {
        type: String,
    },
    jobStartDateTimeStamp: Number,
    jobStartDateUtc: String,
    jobStartDateTimeStampLocal: String,

    jobEndDateTimeStamp: Number,
    jobEndDateUtc: String,
    jobEndDateTimeStampLocal: String,

    employerId: String,
    jobDetails: {
        industry: String,
        roleWiseAction: [
            {
                roleId: String,
                roleName: String,
                jobRoleIcon: {type: String, default: ""},
                skills: [],
                jobType: String,
                payType: String,
                pay: String,
                noOfStuff: String,
                uploadFile: String,
                paymentStatus: String,
                status: String,
                locationTrackingRadius:{
                    type:Number,
                    default:10
                },
                payment: {
                    ratePerHour: Number,
                    noOfStuff: Number,
                    totalHour: Number,
                    vat: Number,
                    ni: Number,
                    fee: Number,
                    totalPayment: Number
                },
                setTime: [
                    {
                        shift: String,
                        startDate: Number,
                        realIdividualStartTimeStamp: Number,
                        endDate: Number,
                        startTime: String,
                        realIdividualEndTimeStamp: Number,
                        endTime: String,
                        totalHour: String,
                    }
                ],
                location: {
                    type: {
                        type: String, // Don't do `{ location: { type: String } }`
                        enum: ['Point'], // 'location.type' must be 'Point'
                        // required: true
                    },
                    coordinates: {
                        type: [Number],
                    },
                },
                isDeleted: {
                    type: String,
                    enum: ['YES', 'NO'],
                    default: 'NO'
                },
                distance: Number,
                description: String,
                locationName: String,
                startRealDate: String,
                startRealDateUtc: String,
                startRealDateTimeStamp: Number,
                endRealDate: String,
                endRealDateUtc: String,
                endRealDateTimeStamp: Number

            },
        ],
    },
    appiliedFromBackupDb: [
        {
            roleId: String,
            candidateId: String,

        }
    ],
    appiliedFrom: [
        {
            roleId: String,
            candidateId: String,

        }
    ],
    hireList: [
        {
            roleId: String,
            candidateId: String,
            currentTimestamp: String,
            currentDate: String,
        }
    ],

    cancelledJobs: [
        {
            roleId: String,
            candidateId: String,
            currentTimestamp: String,
            currentDate: String,
            cancelledBy: {
                type: String, 
                enum: ['employerAllCandidate', 'employerOneCandidate', 'Candidate'], 
            },
        }
    ],

    acceptList: [
        {
            roleId: String,
            candidateId: String,

        }
    ],
    rejectList: [
        {
            roleId: String,
            candidateId: String,

        }
    ],
    approvedTo: [
        {
            roleId: String,
            candidateId: String,
            completionPercentage: String
        }
    ],

},
{
    timestamps: true
})


const Job = mongoose.model("job", jobSchema)

module.exports = Job;

