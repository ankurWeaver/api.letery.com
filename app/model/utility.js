const mongoose = require('mongoose');

const utilitySchema = new mongoose.Schema({
  aboutUs:[{
    userType:String,
    aboutUsDetails:String

  }],
  privacyPolicy:[{
    userType:String,
    PolicyDetails:String

  }],
  termsCondition:[{
    userType:String,
    conditionDetails:String

  }],
  faq:[{
    userType:String,
    questionId:String,
    question:String,
    answer:String,
    position:Number
  }]
},
{
    timestamps: true
})


const Utility = mongoose.model("utility", utilitySchema)

module.exports = Utility;
