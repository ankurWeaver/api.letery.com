const candidateRoutes =  require('./endpoint/candidate-authentication');
const empRoutes =  require('./endpoint/employer-authentication');
const empProfileRoutes =  require('./endpoint/employer-profile');
const adminRoutes =  require('./endpoint/admin');
const commonRoutes =  require('./endpoint/common');
const candidateProfileRoutes =  require('./endpoint/candidate-profile');
const employerJobRoutes =  require('./endpoint/employer-job');
const candidateJobRoutes =  require('./endpoint/candidate-job');
const express =  require('express')
const appRoutes = express.Router();
appRoutes.use('/candidate', candidateRoutes);
appRoutes.use('/employer', empRoutes);
appRoutes.use('/employer-profile', empProfileRoutes);
appRoutes.use('/admin', adminRoutes);
appRoutes.use('/common', commonRoutes);
appRoutes.use('/candidate-profile', candidateProfileRoutes);
appRoutes.use('/emp-job', employerJobRoutes);
appRoutes.use('/candidate-job', candidateJobRoutes);
module.exports = appRoutes;